#define DD
// #define DD1

#include "env.h"

#ifdef SIMUL
#ifdef TMAIN
#define EXTERN
#else
#include "def.h"
#include "config.h"
#include "common.h"
#include "stb.h"
#include "prique.h"
#include "dbg.h"
#include "conf.h"
#include "devsim.h"
#include "dev.h"
#include "event.h"
#include "obs.h"
#include "as.h"

#include "cpu.h"
#include "exec.h"
#include "init.h"

#include "regdev.h"
#include "simulsub2.h"
#endif
#else
#define EXTERN
#endif
#include "disasm.h"
#undef EXTERN
#include "dump.h"

#define DDLOG(p) ((p)->logd && (p)->execcount + 1 >= (p)->logd)

#ifdef SIMUL
#define EXEC(n,m,f) \
  ADDSYM(f); \
  dect[(n)].func[(m)].pexec = (f)
#define EXEC4(n,m,f) \
  ADDSYM(f); \
  dect4[(n)].func[(m)].pexec = (f)
#define EXECAS(n,m,f) \
  ADDSYM(f); \
  dectas[(n)].func[(m)].pexec = (f)
#define EXECMV(n,m,f) \
  ADDSYM(f); \
  dectmv[(n)].func[(m)].pexec = (f)
#define EXECBF(n,m,f) \
  ADDSYM(f); \
  dectbf[(n)].func[(m)].pexec = (f)
#define EXECDP(n,m,f) \
  ADDSYM(f); \
  dectdp[(n)].func[(m)].pexec = (f)
#define EXECDPCS(n,m,f) \
  ADDSYM(f); \
  dectdpcs[(n)].func[(m)].pexec = (f)
#define EXECRCSH(n,m,f) \
  ADDSYM(f); \
  dectrcsh[(n)].func[(m)].pexec = (f)
#define EXECMIS(n,m,f) \
  ADDSYM(f); \
  dectmis[(n)].func[(m)].pexec = (f)
#define EXECSOZ(n,m,f) \
  ADDSYM(f); \
  dectsoz[(n)].func[(m)].pexec = (f)
#define EXECSIMD(n,m,f) \
  ADDSYM(f); \
  dectsimd[(n)].func[(m)].pexec = (f)
#define EXECOTHER(n,m,f) \
  ADDSYM(f); \
  dectother[(n)].func[(m)].pexec = (f)
#define EXEC32MD(n,m,f) \
  ADDSYM(f); \
  dect32md[(n)].func[(m)].pexec = (f)
#define EXEC64MD(n,m,f) \
  ADDSYM(f); \
  dect64md[(n)].func[(m)].pexec = (f)
#define EXECLSETB(n,m,f) \
  ADDSYM(f); \
  dectlsetb[(n)].func[(m)].pexec = (f)

#else

#define ADDSYM(s)
#define EXEC(n,m,f)
#define EXECAS(n,m,f)
#define EXECDP(n,m,f)
#define EXECMV(n,m,f)
#define EXECBF(n,m,f)
#define EXECDPCS(n,m,f)
#define EXECRCSH(n,m,f)
#define EXECMIS(n,m,f)
#define EXECSOZ(n,m,f)
#define EXECSIMD(n,m,f)
#define EXECOTHER(n,m,f)
#define EXEC32MD(n,m,f)
#define EXEC64MD(n,m,f)
#define EXECLSETB(n,m,f)
#define EXEC4(n,m,f)

#endif
char *fmtfname(struct t_cpu *p_cpu,void *p,char *out)
{

  if (!p)
  {
    strcpy(out,"<null>");
    return out;
  }
#ifdef XX
  struct t_sym *ps = 0;
  ps = looksym(&p_cpu->ctsymtab,p);

  if (ps)
    strcpy(out,ps->name);
  else
    sprintf(out,"%p",p);
#else
  sprintf(out,"%p",p);
#endif

  return out;
}

int VFPExpandImm32(int imm)
{
  int v,e,s,f,exp,frac;
  e = 8;
  f = 32 - e - 1;
//printf("VFPExpandImm32 imm %d %x e %d f %d\n",imm,imm,e,f);
  exp = (imm >> 4) & 3;
  s = (imm >> 7) & 1;
  if ((imm >> 6) & 1)
    exp |= (0x1f << 2);
  exp |= ~((imm >> 6) & 1) << 7;
  frac = (imm & 0xf) << (f - 4);
//printf("s %d exp %x frac %x\n",s,exp,frac);
  v = (s << 31) | ((exp & 0xff) << f) | frac;
  return v;
}

int DecodeImmShift(int ty,int imm5,int *t)
{
  *t = -1;
  if (ty == 0)
  {
    *t = 0;  // LSL
    return imm5;
  } else if (ty == 1)
  {
    *t = 1;  // LSR
    if (imm5 == 0)
      return 32;
    return imm5;
  } else if (ty == 2)
  {
    *t = 2;  // LSR
    if (imm5 < 32)
      return imm5;
    else
      return 0;
  } else
  {
    printf("DecodeImmShift ty %d\n",ty);
    error("TODO in DecodeImmShift");
  }
  return 0;
}

int ThumbExpandImm(int v)
{
  int C;
  C = 0;
  return ThumbExpandImmWithC(v,&C);
}

int ThumbExpandImmWithC(int v,int *C)
{
  int ty,ty2,uv,n;

  ty = (v >> 10) & 3;
//printf("ThumbExpandImmWithC v %08x C %d ty %d\n",v,*C,ty);
  if (ty == 0)
  {
    ty2 = (v >> 8) & 3;
    if (ty2 == 0)
      v = v & 0xff;
    else if (ty2 == 1)
    {
      v = v & 0xff;
      v = (v << 16) | v;
    } else if (ty2 == 2)
    {
      v = v & 0xff;
      v = (v << 24) | (v << 8);
    } else if (ty2 == 3)
    {
      v = v & 0xff;
      v = (v << 24) | (v << 16) | (v << 8) | v;
    } else
    {
      error("tbd in ThumbExpandImmWithC ty %d ty2 %d",ty,ty2);
    }
  } else
  {
/*
unrotated_value = ZeroExtend('1':imm12<6:0>, 32);
(imm32, carry_out) = ROR_C(unrotated_value, UInt(imm12<11:7>));
*/
    uv = (1 << 7) | (v & 0x7f);
    n  = (v >> 7) & 0x1f;
//  printf("ur %08x n %x\n",uv,n);
    v = (uv >> n) | (uv << (32 - n));
//  printf("v %x\n",v);
    *C = (v >> 31) & 1;
  }

  return v;
}

int findpos(struct t_dec *pd,char *f)
{
  int i;
  char *p,*q;

  for (i = 0; i < pd->nf; i++)
  {
    p = strstr(pd->f[i].name,f);
    if (p)
      if (p == pd->f[i].name || p[-1] == ',')
      {
        q = p + strlen(f);
        if (*q == 0 || *q == ',')
          return i;
      }
  }
  return -1;
}

void adddec(int t,char *n,int l,int p,int s,int sw)
{
  int i;
  i = dect[t].nf;
  dect[t].f[i].name = n;
  dect[t].f[i].len = l;
  dect[t].f[i].pos = p;
  dect[t].f[i].sig = s;
  dect[t].f[i].sw  = sw;
  dect[t].nf++;
  if (dect[t].nf >= DEC_FIELD)
    error("overflow in adddec");
}

void adddec4(int t,char *n,int l,int p,int s,int sw)
{
  int i;
  i = dect4[t].nf;
  dect4[t].f[i].name = n;
  dect4[t].f[i].len = l;
  dect4[t].f[i].pos = p;
  dect4[t].f[i].sig = s;
  dect4[t].f[i].sw  = sw;
  dect4[t].nf++;
}

void initcondt()
{
  int i,cond,N,Z,C,V;
  
  condmem[0]  = "EQ";
  condmem[1]  = "NE";
  condmem[2]  = "CS";
  condmem[3]  = "CC";
  condmem[4]  = "MI";
  condmem[5]  = "PL";
  condmem[6]  = "VS";
  condmem[7]  = "VC";
  condmem[8]  = "HI";
  condmem[9]  = "LS";
  condmem[10] = "GE";
  condmem[11] = "LT";
  condmem[12] = "GT";
  condmem[13] = "LE";
  condmem[14] = "AL";
  condmem[15] = "--";

  memset(condt,-1,sizeof(condt));
  for (i = 0; i < 256; i++)
  {
    cond = i >> 4;
    N = (i >> 3) & 1;
    Z = (i >> 2) & 1;
    C = (i >> 1) & 1;
    V = (i >> 0) & 1;
    if (cond == 0)       // EQ
      condt[i] = (Z == 1);
    else if (cond == 1)  // NE
      condt[i] = (Z == 0);
    else if (cond == 2)  // CS
      condt[i] = (C == 1);
    else if (cond == 3)  // CC
      condt[i] = (C == 0);
    else if (cond == 4)  // MI 
      condt[i] = (N == 1);
    else if (cond == 5)  // PL 
      condt[i] = (N == 0);
    else if (cond == 6)  // VS 
      condt[i] = (V == 1);
    else if (cond == 7)  // Vc 
      condt[i] = (V == 0);
    else if (cond == 8)  // HI 
      condt[i] = (C == 1) && (Z == 0);
    else if (cond == 9)  // LS 
      condt[i] = (C == 0) || (Z == 1);
    else if (cond == 10) // GE 
      condt[i] = N == V;
    else if (cond == 11) // LT 
      condt[i] = N != V;
    else if (cond == 12) // GT 
      condt[i] = (N == V) && Z == 0;
    else if (cond == 13) // LE 
      condt[i] = (Z == 1 || N != V);
    else if (cond == 14) // AL always 
      condt[i] = 1;
// 15 --
  }
}

void initdec(struct t_cpu *p_cpu)
{
  int i,ist;

  ADDSYM(exec_TBD);
// 
// dect for 16 bits
//   3.2 Instruction encoding for 16-bit Thumb instructions
// dect4 for 32 bits  
//   3.3 Instruction encoding for 32-bit Thumb instructions

  dect[0].desc = "Table 3-2 Shift by immediate and move (register) instructions [1]";
  dect[0].nfunc = 4;

  ADDSYM(deco_movsh);
  dect[0].pfd   = deco_movsh;

//ADDSYM(exec_movs);
//dect[0].func[0].pexec = exec_movs;
  EXEC(0,0,exec_movs);
  dect[0].func[0].fmtinst = "MOVS <rd>,<rm>";

//ADDSYM(exec_lsli);
//dect[0].func[1].pexec = exec_lsli;
  EXEC(0,1,exec_lsli);
  dect[0].func[1].fmtinst = "LSL<#S> <rd>,<rm>,#<imm5>";

//ADDSYM(exec_lsri);
//dect[0].func[2].pexec = exec_lsri;
  EXEC(0,2,exec_lsri);
  dect[0].func[2].fmtinst = "LSR<#S> <rd>,<rm>,#<imm5>";

//ADDSYM(exec_asri);
//dect[0].func[3].pexec = exec_asri;
  EXEC(0,3,exec_asri);
  dect[0].func[3].fmtinst = "ASR<#S> <rd>,<rm>,#<imm5>";

  adddec(0,"opcode",2,11,0,1);
  adddec(0,"imm5"  ,5, 6,0,0);
  adddec(0,"rm"    ,3, 3,0,0);
  adddec(0,"rd"    ,3, 0,0,0);

  dect[1].desc = "Table 3-3 Add and subtract (register) instructions [1]";
  dect[1].nfunc = 2;

//ADDSYM(exec_addr);
//dect[1].func[0].pexec = exec_addr;
  EXEC(1,0,exec_addr);
  dect[1].func[0].fmtinst = "ADD<#S>  <rd>, <rn>, <rm>";

//ADDSYM(exec_subr);
//dect[1].func[1].pexec = exec_subr;
  EXEC(1,1,exec_subr);
  dect[1].func[1].fmtinst = "SUB<#S>  <rd>, <rn>, <rm>";

  adddec(1,"opc"   ,1, 9,0,1);
  adddec(1,"rm"    ,3, 6,0,0);
  adddec(1,"rn"    ,3, 3,0,0);
  adddec(1,"rd"    ,3, 0,0,0);

  dect[2].desc = "Table 3-4 Add and subtract (3-bit immediate) instructions [1]";
  dect[2].nfunc = 2;

//ADDSYM(exec_add2i);
//dect[2].func[0].pexec = exec_add2i;
  EXEC(2,0,exec_add2i);
  dect[2].func[0].fmtinst = "ADD<#S>     <rd>, <rn>, #<imm3>";

//ADDSYM(exec_sub2i);
//dect[2].func[1].pexec = exec_sub2i;
  EXEC(2,1,exec_sub2i);
  dect[2].func[1].fmtinst = "SUB<#S>     <rd>, <rn>, #<imm3>";

  adddec(2,"opc"   ,1, 9,0,1);
  adddec(2,"imm3"  ,3, 6,0,0);
  adddec(2,"rn"    ,3, 3,0,0);
  adddec(2,"rd"    ,3, 0,0,0);

  dect[3].desc = "Table 3-5 Add, subtract, compare, and move (8-bit immediate) instructions [1]";
  dect[3].nfunc = 4;

//ADDSYM(exec_movi);
//dect[3].func[0].pexec = exec_movi;
  EXEC(3,0,exec_movi);
  dect[3].func[0].fmtinst = "MOV<#S>     <rdn>, #<imm8>";

//ADDSYM(exec_cmpi);
//dect[3].func[1].pexec = exec_cmpi;
  EXEC(3,1,exec_cmpi);
  dect[3].func[1].fmtinst = "CMP<#c>     <rdn>, #<imm8>";

//ADDSYM(exec_addi);
//dect[3].func[2].pexec = exec_addi;
  EXEC(3,2,exec_addi);
  dect[3].func[2].fmtinst = "ADD<#S>     <rdn>, #<imm8>";
  
//ADDSYM(exec_subi);
//dect[3].func[3].pexec = exec_subi;
  EXEC(3,3,exec_subi);
  dect[3].func[3].fmtinst = "SUB<#S>     <rdn>, #<imm8>";

  adddec(3,"opcode",2, 11,0,1);
  adddec(3,"rdn",   3,  8,0,0);
  adddec(3,"imm8",  8,  0,0,0);

  dect[4].desc = "Table 3-6 Data processing (register) instructions [1]";
  dect[4].nfunc = 16;

//ADDSYM(exec_and);
//dect[4].func[0].pexec = exec_and;
  EXEC(4,0,exec_and);
  dect[4].func[0].fmtinst = "AND<#S>    <rd>, <rm>";

//ADDSYM(exec_eor);
//dect[4].func[1].pexec = exec_eor;
  EXEC(4,1,exec_eor);
  dect[4].func[1].fmtinst = "EOR<#S>    <rd>, <rm>";

//ADDSYM(exec_lsl);
//dect[4].func[2].pexec = exec_lsl;
  EXEC(4,2,exec_lsl);
  dect[4].func[2].fmtinst = "LSL<#S>    <rd>, <rm>";

//ADDSYM(exec_lsr);
//dect[4].func[3].pexec = exec_lsr;
  EXEC(4,3,exec_lsr);
  dect[4].func[3].fmtinst = "LSR<#S>    <rd>, <rm>";

//ADDSYM(exec_asr);
//dect[4].func[4].pexec = exec_asr;
  EXEC(4,4,exec_asr);
  dect[4].func[4].fmtinst = "ASR<#S>    <rd>, <rm>";

//ADDSYM(exec_adcrt1);
//dect[4].func[5].pexec = exec_adcrt1;
  EXEC(4,5,exec_adcrt1);
  dect[4].func[5].fmtinst = "ADC<#S>     <rd>, <rm>";

//ADDSYM(exec_sbcrt1);
//dect[4].func[6].pexec = exec_sbcrt1;
  EXEC(4,6,exec_sbcrt1);
  dect[4].func[6].fmtinst = "SBC<#S>     <rd>, <rm>";

//ADDSYM(exec_ror);
//dect[4].func[7].pexec = exec_ror;
  EXEC(4,7,exec_ror);
  dect[4].func[7].fmtinst = "ROR<#S>    <rd>, <rm>";

//ADDSYM(exec_tst);
//dect[4].func[8].pexec = exec_tst;
  EXEC(4,8,exec_tst);
  dect[4].func[8].fmtinst = "TST<#c>     <rd>, <rm>";

//ADDSYM(exec_rsbrt1);
//dect[4].func[9].pexec = exec_rsbrt1;
  EXEC(4,9,exec_rsbrt1);
  dect[4].func[9].fmtinst = "RSB<#S>     <rd>, <rm>, #0";

//ADDSYM(exec_cmp);
//dect[4].func[10].pexec = exec_cmp;
  EXEC(4,10,exec_cmp);
  dect[4].func[10].fmtinst = "CMP<#c>     <rd>, <rm>";

//ADDSYM(exec_cmnt1);
//dect[4].func[11].pexec = exec_cmnt1;
  EXEC(4,11,exec_cmnt1);
  dect[4].func[11].fmtinst = "CMN<#c>     <rd>, <rm>";

//ADDSYM(exec_orr);
//dect[4].func[12].pexec = exec_orr;
  EXEC(4,12,exec_orr);
  dect[4].func[12].fmtinst = "ORR<#S><#c>     <rd>, <rm>";

//ADDSYM(exec_mult1);
//dect[4].func[13].pexec = exec_mult1;
  EXEC(4,13,exec_mult1);
  dect[4].func[13].fmtinst = "MUL<#S>     <rd>, <rm>";

//ADDSYM(exec_bic);
//dect[4].func[14].pexec = exec_bic;
  EXEC(4,14,exec_bic);
  dect[4].func[14].fmtinst = "BIC<#S>     <rd>, <rm>";

//ADDSYM(exec_mvn);
//dect[4].func[15].pexec = exec_mvn;
  EXEC(4,15,exec_mvn);
  dect[4].func[15].fmtinst = "MVN<#S>     <rd>, <rm>";

  adddec(4,"opcode",4,  6,0,1);
  adddec(4,"rm",    3,  3,0,0);
  adddec(4,"rd",    3,  0,0,0);

  dect[5].desc = "Table 3-7 Special data processing instructions [1]";
  dect[5].nfunc = 3;

//ADDSYM(exec_addsr);
  ADDSYM(dump_addsr);
//dect[5].func[0].pexec = exec_addsr;
  EXEC(5,0,exec_addsr);
  dect[5].func[0].pdis  = dump_addsr;
  dect[5].func[0].fmtinst = "ADD<#c>     <0#R>, <rm>";

//ADDSYM(exec_cmpsr);
//dect[5].func[1].pexec = exec_cmpsr;
  EXEC(5,1,exec_cmpsr);
  dect[5].func[1].pdis  = dump_addsr;
  dect[5].func[1].fmtinst = "CMP<#c>     <0#R>, <rm>";

//ADDSYM(exec_movsr);
//dect[5].func[2].pexec = exec_movsr;
  EXEC(5,2,exec_movsr);
  dect[5].func[2].pdis  = dump_addsr;
  dect[5].func[2].fmtinst = "MOV<#c>     <0#R>, <rm>";

  adddec(5,"opcode",2, 8,0,1);
  adddec(5,"dn"    ,1, 7,0,0);
  adddec(5,"rm"    ,4, 3,0,0);
  adddec(5,"rd"    ,3, 0,0,0);

  dect[6].desc = "Table 3-8 Branch and exchange instruction set instructions [1]";
  dect[6].nfunc = 2;

//ADDSYM(exec_bx);
//dect[6].func[0].pexec = exec_bx;
  EXEC(6,0,exec_bx);
  dect[6].func[0].fmtinst = "BX      <rm> ; <rm#*>";

//ADDSYM(exec_blx);
//dect[6].func[1].pexec = exec_blx;
  EXEC(6,1,exec_blx);
  dect[6].func[1].fmtinst = "BLX     <rm> ; <rm#*>";

  adddec(6,"L"     ,1, 7,0,1);
  adddec(6,"rm"    ,4, 3,0,0);

  dect[7].desc = "load from literal pool";
  dect[7].nfunc = 1;

//ADDSYM(exec_ldrlp);
  ADDSYM(dump_ldrlp);
//dect[7].func[0].pexec = exec_ldrlp;
  EXEC(7,0,exec_ldrlp);
  dect[7].func[0].pdis  = dump_ldrlp;
  dect[7].func[0].fmtinst = "LDR<#c>     <rd>, [PC, #<0>] ; @<1>";

  adddec(7,"rd"    ,3, 8,0,0);
  adddec(7,"imm8"  ,8, 0,0,0);

  dect[8].desc = "Table 3-9 Load and store (register offset) instructions [1]";
  dect[8].nfunc = 8;

//ADDSYM(exec_strt1);
//dect[8].func[0].pexec = exec_strt1;
  EXEC(8,0,exec_strt1);
  dect[8].func[0].fmtinst = "STR<#c> <rd>,[<rn>,<rm>]";

//ADDSYM(exec_strht1);
//dect[8].func[1].pexec = exec_strht1;
  EXEC(8,1,exec_strht1);
  dect[8].func[1].fmtinst = "STRH<#c> <rd>,[<rn>,<rm>]";

//ADDSYM(exec_strbt1);
//dect[8].func[2].pexec = exec_strbt1;
  EXEC(8,2,exec_strbt1);
  dect[8].func[2].fmtinst = "STRB<#c> <rd>,[<rn>,<rm>]";

//ADDSYM(exec_ldrsbt1);
//dect[8].func[3].pexec = exec_ldrsbt1;
  EXEC(8,3,exec_ldrsbt1);
  dect[8].func[3].fmtinst = "LDRSB<#c> <rd>,[<rn>,<rm>]";

//ADDSYM(exec_ldrt1);
//dect[8].func[4].pexec = exec_ldrt1;
  EXEC(8,4,exec_ldrt1);
  dect[8].func[4].fmtinst = "LDR<#c> <rd>,[<rn>,<rm>]";

//ADDSYM(exec_ldrht1);
//dect[8].func[5].pexec = exec_ldrht1;
  EXEC(8,5,exec_ldrht1);
  dect[8].func[5].fmtinst = "LDRH<#c> <rd>,[<rn>,<rm>]";

//ADDSYM(exec_ldrbt1);
//dect[8].func[6].pexec = exec_ldrbt1;
  EXEC(8,6,exec_ldrbt1);
  dect[8].func[6].fmtinst = "LDRB<#c> <rd>,[<rn>,<rm>]";

//ADDSYM(exec_ldrsht1);
//dect[8].func[7].pexec = exec_ldrsht1;
  EXEC(8,7,exec_ldrsht1);
  dect[8].func[7].fmtinst = "LDRSH<#c> <rd>,[<rn>,<rm>]";

  adddec(8,"opcode" ,3, 9,0,1);
  adddec(8,"rm"     ,3, 6,0,0);
  adddec(8,"rn"     ,3, 3,0,0);
  adddec(8,"rd"     ,3, 0,0,0);

  dect[9].desc = "Table 3-10 Load and store, word or byte (5-bit immediate offset) instructions [1]";
  dect[9].nfunc = 2;

//ADDSYM(exec_strio);
  ADDSYM(dump_strio);
//dect[9].func[0].pexec = exec_strio;
  EXEC(9,0,exec_strio);
//dect[9].func[0].pdis  = dump_strio;
  dect[9].func[0].fmtinst = "STR<B#b><#c>     <rd>, [<rn>, #<imm5>]";

//ADDSYM(exec_ldrio);
  ADDSYM(dump_ldrio);
//dect[9].func[1].pexec = exec_ldrio;
  EXEC(9,1,exec_ldrio);
//dect[9].func[1].pdis  = dump_ldrio;
  dect[9].func[1].fmtinst = "LDR<B#b><#c>     <rd>, [<rn>, #<imm5>]";

  adddec(9,"B"     ,1,12,0,0);
  adddec(9,"L"     ,1,11,0,1);
  adddec(9,"imm5"  ,5, 6,0,0);
  adddec(9,"rn"    ,3, 3,0,0);
  adddec(9,"rd"    ,3, 0,0,0);

  dect[10].desc = "Table 3-11 Load and store halfword (5-bit immediate offset) instructions [1]";
  dect[10].nfunc = 2;

//ADDSYM(exec_strh);
//dect[10].func[0].pexec = exec_strh;
  EXEC(10,0,exec_strh);
  dect[10].func[0].fmtinst = "STRH<#c>     <rd>, [<rn>, #<imm5>]";

//ADDSYM(exec_ldrh);
//dect[10].func[1].pexec = exec_ldrh;
  EXEC(10,1,exec_ldrh);
  dect[10].func[1].fmtinst = "LDRH<#c>     <rd>, [<rn>, #<imm5>]";

  adddec(10,"L"     ,1,11,0,1);
  adddec(10,"imm5"  ,5, 6,0,0);
  adddec(10,"rn"    ,3, 3,0,0);
  adddec(10,"rd"    ,3, 0,0,0);

  dect[11].desc = "Table 3-12 Load from stack and store to stack instructions [1]";
  dect[11].nfunc = 2;

//ADDSYM(exec_strsp);
  ADDSYM(dump_strsp);
//dect[11].func[0].pexec = exec_strsp;
  EXEC(11,0,exec_strsp);
  dect[11].func[0].pdis = dump_strsp;
  dect[11].func[0].fmtinst = "STR<#c> <rd>, [sp, #<0>]";

//ADDSYM(exec_ldrsp);
  ADDSYM(dump_ldrsp);
//dect[11].func[1].pexec = exec_ldrsp;
  EXEC(11,1,exec_ldrsp);
  dect[11].func[1].pdis = dump_ldrsp;
  dect[11].func[1].fmtinst = "LDR<#c> <rd>, [sp, #<0>]";

  adddec(11,"L"    ,1,11,0,1);
  adddec(11,"rd"   ,3, 8,0,0);
  adddec(11,"imm8" ,8, 0,0,0);

  dect[12].desc = "Table 3-13 Add 8-bit immediate to SP or PC instructions [1]";
  dect[12].nfunc = 1;

//ADDSYM(exec_adr);
  ADDSYM(dump_adr);
//dect[12].func[0].pexec = exec_adr;
  EXEC(12,0,exec_adr);
  dect[12].func[0].pdis = dump_adr;
  dect[12].func[0].fmtinst = "ADR<#c>     <rd>, <2#%s>+<0> ; @<1>";

  adddec(12,"sp"   ,1,11,0,0);
  adddec(12,"rd"   ,3, 8,0,0);
  adddec(12,"imm8" ,8, 0,0,0);

// 3.2.1 Miscellaneous instructions
  ADDSYM(deco_misc);
  dect[13].desc  = "3.2.1 Miscellaneous instructions";
  dect[13].pfd   = deco_misc;
  dect[13].nfunc = 16;

//ADDSYM(exec_asp);
  ADDSYM(dump_asp);
//dect[13].func[0].pexec = exec_asp;
  EXEC(13,0,exec_asp);
  dect[13].func[0].pdis = dump_asp;
  dect[13].func[0].fmtinst = "dynamic";

//ADDSYM(exec_xxtx);
  ADDSYM(dump_xxtx);
//dect[13].func[1].pexec = exec_xxtx;
  EXEC(13,1,exec_xxtx);
  dect[13].func[1].pdis = dump_xxtx;
  dect[13].func[1].fmtinst = "<0#%s>XT<1#%s><#c> <rd>,<rm>";

//ADDSYM(exec_cbz);
  ADDSYM(dump_cbz);
//dect[13].func[2].pexec = exec_cbz;
  EXEC(13,2,exec_cbz);
  dect[13].func[2].pdis = dump_cbz;
  dect[13].func[2].fmtinst = "CBZ <rn>, <0>";

//ADDSYM(exec_cbnz);
  ADDSYM(dump_cbnz);
//dect[13].func[3].pexec = exec_cbnz;
  EXEC(13,3,exec_cbnz);
  dect[13].func[3].pdis = dump_cbnz;
  dect[13].func[3].fmtinst = "CBNZ <rn>, <0>";

//ADDSYM(exec_push);
  ADDSYM(dump_push);
//dect[13].func[4].pexec = exec_push;
  EXEC(13,4,exec_push);
  dect[13].func[4].pdis = dump_push;
  dect[13].func[4].fmtinst = "PUSH<#c>    <0#m>";

//ADDSYM(exec_pop);
  ADDSYM(dump_pop);
//dect[13].func[5].pexec = exec_pop;
  EXEC(13,5,exec_pop);
  dect[13].func[5].pdis = dump_pop;
  dect[13].func[5].fmtinst = "POP<#c>     <0#m>";

//ADDSYM(exec_cps);
  ADDSYM(dump_cps);
//dect[13].func[8].pexec = exec_cps;
  EXEC(13,8,exec_cps);
  dect[13].func[8].pdis = dump_cps;
  dect[13].func[8].fmtinst = "CPS<0#%s> <1#%s>";

//ADDSYM(exec_bkpt);
//dect[13].func[9].pexec = exec_bkpt;
  EXEC(13,9,exec_bkpt);
  dect[13].func[9].fmtinst = "BKPT <imm8>";

//ADDSYM(exec_rev);
  ADDSYM(dump_rev);
//dect[13].func[10].pexec = exec_rev;
  EXEC(13,10,exec_rev);
  dect[13].func[10].pdis = dump_rev;
  dect[13].func[10].fmtinst = "REV<0#%s><#c> <rn>,<rm>";

//ADDSYM(exec_it);
  ADDSYM(dump_it);
//dect[13].func[12].pexec = exec_it;
  EXEC(13,12,exec_it);
  dect[13].func[12].pdis = dump_it;
  dect[13].func[12].fmtinst = "IT<0#%s>      <cond#C>";

//ADDSYM(exec_nop);
  ADDSYM(dump_nop);
//dect[13].func[13].pexec = exec_nop;
  EXEC(13,13,exec_nop);
  dect[13].func[13].pdis = dump_nop;
  dect[13].func[13].fmtinst = "NOP      hint=<hint>";

  adddec(13,"sw"       , 4, 8,0,1);
  adddec(13,"cond"     , 4, 4,0,0);
  adddec(13,"mask"     , 4, 0,0,0);
  adddec(13,"imm8"     , 8, 0,0,0);
  adddec(13,"m"        , 1, 8,0,0);
  adddec(13,"imm7"     , 7, 0,0,0);
  adddec(13,"i"        , 1, 9,0,0);
  adddec(13,"imm5"     , 5, 3,0,0);
  adddec(13,"rn,rd,aif", 3, 0,0,0);
  adddec(13,"opc1"     , 1, 7,0,0);
  adddec(13,"opc2"     , 2, 6,0,0);
  adddec(13,"rm"       , 3, 3,0,0);
  adddec(13,"im"       , 1, 4,0,0);
  adddec(13,"_empty_"  , 1, 4,0,0);
  adddec(13,"hint"     , 4, 4,0,0);

  dect[14].desc = "load/store multiple";
  dect[14].nfunc = 2;

//ADDSYM(exec_stm);
//dect[14].func[0].pexec = exec_stm;
  EXEC(14,0,exec_stm);
  dect[14].func[0].fmtinst = "STM<#c>    <rn>!, <rlist#m>";

//ADDSYM(exec_ldm);
  ADDSYM(dump_ldm);
//dect[14].func[1].pexec = exec_ldm;
  EXEC(14,1,exec_ldm);
  dect[14].func[1].pdis = dump_ldm;
  dect[14].func[1].fmtinst = "LDM<#c>    <rn><0#!>, <rlist#m>";

  adddec(14,"L"    ,1,11,0,1);
  adddec(14,"rn"   ,3, 8,0,0);
  adddec(14,"rlist",8, 0,0,0);

  dect[15].desc = "conditional branch";
  dect[15].nfunc = 1;

//ADDSYM(exec_bcond);
  ADDSYM(dump_bcond);
//dect[15].func[0].pexec = exec_bcond;
  EXEC(15,0,exec_bcond);
  dect[15].func[0].pdis = dump_bcond;
  dect[15].func[0].fmtinst = "B<cond#C>   #<0>";

  adddec(15,"cond" ,4,8,0,0);
  adddec(15,"imm8" ,8,0,1,0);

  dect[16].desc = "undefine instruction";

  dect[17].desc = "service (system) call";
  dect[17].nfunc = 1;

//ADDSYM(exec_svc);
//dect[17].func[0].pexec = exec_svc;
  EXEC(17,0,exec_svc);
  dect[17].func[0].fmtinst = "SVC<#c> #<imm8>";

  adddec(17,"imm8" ,8,0,0,0);

  dect[18].desc = "unconditional branch";

  dect[18].nfunc = 1;

//ADDSYM(exec_b);
  ADDSYM(dump_b);
//dect[18].func[0].pexec = exec_b;
  EXEC(18,0,exec_b);
  dect[18].func[0].pdis = dump_b;
  dect[18].func[0].fmtinst = "B       <0>";

  adddec(18,"imm11",11,0,1,0);

  dect4[0].desc = "3.3.1 Data processing: immediate including bitfield and saturate";
  dect4[0].nfunc = 16;     

//ADDSYM(exec_dpind);
  ADDSYM(dump_dpind);
  for (i = 0; i < 8; i++)
  {
//  dect4[0].func[i].pexec = exec_dpind;
    EXEC4(0,i,exec_dpind);
    dect4[0].func[i].pdis = dump_dpind;
  }
//ADDSYM(exec_asind);
  ADDSYM(dump_asind);
//dect4[0].func[8].pexec = exec_asind;
  EXEC4(0,8,exec_asind);
  dect4[0].func[8].pdis = dump_asind;

//dect4[0].func[9].pexec = exec_asind;
  EXEC4(0,9,exec_asind);
  dect4[0].func[9].pdis = dump_asind;

//ADDSYM(exec_mvind);
  ADDSYM(dump_mvind);
//dect4[0].func[10].pexec = exec_mvind;
  EXEC4(0,10,exec_mvind);
  dect4[0].func[10].pdis = dump_mvind;

//dect4[0].func[11].pexec = exec_mvind;
  EXEC4(0,11,exec_mvind);
  dect4[0].func[11].pdis = dump_mvind;

//ADDSYM(exec_bfind);
  ADDSYM(dump_bfind);
//dect4[0].func[12].pexec = exec_bfind;
  EXEC4(0,12,exec_bfind);
  dect4[0].func[12].pdis = dump_bfind;

//ADDSYM(exec_res);
//dect4[0].func[13].pexec = exec_res;
  EXEC4(0,13,exec_res);

//dect4[0].func[14].pexec = exec_bfind;
  EXEC4(0,14,exec_bfind);
  dect4[0].func[14].pdis = dump_bfind;

//dect4[0].func[15].pexec = exec_res;
  EXEC4(0,15,exec_res);
  dect4[0].func[15].pdis = dump_res;

// Data processing instructions with modified 12-bit immediate
//ADDSYM(exec_dptstand);
  ADDSYM(dump_dptstand);
//dectdp[0].func[0].pexec = exec_dptstand;
  EXECDP(0,0,exec_dptstand);
  dectdp[0].func[0].pdis = dump_dptstand;
  dectdp[0].func[0].fmtinst = "dynamic";

//ADDSYM(exec_dpbic);
  ADDSYM(dump_dpbic);
//dectdp[1].func[0].pexec = exec_dpbic;
  EXECDP(1,0,exec_dpbic);
  dectdp[1].func[0].pdis = dump_dpbic;
  dectdp[1].func[0].fmtinst = "BIC<S#S>  <rd>, <rn>, #<0>";

//ADDSYM(exec_dporrmov);
  ADDSYM(dump_dporrmov);
//dectdp[2].func[0].pexec = exec_dporrmov;
  EXECDP(2,0,exec_dporrmov);
  dectdp[2].func[0].pdis = dump_dporrmov;
  dectdp[2].func[0].fmtinst = "dynamic";

//ADDSYM(exec_dpornmvn);
  ADDSYM(dump_dpornmvn);
//dectdp[3].func[0].pexec = exec_dpornmvn;
  EXECDP(3,0,exec_dpornmvn);
  dectdp[3].func[0].pdis = dump_dpornmvn;
  dectdp[3].func[0].fmtinst = "dynamic";

//ADDSYM(exec_dpeor);
  ADDSYM(dump_dpeor);
//dectdp[4].func[0].pexec = exec_dpeor;
  EXECDP(4,0,exec_dpeor);
  dectdp[4].func[0].pdis = dump_dpeor; 
  dectdp[4].func[0].fmtinst = "EOR<#c> <rd>,<rn>,#<0>";

//ADDSYM(exec_dpteq);
//dectdp[4].func[1].pexec = exec_dpteq;
  EXECDP(4,1,exec_dpteq);
  dectdp[4].func[1].pdis = dump_dpeor; 
  dectdp[4].func[1].fmtinst = "TEQ<#c> <rn>,#<0>";

//dectdp[5].func[0].pexec = exec_TBD;
  EXECDP(5,0,exec_TBD);

//dectdp[6].func[0].pexec = exec_TBD;
  EXECDP(6,0,exec_TBD);

//dectdp[7].func[0].pexec = exec_TBD;
  EXECDP(7,0,exec_TBD);

//ADDSYM(exec_dpadd);
  ADDSYM(dump_dpadd);
//dectdp[8].func[0].pexec = exec_dpadd;
  EXECDP(8,0,exec_dpadd);
  dectdp[8].func[0].pdis = dump_dpadd;
  dectdp[8].func[0].fmtinst = "ADD<S#S><#c>     <rd>,<rn>, #<0>";

//ADDSYM(exec_dpcmn);
//dectdp[8].func[1].pexec = exec_dpcmn;
  EXECDP(8,1,exec_dpcmn);
  dectdp[8].func[1].pdis = dump_dpadd;
  dectdp[8].func[1].fmtinst = "CMN<#c>     <rn>, #<0>";

//dectdp[9].func[0].pexec = exec_TBD;
  EXECDP(9,0,exec_TBD);

//ADDSYM(exec_dpadc);
//dectdp[10].func[0].pexec = exec_dpadc;
  EXECDP(10,0,exec_dpadc);
  dectdp[10].func[0].pdis = dump_dpadd;
  dectdp[10].func[0].fmtinst = "ADC<S#S><#c>     <rd>, #<0>";

//ADDSYM(exec_sbci);
//dectdp[11].func[0].pexec = exec_sbci;
  EXECDP(11,0,exec_sbci);
  dectdp[11].func[0].pdis = dump_dpadd;
  dectdp[11].func[0].fmtinst = "SBC<#c> <rd>,<rn>,#<0>";

//dectdp[12].func[0].pexec = exec_TBD;
  EXECDP(12,0,exec_TBD);

//ADDSYM(exec_dpsubcmp);
  ADDSYM(dump_dpsubcmp);
//dectdp[13].func[0].pexec = exec_dpsubcmp;
  EXECDP(13,0,exec_dpsubcmp);
  dectdp[13].func[0].pdis = dump_dpsubcmp;
  dectdp[13].func[0].fmtinst = "dynamic";

//ADDSYM(exec_dprsb);
  ADDSYM(dump_dprsb);
//dectdp[14].func[0].pexec = exec_dprsb;
  EXECDP(14,0,exec_dprsb);
  dectdp[14].func[0].pdis = dump_dprsb;
  dectdp[14].func[0].fmtinst = "RSB<S#S><#c>.W <rd>, <rn>, #<0>";

//dectdp[15].func[0].pexec = exec_TBD;
  EXECDP(15,0,exec_TBD);

// Data processing instructions with plain 12-bit immediate

//ADDSYM(exec_addadrt3);
  ADDSYM(dump_addadrt3);
//dectas[0].func[0].pexec = exec_addadrt3;
  EXECAS(0,0,exec_addadrt3);
  dectas[0].func[0].pdis = dump_addadrt3;
  dectas[0].func[0].fmtinst = "dynamic";
//dectas[0].func[0].fmtinst = "ADR<#c>.W    <rd>, <0> ; <1>";

//dectas[1].func[0].pexec = exec_TBD;
  EXECAS(1,0,exec_TBD);
//dectas[2].func[0].pexec = exec_TBD;
  EXECAS(2,0,exec_TBD);
//dectas[3].func[0].pexec = exec_TBD;
  EXECAS(3,0,exec_TBD);
//dectas[4].func[0].pexec = exec_TBD;
  EXECAS(4,0,exec_TBD);
//dectas[5].func[0].pexec = exec_TBD;
  EXECAS(5,0,exec_TBD);

//ADDSYM(exec_subimm);
  ADDSYM(dump_subimm);
//dectas[6].func[0].pexec = exec_subimm;
  EXECAS(6,0,exec_subimm);
  dectas[6].func[0].pdis = dump_subimm;
  dectas[6].func[0].fmtinst = "SUBW<#c>    <rd>, <rn>, #<0>";

//dectas[7].func[0].pexec = exec_TBD;
  EXECAS(7,0,exec_TBD);

// Data processing instructions with plain 16-bit immediate
//
//ADDSYM(exec_dpmovw);
  ADDSYM(dump_dpmovw);
//dectmv[0].func[0].pexec = exec_dpmovw;
  EXECMV(0,0,exec_dpmovw);
  dectmv[0].func[0].pdis = dump_dpmovw;
  dectmv[0].func[0].fmtinst = "MOVW<#c>    <rd>, #<0>";

//ADDSYM(exec_dpmovt);
  ADDSYM(dump_dpmovt);
//dectmv[1].func[0].pexec = exec_dpmovt;
  EXECMV(1,0,exec_dpmovt);
  dectmv[1].func[0].pdis = dump_dpmovt;
  dectmv[1].func[0].fmtinst = "MOVT<#c>    <rd>, #<0>";

// Data processing instructions, bitfield and saturate

//ADDSYM(exec_bfc);
  ADDSYM(dump_ubfx);
//dectbf[0].func[0].pexec = exec_bfc;
  EXECBF(0,0,exec_bfc);
  dectbf[0].func[0].pdis  = dump_ubfx;
  dectbf[0].func[0].fmtinst = "BFC<#c> <rd>, #<0>, #<1>";

//ADDSYM(exec_bfi);
//dectbf[1].func[0].pexec = exec_bfi;
  EXECBF(1,0,exec_bfi);
  dectbf[1].func[0].pdis  = dump_ubfx;
  dectbf[1].func[0].fmtinst = "BFI<#c> <rd>, <rn>, #<0>, #<1>";

//ADDSYM(exec_sbfx);
//dectbf[2].func[0].pexec = exec_sbfx;
  EXECBF(2,0,exec_sbfx);
  dectbf[2].func[0].pdis  = dump_ubfx;
  dectbf[2].func[0].fmtinst = "SBFX<#c> <rd>, <rn>, #<0>, #<1>";

//ADDSYM(exec_ssat);
  ADDSYM(dump_ssat);
//dectbf[3].func[0].pexec = exec_ssat;
  EXECBF(3,0,exec_ssat);
  dectbf[3].func[0].pdis  = dump_ssat;
  dectbf[3].func[0].fmtinst = "SSAT<#c> <rd>, #<0>, <rn> <1#H>";

//dectbf[4].func[0].pexec = exec_ssat;
  EXECBF(4,0,exec_ssat);
  dectbf[4].func[0].pdis  = dump_ssat;
  dectbf[4].func[0].fmtinst = "SSAT<#c> <rd>, #<0>, <rn> <1#H>";

//ADDSYM(exec_ssat16);
//dectbf[5].func[0].pexec = exec_ssat16;
  EXECBF(5,0,exec_ssat16);
  dectbf[5].func[0].pdis  = dump_ssat;
  dectbf[5].func[0].fmtinst = "SSAT<#c> <rd>, #<0>, <rn> ";

//ADDSYM(exec_ubfx);
//dectbf[6].func[0].pexec = exec_ubfx;
  EXECBF(6,0,exec_ubfx);
  dectbf[6].func[0].pdis  = dump_ubfx;
  dectbf[6].func[0].fmtinst = "UBFX<#c> <rd>, <rn>, #<0>, #<1>";

//ADDSYM(exec_usat);
//dectbf[7].func[0].pexec = exec_usat;
  EXECBF(7,0,exec_usat);
  dectbf[7].func[0].pdis  = dump_ssat;
  dectbf[7].func[0].fmtinst = "USAT<#c> <rd>, #<0>, <rn> <1#H>";

//dectbf[8].func[0].pexec = exec_usat;
  EXECBF(8,0,exec_usat);
  dectbf[8].func[0].pdis  = dump_ssat;
  dectbf[8].func[0].fmtinst = "USAT<#c> <rd>, #<0>, <rn> <1#H>";

  adddec4(0,"i"            ,1,26,0,0);
  adddec4(0,"sw"           ,2,24,0,1);
  adddec4(0,"sw1"          ,1,22,0,1);
  adddec4(0,"S,sw2"        ,1,20,0,1);
  adddec4(0,"op0"          ,4,21,0,0);
  adddec4(0,"op1"          ,1,23,0,0);
  adddec4(0,"op2"          ,2,20,0,0);
  adddec4(0,"rn,imm4"      ,4,16,0,0);
  adddec4(0,"imm3"         ,3,12,0,0);
  adddec4(0,"rd"           ,4, 8,0,0);
  adddec4(0,"imm8"         ,8, 0,0,0);
  adddec4(0,"imm5"         ,5, 0,0,0);
  adddec4(0,"imm2"         ,2, 6,0,0);
  adddec4(0,"sbz"          ,1, 5,0,0);
  adddec4(0,"type"         ,2, 4,0,0);
  adddec4(0,"sh"           ,1,21,0,0);
  adddec4(0,"sat_imm"      ,4, 0,0,0);

// 
// 3.3.2 Data processing instructions, non-immediate
// Figure 3-5 Data processing instructions, non-immediate
//
  ADDSYM(deco_nimm);
  dect4[1].desc = "3.3.2 Data processing instructions, non-immediate";
  dect4[1].nfunc = 8;
  dect4[1].pfd   = deco_nimm;

//
// Data processing instructions with constant shift
// Table 3-25 Data processing instructions with constant shift
//
//ADDSYM(exec_dpcsind);
  ADDSYM(dump_dpcsind);
//dect4[1].func[0].pexec = exec_dpcsind;
  EXEC4(1,0,exec_dpcsind);
  dect4[1].func[0].pdis = dump_dpcsind;
  dect4[1].func[0].desc = "Table 3-25 Data processing instructions with constant shift";

///DDSYM(exec_and3);
  ADDSYM(dump_and3);
//dectdpcs[0].func[0].pexec = exec_and3;
  EXECDPCS(0,0,exec_and3);
  dectdpcs[0].func[0].pdis = dump_and3;
  dectdpcs[0].func[0].fmtinst = "AND<S#S><#c>.W <rd>, <rn>, <rm>, <1#h> #<0#d>";

//dectdpcs[0].func[1].pexec = exec_and3;
  EXECDPCS(0,1,exec_and3);
  dectdpcs[0].func[1].pdis = dump_and3;
  dectdpcs[0].func[1].fmtinst = "TST<#c>.W <rn>, <rm>, <1#r> #<0>";

//ADDSYM(exec_bic3);
  ADDSYM(dump_bic3);
//dectdpcs[1].func[0].pexec = exec_bic3;
  EXECDPCS(1,0,exec_bic3);
  dectdpcs[1].func[0].pdis = dump_bic3;
  dectdpcs[1].func[0].fmtinst = "BIC<S#S><#c>.W <rd>, <rn>, <rm>, <1#h> #<0#d>";

//ADDSYM(exec_movishift);
  ADDSYM(dump_movishift);
//dectdpcs[2].func[0].pexec = exec_movishift;
  EXECDPCS(2,0,exec_movishift);
  dectdpcs[2].func[0].pdis = dump_movishift;
  dectdpcs[2].func[0].fmtinst = 0; // indirect

//ADDSYM(exec_orr3);
  ADDSYM(dump_orr3);
//dectdpcs[2].func[1].pexec = exec_orr3;
  EXECDPCS(2,1,exec_orr3);
  dectdpcs[2].func[1].pdis = dump_orr3;
  dectdpcs[2].func[1].fmtinst = "ORR<S#S><#c>.W <rd>, <rn>, <rm>, <1#h> #<0#d>";

//ADDSYM(exec_orn3);
//dectdpcs[3].func[0].pexec = exec_orn3;
  EXECDPCS(3,0,exec_orn3);
  dectdpcs[3].func[0].pdis = dump_orr3;
  dectdpcs[3].func[0].fmtinst = "ORN<S#S><#c>.W <rd>,<rm>, <1#h> #<0#d>";

//ADDSYM(exec_mvn3);
//dectdpcs[3].func[1].pexec = exec_mvn3;
  EXECDPCS(3,1,exec_mvn3);
  dectdpcs[3].func[1].pdis = dump_orr3;
  dectdpcs[3].func[1].fmtinst = "MVN<S#S><#c>.W <rd>,<rm>, <1#h> #<0#d>";

//ADDSYM(exec_eort2);
//dectdpcs[4].func[0].pexec = exec_eort2;
  EXECDPCS(4,0,exec_eort2);
  dectdpcs[4].func[0].pdis = dump_orr3;
  dectdpcs[4].func[0].fmtinst = "EOR<S#S><#c>.W <rd>, <rn>, <rm>, <1#h> #<0#d>";

//ADDSYM(exec_teqt2);
//dectdpcs[4].func[1].pexec = exec_teqt2;
  EXECDPCS(4,1,exec_teqt2);
  dectdpcs[4].func[1].pdis = dump_orr3;
  dectdpcs[4].func[1].fmtinst = "TEQ<#c>.W <rn>, <rm>, <1#h> #<0#d>";

//ADDSYM(exec_pkhtb);
//dectdpcs[6].func[0].pexec = exec_pkhtb;
  EXECDPCS(6,0,exec_pkhtb);
  dectdpcs[6].func[0].pdis = dump_orr3;
  dectdpcs[6].func[0].fmtinst = "PKHTB<#c> <rd>,<rn>,<rm>, <1#h> #<0#d>";

//ADDSYM(exec_pkhbt);
//dectdpcs[6].func[1].pexec = exec_pkhbt;
  EXECDPCS(6,1,exec_pkhbt);
  dectdpcs[6].func[1].pdis = dump_orr3;
  dectdpcs[6].func[1].fmtinst = "PKHBT<#c> <rd>,<rn>,<rm>, <1#h> #<0#d>";

//ADDSYM(exec_addt2);
//dectdpcs[8].func[0].pexec = exec_addt2;
  EXECDPCS(8,0,exec_addt2);
  dectdpcs[8].func[0].pdis = dump_orr3;
  dectdpcs[8].func[0].fmtinst = "ADD<S#S><#c>.W <rd>, <rn>, <rm>, <1#h> #<0#d>";

//ADDSYM(exec_cmnt2);
//dectdpcs[8].func[1].pexec = exec_cmnt2;
  EXECDPCS(8,1,exec_cmnt2);
  dectdpcs[8].func[1].pdis = dump_orr3;
  dectdpcs[8].func[1].fmtinst = "CMN<#c>.W <rn>, <rm>, <1#h> #<0#d>";

//ADDSYM(exec_adcrt2);
//dectdpcs[10].func[0].pexec = exec_adcrt2;
  EXECDPCS(10,0,exec_adcrt2);
  dectdpcs[10].func[0].pdis = dump_orr3;
  dectdpcs[10].func[0].fmtinst = "ADC<S#S><#c>.W <rd>, <rn>, <rm>, <1#h> #<0#d>";

//ADDSYM(exec_sbcrt2);
//dectdpcs[11].func[0].pexec = exec_sbcrt2;
  EXECDPCS(11,0,exec_sbcrt2);
  dectdpcs[11].func[0].pdis = dump_orr3;
  dectdpcs[11].func[0].fmtinst = "SBC<S#S><#c>.W <rd>, <rn>, <rm>, <1#h> #<0#d>";

//ADDSYM(exec_subrt2);
//dectdpcs[13].func[0].pexec = exec_subrt2;
  EXECDPCS(13,0,exec_subrt2);
  dectdpcs[13].func[0].pdis = dump_orr3;
  dectdpcs[13].func[0].fmtinst = "SUB<S#S><#c>.W <rd>, <rn>, <rm>, <1#h> #<0#d>";

//ADDSYM(exec_cmpt3);
//dectdpcs[13].func[1].pexec = exec_cmpt3;
  EXECDPCS(13,1,exec_cmpt3);
  dectdpcs[13].func[1].pdis = dump_orr3;
  dectdpcs[13].func[1].fmtinst = "CMP<#c>.W <rn>, <rm>, <1#h> #<0#d>";

//ADDSYM(exec_rsbt1);
//dectdpcs[14].func[0].pexec = exec_rsbt1;
  EXECDPCS(14,0,exec_rsbt1);
  dectdpcs[14].func[0].pdis = dump_orr3;
  dectdpcs[14].func[0].fmtinst = "RSB<S#S><#c>.W <rd>, <rn>, <rm>, <1#h> #<0#d>";

// Move, and immediate shift instructions
//ADDSYM(exec_movt3);
//dectmis[0].func[0].pexec = exec_movt3;
  EXECMIS(0,0,exec_movt3);
  dectmis[0].func[0].fmtinst = "MOV<S#S><#c>.W <rd>,<rm>";

//ADDSYM(exec_lslit2);
//dectmis[1].func[0].pexec = exec_lslit2;
  EXECMIS(1,0,exec_lslit2);
  dectmis[1].func[0].fmtinst = "LSL<S#S><#c>.W <rd>,<rm>,#<0>";

//ADDSYM(exec_lsrit2);
//dectmis[2].func[0].pexec = exec_lsrit2;
  EXECMIS(2,0,exec_lsrit2);
  dectmis[2].func[0].fmtinst = "LSR<S#S><#c>.W <rd>,<rm>,#<0>";

//ADDSYM(exec_asrit2);
//dectmis[3].func[0].pexec = exec_asrit2;
  EXECMIS(3,0,exec_asrit2);
  dectmis[3].func[0].fmtinst = "ASR<S#S><#c>.W <rd>,<rm>,#<0>";

//ADDSYM(exec_rorit1);
//dectmis[4].func[0].pexec = exec_rorit1;
  EXECMIS(4,0,exec_rorit1);
  dectmis[4].func[0].fmtinst = "ROR<S#S><#c>.W <rd>,<rm>,#<0>";

//ADDSYM(exec_rrxit1);
//dectmis[5].func[0].pexec = exec_rrxit1;
  EXECMIS(5,0,exec_rrxit1);
  dectmis[5].func[0].fmtinst = "RRX<S#S><#c>.W <rd>,<rm>";

//
// Register-controlled shift instructions
// Table 3-27 Register-controlled shift instructions
//
//ADDSYM(exec_rcshind);
  ADDSYM(dump_rcshind);
//dect4[1].func[1].pexec = exec_rcshind;
  EXEC4(1,1,exec_rcshind);
  dect4[1].func[1].pdis  = dump_rcshind;
  dect4[1].func[1].desc  = "Table 3-27 Register-controlled shift instructions";

//ADDSYM(exec_lslrc);
//dectrcsh[0].func[0].pexec = exec_lslrc;
  EXECRCSH(0,0,exec_lslrc);
  dectrcsh[0].func[0].fmtinst = "LSL<S#S><#c>.W <rd>,<rn>,<rm>";

//ADDSYM(exec_lsrrc);
//dectrcsh[1].func[0].pexec = exec_lsrrc;
  EXECRCSH(1,0,exec_lsrrc);
  dectrcsh[1].func[0].fmtinst = "LSR<S#S><#c>.W <rd>,<rn>,<rm>";

//ADDSYM(exec_asrrc);
//dectrcsh[2].func[0].pexec = exec_asrrc;
  EXECRCSH(2,0,exec_asrrc);
  dectrcsh[2].func[0].fmtinst = "ASR<S#S><#c>.W <rd>,<rn>,<rm>";

//dectrcsh[3].func[0].pexec = exec_TBD;
  EXECRCSH(3,0,exec_TBD);
  dectrcsh[3].func[0].fmtinst = "ROR<S#S><#c>.W <rd>,<rn>,<rm>";

//
// Signed and unsigned extend instructions with optional addition
// Table 3-28 Signed and unsigned extend instructions with optional addition
//

//ADDSYM(exec_sozind);
  ADDSYM(dump_sozind);
//dect4[1].func[2].pexec = exec_sozind;
  EXEC4(1,2,exec_sozind);
  dect4[1].func[2].pdis  = dump_sozind;
  dect4[1].func[2].desc  = "Table 3-28 Signed and unsigned extend instructions with optional addition";

//ADDSYM(exec_sxtah);
//dectsoz[2].func[0].pexec = exec_sxtah;
  EXECSOZ(2,0,exec_sxtah);
  dectsoz[2].func[0].fmtinst = "SXTAH<#c> <rd>,<rn>,<rm>,#rot";

//ADDSYM(exec_sxth);
//dectsoz[3].func[0].pexec = exec_sxth;
  EXECSOZ(3,0,exec_sxth);
  dectsoz[3].func[0].fmtinst = "SXTB<#c> <rd>,<rm>,#rot";

//ADDSYM(exec_sxth);
//dectsoz[5].func[0].pexec = exec_sxth;
  EXECSOZ(5,0,exec_sxth);
  dectsoz[5].func[0].fmtinst = "SXTH<#c> <rd>,<rm>,#rot";

//ADDSYM(exec_uxtab);
//dectsoz[6].func[0].pexec = exec_uxtab;
  EXECSOZ(6,0,exec_uxtab);
  dectsoz[6].func[0].fmtinst = "UXTAB<#c> <rd>,<rn>,<rm>,#rot";

//ADDSYM(exec_uxtah);
//dectsoz[8].func[0].pexec = exec_uxtah;
  EXECSOZ(8,0,exec_uxtah);
  dectsoz[8].func[0].fmtinst = "UXTAH<#c> <rd>,<rn>,<rm>,#rot";

//ADDSYM(exec_uxtb);
//dectsoz[9].func[0].pexec = exec_uxtb;
  EXECSOZ(9,0,exec_uxtb);
  dectsoz[9].func[0].fmtinst = "UXTB<#c> <rd>,<rm>,#rot";

//ADDSYM(exec_uxth);
//dectsoz[11].func[0].pexec = exec_uxth;
  EXECSOZ(11,0,exec_uxth);
  dectsoz[11].func[0].fmtinst = "UXTH<#c> <rd>,<rm>,#rot";
//
// SIMD add and subtract
// Table 3-29 SIMD instructions
// 
//ADDSYM(exec_simd);
  ADDSYM(dump_simd);
//dect4[1].func[3].pexec = exec_simd;
  EXEC4(1,3,exec_simd);
  dect4[1].func[3].pdis  = dump_simd;
  dect4[1].func[3].desc  = "Table 3-29 SIMD instructions";

//ADDSYM(exec_uadd8);
//dectsimd[19].func[0].pexec = exec_uadd8;
  EXECSIMD(19,0,exec_uadd8);
  dectsimd[19].func[0].fmtinst = "UADD8<#c> <rd>,<rn>,<rm>";

//
// Other three-register data processing instructions
// Table 3-31 Other three-register data processing instructions
//

//ADDSYM(exec_otherind);
  ADDSYM(dump_otherind);
//dect4[1].func[4].pexec = exec_otherind;
  EXEC4(1,4,exec_otherind);
  dect4[1].func[4].pdis  = dump_otherind;
  dect4[1].func[4].desc  = "Table 3-31 Other three-register data processing instructions";

//ADDSYM(exec_clz);
//dectother[0].func[0].pexec = exec_clz;
  EXECOTHER(0,0,exec_clz);
  dectother[0].func[0].fmtinst = "CLZ<#c> <rd>,<rm>";

//ADDSYM(exec_rbit);
//dectother[5].func[0].pexec = exec_rbit;
  EXECOTHER(5,0,exec_rbit);
  dectother[5].func[0].fmtinst = "RBIT<#c> <rd>,<rm>";

//ADDSYM(exec_sel);
//dectother[9].func[0].pexec = exec_sel;
  EXECOTHER(9,0,exec_sel);
  dectother[9].func[0].fmtinst = "SEL<#c> <rd>,<rn>,<rm>";
//
// reserved 
// dect4[1].func[5] = ...
//
// 32-bit multiplies and sum of absolute differences, with or without accumulate
// Table 3-32 Other two-register data processing instructions
//
//ADDSYM(exec_32mdind);
  ADDSYM(dump_32mdind);
//dect4[1].func[6].pexec = exec_32mdind;
  EXEC4(1,6,exec_32mdind);
  dect4[1].func[6].pdis  = dump_32mdind;
  dect4[1].func[6].desc  = "Table 3-32 Other two-register data processing instructions";

//ADDSYM(exec_mla);
//dect32md[0].func[0].pexec = exec_mla;
  EXEC32MD(0,0,exec_mla);
  dect32md[0].func[0].fmtinst = "MLA<#c> <rd>,<rn>,<rm>,<racc>";

//ADDSYM(exec_mul);
//dect32md[1].func[0].pexec = exec_mul;
  EXEC32MD(1,0,exec_mul);
  dect32md[1].func[0].fmtinst = "MUL<#c> <rd>,<rn>,<rm>";

//ADDSYM(exec_mls);
//dect32md[2].func[0].pexec = exec_mls;
  EXEC32MD(2,0,exec_mls);
  dect32md[2].func[0].fmtinst = "MLS<#c> <rd>,<rn>,<rm>,<racc>";

//ADDSYM(exec_smlaxx);
//dect32md[3].func[0].pexec = exec_smlaxx;
  EXEC32MD(3,0,exec_smlaxx);
  dect32md[3].func[0].fmtinst = "SMLA<N><M><#c> <rd>,<rn>,<rm>,<ra>";

//ADDSYM(exec_smulxx);
//dect32md[4].func[0].pexec = exec_smulxx;
  EXEC32MD(4,0,exec_smulxx);
  dect32md[4].func[0].fmtinst = "SMUL<0#%s><1#%s><#c> <rd>,<rn>,<rm>";
//
// 64-bit multiply, multiply-accumulate, and divide instructions
// Table 3-33 Other two-register data processing instructions
//
//ADDSYM(exec_64mdind);
  ADDSYM(dump_64mdind);
//dect4[1].func[7].pexec = exec_64mdind;
  EXEC4(1,7,exec_64mdind);
  dect4[1].func[7].pdis  = dump_64mdind;
  dect4[1].func[7].desc  = "Table 3-33 Other two-register data processing instructions";

//ADDSYM(exec_smull);
//dect64md[0].func[0].pexec = exec_smull;
  EXEC64MD(0,0,exec_smull);
  dect64md[0].func[0].fmtinst = "SMULL<#c> <rdl>,<rdh>,<rn>,<rm>";

//ADDSYM(exec_sdiv);
//dect64md[1].func[0].pexec = exec_sdiv;
  EXEC64MD(1,0,exec_sdiv);
  dect64md[1].func[0].fmtinst = "SDIV<#c> <rd>,<rn>,<rm>";

//ADDSYM(exec_umull);
//dect64md[2].func[0].pexec = exec_umull;
  EXEC64MD(2,0,exec_umull);
  dect64md[2].func[0].fmtinst = "UMULL<#c> <rdl>,<rdh>,<rn>,<rm>";

//ADDSYM(exec_udiv);
//dect64md[3].func[0].pexec = exec_udiv;
  EXEC64MD(3,0,exec_udiv);
  dect64md[3].func[0].fmtinst = "UDIV<#c> <rd>,<rn>,<rm>";

//ADDSYM(exec_umlal);
//dect64md[8].func[0].pexec = exec_umlal;
  EXEC64MD(8,0,exec_umlal);
  dect64md[8].func[0].fmtinst = "UMLAL<#c> <rdl>,<rdh>,<rn>,<rm>";

  adddec4(1,"sw1"           ,1,28,0,1);
  adddec4(1,"sw2"           ,2,23,0,0);
  adddec4(1,"op.1"          ,4,21,0,1);
  adddec4(1,"op.2"          ,2,21,0,0);
  adddec4(1,"op.3"          ,3,20,0,0);
  adddec4(1,"S"             ,1,20,0,0);
  adddec4(1,"rn"            ,4,16,0,0);
  adddec4(1,"sbz"           ,1,15,0,0);
  adddec4(1,"imm3"          ,3,12,0,0);
  adddec4(1,"ra,racc,rdl"   ,4,12,0,0);
  adddec4(1,"rd,op2,rdh"    ,4, 8,0,0);
  adddec4(1,"imm2,gen,op2_" ,4, 4,0,0);
  adddec4(1,"rm"            ,4, 0,0,0);
  adddec4(1,"sw3"           ,1, 7,0,0);
  adddec4(1,"i"             ,1,26,0,0);
  adddec4(1,"imm8"          ,8, 0,0,0);
  adddec4(1,"rot"           ,2, 4,0,0);
  adddec4(1,"N"             ,1, 5,0,0);
  adddec4(1,"M"             ,1, 4,0,0);

// deco_ls rt 0 rn 0 L 1 sw 0 S 15

  ADDSYM(deco_ls);
  dect4[2].desc  = "3.3.3 Load and store single data item, and memory hints";
  dect4[2].nfunc = 17;
  dect4[2].pfd   = deco_ls;
// entry  0.. 6 store
// entry 10..16 load
// entry  7..9  hint

//dect4[2].func[0].pexec = exec_TBD; 
  EXEC4(2,0,exec_TBD);

//ADDSYM(exec_stri);
//dect4[2].func[1].pexec = exec_stri;
  EXEC4(2,1,exec_stri);
  dect4[2].func[1].fmtinst = "STR<size#B><#c>.W <rt>, [<rn>, #<imm12>]";

//ADDSYM(exec_strt4);
//dect4[2].func[2].pexec = exec_strt4;
  EXEC4(2,2,exec_strt4);
  dect4[2].func[2].fmtinst = "STR<size#B><#c> <rt>,[<rn>],#<imm8>";

//dect4[2].func[3].pexec = exec_TBD;
  EXEC4(2,3,exec_TBD);

//dect4[2].func[4].pexec = exec_strt4;
  EXEC4(2,4,exec_strt4);
  dect4[2].func[4].fmtinst = "STR<size#B><#c> <rt>,[<rn>],#<imm8>";

//dect4[2].func[5].pexec = exec_strt4;
  EXEC4(2,5,exec_strt4);
  dect4[2].func[5].fmtinst = "STR<size#B><#c> <rt>,[<rn>,#<imm8>]!";

//ADDSYM(exec_strt2);
  ADDSYM(dump_strt2);
//dect4[2].func[6].pexec = exec_strt2;
  EXEC4(2,6,exec_strt2);
  dect4[2].func[6].pdis  = dump_strt2;
  dect4[2].func[6].fmtinst = "STR<size#B><#c>.W <rt>,[<rn>,<rm>{,LSL #<shift>}]";

// hint
//ADDSYM(exec_pld);
//dect4[2].func[7].pexec = exec_pld;
  EXEC4(2,7,exec_pld);
  dect4[2].func[7].fmtinst = "PLD";

//ADDSYM(exec_pli);
//dect4[2].func[8].pexec = exec_pli;
  EXEC4(2,8,exec_pli);
  dect4[2].func[8].fmtinst = "PLI";

//dect4[2].func[9].pexec = exec_TBD;
  EXEC4(2,9,exec_TBD);

//ADDSYM(exec_ldrit2);
//dect4[2].func[10].pexec = exec_ldrit2;
  EXEC4(2,10,exec_ldrit2);
  dect4[2].func[10].fmtinst = "LDR<S#e><size#B><#c>.W <rt>, [<rn>, #<imm12>]";

//dect4[2].func[11].pexec = exec_ldrit2;
  EXEC4(2,11,exec_ldrit2);
  dect4[2].func[11].fmtinst = "LDR<S#e><size#B><#c>.W <rt>, [<rn>, #<imm12>]";

//ADDSYM(exec_ldrit3);
//dect4[2].func[12].pexec = exec_ldrit3;
  EXEC4(2,12,exec_ldrit3);
  dect4[2].func[12].fmtinst = "LDR<S#e><size#B><#c> <rt>, [<rn>, +-#<imm8>]";

//dect4[2].func[13].pexec = exec_TBD;
  EXEC4(2,13,exec_TBD);

//ADDSYM(exec_ldrt3);
//dect4[2].func[14].pexec = exec_ldrt3;
  EXEC4(2,14,exec_ldrt3);
  dect4[2].func[14].fmtinst = "LDR<size#B><#c> <rt>,[<rn>],#<imm8>";

//dect4[2].func[15].pexec = exec_ldrt3;
  EXEC4(2,15,exec_ldrt3);
  dect4[2].func[15].fmtinst = "LDR<size#B><#c> <rt>,[<rn>],#<imm8>";

//ADDSYM(exec_ldrt2);
//dect4[2].func[16].pexec = exec_ldrt2;
  EXEC4(2,16,exec_ldrt2);
  dect4[2].func[16].fmtinst = "LDR<size#B><#c>.W <rt>,[<rn>,<rm>{,LSL #<shift>}]";

  adddec4(2,"S"    , 1,24,0,0);
  adddec4(2,"U"    , 1,23,0,0);
  adddec4(2,"size" , 2,21,0,0);
  adddec4(2,"L"    , 1,20,0,0);
  adddec4(2,"rn"   , 4,16,0,0);
  adddec4(2,"rt"   , 4,12,0,0);
  adddec4(2,"imm12",12, 0,0,0);
  adddec4(2,"imm8" , 8, 0,0,0);
  adddec4(2,"sw"   , 4, 8,0,0);
  adddec4(2,"shift", 2, 4,0,0);
  adddec4(2,"rm"   , 4, 0,0,0);
  adddec4(2,"P_"   , 1,10,0,1);
  adddec4(2,"U_"   , 1, 9,0,1);
  adddec4(2,"W_"   , 1, 8,0,0);

  ADDSYM(deco_lsdet);
  dect4[3].desc  = "3.3.4 Load/store double and exclusive, and table branch";
  dect4[3].nfunc = 8;
  dect4[3].pfd   = deco_lsdet;

//ADDSYM(exec_strd);
//dect4[3].func[0].pexec = exec_strd;
  EXEC4(3,0,exec_strd);
  dect4[3].func[0].fmtinst = "STRD<#c> <rt>, <rt2>, [<rn>, #+/-<imm8>]<W#!>";

//ADDSYM(exec_strex);
//dect4[3].func[1].pexec = exec_strex;
  EXEC4(3,1,exec_strex);
  dect4[3].func[1].fmtinst = "STREX<#c> <rd>,<rt>,[<rn>{,#<imm>}]";

//ADDSYM(exec_lsetbind);
  ADDSYM(dump_lsetbind);
//dect4[3].func[2].pexec = exec_lsetbind;
  EXEC4(3,2,exec_lsetbind);
  dect4[3].func[2].pdis  = dump_lsetbind;

//ADDSYM(exec_ldrd);
//dect4[3].func[3].pexec = exec_ldrd;
  EXEC4(3,3,exec_ldrd);
  dect4[3].func[3].fmtinst = "LDRD<#c> <rt>, <rt2>, [<rn>, #+/-<imm8>]<W#!>";

//ADDSYM(exec_ldrex);
//dect4[3].func[4].pexec = exec_ldrex;
  EXEC4(3,4,exec_ldrex);
  dect4[3].func[4].fmtinst = "LDREX<#c> <rt>,[<rn>{,#<imm>}]";

//ADDSYM(exec_tbb);
//dectlsetb[6].func[0].pexec = exec_tbb;
  EXECLSETB(6,0,exec_tbb);
  dectlsetb[6].func[0].fmtinst = "TBB [<rn>,<rm>]";

//ADDSYM(exec_tbh);
//dectlsetb[7].func[0].pexec = exec_tbh;
  EXECLSETB(7,0,exec_tbh);
  dectlsetb[7].func[0].fmtinst = "TBH [<rn>,<rm>,LSL #1]";

  adddec4(3,"P"     ,1,24,0,1);
  adddec4(3,"U"     ,1,23,0,1);
  adddec4(3,"W"     ,1,21,0,0);
  adddec4(3,"L"     ,1,20,0,1);
  adddec4(3,"rn"    ,4,16,0,0);
  adddec4(3,"rt"    ,4,12,0,0);
  adddec4(3,"rd,rt2",4, 8,0,0);
  adddec4(3,"imm8"  ,8, 0,0,0);
  adddec4(3,"rm"    ,4, 0,0,0);
  adddec4(3,"op"    ,4, 4,0,0);

  dect4[4].desc = "load and store multiple RFE, and Table branch";
  dect4[4].nfunc = 1;

//ADDSYM(exec_ls4);
  ADDSYM(dump_ls4);
//dect4[4].func[0].pexec = exec_ls4;
  EXEC4(4,0,exec_ls4);
  dect4[4].func[0].pdis  = dump_ls4;
  dect4[4].func[0].fmtinst = "<L#L><#c>     <rn><W#!>, <0#m>";

  adddec4(4,"V"    ,1,24,0,0);
  adddec4(4,"U"    ,1,23,0,0);
  adddec4(4,"W"    ,1,21,0,0);
  adddec4(4,"L"    ,1,20,0,0);
  adddec4(4,"rn"   ,4,16,0,0);
  adddec4(4,"P"    ,1,15,0,0);
  adddec4(4,"M"    ,1,14,0,0);
  adddec4(4,"SBZ"  ,1,13,0,0);
  adddec4(4,"mask" ,13,0,0,0);

  ADDSYM(deco_branch);
  dect4[5].desc = "branches, miscellaneous control";
  dect4[5].nfunc = 15;
  dect4[5].pfd   = deco_branch;

//ADDSYM(exec_bwt4);
  ADDSYM(dump_bwt4);
//dect4[5].func[0].pexec = exec_bwt4;
  EXEC4(5,0,exec_bwt4);
  dect4[5].func[0].pdis  = dump_bwt4;
  dect4[5].func[0].fmtinst = "B<#c>.W  <0> ; @<1>";

//ADDSYM(exec_bl);
  ADDSYM(dump_bl);
//dect4[5].func[1].pexec = exec_bl;
  EXEC4(5,1,exec_bl);
  dect4[5].func[1].pdis  = dump_bl;
  dect4[5].func[1].fmtinst = "BL<#c>    <0> ; @<1>";

//ADDSYM(exec_bwt3);
  ADDSYM(dump_bwt3);
//dect4[5].func[4].pexec = exec_bwt3;
  EXEC4(5,4,exec_bwt3);
  dect4[5].func[4].pdis  = dump_bwt3;
  dect4[5].func[4].fmtinst = "B<cond#C>.W  <0> ; @<1>";

//ADDSYM(exec_msr);
//dect4[5].func[7].pexec = exec_msr;
  EXEC4(5,7,exec_msr);
  dect4[5].func[7].fmtinst = "MSR<#c> <mask1>, <rn>; sysm = <sysm>";

//ADDSYM(exec_nop);
//dect4[5].func[8].pexec = exec_nop;
  EXEC4(5,8,exec_nop);
  dect4[5].func[8].fmtinst = "NOP.W hint=<hint>";

//ADDSYM(exec_mrs);
//dect4[5].func[12].pexec = exec_mrs;
  EXEC4(5,12,exec_mrs);
  dect4[5].func[12].fmtinst = "MRS<#c> <rd>, <sysm>";

  adddec4(5,"S"        , 1,26,0,0);
  adddec4(5,"imm10"    ,10,16,0,0);
  adddec4(5,"j1"       , 1,13,0,0);
  adddec4(5,"j2"       , 1,11,0,0);
  adddec4(5,"imm11"    ,11, 0,0,0);
  adddec4(5,"sw1"      , 1,14,0,0);
  adddec4(5,"sw2"      , 1,12,0,0);
  adddec4(5,"sw3"      , 3,20,0,0);
  adddec4(5,"sw4"      , 3,23,0,0);
  adddec4(5,"rn"       , 4,16,0,0);
  adddec4(5,"rd,mask"  , 4, 8,0,0);
  adddec4(5,"imm8,sysm", 8, 0,0,0);
  adddec4(5,"imm6"     , 6,16,0,0);
  adddec4(5,"cond"     , 4,22,0,0);
  adddec4(5,"hint"     , 4, 4,0,0);
  adddec4(5,"mask1"    , 2,10,0,0);

// 
  dect4[6].desc = "3.3.7 Coprocessor instructions [1]";
  dect4[6].nfunc = 0;
  dect4[6].pfd   = 0;

/* A6.4 Floating-point data-processing instructions
    1         1                     1         1
    5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
    1 1 1 T 1 1 1 0|opc1---|opc2---|------- 1 0 1 s opc3- 0 opc4---

NOTE bit 4 == 0 is the switch between A6.4 and A6.6
*/
  ADDSYM(deco_fpdata);
  dect4[7].desc = "A6.4 Floating-point data-processing instructions [2]";
  dect4[7].nfunc = 30;
  dect4[7].pfd   = deco_fpdata;

/*
 A6.6 32-bit transfer between ARM core and extension registers
    1         1                     1         1
    5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
    1 1 1 T 1 1 1 0 A A A L ------- ------- 1 0 1 C - B B 1 -------

NOTE bit 4 == 1 is the switch between A6.4 and A6.6
NOTE vfnms and vfms are not present in the doc

*/

// Table A6.4
  // 0 vsel TODO
//ADDSYM(exec_vmlx);
  ADDSYM(dump_vmlx);
//dect4[7].func[1].pexec = exec_vmlx;
  EXEC4(7,1,exec_vmlx);
  dect4[7].func[1].pdis  = dump_vmlx;
  dect4[7].func[1].fmtinst = "VML<0#%s><#c><sz#f> S<1#%d>, S<2#%d>, S<3#%d>";

//ADDSYM(exec_vnmxx);
  ADDSYM(dump_vnmxx);
//dect4[7].func[2].pexec = exec_vnmxx;
  EXEC4(7,2,exec_vnmxx);
  dect4[7].func[2].pdis  = dump_vnmxx;
  dect4[7].func[2].fmtinst = "VNM<0#%s><#c><sz#f> S<1#%d>, S<2#%d>, S<3#%d>";

//ADDSYM(exec_vmul);
  ADDSYM(dump_3r);
//dect4[7].func[3].pexec = exec_vmul;
  EXEC4(7,3,exec_vmul);
  dect4[7].func[3].pdis  = dump_3r;
  dect4[7].func[3].fmtinst = "VMUL<#c><sz#f> S<1#%d>, S<2#%d>, S<3#%d>";

//ADDSYM(exec_vadd);
//dect4[7].func[4].pexec = exec_vadd;
  EXEC4(7,4,exec_vadd);
  dect4[7].func[4].pdis  = dump_3r;
  dect4[7].func[4].fmtinst = "VADD<#c><sz#f> S<1#%d>, S<2#%d>, S<3#%d>";

//ADDSYM(exec_vsub);
//dect4[7].func[5].pexec = exec_vsub;
  EXEC4(7,5,exec_vsub);
  dect4[7].func[5].pdis  = dump_3r;
  dect4[7].func[5].fmtinst = "VSUB<#c><sz#f> S<1#%d>, S<2#%d>, S<3#%d>";

//ADDSYM(exec_vdiv);
//dect4[7].func[6].pexec = exec_vdiv;
  EXEC4(7,6,exec_vdiv);
  dect4[7].func[6].pdis  = dump_3r;
  dect4[7].func[6].fmtinst = "VDIV<#c><sz#f> S<1#%d>, S<2#%d>, S<3#%d>";

//ADDSYM(exec_vmovi);
  ADDSYM(dump_vmovi);
//dect4[7].func[8].pexec = exec_vmovi;
  EXEC4(7,8,exec_vmovi);
  dect4[7].func[8].pdis  = dump_vmovi;
  dect4[7].func[8].fmtinst = "VMOV<#c><sz#f> S<1#%d>, #<2#%d> ; <3#%x>";

//ADDSYM(exec_vmov);
  ADDSYM(dump_2r);
//dect4[7].func[9].pexec = exec_vmov;
  EXEC4(7,9,exec_vmov);
  dect4[7].func[9].pdis  = dump_2r;
  dect4[7].func[9].fmtinst = "VMOV<#c><sz#f> S<1#%d>, S<2#%d>";

//ADDSYM(exec_vneg);
//dect4[7].func[11].pexec = exec_vneg;
  EXEC4(7,11,exec_vneg);
  dect4[7].func[11].pdis  = dump_2r;
  dect4[7].func[11].fmtinst = "VNEG<#c><sz#f> S<1#%d>, S<2#%d>";

//ADDSYM(exec_vsqrt);
//dect4[7].func[12].pexec = exec_vsqrt;
  EXEC4(7,12,exec_vsqrt);
  dect4[7].func[12].pdis  = dump_2r;
  dect4[7].func[12].fmtinst = "VSQRT<#c><sz#f> S<1#%d>, S<2#%d>";

//ADDSYM(exec_vcmp);
  ADDSYM(dump_vcmp);
//dect4[7].func[14].pexec = exec_vcmp;
  EXEC4(7,14,exec_vcmp);
  dect4[7].func[14].pdis  = dump_vcmp;
  dect4[7].func[14].fmtinst = "VCMP<0#%s><#c><sz#f> S<1#%d>, <2#%s>";

//ADDSYM(exec_vcvt);
  ADDSYM(dump_vcvt);
//dect4[7].func[16].pexec = exec_vcvt;
  EXEC4(7,16,exec_vcvt);
  dect4[7].func[16].pdis  = dump_vcvt;
  dect4[7].func[16].fmtinst = "VCVT<0#%s><#c>.S32.F32 S<1#%d>, S<2#%d>";

//dect4[7].func[21].pexec = exec_vcvt;
  EXEC4(7,21,exec_vcvt);
  dect4[7].func[21].pdis  = dump_vcvt;
  dect4[7].func[21].fmtinst = "VCVT<0#%s><#c>.S32.F32 S<1#%d>, S<2#%d>";

//ADDSYM(exec_vfmx);
//dect4[7].func[22].pexec = exec_vfmx;
  EXEC4(7,22,exec_vfmx);
  dect4[7].func[22].pdis  = dump_vmlx;
  dect4[7].func[22].fmtinst = "VFM<0#%s><#c><sz#f> S<1#%d>, S<2#%d>, S<3#%d>";

//ADDSYM(exec_vfnmx);
//dect4[7].func[23].pexec = exec_vfnmx;
  EXEC4(7,23,exec_vfnmx);
  dect4[7].func[23].pdis  = dump_vmlx;
  dect4[7].func[23].fmtinst = "VFNM<0#%s><#c><sz#f> S<1#%d>, S<2#%d>, S<3#%d>";

// Table A6.6 start at 24
//ADDSYM(exec_vmovcr);
  ADDSYM(dump_vmovcr);
//dect4[7].func[24].pexec = exec_vmovcr;
  EXEC4(7,24,exec_vmovcr);
  dect4[7].func[24].pdis  = dump_vmovcr;
  dect4[7].func[24].fmtinst = "VMOV<#c> S<0#%d>, <Rt>";

  EXEC4(7,25,exec_vmsr);
//dect4[7].func[25].pdis  = dump_vmsr;
  dect4[7].func[25].fmtinst = "VMSR<#c> FPSCRS, <Rt>";

//dect4[7].func[27].pexec = exec_vmovcr;
  EXEC4(7,27,exec_vmovcr);
  dect4[7].func[27].pdis  = dump_vmovcr;
  dect4[7].func[27].fmtinst = "VMOV<#c> S<0#%d>, <Rt>";

//ADDSYM(exec_vmrs);
//dect4[7].func[28].pexec = exec_vmrs;
  EXEC4(7,28,exec_vmrs);
  dect4[7].func[28].fmtinst = "VMRS<#c> <Rt>, FPSCR";

  adddec4(7,"T"             , 1,28,0,0); //  0
  adddec4(7,"opc1"          , 4,20,0,0); //  1
  adddec4(7,"opc2,Vn,imm4H" , 4,16,0,0); //  2
  adddec4(7,"sz,C"          , 1, 8,0,0); //  3
  adddec4(7,"opc3"          , 2, 6,0,0); //  4
  adddec4(7,"opc4,Vm,imm4L" , 4, 0,0,0); //  5
  adddec4(7,"bit4"          , 1, 4,0,0); //  6
  adddec4(7,"A"             , 3,21,0,0); //  7
  adddec4(7,"L,op"          , 1,20,0,0); //  8
  adddec4(7,"B"             , 2, 5,0,0); //  9
  adddec4(7,"D"             , 1,22,0,0); // 10
  adddec4(7,"M"             , 1, 5,0,0); // 11
  adddec4(7,"Vd"            , 4,12,0,0); // 12
  adddec4(7,"Rt"            , 4,12,0,0); // 13
  adddec4(7,"N,op7"         , 1, 7,0,0); // 14
  adddec4(7,"op6"           , 1, 6,0,0); // 15
  adddec4(7,"op8"           , 2,20,0,0); // 16

/* A6.5 Extension register load or store instructions
    1         1                     1         1
    5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
    1 1 1 T 1 1 0|Opcode---|Rn ----|------- 1 0 1 -----------------
*/
  ADDSYM(deco_fpext);
  dect4[8].desc = "A6.5 Extension register load or store instructions [2]";
  dect4[8].nfunc = 11;
  dect4[8].pfd   = deco_fpext;

// 64 bit transfer A6-167 -> vmov64
//ADDSYM(exec_vmov64);
//dect4[8].func[0].pexec = exec_vmov64;
  EXEC4(8,0,exec_vmov64);
//dect4[8].func[0].pdis  = dump_vstr;
  dect4[8].func[0].fmtinst = "VMOV 64 TODO" ;

//ADDSYM(exec_vstr);
  ADDSYM(dump_vstr);
//dect4[8].func[3].pexec = exec_vstr;
  EXEC4(8,3,exec_vstr);
  dect4[8].func[3].pdis  = dump_vstr;
  dect4[8].func[3].fmtinst = "VSTR<#c> S<0#%d>, [<rn><1#%s>]" ;

//ADDSYM(exec_vpush);
  ADDSYM(dump_vpush);
//dect4[8].func[5].pexec = exec_vpush;
  EXEC4(8,5,exec_vpush);
  dect4[8].func[5].pdis  = dump_vpush;
  dect4[8].func[5].fmtinst = "VPUSH<#c> <0#m>";

//ADDSYM(exec_vpop);
//dect4[8].func[8].pexec = exec_vpop;
  EXEC4(8,8,exec_vpop);
  dect4[8].func[8].pdis  = dump_vpush;
  dect4[8].func[8].fmtinst = "VPOP<#c> <0#m>";

//ADDSYM(exec_vldr);
//dect4[8].func[9].pexec = exec_vldr;
  EXEC4(8,9,exec_vldr);
  dect4[8].func[9].pdis  = dump_vstr;
  dect4[8].func[9].fmtinst = "VLDR<#c> <2#%s><0#%d>, [<rn><1#%s>]";

  adddec4(8,"T"       , 1,28,0,0);
  adddec4(8,"opcode"  , 5,20,0,0);
  adddec4(8,"rn,rt2"  , 4,16,0,0);
  adddec4(8,"sw"      , 4, 8,0,0);
  adddec4(8,"D"       , 1,22,0,0);
  adddec4(8,"U"       , 1,23,0,0);
  adddec4(8,"vd,rt"   , 4,12,0,0);
  adddec4(8,"imm8"    , 8, 0,0,0);
  adddec4(8,"C"       , 1, 8,0,0);
  adddec4(8,"op,vm"   , 4, 0,0,0);
  adddec4(8,"M"       , 1, 5,0,0);


/* A6.7 64-bit transfers between ARM core and extension registers
    1         1                     1         1
    5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
    1 1 1 T 1 1 0 0 0 1 0 --------- ------- 1 0 1 C|op-----|-------
*/
// build decode table based on 
// first 10 bit of instruction

  for (i = 0; i < 1024; i++)
  {
    ist = i << 6;
    if (((ist >> 12) & 0xf) == 0xf ||
        ((ist >> 11) & 0x1f) == 0x1d)
    {
      decmt[i].dt = 20; 
      decmt[i].il = 4;
      if (ist >> 11 == 0x1e)
      {
        decmt[i].dt4 = 0;   // 0 or 5
        decmt[i].extra = 1; 
      } else if (ist >> 9 == 0x75 ||
                 ist >> 9 == 0x7d) 
        decmt[i].dt4 = 1; 
      else if (ist >> 9 == 0x7c)
        decmt[i].dt4 = 2; 
      else if (ist >> 9 == 0x74)
      {
        if ((ist >> 6 & 1) == 1)
          decmt[i].dt4 = 3; 
        else
          decmt[i].dt4 = 4; 
      } else if (ist >> 8 == 0xff ||
                 ist >> 8 == 0xef)
        decmt[i].dt4 = 6; 
/* A6.4 Floating-point data-processing instructions
    1         1                     1         1
    5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
    1 1 1 T 1 1 1 0|opc1---|opc2---|------- 1 0 1 s opc3- 0 opc4---
NOTE bit 4 == 0 
   A6.6 32-bit transfer between ARM core and extension registers
    1         1                     1         1
    5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
    1 1 1 T 1 1 1 0 A A A L ------- ------- 1 0 1 C - B B 1 -------
NOTE bit 4 == 1 is the switch between A6.4 and A6.6
*/
      else if ((ist >> 8) == 0xfe || 
               (ist >> 8) == 0xee)
        decmt[i].dt4 = 7; 
/* A6.5 Extension register load or store instructions
    1         1                     1         1
    5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
    1 1 1 T 1 1 0|Opcode---|Rn ----|------- 1 0 1 -----------------
*/
      else if ((ist >> 8) == 0xec || 
               (ist >> 8) == 0xed)
        decmt[i].dt4 = 8; 
      else
        decmt[i].dt4 = -1; 
    
    } else
    {
      decmt[i].il = 2;
      if (ist >> 10 == 6)
        decmt[i].dt = 1;
      else if (ist >> 10 == 7)
        decmt[i].dt = 2;
      else if (ist >> 13 == 0)
        decmt[i].dt = 0;
      else if (ist >> 13 == 1)
        decmt[i].dt = 3;
      else if (ist >> 10 == 16)
        decmt[i].dt = 4;
      else if (ist >> 8 == 0x47)
        decmt[i].dt = 6;
      else if (ist >> 10 == 0x11)
        decmt[i].dt = 5;
      else if (ist >> 11 == 9)
        decmt[i].dt = 7;
      else if (ist >> 12 == 5)
        decmt[i].dt = 8;
      else if (ist >> 13 == 3)
        decmt[i].dt = 9;
      else if (ist >> 12 == 8)
        decmt[i].dt = 10;
      else if (ist >> 12 == 9)
        decmt[i].dt = 11;
      else if (ist >> 12 == 10)
        decmt[i].dt = 12;
      else if (ist >> 12 == 11)
        decmt[i].dt = 13;
      else if (ist >> 12 == 12)
        decmt[i].dt = 14;
      else if (ist >> 8 == 0xde)
        decmt[i].dt = 16;
      else if (ist >> 8 == 0xdf)
        decmt[i].dt = 17;
      else if (ist >> 12 == 13)
        decmt[i].dt = 15;
      else if (ist >> 11 == 0x1c)
        decmt[i].dt = 18;
      else
        error("bug"); 
    } 
  }
}

struct t_dec *decode(struct t_cpu *p_cpu,struct t_stat *p_st,int noerr)
{
  int f,sw,t,i,extra,swi;
  struct t_dec *pd;

  extra = 0;
  if (p_st->il == 2)
  {
    sw = (p_st->inst >> 6) & 0x3ff;
    t = decmt[sw].dt;
    if (!noerr && t < 0) exec_error(p_cpu,"decode l=2");
    if (t < 0) return 0;
    p_st->pd = pd = dect + t;
  } else
  {
    sw = (p_st->inst >> 22) & 0x3ff;
    t = decmt[sw].dt4;
    if (!noerr && t < 0) exec_error(p_cpu,"decode l=4");
    if (t < 0) return 0;
    extra = decmt[sw].extra;
    if (extra)
    {
      if (t == 0)
      {
        if ((p_st->inst >> 15) & 1)
          t = 5;
      } else if (t == 3)
      {
        if (((p_st->inst >> 22) & 1) == 0)
          t = 4;
      }
    }
    p_st->pd = pd = dect4 + t;
  }

  p_st->nf = pd->nf;
  swi = 0;
  for (i = 0; i < pd->nf; i++)
  {  
    f = (p_st->inst >> pd->f[i].pos) & mask[pd->f[i].len];
    if (pd->f[i].sig && (f & masks[pd->f[i].len]))
      f = f | ~mask[pd->f[i].len];
    if (pd->f[i].sw)
    {
      swi = swi << pd->f[i].len;
      swi = swi | f;
    }
#ifdef DD1
    if (p_st->prefix)
      logprefix(p_cpu,2);
    printf("i %2d m %02x ms %02x f %02x sw %d swi %x ",
            i,mask[pd->f[i].len],masks[pd->f[i].len],f,pd->f[i].sw,swi);
    if (pd->func[swi].desc)
      printf("desc %s",pd->func[swi].desc);
    putchar('\n');
#endif
    p_st->fields[i] = f;
  }
  if (pd->pfd)
    swi = pd->pfd(p_cpu,p_st,noerr);
  p_st->swi = swi;
#ifdef DD
  if (DDLOG(p_cpu))
  {
    char bb[50];
    if (p_st->prefix)
      logprefix(p_cpu,0);
    binbuf(p_st->inst,p_st->il * 8,bb);
    printf("D inst %04x %s\n",
           p_st->inst,bb);
    if (p_st->prefix)
      logprefix(p_cpu,0);
    printf("D desc %s\n",pd->desc);
    if (p_st->prefix)
      logprefix(p_cpu,0);
    printf("D sw %2x l %d t %d # of fields %d extra %d swi %d\n",
           sw,decmt[sw].il,t,pd->nf,extra,swi);
    if (swi >= 0)
    {
      char ne[40];
      char nd[40];

      if (p_st->prefix)
        logprefix(p_cpu,0);
      printf("D ncall %d pdis %s pexe %s fmtinst %s\n",
             pd->func[swi].ncall,
             fmtfname(p_cpu,pd->func[swi].pdis,nd),
             fmtfname(p_cpu,pd->func[swi].pexec,ne),
             pd->func[swi].fmtinst);
    }
  }
#endif
#ifdef DD
  if (DDLOG(p_cpu))
    for (i = 0; i < pd->nf; i++)
    {
      if (p_st->prefix)
        logprefix(p_cpu,2);
       printf("- %2d: %-16s p%2d l%2d s%d sw%d v %4x %6d\n",
              i,
              pd->f[i].name,
              pd->f[i].pos,
              pd->f[i].len,
              pd->f[i].sig, 
              pd->f[i].sw, 
              p_st->fields[i],
              p_st->fields[i]);
    }
#endif
  if (noerr == 0 && p_st->swi < 0) 
    exec_error(p_cpu,"decode pc %08x il %d inst %x sw %d swi %d",
               p_st->pc,p_st->il,p_st->inst,sw,swi); 
  return pd;
}

// 3.2.1 Miscellaneous instructions
int deco_misc(struct t_cpu *p_c,struct t_stat *p_st,int noerr)
{
  int sw,mask,idx;

  sw   = p_st->fields[0];  CHK(p_st,0,"sw"   ,__LINE__);
  mask = p_st->fields[2];  CHK(p_st,2,"mask" ,__LINE__);

  idx = -1;
  if (sw == 0)                    // adjust stack pointer ADD + SUB
    idx = 0;
  else if (sw == 2)               // sign or zero extend instructions
    idx = 1;
  else if (sw == 1 || sw == 3)    // Compare and branch on zero instructions
    idx = 2;
  else if (sw == 9 || sw == 11)   // Compare and branch on non-zero instructions
    idx = 3;
  else if (sw == 4 || sw == 5)    // push register list
    idx = 4;
  else if (sw == 12 || sw == 13)  // pop register list
    idx = 5;
  else if (sw == 6)               // also 6,7,9 TODO !!!
    idx = 8;
  else if (sw == 10)              // reverse bytes instructions
    idx = 10;
  else if (sw == 15 && mask != 0) // If-Then instruction
    idx = 12;
  else if (sw == 15 && mask == 0) // NOP-compatible hint instructions.
    idx = 13;
  else if (sw == 14)              // software breakpoint BKPT 
    idx = 9;

#ifdef DD
  if (DDLOG(p_c))
  {
    logprefix(p_c,0);
    printf("in deco_misc sw %d mask %d -> %d\n",sw,mask,idx);
  }
#endif
  if (idx == -1)
  {
    printf("in deco_misc sw %d mask %d -> %d\n",sw,mask,idx);
    if (noerr == 0)
      exec_error(p_c,"deco_misc");
  }

  return idx;
}

int deco_branch(struct t_cpu *p_c,struct t_stat *p_st,int noerr)
{
  int r,sw1,sw2,sw3,sw4,cond;
  
  r    = -1;
  sw1  = p_st->fields[5];  CHK(p_st,5,"sw1"   ,__LINE__);
  sw2  = p_st->fields[6];  CHK(p_st,6,"sw2"   ,__LINE__);
  sw3  = p_st->fields[7];  CHK(p_st,7,"sw3"   ,__LINE__);
  sw4  = p_st->fields[8];  CHK(p_st,8,"sw4"   ,__LINE__);
  cond = p_st->fields[13]; CHK(p_st,13,"cond" ,__LINE__);
  
  if (sw1 == 0 && sw2 == 1)
    r = 0; 
  else if (sw1 == 1 && sw2 == 1)
    r = 1;
  else if (sw1 == 0 && sw2 == 0)
  {
    if ((sw3 == 0 || sw3 == 1) && sw4 == 7)
      r = 7;
    else if ((sw3 == 6 || sw3 == 7) && sw4 == 7)
      r = 12;
    else 
    {
      if (cond == 14 || cond == 15)
        r = 8;
      else
        r = 4;
    }
  } 
// 0 0 7 0
#ifdef DD
  if (DDLOG(p_c))
  {
    logprefix(p_c,0);
    printf("in deco_branch sw1,2,3,4 %d %d %d %d cond %d -> %d\n",sw1,sw2,sw3,sw4,cond,r);
  }
#endif
  if (r == -1)
  {
    printf("in deco_branch sw1,2,3,4 %d %d %d %d -> %d\n",sw1,sw2,sw3,sw4,r);
    if (noerr == 0)
      exec_error(p_c,"deco_branch");
  }

  return r;
}

int deco_nimm(struct t_cpu *p_c,struct t_stat *p_st,int noerr)
{
  int rn,sw1,sw2,sw3,r;

  r = -1;
  rn  = p_st->fields[9];  CHK(p_st, 9,"racc",__LINE__);
  sw1 = p_st->fields[0];  CHK(p_st, 0,"sw1", __LINE__);
  sw2 = p_st->fields[1];  CHK(p_st, 1,"sw2", __LINE__);
  sw3 = p_st->fields[13]; CHK(p_st,13,"sw3", __LINE__);

  if (sw1 == 0)
    r = 0;
  else 
  {
    if (rn != PC && sw2 <= 1)
      r = 5;     
    else if (sw2 == 0)
    {
      if (sw3 == 0)
        r = 1;
      else
        r = 2;
    } else if (sw2 == 1)
    {
      if (sw3 == 0)
        r = 3;
      else
        r = 4;
    } else if (sw2 == 2)
      r = 6;
    else if (sw2 == 3)
      r = 7;
  }
#ifdef DD
  if (DDLOG(p_c))
  {
    logprefix(p_c,0);
    printf("in deco_nimm rn %d sw1,2,3 %d %d %d -> %d\n",rn,sw1,sw2,sw3,r);
  }
#endif
  if (r == -1 && noerr == 0)
    exec_error(p_c,"deco_nimm");
  return r;
}

int deco_movsh(struct t_cpu *p_c,struct t_stat *p_st,int noerr)
{
  int r,op,imm5;

  r    = -1;
  op   = p_st->fields[0];  CHK(p_st,0,"opcode",__LINE__);
  imm5 = p_st->fields[1];  CHK(p_st,1,"imm5"  ,__LINE__);

  if (op == 0)
    if (imm5 == 0)
      r = 0;
    else
      r = 1;
  else if (op == 1)
    r = 2;
  else if (op == 2)
    r = 3;
  
#ifdef DD
  if (DDLOG(p_c))
  {
    logprefix(p_c,0);
    printf("in deco_movsh op %d imm5 %d -> %d\n",op,imm5,r);
  }
#endif
  if (r == -1 && noerr == 0)
    exec_error(p_c,"deco_movsh");
  return r;
}

// see 3.3.4 Load/store double and exclusive, and table branch
//
// 0 1 2 as table(only store) +
// 3 4                 load

int deco_lsdet(struct t_cpu *p_c,struct t_stat *p_st,int noerr)
{
  int r,P,W,U,L;

  P    = p_st->fields[0]; CHK(p_st,0,"P" ,__LINE__);
  U    = p_st->fields[1]; CHK(p_st,1,"U" ,__LINE__);
  W    = p_st->fields[2]; CHK(p_st,2,"W" ,__LINE__);
  L    = p_st->fields[3]; CHK(p_st,3,"L" ,__LINE__);

  r = -1;
  if (P != 0 || W != 0)
  {
    if (L == 0)
      r = 0;
    else
      r = 3; 
  } else if (U == 0)
  {
    if (L == 0)
      r = 1;
    else
      r = 4; 
  } else 
    r = 2;
  
#ifdef DD
  if (DDLOG(p_c))
  {
    logprefix(p_c,0);
    printf("in deco_lsdet P %d U %d W %d L %d -> %d\n",P,U,W,L,r);
  }
#endif
  return r;
}

// see 3.3.3 Load and store single data item, and memory hints
// pg 3.26
// output as table  0.. 6 for store  7..9 reserved
//                 10..16 for load

int deco_ls(struct t_cpu *p_c,struct t_stat *p_st,int noerr)
{
  int L,U,rn,sw,r,S,rt,size;

  U    = p_st->fields[1]; CHK(p_st,1,"U" ,__LINE__);
  L    = p_st->fields[3]; CHK(p_st,3,"L" ,__LINE__);
  rn   = p_st->fields[4]; CHK(p_st,4,"rn",__LINE__);
  rt   = p_st->fields[5]; CHK(p_st,5,"rt",__LINE__);
  sw   = p_st->fields[8]; CHK(p_st,8,"sw",__LINE__);
  S    = p_st->fields[0]; CHK(p_st,0,"S" ,__LINE__);
  size = p_st->fields[2]; CHK(p_st,2,"size" ,__LINE__);

  r = -1;

  if (rn == PC)
  {
    if (L == 1)
      r = 0;
    else 
    {
      error("check deco_ls.1 rn %2d L %d sw %d S %d rt %2d",rn,L,sw,S,rt);
      r = 9;
    }
  } else if (U == 1)
    r = 1;
  else if (sw == 12)
    r = 2;
  else if (sw == 14)
    r = 3;
  else if (sw == 9  || sw == 11)
    r = 4;
  else if (sw == 13 || sw == 15)
    r = 5;
  else if (sw == 0)
    r = 6;
  else
  {
// r 7..9
    error("check deco_ls.2 rn %2d L %d sw %d S %d rt %2d",rn,L,sw,S,rt);
  }
  if (rt == PC && size == 0 && L == 1)
  {
    if ((r == 0 || r == 1 || r == 2 || r == 6))
    {
      if (S == 0)
        r = 7;
      else
        r = 8;
    } else
    {
      r = 9;
      exec_warning(p_c,"check deco_ls.3 rn %2d L %d sw %d S %d rt %2d -> %d",rn,L,sw,S,rt,r);
    }
  }
  if (r <= 6 && L == 1) // load store switch
  {
    r += 10;
  }
#ifdef DD
  if (DDLOG(p_c))
  {
    logprefix(p_c,0);
    printf("in deco_ls rn %d U %d sw %d -> %d\n",rn,U,sw,r);
  }
#endif
  return r;
}

// A6.4 Floating-point data-processing instructions
// A6.6 64-bit transfers between ARM core and extension registers
// decode
// both share the same hight 10 bit instruction space
// switch based on bit 4
// 22 places for A6.4  0..21
//  1 forgot inst vfma    22
//  6 places for A6.6 23..28
int deco_fpdata(struct t_cpu *p_c,struct t_stat *p_st,int noerr)
{
  int r;
  int T,opc1,opc2,opc3;
  int bit4,A,L,C,B;

  T    = p_st->fields[ 0]; CHK(p_st, 0,"T"      ,__LINE__);
  opc1 = p_st->fields[ 1]; CHK(p_st, 1,"opc1"   ,__LINE__);
  opc2 = p_st->fields[ 2]; CHK(p_st, 2,"opc2"   ,__LINE__);
  opc3 = p_st->fields[ 4]; CHK(p_st, 4,"opc3"   ,__LINE__);
  bit4 = p_st->fields[ 6]; CHK(p_st, 6,"bit4"   ,__LINE__);
  A    = p_st->fields[ 7]; CHK(p_st, 7,"A"      ,__LINE__);
  L    = p_st->fields[ 8]; CHK(p_st, 8,"L"      ,__LINE__);
  C    = p_st->fields[ 3]; CHK(p_st, 3,"C"      ,__LINE__);
  B    = p_st->fields[ 9]; CHK(p_st, 9,"B"      ,__LINE__);

  r = -1;
#ifdef DD
  if (DDLOG(p_c))
  {
    logprefix(p_c,0);
    printf("deco_fpdata bit4 %d ",bit4);
  }
#endif
  if (bit4 == 0)
  {
    if (T == 1 && opc1 < 8)
      r = 0; // vsel
    else if (T == 0 && (opc1 == 0 || opc1 == 4))
      r = 1;  // vmla, vmls
    else if (T == 0 && (opc1 == 1 || opc1 == 5))
      r = 2; // vnmla,vnkls,vnmul
    else if (T == 0 && (opc1 == 2 || opc1 == 6) && (opc3 & 1))
      r = 2;  // vnmla,vnkls,vnmul repeated !!
    else if (T == 0 && (opc1 == 2 || opc1 == 6) && !(opc3 & 1))
      r = 3; // vmul
    else if (T == 0 && (opc1 == 3 || opc1 == 7) && !(opc3 & 1))
      r = 4; // vadd
    else if (T == 0 && (opc1 == 3 || opc1 == 7) && (opc3 & 1))
      r = 5; // vsub
    else if (T == 0 && (opc1 == 8 || opc1 == 12) && !(opc3 & 1))
      r = 6; // vdiv
    else if (T == 1 && (opc1 == 8 || opc1 == 12) && !(opc3 & 1))
      r = 7; // vmaxnm,vminnm
    else if (T == 0 && (opc1 == 14 || opc1 == 10)) // not present in table but in instruction detail
      r = 22; // vfma  fused multiply add
    else if (T == 0 && (opc1 == 13 || opc1 == 9)) // not present in table but in instruction detail
      r = 23;
    else if (T == 0 && (opc1 == 11 || opc1 == 15))
    {
      if (!(opc3 & 1))
        r = 8; // vmov
      else if (opc2 == 0 && opc3 == 1)
        r = 9; // vmov
      else if (opc2 == 0 && opc3 == 3)
        r = 10; // vabs
      else if (opc2 == 1 && opc3 == 1)
        r = 11; // vneg
      else if (opc2 == 1 && opc3 == 3)
        r = 12; // vsqrt
      else if ((opc2 == 2 || opc2 == 3) && opc3 & 1)
        r = 13; // vcvt..
      else if ((opc2 == 4 || opc2 == 5) && opc3 & 1)
        r = 14; // vcmp ..
      else if ((opc2 == 6 || opc2 == 7) && opc3 & 1)
        r = 15; // vrintz ..
      else if (opc2 == 8 && (opc3 & 1))
        r = 16; // vcvt 
    } 
    if (r == -1 && (opc1 == 11 || opc1 == 15))
    {
//    printf("HERE T %d opc2 %d %d\n",T,opc2,opc2 & 14);
      if (T == 1 && (opc2 >= 8 && opc2 < 12) && (opc3 == 1))
        r = 18; // vrinta, vrintn, vrintp, vrintm
      else if (T == 1 && (opc2 > 12) && ((opc3 & 1) == 1))
        r = 19; // vcvta, vcvtn, vcvtp, vcvtm
      else if (T == 0 && ((opc2 & 10) == 10) && ((opc3 & 1) == 1))
        r = 20; // vcvt
      else if (T == 0 && ((opc2 & 14) == 12) && ((opc3 & 1) == 1))
        r = 21; // vcvt, vcvtr
    }
#ifdef DD
    if (DDLOG(p_c))
    {
      int opc4 = p_st->fields[ 5]; CHK(p_st, 5,"opc4"   ,__LINE__);
      int sz   = p_st->fields[ 3]; CHK(p_st, 3,"sz"     ,__LINE__);
      printf(" T %d opc1 %d opc2 %d sz %d opc3 %d opc4 %d -> r %d\n",
             T,opc1,opc2,sz,opc3,opc4,r);
    }
#endif
  } else // bit4 == 1
// Table A6.6 start at 24
  {
    if (L == 0 && C == 0 && A == 0)             // vmov
      r = 24;
    else if (L == 0 && C == 0 && A == 7)        // vmsr
      r = 25;                                  
    else if (L == 0 && C == 1 && (A == 0 || A == 1) && B == 0) // vmov
      r = 26;
    else if (L == 1 && C == 0 && A == 0)       // vmov
      r = 27;
    else if (L == 1 && C == 0 && A == 7)       // vmrs
      r = 28;
    else if (L == 1 && C == 1 && (A == 0 || A == 1) && B == 0) // vmov
      r = 29;
#ifdef DD
    if (DDLOG(p_c))
      printf("L %d C %d A %d B %d -> r %d\n",L,C,A,B,r);
#endif
  }
  return r;
}

// A6.5 Extension register load or store instructions
int deco_fpext(struct t_cpu *p_c,struct t_stat *p_st,int noerr)
{
  int r,T,op,rn;

  r = -1;
  T  = p_st->fields[0]; CHK(p_st,0,"T"      ,__LINE__);
  op = p_st->fields[1]; CHK(p_st,1,"opcode" ,__LINE__);
  rn = p_st->fields[2]; CHK(p_st,2,"rn"     ,__LINE__);

  if (op == 4 || op == 5) // 64 bit transfer A6-167 -> vmov64
    r = 0;
  if ((op & 0x13) == 0x11)
    r = 9;
  else if ((op & 0x13) == 0x10)
    r = 3;
  else if ((op & ~4) == 0x12 && rn == 0xd) // vpush
    r = 5;
  else if ((op & ~4) == 0xb && rn == 0xd) // vpop
    r = 8;
#ifdef DD
  if (DDLOG(p_c))
  {
    logprefix(p_c,0);
    printf("in deco_fpext T %d opcode %d rn %d -> %d\n",T,op,rn,r);
  }
#endif
  if (r == -1 && noerr == 0)
    warning("deco_fpext T %x op %x rn %x -> -1",T,op,rn);
  return r;
}

void initm()
{
  int i;

  masks[1] = 1;
  for (i = 2; i < 32; i++)
    masks[i] = masks[i - 1] << 1;
  mask[1] = 1;
  for (i = 2; i < 32; i++)
    mask[i] = (mask[i - 1] << 1) + 1;
}

void exec_error(struct t_cpu *p_c,char *e,...)
{
  static char mes[256];
  va_list args;
  va_start(args,e);
  vsprintf(mes,e,args);
  printf("exec_error: %s\n",mes);
  exit(1);
}

void exec_warning(struct t_cpu *p_c,char *e,...)
{
  static char mes[256];
  va_list args;
  va_start(args,e);
  vsprintf(mes,e,args);
  printf("exec_warning: %s\n",mes);
}

/*
void warning(char *e,...)
{
  static char mes[256];
  va_list args;
  va_start(args,e);
  vsprintf(mes,e,args);
  printf("W: %s\n",mes);
}
*/

int regr(struct t_cpu *p_cpu,int rm,int mode)
{ 
//printf("[regr R%02d %08x]",rm,p_cpu->regp0.r[rm]);
  return p_cpu->regp0.r[rm]; 
}

void logprefix(struct t_cpu *p_cpu,int l)
{
//printf("DD -->            ");
}

char *binbuf(int n,int l,char *buf)
{
  int i;
  char *p;

  memset(buf,' ',l + l/4);
  p = buf + l + 1 + l/4;
  *p-- = 0;
  for (i = 0; i < l; i++)
  {
    if (i && i % 4 == 0)
      *p-- = ' ';
    *p-- = '0' + (n & 1);
    n = n >> 1;
  }
  return buf;
}

#ifdef TMAIN
void test()
{

  int  l,ist,ist2;
  struct t_cpu  *p_cpu;
  struct t_stat *p_st;
  struct t_dec  *pd;

  p_cpu = malloc(sizeof(*p_cpu));
  memset(p_cpu,0,sizeof(*p_cpu));

  initdec(p_cpu);
  initm();
  initcondt();

  p_st = malloc(sizeof(*p_st));
  memset(p_st,0,sizeof(*p_st));
  printf("p_st %p\n",p_st);

//        5 20002dbe 429a     [       1] -----          9 CMP     R02, R03

  p_cpu->execpc = 0x20002dbe;
  ist2 = 0x429a;
  ist = 0x0;
//  printf("ist %08x p_st %p\n",ist,p_st);
  if (((ist2 >> 12) & 0xf) == 0xf ||
      ((ist2 >> 11) & 0x1f) == 0x1d)
  {
    p_st->inst = (ist2 << 16) | ((ist >> 16) & MW);
    l = 4;
  } else
  {
    p_st->inst = ist2;
    l = 2;
  }

  p_st->il = l;
    
  pd = decode(p_cpu,p_st,0);
/*
    if (l == 4) 
      printf("%6d %08x %04x %04x %08x ",
             i,regs[i1][PC],ist2,(ist >> 16) & 0xffff,st);
    else
      printf("%6d %08x %04x      %08x ",
             i,regs[i1][PC],ist2,st);
*/
  if (pd->nfunc <= p_st->swi || p_st->swi < 0)
  {
    printf("nf %d swi %d\n",pd->nfunc,p_st->swi);
    printf("no func to call\n");
  }
  if (pd->nfunc > 0)
  {
    if (pd->func[p_st->swi].pdis)
      pd->func[p_st->swi].pdis(p_cpu,p_st,pd->func + p_st->swi);
    else if (pd->func[p_st->swi].fmtinst)
      dump_gen(p_cpu,p_st,pd->func + p_st->swi);
    else
    {
      printf("TBD null function swi %d\n",p_st->swi);
//    error("TDB");
    }
  } else
  {
    printf("TBD nfunc %d\n",pd->nfunc);
//  error("no func to call");
  }
}

int main()
{
  printf("disasmtest\n");
  test();
  return 0;
}
#endif
