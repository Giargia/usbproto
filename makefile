# note on libusb-1.0
# cygwin distribution is an old version that don't work with cmis-dap
# latest version on 9/11/23 (1.0.26) has been downloaded and extracted here for cygwin-32 and cygwin-64
# file are:
# - libusb.h (same for 32 and 64)
# - cygusb_32-1.0.dll fail
# - cygusb_62-1.0.dll fail
# - libsb_32-1.0.dll fail
# - libsb_64-1.0.dll OK
# so build with 64 bit !!!

MACHINE=$(shell uname -m)
OS=$(shell uname -o)

ifeq ($(OS),Cygwin)
LIBUSB=/cygdrive/c/prog_src/libusb-master/libusb/.libs/libusb-1.0.a
else
#LIBUSB=~/prog_src/libusb/libusb/.libs/libusb-1.0.a -ludev -lpthread -lm -lssl -Wl,--copy-dt-needed-entries 
LIBUSB=~/prog_src/libusb/libusb/.libs/libusb-1.0.a -ludev -lpthread -lm -lssl -lcrypto -ldl
endif

$(info MACHINE is ${MACHINE} OS is ${OS})

#CFLAGS=-Wall -g -DDD
CFLAGS=-Wall -g -Ijimtcl
M=makefile
# repl module
#R=repl.o
R=jim-interface-stlink.o

all: mydriver-stlink readpcapng-stlink mydriver-pico driver-cmsis readlog

mydriver-stlink: mydriver-stlink.o chipid.o driversub.o readelf.o util.o disasm.o dump.o common-driver.o \
                 $R armreg.o *.h $M 
	gcc -Wall -g -o mydriver-stlink mydriver-stlink.o util.o chipid.o driversub.o readelf.o \
        disasm.o dump.o common-driver.o $R armreg.o -lusb-1.0 jimtcl/libjim.a -lz

mydriver-pico: mydriver-pico.c *.h $M
#	gcc -Wall -g -o mydriver-pico mydriver-pico.c libusb-1.0.dll
	gcc -Wall -g -o mydriver-pico mydriver-pico.c -lusb-1.0

readpcapng-stlink: readpcapng-stlink.c dapreg.o armreg.o pico-decode.o util.o dap-decode.o *.h $M
	gcc -Wall -g -o readpcapng-stlink readpcapng-stlink.c dapreg.o armreg.o dap-decode.o \
            pico-decode.o util.o -lm

driver-cmsis: driver-cmsis.c driver-pico.o dapreg.o armreg.o util.o test.o \
              cortex.o disasm.o dump.o dap-decode.o *.h jim-interface-cmsis.o common-driver.o $M
	gcc -Wall -g -o driver-cmsis driver-cmsis.c dap-decode.o \
        test.c util.o cortex.o dapreg.o armreg.o disasm.o dump.o driver-pico.c common-driver.o \
        jim-interface-cmsis.o jimtcl/libjim.a -lz \
        $(LIBUSB)
#
#	gcc -Wall -g -o driver-cmsis driver-cmsis.c /cygdrive/c/programs/openocd-v0.12.0/bin/libusb-1.0.dll
#	gcc -Wall -g -o driver-cmsis driver-cmsis.c cygusb_64-1.0.dll 
#	gcc -Wall -g -o driver-cmsis driver-cmsis.c util.o -lusb-1.0 
#	gcc -Wall -g -o driver-cmsis driver-cmsis.c /cygdrive/c/Users/V.Giargia/AppData/Local/Packages/PythonSoftwareFoundation.Python.3.10_qbz5n2kfra8p0/LocalCache/local-packages/Python310/site-packages/libusb_package/libusb-1.0.dll 

readlog: readlog.c util.o dap-decode.o dapreg.o armreg.o $M
	gcc -Wall -g -o readlog readlog.c util.o dap-decode.o dapreg.o armreg.o

*.o: *.h

clean:
	rm *.exe *.o
