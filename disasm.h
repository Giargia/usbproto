#ifndef _DISASM_H
#define _DISASM_H

#define DEC_FUN   32
#define DEC_FIELD 20
#define NFIELDS  20
#define NEFIELDS  6

#define SP 13
#define PC 15

#if defined(DO_CHK)
#define CHK(s,p,n,l) chk(s,p,n,l)
#else
#define CHK(s,p,n,l)
#endif

#include "disasm_env.h"

struct t_decf  // field definition
{
  int    len;
  int    pos;
  int    sig;
  int    sw;
  char   *name;
  char   *desc;
};

struct t_decfun     // decode functions
{
  uint   ncall;
  uint   inst;
  int    (*pexec)(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_d);
  int    (*pdis )(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_d);
  char   *fmtinst;
  char   *desc;
};

struct t_decm  // decode map
{
  int    il;
  int    dt;
  int    dt4;
  int    extra;
};

struct t_dec
{
  int    nf,nfunc;
  uint   ncall;
  struct t_decfun func[DEC_FUN];
  struct t_decf   f[DEC_FIELD];
  int    (*pfd)(struct t_cpu *p_cpu,struct t_stat *p_st,int noerr);
  char   *desc;
};

#ifdef EXTERN
#else
#define EXTERN extern
#endif

EXTERN struct t_decm decmt[1024];
EXTERN struct t_dec
  dect[21],
  dect4[9],
// second level
  dectdp[16],
  dectas[8],
  dectmv[2],
  dectbf[10],
  dectdpcs[16],
  dectsoz[12],
  dectmis[6],   // move, and immediate shift instructions tab 3.26
  dectsimd[36], // Table 3-29 SIMD instructions
  dectother[10],
  dect64md[10],
  dectlsetb[8],
  dect32md[16],
  dectrcsh[4];
EXTERN int  mask[32],masks[32];
EXTERN int  condt[256];
EXTERN char *condmem[16];
#undef EXTERN

char *fmtfname(struct t_cpu *p_cpu,void *p,char *out);
void initm();
void initcondt();
int  VFPExpandImm32(int imm);
int  ThumbExpandImmWithC(int imm,int *C);
int  ThumbExpandImm(int v);
int  DecodeImmShift(int ty,int imm5,int *t);
void initdec(struct t_cpu *p_cpu);
int  findpos      (struct t_dec *pd,char *f);
int  deco_misc    (struct t_cpu *p_cpu,struct t_stat *p_st,int noerr);
int  deco_ls      (struct t_cpu *p_cpu,struct t_stat *p_st,int noerr);
int  deco_lsdet   (struct t_cpu *p_cpu,struct t_stat *p_st,int noerr);
int  deco_nimm    (struct t_cpu *p_cpu,struct t_stat *p_st,int noerr);
int  deco_movsh   (struct t_cpu *p_cpu,struct t_stat *p_st,int noerr);
int  deco_branch  (struct t_cpu *p_cpu,struct t_stat *p_st,int noerr);
int  deco_fpdata  (struct t_cpu *p_cpu,struct t_stat *p_st,int noerr);
int  deco_fpext   (struct t_cpu *p_cpu,struct t_stat *p_st,int noerr);
struct t_dec *decode(struct t_cpu *p_cpu,struct t_stat *p_st,int noerr);

#endif
