#ifndef _UTIL_H
#define _UTIL_H

struct buf_env
{
  char *buf;
  int  lbuf;
  int  lpre;
  int  lmax;
};

int    get32(uchar *p);
int    get32p(uchar *pb,int *p);
int    get16(uchar *p);
int    set16(uchar *p,int v);
int    set32(uchar *p,int v);
int    rev(int v);
char   *tobin(int v,int n,char *buf);
uint   nbit(uint v);
double dtime();
void   memdump(uchar *add,int len,char *pre,int fl);
void   dumpmem(uchar *add,size_t len,uchar *off);
void   *myalloc(size_t s);
void   *mycalloc(size_t s,size_t n);
void   myfree(void *p);
void   error(char *e,...);
void   warning(char *e,...);
void   info(char *e,...);
int    util_count();

#endif
