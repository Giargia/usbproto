#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <libusb-1.0/libusb.h>
#include "driversub.h"
#include "chipid.h"
#include "conf.h"
#include "disasm.h"
#include "dump.h"
#include "repl.h"

static char *pre = ">> ";

struct t_cmd
{
  char *name;
  char *fmt;
  int (*fcmd)(stlink_t* sl,struct t_cmd *cmd,char *line);
};

static int preg_cmd(stlink_t* sl,struct t_cmd *cmd,char *line)
{
  int  n;
  uint flag;
  char desc[100];

  printf("%spreg_cmd %s",pre,line);
  n = sscanf(line,cmd->fmt,&flag,desc);
  printf("%sn %d flag %x desc %s\n",pre,n,flag,desc);
  if (n == 2)
    return dumpreg(sl,flag,desc);
  printf("%serror in preg_cmd line %s",pre,line);
  return 1;
}

static int exit_cmd(stlink_t* sl,struct t_cmd *cmd,char *line)
{
  printf("%sread_cmd %s",pre,line);
  exit(0);
  return 1;
}

static int read_cmd(stlink_t* sl,struct t_cmd *cmd,char *line)
{
  int  n;
  uint add,size;
  char fn[260];

  printf("%sread_cmd %s",pre,line);
  fn[0] = 0;
  n = sscanf(line,cmd->fmt,&add,&size,fn);
  printf("%sn %d add %x len %d(0x%x) fn %s\n",pre,n,add,size,size,fn);
  if (n == 3)
    return exec_read(sl,add,size,fn);
  else if (n == 2)
    return exec_read(sl,add,size,NULL);
  printf("%serror in read_cmd line %s",pre,line);
  return 1;
}

static int dump_cmd(stlink_t* sl,struct t_cmd *cmd,char *line)
{
  int  n;
  uint add,size;

  printf("%sdump_cmd %s",pre,line);
  n = sscanf(line,cmd->fmt,&add,&size);
  printf("%sn %d add %x len %d(0x%x)\n",pre,n,add,size,size);
  if (n == 2)
    return exec_read(sl,add,size,0);
  printf("%serror in dump_cmd line %s",pre,line);
  return 1;
}

static int break_cmd(stlink_t* sl,struct t_cmd *cmd,char *line)
{
  int  n,id;
  uint add;

  printf("%sbreak_cmd %s",pre,line);
  n = sscanf(line,cmd->fmt,&add,&id);
  printf("%sn %d add %x id %d\n",pre,n,add,id);
  if (n == 2)
    return breakpoint(sl,id,add);
  printf("%serror in break_cmd line %s",pre,line);
  return 1;
}

static int cmp(const void *p,const void *q)
{
  const uint *a,*b;
  a = (const uint *) p;
  b = (const uint *) q;
  return *a - *b;
}

static int run_cmd(stlink_t* sl,struct t_cmd *cmd,char *line)
{
  int  n;
  uint wtime,count;
  struct t_stat st,*p_st;
  struct t_cpu  cpu  = {0},*p_cpu;

  p_st = &st;
  p_cpu = &cpu;
  printf("%srun_cmd %s",pre,line);
  n = sscanf(line,cmd->fmt,&wtime,&count);
  printf("%sn %d wtime %d count %d\n",
         pre,n,wtime,count);
  if (n == 2)
  {
    uint *mem = malloc(count * sizeof(uint));
    int r = runcpu(sl,wtime,count,mem,1);
    qsort(mem,count,sizeof(mem[0]),cmp);
    int i,j = 0;
/*
    for (i = 0; i < count; i++)
      printf("X %2d %08x\n",i,mem[i]);
*/
    for (i = 1; i < count; i++)
      if (mem[i] != mem[i - 1])
      {
        printf("[%4d] ",i - j);
        disasm(sl,p_cpu,mem[i - 1],p_st);
        putchar('\n');
        j = i;
      }
    printf("[%4d] ",i - j);
    disasm(sl,p_cpu,mem[i - 1],p_st);
    putchar('\n');
    free(mem);
    return r;
  }
  printf("%serror in run_cmd line %s",pre,line);
  return 1;
}

static int trace_cmd(stlink_t* sl,struct t_cmd *cmd,char *line)
{
  int  n;
  uint nstep,flag,start;

  printf("%strace_cmd %s",pre,line);
  n = sscanf(line,cmd->fmt,&nstep,&flag,&start);
  printf("%sn %d nstep %d flag %x start %d\n",
         pre,n,nstep,flag,start);
  if (n == 3)
    return trace(sl,nstep,flag,start);
  printf("%serror in trace_cmd line %s",pre,line);
  return 1;
}

static int conn_cmd(stlink_t* sl,struct t_cmd *cmd,char *line)
{
  int  n;
  uint flag,deb;

  printf("%sconn_cmd %s",pre,line);
  n = sscanf(line,cmd->fmt,&flag,&deb);
  printf("%sn %d flag %d deb %d\n",pre,n,flag,deb);
  if (n == 2)
  {
    return connect(sl,flag,deb);
  } else
    printf("%serror in conn_cmd line %s",pre,line);
  return 1;
}

static int fill_cmd(stlink_t* sl,struct t_cmd *cmd,char *line)
{
  int  n;
  uint p,s,l;

  printf("%sfill_cmd %s",pre,line);
  n = sscanf(line,cmd->fmt,&p,&s,&l);
  printf("%sn %d p %x s %x l %d\n",pre,n,p,s,l);
  if (n == 3)
  {
    return fillmem(sl,p,s,l);
  } else
    printf("%serror in fill_cmd line %s",pre,line);
  return 1;
}

static int load_cmd(stlink_t* sl,struct t_cmd *cmd,char *line)
{
  int  n;
  char fn[256];
  int  pc,sp;

  printf("%sload_cmd %s",pre,line);
  n = sscanf(line,cmd->fmt,&fn,&pc,&sp);
  printf("%sn fn %s pc %x sp %x\n",pre,fn,pc,sp);
  if (n == 3)
  {
    return loadprg(sl,fn,0x20000000,pc,sp,1);
  } else
    printf("%serror in fill_cmd line %s",pre,line);
  return 1;
}

static struct t_cmd ctab[] = 
{
  {"fill",   "%x %x %i",  fill_cmd },
  {"trace",  "%u %x %u",  trace_cmd},
  {"connect","%d %d",     conn_cmd },
  {"preg",   "%x %s",     preg_cmd },
  {"read",   "%x %i %s",  read_cmd},
  {"dump",   "%x %i",     dump_cmd},
  {"run",    "%d %d",     run_cmd},
  {"load",   "%s %x %x",  load_cmd},
  {"break",  "%x %d",     break_cmd},
  {"exit",   "",          exit_cmd},
  {0}
};

int repl(stlink_t* sl,FILE *pf)
{
  int  nl = 0;
  int  n,l;
  char line[1024],*p;
  char cmd[40];
  
  while((p = fgets(line,sizeof line,pf)))
  {
    nl++;
    printf("%s%4d: %s",pre,nl,p);
    while (*p == ' ' || *p == '\t') p++;
    if (*p == '#') // comment
      continue;
    if (*p == '\n') // empty line
      continue;
    cmd[0] = 0;
    n = sscanf(p,"%s%n",cmd,&l);
    printf("%sn %d l %d cmd %s\n",pre,n,l,cmd);
    if (n == 1)
    {
      int i;
      for (i = 0; ctab[i].name && strcmp(cmd,ctab[i].name); i++);
      if (ctab[i].name && ctab[i].fcmd)
      {
        int r = ctab[i].fcmd(sl,ctab + i,p + l + 1);
        CHKERR(r,"fcmd");
        continue;
      } else
      {
        printf("%sunknown command %s at line %d: %s\n",pre,cmd,nl,line);
        exit(0);
      }
    } else
    {
      printf("%srepl error: line %d: %s",pre,nl,line);
      exit(0);
    }
  }
  return 0;
}
