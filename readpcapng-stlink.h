#ifdef __INTER
#define FILE int
typedef int  time_t;

struct tm 
{
  int tm_sec;         /* seconds */
  int tm_min;         /* minutes */
  int tm_hour;        /* hours */
  int tm_mday;        /* day of the month */
  int tm_mon;         /* month */
  int tm_year;        /* year */
  int tm_wday;        /* day of the week */
  int tm_yday;        /* day in the year */
  int tm_isdst;       /* daylight saving time */
};

#else
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#endif
typedef unsigned char uchar;

#ifndef SEEK_SET
#define SEEK_SET        0       /* set file offset to offset */
#endif
#ifndef SEEK_CUR
#define SEEK_CUR        1       /* set file offset to current plus offset */
#endif
#ifndef SEEK_END
#define SEEK_END        2       /* set file offset to EOF plus offset */
#endif

#define PICO      1
#define STLINK    2
#define CMSIS_DAP 3
#define NPROTO    4

#define MB 0xff
#define MW 0xffff
#define NSW 10

enum PROBE_CMDS 
{
  PROBE_INVALID      = 0, // Invalid command
  PROBE_WRITE_BITS   = 1, // Host wants us to write bits
  PROBE_READ_BITS    = 2, // Host wants us to read bits
  PROBE_SET_FREQ     = 3, // Set TCK
  PROBE_RESET        = 4, // Reset all state
  PROBE_TARGET_RESET = 5, // Reset target
};

struct t_ses
{
  int nrec;
  double tstart,tend;
};

// #define STLINK_DEV_DFU_MODE             0x00
#define STLINK_DEV_MASS_MODE               0x01
#define STLINK_DEV_DEBUG_MODE              0x02

// symbol definition from openOCD source code 
// https://sourceforge.net/p/openocd/code/ci/master/tree/src/jtag/drivers/stlink_usb.c
// 
#define STLINK_SWIM_ENTER                  0x00
#define STLINK_SWIM_EXIT                   0x01
#define STLINK_SWIM_READ_CAP               0x02
#define STLINK_SWIM_SPEED                  0x03
#define STLINK_SWIM_ENTER_SEQ              0x04
#define STLINK_SWIM_GEN_RST                0x05
#define STLINK_SWIM_RESET                  0x06
#define STLINK_SWIM_ASSERT_RESET           0x07
#define STLINK_SWIM_DEASSERT_RESET         0x08
#define STLINK_SWIM_READSTATUS             0x09
#define STLINK_SWIM_WRITEMEM               0x0a
#define STLINK_SWIM_READMEM                0x0b
#define STLINK_SWIM_READBUF                0x0c

#define STLINK_DEBUG_GETSTATUS             0x01
#define STLINK_DEBUG_FORCEDEBUG            0x02
#define STLINK_DEBUG_APIV1_RESETSYS        0x03
#define STLINK_DEBUG_APIV1_READALLREGS     0x04
#define STLINK_DEBUG_APIV1_READREG         0x05
#define STLINK_DEBUG_WRITEREG              0x06
#define STLINK_DEBUG_APIV1_WRITEREG        0x06
#define STLINK_DEBUG_READMEM_32BIT         0x07
#define STLINK_DEBUG_WRITEMEM_32BIT        0x08
#define STLINK_DEBUG_RUNCORE               0x09
#define STLINK_DEBUG_STEPCORE              0x0a
#define STLINK_DEBUG_APIV1_SETFP           0x0b
#define STLINK_DEBUG_READMEM_8BIT          0x0c
#define STLINK_DEBUG_WRITEMEM_8BIT         0x0d
#define STLINK_DEBUG_APIV1_CLEARFP         0x0e
#define STLINK_DEBUG_APIV1_WRITEDEBUGREG   0x0f
#define STLINK_DEBUG_APIV1_SETWATCHPOINT   0x10

#define STLINK_DEBUG_ENTER_JTAG_RESET      0x00
#define STLINK_DEBUG_ENTER_SWD_NO_RESET    0xa3
#define STLINK_DEBUG_ENTER_JTAG_NO_RESET   0xa4

#define STLINK_DEBUG_APIV1_ENTER           0x20
#define STLINK_DEBUG_EXIT                  0x21
#define STLINK_DEBUG_READCOREID            0x22

#define STLINK_DEBUG_APIV2_ENTER           0x30
#define STLINK_DEBUG_APIV2_READ_IDCODES    0x31
#define STLINK_DEBUG_APIV2_RESETSYS        0x32
#define STLINK_DEBUG_APIV2_READREG         0x33
#define STLINK_DEBUG_APIV2_WRITEREG        0x34
#define STLINK_DEBUG_APIV2_WRITEDEBUGREG   0x35
#define STLINK_DEBUG_APIV2_READDEBUGREG    0x36

#define STLINK_DEBUG_APIV2_READALLREGS     0x3a
#define STLINK_DEBUG_APIV2_GETLASTRWSTATUS 0x3b
#define STLINK_DEBUG_APIV2_DRIVE_NRST      0x3c

#define STLINK_DEBUG_APIV2_GETLASTRWSTATUS2 0x3e

#define STLINK_DEBUG_APIV2_START_TRACE_RX  0x40
#define STLINK_DEBUG_APIV2_STOP_TRACE_RX   0x41
#define STLINK_DEBUG_APIV2_GET_TRACE_NB    0x42
#define STLINK_DEBUG_APIV2_SWD_SET_FREQ    0x43
#define STLINK_DEBUG_APIV2_JTAG_SET_FREQ   0x44
#define STLINK_DEBUG_APIV2_READ_DAP_REG    0x45
#define STLINK_DEBUG_APIV2_WRITE_DAP_REG   0x46
#define STLINK_DEBUG_APIV2_READMEM_16BIT   0x47
#define STLINK_DEBUG_APIV2_WRITEMEM_16BIT  0x48

#define STLINK_DEBUG_APIV2_INIT_AP         0x4b
#define STLINK_DEBUG_APIV2_CLOSE_AP_DBG    0x4c
#define STLINK_DEBUG_ENTER_SWD             0xa3

#define STLINK_APIV3_SET_COM_FREQ           0x61
#define STLINK_APIV3_GET_COM_FREQ           0x62

#define STLINK_APIV3_GET_VERSION_EX         0xfb

#define STLINK_DEBUG_APIV2_DRIVE_NRST_LOW   0x00
#define STLINK_DEBUG_APIV2_DRIVE_NRST_HIGH  0x01
#define STLINK_DEBUG_APIV2_DRIVE_NRST_PULSE 0x02

#define STLINK_DEBUG_PORT_ACCESS            0xffff

#define STLINK_TRACE_SIZE               4096
#define STLINK_TRACE_MAX_HZ             2000000
#define STLINK_V3_TRACE_MAX_HZ          24000000

#define STLINK_V3_MAX_FREQ_NB               10

#define REQUEST_SENSE        0x03
#define REQUEST_SENSE_LENGTH 18

#define STLINK_SWIM_ERR_OK             0x00
#define STLINK_SWIM_BUSY               0x01
#define STLINK_DEBUG_ERR_OK            0x80
#define STLINK_DEBUG_ERR_FAULT         0x81
#define STLINK_SWD_AP_WAIT             0x10
#define STLINK_SWD_AP_FAULT            0x11
#define STLINK_SWD_AP_ERROR            0x12
#define STLINK_SWD_AP_PARITY_ERROR     0x13
#define STLINK_JTAG_GET_IDCODE_ERROR   0x09
#define STLINK_JTAG_WRITE_ERROR        0x0c
#define STLINK_JTAG_WRITE_VERIF_ERROR  0x0d
#define STLINK_SWD_DP_WAIT             0x14
#define STLINK_SWD_DP_FAULT            0x15
#define STLINK_SWD_DP_ERROR            0x16
#define STLINK_SWD_DP_PARITY_ERROR     0x17

#define STLINK_SWD_AP_WDATA_ERROR      0x18
#define STLINK_SWD_AP_STICKY_ERROR     0x19
#define STLINK_SWD_AP_STICKYORUN_ERROR 0x1a

#define STLINK_BAD_AP_ERROR            0x1d

#define STLINK_CORE_RUNNING            0x80
#define STLINK_CORE_HALTED             0x81
#define STLINK_CORE_STAT_UNKNOWN       -1

#define STLINK_GET_VERSION             0xf1
#define STLINK_DEBUG_COMMAND           0xf2
#define STLINK_DFU_COMMAND             0xf3
#define STLINK_DFU_EXIT                0x07
#define STLINK_SWIM_COMMAND            0xf4
#define STLINK_GET_CURRENT_MODE        0xf5
#define STLINK_GET_TARGET_VOLTAGE      0xf7

#define STLINK_DEV_DFU_MODE            0x00


/*
 * Map the relevant features, quirks and workaround for specific firmware
 * version of stlink
 */
#define STLINK_F_HAS_TRACE              BIT(0)
#define STLINK_F_HAS_SWD_SET_FREQ       BIT(1)
#define STLINK_F_HAS_JTAG_SET_FREQ      BIT(2)
#define STLINK_F_HAS_MEM_16BIT          BIT(3)
#define STLINK_F_HAS_GETLASTRWSTATUS2   BIT(4)
#define STLINK_F_HAS_DAP_REG            BIT(5)
#define STLINK_F_QUIRK_JTAG_DP_READ     BIT(6)
#define STLINK_F_HAS_AP_INIT            BIT(7)
#define STLINK_F_HAS_DPBANKSEL          BIT(8)
#define STLINK_F_HAS_RW8_512BYTES       BIT(9)
#define STLINK_F_FIX_CLOSE_AP           BIT(10)

struct t_decode
{
  int  op,op1;
  int  contlen;
  int  regadd;
  int  logh;     // output header
  void *priv;    // decoder private part
};

struct t_switch
{
  int blocktype;
  int ncall;
  int (*handler)(FILE *pf,int len,struct t_decode *pdec);
};

struct t_shb
{
  int   bom;
  short major,minor;
  int   sl[2];
};

struct t_idb
{
  short lt,res;
  int snaplen;
};

struct t_epb
{
  int iid;
  int tsh;
  int tsl;
  int cpl;
  int opl;
};

#define NADD 100

struct t_add
{
  int    dir,bus,dev,ep;
  int    count;
  int    setup;
  int    desc;
  int    othersetup;
  int    decode;
  ushort vendor;
  ushort product;
};

struct t_cmd
{
  int  cmd;
  int  l;
  int  fmt;
  char *desc;
};

#define URB_FUNCTION_SELECT_CONFIGURATION 0x0000
#define URB_FUNCTION_SELECT_INTERFACE 0x0001
#define URB_FUNCTION_ABORT_PIPE 0x0002
#define URB_FUNCTION_TAKE_FRAME_LENGTH_CONTROL 0x0003
#define URB_FUNCTION_RELEASE_FRAME_LENGTH_CONTROL 0x0004
#define URB_FUNCTION_GET_FRAME_LENGTH 0x0005
#define URB_FUNCTION_SET_FRAME_LENGTH 0x0006
#define URB_FUNCTION_GET_CURRENT_FRAME_NUMBER 0x0007
#define URB_FUNCTION_CONTROL_TRANSFER 0x0008
#define URB_FUNCTION_BULK_OR_INTERRUPT_TRANSFER 0x0009
#define URB_FUNCTION_ISOCH_TRANSFER 0x000a
#define URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE 0x000b
#define URB_FUNCTION_SET_DESCRIPTOR_TO_DEVICE 0x000c
#define URB_FUNCTION_SET_FEATURE_TO_DEVICE 0x000d
#define URB_FUNCTION_SET_FEATURE_TO_INTERFACE 0x000e
#define URB_FUNCTION_SET_FEATURE_TO_ENDPOINT 0x000f
#define URB_FUNCTION_CLEAR_FEATURE_TO_DEVICE 0x0010
#define URB_FUNCTION_CLEAR_FEATURE_TO_INTERFACE 0x0011
#define URB_FUNCTION_CLEAR_FEATURE_TO_ENDPOINT 0x0012
#define URB_FUNCTION_GET_STATUS_FROM_DEVICE 0x0013
#define URB_FUNCTION_GET_STATUS_FROM_INTERFACE 0x0014
#define URB_FUNCTION_GET_STATUS_FROM_ENDPOINT 0x0015
#define URB_FUNCTION_RESERVED_0X0016 0x0016
#define URB_FUNCTION_VENDOR_DEVICE 0x0017
#define URB_FUNCTION_VENDOR_INTERFACE 0x0018
#define URB_FUNCTION_VENDOR_ENDPOINT 0x0019
#define URB_FUNCTION_CLASS_DEVICE 0x001a
#define URB_FUNCTION_CLASS_INTERFACE 0x001b
#define URB_FUNCTION_CLASS_ENDPOINT 0x001c
#define URB_FUNCTION_RESERVE_0X001D 0x001d
#define URB_FUNCTION_SYNC_RESET_PIPE_AND_CLEAR_STALL 0x001e
#define URB_FUNCTION_CLASS_OTHER 0x001f
#define URB_FUNCTION_VENDOR_OTHER 0x0020
#define URB_FUNCTION_GET_STATUS_FROM_OTHER 0x0021
#define URB_FUNCTION_CLEAR_FEATURE_TO_OTHER 0x0022
#define URB_FUNCTION_SET_FEATURE_TO_OTHER 0x0023
#define URB_FUNCTION_GET_DESCRIPTOR_FROM_ENDPOINT 0x0024
#define URB_FUNCTION_SET_DESCRIPTOR_TO_ENDPOINT 0x0025
#define URB_FUNCTION_GET_CONFIGURATION 0x0026
#define URB_FUNCTION_GET_INTERFACE 0x0027
#define URB_FUNCTION_GET_DESCRIPTOR_FROM_INTERFACE 0x0028
#define URB_FUNCTION_SET_DESCRIPTOR_TO_INTERFACE 0x0029
#define URB_FUNCTION_GET_MS_FEATURE_DESCRIPTOR 0x002a
#define URB_FUNCTION_RESERVE_0X002B 0x002b
#define URB_FUNCTION_RESERVE_0X002C 0x002c
#define URB_FUNCTION_RESERVE_0X002D 0x002d
#define URB_FUNCTION_RESERVE_0X002E 0x002e
#define URB_FUNCTION_RESERVE_0X002F 0x002f
#define URB_FUNCTION_SYNC_RESET_PIPE 0x0030
#define URB_FUNCTION_SYNC_CLEAR_STALL 0x0031
#if _WIN32_WINNT >= 0x0600
#define URB_FUNCTION_CONTROL_TRANSFER_EX 0x0032
#define URB_FUNCTION_RESERVE_0X0033 0x0033
#define URB_FUNCTION_RESERVE_0X0034 0x0034
#endif

int  rev(int v);
void relineheader();
int  get32(uchar *p);
int  get32p(uchar *pb,int *p);

void decodestlink(uchar *pd,int len,struct t_decode *pdec,int transfer);
void decoderepstlink(uchar *pd,int len,struct t_decode *pdec,int transfer,int);
