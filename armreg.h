#ifndef _ARMREG_H_
#define _ARMREG_H_

#define NREG 500

#define RO 1
#define WO 0
#define RW 2

// Debug Halting Control and Status Register
#define DHCSR_ADD                 0xe000edf0
#define DHCSR_DBGKEY              (0xa05f << 16)
#define DHCSR_DBGKEY_MASK         (0xffff << 16)
#define DHCSR_C_DEBUGEN           (1 << 0)
#define DHCSR_C_HALT              (1 << 1)
#define DHCSR_C_STEP              (1 << 2)
#define DHCSR_C_MASKINTS          (1 << 3)
#define DHCSR_C_SNAPSTALL         (1 << 5)
#define DHCSR_C_PMOV              (1 << 6)
#define DHCSR_S_REGRDY            (1 << 16)
#define DHCSR_S_HALT              (1 << 17)
#define DHCSR_S_SLEEP             (1 << 18)
#define DHCSR_S_LOCKUP            (1 << 19)
#define DHCSR_S_RETIRE_ST         (1 << 24)
#define DHCSR_S_RESET_ST          (1 << 25)

#define CPUID_ADD                 0xe000ed00
#define CPUID_IMPL_SHIFT          24           
#define CPUID_IMPL_MASK           (0xff << CPUID_IMPL_SHIFT)
#define CPUID_VARIANT_SHIFT       20
#define CPUID_VARIANT_MASK        (0xf << CPUID_VARIANT_SHIFT)
#define CPUID_CONSTANT_SHIFT      16
#define CPUID_CONSTANT_MASK       (0xf << CPUID_CONSTANT_SHIFT)
#define CPUID_PARTNO_SHIFT        4
#define CPUID_PARTNO_MASK         (0xfff << CPUID_PARTNO_SHIFT)
#define CPUID_REVISION_SHIFT      0
#define CPUID_REVISION_MASK       (0xf << CPUID_REVISION_SHIFT)

#define DEMCR_ADD                 0xe000edfc
#define DEMCR_TRCENA              (1 << 24)
#define DEMCR_VC_SFERR            (1 << 11)
#define DEMCR_VC_HARDERR          (1 << 10)
#define DEMCR_VC_INTERR           (1 << 9)
#define DEMCR_VC_BUSERR           (1 << 8)
#define DEMCR_VC_STATERR          (1 << 7)
#define DEMCR_VC_CHKERR           (1 << 6)
#define DEMCR_VC_NOCPERR          (1 << 5)
#define DEMCR_VC_MMERR            (1 << 4)
#define DEMCR_VC_CORERESET        (1 << 0)

#define DWT_CTRL_ADD              0xe0001000
#define DWT_CTRL_NUM_COMP_MASK    (0xF << 28)
#define DWT_CTRL_NUM_COMP_SHIFT   28
#define DWT_CTRL_CYCEVTENA_MASK   (1 << 22)
#define DWT_CTRL_FOLDEVTENA_MASK  (1 << 21)
#define DWT_CTRL_LSUEVTENA_MASK   (1 << 20)
#define DWT_CTRL_SLEEPEVTENA_MASK (1 << 19)
#define DWT_CTRL_EXCEVTENA_MASK   (1 << 18)
#define DWT_CTRL_CPIEVTENA_MASK   (1 << 17)
#define DWT_CTRL_EXCTRCENA_MASK   (1 << 16)
#define DWT_CTRL_PCSAMPLENA_MASK  (1 << 12)
#define DWT_CTRL_SYNCTAP_MASK     (0x3 << 10)
#define DWT_CTRL_SYNCTAP_SHIFT    10
#define DWT_CTRL_CYCTAP_MASK      (1 << 9)
#define DWT_CTRL_POSTINIT_MASK    (0xF << 5)
#define DWT_CTRL_POSTINIT_SHIFT   5
#define DWT_CTRL_POSTRESET_MASK   (0xF << 1)
#define DWT_CTRL_POSTRESET_SHIFT  1
#define DWT_CTRL_CYCCNTENA_MASK   (1 << 0)
#define DWT_COMPn(n)              (0xe0001020 + n*16)
#define DWT_MASKn(n)              (0xe0001024 + n*16)
#define DWT_FUNn(n)               (0xe0001028 + n*16)

#define DWT_DEVARCH_ADD           0xe0001fbc

#define FPCCR_ADD                 0xe000ef34
#define FPCAR_ADD                 0xe000ef38
#define FPDSCR_ADD                0xe000ef3c
#define MVFR0_ADD                 0xe000ef40
#define MVFR1_ADD                 0xe000ef44

#define FP_CTRL_ADD               0xe0002000
#define FP_CTRL_KEY_MASK          (1 << 1)
#define FP_CTRL_KEY_SHIFT         1 
#define FP_CTRL_EN_MASK           1
#define FP_CTRL_EN_SHIFT          0 
#define FP_CTRL_NUM_CODE1_MASK    (0xf << 4)
#define FP_CTRL_NUM_CODE1_MASK    (0xf << 4)
#define FP_CTRL_NUM_CODE1_SHIFT   4
#define FP_CTRL_NUM_LIT_MASK      (0xf << 8)
#define FP_CTRL_NUM_LIT_SHIFT     8
#define FP_CTRL_NUM_CODE2_MASK    (0x3 << 4)
#define FP_CTRL_NUM_CODE2_SHIFT   12 
#define FP_CTRL_REV_MASK          0xf0000000
#define FP_CTRL_REV_SHIFT         28

#define FP_REMAP_ADD              0xe0002004

#define FP_COMPn_ADD(n)           (0xe0002008 + n*4)
#define FP_COMP_ENABLE            1 
#define FP_COMP_MATCH_SHIFT       30 
#define FP_COMP_MATCH_MASK        (3 << 30) 
#define FP_COMP_MATCH_NO          (0 << 30)
#define FP_COMP_MATCH_LOW         (1 << 30)
#define FP_COMP_MATCH_UP          (2 << 30)
#define FP_COMP_MATCH_BOTH        (3 << 30)
#define FP_COMP_MATCH_MASK        (3 << 30)
#define FP_COMP_COMP_MASK         0x1ffffffc
#define DFSR_ADD                  0xe000ed30
#define DFSR_EXTERNAL             (1 << 4)
#define DFSR_VCATCH               (1 << 3)
#define DFSR_DWTTRAP              (1 << 2)
#define DFSR_BKPT                 (1 << 1)
#define DFSR_HALTED               (1 << 0)
// Debug Core Register Selector Register
#define DCRSR_ADD                 0xe000edf4
#define DCRSR_REGWnR              (1 << 16)
#define DCRSR_REGSEL              0x1f
// values:
//   0..12   R00 .. R12
//  13       R13    SP
//  14       R14    LR
//  15       Debug return address
//  16       xPSR
//  17       MSP
//  18       PSP
//  20       31:24 CONTROL 23:16 FAULTMASK 15:8 BASEPRI 7:0 PRIMASK
//  33       FPCSR
//  64..05   S0 .. S31

#define DCRSR_REGSEL_R0           0
#define DCRSR_REGSEL_XPSR         16
#define DCRSR_REGSEL_MSP          17
#define DCRSR_REGSEL_PSP          18
#define DCRSR_REGSEL_CONTROL      20
#define DCRSR_REGSEL_FPCSR        33
#define DCRSR_REGSEL_S0           64 

// Debug Core Register Data Register
#define DCRDR_ADD                 0xe000edf8

// NVIC
#define AIRCR_ADD                 0xe000ed0c
#define AIRCR_VECTKEY             (0x5fa << 16)
#define AIRCR_VECTRESET           (1 << 0)
#define AIRCR_VECTCLRACTIVE       (1 << 1)
#define AIRCR_SYSRESETREQ         (1 << 2)
#define AIRCR_PRIGROUP_MASK       0x700
#define AIRCR_PRIGROUP_SHIFT      8

struct t_reg
{
  uint  add;
  char  *name;
  uint  valr,valw,val;
  uint  countr,countw;
  uint  where1,wheren;
  char  *(* fdump)(struct t_reg *,char *buf,int lbuf,int fl,int op,int lmax,int lpre);
  uchar cache_v;
};

struct t_cpu;

extern struct t_reg treg[NREG];
extern int    nreg;

void   addregex(char *name,uint add,char *(*fdump)(struct t_reg *,char *,int lb,int fl,int op,int lmax,int lpre));
void   addreg(char *name,uint add);
char   *dumpdcrsr(struct t_reg *pr,char *buf,int lb,int fl,int op,int lmax,int lpre);
char   *dumpdhcsr(struct t_reg *pr,char *buf,int lb,int fl,int op,int lmax,int lpre);
char   *dumpdfsr(struct t_reg *pr,char *buf,int lb,int fl,int op,int lmax,int lpre);
char   *dumpdemcr(struct t_reg *pr,char *buf,int lb,int fl,int op,int lmax,int lpre);
char   *dumprom(struct t_reg *pr,char *buf,int lb,int fl,int op,int lmax,int lpre);
void   arm_initreg(int log);
void   arm_setreg(uint regadd,uint val,int ty,int where,int log);
void   arm_dump_allreg(int fl);
char   *fmtflags(struct t_cpu *p_cpu);
char   *arm_getregname(int add,int fl);
void   arm_dumpreg(uint add,int val,char *buf,int lbuf,uint fl);
struct t_reg *getregadd(int add);
// struct t_reg *getreg(int add);

#endif

