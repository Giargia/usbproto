#ifndef _DUMP_H_
#define _DUMP_H_

// it should work on both 32 and 64 bits platform
#define EFIELD(f) ((void *)(long)(f))

int dump_res     (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_gen     (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_movishift(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_cps     (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_nop     (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_pop     (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_push    (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_asp     (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_it      (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_cmpr3   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_subimm  (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_addsr   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dptstand(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dpmovt  (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dpmovw  (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_orr3    (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_and3    (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_bic3    (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dpornmvn(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dporrmov(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dpadd   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dpcmp   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dpcscmp (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dprsb   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dpsub   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dpcssub (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dpsubcmp(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_subcmpr3(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dpmvn   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dpeor   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dpbic   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_b       (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_bwt3    (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_bwt4    (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_bcond   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_cbz     (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_cbnz    (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_bl      (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_addadrt3(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_adr     (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_ldrio   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_strt2   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_strio   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_ldrlp   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_strsp   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_ldrsp   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_ldm     (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_ls4     (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dpind   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_asind   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_mvind   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_bfind   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_sozind  (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_ubfx    (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_rev     (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_xxtx    (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_ssat    (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_dpcsind (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_rcshind (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_otherind(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_simd    (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_32mdind (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_64mdind (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_lsetbind(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_vstr    (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_vpush   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_vmovi   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_vmovcr  (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_vcvt    (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_vcmp    (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_vmlx    (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_vnmxx   (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_3r      (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_2r      (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);
int dump_1r      (struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f);

#endif
