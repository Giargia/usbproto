#include "env.h"
#include <ctype.h>
#include "util.h"
#include "readpcapng-stlink.h"
#include "dap-decode.h"
#include "dapreg.h"
#include "armreg.h"

struct t_reg treg[NREG];
int    nreg;

struct t_add tadd[NADD];
int    nadd;

static struct t_dap_reg s_dap_reg[DAP_NREG];
static int num_line = 1;
static int out_out  = 1;

void usage()
{
  printf("readlog [switches] log-file\n");
  exit(0);
}

int loadbuf(uchar *buf,char *line)
{
//printf("loadbuf %s",line);
  int n = 0;
  int l = 0;
  while (*line && *line != '\n')
  {
    while(*line == ' ') line++;
    n = 0;
    if (isxdigit((int) *line))
    {
      while (isxdigit((int) *line))
      {
        if (isdigit((int) *line))
          n = n * 16 + *line - '0';
        else if (isupper((int) *line))
          n = n * 16 + *line - 'A' + 10;
        else
          n = n * 16 + *line - 'a' + 10;
        line++;
      }
      l++;
      *buf++ = n;
    } else
      break;
  }
//printf("-> %d\n",l);
  return l;
}

void outbuf(uchar *buf,int len,int dir,struct t_decode *pd)
{
  memdump(buf,len,dir == 1 ? "B>> " : "B<< ",1);
  if (dir == 1)
    decodedap(buf,len,pd,1);
  else if (dir == 2)
    decoderepdap(buf,len,pd,1,0);
}

void readlog(FILE *pf)
{
  char line[1000];
  struct t_decode dec = {0};
  char *p;
  uchar buf[1024];
  uchar *pbuf;
  int   dir = 0;
  int   nl = 0;
  int   len = 0;
  
  pbuf = buf;
  initdap(&dec,2);
  dap_set_prefix(&dec,"dap >>","dap <<");
  while (/* nl < 400 && */ (p = fgets(line,sizeof line,pf)))
  {
    nl++;
    if (p[0] == 'B' && (p[1] == '>' || p[1] == '<') &&
                       (p[2] == '>' || p[2] == '<'))
    {
      if (strstr(p + 4,"dumpbuf"))
      {
        if (len)
          outbuf(buf,len,dir,&dec);
        len = 0;
//      printf("%s",p);
        continue;
      } else if (p[1] == '<')
        dir = 2;
      else if (p[1] == '>')
        dir = 1;
      int off,l;
      int n = sscanf(p + 4,"%x:%n",&off,&l);
      if (n == 1)
        len = off + loadbuf(pbuf + off,p + l + 4);
    } else
    {
      if (len)
        outbuf(buf,len,dir,&dec);
      len = 0;
      if (out_out && num_line)
        printf("%4d: %s",nl,p);
      else if (out_out)
        printf("%s",p);
    }
  }  
  if (len)
    outbuf(buf,len,dir,&dec);
  len = 0;
  enddap(&dec);
}

int main(int na,char **av)
{
  char *fn = 0;

  for (int i = 1; i < na; i++)
  {
    if (av[i][0] == '-')
    {
      if (strcmp(av[i],"-l") == 0)
        num_line = 1;
      if (strcmp(av[i],"-nl") == 0)
        num_line = 0;
      if (strcmp(av[i],"-o") == 0)
        out_out = 1;
      if (strcmp(av[i],"-no") == 0)
        out_out = 0;
    } else if (!fn)
      fn = av[i];
    else
      usage();
  }
    
  if (na < 2)
    usage();
  FILE *pf = fopen(fn,"r");
  if (pf)
  { 
    arm_initreg(0);
    initdapreg(s_dap_reg);
    readlog(pf);
    dap_dump();
    arm_dump_allreg(0);
    fclose(pf);
  } else
    printf("can't open %s\n",av[1]);
  return 0;
}
