#include "env.h"
#include <libusb-1.0/libusb.h>
#include "driversub.h"
#include "chipid.h"
#include "conf.h"
#include "disasm.h"
#include "dump.h"
#include "repl.h"
#include "armreg.h"
#include "util.h"
#include "jim-interface-stlink.h"

/*
#define CHKST(st)    {if (st != 0) {printf("CHKST  %d line %d\n",st,__LINE__); exit(0);}}
#define CHKSTR(st,l) {if (st != 0) {printf("CHKSTR %d line %d\n",st,l); return -1;}}
*/

struct t_reg treg[NREG];
int    nreg;
double start_time;

void logmsg(char *e,...)
{
  char mes[1024];
  va_list args;
  va_start(args,e);
  vsprintf(mes,e,args);
//printf("[%9.6f %4d] %s\n",dtime() - start_time,get_count(),mes);
  printf("[%9.6f] %s\n",dtime() - start_time,mes);
}

void lineheader0()
{
}

int runcpu_(stlink_t* sl)
{
  int err;
  uint dhcsr;
  struct stlink_reg regp;
/*
  err = stlink_run(sl);
  CHKERR(err,"stlink_run");
*/
  dhcsr = readdhcsr(sl,1,__LINE__);
  dhcsr &= ~2;
  writedhcsr(sl,dhcsr,__LINE__);
  dhcsr = readdhcsr(sl,1,__LINE__);
  int n = 0;
  while((dhcsr & 3) != 3)
  {
    usleep(500000);
    int st = stlink_status(sl);
    printf("stlink_status %d %d\n",st,sl->core_stat);
    dhcsr = readdhcsr(sl,0,__LINE__);
    readdwt(sl,1,0,0);
    err = stlink_read_all_regs(sl, &regp);
    CHKERR(err,"stlink_read_all_regs");
    printf("PC  %08x\n",regp.r[15]);
/*
    if (n > 10)
    {
      ret = stlink_force_debug(sl);
      printf("stlink_force_debug %d\n",ret);
    }
*/
    n++;
  }
  return 1;
}

void readrom(stlink_t* sl)
{
  int i;
// read the rom table
    printf("ROM table\n");
    st_dumpmem(sl,0xe00ff000,32);
    st_dumpmem(sl,0xe00fffcc,0xe0100000 - 0xe00fffcc);
    int rom,ret;
    for (i = 0; i < 1024; i += 4)
    {
      int off;
      ret = stlink_read_debug32(sl, 0xe00ff000 + i, (unsigned int *) &rom);
      if (ret == 0)
      {
        off = (rom >> 12) << 12;
        printf("rom at %08x: %08x off %x -> %x valid %d 32bit %d\n",
               0xe00ff000 + i,rom,off,0xe00ff000 + off,rom & 1, (rom >> 1) & 1);
        if (rom == 0) 
          break;
      } else
      {
        printf("ret %d\n",ret);
        break;
      }
    }
    for (i = 0xe00fffcc; i < 0xe0100000; i += 4)
    {
      ret = stlink_read_debug32(sl, i , (unsigned int *) &rom);
      if (ret == 0)
        printf("rom at %08x: %08x\n",0xe00ff000 + i,rom);
      else
        break;    
    }
}

#define RCC_AHB1ENR_ADD (0x40023800 + 0x30)
#define DMA2EN_MASK (1 << 22)
#define DMA1EN_MASK (1 << 21)
/* 
specific stm32f4xx config
- disable dma
*/

int checkf4xx(stlink_t* sl)
{
  uint cpuid;
  uint ahb1enr;
  int  err;

  err = stlink_read_debug32(sl, CPUID_ADD, &cpuid);
  CHKERR(err,"sstlink_read_mem32");
  printf("specific stm32f4xx settings cpuid %08x\n",cpuid);
// check RCC_AHB1ENR for active dma and disable them removing clock
// active dma may run with stopped cpu and destroy ram content
  err = stlink_read_debug32(sl, RCC_AHB1ENR_ADD, &ahb1enr);
  CHKERR(err,"sstlink_read_debug32");

  printf("1) anb1enr %08x\n",ahb1enr);
  if (ahb1enr & (DMA1EN_MASK | DMA2EN_MASK))
  {
    printf("disable DMA\n");
    ahb1enr &= ~(DMA1EN_MASK | DMA2EN_MASK);
    err = stlink_write_debug32(sl, RCC_AHB1ENR_ADD, ahb1enr);
    CHKERR(err,"sstlink_write_debug32");
    err = stlink_read_debug32(sl, RCC_AHB1ENR_ADD, &ahb1enr);
    CHKERR(err,"sstlink_read_debug32");

    printf("2) anb1enr %08x\n",ahb1enr);
  }
  return 0;
}

int main(int na,char **v)
{
//  stlink_t* sl = stlink_open();
  start_time = dtime();
  int  i,log = 0;
  int  traceflag = 0;
  int  n,add = 0,len = 0;
  int  reset = 0;
  int  hot_plug = 0;
  int  maski = 0;
  int  ifault = 0; // intercept fault
//int  rom = 0;
  char *prog = 0;
  char *in = 0;
  uint loadadd,inipc,inisp;
  int  tracecount = 0, run = 1;
  int  err,dhcsr;
  int  chkmem = 1;
  int  doread = 0;
  inipc = inisp = -1;
  int  bradd = 0;
  int  wtime = 500000; // 500ms
  int  tracestart = 0;

  loadadd = 0x20000000;
  printf("mydriver-stlink command line:\n");
  for (i = 1; i < na; i++)
    printf("%s ",v[i]);
  putchar('\n');
  for (i = 1; i < na; i++)
  {
    if (strcmp(v[i],"-in") == 0 && i < na - 1)
    {
      in = v[++i];
    } else if (strcmp(v[i],"-pc") == 0 && i < na - 1)
    {
      n = sscanf(v[++i],"%i",&inipc);
      if (n != 1)
        inipc = -1;
    } else if (strcmp(v[i],"-wtime") == 0 && i < na - 1)
    {
      n = sscanf(v[++i],"%d",&wtime);
      if (n != 1)
        wtime = 500000;
    } else if (strcmp(v[i],"-loadadd") == 0 && i < na - 1)
    {
      n = sscanf(v[++i],"%i",&loadadd);
      if (n != 1)
        loadadd = 0x20000000;
    } else if (strcmp(v[i],"-sp") == 0 && i < na - 1)
    {
      n = sscanf(v[++i],"%i",&inisp);
      if (n != 1)
        inisp = -1;
    } else if (strcmp(v[i],"-bs") == 0 && i < na - 1)
    {
      n = sscanf(v[++i],"%i",&bradd);
      if (n != 1)
        bradd = 0;
    } else if (strcmp(v[i],"-add") == 0 && i < na - 1)
    {
      n = sscanf(v[++i],"%i",&add);
      if (n != 1)
        add = 0;
      else if (len == 0) 
        len = 64;
    } else if (strcmp(v[i],"-len") == 0 && i < na - 1)
    {
      n = sscanf(v[++i],"%i",&len);
      if (n != 1)
        len = add ? 64 : 0;
    } else if (strcmp(v[i],"-read") == 0)
    {
      doread = 1;
    } else if (strcmp(v[i],"-trace") == 0 && i < na - 1)
    {
      n = sscanf(v[++i],"%d",&tracecount);
    } else if (strcmp(v[i],"-tracestart") == 0 && i < na - 1)
    {
      n = sscanf(v[++i],"%d",&tracestart);
    } else if (strcmp(v[i],"-tracef") == 0 && i < na - 1)
    {
      n = sscanf(v[++i],"%i",&traceflag);
    } else if (strcmp(v[i],"-log") == 0 && i < na - 1)
      log = atoi(v[++i]);
    else if (strcmp(v[i],"-maski") == 0)
      maski = 1;
    else if (strcmp(v[i],"-if") == 0) // intercept fault
      ifault = 1;
    else if (strcmp(v[i],"-reset") == 0)
      reset = 1;
    else if (strcmp(v[i],"-noreset") == 0 ||
             strcmp(v[i],"-hot_plug") == 0)
      hot_plug = 1;
    else if (strcmp(v[i],"-nochkmem") == 0)
      chkmem = 0;
/*
    else if (strcmp(v[i],"-rom") == 0)
      rom = 1;
*/
    else if (i < na)
      prog = v[i];
  }

  if (reset && hot_plug)
    error("confficting switch reset + hot_plug");
/*
  printf("args settings:\n");
  printf("- add %08x\n- len %d\n- prog %s\n- inipc %08x\n- inisp %08x\n",add,len,prog,inipc,inisp);
  printf("- reset %d\n- rom %d\n- doread %d\n",reset,rom,doread);
  printf("- hot_plug %d\n",hot_plug);
  printf("- maski %d\n- intercept fault %d\n",maski,ifault);
  printf("- bradd %0x\n",bradd);
  printf("- log %d\n",log);
  printf("- in %s\n",in);
*/

  char serial[STLINK_SERIAL_BUFFER_SIZE] = {0};
  int con = CONNECT_NORMAL;
  if (reset)
    con = CONNECT_UNDER_RESET;
  else if (hot_plug)
    con = CONNECT_HOT_PLUG;
  stlink_t* sl = stlink_open_usbv2(1, con, serial,100000,log);
  
  if (sl)
  {
    static struct t_cpu cpu = {0};
    sl->p_cpu = &cpu;
    initdec(sl->p_cpu);
    initm();
    initcondt();
    struct stlink_reg regp0;
    printf("st vid         = 0x%04x (expect 0x%04x)\n", sl->version.st_vid, STLINK_USB_VID_ST);
    printf("stlink pid     = 0x%04x\n", sl->version.stlink_pid);
    printf("stlink version = 0x%x\n", sl->version.stlink_v);
    printf("jtag version   = 0x%x\n", sl->version.jtag_v);
    printf("swim version   = 0x%x\n", sl->version.swim_v);
    printf("chipid         = %x\n",sl->chip_id);
    if (sl->version.jtag_v == 0) {
      printf("    notice: the firmware doesn't support a jtag/swd interface\n");
    }
    if (sl->version.swim_v == 0) {
      printf("    notice: the firmware doesn't support a swim interface\n");
    }
#ifdef NEW 
    if (stlink_current_mode(sl) == STLINK_DEV_DFU_MODE) {
        if (stlink_exit_dfu_mode(sl)) {
            printf("Failed to exit DFU mode\n");
            goto on_error;
        }
    }

    if (stlink_current_mode(sl) != STLINK_DEV_DEBUG_MODE) {
        if (stlink_enter_swd_mode(sl)) {
            printf("Failed to enter SWD mode\n");
            goto on_error;
        }
    }

#endif
    if (in)
    {
      FILE *pfin = fopen(in,"r");
      if (pfin)
      {
        logmsg("processing file %s",in);
        jim_file_stlink(in,sl);
//      repl(sl,pfin);
        fclose(pfin);
      } else
        printf("can't open %s\n",in);
      return 0;
    }

    connect(sl,ifault,1);

    if (doread)
    {
      exec_read(sl,add,len,"t.bin");
      libusb_close(sl->backend_data->usb_handle);
      return 0;
    }
     
    dumpreg(sl,0,"initial register content");
    int st = stlink_read_all_regs(sl, &regp0);
    CHKERR(st,"stlink_read_all_regs");

    writedhcsr(sl,3,__LINE__);
    dhcsr = readdhcsr(sl,1,__LINE__);
    if (maski)
      if ((dhcsr >> 17) & 1) // S_HALT
        writedhcsr(sl,3 + 8,__LINE__); // mask interrupt
    dumpreg(sl,0,"register content");
    if (0) // fillmem
      fillmem(sl,0xabcd0123,0x20000000,0x5000);
    err = 0;
    if (prog)
    {
      err = loadprg(sl,prog,loadadd,inipc,inisp,chkmem);
      if (err == 0)
        dumpreg(sl,0,"after loadprog");
    }
    if (err == 0)
    {
      dhcsr = readdhcsr(sl,1,__LINE__);
      int err = stlink_read_all_regs(sl, &regp0);
      CHKERR(err,"stlink_read_all_regs");
      if (tracecount && !bradd)
      {
        printf("trace %d instructions tracef %d\n",tracecount,traceflag);
        trace(sl,tracecount,traceflag,tracestart);
        exit(0);
      }
      if (run)
      {
        printf("run 1\n");
        if (bradd)
          breakpoint(sl,1,bradd);
        runcpu(sl,wtime,100,0,0);
/* case of bpt in code
        err = stlink_read_all_regs(sl, &regp0);
        err = stlink_write_reg(sl,regp0.r[15] + 2,15);

        trace(sl,1000,0,0);
        runcpu(sl,wtime,100,0,0);
*/
        if (bradd)
        {
          breakpoint(sl,1,0);
          if (tracecount)
          {
            printf("trace %d instructions tracef %d\n",tracecount,traceflag);
            trace(sl,tracecount,traceflag,tracestart);
            exit(0);
          }
        }  
      }
      readdwt(sl,0,0,0);
      if (add == 0 || len == 0) 
        return 0;
      len = (len + 3) & ~3;
      printf("add %x len %d\n",add,len);
      uint *status = malloc(len + 1);
      printf("status at %p\n",status);
      int l = len;
      int off = 0;
      while (l > 0)
      {
        int ll = l;
        if (ll > 2048) ll = 2048;
        err = stlink_read_mem32(sl, add + off,((uchar *) status) + off, ll);
        CHKERR(err,"sstlink_read_mem32");
        l -= ll;
        off += ll;
      }
      printf("status\n");
      for (i = 0; i < len / 4; i++)
        printf("[%3d] %08x %s\n",i,status[i],tobin(status[i],32,0));
/* only on F1xx
      printf("GPIOB\n");
      st_dumpmem(sl,F1XX_GPIOB_BASE,F1XX_GPIO_SIZE);
*/
      return 0;
    } else
      printf("failed to load program to ram\n");
#ifdef NEW
    stlink_exit_debug_mode(sl);
#endif
    libusb_close(sl->backend_data->usb_handle);
  } else
  {
    printf("stlink not found\n");
    err = loadprg(sl,prog,loadadd,inipc,inisp,chkmem);
  }
//libusb_exit(ctx);
}
