/*
 cygwin distribution is an old version that don't work with cmis-dap
 latest version on 9/11/23 (1.0.26) has been downloaded and extracted here for cygwin-32 and cygwin-64
 file are:
- libusb.h (same for 32 and 64)
- cygusb_32-1.0.dll fail
- cygusb_62-1.0.dll fail
- libsb_32-1.0.dll fail
- libsb_64-1.0.dll OK

so build with 64 bit !!!

dap protocol:

https://arm-software.github.io/CMSIS_5/DAP/html/group__DAP__Connect.html
*/

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
// see note on libusb-1.0
//#include <libusb-1.0/libusb.h>
#include "libusb.h"

#include "env.h"
#include "util.h"
#include "conf.h"
#include "readpcapng-stlink.h"
#include "dap-decode.h"
#include "dapreg.h"
#include "armreg.h"
#include "driver-cmsis.h"
#include "driver-pico.h"

#include "disasm.h"
#include "dump.h"

#undef CHKSEQ
#define CHKSEQ(s)     {if (pp->plu->log && s != get_count()) printf("CHKSEQ %5d %5d @%d\n",s,get_count(),__LINE__);}

// TODO remove
// struct t_dap s_dap;
extern const char *state_name[];

void pico_clear(struct t_picoprobe *pp)
{
  memset(pp->plu->txbuf,0,pp->plu->bufsize);
  pp->pbuf = pp->plu->txbuf + 4; // 4 byte length
}

#define QUEUE_EMPTY(pp) \
if (pp->pbuf - pp->plu->txbuf > 4) error("queue not empty @%d",__LINE__);

int pico_flush(struct t_picoprobe *pp)
{
  int len = pp->pbuf - pp->plu->txbuf;
  if (len > 0)
  {
    if (pp->plu->log)
      logmsg("flush %3d %02x",len,pp->plu->txbuf[4]);
    set32(pp->plu->txbuf,len);
    int ret = send(pp->plu,len,__LINE__);
    pico_clear(pp);
    return ret;
  }
  return 0;
}

uchar pico_queue(struct t_picoprobe *pp,int cmd,int len,uchar *data)
{
  if (pp->plu->log)
    logmsg("queue cmd %d len %3d id %2x off %3d",cmd,len,pp->id,pp->pbuf - pp->plu->txbuf);
  int chklen = cmd == PICO_WRITE ? (len + 7) / 8 : 0;
  if (pp->pbuf + 6 + chklen - pp->plu->txbuf > pp->plu->bufsize)
    error("buffer overflow @%d",__LINE__);

  uchar ret = pp->id;
  *pp->pbuf++ = pp->id++;
  *pp->pbuf++ = cmd;
  pp->pbuf += set32(pp->pbuf,len);
  if (cmd == PICO_WRITE)
  {
    memcpy(pp->pbuf,data,chklen);
    pp->pbuf += chklen;
  }
  return ret;
}

int pico_idle_cycles(struct t_picoprobe *pp,int cy)
{
  static uchar rb[8] = {0};

  if (cy > sizeof(rb) * 8)
    error("pico_idle_cycles");
  pico_queue(pp,PICO_WRITE,cy,rb);
  return pico_flush(pp);
}

int pico_line_reset(struct t_picoprobe *pp)
{
// > 50 bits to 1
  static uchar rb[] = {0xff,0xff,0xff,0xff,0xff,0xff,0xff};
  pico_queue(pp,PICO_WRITE,51,rb);
  return pico_flush(pp);
}

uint write_dp_reg(struct t_picoprobe *pp,int regid,uint val)
{
  uchar buf[5];
  struct t_libusb *plu = pp->plu;
  struct t_dap *pdap = pp->pmdap->daps + pp->c_dapidx;

  if (plu->log)
    logmsg("write_dp_reg %s %x val %08x",
            pdap->regs[regid].name,pdap->regs[regid].add,val);

  QUEUE_EMPTY(pp);
  buf[0] = swd_cmd(pdap,regid,DAP_WO);
  pico_queue(pp,PICO_WRITE,8,buf);
  uchar save_id = pico_queue(pp,PICO_READ,5,0); // ack
  pico_flush(pp);

  int l;
  int res = receive(plu,&l,__LINE__); // ack
//printf("write_dp_reg res %d l %d\n",res,l);
//   0: 0b 00 00 00 17 02 05 00 00 00 13
  if (res == LIBUSB_OK && l == 11)
  {
    int i = 0;
    int l1 = get32(plu->rxbuf + i);
    i += 4;
    uchar id = plu->rxbuf[i++];
    uchar cmd = plu->rxbuf[i++];
    int nb = get32(plu->rxbuf + i);
    i += 4;
    uchar bits = plu->rxbuf[i++];
    if (l1 == l && id == save_id && nb == 5 && cmd == PICO_READ)
    {
      if (plu->log)
        logmsg("write_dp_reg ACK %x",bits);
      set32p(buf,val);
      pico_queue(pp,PICO_WRITE,32 + 1 + 3,buf);
      res = pico_flush(pp);
      if (plu->log)
        logmsg("write_dp_reg %s %08x @%d",pdap->regs[regid].name,val,__LINE__);
      dap_setreg(pdap,regid,val,plu->log ? OUT_MIN : OUT_NONE,DAP_WO,DAP_SEND,id);
    } else
      error("wrong response l (%d %d) id (%x %x) cmd %d nb %d @%d",
            l,l1,save_id,id,cmd,nb,__LINE__);
  } else
    return res;
  
  return LIBUSB_OK;
}

uint read_dp_reg(struct t_picoprobe *pp,int regid)
{
  uchar buf[1];
  struct t_libusb *plu = pp->plu;

  QUEUE_EMPTY(pp);
/*
  buf[0] = 0xa5; // 1010 0101    -1 start -0 dp -1 read -00 addr -1 parity -0 stop -1 park
                 //               7        6     5       43       2         1       0 
*/
  struct t_dap *pdap = pp->pmdap->daps + pp->c_dapidx;
  buf[0] = swd_cmd(pdap,regid,DAP_RO);
  pico_queue(pp,PICO_WRITE,8,buf);
  pico_queue(pp,PICO_READ,4,0);
  pico_flush(pp);

  int l;
  int res = receive(plu,&l,__LINE__);
//printf("res %d l %d\n",res,l);
  uint save_id = pico_queue(pp,PICO_READ,34,0);
  buf[0] = 0;
  pico_queue(pp,PICO_WRITE,3,buf);
  pico_flush(pp);
  res = receive(plu,&l,__LINE__);
//printf("res %d l %d\n",res,l);
// 0f 00 00 00 0d 02 22 00 00 00 77 24 c1 0b 02
  if (res == LIBUSB_OK && l == 15)
  {
    int i = 0;
    int tl = get32(plu->rxbuf + i);
    i += 4;
    if (tl == l)
    {
      int id = plu->rxbuf[i++];
      if (id != save_id)
        error("wrong id match @%d",__LINE__);
      int op = plu->rxbuf[i++];
      int nbit = get32(plu->rxbuf + i);
      i += 4;
//    printf("tl %d nbit %d\n",tl,nbit);
      if (op == PICO_READ && nbit == 34)
      {
        uint rval = get32(plu->rxbuf + i);
        i++;
        int p = plu->rxbuf[i++];
        int p1 = parity32(rval);
        if (plu->log)
          logmsg("read_dp_reg %s %08x(p %d %d) @%d",pdap->regs[regid].name,rval,p,p1,__LINE__);
        dap_setreg(pdap,regid,rval,plu->log ? OUT_MIN : OUT_NONE,DAP_RO,DAP_RECEIVE,id);
        return rval;
      } else
        error("read_dp_reg: wrong received data @%d",__LINE__);
    }
  } else
    error("read_dp_reg: unexpected @%d",__LINE__);

  return res;
}

uint write_ap_reg_m(struct t_picoprobe *pp,int regid,uint *vals,int n)
{
  uchar buf[8];
//int   id[256];
  struct t_libusb *plu = pp->plu;
  struct t_dap *pdap = pp->pmdap->daps + pp->c_dapidx;

  QUEUE_EMPTY(pp);

  if (plu->log)
    logmsg("write_ap_reg_m ap %s %x n %d %08x ...",
           pdap->regs[regid].name,pdap->regs[regid].add,n,vals[0]);
/*
  if (TODO)
    write_dp_reg(pp,DAP_DP_SELECT,(ap << 24) | (s_dap.regs[regid].add & 0xf0));
*/
  buf[0] = swd_cmd(pdap,regid,DAP_WO);
  for (int i = 0; i < n; i++)
  {
    pico_queue(pp,PICO_WRITE,8,buf);
    set32p(buf,vals[i]);
    buf[5] = 0;
    pico_queue(pp,PICO_WRITE,32 + 1 + 3,buf);
    /* id[i] = */ pico_queue(pp,PICO_READ,5,0);
  }

  return LIBUSB_OK;
}

uint write_ap_reg(struct t_picoprobe *pp,int regid,uint val,int fl)
{
  uchar buf[8],id;
  int len[] = {5,36,4,34};
  struct t_libusb *plu = pp->plu;
  struct t_dap *pdap = pp->pmdap->daps + pp->c_dapidx;

  QUEUE_EMPTY(pp);

  if (plu->log)
    logmsg("write_ap_reg ap %s %x val %08x fl %d",
           pdap->regs[regid].name,pdap->regs[regid].add,val,fl);
  if (fl)
    write_dp_reg(pp,DAP_DP_SELECT,(pp->ap << 24) | (pdap->regs[regid].add & 0xf0));
  int lr = 4;
  buf[0] = swd_cmd(pdap,regid,DAP_WO);
  pico_queue(pp,PICO_WRITE,8,buf);
  id = pico_queue(pp,PICO_READ,len[0],0);
  lr += 2 + 4 + (len[0] + 7) / 8;
  set32p(buf,val);
  buf[5] = 0;
  pico_queue(pp,PICO_WRITE,len[1],buf);
  pico_flush(pp);

  int l;
  int res = receive(plu,&l,__LINE__);  
  if (plu->log)
    logmsg("write_ap_reg res %d l %d lr %d",res,l,lr);
  int l1 = get32(plu->rxbuf);
  int i = 4;
  if (res == LIBUSB_OK && l == lr && l1 == l)
  {
    int l = get32(plu->rxbuf + i + 2);
    if (id == plu->rxbuf[i] && plu->rxbuf[i + 1] == PICO_READ && l == len[0])
    {
      dap_setreg(pdap,regid,val,plu->log ? OUT_MIN : OUT_NONE,DAP_WO,DAP_SEND,id);
      i += 6;
    } else
      error("write_ap_reg: unexpected @%d",__LINE__);
// TODO check ...
  } else
    error("write_ap_reg: unexpected @%d",__LINE__);

  return LIBUSB_OK;
}

uint read_ap_reg_m(struct t_picoprobe *pp,uint *vals,int len)
{
  uchar buf[1],id[64];
  struct t_libusb *plu = pp->plu;

  if (plu->log)
    logmsg("read_ap_reg_m ap %d len %d",pp->ap,len);
  QUEUE_EMPTY(pp);
  struct t_dap *pdap = pp->pmdap->daps + pp->c_dapidx;

  int lr = 4;
  int i;
// first read is dummy
// do we need len + 1 read (last of RDBUFF)
  for (i = 0; i < len; i++)
  {
    buf[0] = swd_cmd(pdap,DAP_MEMAP_DRW,DAP_RO);
    pico_queue(pp,PICO_WRITE,8,buf);
    lr += 2 + 4 + (4 + 7) / 8;
    pico_queue(pp,PICO_READ,4,0);
    lr += 2 + 4 + (32 + 2 + 7) / 8;
    id[i]  = pico_queue(pp,PICO_READ,32 + 2,0);
  }
  buf[0] = swd_cmd(pdap,DAP_DP_RDBUFF,DAP_RO);
  pico_queue(pp,PICO_WRITE,8,buf);
  lr += 2 + 4 + (4 + 7) / 8;
  pico_queue(pp,PICO_READ,4,0);
  lr += 2 + 4 + (32 + 2 + 7) / 8;
  id[i]  = pico_queue(pp,PICO_READ,32 + 2,0);
  buf[0] = 0;
  pico_queue(pp,PICO_WRITE,3,buf);
  pico_flush(pp);
  int l;
  int res = receive(plu,&l,__LINE__); // multiple read 
  if (plu->log)
    logmsg("read_ap_reg res %d l %d lr %d",res,l,lr);
  int l1 = get32(plu->rxbuf);
  i = 4;
  if (res == LIBUSB_OK && l == lr && l1 == l)
  {
    for (int j = 0; j < len + 1; j++)
    {
      int l = get32(plu->rxbuf + i + 2);
//    printf("i %3d l %3d ",i,l);
      if (plu->rxbuf[i + 1] == PICO_READ && l == 4)
      {
        if (plu->rxbuf[i + 6] != 3)
          error("read_ap_reg_m: !ACK @%d",__LINE__);
//      printf("read %02x ",plu->rxbuf[i + 6]);
        i += 6 + (l + 7) / 8;
        int l = get32(plu->rxbuf + i + 2);
        if (id[j] == plu->rxbuf[i] && plu->rxbuf[i + 1] == PICO_READ && l == 34)
        {
          if (j > 0)
          {
            vals[j - 1] = get32(plu->rxbuf + i + 6); 
            if (plu->log)
              printf("l %2d vals[%2d] %08x %12d ",l,j,vals[j - 1],vals[j - 1]);
          }
          i += 6 + (l + 7) / 8;
        } else
          error("read_ap_reg_m: unexpected @%d",__LINE__);
      } else
        error("read_ap_reg_m: unexpected @%d",__LINE__);
//    putchar('\n');
    }
  } else
    error("read_ap_reg_m: unexpected @%d",__LINE__);

  return LIBUSB_OK; 
}

uint read_ap_reg(struct t_picoprobe *pp,int regid,int fl)
{
  uchar buf[1],id[4];
  int len[] = {4,34,4,34};
  struct t_libusb *plu = pp->plu;
  struct t_dap *pdap = pp->pmdap->daps + pp->c_dapidx;

  QUEUE_EMPTY(pp);

  if (plu->log)
    logmsg("read_ap_reg ap %d %s %x %d",pp->ap,pdap->regs[regid].name,pdap->regs[regid].add,fl);
  if (fl == 0)
    write_dp_reg(pp,DAP_DP_SELECT,(pp->ap << 24) | (pdap->regs[regid].add & 0xf0));
  buf[0] = swd_cmd(pdap,regid,DAP_RO);
//printf("swd_cmd(%d,%d) %x\n",regid,DAP_RO,buf[0]);
  pico_queue(pp,PICO_WRITE,8,buf);
  id[0] = pico_queue(pp,PICO_READ,len[0],0);
  int lr = 4;
  lr += 2 + 4 + (len[0] + 7) / 8;
  id[1] = pico_queue(pp,PICO_READ,len[1],0);
  lr += 2 + 4 + (len[1] + 7) / 8;
  buf[0] = swd_cmd(pdap,DAP_DP_RDBUFF,DAP_RO);
  pico_queue(pp,PICO_WRITE,8,buf);
  id[2] = pico_queue(pp,PICO_READ,len[2],0);
  lr += 2 + 4 + (len[2] + 7) / 8;
  id[3] = pico_queue(pp,PICO_READ,len[3],0);
  lr += 2 + 4 + (len[3] + 7) / 8;
  buf[0] = 0;
  pico_queue(pp,PICO_WRITE,3,buf);
  pico_flush(pp);

  int l;
  int val = 0;
  int res = receive(plu,&l,__LINE__); // multiple read 
  if (plu->log)
    logmsg("read_ap_reg res %d l %d lr %d",res,l,lr);
  int l1 = get32(plu->rxbuf);
  int i = 4;
  if (res == LIBUSB_OK && l == lr && l1 == l)
  {
    int j;
    for (j = 0; j < 4; j++)
    {
      int l = get32(plu->rxbuf + i + 2);
/*
      printf("j %d i %d rxbuf %02x %02x l %d\n",
              j,i,plu->rxbuf[i],plu->rxbuf[i + 1],l);
*/
      if (id[j] == plu->rxbuf[i] && plu->rxbuf[i + 1] == PICO_READ && l == len[j])
      {
        i += 6;
/*
        if (j == 1)
          printf("read 2 val %08x\n",get32(plu->rxbuf + i));
*/
        if (j == 3)
        {
          val = get32(plu->rxbuf + i);
          dap_setreg(pdap,regid,val,plu->log ? OUT_MIN : OUT_NONE,DAP_RO,DAP_RECEIVE,id[j]);
        }
        i += (len[j] + 7) / 8;
      } else
        error("read_ap_reg: j %d unexpected @%d",j,__LINE__);
    }
    if (j != 4)
      error("read_ap_reg: j %d unexpected @%d",j,__LINE__);
  } else
    error("read_ap_reg: unexpected @%d",__LINE__);

  if (plu->log)
    logmsg("read_ap_reg ap %d %s %x %d -> %08x",
           pp->ap,pdap->regs[regid].name,pdap->regs[regid].add,fl,val);
  return val;
}

int pico_find_ap(struct t_picoprobe *pp)
{
  for (int ap = 0; ap < 4; ap++)
  {
    uint idr = read_ap_reg(pp,DAP_AP_IDR,0);
    logmsg("ap %2x IDR %08x",pp->ap,idr);
/*
    if (idr == 0)
      break;
*/
  }
// TODO fix: return good ap
  return 0;
}

// read rom table
// _init_rom_table_base 
uint pico_rom_base(struct t_picoprobe *pp)
{
  uint rom_base = read_ap_reg(pp,DAP_MEMAP_BASE,0);
  logmsg("rom_base %08x",rom_base);

  return rom_base;
}

int pico_write32(struct t_picoprobe *pp,uint add,uint val)
{
  struct t_libusb *plu = pp->plu;
  struct t_dap *pdap = pp->pmdap->daps + pp->c_dapidx;
  QUEUE_EMPTY(pp);
  if (plu->log)
    logmsg("pico_write32 ap %d add %08x val %08x",pp->ap,add,val);

  uint id = pp->id;
// _write_memory ap.py
//  old csw | TRANSFER_SIZE[transfer_size]
  uint csw = 0x3000012; // TODO
  if (pdap->regs[DAP_MEMAP_CSW].vals != csw)
  {
    if (plu->log)
      logmsg("change csw %08x -> %08x",pdap->regs[DAP_MEMAP_CSW].vals,csw);
    write_ap_reg(pp,DAP_MEMAP_CSW,csw,0); 
  }
  int res = write_ap_reg(pp,DAP_MEMAP_TAR,add,0);
  res = write_ap_reg(pp,DAP_MEMAP_DRW,val,0);
  arm_setreg(add,val,WO,id,1);

  return res;
}

int pico_read_block32(struct t_picoprobe *pp,uint add,uint *vals,int len)
{
  struct t_libusb *plu = pp->plu;
  struct t_dap *pdap = pp->pmdap->daps + pp->c_dapidx;

  QUEUE_EMPTY(pp);
  if (plu->log)
    logmsg("pico_read_block32 ap %d add %08x len %d",pp->ap,add,len);
  uint csw = 0x3000012; // TODO
  if (pdap->regs[DAP_MEMAP_CSW].vals != csw)
  {
    if (plu->log)
      logmsg("change csw %08x -> %08x",pdap->regs[DAP_MEMAP_CSW].vals,csw);
    write_ap_reg(pp,DAP_MEMAP_CSW,csw,0); 
  }
  write_ap_reg(pp,DAP_MEMAP_TAR,add,0);
  
  read_ap_reg_m(pp,vals,len);

  return LIBUSB_OK;
}

size_t pico_read(struct t_picoprobe *pp,uint add,uint len,char *fn)
{
  printf("read add %x len %d file %s\n",add,len,fn);
  FILE *pf = fopen(fn,"wb");
  if (!pf)
  {
    printf("can't open %s\n",fn);
    return -1;
  } 

  size_t ret = 0;
  while (len > 0)
  {
    uint val[32];
    uint l = len > sizeof(val) ? SIZE(val) : (len + sizeof(uint) - 1) / sizeof(uint);
    int r = pico_read_block32(pp,add,val,l);
    if (r == LIBUSB_OK)
    {
      size_t n = fwrite(val,sizeof(val[0]),l,pf);
      len -= n * sizeof(uint);
      add += n * sizeof(uint);
      ret += n * sizeof(uint);
      if (n != l)
      {
        printf("write %zd\n",n);
        break;
      }
    } else
      break;
  }
  fclose(pf);
  return ret;
}

ushort pico_read16(struct t_picoprobe *pp,uint add)
{
  struct t_libusb *plu = pp->plu;
  struct t_dap *pdap = pp->pmdap->daps + pp->c_dapidx;

  QUEUE_EMPTY(pp);
  if (plu->log)
    logmsg("pico_read16 ap %d add %08x",pp->ap,add);
 
  if (add & 1)
    error("odd address in read16 %08x @%d",add,__LINE__);
  uint id = pp->id;
// _read_memory ap.py
//  old csw | TRANSFER_SIZE[transfer_size]
  uint v = 0x3000011; // TODO
  if (pdap->regs[DAP_MEMAP_CSW].vals != v)
    write_ap_reg(pp,DAP_MEMAP_CSW,v,0); 
  write_ap_reg(pp,DAP_MEMAP_TAR,add,0);
  uint val = read_ap_reg(pp,DAP_MEMAP_DRW,1);
  if (plu->log)
    logmsg("pico_read16 ap %d add %08x -> %04x",pp->ap,add,val >> 16);
  arm_setreg(add,val,RO,id,1);

  return val >> 16;
}

uint pico_read32(struct t_picoprobe *pp,uint add)
{
  struct t_libusb *plu = pp->plu;
  struct t_dap *pdap = pp->pmdap->daps + pp->c_dapidx;

  QUEUE_EMPTY(pp);
  if (plu->log)
    logmsg("pico_read32 ap %d add %08x",pp->ap,add);

  if (add & 3)
    error("ipico_read32: wrong address alignemet %08x @%d",add,__LINE__);
  uint id = pp->id;
// _read_memory ap.py
//  old csw | TRANSFER_SIZE[transfer_size]
  uint v = 0x3000012; // TODO
  if (pdap->regs[DAP_MEMAP_CSW].vals != v)
    write_ap_reg(pp,DAP_MEMAP_CSW,v,0); 
  write_ap_reg(pp,DAP_MEMAP_TAR,add,0);
  uint val = read_ap_reg(pp,DAP_MEMAP_DRW,1);
  if (plu->log)
    logmsg("pico_read32 ap %d add %08x -> %08x",pp->ap,add,val);
  arm_setreg(add,val,RO,id,1);

  return val;
}

int pico_set_vector_catch(struct t_picoprobe *pp)
{
// set_vector_catch cortex_m.py
  uint demcr = pico_read32(pp,DEMCR_ADD); 
  demcr = 0x01000400; // TODO fix
  pico_write32(pp,DEMCR_ADD,demcr); 

  return LIBUSB_OK;
}

int pico_halt_core(struct t_picoprobe *pp)
{
// halt in cortex_m.py
  pico_write32(pp,DHCSR_ADD,DHCSR_DBGKEY | DHCSR_C_DEBUGEN | DHCSR_C_HALT);

  return LIBUSB_OK;
}

int pico_select_core(struct t_picoprobe *pp)
{
  uint dhcsr = pico_read32(pp,DHCSR_ADD); 
  logmsg("dhcsr %08x",dhcsr);

  pico_write32(pp,DHCSR_ADD,0xa05f0001);
  uint cpuid = pico_read32(pp,CPUID_ADD); 
  logmsg("cpuid %08x",cpuid);
  uint demcr = pico_read32(pp,DEMCR_ADD); 
  logmsg("demcr %08x",demcr);
// second time ...
// in DWT.init dwt.py
  demcr = pico_read32(pp,DEMCR_ADD); 
// TODO check DEMCR_TRCENA 
  uint dwt_ctrl = pico_read32(pp,DWT_CTRL_ADD); 
  int wc = (dwt_ctrl & DWT_CTRL_NUM_COMP_MASK) >> DWT_CTRL_NUM_COMP_SHIFT;
  logmsg("dwt_ctrl %08x watchpoint count %d",dwt_ctrl,wc);
  for (int i = 0; i < wc; i++)
  {
    pico_write32(pp,DWT_FUNn(i),0); // clear watchpoint
  }
  pico_write32(pp,DWT_CTRL_ADD,DWT_CTRL_CYCCNTENA_MASK); 
// BTU breakpoint unit
// fpb.py
  uint fpcr = pico_read32(pp,FP_CTRL_ADD); 
  int fpb_rev = 1 + ((fpcr & FP_CTRL_REV_MASK) >> FP_CTRL_REV_SHIFT);
  int nb_code = ((fpcr >> 8) & 0x70) | ((fpcr >> 4) & 0xF);
  int nb_lit = (fpcr >> 7) & 0xf;

  logmsg("fp_ctrl %08x rev %d code breakpoint %d comparators %d",
         fpcr,fpb_rev,nb_code,nb_lit);

  pico_write32(pp,FP_CTRL_ADD,2); // disable it 
  for (int i = 0; i < nb_code; i++)
    pico_write32(pp,FP_COMPn_ADD(i),0); // clear breakpoint

  return LIBUSB_OK;
}

int pico_find_component(struct t_picoprobe *pp,int rom_address)
{
  struct t_libusb *plu = pp->plu;

  if (plu->log)
    logmsg("pico_find_component ap %d rom_address %08x",pp->ap,rom_address);
// find_component ap.py
  uint demcr = pico_read32(pp,DEMCR_ADD);
  logmsg("demcr %08x",demcr);
  int res = pico_write32(pp,DEMCR_ADD,demcr | DEMCR_TRCENA);
// is_enabled_for dap.py
  uint csw = read_ap_reg(pp,DAP_MEMAP_CSW,1);
  logmsg("csw %08x",csw);
  uint vals[16];
  pico_read_block32(pp,rom_address + PIDR4,vals,12);
  pico_read_block32(pp,rom_address,vals,9);
  pico_read_block32(pp,0xe000e000 + PIDR4,vals,12); // TODO 
  pico_read_block32(pp,0xe0001000 + PIDR4,vals,12); // TODO 
  pico_read_block32(pp,0xe0002000 + PIDR4,vals,12); // TODO 

  return res;
}

int pico_get_state(struct t_picoprobe *pp)
{
  struct t_libusb *plu = pp->plu;
  uint dhcsr = pico_read32(pp,DHCSR_ADD); 
  if (plu->log)
    logmsg("get_state dhcsr %08x",dhcsr);
  if (dhcsr & DHCSR_S_RESET_ST)
  {
    uint dhcsr1 = pico_read32(pp,DHCSR_ADD);
    if (dhcsr1 & DHCSR_S_RESET_ST)
      return S_RESET;
  }
  return dhcsr_state(dhcsr);
}

uint is_running(struct t_picoprobe *pp)
{
  int st = pico_get_state(pp);
  if (pp->plu->log)
    logmsg("is_running state %d: %s",st,state_name[st]);
  return st == S_RUNNING;
}

void pico_read_regs(struct t_picoprobe *pp,int *rid,int nr,uint *vals,int fl)
{
  struct t_libusb *plu = pp->plu;

  if (plu->log)
    logmsg("pico_read_regs");
// read_core_registers_raw register.py
// check is_running
  if (fl) // fl = 1 from read_core_register[_raw] in context.py
          // fl = 0 from read_core_registers_raw in cortex_m.py
  {
    int run = is_running(pp);
    if (plu->log)
      logmsg("pico_read_regs: is_running %d",run);
  }
// check is_halted
  int st = pico_get_state(pp);
  if (plu->log)
    logmsg("is_halted state %d",st);

  for (int i = 0; i < nr; i++)
  {
    if (plu->log)
      logmsg("i %d set DCSR %d",i,rid[i]);
    pico_write32(pp,DCRSR_ADD,rid[i]);
    uint dhcsr = pico_read32(pp,DHCSR_ADD);
    uint reg   = pico_read32(pp,DCRDR_ADD);
    vals[i] = reg;
    if (plu->log)
      logmsg("%2d dhcsr %08x rid %2d reg %08x",i,dhcsr,rid[i],reg);
  }
  if (plu->log)
  {
    printf("reg contents:\n");
    for (int i = 0; i < nr; i++)
      printf("%2d rid %2d reg %08x\n",i,rid[i],vals[i]);
  }
}

int pico_disasm(struct t_picoprobe *pp,struct t_cpu *p_cpu,int pc,struct t_stat *p_st)
{
  int  l,ist,ist2;
  ushort code[4];
//struct t_stat stat = {0},*p_st;
  struct t_dec  /* dec  = {0},*/ *pd;

/*
  err = stlink_read_mem32(sl, pc & ~3 ,(uchar *) code, sizeof code);
  CHKERR(err,"stlink_read_mem32");
*/
  pico_read_block32(pp,pc & ~3,(uint *) code,2);
  p_st->pc = pc;
  int idx = 0;
  if ((pc & 3) == 2)
    idx = 1;
  ist2 = code[idx];
  ist  = code[idx + 1];
//  printf("ist %04x %04x \n",ist2,ist);
  if (((ist2 >> 12) & 0xf) == 0xf ||
     ((ist2 >> 11) & 0x1f) == 0x1d)
  {
    p_st->inst = (ist2 << 16) | (ist & 0xffff);
    l = 4;
  } else
  {
    p_st->inst = ist2;
    l = 2;
  }
  p_st->il = l;

  printf("%08x ",pc);
  if (p_st->il == 4)
    printf("%04x %04x ",p_st->inst >> 16,p_st->inst & MW);
  else
    printf("%04x      ",p_st->inst & MW);
  if (p_cpu->logflags)
    printf("%s ",fmtflags(p_cpu));
    
  pd = decode(p_cpu,p_st,0);
//printf("pd %p nfunc %d swi %d\n",pd,pd->nfunc,p_st->swi);
  if (pd->nfunc <= p_st->swi || p_st->swi < 0)
  {
    printf("nf %d swi %d\n",pd->nfunc,p_st->swi);
    printf("no func to call\n");
  }
  if (pd->nfunc > 0)
  {
//  printf("pdis %p fmtinst %p\n",pd->func[p_st->swi].pdis,pd->func[p_st->swi].fmtinst);
    if (pd->func[p_st->swi].pdis)
      pd->func[p_st->swi].pdis(p_cpu,p_st,pd->func + p_st->swi);
    else if (pd->func[p_st->swi].fmtinst)
      dump_gen(p_cpu,p_st,pd->func + p_st->swi);
    else
    {
      printf("TBD null function swi %d",p_st->swi);
//    error("TDB");
    }
  } else
  {
    printf("TBD nfunc %d",pd->nfunc);
//  error("no func to call");
  }
  return 0;
}

void pico_read_all_regs(struct t_picoprobe *pp,struct stlink_reg *reg,int log)
{
  int  rid[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
  uint vals[32];
  pico_read_regs(pp,rid,SIZE(rid),vals,0);

  memcpy(reg->r,vals,sizeof(uint) * 16);
  reg->xpsr = vals[16];
  if (log == 0) 
    return;
  for (int i = 0; i < SIZE(rid); i++)
    printf("R%02d: %08x\n",rid[i],vals[i]);
}

void pico_step(struct t_picoprobe *pp)
{
  struct t_libusb *plu = pp->plu;
// step entry [cortex_m]

  int s = get_count();
  uint dhcsr = pico_read32(pp,DHCSR_ADD);
  if (plu->log)
    logmsg("pico_step: dhcsr %08x",dhcsr);
  CHKSEQ(s + 4);
// clear_debug_cause_bits [cortex_m]
  pico_write32(pp,DFSR_ADD,DFSR_EXTERNAL | DFSR_VCATCH |
                              DFSR_DWTTRAP | DFSR_BKPT |
                              DFSR_HALTED);
/*
  here: remove/insert breakpoint
*/
  CHKSEQ(s + 8);
// step after # Get current state.
  uint maskints = dhcsr & DHCSR_C_MASKINTS;
  uint pmov = dhcsr & DHCSR_C_PMOV;
  uint dhcsr_step = DHCSR_DBGKEY | DHCSR_C_DEBUGEN | DHCSR_C_STEP | pmov;
// if (something..) // TODO check
  pico_write32(pp,DHCSR_ADD,dhcsr_step | DHCSR_C_HALT);
  CHKSEQ(s + 12);
// step starting loop
  while (1)
  {
    pico_write32(pp,DHCSR_ADD,dhcsr_step);
    CHKSEQ(s + 16);
    dhcsr = pico_read32(pp,DHCSR_ADD);
    if (plu->log)
      logmsg("pico_step: dhcsr %08x",dhcsr);
    CHKSEQ(s + 20);
    if (dhcsr & DHCSR_C_HALT)
      break;
    else
      error("unexpected @%d",__LINE__);
  }
// if (something..) // TODO check
  pico_write32(pp,DHCSR_ADD,DHCSR_DBGKEY | DHCSR_C_DEBUGEN |
                     DHCSR_C_HALT | maskints | pmov);

  CHKSEQ(s + 24);
// step exit
}

void pico_step1(struct t_picoprobe *pp,uint dhcsr)
{
  struct t_libusb *plu = pp->plu;
// step entry [cortex_m]

#ifdef XX
  int s = get_count();
  uint dhcsr = pico_read32(pp,DHCSR_ADD);
  if (plu->log)
    logmsg("pico_step: dhcsr %08x",dhcsr);
  CHKSEQ(s + 4);
// clear_debug_cause_bits [cortex_m]
  pico_write32(pp,DFSR_ADD,DFSR_EXTERNAL | DFSR_VCATCH |
                              DFSR_DWTTRAP | DFSR_BKPT |
                              DFSR_HALTED);
#endif
/*
  here: remove/insert breakpoint
*/
//CHKSEQ(s + 8);
// step after # Get current state.
  uint maskints = dhcsr & DHCSR_C_MASKINTS;
  uint pmov = dhcsr & DHCSR_C_PMOV;
  uint dhcsr_step = DHCSR_DBGKEY | DHCSR_C_DEBUGEN | DHCSR_C_STEP | DHCSR_C_MASKINTS | pmov;
// if (something..) // TODO check
  pico_write32(pp,DHCSR_ADD,dhcsr_step | DHCSR_C_HALT);
//CHKSEQ(s + 12);
// step starting loop
  while (1)
  {
    pico_write32(pp,DHCSR_ADD,dhcsr_step);
//  CHKSEQ(s + 16);
    dhcsr = pico_read32(pp,DHCSR_ADD);
    if (plu->log)
      logmsg("pico_step: dhcsr %08x",dhcsr);
//  CHKSEQ(s + 20);
    if (dhcsr & DHCSR_C_HALT)
      break;
    else
      error("unexpected @%d",__LINE__);
  }
// if (something..) // TODO check
  pico_write32(pp,DHCSR_ADD,DHCSR_DBGKEY | DHCSR_C_DEBUGEN |
                     DHCSR_C_HALT | maskints | pmov);

//CHKSEQ(s + 24);
// step exit
}

static struct t_cpu cpu;

static void disasm_init()
{
  if (cpu.init == 0)
  {
    initdec(&cpu);
    initm();
    initcondt();
    cpu.init = 1;
  }
}

// single step nstep instruction
// flag:  1  read cpu registers
//        2  read cpu float regs
//
int pico_trace_ex(struct t_picoprobe *pp,uint nstep,uint flag,uint tracestart)
{
  int  i,j;
  char *ident;
//struct t_dec  /* dec  = {0}, */ *pd;
  struct t_stat stat = {0},*p_st;
  struct t_cpu *p_cpu = &cpu;

  disasm_init();
  p_st  = &stat;
//pd    = &dec;

  p_cpu->logflags = 1;
  printf("trace nstep %u flag %x tracestart %d\n",nstep,flag,tracestart);
  p_cpu->logd = 0;
  p_st->prefix = 0;
  p_cpu->execcount = 10;


/*
  dumpreg(sl,flag & 2,"trace init");
  err = stlink_read_all_regs(sl, &p_cpu->regp0);
  CHKERR(err,"stlink_read_all_regs");
  err = stlink_read_all_unsupported_regs(sl, &p_cpu->regp0);
  CHKERR(err,"stlink_read_all_unsupported_regs");
*/
  printf("#### initial register values:\n");
  pico_read_all_regs(pp,&p_cpu->regp0,1);
 
  ident = "                                    ";
  for (i = 0; i < nstep; i++)
  {
//  printf("%d/%d\n",i,nstep);
    p_cpu->N = (p_cpu->regp0.xpsr >> 31) & 1;
    p_cpu->Z = (p_cpu->regp0.xpsr >> 30) & 1;
    p_cpu->C = (p_cpu->regp0.xpsr >> 29) & 1;
    p_cpu->V = (p_cpu->regp0.xpsr >> 28) & 1;
    p_cpu->IT = ((p_cpu->regp0.xpsr >> 25) & 3) | (((p_cpu->regp0.xpsr >> 10) & 0x3f) << 2);
    p_cpu->ISR = p_cpu->regp0.xpsr & 0x1ff;
    printf("%8d ",i + tracestart);
    pico_disasm(pp,p_cpu,p_cpu->regp0.r[15],p_st);
    putchar('\n');

/*
    err = stlink_step(sl);
    CHKERR(err,"stlink_step");
*/
    pico_step(pp);
    if (flag)
    {
      if (flag & 1)
        pico_read_all_regs(pp,&p_cpu->regp,0);
/*
      if (flag & 2)
      {
        err = stlink_read_all_unsupported_regs(sl, &p_cpu->regp);
        CHKERR(err,"stlink_read_all_regs");
      } 
 */     
      if (p_cpu->regp.xpsr != p_cpu->regp0.xpsr)
      {
        int N,Z,C,V,ISR,IT;

        N = (p_cpu->regp.xpsr >> 31) & 1;
        Z = (p_cpu->regp.xpsr >> 30) & 1;
        C = (p_cpu->regp.xpsr >> 29) & 1;
        V = (p_cpu->regp.xpsr >> 28) & 1;
        IT = ((p_cpu->regp.xpsr >> 25) & 3) | (((p_cpu->regp.xpsr >> 10) & 0x3f) << 2);
        ISR = p_cpu->regp.xpsr & 0x1ff;
 
        printf("%s",ident);
        if (p_cpu->N != N ||
            p_cpu->Z != Z ||
            p_cpu->C != C ||
            p_cpu->V != V)
        {   
          printf("setflags ");
          if (p_cpu->N)   printf("N");
          else            printf("-");
          if (p_cpu->Z)   printf("Z");
          else            printf("-");
          if (p_cpu->C)   printf("C");
          else            printf("-");
          if (p_cpu->V)   printf("V");
          else            printf("-");
          if (p_cpu->ISR) printf("I");
          else            printf("-");

          printf("  -> ");

          if (N)          printf("N");
          else            printf("-");
          if (Z)          printf("Z");
          else            printf("-");
          if (C)          printf("C");
          else            printf("-");
          if (V)          printf("V");
          else            printf("-");
          if (p_cpu->ISR) printf("I");
          else            printf("-");
          printf(" ");
        } else if (IT != p_cpu->IT)
          printf("IT %x -> %x ",p_cpu->IT,IT);
        else if (ISR != p_cpu->ISR)
          printf("ISR %x -> %x ",p_cpu->ISR,ISR);
        printf("%08x\n", p_cpu->regp.xpsr);
      }
      if (p_cpu->regp.main_sp != p_cpu->regp0.main_sp && p_cpu->regp.main_sp != p_cpu->regp.r[SP])
        printf("%smain_sp    %08x -> %08x\n",ident,p_cpu->regp0.main_sp,p_cpu->regp.main_sp);
      if (p_cpu->regp.process_sp != p_cpu->regp0.process_sp && p_cpu->regp.process_sp != p_cpu->regp.r[SP])
        printf("%ssetpsp     %08x -> %08x\n",ident,p_cpu->regp0.process_sp,p_cpu->regp.process_sp);
      if (p_cpu->regp.rw != p_cpu->regp0.rw && p_cpu->regp.rw != p_cpu->regp.r[SP])
        printf("%srw         %08x -> %08x\n",ident,p_cpu->regp0.rw,p_cpu->regp.rw);
      if (p_cpu->regp.rw2 != p_cpu->regp0.rw2 && p_cpu->regp.rw2 != p_cpu->regp.r[SP])
        printf("%ssetprimask %d -> %d\n",ident,p_cpu->regp0.rw2,p_cpu->regp.rw2);
      if (p_cpu->regp.control != p_cpu->regp0.control)
        printf("%scontrol    %08x -> %08x\n",ident,p_cpu->regp0.control,p_cpu->regp.control);
      if (p_cpu->regp.faultmask != p_cpu->regp0.faultmask)
        printf("%sfaultmask  %08x -> %08x\n",ident,p_cpu->regp0.faultmask,p_cpu->regp.faultmask);
      if (p_cpu->regp.basepri != p_cpu->regp0.basepri)
        printf("%sbasepri    %08x -> %08x\n",ident,p_cpu->regp0.basepri,p_cpu->regp.basepri);
      if (p_cpu->regp.primask != p_cpu->regp0.primask)
        printf("%ssetprimask %d -> %d\n",ident,p_cpu->regp0.primask,p_cpu->regp.primask);
      for (j = 0; j < 15; j++) // general register without PC
      {
        if (p_cpu->regp.r[j] != p_cpu->regp0.r[j])
        {
          printf("%sreg        W       R%02d: %08x -> %08x\n",
                 ident,j, p_cpu->regp0.r[j], p_cpu->regp.r[j]);
        }
      }
    } else
    { // read only PC
/*
      err = stlink_read_reg(sl, 15, &p_cpu->regp);
      CHKERR(err,"stlink_read_all_regs");
*/    ;
    }
    p_cpu->regp0 = p_cpu->regp;
  }
  printf("#### final register values:\n");
  pico_read_all_regs(pp,&p_cpu->regp0,1);
//dumpreg(sl,flag & 2,"trace end");
  return 0;
}

void pico_set_break(struct t_picoprobe *pp,uint add)
{
// enable in fpb.py
  pico_write32(pp,FP_CTRL_ADD,FP_CTRL_KEY_MASK | FP_CTRL_EN_MASK);
// set breakpoint in fpb.py
  logmsg("pico_set_break add %08x set add %08x val %08x",
         add,
         FP_COMPn_ADD(0),FP_COMP_MATCH_LOW | (add & FP_COMP_COMP_MASK) | FP_COMP_ENABLE);
  pico_write32(pp,FP_COMPn_ADD(0),FP_COMP_MATCH_LOW | (add & FP_COMP_COMP_MASK) | FP_COMP_ENABLE);
}

void pico_cont(struct t_picoprobe *pp)
{
  uint vals[4];
// resume in cortex_m.py
  pico_write32(pp,DHCSR_ADD,DHCSR_DBGKEY | DHCSR_C_DEBUGEN);
  CHKSEQ(653);
  uint st = pico_get_state(pp);
  logmsg("st %d",st);
  int rid3[] = {15};
  pico_read_regs(pp,rid3,SIZE(rid3),vals,1);
  CHKSEQ(677);
// get_t_response [gdbserver]
// is_debug_trap [cortex_m]
//pico_is_debug_trap(pp); // use diffrent register list
  uint dfsr = pico_read32(pp,DFSR_ADD);
  logmsg("pico_cont: dfsr %08x",dfsr);

  int rid4[] = {7,13,14,15}; 
  pico_read_regs(pp,rid4,SIZE(rid4),vals,1);
}

void pico_rem_break(struct t_picoprobe *pp)
{
// remove breakpoint
  pico_write32(pp,FP_COMPn_ADD(0),0);
}

// reset_and_halt cortex_m.py
int pico_reset_halt(struct t_picoprobe *pp)
{
  struct t_dap *pdap = pp->pmdap->daps + pp->c_dapidx;
  int s = get_count(); // 548
  int r = pico_halt_core(pp);
  CHKSEQ(s + 4);
// set_reset_catch
// CortexM.DEMCR, demcr | CortexM.DEMCR_VC_CORERESET)
  uint demcr = pico_read32(pp,DEMCR_ADD); 
  logmsg("demcr %08x",demcr);
  CHKSEQ(s + 8);
// set_reset_catch
  r = pico_write32(pp,DEMCR_ADD,demcr | DEMCR_VC_CORERESET);
  CHKSEQ(s + 12);
// 559
// _perform_reset cortex_m
// TODO TODO TODO ++
// invalidate CSW cache TODO check a better method
  pdap->regs[DAP_MEMAP_CSW].vals = 0;
  write_dp_reg(pp,DAP_DP_SELECT,0); // restore DPBANKSEL to 0 
// TODO --
  int mask = 4;
  r = pico_write32(pp,AIRCR_ADD,AIRCR_VECTKEY | mask);
  CHKSEQ(s + 21);
// post_reset_recovery dap.py
  uint sc = read_dp_reg(pp,DAP_DP_CTRL_STAT);
  logmsg("ctrl_stat %08x",sc);
// _post_reset_core_accessibility_test cortex_m.py
  CHKSEQ(s + 25);
  for (int i = 0; i < 10; i++)
  {
    uint dhcsr = pico_read32(pp,DHCSR_ADD); 
    logmsg("dhcsr.%d %08x",i,dhcsr);
    if ((dhcsr & DHCSR_S_RESET_ST) == 0)
      break;
  }
  CHKSEQ(s + 33);
// _post_reset_core_accessibility_test exit cortex_m.py
// return in reset_and_halt

// TODO TODO TODO ++
// invalidate CSW cache TODO check a better method
  pdap->regs[DAP_MEMAP_CSW].vals = 0;
  write_dp_reg(pp,DAP_DP_SELECT,0); // restore DPBANKSEL to 0 
// TODO --

  int st = pico_get_state(pp);
  logmsg("st %d",st);
  CHKSEQ(s + 42);
// clear_reset_catch
  demcr = pico_read32(pp,DEMCR_ADD); 
  logmsg("demcr %08x",demcr);
  if (demcr & DEMCR_VC_CORERESET)
    pico_write32(pp,DEMCR_ADD,demcr & ~DEMCR_VC_CORERESET); 
  CHKSEQ(s + 50);
  st = pico_get_state(pp);
  logmsg("st %d",st);
  CHKSEQ(s + 54);
// _check_t_bit cortex_m.py
  int rid[] = {16};
  uint vals[1];
  pico_read_regs(pp,rid,SIZE(rid),vals,0);
    
  return r;
}

void pico_is_debug_trap(struct t_picoprobe *pp)
{
  int s = get_count();
  uint dfsr = pico_read32(pp,DFSR_ADD); 
  logmsg("dfsr %08x",dfsr);
  CHKSEQ(s + 4);

// D read_core_registers_raw entry reg_list ['r7', 'sp', 'lr', 'pc'] [register]
  uint vals[4];

  int rid[] = {15,13,14,7};
  pico_read_regs(pp,rid,SIZE(rid),vals,1);
  CHKSEQ(s + 60);
}

void pico_trace(struct t_picoprobe *pp,int nstep)
{
  struct t_libusb *plu = pp->plu;

  if (plu->log)
    logmsg("pico_trace entry");
  pico_halt_core(pp);
  uint dfsr = pico_read32(pp,DFSR_ADD); 
  logmsg("trace: dfsr %08x",dfsr);

  int rid[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
  uint regs[64];
  uint *pr0 = regs;
  uint *pr1 = regs + 32;

  pico_read_regs(pp,rid,SIZE(rid),pr0,1);

  for (int i = 0; i < SIZE(rid); i++)
    printf("R%02d: %08x\n",rid[i],pr0[i]);
  pico_reset_halt(pp);

  int st = pico_get_state(pp);
  logmsg("st %d",st);
  pico_read_regs(pp,rid,SIZE(rid),pr0,1);

  for (int i = 0; i < SIZE(rid); i++)
    printf("R%02d: %08x\n",rid[i],pr0[i]);

  for (int i = 0; i < nstep; i++)
  {
    pico_read_regs(pp,rid,SIZE(rid),pr1,1);
    printf("%5d %08x\n",i,pr1[15]);

    for (int i = 0; i < SIZE(rid); i++)
      if (i != 15 && pr1[i] != pr0[i])
        printf("         R%02d: %08x\n",rid[i],pr1[i]);
    if (pr0 == regs)
      pr1 = regs, pr0 = regs + 32;
    else
      pr0 = regs, pr1 = regs + 32;
    pico_step(pp);
  }

  if (plu->log)
    logmsg("pico_trace exit");
}

// first part (up to 621) equal to pico_test_dbg1
// after try to use a breakpoint

void pico_test_dbg2(struct t_picoprobe *pp)
{
  struct t_libusb *plu = pp->plu;
  uint vals[80];

  if (plu->log)
    logmsg("pico_test_dbg1 entry");
// 207
// I GDB server started on port 3333 (core 0) [gdbserver]
// D run connection done now halt target [gdbserver]
  pico_halt_core(pp);
  CHKSEQ(211);
// 211
/*
gdb input:

set trace-commands on
set debug remote 1
target extended :3333
info reg
monit reset halt
break *0x00002518
cont
info reg
stepi
info reg
quit


+set debug remote 1
+target extended :3333

D packet b'$qSupported:multiprocess+;swbreak+;hwbreak+;qRelocInsn+;fork-events+;vfork-events+;exec-events+;vContSupported+;QThreadEvents+;no-resumed+#df' [gdbserver]
D resp b'$qXfer:features:read+;QStartNoAckMode+;qXfer:threads:read+;QNonStop+;PacketSize=800;qXfer:memory-map:read+#28' [gdbserver]
D packet b'$vMustReplyEmpty#3a' [gdbserver]
D resp b'$#00' [gdbserver]
D packet b'$QStartNoAckMode#b0' [gdbserver]
D resp b'$OK#9a' [gdbserver]
D packet b'$Hg0#df' [gdbserver]
D resp b'$OK#9a' [gdbserver]
D packet b'$qXfer:features:read:target.xml:0,7fb#4a' [gdbserver]
D resp b'$l<?xml version="1.0"?>\n<!DOCTYPE feature SYSTEM "gdb-target.dtd">\n<target><feature name="org.gnu.gdb.arm.m-profile"><reg name="r0" bitsize="32" type="int" group="general" regnum="0" /><reg name="r1" bitsize="32" type="int" group="general" regnum="1" /><reg name="r2" bitsize="32" type="int" group="general" regnum="2" /><reg name="r3" bitsize="32" type="int" group="general" regnum="3" /><reg name="r4" bitsize="32" type="int" group="general" regnum="4" /><reg name="r5" bitsize="32" type="int" group="general" regnum="5" /><reg name="r6" bitsize="32" type="int" group="general" regnum="6" /><reg name="r7" bitsize="32" type="int" group="general" regnum="7" /><reg name="r8" bitsize="32" type="int" group="general" regnum="8" /><reg name="r9" bitsize="32" type="int" group="general" regnum="9" /><reg name="r10" bitsize="32" type="int" group="general" regnum="10" /><reg name="r11" bitsize="32" type="int" group="general" regnum="11" /><reg name="r12" bitsize="32" type="int" group="general" regnum="12" /><reg name="sp" bitsize="32" type="data_ptr" group="general" regnum="13" /><reg name="lr" bitsize="32" type="code_ptr" group="general" regnum="14" /><reg name="pc" bitsize="32" type="code_ptr" group="general" regnum="15" /><reg name="msp" bitsize="32" type="data_ptr" group="system" regnum="16" /><reg name="psp" bitsize="32" type="data_ptr" group="system" regnum="17" /><reg name="primask" bitsize="32" type="int" group="system" regnum="18" /><reg name="xpsr" bitsize="32" type="int" group="general" regnum="19" /><reg name="control" bitsize="32" type="int" group="system" regnum="20" /></feature></target>#ed' [gdbserver]
D packet b'$QNonStop:0#8c' [gdbserver]
D resp b'$OK#9a' [gdbserver]
D packet b'$qTStatus#49' [gdbserver]
D resp b'$#00' [gdbserver]
D packet b'$?#3f' [gdbserver]
*/
// 212 .. 271
// is_debug_trap 
  uint dfsr = pico_read32(pp,DFSR_ADD); 
  logmsg("dfsr %08x",dfsr);

// D read_core_registers_raw entry reg_list ['r7', 'sp', 'lr', 'pc'] [register]

  int rid[] = {15,13,14,7};
  pico_read_regs(pp,rid,SIZE(rid),vals,1);
  CHKSEQ(271);
/*
D resp b'$T0507:00000000;0d:a81f0420;0e:9f0f0010;0f:f80c0010;thread:1;#2f' [gdbserver]
D packet b'$qXfer:threads:read::0,7fb#d0' [gdbserver]
*/
// 272 .. 307
// get_threads_xml
//   .. is_target_in_reset
// get_state() == Target.State.RESET
  int run = is_running(pp);
  logmsg("is_running %d",run);
// exception_name
// ipsr = self.target_context.read_core_register('ipsr')
  int rid1[] = {16,16};
  pico_read_regs(pp,rid1,SIZE(rid1),vals,1);
  CHKSEQ(307);
/*
D resp b'$l<?xml version="1.0"?><!DOCTYPE feature SYSTEM "threads.dtd"><threads><thread id="1">Thread</thread></threads>#6b' [gdbserver]
D packet b'$qAttached#8f' [gdbserver]
D resp b'$1#31' [gdbserver]
D packet b'$Hc-1#09' [gdbserver]
D resp b'$OK#9a' [gdbserver]
D packet b'$qOffsets#4b' [gdbserver]
D resp b'$Text=0;Data=0;Bss=0#04' [gdbserver]
D packet b'$qXfer:threads:read::0,7fb#d0' [gdbserver]
*/
// 308 .. 315
  run = is_running(pp);
  logmsg("is_running %d",run);
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(307);
/*
D resp b'$l<?xml version="1.0"?><!DOCTYPE feature SYSTEM "threads.dtd"><threads><thread id="1">Thread</thread></threads>#6b' [gdbserver]
D packet b'$qXfer:memory-map:read::0,7fb#e9' [gdbserver]
D resp b'$l<?xml version="1.0"?>\n<!DOCTYPE memory-map PUBLIC "+//IDN gnu.org//DTD GDB Memory Map V1.0//EN" "http://sourceware.org/gdb/gdb-memory-map.dtd">\n<memory-map><memory type="rom" start="0x0" length="0x4000" /><memory type="flash" start="0x10000000" length="0x1000000"><property name="blocksize">0x1000</property></memory><memory type="rom" start="0x11000000" length="0x1000000" /><memory type="rom" start="0x12000000" length="0x1000000" /><memory type="rom" start="0x13000000" length="0x1000000" /><memory type="ram" start="0x20000000" length="0x40000" /><memory type="ram" start="0x20040000" length="0x2000" /><memory type="ram" start="0x21000000" length="0x40000" /><memory type="ram" start="0x51000000" length="0x1000" /></memory-map>#15' [gdbserver]
D packet b'$m10000cf8,4#bf' [gdbserver]
*/
// 316 .. 323
// handler get_memory
// read_memory_block8 memory.py

  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(319);
  pico_read_block32(pp,0x10000cf8,vals,1); // TODO symbolize address from packet b'$m10000cf8,2#bd'
  logmsg("vals[0] %08x",vals[0]);
  CHKSEQ(323);

/*
D resp b'$059a0c4b#28' [gdbserver]
D packet b'$g#67' [gdbserver]
*/
// 324 .. 535
// gdbserver.py", line 927, in get_registers
  int rid2[] = {0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 17, 18, 20, 20, 20};
  pico_read_regs(pp,rid2,SIZE(rid2),vals,1);
  CHKSEQ(535);

/*
D resp b'$2eafc37eb27fc77e000000000000000089d0030000000000b87fc77e00000000fffffffffffffffffffffffffffffffff4030020a81f04209f0f0010f80c0010a81f0420fcffffff000000000000006100000000#33' [gdbserver]
D packet b'$m10000cf8,2#bd' [gdbserver]
*/
// 536 .. 539
  run = is_running(pp);
  logmsg("is_running %d",run);
// replay probably in cache
  CHKSEQ(539);

/*
D resp b'$059a#ff' [gdbserver]
D packet b'$m10000c40,40#b5' [gdbserver]
*/
// 540 .. 547
  run = is_running(pp);
  logmsg("is_running %d",run);
  pico_read_block32(pp,0x10000c40,vals,16); // TODO symbolize address 
  CHKSEQ(547);

/*
D resp b'$f5e7c046e44100107c400010e0420010044100101840001020410010f0b587b006000f00fff7fffa00282ad106246442e517a4197d41bd4202d803d1b44201d9#9f' [gdbserver]
D packet b'$qSymbol::#5b' [gdbserver]
D resp b'$OK#9a' [gdbserver]

+ info reg
+monit reset halt


D packet b'$qRcmd,72657365742068616c74#72' [gdbserver]

*/
// 548 .. 621 
  pico_reset_halt(pp);
  CHKSEQ(617);
  int st = pico_get_state(pp);
  logmsg("st %d",st);
  CHKSEQ(621);

/*
D resp b'$526573657474696e672074617267657420776974682068616c740a5375636365737366756c6c792068616c74656420646576696365206f6e2072657365740a#ae' [gdbserver]

+break *0x00002518
D packet b'$m2518,2#9b' [gdbserver]

*/

// 622 .. 631

  run = is_running(pp);
  logmsg("is_running %d",run);

  pico_read16(pp,0x00002518); // TODO symbolize address from packet
  CHKSEQ(631);

/*

D resp b'$d020#f6' [gdbserver]

+cont

D packet b'$Z1,2518,2#e5' [gdbserver]
D resp b'$OK#9a' [gdbserver]
D packet b'$vCont?#49' [gdbserver]
D resp b'$vCont;c;C;s;S;r;t#be' [gdbserver]
D packet b'$vCont;c#a8' [gdbserver]

*/

// 632 .. 725
  run = is_running(pp);
  logmsg("is_running %d",run);

  CHKSEQ(637); // 6 cycle: pass from 16 to 32 bit access
// enable in fpb.py
  pico_write32(pp,FP_CTRL_ADD,FP_CTRL_KEY_MASK | FP_CTRL_EN_MASK);
// set breakpoint in fpb.py
  pico_write32(pp,0xe0002008,0x40002519); // TODO fix
  CHKSEQ(645); 
// clear_debug_cause_bits [cortex_m]
  pico_write32(pp,DFSR_ADD,DFSR_EXTERNAL | DFSR_VCATCH |
                              DFSR_DWTTRAP  | DFSR_BKPT   |
                              DFSR_HALTED);
  CHKSEQ(649); 
// resume in cortex_m.py
  pico_write32(pp,DHCSR_ADD,DHCSR_DBGKEY | DHCSR_C_DEBUGEN); 
  CHKSEQ(653); 
  st = pico_get_state(pp);
  logmsg("st %d",st);
  int rid3[] = {15};
  pico_read_regs(pp,rid2,SIZE(rid3),vals,1);
  CHKSEQ(677); 
// get_t_response [gdbserver]
// is_debug_trap [cortex_m]
//pico_is_debug_trap(pp); // use diffrent register list
  dfsr = pico_read32(pp,DFSR_ADD); 
  logmsg("dfsr %08x",dfsr);

  int rid4[] = {13,14,7}; // r15 already read {15,13,14,7};
  pico_read_regs(pp,rid4,SIZE(rid4),vals,1);

  CHKSEQ(725); 

/*
D resp b'$T0507:2c800540;0d:e81e0420;0e:27010000;0f:18250000;thread:1;#aa' [gdbserver]
D packet b'$qXfer:threads:read::0,7fb#d0' [gdbserver]
*/

// 726 .. 761
// get_threads_xml
//   .. is_target_in_reset
// get_state() == Target.State.RESET
  run = is_running(pp);
  logmsg("is_running %d",run);
// exception_name
// ipsr = self.target_context.read_core_register('ipsr')
  pico_read_regs(pp,rid1,SIZE(rid1),vals,1);
  CHKSEQ(761);
/*
D resp b'$l<?xml version="1.0"?><!DOCTYPE feature SYSTEM "threads.dtd"><threads><thread id="1">Thread</thread></threads>#6b' [gdbserver]
D packet b'$z1,2518,2#05' [gdbserver]
D resp b'$OK#9a' [gdbserver]
D packet b'$m2518,4#9d' [gdbserver]

*/

// 762 .. 769
  run = is_running(pp);
  logmsg("is_running %d",run);
  pico_read_block32(pp,0x00002518,vals,1); // TODO symbolize address 
  CHKSEQ(769);

/*
D resp b'$d0200924#c5' [gdbserver]
D packet b'$g#67' [gdbserver]
*/

// 770 .. 981
  pico_read_regs(pp,rid2,SIZE(rid2),vals,1);

/*
D resp b'$00000000000000000200000000000000ffffffffffffffffd3c007b02c800540fffffffffffffffffffffffffffffffffb000000e81e04202701000018250000e81e0420fcffffff000000000000006100000000#b3' [gdbserver]
D packet b'$m2518,2#9b' [gdbserver]
*/
// 982..985
  run = is_running(pp);
  logmsg("is_running %d",run);
/* not done
  pico_read_block32(pp,0x00002518,vals,1); // TODO symbolize address 
*/
  CHKSEQ(985);

/*
D resp b'$d020#f6' [gdbserver]

Breakpoint 1, 0x00002518 in ?? ()
+info reg

+stepi
D packet b'$vCont;s:1#23' [gdbserver]
*/
// 986 .. 1073
  uint dhcsr = pico_read32(pp,DHCSR_ADD);
  if (plu->log)
    logmsg("dhcsr %08x",dhcsr);
// remove breakpoint
  pico_write32(pp,0xe0002008,0); 
// clear_debug_cause_bits [cortex_m]
  pico_write32(pp,DFSR_ADD,DFSR_EXTERNAL | DFSR_VCATCH |
                              DFSR_DWTTRAP  | DFSR_BKPT   |
                              DFSR_HALTED);
  pico_step1(pp,dhcsr);
  CHKSEQ(1013);
  pico_is_debug_trap(pp);
  CHKSEQ(1073);

/*
D resp b'$T0507:2c800540;0d:e81e0420;0e:27010000;0f:1a250000;thread:1;#d3' [gdbserver]
D packet b'$m251a,4#c6' [gdbserver]

*/
// 1074 .. 1087
  st = pico_get_state(pp);
  pico_read16(pp,0x0000251a); // TODO symbolize address from packet
  pico_read16(pp,0x0000251c); // TODO symbolize address from packet
}

void pico_test_dbg1(struct t_picoprobe *pp)
{
  struct t_libusb *plu = pp->plu;
  uint vals[80];

  if (plu->log)
    logmsg("pico_test_dbg1 entry");
// 207
// I GDB server started on port 3333 (core 0) [gdbserver]
// D run connection done now halt target [gdbserver]
  pico_halt_core(pp);
  CHKSEQ(211);
// 211
/*
gdb input:

set debug remote 1
target extended :3333
info reg
monit reset halt
stepi
info reg
quit


+set debug remote 1
+target extended :3333

D packet b'$qSupported:multiprocess+;swbreak+;hwbreak+;qRelocInsn+;fork-events+;vfork-events+;exec-events+;vContSupported+;QThreadEvents+;no-resumed+#df' [gdbserver]
D resp b'$qXfer:features:read+;QStartNoAckMode+;qXfer:threads:read+;QNonStop+;PacketSize=800;qXfer:memory-map:read+#28' [gdbserver]
D packet b'$vMustReplyEmpty#3a' [gdbserver]
D resp b'$#00' [gdbserver]
D packet b'$QStartNoAckMode#b0' [gdbserver]
D resp b'$OK#9a' [gdbserver]
D packet b'$Hg0#df' [gdbserver]
D resp b'$OK#9a' [gdbserver]
D packet b'$qXfer:features:read:target.xml:0,7fb#4a' [gdbserver]
D resp b'$l<?xml version="1.0"?>\n<!DOCTYPE feature SYSTEM "gdb-target.dtd">\n<target><feature name="org.gnu.gdb.arm.m-profile"><reg name="r0" bitsize="32" type="int" group="general" regnum="0" /><reg name="r1" bitsize="32" type="int" group="general" regnum="1" /><reg name="r2" bitsize="32" type="int" group="general" regnum="2" /><reg name="r3" bitsize="32" type="int" group="general" regnum="3" /><reg name="r4" bitsize="32" type="int" group="general" regnum="4" /><reg name="r5" bitsize="32" type="int" group="general" regnum="5" /><reg name="r6" bitsize="32" type="int" group="general" regnum="6" /><reg name="r7" bitsize="32" type="int" group="general" regnum="7" /><reg name="r8" bitsize="32" type="int" group="general" regnum="8" /><reg name="r9" bitsize="32" type="int" group="general" regnum="9" /><reg name="r10" bitsize="32" type="int" group="general" regnum="10" /><reg name="r11" bitsize="32" type="int" group="general" regnum="11" /><reg name="r12" bitsize="32" type="int" group="general" regnum="12" /><reg name="sp" bitsize="32" type="data_ptr" group="general" regnum="13" /><reg name="lr" bitsize="32" type="code_ptr" group="general" regnum="14" /><reg name="pc" bitsize="32" type="code_ptr" group="general" regnum="15" /><reg name="msp" bitsize="32" type="data_ptr" group="system" regnum="16" /><reg name="psp" bitsize="32" type="data_ptr" group="system" regnum="17" /><reg name="primask" bitsize="32" type="int" group="system" regnum="18" /><reg name="xpsr" bitsize="32" type="int" group="general" regnum="19" /><reg name="control" bitsize="32" type="int" group="system" regnum="20" /></feature></target>#ed' [gdbserver]
D packet b'$QNonStop:0#8c' [gdbserver]
D resp b'$OK#9a' [gdbserver]
D packet b'$qTStatus#49' [gdbserver]
D resp b'$#00' [gdbserver]
D packet b'$?#3f' [gdbserver]
*/
// 212 .. 271
// is_debug_trap 
  uint dfsr = pico_read32(pp,DFSR_ADD); 
  logmsg("dfsr %08x",dfsr);

// D read_core_registers_raw entry reg_list ['r7', 'sp', 'lr', 'pc'] [register]

  int rid[] = {15,13,14,7};
  pico_read_regs(pp,rid,SIZE(rid),vals,1);
  CHKSEQ(271);
/*
D resp b'$T0507:00000000;0d:a81f0420;0e:9f0f0010;0f:f80c0010;thread:1;#2f' [gdbserver]
D packet b'$qXfer:threads:read::0,7fb#d0' [gdbserver]
*/
// 272 .. 307
// get_threads_xml
//   .. is_target_in_reset
// get_state() == Target.State.RESET
  int run = is_running(pp);
  logmsg("is_running %d",run);
// exception_name
// ipsr = self.target_context.read_core_register('ipsr')
  int rid1[] = {16,16};
  pico_read_regs(pp,rid1,SIZE(rid1),vals,1);
  CHKSEQ(307);
/*
D resp b'$l<?xml version="1.0"?><!DOCTYPE feature SYSTEM "threads.dtd"><threads><thread id="1">Thread</thread></threads>#6b' [gdbserver]
D packet b'$qAttached#8f' [gdbserver]
D resp b'$1#31' [gdbserver]
D packet b'$Hc-1#09' [gdbserver]
D resp b'$OK#9a' [gdbserver]
D packet b'$qOffsets#4b' [gdbserver]
D resp b'$Text=0;Data=0;Bss=0#04' [gdbserver]
D packet b'$qXfer:threads:read::0,7fb#d0' [gdbserver]
*/
// 308 .. 315
  run = is_running(pp);
  logmsg("is_running %d",run);
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(307);
/*
D resp b'$l<?xml version="1.0"?><!DOCTYPE feature SYSTEM "threads.dtd"><threads><thread id="1">Thread</thread></threads>#6b' [gdbserver]
D packet b'$qXfer:memory-map:read::0,7fb#e9' [gdbserver]
D resp b'$l<?xml version="1.0"?>\n<!DOCTYPE memory-map PUBLIC "+//IDN gnu.org//DTD GDB Memory Map V1.0//EN" "http://sourceware.org/gdb/gdb-memory-map.dtd">\n<memory-map><memory type="rom" start="0x0" length="0x4000" /><memory type="flash" start="0x10000000" length="0x1000000"><property name="blocksize">0x1000</property></memory><memory type="rom" start="0x11000000" length="0x1000000" /><memory type="rom" start="0x12000000" length="0x1000000" /><memory type="rom" start="0x13000000" length="0x1000000" /><memory type="ram" start="0x20000000" length="0x40000" /><memory type="ram" start="0x20040000" length="0x2000" /><memory type="ram" start="0x21000000" length="0x40000" /><memory type="ram" start="0x51000000" length="0x1000" /></memory-map>#15' [gdbserver]
D packet b'$m10000cf8,4#bf' [gdbserver]
*/
// 316 .. 323
// handler get_memory
// read_memory_block8 memory.py

  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(319);
  pico_read_block32(pp,0x10000cf8,vals,1); // TODO symbolize address from packet b'$m10000cf8,2#bd'
  logmsg("vals[0] %08x",vals[0]);
  CHKSEQ(323);

/*
D resp b'$059a0c4b#28' [gdbserver]
D packet b'$g#67' [gdbserver]
*/
// 324 .. 535
// gdbserver.py", line 927, in get_registers
  int rid2[] = {0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 17, 18, 20, 20, 20};
  pico_read_regs(pp,rid2,SIZE(rid2),vals,1);
  CHKSEQ(535);

/*
D resp b'$2eafc37eb27fc77e000000000000000089d0030000000000b87fc77e00000000fffffffffffffffffffffffffffffffff4030020a81f04209f0f0010f80c0010a81f0420fcffffff000000000000006100000000#33' [gdbserver]
D packet b'$m10000cf8,2#bd' [gdbserver]
*/
// 536 .. 539
  run = is_running(pp);
  logmsg("is_running %d",run);
// replay probably in cache
  CHKSEQ(539);

/*
D resp b'$059a#ff' [gdbserver]
D packet b'$m10000c40,40#b5' [gdbserver]
*/
// 540 .. 547
  run = is_running(pp);
  logmsg("is_running %d",run);
  pico_read_block32(pp,0x10000c40,vals,16); // TODO symbolize address 
  CHKSEQ(547);

/*
D resp b'$f5e7c046e44100107c400010e0420010044100101840001020410010f0b587b006000f00fff7fffa00282ad106246442e517a4197d41bd4202d803d1b44201d9#9f' [gdbserver]
D packet b'$qSymbol::#5b' [gdbserver]
D resp b'$OK#9a' [gdbserver]

+ info reg
+monit reset halt


D packet b'$qRcmd,72657365742068616c74#72' [gdbserver]

*/
// 548 .. 621 
  pico_reset_halt(pp);
  CHKSEQ(617);
  int st = pico_get_state(pp);
  logmsg("st %d",st);
  CHKSEQ(621);
/*
D resp b'$526573657474696e672074617267657420776974682068616c740a5375636365737366756c6c792068616c7465642
0646576696365206f6e2072657365740a#ae' [gdbserver]

+stepi

D packet b'$vCont?#49' [gdbserver]
D resp b'$vCont;c;C;s;S;r;t#be' [gdbserver]
D packet b'$vCont;s:1;c#c1' [gdbserver]
*/
// 622 .. 705

  pico_step(pp);
  pico_is_debug_trap(pp);
/*
D resp b'$T0507:ffffffff;0d:001f0420;0e:ffffffff;0f:f0000000;thread:1;#a4' [gdbserver]
D packet b'$mf0,4#63' [gdbserver]

*/
// 706 .. 713
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(709);

  pico_read_block32(pp,0x000000f0,vals,1); // TODO symbolize address 
  CHKSEQ(713);
/*
D resp b'$01680029#9a' [gdbserver]
D packet b'$g#67' [gdbserver]
*/
// 714 .. 937

  int rid3[] = {0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 16,17, 18, 20, 20, 20};
  pico_read_regs(pp,rid3,SIZE(rid3),vals,1);
  CHKSEQ(937);
/*
D resp b'$000000d0ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff001f0420fffffffff0000000001f0420fcffffff00000000000000f100000000#38' [gdbserver]
D packet b'$mf0,2#61' [gdbserver]
*/
// 938 .. 941

  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(941);
/*
D resp b'$0168#cf' [gdbserver]
D packet b'$qXfer:threads:read::0,7fb#d0' [gdbserver]
*/
// 942 .. 949
  run = is_running(pp);
  logmsg("is_running %d",run);
  run = is_running(pp);
// register cached
  CHKSEQ(949);
/*
D resp b'$l<?xml version="1.0"?><!DOCTYPE feature SYSTEM "threads.dtd"><threads><thread id="1">Thread</thread></threads>#6b' [gdbserver]
D packet b'$mf0,4#63' [gdbserver]
*/
// 950 ..953

  pico_read_block32(pp,0x000000f0,vals,1); // TODO symbolize address 
  CHKSEQ(953);

/*
D resp b'$01680029#9a' [gdbserver]
D packet b'$mf0,2#61' [gdbserver]
*/
// 954..957
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(957);

/*
D resp b'$0168#cf' [gdbserver]

+info reg
+quit

D packet b'$D#44' [gdbserver]
D resp b'$OK#9a' [gdbserver]

*/

}

void pico_test_dbg(struct t_picoprobe *pp)
{
  struct t_libusb *plu = pp->plu;
  uint vals[80];

  if (plu->log)
    logmsg("pico_test_dbg entry");
// 207
// I GDB server started on port 3333 (core 0) [gdbserver]
// D run connection done now halt target [gdbserver]
  pico_halt_core(pp);
  CHKSEQ(211);
// 211
/*
D packet b'$qSupported:multiprocess+;swbreak+;hwbreak+;qRelocInsn+;fork-events+;vfork-events+;exec-events+;vContSupported+;QThreadEvents+;no-resumed+#df' [gdbserver]
D resp b'$qXfer:features:read+;QStartNoAckMode+;qXfer:threads:read+;QNonStop+;PacketSize=800;qXfer:memory-map:read+#28' [gdbserver]
D packet b'$vMustReplyEmpty#3a' [gdbserver]
D resp b'$#00' [gdbserver]
D packet b'$QStartNoAckMode#b0' [gdbserver]
D resp b'$OK#9a' [gdbserver]
D packet b'$Hg0#df' [gdbserver]
D resp b'$OK#9a' [gdbserver]
D packet b'$qXfer:features:read:target.xml:0,7fb#4a' [gdbserver]
D resp b'$l<?xml version="1.0"?>\n<!DOCTYPE feature SYSTEM "gdb-target.dtd">\n<target><feature name="org.gnu.gdb.arm.m-profile"><reg name="r0" bitsize="32" type="int" group="general" regnum="0" /><reg name="r1" bitsize="32" type="int" group="general" regnum="1" /><reg name="r2" bitsize="32" type="int" group="general" regnum="2" /><reg name="r3" bitsize="32" type="int" group="general" regnum="3" /><reg name="r4" bitsize="32" type="int" group="general" regnum="4" /><reg name="r5" bitsize="32" type="int" group="general" regnum="5" /><reg name="r6" bitsize="32" type="int" group="general" regnum="6" /><reg name="r7" bitsize="32" type="int" group="general" regnum="7" /><reg name="r8" bitsize="32" type="int" group="general" regnum="8" /><reg name="r9" bitsize="32" type="int" group="general" regnum="9" /><reg name="r10" bitsize="32" type="int" group="general" regnum="10" /><reg name="r11" bitsize="32" type="int" group="general" regnum="11" /><reg name="r12" bitsize="32" type="int" group="general" regnum="12" /><reg name="sp" bitsize="32" type="data_ptr" group="general" regnum="13" /><reg name="lr" bitsize="32" type="code_ptr" group="general" regnum="14" /><reg name="pc" bitsize="32" type="code_ptr" group="general" regnum="15" /><reg name="msp" bitsize="32" type="data_ptr" group="system" regnum="16" /><reg name="psp" bitsize="32" type="data_ptr" group="system" regnum="17" /><reg name="primask" bitsize="32" type="int" group="system" regnum="18" /><reg name="xpsr" bitsize="32" type="int" group="general" regnum="19" /><reg name="control" bitsize="32" type="int" group="system" regnum="20" /></feature></target>#ed' [gdbserver]
D packet b'$QNonStop:0#8c' [gdbserver]
D resp b'$OK#9a' [gdbserver]
D packet b'$qTStatus#49' [gdbserver]
D resp b'$#00' [gdbserver]
D packet b'$?#3f' [gdbserver]
*/
// 212 .. 271
// is_debug_trap 
  uint dfsr = pico_read32(pp,DFSR_ADD); 
  logmsg("dfsr %08x",dfsr);

// D read_core_registers_raw entry reg_list ['r7', 'sp', 'lr', 'pc'] [register]

  int rid[] = {15,13,14,7};
  pico_read_regs(pp,rid,SIZE(rid),vals,1);
  CHKSEQ(271);
/*
D resp b'$T0507:00000000;0d:a81f0420;0e:9f0f0010;0f:f80c0010;thread:1;#2f' [gdbserver]
D packet b'$qXfer:threads:read::0,7fb#d0' [gdbserver]
*/
// 272 .. 307
// get_threads_xml
//   .. is_target_in_reset
// get_state() == Target.State.RESET
  int run = is_running(pp);
  logmsg("is_running %d",run);
// exception_name
// ipsr = self.target_context.read_core_register('ipsr')
  int rid1[] = {16,16};
  pico_read_regs(pp,rid1,SIZE(rid1),vals,1);
  CHKSEQ(307);
/*
D resp b'$l<?xml version="1.0"?><!DOCTYPE feature SYSTEM "threads.dtd"><threads><thread id="1">Thread</thread></threads>#6b' [gdbserver]
D packet b'$qAttached#8f' [gdbserver]
D resp b'$1#31' [gdbserver]
D packet b'$Hc-1#09' [gdbserver]
D resp b'$OK#9a' [gdbserver]
D packet b'$qOffsets#4b' [gdbserver]
D resp b'$Text=0;Data=0;Bss=0#04' [gdbserver]
D packet b'$qXfer:threads:read::0,7fb#d0' [gdbserver]
*/
// 308 .. 315
  run = is_running(pp);
  logmsg("is_running %d",run);
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(307);
/*
D resp b'$l<?xml version="1.0"?><!DOCTYPE feature SYSTEM "threads.dtd"><threads><thread id="1">Thread</thread></threads>#6b' [gdbserver]
D packet b'$qXfer:memory-map:read::0,7fb#e9' [gdbserver]
D resp b'$l<?xml version="1.0"?>\n<!DOCTYPE memory-map PUBLIC "+//IDN gnu.org//DTD GDB Memory Map V1.0//EN" "http://sourceware.org/gdb/gdb-memory-map.dtd">\n<memory-map><memory type="rom" start="0x0" length="0x4000" /><memory type="flash" start="0x10000000" length="0x1000000"><property name="blocksize">0x1000</property></memory><memory type="rom" start="0x11000000" length="0x1000000" /><memory type="rom" start="0x12000000" length="0x1000000" /><memory type="rom" start="0x13000000" length="0x1000000" /><memory type="ram" start="0x20000000" length="0x40000" /><memory type="ram" start="0x20040000" length="0x2000" /><memory type="ram" start="0x21000000" length="0x40000" /><memory type="ram" start="0x51000000" length="0x1000" /></memory-map>#15' [gdbserver]
D packet b'$m10000cf8,4#bf' [gdbserver]
*/
// 316 .. 323
// handler get_memory
// read_memory_block8 memory.py

  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(319);
  pico_read_block32(pp,0x10000cf8,vals,1); // TODO symbolize address from packet b'$m10000cf8,2#bd'
  logmsg("vals[0] %08x",vals[0]);
  CHKSEQ(323);

/*
D resp b'$059a0c4b#28' [gdbserver]
D packet b'$g#67' [gdbserver]
*/
// 324 .. 535
// gdbserver.py", line 927, in get_registers
  int rid2[] = {0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 17, 18, 20, 20, 20};
  pico_read_regs(pp,rid2,SIZE(rid2),vals,0);
  CHKSEQ(535);

/*
D resp b'$2eafc37eb27fc77e000000000000000089d0030000000000b87fc77e00000000fffffffffffffffffffffffffffffffff4030020a81f04209f0f0010f80c0010a81f0420fcffffff000000000000006100000000#33' [gdbserver]
D packet b'$m10000cf8,2#bd' [gdbserver]
*/
// 536 .. 539
  run = is_running(pp);
  logmsg("is_running %d",run);
// replay probably in cache
  CHKSEQ(539);

/*
D resp b'$059a#ff' [gdbserver]
D packet b'$m10000c40,40#b5' [gdbserver]
*/
// 540 .. 547
  run = is_running(pp);
  logmsg("is_running %d",run);
  pico_read_block32(pp,0x10000c40,vals,16); // TODO symbolize address 
  CHKSEQ(547);

/*
D resp b'$f5e7c046e44100107c400010e0420010044100101840001020410010f0b587b006000f00fff7fffa00282ad106246442e517a4197d41bd4202d803d1b44201d9#9f' [gdbserver]
D packet b'$qSymbol::#5b' [gdbserver]
D resp b'$OK#9a' [gdbserver]
D packet b'$vCont?#49' [gdbserver]
D resp b'$vCont;c;C;s;S;r;t#be' [gdbserver]
D packet b'$vCont;s:1;c#c1' [gdbserver]
*/
// 548 .. 631

// step entry [cortex_m]

  uint dhcsr = pico_read32(pp,DHCSR_ADD);
  logmsg("dhcsr %08x",dhcsr);
  CHKSEQ(551);
// clear_debug_cause_bits [cortex_m]
  pico_write32(pp,DFSR_ADD,DFSR_EXTERNAL | DFSR_VCATCH | 
                              DFSR_DWTTRAP  | DFSR_BKPT   |
                              DFSR_HALTED); 
  CHKSEQ(555);
// step after # Get current state.  
  uint maskints = dhcsr & DHCSR_C_MASKINTS;
  uint pmov = dhcsr & DHCSR_C_PMOV;
  uint dhcsr_step = DHCSR_DBGKEY | DHCSR_C_DEBUGEN | DHCSR_C_STEP | pmov;
// if (something..) // TODO check 
  pico_write32(pp,DHCSR_ADD,dhcsr_step | DHCSR_C_HALT); 
  CHKSEQ(559);
// step starting loop
  while (1)
  {
    pico_write32(pp,DHCSR_ADD,dhcsr_step); 
    CHKSEQ(563);
    dhcsr = pico_read32(pp,DHCSR_ADD);
    logmsg("dhcsr %08x",dhcsr);
    CHKSEQ(567);
    if (dhcsr & DHCSR_C_HALT)
      break;
    else
      error("unexpected @%d",__LINE__);
  }
// if (something..) // TODO check 
  pico_write32(pp,DHCSR_ADD,DHCSR_DBGKEY | DHCSR_C_DEBUGEN | 
                     DHCSR_C_HALT | maskints | pmov); 

  CHKSEQ(561);
// step exit
// is_debug_trap

  dfsr = pico_read32(pp,DFSR_ADD);
  logmsg("dfsr %08x",dfsr);
  CHKSEQ(575);
// get_t_response

  pico_read_regs(pp,rid,SIZE(rid),vals,1);
  CHKSEQ(631);

/*
D resp b'$T0507:00000000;0d:a81f0420;0e:9f0f0010;0f:fa0c0010;thread:1;#58' [gdbserver]
D packet b'$m10000cfa,4#e8' [gdbserver]
*/
// 632 .. 645
// GDBServer.get_memory

  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(635);
  ushort v = pico_read16(pp,0x10000cfa); // TODO symbolize address from packet
  logmsg("v %04x",v);
  CHKSEQ(641);
  v = pico_read16(pp,0x10000cfc); // TODO symbolize address from packet
  logmsg("v %04x",v);
  CHKSEQ(645);
  
/*
D resp b'$0c4b5b6a#57' [gdbserver]
D packet b'$g#67' [gdbserver]
*/
// 646 .. 871
  int rid6[] = {0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 16, 17, 18, 20, 20, 20};
  pico_read_regs(pp,rid6,SIZE(rid6),vals,1);
  CHKSEQ(871);
/*
D resp b'$2eafc37eb27fc77e000000000000000089d0030000000000b87fc77e00000000fffffffffffffffffffffffffffffffff4030020a81f04209f0f0010fa0c0010a81f0420fcffffff000000000000006100000000#5c' [gdbserver]
D packet b'$m10000cfa,2#e6' [gdbserver]
*/
// 872 .. 875
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(875);
/*
D resp b'$0c4b#29' [gdbserver]
D packet b'$m10000c40,40#b5' [gdbserver]
*/
// 876..883
// read_memory_block8
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(879);
  pico_read_block32(pp,0x10000c40,vals,16); // TODO symbolize address 
  CHKSEQ(883);
/* 
D resp b'$f5e7c046e44100107c400010e0420010044100101840001020410010f0b587b006000f00fff7fffa00282ad106246442e517a4197d41bd4202d803d1b44201d9#9f' [gdbserver]
D packet b'$qXfer:threads:read::0,7fb#d0' [gdbserver]
*/
// 884..891
// exception_name
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(887);
// ipsr = self.target_context.read_core_register('ipsr')
  run = is_running(pp);
  logmsg("is_running %d",run);
//pico_read_regs(pp,rid1,SIZE(rid1),vals,1);
// dont't read reg propably cached
  CHKSEQ(891);

/*
D resp b'$l<?xml version="1.0"?><!DOCTYPE feature SYSTEM "threads.dtd"><threads><thread id="1">Thread</thread></threads>#6b' [gdbserver]
D packet b'$m10000cfa,4#e8' [gdbserver]
*/
// 892..895
// read_memory_block8
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(895);
// read memory cached

/*
D resp b'$0c4b5b6a#57' [gdbserver]
D packet b'$m10000cfa,2#e6' [gdbserver]
*/
// 986..899
// read_memory_block8
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(899);
// read memory cached

/*
D resp b'$0c4b#29' [gdbserver]
D packet b'$vCont;s:1;c#c1' [gdbserver]
*/
// 900..983

// step entry [cortex_m]

  dhcsr = pico_read32(pp,DHCSR_ADD);
  logmsg("dhcsr %08x",dhcsr);
  CHKSEQ(903);
// clear_debug_cause_bits [cortex_m]
  pico_write32(pp,DFSR_ADD,DFSR_EXTERNAL | DFSR_VCATCH | 
                              DFSR_DWTTRAP  | DFSR_BKPT   |
                              DFSR_HALTED); 
  CHKSEQ(907);
// step after # Get current state.  
  maskints = dhcsr & DHCSR_C_MASKINTS;
  pmov = dhcsr & DHCSR_C_PMOV;
  dhcsr_step = DHCSR_DBGKEY | DHCSR_C_DEBUGEN | DHCSR_C_STEP | pmov;
// if (something..) // TODO check 
  pico_write32(pp,DHCSR_ADD,dhcsr_step | DHCSR_C_HALT); 
  CHKSEQ(911);
// step starting loop
  while (1)
  {
    pico_write32(pp,DHCSR_ADD,dhcsr_step); 
    CHKSEQ(915);
    dhcsr = pico_read32(pp,DHCSR_ADD);
    logmsg("dhcsr %08x",dhcsr);
    CHKSEQ(919);
    if (dhcsr & DHCSR_C_HALT)
      break;
    else
      error("unexpected @%d",__LINE__);
  }
// if (something..) // TODO check 
  pico_write32(pp,DHCSR_ADD,DHCSR_DBGKEY | DHCSR_C_DEBUGEN | 
                     DHCSR_C_HALT | maskints | pmov); 

  CHKSEQ(923);
// step exit
// is_debug_trap

  dfsr = pico_read32(pp,DFSR_ADD);
  logmsg("dfsr %08x",dfsr);
  CHKSEQ(927);
// get_t_response

  pico_read_regs(pp,rid,SIZE(rid),vals,1);
  CHKSEQ(983);

/*
D resp b'$T0507:00000000;0d:a81f0420;0e:9f0f0010;0f:fc0c0010;thread:1;#5a' [gdbserver]
D packet b'$m10000cfc,4#ea' [gdbserver]
*/
// 984..991
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(988);
// read memory cached
  pico_read_block32(pp,0x10000cfc,vals,1); // TODO symbolize address 
  CHKSEQ(991);
/*
D resp b'$5b6a9a42#2e' [gdbserver]
D packet b'$g#67' [gdbserver]
*/
// 992..1215
  pico_read_regs(pp,rid6,SIZE(rid6),vals,1);
  CHKSEQ(1215);
/*
D resp b'$a2bf545a2690585a000000000040054089d00300000000002c90585a00000000fffffffffffffffffffffffffffffffff4030020a81f04209f0f0010fc0c0010a81f0420fcffffff000000000000006100000000#36' [gdbserver]
D packet b'$m10000cfc,2#e8' [gdbserver]
*/
// 1216..1219

  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(1219);
/*
D resp b'$5b6a#2e' [gdbserver]
D packet b'$m10000c40,40#b5' [gdbserver]

*/
// 1220..1227
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(1224);
  pico_read_block32(pp,0x10000c40,vals,16); // TODO symbolize address 
  CHKSEQ(1227);

/*
D resp b'$f5e7c046e44100107c400010e0420010044100101840001020410010f0b587b006000f00fff7fffa00282ad106246442e517a4197d41bd4202d803d1b44201d9#9f' [gdbserver]
D packet b'$qXfer:threads:read::0,7fb#d0' [gdbserver]
*/
// 1228..1235
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(1231);
// read register cached
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(1235);
/*
D resp b'$l<?xml version="1.0"?><!DOCTYPE feature SYSTEM "threads.dtd"><threads><thread id="1">Thread</thread></threads>#6b' [gdbserver]
D packet b'$m10000cfc,4#ea' [gdbserver]
*/
// 1236..1239
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(1239);
/*
D resp b'$5b6a9a42#2e' [gdbserver]
D packet b'$m10000cfc,2#e8' [gdbserver]
*/
// 1240..1243
  run = is_running(pp);
  logmsg("is_running %d",run);
  CHKSEQ(1243);

  if (plu->log)
    logmsg("pico_test_dbg exit");
}

// see pyocd/target/builtin/target_RP2040.py
//     pyocd/probe/swj.py
//       dormant_to_swd

#define B(b) buf[i++] = (b);
void pico_connect(struct t_picoprobe *pp)
{
//int   res;
  struct t_libusb *plu = pp->plu;
  uchar buf[64];

  pico_clear(pp);
  pico_queue(pp,PICO_SPEED,1000,0);
  pico_flush(pp);

// pyocd/target/builtin/target_RP2040.py select_dp
// pyocd/probe/swj.py dormant_to_swd 
// 8 SWDIOTMS cycles high + 128-bit selection alert sequence.
  int i = 0;
  B(0xff); B(0x92); B(0xf3); B(0x09); B(0x62); B(0x95);
  B(0x2d); B(0x85); B(0x86); B(0xe9); B(0xaf); B(0xdd);
  B(0xe3); B(0xa2); B(0x0e); B(0xbc); B(0x19);
  pico_queue(pp,PICO_WRITE,136,buf);
  pico_flush(pp);

//  4-bit SWDIOTMS cycles low + 8-bit SWD activation code.
  i = 0;
  B(0xa0);
  B(0x01);
  pico_queue(pp,PICO_WRITE,12,buf);
  pico_flush(pp);

// SWD line reset (>50 SWDIOTMS cycles high).
  pico_line_reset(pp);
//  >=2 SWDIOTMS cycles low.
  pico_idle_cycles(pp,2);
// dormant_to_swd end

// select_dp in ./target/builtin/target_RP2040.py
// SWD line reset to activate all DPs.
  pico_line_reset(pp);
  pico_idle_cycles(pp,2);

// Send multi-drop SWD target selection sequence to select the requested DP.
/*
 # DP TARGETSEL (add 0xc) write
            # output 8 cycles:
            #   - Start = 1
            #   - APnDP = 0
            #   - RnW = 0
            #   - A[2:3] = 2'b11
            #   - Parity = 0
            #   - Stop = 0
            #   - Park = 1
            # -> LSB first, that's 0b10011001 or 0x99
*/
  i = 0;
  B(0x99);
  pico_queue(pp,PICO_WRITE,8,buf);
  pico_queue(pp,PICO_READ,5,0);
  i = 0;
  B(0x27);
  B(0x29);
  B(0x00);
  B(0x01);
  B(0x00);
  pico_queue(pp,PICO_WRITE,33,buf);
  pico_idle_cycles(pp,2); // implicit flush
  int l;
// receive 5 bit discarding value
  receive(plu,&l,__LINE__);
//printf("res %d l %d\n",res,l);

  uint dpidr = read_dp_reg(pp,DAP_DP_DPIDR);
  logmsg("connect: DPIDR .....: %08x", dpidr);

  write_dp_reg(pp,DAP_DP_SELECT,2); // set DPBANKSEL to reach TARGETID 
  uint targetid = read_dp_reg(pp,DAP_DP_TARGETID);
  logmsg("connect: TARGETID ..: %08x",targetid);

  write_dp_reg(pp,DAP_DP_SELECT,3); // set DPBANKSEL to reach DLPIDR 
  uint dlpidr = read_dp_reg(pp,DAP_DP_DLPIDR);
  logmsg("connect: DLPIDR ....: %08x",dlpidr);

  write_dp_reg(pp,DAP_DP_SELECT,0); // restore DPBANKSEL to 0 
// end of select_dp(self, targetsel)
  dpidr = read_dp_reg(pp,DAP_DP_DPIDR);
// clear sticky error bit
  write_dp_reg(pp,DAP_DP_ABORT,0x1e); // TODO use constants
// power_up_debug in ./coresight/dap.py 
  write_dp_reg(pp,DAP_DP_SELECT,0); // restore DPBANKSEL to 0 
  write_dp_reg(pp,DAP_DP_CTRL_STAT,0x50000f00); // TODO use constants
/*
  printf("DAP_DP_CTRL_STAT %08x\n",ABORT_ORUNERRCLR | ABORT_WDERRCLR |
                                    ABORT_STKERRCLR | ABORT_STKCMPCLR);
  printf("DAP_DP_CTRL_STAT %08x\n", CSYSPWRUPREQ | CDBGPWRUPREQ | TRNNORMAL | MASKLANE
                    | CTRL_STAT_STICKYERR | CTRL_STAT_STICKYCMP | CTRL_STAT_STICKYORUN);
  write_dp_reg(pp,DAP_DP_CTRL_STAT, ABORT_ORUNERRCLR | ABORT_WDERRCLR | 
                                    ABORT_STKERRCLR | ABORT_STKCMPCLR); 
*/

  ulong sc = read_dp_reg(pp,DAP_DP_CTRL_STAT);
  logmsg("ctrl_stat %08x",sc);
  pp->ap = pico_find_ap(pp);
  uint idr  = read_ap_reg(pp,DAP_AP_IDR,0);
  logmsg("idr %08x",idr);
  uint csw0 = read_ap_reg(pp,DAP_MEMAP_CSW,0);
  logmsg("csw %08x",csw0);
  uint cfg  = read_ap_reg(pp,DAP_MEMAP_CFG,0);
  logmsg("cfg %08x",cfg);
// _init_hprot in ap.py
  write_ap_reg(pp,DAP_MEMAP_CSW,csw0 | CSW_HNONSEC_MASK | CSW_HPROT_MASK,1); 
  uint csw = read_ap_reg(pp,DAP_MEMAP_CSW,1);
  logmsg("csw %08x",csw);
  uint rom = pico_rom_base(pp);
// reset CSW
  write_ap_reg(pp,DAP_MEMAP_CSW,csw0,1); 
// end init ap.py

  pico_find_component(pp,rom & ~3); // e000e000
  pico_select_core(pp);
  pico_halt_core(pp);
  pico_set_vector_catch(pp);
// get_state in cortex_m.py
  uint dhcsr = pico_read32(pp,DHCSR_ADD);
  logmsg("dhcsr %08x",dhcsr);
}

#undef B

// power_down_debug in dap.py
void pico_power_down(struct t_picoprobe *pp)
{
  write_dp_reg(pp,DAP_DP_CTRL_STAT, CTRL_STAT_CDBGPWRUPREQ_MASK | 
                                    CTRL_STAT_MASKLANE_MASK | 
                                    CTRL_STAT_TRNNORMAL);
  ulong sc = read_dp_reg(pp,DAP_DP_CTRL_STAT);
  logmsg("1) ctrl_stat %08x",sc);
  write_dp_reg(pp,DAP_DP_CTRL_STAT, CTRL_STAT_MASKLANE_MASK | 
                                    CTRL_STAT_TRNNORMAL);
  sc = read_dp_reg(pp,DAP_DP_CTRL_STAT);
  logmsg("2) ctrl_stat %08x",sc);
}

void pico_disconnect(struct t_picoprobe *pp)
{
// get_state in cortex_m.py
  
  uint dhcsr = pico_read32(pp,DHCSR_ADD);
  logmsg("dhcsr %08x",dhcsr);
//  clear_debug_cause_bitsi in cortex_m.py
  pico_write32(pp,DFSR_ADD,DFSR_EXTERNAL | DFSR_VCATCH | 
                     DFSR_DWTTRAP | DFSR_BKPT | DFSR_HALTED);  
// resume in cortex_m.py
  pico_write32(pp,DHCSR_ADD,DHCSR_DBGKEY | DHCSR_C_DEBUGEN); 
// disconnect in cortex_m.py
// Clear debug controls
  pico_write32(pp,DHCSR_ADD,DHCSR_DBGKEY | 0); 
// Disable other debug blocks
  pico_write32(pp,DEMCR_ADD,0); 

  pico_power_down(pp);
}
