#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
// see note on libusb-1.0
//#include <libusb-1.0/libusb.h>
#include "libusb.h"

#include "env.h"
#include "util.h"
#include "conf.h"
#include "readpcapng-stlink.h"
#include "dap-decode.h"
#include "dapreg.h"
#include "armreg.h"
#include "driver-cmsis.h"
#include "driver-pico.h"

#include "disasm.h"
#include "common-driver.h"
#include "dump.h"
#include "test.h"
#include "jim-interface-cmsis.h"

#include <jim.h>

static const char *strpar(Jim_Interp *interp,
                          int n,int argc,
                          Jim_Obj *const *argv,
                          const char *pname,
                          const char *fname,
                          const char *dval)
{
  const char *v = dval;

  if (argc > n)
  {
    int len;
    v = Jim_GetString(argv[n],&len);
  }
  return v;
}

static long longpar(Jim_Interp *interp,
                    int n,int argc,
                    Jim_Obj *const *argv,
                    const char *pname,
                    const char *fname,
                    long dval)
{
  long v = dval;

  if (argc > n)
  {
    int res = Jim_GetLong(interp,argv[n],&v);
    if (res != JIM_OK)
      error("wrong parameter %d %s in %s",n,pname,fname);
  }
  return v;
}

static int jim_comment(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  printf("### ");
  for (int i = 1; i < argc; i++)
  {
    Jim_Obj *jo = argv[i];
    printf("%s ",Jim_GetString(jo,0));
  }
  putchar('\n');

  return JIM_OK;
}

static int jim_dump_armregs(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;
  struct t_dap *pdap = cp->pmdap->daps + cp->c_dapidx;

  arm_dump_allreg(0);
  dump_dapreg(pdap,OUT_MIN);

  return JIM_OK;
}

static int jim_init_const(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  Jim_Obj *v = Jim_NewIntObj(interp, SET_AUTO);
  Jim_SetVariableStr(interp, "SET_AUTO", v);

  return JIM_OK;
}

static int jim_check(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;

  assert(argc <= 3 && "wrong number of arguments in check");
  long chk = -1;
  int res = Jim_GetLong(interp,argv[1],&chk);
  assert(res == JIM_OK && "error parameter 1 (chk sequence) in jim_check");
  const char *line = 0;
  int len;
  if (argc == 3)
    line = Jim_GetString(argv[2],&len);

  logmsg("jim_check  %ld %s %s",chk,chk == cp->plu->count ? "OK" : "ERR", line ? line : "NO line info");
  struct jim_data *jd = cp->priv;
  jd->prev_chk = cp->plu->count;

  return JIM_OK;
}

static int jim_checkr(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;

  assert(argc <= 3 && "wrong number of arguments in checkr");
  long chk = -1;
  int res = Jim_GetLong(interp,argv[1],&chk);
  assert(res == JIM_OK && "error parameter 1 (chk sequence) in jim_check");
  const char *line = 0;
  int len;
  if (argc == 3)
    line = Jim_GetString(argv[2],&len);
  char buf[300] = "NO line info";
  if (line)
    sprintf(buf,"@[%s]",line);

  struct jim_data *jd = cp->priv;
  logmsg("jim_checkr %ld (%ld->%ld) %s %s",chk,jd->prev_chk,chk + jd->prev_chk,
         chk + jd->prev_chk == cp->plu->count ? "OK" : "ERR", buf);
  jd->prev_chk = cp->plu->count;

  return JIM_OK;
}

static int jim_read_all_reg(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;
  assert(argc == 2 && "wrong number of arguments in jim_read_all_reg");

  long fl = 0;
  int res = Jim_GetLong(interp,argv[1],&fl);
  assert(res == JIM_OK && "error parameter 1 (flag) in jim_read_all_reg");

  if (cp->plu->tlog)
    logmsg("in jim_read_all_reg fl %x",fl);
  uint regs[0x60];
  res = cmsis_read_regs_f(cp,fl,regs,3);
  cmsis_dumpreg(cp,regs);

  assert(res == LIBUSB_OK && "error from cmsis_read_all_reg");
  return JIM_OK;
}

static int jim_step_fast(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;
  assert(argc == 1 && "wrong number of arguments in jim_step_fast");
  if (cp->plu->tlog)
    logmsg("in jim_step_fast");

  int res = cmsis_step_fast(cp);
  assert(res == LIBUSB_OK && "error from cmsis_step_fast");
  return JIM_OK;
}

static int jim_target(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;
  assert(argc == 2 && "wrong number of arguments in target");

  long id = -1;
  
  int res = Jim_GetLong(interp,argv[1],&id);
  assert(res == JIM_OK && "error parameter 1 (targetid) in jim_target");

  assert(id >= 0 && id < 2 && "wrong target id in jim_target");

  if (cp->c_dapidx == id)
    logmsg("jim_target useless set target id %ld",id);
  cp->r_dapidx = id;
  return JIM_OK;
}

static int jim_cleanup(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;
  assert(argc == 1 && "wrong number of arguments in jim_cleanup");

  if (cp->plu->tlog)
    logmsg("in jim_clenup");

  int res = cmsis_cleanup(cp);
  assert(res == LIBUSB_OK && "error from cmsis_cleanup");

  return JIM_OK;
}

static int jim_step(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;
  assert(argc == 1 && "wrong number of arguments in jim_step");

  if (cp->plu->tlog)
    logmsg("in jim_step");

  int res = cmsis_step(cp);
  assert(res == LIBUSB_OK && "error from cmsis_step");;

  return JIM_OK;
}

static int jim_read_flash(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;

  if (cp->plu->tlog)
    logmsg("in jim_read_flash argc %d ",argc);
  int res,num = 0;
  long n;
  if (argc > 1)
  {
    res = Jim_GetLong(interp,argv[1],&n);
    assert(res == JIM_OK && "error parameter 1 (num) in jim_read_flash");
    num = n;
  }

  res = cmsis_read_flash(cp,num);
  assert(res == LIBUSB_OK && "error in jim_read_flash from cmsis_read_flash");

  return JIM_OK;
}

static int jim_examine(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;

  if (cp->plu->tlog)
    logmsg("in jim_examine argc %d ",argc);
  int res,num = 0;
  long n;
  if (argc > 1)
  {
    res = Jim_GetLong(interp,argv[1],&n);
    assert(res == JIM_OK && "error parameter 1 (num) in jim_examine");
    num = n;
  }

  res = cmsis_examine(cp,num);
  assert(res == LIBUSB_OK && "error in jim_examine");

  return JIM_OK;
}

static int jim_exit(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  exit(0);
  return JIM_OK;
}

static int jim_halt(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;

  if (cp->plu->tlog)
    logmsg("in jim_halt argc %d ",argc);

  long flag = 0;
  int res;
  if (argc > 1)
  {
    res = Jim_GetLong(interp,argv[1],&flag);
    assert(res == JIM_OK && "error parameter 1 (size) of jim_halt");
  }
  res = cmsis_halt(cp,flag);
  assert(res == LIBUSB_OK && "error in jim_halt");

  return JIM_OK;
}

static int jim_assert_reset(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;

  if (cp->plu->tlog)
    logmsg("in jim_assert_reset argc %d ",argc);

  int res = cmsis_assert_reset(cp);
  assert(res == LIBUSB_OK && "error in cmsis_assert_reset");

  return JIM_OK;
}

static int jim_target_reset(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;

  if (cp->plu->tlog)
    logmsg("in jim_target_reset argc %d ",argc);

  int res = cmsis_target_reset(cp);
  assert(res == LIBUSB_OK && "error in cmsis_target_reset");

  return JIM_OK;
}

static int jim_gdb_connect(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;

  if (cp->plu->tlog)
    logmsg("in jim_gdb_connect argc %d ",argc);

  int res = cmsis_gdb_connect(cp);
  assert(res == LIBUSB_OK && "error in cmsis_gdb_connect");

  return JIM_OK;
}

static int jim_read_reg(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;
  long regid;
  assert(argc == 2 && "missing argument in jim_read_reg regid");

  int res = Jim_GetLong(interp,argv[1],&regid);
  assert(res == JIM_OK && "error parameter 1 (add) of jim_read_reg");

//if (cp->plu->tlog)
    logmsg("jim_read_reg: %d",regid);
  uint val;
  res = cmsis_read_reg(cp,regid,&val,SET_AUTO);
  assert(res == LIBUSB_OK && "error in jim_read_reg");
  logmsg("jim_read_reg: %d val %08x",regid,val);

  return res;  
}

// write_mem add size len val
static int jim_set_mem(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;
  long add,size = 4,len = 1;
  long val;

  assert(argc >= 4 && "missing argument in jim_set_mem add [size len val]");
  int res = Jim_GetLong(interp,argv[1],&add);
  assert(res == JIM_OK && "error parameter 1 (add) of jim_set_mem");
  if (argc > 2)
  {
    res = Jim_GetLong(interp,argv[2],&size);
    assert(res == JIM_OK && "error parameter 2 (size) of jim_set_mem");
    assert((size == 1 || size == 2 || size == 4) && "invalid size in jim_set_mem");
    assert((add % size == 0) && "unalined address in jim_set_mem");
  }
  if (argc > 3)
  {
    res = Jim_GetLong(interp,argv[3],&len);
    assert(res == JIM_OK && "error parameter 3 (len) of jim_set_mem");
  }
  if (argc > 4)
  {
    res = Jim_GetLong(interp,argv[4],&val);
    assert(res == JIM_OK && "error parameter 4 (val) of jim_set_mem");
  }
  if (cp->plu->tlog)
    logmsg("in jim_set_mem add %08x size %d len %d val %08x",add,size,len,val);
 
  switch(size)
  {
    case 1:
      error("jim_write_mem TODO"); break;
    case 2:
      {
        uint chunk = len > 1024 ? 1024 : len;
        ushort *mem = malloc(len * sizeof(*mem));
        for (int i = 0; i < chunk; i++) 
          mem[i] = val;
        while(len > 0)
        {
          chunk = len > 1024 ? 1024 : len;
          res = cmsis_write_mem16(cp,add,chunk,mem,SET_AUTO); 
          len -= chunk;
          add += chunk * 2;
        }
        free(mem);
      }
      break;
    case 4:
      {
        uint chunk = len > 1024 ? 1024 : len;
        uint *mem = malloc(len * sizeof(*mem));
        for (int i = 0; i < chunk; i++) 
          mem[i] = val;
        while(len > 0)
        {
          chunk = len > 1024 ? 1024 : len;
          printf("cmsis_write_mem32 add %08lx len %d\n",add,chunk);
          res = cmsis_write_mem32(cp,add,chunk,mem,SET_AUTO); 
          len -= chunk;
          add += chunk * 4;
        }
        free(mem);
      }
      break;
  }

  return res;
}

// write_mem add size val
// write_mem sdd size {val0 ... valn} TODO
static int jim_write_mem(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;
  long add,size = 4;
  long val;
  assert(argc >= 3 && "missing argument in jim_write_mem add [size len]");
  int res = Jim_GetLong(interp,argv[1],&add);
  assert(res == JIM_OK && "error parameter 1 (add) of jim_write_mem");
  if (argc > 2)
  {
    res = Jim_GetLong(interp,argv[2],&size);
    assert(res == JIM_OK && "error parameter 2 (size) of jim_write_mem");
    assert((size == 1 || size == 2 || size == 4) && "invalid size in jim_write_mem");
  }
  if (argc > 3)
  {
    res = Jim_GetLong(interp,argv[3],&val);
    assert(res == JIM_OK && "error parameter 3 (val) of jim_write_mem");
  }

  switch(size)
  {
    case 1:
      error("jim_write_mem TODO"); break;
    case 2:
      res = cmsis_write_16(cp,add,val,SET_AUTO); break;
    case 4:
      res = cmsis_write_32(cp,add,val,SET_AUTO); break;
  }
  return res;
}

static int jim_read_mem(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;
  long add,size = 4,len = 1;
  assert(argc >= 2 && "missing argument in jim_read_mem add [size len]");

  int res = Jim_GetLong(interp,argv[1],&add);
  assert(res == JIM_OK && "error parameter 1 (add) of jim_read_mem");
  if (argc > 2)
  {
    res = Jim_GetLong(interp,argv[2],&size);
    assert(res == JIM_OK && "error parameter 2 (size) of jim_read_mem");
    assert((size == 1 || size == 2 || size == 4) && "invalid size in jim_read_mem");
  }
  if (argc > 3)
  {
    res = Jim_GetLong(interp,argv[3],&len);
    assert(res == JIM_OK && "error parameter 3 (len) of jim_read_mem");
  }

  uchar *mem = calloc(size,len);
  assert(mem && "out of memory in jim_read_mem");
  if (cp->plu->tlog)
    logmsg("in jim_read_mem add %08x size %d len %d",add,size,len);
  res = cmsis_read_mem(cp,add,size,len,mem);
  assert(res == LIBUSB_OK && "error in jim_read_mem");
  if (size == 2)
  {
    ushort *p = (ushort *) mem;
    for (int i = 0; i < len; i++)
      printf("%08lx: %04x\n",add + i * 2,p[i]);
  } else if (size == 4)
  {
    uint *p = (uint *) mem;
/*
    for (int i = 0; i < len; i++)
    {
      if (i % 8 == 0)
      {
        if (i) putchar('\n');
        printf("%08lx: ",add + i * 4);
      }
      printf("%08x ",p[i]);
    }
    putchar('\n');
*/
    int dmems = size * len;
    int i = 0;
    int val;
    while (i < dmems)
    {
      printf("%08lx: ",add + i);
      int k = i;
      int j;
      for (j = 0; i < dmems && j < 32; i += 4)
      {
//      val = memr(p_cpu,dmem + i,OP_NULL,4);
        val = p[i / 4];
        printf("%08x ",val);
        j += 4;
      }
      for (;j < 32; j += 4)
        printf("         ");
      for (int j = 0; k < dmems && j < 32; k++)
      {
//      val = memr(p_cpu,dmem + k,OP_NULL,1);
        val = mem[k];
        if (val >= ' ' && val < 127)
          printf("%c",val);
        else
          printf(".");
        j++;
      }
      putchar('\n');
    }

  }
  free(mem);
  return JIM_OK;
}

static void dumpheader(uchar *mem)
{
  uint *p = (uint *) mem;
  printf("bootrom header:\n");
  int i = 0;
  printf("%08x: %08x\n",i * 4,p[i]);
  i++;
  printf("%08x: %08x\n",i * 4,p[i]);
  i++;
  printf("%08x: %08x\n",i * 4,p[i]);
  i++;
  printf("%08x: %08x\n",i * 4,p[i]);
  i++;
  printf("%08x: %08x %c%c%d v%d\n",
         i * 4,p[i],mem[16],mem[17],mem[18],mem[19]);
  ushort *q = (ushort *) p;
  i = 0;
  printf("%08x: %04x\n",0x14 + i * 2,q[i]);
  i++;
  printf("%08x: %04x\n",0x14 + i * 2,q[i]);
  i++;
  printf("%08x: %04x\n",0x14 + i * 2,q[i]);
  i++;
}

static int jim_dump(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;
  uint add,size = 4;
  long l;
  const char *fname;
  assert(argc == 4 && "missing argument in jim_dump add len file");

  int res = Jim_GetLong(interp,argv[1],&l);
  assert(res == JIM_OK && "error parameter 1 (add) of jim_dump");
  add = l;
  if (argc > 2)
  {
    res = Jim_GetLong(interp,argv[2],&l);
    assert(res == JIM_OK && "error parameter 2 (size) of jim_dump");
    size = l;
  }
  if (argc > 3)
  {
    int len;
    fname = Jim_GetString(argv[3],&len);
    assert(fname && "error parameter 3 (fname) of jim_dump");
  }

  if (cp->plu->tlog)
    logmsg("in jim_dump add %08x size %d fname %s",add,size,fname);

  const uint page = 1024 / 4;
  uchar *mem = calloc(page * 4,1);
  assert(mem && "out of memory in jim_dump");
  FILE *pf = fopen(fname,"wb");
  assert(pf && "can't' open output file name");
  size = (size + 3) / 4;
  for (int seg = 0; seg * page < size; seg++)
  {
    uint d = page;
    if (size - seg * page < d) d = size - seg * page;
    printf("seg %d add %08x d %d size %d\n",seg,add + seg * page * 4,d,size);
    res = cmsis_read_mem(cp,add + seg * page * 4,4,d,mem);
    assert(res == LIBUSB_OK && "jim_dump error from cmsis_read_mem");
    int n = fwrite(mem,4,d,pf);
    assert(n == d && "error from fwrite");
    if (add + seg * page == 0)
      dumpheader(mem);
  }
  fclose(pf);
  free(mem);
  return JIM_OK;
}

static int jim_continue(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;

  if (cp->plu->tlog)
    logmsg("in jim_continue");

  int res = cmsis_cont(cp);
  assert(res == LIBUSB_OK && "error from cmsis_continue");

  return JIM_OK;
}

static int jim_resume(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  int    res;
  long   fl = 0;
  struct t_cmsisprobe *cp = interp->cmdPrivData;
  struct t_set_reg sr[1];

  sr[0].regid = -1;
  if (cp->plu->tlog)
    logmsg("in jim_resume");

  if (argc > 1)
  {
    res = Jim_GetLong(interp,argv[1],&fl);
    assert(res == JIM_OK && "error parameter 1 (flag) of jim_resume");
  }
  res = cmsis_target_resume(cp,sr,fl);

  assert(res == LIBUSB_OK && "error from cmsis_target_resume");

  return JIM_OK;
}

static int jim_step1(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;

  int  res;
  long l;
  uint add;
  uint idx = 0;
  
  if (argc > 1)
  {
    res = Jim_GetLong(interp,argv[1],&l);
    assert(res == JIM_OK && "error parameter 1 (add) of jim_step1");
    add = l;
  }
  if (argc > 2)
  {
    res = Jim_GetLong(interp,argv[2],&l);
    assert(res == JIM_OK && "error parameter 1 (idx) of jim_step1");
    idx = l;
  }
  if (cp->plu->tlog)
    logmsg("in jim_step1 add %08x ind %d",add,idx);

  uint regs[0x60];
  res = cmsis_step1(cp,add,idx,regs);
  assert(res == LIBUSB_OK && "error from cmsis_step1");

  return JIM_OK;
}

static int jim_set_break(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;

  int res;
  long l;
  uint add;
  uint idx = 0;
  
  if (argc > 1)
  {
    res = Jim_GetLong(interp,argv[1],&l);
    assert(res == JIM_OK && "error parameter 1 (add) of jim_set_break");
    add = l;
  }
  if (argc > 2)
  {
    res = Jim_GetLong(interp,argv[2],&l);
    assert(res == JIM_OK && "error parameter 1 (idx) of jim_set_break");
    idx = l;
  }
  if (cp->plu->tlog)
    logmsg("in jim_set_break add %08x ind %d",add,idx);

  res = cmsis_set_break(cp,add,idx);
  assert(res == LIBUSB_OK && "error from cmsis_set_break");

  return JIM_OK;
}

static int jim_trace(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;

  int res;
  long l;
  uint count;
  uint trace_start = 0;
  
  if (argc > 1)
  {
    res = Jim_GetLong(interp,argv[1],&l);
    assert(res == JIM_OK && "error parameter 1 (count) of jim_trace");
    count = l;
  }
  if (argc > 2)
  {
    res = Jim_GetLong(interp,argv[2],&l);
    assert(res == JIM_OK && "error parameter 2 (trace_start) of jim_trace");
    trace_start = l;
  }
  if (cp->plu->tlog)
    logmsg("in jim_trace count %d trace_start %d",count,trace_start);

  res = cmsis_trace_ex(cp,count,1,trace_start);
  assert(res == LIBUSB_OK && "error from cmsis_trace_ex");

  return JIM_OK;
}

static int jim_poll(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  uint regs[0x60];
  struct t_cmsisprobe *cp = interp->cmdPrivData;

  uint fl = 0,poll_fl = 0,dfsr = 0;

  long l;
  int res;
  if (argc > 1)
  {
    res = Jim_GetLong(interp,argv[1],&l);
    assert(res == JIM_OK && "error parameter 1 (flag)  of jim_poll");
    fl = l;
  }
  if (argc > 2)
  {
    res = Jim_GetLong(interp,argv[2],&l);
    assert(res == JIM_OK && "error parameter 2 (poll_flag) of jim_poll");
    poll_fl = l;
  }
  if (argc > 3)
  {
    res = Jim_GetLong(interp,argv[3],&l);
    assert(res == JIM_OK && "error parameter 3 (dfsr) of jim_poll");
    dfsr = l;
  }
  if (cp->plu->tlog)
    logmsg("in jim_poll fl %x poll_fl %x dfsr %08x",fl,poll_fl,dfsr);
    
  res = cmsis_poll(cp,fl,poll_fl,dfsr,regs);
  if (dfsr)
    printf("regs PC %08x\n",regs[15]);

  assert(res == LIBUSB_OK && "error in jim_poll from cmsis_poll");
  return JIM_OK;
}

static int jim_setup(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;
  if (cp->plu->tlog)
    logmsg("in jim_setup argc %d",argc);

  int res = cmsis_setup(cp);
  assert(res == LIBUSB_OK && "error in jim_setup from cmsis_connect");
  return JIM_OK;
}

static int jim_test(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  int len;

  logmsg("in jim_test argc %d private %p",argc,interp->cmdPrivData);
  for (int i = 0; i < argc; i++)
  {
    Jim_Obj *jo = argv[i];
    printf("%d %p bytes %p typ %p %s rc %d len %d %d ",
           i,jo,jo->bytes,jo->typePtr,jo->typePtr->name,jo->refCount,jo->length,
           Jim_IsList(jo));
    const Jim_ObjType *ot = jo->typePtr;
    if (ot)
      printf("name %s flags %x ",ot->name,ot->flags);
    printf("val ");
    long l;
    double d;
    if (Jim_GetLong(interp,jo,&l) == JIM_OK)
      printf("long %ld ",l);
    else if (Jim_GetDouble(interp,jo,&d) == JIM_OK)
      printf("double %f ",d);
    else
      printf("str %s ",Jim_GetString(jo,&len));

    putchar('\n');
  }
  return JIM_OK;
}

static int jim_load(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;
  long pc,sp,loadadd;

  if (argc < 3)
    error("wrong arguments in load(finename,initial_pc,initial_sp)");

  const char *fn = strpar(interp,1,argc,argv,"fileName","read",0);
  pc = longpar(interp,2,argc,argv,"initialPC","load",0);
  sp = longpar(interp,3,argc,argv,"initialSP","load",0);
  loadadd = longpar(interp,4,argc,argv,"loadadd","load",0x20000000);

  logmsg("load fn %s pc %x sp %x loadadd %x",fn,pc,sp,loadadd);
  cmsis_loadprg(cp,fn,loadadd,pc,sp,1);
  return JIM_OK;
}

// qsort compare
static int cmp(const void *p,const void *q)
{
  const uint *a,*b;
  a = (const uint *) p;
  b = (const uint *) q;
  return *a - *b;
}

static int dis_read_mem(struct disasm_par *par,uint pc,uchar *code,size_t len)
{
  return cmsis_read_mem32(par->env,pc & ~3,len,(uint *) code,SET_AUTO);
}

static int jim_run(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  struct t_cmsisprobe *cp = interp->cmdPrivData;
  long wtime = 100000, count = 1;
  struct t_stat st,*p_st;
  struct t_cpu  cpu  = {0},*p_cpu;

  p_st = &st;
  p_cpu = &cpu;

  if (argc < 2)
    error("wrong number of parameters in run");

  wtime = longpar(interp,1,argc,argv,"wtime","run",100000);
  count = longpar(interp,2,argc,argv,"count","run",1);

  logmsg("run wtime %d count %d",wtime,count);

  uint *mem = malloc(count * sizeof(uint));
  cmsis_runcpu(cp,wtime,count,mem,1);
  qsort(mem,count,sizeof(mem[0]),cmp);
  struct disasm_par dp = {.env = cp, .fread_mem = dis_read_mem};
  int i,j = 0;
/*
  for (i = 0; i < count; i++)
    printf("X %2d %08x\n",i,mem[i]);
*/
  for (i = 1; i < count; i++)
    if (mem[i] != mem[i - 1])
    {
      printf("[%4d] ",i - j);
      disasm(&dp,p_cpu,mem[i - 1],p_st);

//    cmsis_disasm(cp,p_cpu,mem[i - 1],p_st);
      putchar('\n');
      j = i;
    }
  printf("[%4d] ",i - j);
  disasm(&dp,p_cpu,mem[i - 1],p_st);
//cmsis_disasm(cp,p_cpu,mem[i - 1],p_st);
  putchar('\n');
  free(mem);

  return JIM_OK;
}

void jim_file_cmsis(char *fname,struct t_cmsisprobe *cp)
{
  logmsg("jim_file %s",fname);
  Jim_Interp *interp;
  struct jim_data data;
  int error;

  assert(cp->priv == 0 && "cp->priv not null");
  cp->priv = &data;
  /* Create an interpreter. */
  interp = Jim_CreateInterp();
  assert(interp != NULL && "couldn't create interpreter");

  /* We register base commands, so that we actually implement Tcl. */
  Jim_RegisterCoreCommands(interp);

  /* And initialise any static extensions */
  Jim_InitStaticExtensions(interp);

  /* Register our Jim commands. */
  Jim_CreateCommand(interp, "test"        , jim_test        , cp, NULL);
  Jim_CreateCommand(interp, "setup"       , jim_setup       , cp, NULL);
  Jim_CreateCommand(interp, "poll"        , jim_poll        , cp, NULL);
  Jim_CreateCommand(interp, "target"      , jim_target      , cp, NULL);
  Jim_CreateCommand(interp, "halt"        , jim_halt        , cp, NULL);
  Jim_CreateCommand(interp, "read_flash"  , jim_read_flash  , cp, NULL);
  Jim_CreateCommand(interp, "check"       , jim_check       , cp, NULL);
  Jim_CreateCommand(interp, "checkr"      , jim_checkr      , cp, NULL);
  Jim_CreateCommand(interp, "gdb_connect" , jim_gdb_connect , cp, NULL);
  Jim_CreateCommand(interp, "assert_reset", jim_assert_reset, cp, NULL);
  Jim_CreateCommand(interp, "target_reset", jim_target_reset, cp, NULL);
  Jim_CreateCommand(interp, "write_mem"   , jim_write_mem   , cp, NULL);
  Jim_CreateCommand(interp, "set_mem"     , jim_set_mem     , cp, NULL);
  Jim_CreateCommand(interp, "read_mem"    , jim_read_mem    , cp, NULL);
  Jim_CreateCommand(interp, "read_reg"    , jim_read_reg    , cp, NULL);
  Jim_CreateCommand(interp, "rem"         , jim_comment     , cp, NULL);
  Jim_CreateCommand(interp, "comment"     , jim_comment     , cp, NULL);
  Jim_CreateCommand(interp, "examine"     , jim_examine     , cp, NULL);
  Jim_CreateCommand(interp, "step"        , jim_step        , cp, NULL);
  Jim_CreateCommand(interp, "step1"       , jim_step1       , cp, NULL);
  Jim_CreateCommand(interp, "cleanup"     , jim_cleanup     , cp, NULL);
  Jim_CreateCommand(interp, "read_all_reg", jim_read_all_reg, cp, NULL);
  Jim_CreateCommand(interp, "step_fast"   , jim_step_fast   , cp, NULL);
  Jim_CreateCommand(interp, "trace"       , jim_trace       , cp, NULL);
  Jim_CreateCommand(interp, "dump"        , jim_dump        , cp, NULL);
  Jim_CreateCommand(interp, "set_break"   , jim_set_break   , cp, NULL);
  Jim_CreateCommand(interp, "continue"    , jim_continue    , cp, NULL);
  Jim_CreateCommand(interp, "resume"      , jim_resume      , cp, NULL);
  Jim_CreateCommand(interp, "dump_armregs", jim_dump_armregs, cp, NULL);
  Jim_CreateCommand(interp, "run"         , jim_run         , cp, NULL);
  Jim_CreateCommand(interp, "load"        , jim_load        , cp, NULL);
  Jim_CreateCommand(interp, "init_const"  , jim_init_const  , cp, NULL);
  Jim_CreateCommand(interp, "exit"        , jim_exit        , cp, NULL);

/*
 * Parse a script
 */
  error = Jim_EvalFile(interp, fname);
  if (error == JIM_ERR) 
  {
    Jim_MakeErrorMessage(interp);
    fprintf(stderr, "%s\n", Jim_GetString(Jim_GetResult(interp), NULL));
  }

  Jim_FreeInterp(interp);
  cp->priv = 0;
  return;
}
