/*
 cygwin distribution is an old version that don't work with cmis-dap
 latest version on 9/11/23 (1.0.26) has been downloaded and extracted here for cygwin-32 and cygwin-64
 file are:
- libusb.h (same for 32 and 64)
- cygusb_32-1.0.dll fail
- cygusb_62-1.0.dll fail
- libsb_32-1.0.dll fail
- libsb_64-1.0.dll OK

so build with 64 bit !!!

dap protocol:

https://arm-software.github.io/CMSIS_5/DAP/html/group__DAP__Connect.html
*/

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
// see note on libusb-1.0
//#include <libusb-1.0/libusb.h>
#include "libusb.h"

#include "env.h"
#include "util.h"
#include "conf.h"
#include "readpcapng-stlink.h"
#include "dap-decode.h"
#include "dapreg.h"
#include "armreg.h"
#include "driver-cmsis.h"
#include "driver-pico.h"

#include "disasm.h"
#include "dump.h"

// extern struct t_dap s_dap;

static int dbgen(libusb_device_handle *handle,int ep,int log)
{
  int res;

  if (log)
    printf("dbgen\n");

  uchar txbuf[64] = {0};
  uchar rxbuf[64] = {0};
  int count = 15;
// set TAR and N read
  int i = 0;
  txbuf[i++] = ID_DAP_TRANSFER;
  txbuf[i++] = 0;       // SWD ignore
  txbuf[i++] = count;

  txbuf[i++] = 5;  // (a 4 AP W ) ap0:004 TAR W e000edf0 DHCSR
  i += set32(txbuf + i,DHCSR_ADD); // DHCSR

  txbuf[i++] = 0xd; // (a c AP W ) ap0:00c DRW W a05f0001
  i += set32(txbuf + i,0xa05f0001);

  if (log)
    memdump(txbuf,64,"",0);
  res = sendrec(handle, ep, txbuf, (int) sizeof(txbuf),  rxbuf, (int) sizeof(rxbuf),__LINE__);
  if (log)
    memdump(rxbuf,20,"",0);
  if (res == 0 && rxbuf[0] == ID_DAP_TRANSFER && rxbuf[1] == count && rxbuf[2] == 1)
  {
    if (log)
      printf("dbgen ok\n");
  } else
  {
    printf("res %d/0 rxbuf[0] %d/%d rxbuf[1] %d/%d rxbuf[2] %d/1 \n",
           res,rxbuf[0],ID_DAP_TRANSFER,rxbuf[1],count,rxbuf[2]);
    memdump(rxbuf,20,"",0);
    DAP_error("transfer",res,__LINE__);
  }
  return 0;
}

static int dbghalt(libusb_device_handle *handle,int ep,int log)
{
  int res;

  if (log)
    printf("dbghalt\n");

  uchar txbuf[64] = {0};
  uchar rxbuf[64] = {0};
  int count = 15;
// set TAR and N read
  int i = 0;
  txbuf[i++] = ID_DAP_TRANSFER;
  txbuf[i++] = 0;       // SWD ignore
  txbuf[i++] = count;

  txbuf[i++] = 5;  // (a 4 AP W ) ap0:004 TAR W e000edf0 DHCSR
  i += set32(txbuf + i,DHCSR_ADD); // DHCSR

  txbuf[i++] = 0xd; // (a c AP W ) ap0:00c DRW W a05f0001
  i += set32(txbuf + i,0xa05f0003);

  if (log)
    memdump(txbuf,64,"",0);
  res = sendrec(handle, ep, txbuf, (int) sizeof(txbuf),  rxbuf, (int) sizeof(rxbuf),__LINE__);
  if (log)
    memdump(rxbuf,20,"",0);
  if (res == 0 && rxbuf[0] == ID_DAP_TRANSFER && rxbuf[1] == count && rxbuf[2] == 1)
  {
    if (log)
      printf("dbghalt ok\n");
  } else
  {
    memdump(rxbuf,20,"",0);
    DAP_error("transfer",res,__LINE__);
  }
  return 0;
}

static int readblock(libusb_device_handle *handle,int ep,uint add,uint count,uint *buf,int log)
{
  int i,res;

  uchar txbuf[64] = {0};
  uchar rxbuf[64] = {0};
// set TAR and N read
  txbuf[0] = ID_DAP_TRANSFER;
  txbuf[1] = 0;       // SWD ignore
  txbuf[2] = count + 1;  // TAR + count
  txbuf[3] = 5;       // (a 4 AP W ) ap0:004 TAR
  set32(txbuf + 4,add);
  for (i = 0; i < count; i++)
    txbuf[8 + i] = 0xf; // (a c AP R ) ap0:00c DRW
//memdump(txbuf,8 + i,0,0);
  res = sendrec(handle, ep, txbuf, (int) sizeof(txbuf),  rxbuf, (int) sizeof(rxbuf),__LINE__);
//memdump(rxbuf,20,0,0);
  if (res == 0 && rxbuf[0] == ID_DAP_TRANSFER &&
      rxbuf[1] == count + 1 && rxbuf[2] == 1)
  {
    if (log)
      printf("readblock ok\n");

    for (i = 0; i < count; i++)
    {
      uint v = get32(rxbuf + 3 + i * 4);
      buf[i] = v;
      if (log)
        printf("%08x: %08x\n",add + i * 4,v);
    }
  } else
  {
    memdump(rxbuf,20,"",0);
    DAP_error("transfer",res,__LINE__);
  }
  return 0;
}

static int readreg(libusb_device_handle *handle,int ep,int log)
{
  int res;

  if (log)
    printf("readreg\n");

  uchar txbuf[64] = {0}; 
  uchar rxbuf[64] = {0};
  int count = 15;
// set TAR and N read
  int i = 0;
  txbuf[i++] = ID_DAP_TRANSFER;
  txbuf[i++] = 0;       // SWD ignore
  txbuf[i++] = count;
// 0
  txbuf[i++] = 5;    // (a 4 AP W ) ap0:004 TAR W e000edf4 DCRSR 
  i += set32(txbuf + i,0xe000edf4); // DCRSR
// 1
  txbuf[i++] = 0xd; // (a c AP W ) ap0:00c DRW W 0000000f R R15
  i += set32(txbuf + i,0xf);   //  R R15
// 2
  txbuf[i++] = 5;  // (a 4 AP W ) ap0:004 TAR W e000edf0 DHCSR
  i += set32(txbuf + i,DHCSR_ADD); // DHCSR
// 3
  txbuf[i++] = 0xf; // (a c AP R ) ap0:00c DRW R
// 4
  txbuf[i++] = 5;  // (a 4 AP W ) ap0:004 TAR W e000edf8 DCRDR 
  i += set32(txbuf + i,0xe000edf8); // DCRDR
// 5
  txbuf[i++] = 0xf; // (a c AP R ) ap0:00c DRW
// 6
  txbuf[i++] = 5;   // (a 4 AP W ) ap0:004 TAR W e000edf4 DCRSR
  i += set32(txbuf + i,0xe000edf4); // DCRSR
// 7
  txbuf[i++] = 0xd; // (a c AP W ) ap0:00c DRW W 0000000d R R13
  i += set32(txbuf + i,0xd); // R R14
// 8
  txbuf[i++] = 5;   // (a 4 AP W ) ap0:004 TAR W e000edf0 DHCSR  
  i += set32(txbuf + i,DHCSR_ADD); // DHCSR
// 9
  txbuf[i++] = 0xf; // (a c AP R ) ap0:00c DRW R
// 10 
  txbuf[i++] = 5;  // (a 4 AP W ) ap0:004 TAR W e000edf8 DCRDR 
  i += set32(txbuf + i,0xe000edf8); // DCRDR
// 11 
  txbuf[i++] = 0xf; // (a c AP R ) ap0:00c DRW R
// 12 
  txbuf[i++] = 5;   // (a 4 AP W ) ap0:004 TAR W e000edf4 DCRSR
  i += set32(txbuf + i,0xe000edf4); // DCRSR
// 13
  txbuf[i++] = 0xd; // (a c AP W ) ap0:00c DRW W 0000000e R R14
  i += set32(txbuf + i,0xe);   //  R R14
// 14 
  txbuf[i++] = 5;  // (a 4 AP W ) ap0:004 TAR W e000edf0 DHCSR
  i += set32(txbuf + i,DHCSR_ADD); // DHCSR

  res = sendrec(handle, ep, txbuf, (int) sizeof(txbuf),  rxbuf, (int) sizeof(rxbuf),__LINE__);
  if (res == 0 && rxbuf[0] == ID_DAP_TRANSFER && rxbuf[1] == count && rxbuf[2] == 1)
  {
    if (log)
      printf("readblock ok\n");
    printf("DHCSR     %08x\n",get32(rxbuf + 3));
    printf("DCRDR  PC %08x\n",get32(rxbuf + 7));
    printf("DHCSR     %08x\n",get32(rxbuf + 11));
    printf("DCRDR  SP %08x\n",get32(rxbuf + 15));
  } else
  {
    memdump(rxbuf,20,"",0);
/*
buf 05 02 04 40 00 00 01 00 00 00 00 00 00 00 00 00
    00 00 00 25
*/
    DAP_error("transfer",res,__LINE__);
  }
  return 0;
}


// test all DAP_Info commands
void test1(libusb_device_handle *handle,int ep)
{
  static int query[] = {1,2,3,4,5,6,7,8,9,0xf0,0xf1,0xfb,0xfc,0xfd,0xfe,0xff};
  static int resp[]  = {0,0,0,0,0,0,0,0,0,   1,   1,   4,   4,   4,   1,   2};
  int res;
  uchar txbuf[64] = {0};
  uchar rxbuf[64] = {0};

  printf("test1\n");
  for (int i = 0; i < sizeof query / sizeof query[0]; i++)
  {
    txbuf[1] = query[i]; 
    printf("query %x %x ",txbuf[0],txbuf[1]);
    res = sendrec(handle, ep, txbuf, (int) sizeof(txbuf),  rxbuf, (int) sizeof(rxbuf),__LINE__);
    if (res != 0 || rxbuf[0] != 0)
    {
      printf("ERROR from query res %d rxbuf[0] %x\n",res,rxbuf[0]);
    }
    int l = rxbuf[1];
    if (resp[i] == 0 && l > 0)
      printf("str %s",rxbuf + 2);
    else if (l == resp[i])
    {
      short *ps;
      int *pi;
      switch (l)
      {
        case 1:
          printf("byte %02x %3d",rxbuf[2],rxbuf[2]);
          break;
        case 2:
          ps = (short *) (rxbuf + 2);
          printf("short %d",*ps);
          break;
        case 4:
          pi = (int *) (rxbuf + 2);
          printf("word %d",*pi);
          break;
      }
    }
    putchar('\n');
  }
}

// read CPUID  0xe000ed00
//      DFSR   0xe000ed30
//      DHCSR  DHCSR_ADD
//      DCRSR  0xe000edf4
//      DCRDR  0xe000edf8
//      DEMCR  0xe000edfc
void test5(libusb_device_handle *handle,int ep)
{
  int res;
  uint cpuid,dfsr;
  uint dregs[4];

  printf("test5\n");
  res = readblock(handle,ep,CPUID_ADD ,1,&cpuid,0);
  if (res == 0)
    printf("CPUID  %08x\n",cpuid);
 
  res = readblock(handle,ep,DFSR_ADD ,1,&dfsr,0);
  if (res == 0)
    printf("DFSR   %08x\n",dfsr);
  printf("1)\n");
  res = readblock(handle,ep,DHCSR_ADD ,1,dregs,0);
  if (res == 0)
    printf("DHCSR  %08x\nDCRSR  %08x\nDCRDR  %08x\nDEMCR  %08x\n",
           dregs[0],dregs[1],dregs[2],dregs[3]);
  printf("2)\n");
  res = readblock(handle,ep,DHCSR_ADD ,1,dregs,0);
  if (res == 0)
    printf("DHCSR  %08x\nDCRSR  %08x\nDCRDR  %08x\nDEMCR  %08x\n",
           dregs[0],dregs[1],dregs[2],dregs[3]);
}

void test4(libusb_device_handle *handle,int ep,int fl)
{
  int res;
  uchar txbuf[64] = {0}; 
  uchar rxbuf[64] = {0};
//
// set CSW & TAR read DWR
  txbuf[0] = ID_DAP_TRANSFER;
  txbuf[1] = 0;       // SWD ignore
  txbuf[2] = 5;       // count
  txbuf[3] = 8;       // (a 8 DP W ) SELECT 00000000 [sel 0 0 0] 
  set32(txbuf + 4,0x0);
  txbuf[8] = 1;       // (a 0 AP W ) ap0:000 CSW 23000040
  set32(txbuf + 9,0x23000040);
  txbuf[13] = 1;      // (a 0 AP W ) ap0:000 CSW 23000012
  set32(txbuf + 14,0x23000012);
  txbuf[18] = 5;      // (a 4 AP W ) ap0:004 TAR e000edfc 
  set32(txbuf + 19,0xe000edfc);
  txbuf[23] = 0xf;    // (a c AP R ) ap0:00c DRW  
//memdump(txbuf,20);
  res = sendrec(handle, ep, txbuf, (int) sizeof(txbuf),  rxbuf, (int) sizeof(rxbuf),__LINE__);
  if (res == 0 && rxbuf[0] == ID_DAP_TRANSFER && rxbuf[1] == 5 /* count */ && rxbuf[2] == 1 /* OK */)
  {
    uint drw = get32(rxbuf + 3);
    printf("Transfer block OK DRW %08x\n",drw);
  } else
  {
    memdump(rxbuf,20,"",0);
    DAP_error("transfer block",res,__LINE__);
  }
//
// set TAR Write DWR
  txbuf[0] = ID_DAP_TRANSFER;
  txbuf[1] = 0;       // SWD ignore
  txbuf[2] = 2;       // count
  txbuf[3] = 5;       // ap0:004 TAR e000edfc 
  set32(txbuf + 4,0xe000edfc);
  txbuf[8] = 0xd;     // (a c AP W ) ap0:00c DRW 01000000
  set32(txbuf + 9,0x01000000);
//memdump(txbuf,20,0,0);
  res = sendrec(handle, ep, txbuf, (int) sizeof(txbuf),  rxbuf, (int) sizeof(rxbuf),__LINE__);
  if (res == 0 && rxbuf[0] == ID_DAP_TRANSFER && rxbuf[1] == 2 /* count */ && rxbuf[2] == 1 /* OK */)
  {
    printf("transfer block OK\n");
  } else
  {
    memdump(rxbuf,20,"",0);
    DAP_error("transfer block",res,__LINE__);
  }
// read CSW
  txbuf[0] = ID_DAP_TRANSFER_BLOCK;
  txbuf[1] = 0;       // SWD ignore
  set16(txbuf + 2,1); // count
  txbuf[4] = 3;       // (a 0 AP R ) ap0:000 CSW 
//memdump(txbuf,20,0,0);
  res = sendrec(handle, ep, txbuf, (int) sizeof(txbuf),  rxbuf, (int) sizeof(rxbuf),__LINE__);
  if (res == 0 && rxbuf[0] == ID_DAP_TRANSFER_BLOCK && get16(rxbuf + 1) == 1 && rxbuf[3] == 1)
  {
    uint csw = get32(rxbuf + 4);
    printf("Transfer block OK MEM-AP CSW %08x DbgSwEn %d Prot %x SDeviveEn %d MTE %d Type %d Mode %x TrinProg %d DeviceEn %d AddInc %d Size %d\n",
           csw,(csw >> 31) & 1,(csw >> 24) & 0x7f,(csw >> 23) & 1,(csw >> 15) & 1,
           (csw >> 12) & 7,(csw >> 8) & 15,(csw >> 7) & 1,(csw >> 6) & 1,
           (csw >> 4) & 3, csw & 7);
  } else
  {
    memdump(rxbuf,20,"",0);
    DAP_error("transfer block",res,__LINE__);
  }
  uint ubuf[12];
  uint baseromadd = 0xe00fffd0;
  char *names[] = {"Per ID4","","","",
                   "Per ID0","Per ID1","Per ID2","Per ID 3",
                   "Comp ID0","Comp ID1","Comp ID2","Comp ID3"};
  res = readblock(handle,ep,baseromadd,12,ubuf,0);
  if (res == 0)
  {
    printf("Cortex-M4 ROM table\n");
    for (int i = 0; i < 12; i++)
      printf("%08x: %-12s %08x\n",baseromadd + i * 4,names[i],ubuf[i]);
  } else
    DAP_error("readblock",res,__LINE__);
  
  uint romadd = 0xe00ff000;
  char *cnames[] = {"SCS","DWT","FPB","ITM",
                   "TPIU","EMT","End marker"};
  res = readblock(handle,ep,romadd,12,ubuf,0);
  if (res == 0)
  {
    printf("Cortex-M4 CoreSight ROM table\n");
    for (int i = 0; i < 7; i++)
    {
      int pre = ubuf[i] & 1;
      printf("%08x: %-12s %08x %-5s ",
             romadd + i * 4,cnames[i],ubuf[i],pre ? "pre" : "");
      if (pre)
        printf("%08x ",(ubuf[i] + baseromadd) & ~3);
      putchar('\n');
    }
  } else
    DAP_error("readblock",res,__LINE__);

  if (!fl) return;
  uint ubuf1[12];
  for (int i = 0; i < 7; i++)
  {
    int pre = ubuf[i] & 1;
    if (pre)
    {
      uint add = (ubuf[i] + baseromadd) & ~3;
      res = readblock(handle,ep,add,12,ubuf1,0);
      if (res == 0)
      {
        printf("---- ROM @ %08x\n",add);
        for (int j = 0; j < 12; j++)
          printf("%08x: %08x\n",add + j * 4,ubuf1[j]);
      }
    }
  }
}

// DAP_TRANSFER_BLOCK
// DAP_TRANSFER
void test3(struct t_cmsisprobe *cp)
{
  struct t_libusb *plu = cp->plu;
  struct t_dap *pdap = cp->pmdap->daps + cp->c_dapidx;
  int res;
// read DPIDR Debug Port Identification Register
  int i = 0;
  memset(plu->txbuf,0,plu->bufsize);
  plu->txbuf[i++] = ID_DAP_TRANSFER_BLOCK;
  plu->txbuf[i++] = 0;     // SWD ignore
  int count = 1;
  i += set16(plu->txbuf + i,count); // count
  i += setreqblock(cp,plu->txbuf + i,DAP_DP_DPIDR,DAP_RO,0,plu->log);
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_TRANSFER_BLOCK && get16(plu->rxbuf + 1) == count && 
      plu->rxbuf[3] == 1)
  {
    if (plu->log)
      logmsg("transfer block OK");
// set pdap
  } else
    DAP_error("transfer block",res,__LINE__);

// enable power to the DAP (system and debug bit on STAT/CTRL DAP register)
  i = 0;
  memset(plu->txbuf,0,plu->bufsize);
  count = 4;
  plu->txbuf[i++] = ID_DAP_TRANSFER;
  plu->txbuf[i++] = 0;       // SWD ignore
  plu->txbuf[i++] = count;   // count
  i += setreqblock(cp,plu->txbuf + i,DAP_DP_ABORT,DAP_WO,0x1e,plu->log);
  i += setreqblock(cp,plu->txbuf + i,DAP_DP_SELECT,DAP_WO,0x0,plu->log);
  i += setreqblock(cp,plu->txbuf + i,DAP_DP_CTRL_STAT,DAP_WO,0x50000f00,plu->log);
  i += setreqblock(cp,plu->txbuf + i,DAP_DP_CTRL_STAT,DAP_RO,0,plu->log);
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_TRANSFER && plu->rxbuf[1] == count && 
      plu->rxbuf[2] == DAP_TRANSFER_OK /* OK */)
  {
    if (plu->log)
      logmsg("transfer block OK");
    dap_setreg(pdap,DAP_DP_CTRL_STAT,get32(plu->rxbuf + 3),plu->log ? OUT_MIN : OUT_NONE,DAP_WO,DAP_RECEIVE,0);
  } else
    DAP_error("transfer block",res,__LINE__);
// read AP0:3 IDR reg 
  for (int j = 0; j < 4; j++)
  {  
    i = 0;
    count = 2;
    memset(plu->txbuf,0,plu->bufsize);
    plu->txbuf[i++] = ID_DAP_TRANSFER;
    plu->txbuf[i++] = 0;       // SWD ignore
    plu->txbuf[i++] = count;   
    i += setreqblock(cp,plu->txbuf + i,DAP_DP_SELECT,DAP_WO,0xf0 | (j << 24),plu->log);
/*
    plu->txbuf[i++] = 8;       // (a 8 DP W ) SELECT 0x00000f0 [sel 0 15 0] 
    set32(plu->txbuf + i,0xf0 | (j << 24)); 
    i += 4;
*/
    i += setreqblock(cp,plu->txbuf + i,DAP_AP_IDR,DAP_RO,0,plu->log);
/*
    plu->txbuf[i++] = 0xf;     // (a c AP R ) ap0:0fc IDR
*/
    res = sendrec1(plu,i,__LINE__);
    if (res == 0 && plu->rxbuf[0] == ID_DAP_TRANSFER && plu->rxbuf[1] == count && 
        plu->rxbuf[2] == DAP_TRANSFER_OK /* OK */)
    {
      uint idr = get32(plu->rxbuf + 3);
      uint rev = (idr >> 28) & 0xf;
      uint designer = (idr >> 17) & 0x7ff;
      uint class = (idr >> 13) & 0xf;
      uint variant = (idr >> 4) & 0xf;
      uint type = idr & 0xf;
      if (plu->log)
        logmsg("transfer OK");
      printf("AP%d IDR %08x rev %d designer %x class %d variant %d type %d\n",
             j,idr,rev,designer,class,variant,type);
    } else
      DAP_error("transfer block",res,__LINE__);
  }
// 
// read CSW reg 
  i = 0;
  memset(plu->txbuf,0,plu->bufsize);
  plu->txbuf[i++] = ID_DAP_TRANSFER;
  plu->txbuf[i++] = 0;       // SWD ignore
  plu->txbuf[i++] = 2;       // count
  plu->txbuf[i++] = 8;       // (a 8 DP W ) SELECT 00000000 [sel 0 0 0] 
  set32(plu->txbuf + i,0); 
  i += 4;
  plu->txbuf[i++] = 3;       // ap0:000 CSW
  res = sendrec1(plu,i,__LINE__);
//memdump(rxbuf,40,0);
// buf 05 02 01 40 00 00 23 2b 00 00 00 00 00 00 00 00 00 00 00 
  if (res == 0 && plu->rxbuf[0] == ID_DAP_TRANSFER && plu->rxbuf[1] == 2 /* count */ && 
      plu->rxbuf[2] == 1 /* OK */)
  {
    uint csw = get32(plu->rxbuf + 3);
    if (plu->log)
      logmsg("transfer OK");
    printf("MEM-AP CSW %08x DbgSwEn %d Prot %x SDeviveEn %d MTE %d Type %d Mode %x TrinProg %d DeviceEn %d AddInc %d Size %d\n",
           csw,(csw >> 31) & 1,(csw >> 24) & 0x7f,(csw >> 23) & 1,(csw >> 15) & 1,
           (csw >> 12) & 7,(csw >> 8) & 15,(csw >> 7) & 1,(csw >> 6) & 1,
           (csw >> 4) & 3, csw & 7);
  } else
    DAP_error("transfer block",res,__LINE__);
// 
// read CFG reg 
  i = 0;
  memset(plu->txbuf,0,plu->bufsize);
  plu->txbuf[i++] = ID_DAP_TRANSFER;
  plu->txbuf[i++] = 0;       // SWD ignore
  plu->txbuf[i++] = 2;       // count
  plu->txbuf[i++] = 8;       // (a 8 DP W ) SELECT 000000f0 [sel 0 15 0] 
  set32(plu->txbuf + i,0xf0);
  i += 4; 
  plu->txbuf[i++] = 7;       // ap0:0f4 CFG
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_TRANSFER && plu->rxbuf[1] == 2 /* count */ && 
      plu->rxbuf[2] == 1 /* OK */)
  {
    uint cfg = get32(plu->rxbuf + 3);
    if (plu->log)
      logmsg("transfer OK");
    printf("MEM-AP CFG %08x LD %d LA %d BE %d\n",
           cfg,(cfg >> 2) & 1,(cfg >> 1) & 1,cfg & 1);
  } else
    DAP_error("transfer block",res,__LINE__);
// 
// write/read CSW reg 
  i = 0;
  memset(plu->txbuf,0,plu->bufsize);
  plu->txbuf[i++] = ID_DAP_TRANSFER;
  plu->txbuf[i++] = 0;       // SWD ignore
  plu->txbuf[i++] = 3;       // count
  plu->txbuf[i++] = 8;       // (a 8 DP W ) SELECT 00000000 [sel 0 0 0] 
  i += set32(plu->txbuf + i,0x0); 
  plu->txbuf[i] = 1;       // (a 0 AP W ) ap0:000 CSW 6f000040 
  i += set32(plu->txbuf + i,0x6f000040); 
  plu->txbuf[i++] = 3;      // (a 0 AP R ) ap0:000 CSW
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_TRANSFER && plu->rxbuf[1] == 3 /* count */ && 
      plu->rxbuf[2] == 1 /* OK */)
  {
    uint csw = get32(plu->rxbuf + 3);
    if (plu->log)
      logmsg("transfer OK");
    printf("MEM-AP CSW %08x DbgSwEn %d Prot %x SDeviveEn %d MTE %d Type %d Mode %x TrinProg %d DeviceEn %d AddInc %d Size %d\n",
           csw,(csw >> 31) & 1,(csw >> 24) & 0x7f,(csw >> 23) & 1,(csw >> 15) & 1,
           (csw >> 12) & 7,(csw >> 8) & 15,(csw >> 7) & 1,(csw >> 6) & 1,
           (csw >> 4) & 3, csw & 7);
  } else
    DAP_error("transfer block",res,__LINE__);
// 
// read BASE 
  i = 0;
  memset(plu->txbuf,0,plu->bufsize);
  plu->txbuf[i++] = ID_DAP_TRANSFER;
  plu->txbuf[i++] = 0;       // SWD ignore
  plu->txbuf[i++] = 2;       // count
  plu->txbuf[i++] = 8;       // (a 8 DP W ) SELECT 000000f0 [sel 0 0 0] 
  i += set32(plu->txbuf + i,0xf0); 
  plu->txbuf[i++] = 0xb;     // (a 8 AP R ) ap0:0f8 BASE 
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_TRANSFER && plu->rxbuf[1] == 2 /* count */ && 
      plu->rxbuf[2] == 1 /* OK */)
  {
    uint base = get32(plu->rxbuf + 3);
    if (plu->log)
      logmsg("transfer OK");
    printf("MEM-AP BASE %08x\n",base);
  } else 
    DAP_error("transfer block",res,__LINE__);
}

// test read pin value  DAP_SWJ_Pins
// set clock            DAP_SWJ_Clock
// connect              DAP_Connect
// configure transfer   DAP_TransferConfigure
// SWD_config           DAP_SWD_Configure
// SWJ sequence         DAP_SWJ_Sequence

void test2(libusb_device_handle *handle,int ep)
{
  int res;
  uchar txbuf[64] = {0x10,0,0,0,0,0,0}; // do nothing
  uchar rxbuf[64] = {0};

  printf("test2\n");
  printf("query %x %x %x %x %x\n",txbuf[0],txbuf[1],txbuf[2],txbuf[3],txbuf[4]);
  for (int i = 0; i < 5; i++)
  {
    res = sendrec(handle, ep, txbuf, (int) sizeof(txbuf),  rxbuf, (int) sizeof(rxbuf),__LINE__);
//  memdump(rxbuf,20,0);
    if (res == 0 && rxbuf[0] == 0x10)
      printf("input pin staus SWCLK %d SWDIO %d TDI %d TDO %d nTRST %d nRESET %d\n",
             rxbuf[1] & 1,
             (rxbuf[1] >> 1) & 1,
             (rxbuf[1] >> 2) & 1,
             (rxbuf[1] >> 3) & 1,
             (rxbuf[1] >> 5) & 1,
             (rxbuf[1] >> 7) & 1);
  }

  txbuf[0] = 0x11;
  set32(txbuf + 1,1000000); // speed
  res = sendrec(handle, ep, txbuf, (int) sizeof(txbuf),  rxbuf, (int) sizeof(rxbuf),__LINE__);
//memdump(rxbuf,20,0,0);
  if (res == 0 && rxbuf[0] == 0x11 &&  rxbuf[1] == 0)
    printf("set clock OK\n");
  else
    DAP_error("set clock",res,__LINE__);

  txbuf[0] = 2;
  txbuf[1] = 1; // SWD 
  res = sendrec(handle,ep,txbuf, (int) sizeof(txbuf),rxbuf,(int) sizeof(rxbuf),__LINE__);
//memdump(rxbuf,20,0,0);
  if (res == 0 && rxbuf[0] == 2 && rxbuf[1] == 1)
    printf("SWD OK\n"); 
  else
    DAP_error("set SWD",res,__LINE__);

  txbuf[0] = 4; // DAP_TransferConfigure
  txbuf[1] = 2; // idle cycles
  set16(txbuf + 2,0x80); // wait retry
  set16(txbuf + 4,0); // matxh retry
  res = sendrec(handle,ep,txbuf, (int) sizeof(txbuf),rxbuf,(int) sizeof(rxbuf),__LINE__);
  if (res == 0 && rxbuf[0] == 4 && rxbuf[1] == 0)
    printf("Configure OK\n");
  else
    DAP_error("configure",res,__LINE__);

  txbuf[0] = 0x13; // DAP_SWD_Configure
  txbuf[1] = 0; // 
  res = sendrec(handle,ep,txbuf, (int) sizeof(txbuf),rxbuf,(int) sizeof(rxbuf),__LINE__);
  if (res == 0 && rxbuf[0] == 0x13 && rxbuf[1] == 0)
    printf("SWD configure OK\n");
  else
    DAP_error("SWD configure",res,__LINE__);

  txbuf[0] = ID_DAP_SWJ_SEQUENCE; // DAP_SWJ_Sequence;
  txbuf[1] = 51;   // bit count
  txbuf[2] = 0xff; 
  txbuf[3] = 0xff; 
  txbuf[4] = 0xff; 
  txbuf[5] = 0xff; 
  txbuf[6] = 0xff; 
  txbuf[7] = 0xff; 
  txbuf[8] = 0xff; 
  res = sendrec(handle,ep,txbuf, (int) sizeof(txbuf),rxbuf,(int) sizeof(rxbuf),__LINE__);
  if (res == 0 && rxbuf[0] == 0x12 && rxbuf[1] == 0)
    printf("SWJ Sequence OK\n");
  else
    DAP_error("SWJ Sequence",res,__LINE__);

// switch to SWD
  txbuf[0] = ID_DAP_SWJ_SEQUENCE; // DAP_SWJ_Sequence;
  txbuf[1] = 16;   // bit count
  txbuf[2] = 0x9e; 
  txbuf[3] = 0xe7; 
  res = sendrec(handle,ep,txbuf, (int) sizeof(txbuf),rxbuf,(int) sizeof(rxbuf),__LINE__);
  if (res == 0 && rxbuf[0] == 0x12 && rxbuf[1] == 0)
    printf("SWJ Sequence OK\n");
  else
    DAP_error("SWJ Sequence",res,__LINE__);

  txbuf[0] = ID_DAP_SWJ_SEQUENCE; // DAP_SWJ_Sequence;
  txbuf[1] = 51;   // bit count
  txbuf[2] = 0xff; 
  txbuf[3] = 0xff; 
  txbuf[4] = 0xff; 
  txbuf[5] = 0xff; 
  txbuf[6] = 0xff; 
  txbuf[7] = 0xff; 
  txbuf[8] = 0xff; 
  res = sendrec(handle,ep,txbuf, (int) sizeof(txbuf),rxbuf,(int) sizeof(rxbuf),__LINE__);
  if (res == 0 && rxbuf[0] == 0x12 && rxbuf[1] == 0)
    printf("SWJ Sequence OK\n");
  else
    DAP_error("SWJ Sequence",res,__LINE__);

  txbuf[0] = ID_DAP_SWJ_SEQUENCE; // DAP_SWJ_Sequence;
  txbuf[1] = 8;   // bit count
  txbuf[2] = 0; 
  res = sendrec(handle,ep,txbuf, (int) sizeof(txbuf),rxbuf,(int) sizeof(rxbuf),__LINE__);
  if (res == 0 && rxbuf[0] == 0x12 && rxbuf[1] == 0)
    printf("SWJ Sequence OK\n");
  else
    DAP_error("SWJ Sequence",res,__LINE__);
}

void dotest(struct t_cmsisprobe *cp,struct t_libusb *plu,libusb_device_handle *handle,int ep)
{
  printf("dotest\n");
  printf("test1\n");
  test1(handle,ep);
  printf("test2\n");
  test2(handle,ep);
  printf("test3\n");
  test3(cp);
  printf("test4\n");
  test4(handle,ep,0);
  printf("test5\n");
  test5(handle,ep);
  printf("dbgen\n");
  dbgen(handle,ep,1);
  printf("readreg\n");
  readreg(handle,ep,1);
  printf("dbghalt\n");
  dbghalt(handle,ep,1);
  printf("readreg\n");
  readreg(handle,ep,1);
  printf("dbgen\n");
  dbgen(handle,ep,1);
  dbghalt(handle,ep,1);
  printf("readreg\n");
  readreg(handle,ep,1);
//test5(handle,ep);
  dbgen(handle,ep,1);
  printf("readreg\n");
  readreg(handle,ep,1);
  printf("readreg\n");
  readreg(handle,ep,1);
  printf("readreg\n");
  readreg(handle,ep,1);
  printf("dotest end\n");
}

