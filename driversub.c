#define D
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <libusb-1.0/libusb.h>
#include "env.h"
#include "util.h"
#include "conf.h"
#include "readpcapng-stlink.h"
#include "dap-decode.h"
#include "dapreg.h"
#include "armreg.h"
#include "driver-cmsis.h"
#include "disasm.h"
#include "common-driver.h"
#include "dump.h"
#include "driversub.h"
#include "chipid.h"
#define EXTERN
#include "readelf.h"
#undef EXTERN

#define SCB_OFF(n) {SCB_##n##_OFF,#n,0}
#define SCB_OFF_F(n,f) {SCB_##n##_OFF,#n,f}
#define CODE_BREAK_NUM_MAX 15

struct off_t
{
  int  off;
  char *name;
  void (*out)(stlink_t* sl,uint val);
};

static int code_break_num;
static int code_lit_num;
static int code_break_rev;

static void dump_dhcsr(uint dhcsr,int line)
{
  printf("%4d: dhcsr %08x ",line,dhcsr);
  if ((dhcsr >> 25) & 1) printf("S_RESET_ST ");
  if ((dhcsr >> 24) & 1) printf("S_RETIRE_ST ");
  if ((dhcsr >> 19) & 1) printf("S_LOCKUP ");
  if ((dhcsr >> 18) & 1) printf("S_SLEEP ");
  if ((dhcsr >> 17) & 1) printf("S_HALT ");
  if ((dhcsr >> 16) & 1) printf("S_REGRDY ");
  if ((dhcsr >>  5) & 1) printf("C_SNAPSTALL ");
  if ((dhcsr >>  3) & 1) printf("C_MASKINTS ");
  if ((dhcsr >>  2) & 1) printf("C_STEP ");
  if ((dhcsr >>  1) & 1) printf("C_HALT ");
  if ((dhcsr >>  0) & 1) printf("C_DEBUGEN ");
  putchar('\n');
}

uint readdhcsr(stlink_t* sl,int log,int line)
{
  int ret;
  uint dhcsr;

  ret = stlink_read_debug32(sl, STLINK_REG_DHCSR, &dhcsr);
  CHKSTR(ret,line);
  if (log)
    dump_dhcsr(dhcsr,line);
  return dhcsr;
}

int writedhcsr(stlink_t* sl,int dhcsr,int line)
{
  int ret;
  uint val = (0xa05f << 16) | (dhcsr & 0xffff);
  printf("%4d: write dhcsr %08x\n",line,val);
  ret = stlink_write_debug32(sl, STLINK_REG_DHCSR, val);
  CHKST(ret);

  return readdhcsr(sl,1,__LINE__);
}

static void fcfsr(stlink_t* sl,uint val)
{
  uint mmfsr,bfsr,ufsr;

  mmfsr = val & 0xff;
  bfsr  = (val >> 8) & 0xff;
  ufsr  = val >> 16;

  if (mmfsr)
    printf("MMFSR %02x ",mmfsr);
  if (bfsr)
  {
    printf("BFSR %02x ",bfsr);
    if ((bfsr >> 7) & 1)
      printf("BFARVALID ");
    if ((bfsr >> 5) & 1)
      printf("LSPERR ");
    if ((bfsr >> 4) & 1)
      printf("STKERR ");
    if ((bfsr >> 3) & 1)
      printf("UNSTKERR ");
    if ((bfsr >> 2) & 1)
      printf("IMPRECISERR ");
    if ((bfsr >> 1) & 1)
      printf("PRECISERR ");
    if ((bfsr >> 0) & 1)
      printf("IBUSERR ");
  }
  if (ufsr)
    printf("UFSR %02x ",ufsr);

  putchar('\n');
}

static struct off_t scb_table[] =
{
  SCB_OFF(CPUID),
  SCB_OFF(ICSR),
  SCB_OFF(VTOR),
  SCB_OFF(AIRCR),
  SCB_OFF(SCR),
  SCB_OFF(CCR),
  SCB_OFF(SHPR1),
  SCB_OFF(SHPR2),
  SCB_OFF(SHPR3),
  SCB_OFF(SHCSR),
  SCB_OFF_F(CFSR,fcfsr),
  SCB_OFF(HFSR),
  SCB_OFF(DFSR),
  SCB_OFF(MMFAR),
  SCB_OFF(BFAR),
  SCB_OFF(AFSR),
  SCB_OFF(CPACR),
  SCB_OFF(NSACR),
  {-1,0}
};

// #define D

// compiler dependent ?

uint is_bigendian(void) 
{
  static volatile const uint i = 1;
  return *(volatile const char*) &i == 0;
}

static inline uint32_t le_to_h_u32(const uint8_t* buf) {
  return((uint32_t)((uint32_t)buf[0] | (uint32_t)buf[1] << 8 | 
         (uint32_t)buf[2] << 16 | (uint32_t)buf[3] << 24));
}

unsigned time_ms() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return (unsigned)(tv.tv_sec * 1000 + tv.tv_usec / 1000);
}

char *regname(int add)
{
  switch (add)
  {
    case STLINK_REG_DHCSR:
      return "STLINK_REG_CM3_DHCSR";
    case STLINK_REG_CM3_CPUID:
      return "STLINK_REG_CM3_CPUID";
    case 0x5c001054:  // misplaced define
      return "STM32H7_DBGMCU_APB1HFZ";
    case STLINK_REG_CM3_FP_CTRL:
      return "STLINK_REG_CM3_FP_CTRL";
    case STLINK_REG_CM3_DEMCR:
      return "STLINK_REG_CM3_DEMCR";
    case STLINK_REG_CM7_CTR:
      return "STLINK_REG_CM7_CTR";
    case STLINK_REG_CM7_CLIDR:
      return "STLINK_REG_CM7_CLIDR";
    case STLINK_REG_CM7_CCR:
      return "STLINK_REG_CM7_CCR";
    case STLINK_REG_CM7_CCSIDR:
      return "STLINK_REG_CM7_CCSIDR";
    case STLINK_REG_AIRCR:
      return "STLINK_REG_AIRCR";
    case STLINK_REG_DFSR:
      return "STLINK_REG_DFSR";
    case STLINK_REG_CM7_FP_LAR:
      return "STLINK_REG_CM7_FP_LAR";
    case STLINK_REG_CM7_CSSELR:
      return "STLINK_REG_CM7_CSSELR";

    case STLINK_REG_CM3_FP_COMPn(0):
      return "STLINK_REG_CM3_FP_COMP0";
    case STLINK_REG_CM3_FP_COMPn(1):
      return "STLINK_REG_CM3_FP_COMP1";
    case STLINK_REG_CM3_FP_COMPn(2):
      return "STLINK_REG_CM3_FP_COMP2";
    case STLINK_REG_CM3_FP_COMPn(3):
      return "STLINK_REG_CM3_FP_COMP3";
    case STLINK_REG_CM3_FP_COMPn(4):
      return "STLINK_REG_CM3_FP_COMP4";
    case STLINK_REG_CM3_FP_COMPn(5):
      return "STLINK_REG_CM3_FP_COMP5";
    case STLINK_REG_CM3_FP_COMPn(6):
      return "STLINK_REG_CM3_FP_COMP6";
    case STLINK_REG_CM3_FP_COMPn(7):
      return "STLINK_REG_CM3_FP_COMP7";
    case STLINK_REG_CM3_DWT_FUNn(0):
      return "STLINK_REG_CM3_DWT_FUN0";
    case STLINK_REG_CM3_DWT_FUNn(1):
      return "STLINK_REG_CM3_DWT_FUN1";
    case STLINK_REG_CM3_DWT_FUNn(2):
      return "STLINK_REG_CM3_DWT_FUN2";
    case STLINK_REG_CM3_DWT_FUNn(3):
      return "STLINK_REG_CM3_DWT_FUN3";
    case STLINK_REG_CM3_DWT_FUNn(4):
      return "STLINK_REG_CM3_DWT_FUN4";
    case STLINK_REG_CM3_DWT_FUNn(5):
      return "STLINK_REG_CM3_DWT_FUN5";
    case STLINK_REG_CM3_DWT_FUNn(6):
      return "STLINK_REG_CM3_DWT_FUN6";
    case STLINK_REG_CM3_DWT_FUNn(7):
      return "STLINK_REG_CM3_DWT_FUN7";
    case 0x52002000 + 0x4:
      return "FLASH_H7_KEYR1";
    case 0x52002000 + 0x104:
      return "FLASH_H7_KEYR2";
    case 0x52002000 + 0x8:
      return "FLASH_H7_OPT_KEYR";
    case 0x52002000 + 0xc:
      return "FLASH_H7_CR1";
    case 0x52002000 + 0x10c:
      return "FLASH_H7_CR2";
    case 0x52002000 + 0x10:
      return "FLASH_H7_SR1";
    case 0x52002000 + 0x110:
      return "FLASH_H7_SR2";
    case 0x52002000 + 0x14:
      return "FLASH_H7_CCR1";
    case 0x52002000 + 0x114:
      return "FLASH_H7_CCR2";
    case 0x52002000 + 0x18:
      return "FLASH_H7_OPTCR";
    case 0x52002000 + 0x118:
      return "FLASH_H7_OPTCR2";
    case 0x52002000 + 0x1c:
      return "FLASH_H7_OPTSR_CUR";
    case 0x52002000 + 0x24:
      return "iFLASH_H7_OPTCCR";
    case 0x1FF1E880:
      return "F_SIZE";
    case 0x5c001000:
      return "DBGMCU_IDC";
    default:
      return "???";
  }
}

void write_uint16(uchar* buf, uint ui) 
{
  if (!is_bigendian()) 
  { // le -> le (don't swap)
    buf[0] = ((uchar*) &ui)[0];
    buf[1] = ((uchar*) &ui)[1];
  } else 
  {
    buf[0] = ((uchar*) &ui)[1];
    buf[1] = ((uchar*) &ui)[0];
  }
}

/**
 * Cortex M tech ref manual, CPUID register description
 * @param sl stlink context
 * @param cpuid pointer to the result object
 */
int stlink_cpu_id(stlink_t *sl, cortex_m3_cpuid_t *cpuid) {
  uint32_t raw;

  if (stlink_read_debug32(sl, STLINK_REG_CM3_CPUID, &raw)) {
    cpuid->implementer_id = 0;
    cpuid->variant = 0;
    cpuid->part = 0;
    cpuid->revision = 0;
    return (-1);
  }

  cpuid->implementer_id = (raw >> 24) & 0x7f;
  cpuid->variant = (raw >> 20) & 0xf;
  cpuid->part = (raw >> 4) & 0xfff;
  cpuid->revision = raw & 0xf;
  return (0);
}


// this function is called by stlink_load_device_params()
// do not call this procedure directly.
int32_t stlink_chip_id(stlink_t *sl, uint32_t *chip_id) {
  int32_t ret;
  cortex_m3_cpuid_t cpu_id;

  // Read the CPU ID to determine where to read the core id
  if(stlink_cpu_id(sl, &cpu_id) ||
      cpu_id.implementer_id != STM32_REG_CMx_CPUID_IMPL_ARM) {
    logmsg("Can not connect to target. Please use \'connect under reset\' and try again");
    return -1;
  }

  /*
   * the chip_id register in the reference manual have
   * DBGMCU_IDCODE / DBG_IDCODE name
   */

  if((sl->core_id == STM32_CORE_ID_M7F_M33_SWD || sl->core_id == STM32_CORE_ID_M7F_M33_JTAG) &&
      cpu_id.part == STM32_REG_CMx_CPUID_PARTNO_CM7) {
    // STM32H7 chipid in 0x5c001000 (RM0433 pg3189)
    ret = stlink_read_debug32(sl, 0x5c001000, chip_id);
  } else if(cpu_id.part == STM32_REG_CMx_CPUID_PARTNO_CM0 ||
             cpu_id.part == STM32_REG_CMx_CPUID_PARTNO_CM0P) {
    // STM32F0 (RM0091, pg914; RM0360, pg713)
    // STM32L0 (RM0377, pg813; RM0367, pg915; RM0376, pg917)
    // STM32G0 (RM0444, pg1367)
    ret = stlink_read_debug32(sl, 0x40015800, chip_id);
  } else if(cpu_id.part == STM32_REG_CMx_CPUID_PARTNO_CM33) {
    // STM32L5 (RM0438, pg2157)
    ret = stlink_read_debug32(sl, 0xe0044000, chip_id);
  } else /* СM3, СM4, CM7 */ {
    // default chipid address

    // STM32F1 (RM0008, pg1087; RM0041, pg681)
    // STM32F2 (RM0033, pg1326)
    // STM32F3 (RM0316, pg1095; RM0313, pg874)
    // STM32F7 (RM0385, pg1676; RM0410, pg1912)
    // STM32L1 (RM0038, pg861)
    // STM32L4 (RM0351, pg1840; RM0394, pg1560)
    // STM32G4 (RM0440, pg2086)
    // STM32WB (RM0434, pg1406)
    ret = stlink_read_debug32(sl, 0xe0042000, chip_id);
  }

  if(ret || !(*chip_id)) {
    *chip_id = 0;
    ret = ret?ret:-1;
    ELOG("Could not find chip id!\n");
  } else {
    *chip_id = (*chip_id) & 0xfff;

    // Fix chip_id for F4 rev A errata, read CPU ID, as CoreID is the same for
    // F2/F4
    if(*chip_id == 0x411 && cpu_id.part == STM32_REG_CMx_CPUID_PARTNO_CM4) {
      *chip_id = 0x413;
    }
  }

  return (ret);
}


/**
 * Reads and decodes the flash parameters, as dynamically as possible
 * @param sl
 * @return 0 for success, or -1 for unsupported core type.
 */
int32_t stlink_load_device_params(stlink_t *sl) {
  // This seems to normally work so is unnecessary info for a normal user.
  // Demoted to debug. -- REW
//DLOG("Loading device parameters....\n");
  const struct stlink_chipid_params *params = NULL;
  stlink_core_id(sl);
  uint32_t flash_size;

  if(stlink_chip_id(sl, &sl->chip_id)) {
    return (-1);
  }

  params = stlink_chipid_get_params(sl->chip_id);

  if(params == NULL) {
    WLOG("unknown chip id! %#x\n", sl->chip_id);
    return (-1);
  }

  if(params->flash_type == STM32_FLASH_TYPE_UNKNOWN) {
    WLOG("Invalid flash type, please check device declaration\n");
    sl->flash_size = 0;
    return (0);
  }

  // These are fixed...
  sl->flash_base = STM32_FLASH_BASE;
  sl->sram_base = STM32_SRAM_BASE;
  stlink_read_debug32(sl, (params->flash_size_reg) & ~3, &flash_size);

  if(params->flash_size_reg & 2) {
    flash_size = flash_size >> 16;
  }
  flash_size = flash_size & 0xffff;

  if((sl->chip_id == STM32_CHIPID_L1_MD ||
       sl->chip_id == STM32_CHIPID_F1_VL_MD_LD ||
       sl->chip_id == STM32_CHIPID_L1_MD_PLUS) &&
      (flash_size == 0)) {
    sl->flash_size = 128 * 1024;
  } else if(sl->chip_id == STM32_CHIPID_L1_CAT2) {
    sl->flash_size = (flash_size & 0xff) * 1024;
  } else if((sl->chip_id & 0xFFF) == STM32_CHIPID_L1_MD_PLUS_HD) {
    // 0 is 384k and 1 is 256k
    if(flash_size == 0) {
      sl->flash_size = 384 * 1024;
    } else {
      sl->flash_size = 256 * 1024;
    }
  } else {
    sl->flash_size = flash_size * 1024;
  }

  sl->flash_type = params->flash_type;
  sl->flash_pgsz = params->flash_pagesize;
  sl->sram_size = params->sram_size;
  sl->sys_base = params->bootrom_base;
  sl->sys_size = params->bootrom_size;
  sl->option_base = params->option_base;
  sl->option_size = params->option_size;
  sl->chip_flags = params->flags;
  sl->otp_base = params->otp_base;
  sl->otp_size = params->otp_size;

  // medium and low devices have the same chipid. ram size depends on flash
  // size. STM32F100xx datasheet Doc ID 16455 Table 2
  if(sl->chip_id == STM32_CHIPID_F1_VL_MD_LD && sl->flash_size < 64 * 1024) {
    sl->sram_size = 0x1000;
  }

  if(sl->chip_id == STM32_CHIPID_G4_CAT3) {
    uint32_t flash_optr;
    stlink_read_debug32(sl, STM32_FLASH_Gx_OPTR, &flash_optr);

    if(!(flash_optr & (1 << STM32_FLASH_G4_OPTR_DBANK))) {
      sl->flash_pgsz <<= 1;
    }
  }

  if(sl->chip_id == STM32_CHIPID_L5x2xx) {
    uint32_t flash_optr;
    stlink_read_debug32(sl, STM32_FLASH_L5_OPTR, &flash_optr);

    if(sl->flash_size == 512*1024 && (flash_optr & (1 << 22)) != 0) {
      sl->flash_pgsz = 0x800;
    }
  }

  // H7 devices with small flash has one bank
  if(sl->chip_flags & CHIP_F_HAS_DUAL_BANK &&
      sl->flash_type == STM32_FLASH_TYPE_H7) {
    if((sl->flash_size / sl->flash_pgsz) <= 1)
      sl->chip_flags &= ~CHIP_F_HAS_DUAL_BANK;
  }
  ILOG("%s: %u KiB SRAM, %u KiB flash in at least %u %s pages.\n",
      params->dev_type, (sl->sram_size / 1024), (sl->flash_size / 1024),
      (sl->flash_pgsz < 1024) ? sl->flash_pgsz : (sl->flash_pgsz / 1024),
      (sl->flash_pgsz < 1024) ? "byte" : "KiB");

  return (0);
}

int _stlink_usb_reset(stlink_t * sl) {
    struct stlink_libusb * const slu = sl->backend_data;
    unsigned char* const data = sl->q_buf;
    unsigned char* const cmd = sl->c_buf;
    ssize_t size;
    int i, rep_len = 2;

    // send reset command
    i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);
    cmd[i++] = STLINK_DEBUG_COMMAND;

    if (sl->version.jtag_api == STLINK_JTAG_API_V1) {
        cmd[i++] = STLINK_DEBUG_APIV1_RESETSYS;
    } else {
        cmd[i++] = STLINK_DEBUG_APIV2_RESETSYS;
    }

    size = send_recv(slu, 1, cmd, slu->cmd_len, data, rep_len, CMD_CHECK_RETRY , "RESETSYS",__LINE__);

    return(size<0?-1:0);
}

int _stlink_usb_jtag_reset(stlink_t * sl, int value) {
    struct stlink_libusb * const slu = sl->backend_data;
    unsigned char* const data = sl->q_buf;
    unsigned char* const cmd = sl->c_buf;
    ssize_t size;
    int rep_len = 2;
    int i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);

    cmd[i++] = STLINK_DEBUG_COMMAND;
    cmd[i++] = STLINK_DEBUG_APIV2_DRIVE_NRST;
    cmd[i++] = value;
    size = send_recv(slu, 1, cmd, slu->cmd_len, data, rep_len, CMD_CHECK_RETRY , "DRIVE_NRST",__LINE__);

    return(size<0?-1:0);
}

int _stlink_usb_step(stlink_t* sl,int flag) {
   struct stlink_libusb * const slu = sl->backend_data;

#ifdef D
   if (slu->log & 1) 
     printf("sl->version.jtag_api %d %d\n",sl->version.jtag_api,STLINK_JTAG_API_V1);
#endif
   if (sl->version.jtag_api != STLINK_JTAG_API_V1) 
   {
//      printf("HERE\n");
        int mask = STLINK_REG_DHCSR_DBGKEY | STLINK_REG_DHCSR_C_HALT | STLINK_REG_DHCSR_C_DEBUGEN;
        if (flag == 0)
          mask |= STLINK_REG_DHCSR_C_MASKINTS;
        // emulates the JTAG v1 API by using DHCSR
        _stlink_usb_write_debug32(sl, STLINK_REG_DHCSR, mask);
/*
STLINK_REG_DHCSR_DBGKEY | STLINK_REG_DHCSR_C_HALT |
                                                        STLINK_REG_DHCSR_C_MASKINTS | STLINK_REG_DHCSR_C_DEBUGEN);
*/
        mask = STLINK_REG_DHCSR_DBGKEY | STLINK_REG_DHCSR_C_STEP | STLINK_REG_DHCSR_C_DEBUGEN;
        if (flag == 0)
          mask |= STLINK_REG_DHCSR_C_MASKINTS;
       
        _stlink_usb_write_debug32(sl, STLINK_REG_DHCSR, mask);
/*
STLINK_REG_DHCSR_DBGKEY | STLINK_REG_DHCSR_C_STEP |
                                                        STLINK_REG_DHCSR_C_MASKINTS | STLINK_REG_DHCSR_C_DEBUGEN);
*/
        
        return _stlink_usb_write_debug32(sl, STLINK_REG_DHCSR, STLINK_REG_DHCSR_DBGKEY | STLINK_REG_DHCSR_C_HALT |
                                                                STLINK_REG_DHCSR_C_DEBUGEN);
    }

    uchar* const data = sl->q_buf;
    uchar* const cmd = sl->c_buf;
    ssize_t size;
    int rep_len = 2;
    int i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);

    cmd[i++] = STLINK_DEBUG_COMMAND;
    cmd[i++] = STLINK_DEBUG_STEPCORE;

    size = send_recv(slu, 1, cmd, slu->cmd_len, data, rep_len, CMD_CHECK_RETRY,"STEPCORE",__LINE__);
    if (size == -1) {
        printf("[!] send_recv STLINK_DEBUG_STEPCORE\n");
        return (int) size;
    }

    return size < 0 ? -1 : 0;
}

/**
 * This seems to do a good job of restarting things from the beginning?
 * @param sl
 */
int _stlink_usb_run(stlink_t* sl,enum run_type type) {
    struct stlink_libusb * const slu = sl->backend_data;

    int res;

    if (sl->version.jtag_api != STLINK_JTAG_API_V1) {
        res = _stlink_usb_write_debug32(sl, STLINK_REG_DHCSR, 
                    STLINK_REG_DHCSR_DBGKEY | STLINK_REG_DHCSR_C_DEBUGEN |
                    ((type==RUN_FLASH_LOADER)?STLINK_REG_DHCSR_C_MASKINTS:0));
        return(res);
    }

    uchar* const data = sl->q_buf;
    uchar* const cmd = sl->c_buf;
    ssize_t size;
    int rep_len = 2;
    int i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);

    cmd[i++] = STLINK_DEBUG_COMMAND;
    cmd[i++] = STLINK_DEBUG_RUNCORE;

    size = send_recv(slu, 1, cmd, slu->cmd_len, data, rep_len,CMD_CHECK_RETRY,"RUNCORE",__LINE__);
    if (size == -1) {
        printf("[!] send_recv STLINK_DEBUG_RUNCORE\n");
        return (int) size;
    }

    return 0;
}

int stlink_soft_reset(stlink_t *sl, int halt_on_reset,int mask) {
  int ret;
  unsigned timeout;
  uint32_t dhcsr, dfsr;

  DLOG(sl,"*** stlink_soft_reset %s***\n", halt_on_reset ? "(halt) " : "");

  // halt core and enable debugging (if not already done)
  // C_DEBUGEN is required to Halt on reset (DDI0337E, p. 10-6)
  stlink_write_debug32(sl, STLINK_REG_DHCSR,
                       STLINK_REG_DHCSR_DBGKEY | STLINK_REG_DHCSR_C_HALT |
                           STLINK_REG_DHCSR_C_DEBUGEN);

  // enable Halt on reset by set VC_CORERESET and TRCENA (DDI0337E, p. 10-10)
  if (halt_on_reset) {
    stlink_write_debug32(sl, STLINK_REG_CM3_DEMCR,mask);
/*
        STLINK_REG_CM3_DEMCR_TRCENA | STLINK_REG_CM3_DEMCR_VC_HARDERR |
            STLINK_REG_CM3_DEMCR_VC_BUSERR | STLINK_REG_CM3_DEMCR_VC_CORERESET);
*/
    // clear VCATCH in the DFSR register
    stlink_write_debug32(sl, STLINK_REG_DFSR, STLINK_REG_DFSR_VCATCH);
  } else {
    stlink_write_debug32(sl, STLINK_REG_CM3_DEMCR,
                         STLINK_REG_CM3_DEMCR_TRCENA |
                             STLINK_REG_CM3_DEMCR_VC_HARDERR |
                             STLINK_REG_CM3_DEMCR_VC_BUSERR);
  }

  // clear S_RESET_ST in the DHCSR register
  stlink_read_debug32(sl, STLINK_REG_DHCSR, &dhcsr);

  // soft reset (core reset) by SYSRESETREQ (DDI0337E, p. 8-23)
  ret = stlink_write_debug32(sl, STLINK_REG_AIRCR,
                             STLINK_REG_AIRCR_VECTKEY |
                                 STLINK_REG_AIRCR_SYSRESETREQ);
  if (ret) {
    logmsg("Soft reset failed: error write to AIRCR");
    return (ret);
  }

  // waiting for a reset within 500ms
  // DDI0337E, p. 10-4, Debug Halting Control and Status Register
  timeout = time_ms() + 500;
  while (time_ms() < timeout) {
    // DDI0337E, p. 10-4, Debug Halting Control and Status Register
    dhcsr = STLINK_REG_DHCSR_S_RESET_ST;
    stlink_read_debug32(sl, STLINK_REG_DHCSR, &dhcsr);
    if ((dhcsr & STLINK_REG_DHCSR_S_RESET_ST) == 0) {
      if (halt_on_reset) {
        // waiting halt by the SYSRESETREQ exception
        // DDI0403E, p. C1-699, Debug Fault Status Register
        dfsr = 0;
        stlink_read_debug32(sl, STLINK_REG_DFSR, &dfsr);
        if ((dfsr & STLINK_REG_DFSR_VCATCH) == 0) {
          continue;
        }
      }
      timeout = 0;
      break;
    }
  }
  // reset DFSR register. DFSR is power-on reset only (DDI0337H, p. 7-5)
  stlink_write_debug32(sl, STLINK_REG_DFSR, STLINK_REG_DFSR_CLEAR);

  if (timeout) {
    logmsg("Soft reset failed: timeout");
    return (-1);
  }

  return (0);
}

int stlink_jtag_reset(stlink_t *sl, int value) {
  DLOG(sl,"*** stlink_jtag_reset %d ***\n", value);
  return (_stlink_usb_jtag_reset(sl, value));
}

int stlink_reset(stlink_t *sl, enum reset_type type) {
  uint32_t dhcsr;
  unsigned timeout;

  logmsg("stlink_reset type %d",type);

  sl->core_stat = TARGET_RESET;

  if (type == RESET_AUTO) {
    // clear S_RESET_ST in DHCSR register for reset state detection
    stlink_read_debug32(sl, STLINK_REG_DHCSR, &dhcsr);
  }

  if (type == RESET_HARD || type == RESET_AUTO) {
    // hardware target reset
    if (sl->version.stlink_v > 1) {
      stlink_jtag_reset(sl, STLINK_DEBUG_APIV2_DRIVE_NRST_LOW);
      // minimum reset pulse duration of 20 us (RM0008, 8.1.2 Power reset)
      usleep(100);
      stlink_jtag_reset(sl, STLINK_DEBUG_APIV2_DRIVE_NRST_HIGH);
    }
    _stlink_usb_reset(sl);
    usleep(10000);
  }

  if (type == RESET_AUTO) {
    /* Check if the S_RESET_ST bit is set in DHCSR
     * This means that a reset has occurred
     * DDI0337E, p. 10-4, Debug Halting Control and Status Register */

    dhcsr = 0;
    int res = stlink_read_debug32(sl, STLINK_REG_DHCSR, &dhcsr);
    if ((dhcsr & STLINK_REG_DHCSR_S_RESET_ST) == 0 && !res) {
      // reset not done yet
      // try reset through AIRCR so that NRST does not need to be connected

      logmsg("stlink_reset NRST is not connected");
      DLOG(sl,"Using reset through SYSRESETREQ\n");
      return stlink_soft_reset(sl, 0,0);
    }

    // waiting for reset the S_RESET_ST bit within 500ms
    timeout = time_ms() + 500;
    while (time_ms() < timeout) {
      dhcsr = STLINK_REG_DHCSR_S_RESET_ST;
      stlink_read_debug32(sl, STLINK_REG_DHCSR, &dhcsr);
      if ((dhcsr & STLINK_REG_DHCSR_S_RESET_ST) == 0) {
        return (0);
      }
    }

    return (-1);
  }

  if (type == RESET_SOFT || type == RESET_SOFT_AND_HALT) {
    return stlink_soft_reset(sl, (type == RESET_SOFT_AND_HALT),
            STLINK_REG_CM3_DEMCR_TRCENA | STLINK_REG_CM3_DEMCR_VC_HARDERR |
            STLINK_REG_CM3_DEMCR_VC_BUSERR | STLINK_REG_CM3_DEMCR_VC_CORERESET);
  }

  return (0);
}

int _stlink_match_speed_map(const uint32_t *map, uint map_size, uint32_t khz) {
    uint i;
    int speed_index = -1;
    int speed_diff = INT_MAX;
    int last_valid_speed = -1;
    bool match = true;

    for (i = 0; i < map_size; i++) {
        if (!map[i]) { continue; }

        last_valid_speed = i;

        if (khz == map[i]) {
            speed_index = i;
            break;
        } else {
            int current_diff = khz - map[i];
            // get abs value for comparison
            current_diff = (current_diff > 0) ? current_diff : -current_diff;

            if (current_diff < speed_diff) {
                speed_diff = current_diff;
                speed_index = i;
            }
        }
    }

    if (speed_index == -1) {
        // This will only be here if we cannot match the slow speed.
        // Use the slowest speed we support.
        speed_index = last_valid_speed;
        match = false;
    } else if (i == map_size) {
        match = false;
    }

    if (!match) {
        logmsg("Unable to match requested speed %d kHz, using %d kHz", khz, map[speed_index]);
    }

    return(speed_index);
}

void stlink_core_stat(stlink_t *sl) {
    if (sl->q_len <= 0)
    {
        printf("stlink_core_stat q_len %d\n",sl->q_len);
        return;
    }

    switch (sl->q_buf[0]) {
    case STLINK_CORE_RUNNING:
        sl->core_stat = STLINK_CORE_RUNNING;
        DLOG(sl,"  core status: running\n");
        return;
    case STLINK_CORE_HALTED:
        sl->core_stat = STLINK_CORE_HALTED;
        DLOG(sl,"  core status: halted\n");
        return;
    default:
        sl->core_stat = STLINK_CORE_STAT_UNKNOWN;
        fprintf(stderr, "  core status: unknown\n");
    }
}
int _stlink_usb_set_swdclk(stlink_t* sl, int clk_freq) {
    struct stlink_libusb * const slu = sl->backend_data;
    unsigned char* const data = sl->q_buf;
    unsigned char* const cmd = sl->c_buf;
    ssize_t size;
    int rep_len = 2;
    int i;

    // clock speed only supported by stlink/v2 and for firmware >= 22
    if (sl->version.stlink_v == 2 && sl->version.jtag_v >= 22) {
        uint16_t clk_divisor;
        if (clk_freq) {
            const uint32_t map[] = {5, 15, 25, 50, 100, 125, 240, 480, 950, 1200, 1800, 4000};
            int speed_index = _stlink_match_speed_map(map, STLINK_ARRAY_SIZE(map), clk_freq);
            switch (map[speed_index]) {
            case 5:   clk_divisor = STLINK_SWDCLK_5KHZ_DIVISOR; break;
            case 15:  clk_divisor = STLINK_SWDCLK_15KHZ_DIVISOR; break;
            case 25:  clk_divisor = STLINK_SWDCLK_25KHZ_DIVISOR; break;
            case 50:  clk_divisor = STLINK_SWDCLK_50KHZ_DIVISOR; break;
            case 100: clk_divisor = STLINK_SWDCLK_100KHZ_DIVISOR; break;
            case 125: clk_divisor = STLINK_SWDCLK_125KHZ_DIVISOR; break;
            case 240: clk_divisor = STLINK_SWDCLK_240KHZ_DIVISOR; break;
            case 480: clk_divisor = STLINK_SWDCLK_480KHZ_DIVISOR; break;
            case 950: clk_divisor = STLINK_SWDCLK_950KHZ_DIVISOR; break;
            case 1200: clk_divisor = STLINK_SWDCLK_1P2MHZ_DIVISOR; break;
            default:
            case 1800: clk_divisor = STLINK_SWDCLK_1P8MHZ_DIVISOR; break;
            case 4000: clk_divisor = STLINK_SWDCLK_4MHZ_DIVISOR; break;
            }
        } else
            clk_divisor = STLINK_SWDCLK_1P8MHZ_DIVISOR;
        i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);

        cmd[i++] = STLINK_DEBUG_COMMAND;
        cmd[i++] = STLINK_DEBUG_APIV2_SWD_SET_FREQ;
        cmd[i++] = clk_divisor & 0xFF;
        cmd[i++] = (clk_divisor >> 8) & 0xFF;
        size = send_recv(slu, 1, cmd, slu->cmd_len, data, rep_len, CMD_CHECK_RETRY, "SWD_SET_FREQ",__LINE__);

        return(size<0?-1:0);
    } else if (sl->version.stlink_v == 3) {
        int speed_index;
        uint32_t map[STLINK_V3_MAX_FREQ_NB];
        i = fill_command(sl, SG_DXFER_FROM_DEV, 16);

        cmd[i++] = STLINK_DEBUG_COMMAND;
        cmd[i++] = STLINK_DEBUG_APIV3_GET_COM_FREQ;
        cmd[i++] = 0; // SWD mode
        size = send_recv(slu, 1, cmd, slu->cmd_len, data, 52, CMD_CHECK_STATUS , "GET_COM_FREQ",__LINE__);

        if (size < 0) {
            return(-1);
        }

        int speeds_size = data[8];
        if (speeds_size > STLINK_V3_MAX_FREQ_NB) {
            speeds_size = STLINK_V3_MAX_FREQ_NB;
        }

        for (i = 0; i < speeds_size; i++) map[i] = le_to_h_u32(&data[12 + 4 * i]);

        // Set to zero all the next entries
        for (i = speeds_size; i < STLINK_V3_MAX_FREQ_NB; i++) map[i] = 0;

        if (!clk_freq) clk_freq = 1000; // set default frequency
        speed_index = _stlink_match_speed_map(map, STLINK_ARRAY_SIZE(map), clk_freq);

        i = fill_command(sl, SG_DXFER_FROM_DEV, 16);

        cmd[i++] = STLINK_DEBUG_COMMAND;
        cmd[i++] = STLINK_DEBUG_APIV3_SET_COM_FREQ;
        cmd[i++] = 0; // SWD mode
        cmd[i++] = 0;
        cmd[i++] = (uint8_t)((map[speed_index] >> 0) & 0xFF);
        cmd[i++] = (uint8_t)((map[speed_index] >> 8) & 0xFF);
        cmd[i++] = (uint8_t)((map[speed_index] >> 16) & 0xFF);
        cmd[i++] = (uint8_t)((map[speed_index] >> 24) & 0xFF);

        size = send_recv(slu, 1, cmd, slu->cmd_len, data, 8, CMD_CHECK_STATUS , "SET_COM_FREQ",__LINE__);

        return(size<0?-1:0);
    } else if (clk_freq) {
        WLOG("ST-Link firmware does not support frequency setup\n");
    }


    return(-1);
}

int stlink_set_swdclk(stlink_t *sl, int freq_khz) {
  DLOG(sl,"*** set_swdclk ***\n");
  return _stlink_usb_set_swdclk(sl, freq_khz);
}

int stlink_write_debug32(stlink_t *sl, uint addr, uint data) {
    DLOG(sl,"*** stlink_write_debug32 %x to %#x\n", data, addr);
    return _stlink_usb_write_debug32(sl, addr, data);
}

int stlink_write_mem32(stlink_t *sl, uint addr, uchar *buf,uint len) {
    DLOG(sl,"*** stlink_write_mem32 %u bytes to %#x\n", len, addr);
    if (len % 4 != 0) {
        fprintf(stderr, "Error: Data length doesn't have a 32 bit alignment: +%d byte.\n", len % 4);
        abort();
    }
    return _stlink_usb_write_mem32(sl, addr, buf, len);
}

int stlink_read_mem32(stlink_t *sl, uint addr, uchar *buf, ushort len) {
    DLOG(sl,"*** stlink_read_mem32 ***\n");
    if (len % 4 != 0) { // !!! never ever: fw gives just wrong values
        fprintf(stderr, "Error: Data length doesn't have a 32 bit alignment: +%d byte.\n",
                len % 4);
        abort();
    }
    return _stlink_usb_read_mem32(sl, addr, buf, len);
}

int stlink_write_reg(stlink_t *sl, uint reg, int idx) {
    DLOG(sl,"*** stlink_write_reg\n");
    return _stlink_usb_write_reg(sl, reg, idx);
}

int stlink_run(stlink_t *sl,enum run_type type) {
    DLOG(sl,"*** stlink_run ***\n");
    return _stlink_usb_run(sl,type);
}

int stlink_exit_debug_mode(stlink_t *sl) {
    int ret;

    DLOG(sl,"*** stlink_exit_debug_mode ***\n");
    ret = stlink_write_debug32(sl, STLINK_REG_DHCSR, STLINK_REG_DHCSR_DBGKEY);
    if (ret == -1)
        return ret;

    return _stlink_usb_exit_debug_mode(sl);
}

uint32_t get_stm32l0_flash_base(stlink_t *sl) {
  switch (sl->chip_id) {
  case STM32_CHIPID_L0_CAT1:
  case STM32_CHIPID_L0_CAT2:
  case STM32_CHIPID_L0_CAT3:
  case STM32_CHIPID_L0_CAT5:
    return (STM32_FLASH_L0_REGS_ADDR);

  case STM32_CHIPID_L1_CAT2:
  case STM32_CHIPID_L1_MD:
  case STM32_CHIPID_L1_MD_PLUS:
  case STM32_CHIPID_L1_MD_PLUS_HD:
  case STM32_CHIPID_L152_RE:
    return (STM32_FLASH_Lx_REGS_ADDR);

  default:
    WLOG("Flash base use default L0 address\n");
    return (STM32_FLASH_L0_REGS_ADDR);
  }
}

static void stop_wdg_in_debug(stlink_t *sl) {
  uint32_t dbgmcu_cr;
  uint32_t set;
  uint32_t value;

//printf("stop_wdg_in_debug flash_type %d\n",sl->flash_type);
  switch (sl->flash_type) {
  case STM32_FLASH_TYPE_F0_F1_F3:
  case STM32_FLASH_TYPE_F1_XL:
  case STM32_FLASH_TYPE_G4:
    dbgmcu_cr = STM32F0_DBGMCU_CR;
    set = (1 << STM32F0_DBGMCU_CR_IWDG_STOP) |
          (1 << STM32F0_DBGMCU_CR_WWDG_STOP);
    break;
  case STM32_FLASH_TYPE_F2_F4:
  case STM32_FLASH_TYPE_F7:
  case STM32_FLASH_TYPE_L4:
    dbgmcu_cr = STM32F4_DBGMCU_APB1FZR1;
    set = (1 << STM32F4_DBGMCU_APB1FZR1_IWDG_STOP) |
          (1 << STM32F4_DBGMCU_APB1FZR1_WWDG_STOP);
    break;
  case STM32_FLASH_TYPE_L0_L1:
  case STM32_FLASH_TYPE_G0:
    if(get_stm32l0_flash_base(sl) == STM32_FLASH_Lx_REGS_ADDR) {
      dbgmcu_cr = STM32L1_DBGMCU_APB1_FZ;
      set = (1 << STM32L1_DBGMCU_APB1_FZ_IWDG_STOP) |
            (1 << STM32L1_DBGMCU_APB1_FZ_WWDG_STOP);
    } else {
      dbgmcu_cr = STM32L0_DBGMCU_APB1_FZ;
      set = (1 << STM32L0_DBGMCU_APB1_FZ_IWDG_STOP) |
            (1 << STM32L0_DBGMCU_APB1_FZ_WWDG_STOP);
    }
    break;
  case STM32_FLASH_TYPE_H7:
    dbgmcu_cr = STM32H7_DBGMCU_APB1HFZ;
    set = (1 << STM32H7_DBGMCU_APB1HFZ_IWDG_STOP);
    break;
  case STM32_FLASH_TYPE_WB_WL:
    dbgmcu_cr = STM32WB_DBGMCU_APB1FZR1;
    set = (1 << STM32WB_DBGMCU_APB1FZR1_IWDG_STOP) |
          (1 << STM32WB_DBGMCU_APB1FZR1_WWDG_STOP);
    break;
  default:
    return;
  }

  if(!stlink_read_debug32(sl, dbgmcu_cr, &value)) {
    stlink_write_debug32(sl, dbgmcu_cr, value | set);
  }
}

// Force the core into the debug mode -> halted state.
int stlink_force_debug(stlink_t *sl) {
#ifdef D
  if (sl->backend_data->log & 1) 
    printf("stlink_force_debug enter\n");
#endif
  DLOG(sl,"*** stlink_force_debug_mode ***\n");
  int res = _stlink_usb_force_debug(sl);
  if (res) {
#ifdef D
   if (sl->backend_data->log & 1) 
     printf("stlink_force_debug exit %d\n",res);
#endif
     return (res);
  }
  // Stop the watchdogs in the halted state for suppress target reboot
  stop_wdg_in_debug(sl);
#ifdef D
  if (sl->backend_data->log & 1) 
    printf("stlink_force_debug exit 0\n");
#endif
  return (0);
}

int stlink_step(stlink_t *sl,int flag) {
    DLOG(sl,"*** stlink_step ***\n");
    return _stlink_usb_step(sl,flag);
}

int stlink_read_reg(stlink_t *sl, int idx, struct stlink_reg *regp) {
    DLOG(sl,"*** stlink_read_all_regs ***\n");
    return _stlink_usb_read_reg(sl, idx, regp);
}

int stlink_read_all_regs(stlink_t *sl, struct stlink_reg *regp) {
    DLOG(sl,"*** stlink_read_all_regs ***\n");
    return _stlink_usb_read_all_regs(sl, regp);
}

int stlink_read_all_unsupported_regs(stlink_t *sl, struct stlink_reg *regp) {
    DLOG(sl,"*** stlink_read_all_unsupported_regs ***\n");
    return _stlink_usb_read_all_unsupported_regs(sl, regp);
}

/**
 * Decode the version bits, originally from -sg, verified with usb
 * @param sl stlink context, assumed to contain valid data in the buffer
 * @param slv output parsed version object
 */
void _parse_version(stlink_t *sl, stlink_version_t *slv) {
    uint b0 = sl->q_buf[0]; //lsb
    uint b1 = sl->q_buf[1];
    uint b2 = sl->q_buf[2];
    uint b3 = sl->q_buf[3];
    uint b4 = sl->q_buf[4];
    uint b5 = sl->q_buf[5]; //msb

    // b0 b1                       || b2 b3  | b4 b5
    // 4b        | 6b     | 6b     || 2B     | 2B
    // stlink_v  | jtag_v | swim_v || st_vid | stlink_pid

    slv->stlink_v = (b0 & 0xf0) >> 4;
    slv->jtag_v = ((b0 & 0x0f) << 2) | ((b1 & 0xc0) >> 6);
    slv->swim_v = b1 & 0x3f;
    slv->st_vid = (b3 << 8) | b2;
    slv->stlink_pid = (b5 << 8) | b4;
    return;
}

#ifdef OLD
// stlink_chip_id() is called by stlink_load_device_params()
// do not call this procedure directly.
int stlink_chip_id(stlink_t *sl, uint32_t *chip_id) {
  int ret;
  cortex_m3_cpuid_t cpu_id;

  // Read the CPU ID to determine where to read the core id
  if (stlink_cpu_id(sl, &cpu_id) ||
      cpu_id.implementer_id != STLINK_REG_CMx_CPUID_IMPL_ARM) {
    logmsg("Can not connect to target. Please use \'connect under reset\' and "
         "try again");
    return -1;
  }

  /*
   * the chip_id register in the reference manual have
   * DBGMCU_IDCODE / DBG_IDCODE name
   *
   */

  if ((sl->core_id == STM32H7_CORE_ID || sl->core_id == STM32H7_CORE_ID_JTAG) &&
      cpu_id.part == STLINK_REG_CMx_CPUID_PARTNO_CM7) {
    // STM32H7 chipid in 0x5c001000 (RM0433 pg3189)
    ret = stlink_read_debug32(sl, 0x5c001000, chip_id);
  } else if (cpu_id.part == STLINK_REG_CMx_CPUID_PARTNO_CM0 ||
             cpu_id.part == STLINK_REG_CMx_CPUID_PARTNO_CM0P) {
    // STM32F0 (RM0091, pg914; RM0360, pg713)
    // STM32L0 (RM0377, pg813; RM0367, pg915; RM0376, pg917)
    // STM32G0 (RM0444, pg1367)
    ret = stlink_read_debug32(sl, 0x40015800, chip_id);
  } else if (cpu_id.part == STLINK_REG_CMx_CPUID_PARTNO_CM33) {
    // STM32L5 (RM0438, pg2157)
    ret = stlink_read_debug32(sl, 0xe0044000, chip_id);
  } else /* СM3, СM4, CM7 */ {
    // default chipid address
    // STM32F1 (RM0008, pg1087; RM0041, pg681)
    // STM32F2 (RM0033, pg1326)
    // STM32F3 (RM0316, pg1095; RM0313, pg874)
    // STM32F7 (RM0385, pg1676; RM0410, pg1912)
    // STM32L1 (RM0038, pg861)
    // STM32L4 (RM0351, pg1840; RM0394, pg1560)
    // STM32G4 (RM0440, pg2086)
    // STM32WB (RM0434, pg1406)
    ret = stlink_read_debug32(sl, 0xe0042000, chip_id);
  }

  if (ret || !(*chip_id)) 
  {
    *chip_id = 0;
    ret = ret?ret:-1;
    ELOG("Could not find chip id!\n");
  } else 
  {
    *chip_id = (*chip_id) & 0xfff;

    // Fix chip_id for F4 rev A errata, read CPU ID, as CoreID is the same for
    // F2/F4
    if (*chip_id == 0x411 && cpu_id.part == STLINK_REG_CMx_CPUID_PARTNO_CM4) {
      *chip_id = 0x413;
    }
  }

  return (ret);
}
#endif

int stlink_read_debug32(stlink_t *sl, uint addr, uint *data) {
    int ret;

    ret = _stlink_usb_read_debug32(sl, addr, data);
    if (!ret)
      DLOG(sl,"*** stlink_read_debug32 add %08x is %08x\n", addr,*data);

    return ret;
}

int stlink_status(stlink_t *sl) {
    int ret;

    DLOG(sl,"*** stlink_status ***\n");
    ret = _stlink_usb_status(sl);
    stlink_core_stat(sl);

    return ret;
}

void stlink_print_data(stlink_t * sl) {
    if (sl->q_len <= 0 || sl->verbose < UDEBUG)
        return;
    if (sl->verbose > 2)
        fprintf(stdout, "data_len = %d 0x%x\n", sl->q_len, sl->q_len);

    for (int i = 0; i < sl->q_len; i++) {
        if (i % 16 == 0) {
            /*
               if (sl->q_data_dir == Q_DATA_OUT)
               fprintf(stdout, "\n<- 0x%08x ", sl->q_addr + i);
               else
               fprintf(stdout, "\n-> 0x%08x ", sl->q_addr + i);
               */
        }
        fprintf(stdout, " %02x", (uint) sl->q_buf[i]);
    }
    fputs("\n\n", stdout);
}

int stlink_core_id(stlink_t *sl) {
    int ret;

    DLOG(sl,"*** stlink_core_id ***\n");
    ret = _stlink_usb_core_id(sl);
    if (ret == -1) {
        ELOG("Failed to read core_id\n");
        return ret;
    }
    if (sl->verbose > 2)
        stlink_print_data(sl);
    DLOG(sl,"core_id = 0x%08x\n", sl->core_id);
    return ret;
}

int stlink_version(stlink_t *sl) {
    DLOG(sl,"*** looking up stlink version\n");
    if (_stlink_usb_version(sl))
        return -1;

    _parse_version(sl, &sl->version);

    DLOG(sl,"st vid         = 0x%04x (expect 0x%04x)\n", sl->version.st_vid, STLINK_USB_VID_ST);
    DLOG(sl,"stlink pid     = 0x%04x\n", sl->version.stlink_pid);
    DLOG(sl,"stlink version = 0x%x\n", sl->version.stlink_v);
    DLOG(sl,"jtag version   = 0x%x\n", sl->version.jtag_v);
    DLOG(sl,"swim version   = 0x%x\n", sl->version.swim_v);
    if (sl->version.jtag_v == 0) {
        DLOG(sl,"    notice: the firmware doesn't support a jtag/swd interface\n");
    }
    if (sl->version.swim_v == 0) {
        DLOG(sl,"    notice: the firmware doesn't support a swim interface\n");
    }

    return 0;
}

uint read_uint32(const uchar *c, const int pt) {
    uint ui;
    char *p = (char *) &ui;

    if (!is_bigendian()) { // le -> le (don't swap)
        p[0] = c[pt + 0];
        p[1] = c[pt + 1];
        p[2] = c[pt + 2];
        p[3] = c[pt + 3];
    } else {
        p[0] = c[pt + 3];
        p[1] = c[pt + 2];
        p[2] = c[pt + 1];
        p[3] = c[pt + 0];
    }
    return ui;
}

void write_uint32(uchar* buf, uint ui) {
    if (!is_bigendian()) { // le -> le (don't swap)
        buf[0] = ((uchar*) &ui)[0];
        buf[1] = ((uchar*) &ui)[1];
        buf[2] = ((uchar*) &ui)[2];
        buf[3] = ((uchar*) &ui)[3];
    } else {
        buf[0] = ((uchar*) &ui)[3];
        buf[1] = ((uchar*) &ui)[2];
        buf[2] = ((uchar*) &ui)[1];
        buf[3] = ((uchar*) &ui)[0];
    }
}

int stlink_enter_swd_mode(stlink_t *sl) {
    DLOG(sl,"*** stlink_enter_swd_mode ***\n");
    return _stlink_usb_enter_swd_mode(sl);
}

void _stlink_usb_close(stlink_t* sl) {
    if (!sl)
        return;

    struct stlink_libusb * const handle = sl->backend_data;
    // maybe we couldn't even get the usb device?
    if (handle != NULL) {
        if (handle->usb_handle != NULL) {
            libusb_close(handle->usb_handle);
        }

        libusb_exit(handle->libusb_ctx);
        free(handle);
    }
}

void stlink_close(stlink_t *sl) {
    DLOG(sl,"*** stlink_close ***\n");
    if (!sl)
         return;
    _stlink_usb_close(sl);
    free(sl);
}

int stlink_current_mode(stlink_t *sl) {
    int mode = _stlink_usb_current_mode(sl);
    switch (mode) {
    case STLINK_DEV_DFU_MODE:
        DLOG(sl,"stlink current mode: dfu\n");
        return mode;
    case STLINK_DEV_DEBUG_MODE:
        DLOG(sl,"stlink current mode: debug (jtag or swd)\n");
        return mode;
    case STLINK_DEV_MASS_MODE:
        DLOG(sl,"stlink current mode: mass\n");
        return mode;
    }
    DLOG(sl,"stlink mode: unknown!\n");
    return STLINK_DEV_UNKNOWN_MODE;
}

int stlink_exit_dfu_mode(stlink_t *sl) {
    DLOG(sl,"*** stlink_exit_dfu_mode ***\n");
    return _stlink_usb_exit_dfu_mode(sl);
}

#ifdef OLD
#if defined(D) || defined(D1)
void dumpmem(uchar *add,size_t len,char *off)
{
  size_t i,j;
  uchar *badd;
  char c;

  badd = add;
//printf("In dumpmem add %p len %ld\n",add,(long) len);
  while(len > 0)
  {
    j = len;
    if (j > 16) j = 16;
    printf("%8p: ",add - badd + off);
    for (i = 0; i < j; i = i + 1)
      printf("%02x ",add[i] & 0xff);
    for (; i < 16; i = i + 1) printf("   ");
    for (i = 0; i < j; i = i + 1)
    {
      c = add[i];
      if (c < ' ' || c >= 127) c = '.';
      printf("%c",c);
    }
    putchar('\n');
    if (len >= 16)
      len = len - 16;
    else
      len = 0;
    add = add + 16;
  }
}
#endif
#endif

ssize_t send_recv(struct stlink_libusb* handle, int terminate,
                  unsigned char* txbuf, size_t txsize, unsigned char* rxbuf,
                  size_t rxsize, int check_error, const char *cmd,int line) {
    // Note: txbuf and rxbuf can point to the same area
    int res,retry = 0;
    size_t t;

    while (1) {
        res = 0;
        t = libusb_bulk_transfer(handle->usb_handle, handle->ep_req, txbuf, (int)txsize, &res, 3000);
#ifdef D
    if (handle->log & 1)
    {
    static int count = 0;
    static double t0;
    double dt;
    putchar('\n');
    struct timeval tv;
    gettimeofday(&tv,NULL);
    if (count == 0)
    {
      t0 = tv.tv_sec + tv.tv_usec / 1000000.0;
      printf("Start at %ld.%06ld\n",tv.tv_sec,tv.tv_usec);
    }
    dt = tv.tv_sec + tv.tv_usec / 1000000.0 - t0;
    printf("%5d %10.6f send_recv terminate %d txsize %ld rxsize %ld chk %d cmd %s line %d\n",
           ++count,dt,terminate,txsize,rxsize,check_error,cmd,line);
    printf("      libusb_bulk_transer Write USB Handle %p ep_req %x size %ld -> %ld\n",
           handle->usb_handle,handle->ep_req,txsize,t);
//  printf("      usb.send line %4d %02x ",line,txbuf[0]); TODO
    printf("      usb.send %02x ",txbuf[0]);
    if (handle->prev_terminate == 0)
      printf("continuation buffer ");
    else
    {
      switch(txbuf[0])
      {
      int *pi;
      case STLINK_GET_CURRENT_MODE:
        printf("STLINK_GET_CURRENT_MODE ");
        break;
      case STLINK_GET_VERSION:
        printf("STLINK_GET_VERSION ");
        break;
      case STLINK_GET_TARGET_VOLTAGE:
        printf("STLINK_GET_TARGET_VOLTAGE ");
        break;
      case STLINK_DEBUG_COMMAND:
        printf("DEBUG_COMMAND ");
        printf("%02x ",txbuf[1]);
        switch(txbuf[1])
        {
          case STLINK_DEBUG_APIV1_WRITEREG:
            pi = (int *) (txbuf + 3);
            printf("STLINK_DEBUG_APIV1_WRITEREG idx %d val %08x",txbuf[2],*pi);
            break;
          case STLINK_DEBUG_APIV2_WRITEREG:
            pi = (int *) (txbuf + 3);
            printf("STLINK_DEBUG_APIV2_WRITEREG idx %d val %08x",txbuf[2],*pi);
            break;
          case STLINK_DEBUG_APIV2_ENTER:
            printf("STLINK_DEBUG_APIV2_ENTER ");
            break;
          case STLINK_DEBUG_GETSTATUS:
            printf("STLINK_DEBUG_GETSTATUS ");
            break;
          case STLINK_DEBUG_FORCEDEBUG:
            printf("STLINK_DEBUG_FORCEDEBUG ");
            break;
          case STLINK_DEBUG_APIV2_DRIVE_NRST:
            printf("STLINK_DEBUG_APIV2_DRIVE_NRST val %02x",txbuf[2]);
            break;
          case STLINK_DEBUG_APIV2_READ_IDCODES:
            printf("STLINK_DEBUG_APIV2_READ_IDCODES");
            break;
          case STLINK_DEBUG_APIV2_RESETSYS:
            printf("STLINK_DEBUG_APIV2_RESETSYS");
            break;
          case STLINK_DEBUG_APIV2_READDEBUGREG:
            pi = (int *) (txbuf + 2);
            printf("STLINK_DEBUG_APIV2_READDEBUGREG add %08x",*pi);
            break;
          case STLINK_DEBUG_APIV2_WRITEDEBUGREG:
            pi = (int *) (txbuf + 2);
            printf("STLINK_DEBUG_APIV2_WRITEDEBUGREG add %08x val %08x",*pi,pi[1]);
            break;
          case STLINK_DEBUG_APIV2_GETLASTRWSTATUS:
            printf("STLINK_DEBUG_APIV1_GETLASTRWSTATUS");
            break;
          case STLINK_DEBUG_APIV2_GETLASTRWSTATUS2:
            printf("STLINK_DEBUG_APIV2_GETLASTRWSTATUS2");
            break;
          case STLINK_DEBUG_APIV1_READREG:
            printf("STLINK_DEBUG_APIV1_READREG idx %d",txbuf[2]);
            break;
          case STLINK_DEBUG_APIV2_READREG:
            printf("STLINK_DEBUG_APIV2_READREG idx %d",txbuf[2]);
            break;
          case STLINK_DEBUG_APIV1_READALLREGS:
            printf("STLINK_DEBUG_APIV1_READALLREGS");
            break;
          case STLINK_DEBUG_APIV2_READALLREGS:
            printf("STLINK_DEBUG_APIV2_READALLREGS");
            break;
          case STLINK_DEBUG_READMEM_32BIT:
            pi = (int *) (txbuf + 2);
            printf("STLINK_DEBUG_READMEM_32BIT add %08x size %08x",pi[0],pi[1]);
            break;
          case STLINK_DEBUG_RUNCORE:
            printf("STLINK_DEBUG_RUNCORE ");
            break;
          case STLINK_DEBUG_STEPCORE:
            printf("STLINK_DEBUG_STEPCORE ");
            break;
          case STLINK_DEBUG_WRITEMEM_32BIT:
            pi = (int *) (txbuf + 2);
            printf("STLINK_DEBUG_WRITEMEM_32BIT add %08x size %08x",pi[0],pi[1]);
            break;
          case STLINK_DEBUG_WRITEMEM_8BIT:
            pi = (int *) (txbuf + 2);
            printf("STLINK_DEBUG_WRITEMEM_8BIT add %08x size %08x",pi[0],pi[1]);
            break;
          case STLINK_DEBUG_EXIT:
            printf("STLINK_DEBUG_EXIT ");
            break;
          case STLINK_DEBUG_READCOREID:
            printf("STLINK_DEBUG_READCOREID ");
            break;
          case STLINK_DEBUG_ENTER_SWD:
            printf("STLINK_DEBUG_ENTER_SWD ");
            break;
          default:
            printf("??? ");
            break;
        }
        break;
      case STLINK_DFU_COMMAND:
        printf("STLINK_DFU_COMMAND ");
        printf("%02x ",txbuf[1]);
        switch(txbuf[1])
        {
          case STLINK_DFU_EXIT:
            printf("STLINK_DFU_EXIT ");
            break;
          default:
            printf("??? ");
            break;
        }
        break;
      default:
        printf("??? ");
        break;
      }
    }
    putchar('\n');
    dumpmem(txbuf,txsize,0);
    handle->prev_terminate = terminate;
    }
#endif

        if (t) {
            ELOG("%s send request failed: %s\n", cmd, libusb_error_name(t));
            return(-1);
        } else if ((size_t)res != txsize) {
            ELOG("%s send request wrote %u bytes, instead of %u\n",
                   cmd, (uint)res, (uint)txsize);
        }

        if (rxsize != 0) {
            t = libusb_bulk_transfer(handle->usb_handle, handle->ep_rep, rxbuf, (int)rxsize, &res, 3000);

#ifdef D
            if (handle->log & 1)
            {
            printf("      libusb_bulk_transer Read  USB Handle %p ep_rep %x size %ld -> %ld\n",
                   handle->usb_handle,handle->ep_rep,rxsize,t);
            dumpmem(rxbuf,rxsize,0);
            if (t == 0)
            {
              switch(txbuf[0])
              {
                int *pi;
                case STLINK_DEBUG_APIV2_READDEBUGREG:
                  pi = (int *) rxbuf;
                  printf("       val %08x\n",*pi);
                  break;
                case STLINK_DEBUG_APIV2_READ_IDCODES:
                  pi = (int *) rxbuf;
                  printf("       v1 %08x v2 %08x\n",*pi,pi[1]);
                  break;
              }
            }
            }
#endif
            if (t) {
                ELOG("%s read reply failed: %s\n", cmd, libusb_error_name(t));
                return(-1);
            }

            /* Checking the command execution status stored in the first byte of the response */
            if (handle->protocoll != 1 && check_error >= CMD_CHECK_STATUS &&
                        rxbuf[0] != STLINK_DEBUG_ERR_OK) {
                switch(rxbuf[0]) {
                case STLINK_DEBUG_ERR_AP_WAIT:
                case STLINK_DEBUG_ERR_DP_WAIT:
                    if (check_error == CMD_CHECK_RETRY && retry < 3) {
                        uint delay_us = (1<<retry) * 1000;
                        DLOG(handle->sl,"%s wait error (0x%02X), delaying %u us and retry\n", cmd, rxbuf[0], delay_us);
                        usleep(delay_us);
                        retry++;
                        continue;
                    }
                    DLOG(handle->sl,"%s wait error (0x%02X)\n", cmd, rxbuf[0]);
                    break;
                case STLINK_DEBUG_ERR_FAULT: DLOG(handle->sl,"%s response fault\n", cmd); break;
                case STLINK_DEBUG_ERR_AP_FAULT: DLOG(handle->sl,"%s access port fault\n", cmd); break;
                case STLINK_DEBUG_ERR_DP_FAULT: DLOG(handle->sl,"%s debug port fault\n", cmd); break;
                case STLINK_DEBUG_ERR_AP_ERROR: DLOG(handle->sl,"%s access port error\n", cmd); break;
                case STLINK_DEBUG_ERR_DP_ERROR: DLOG(handle->sl,"%s debug port error\n", cmd); break;
                case STLINK_DEBUG_ERR_WRITE_VERIFY:  DLOG(handle->sl,"%s verification error\n", cmd); break;
                case STLINK_DEBUG_ERR_WRITE:  DLOG(handle->sl,"%s write error\n", cmd); break;
                default: DLOG(handle->sl,"%s error (0x%02X)\n", cmd, rxbuf[0]); break;
                }

                return(-1);
            }

            if (check_error == CMD_CHECK_REP_LEN && res != (int)rxsize) {
                ELOG("%s wrong reply length\n", cmd);
                res = -1;
            }
        }

        if ((handle->protocoll == 1) && terminate) {
            // read the SG reply
            unsigned char sg_buf[13];
            t = libusb_bulk_transfer(handle->usb_handle, handle->ep_rep, sg_buf, 13, &res, 3000);

            if (t) {
                ELOG("%s read storage failed: %s\n", cmd, libusb_error_name(t));
                return(-1);
            }

            // The STLink doesn't seem to evaluate the sequence number.
            handle->sg_transfer_idx++;
        }

#ifdef D
        if (handle->log & 1)
        printf("      send_recv ret %d\n",res);
#endif
        return(res);
    }
}

static inline int send_only
(struct stlink_libusb* handle, int terminate,
 uchar* txbuf, size_t txsize,char *name,int l) {
    return (int) send_recv(handle, terminate, txbuf, txsize, NULL, 0,CMD_CHECK_NO,name,l);
}

int fill_command (stlink_t * sl, enum SCSI_Generic_Direction dir, uint len) {
    struct stlink_libusb * const slu = sl->backend_data;
    uchar* const cmd = sl->c_buf;
    int i = 0;
    memset(cmd, 0, sizeof (sl->c_buf));
    if(slu->protocoll == 1) {
        cmd[i++] = 'U';
        cmd[i++] = 'S';
        cmd[i++] = 'B';
        cmd[i++] = 'C';
        write_uint32(&cmd[i], slu->sg_transfer_idx);
        write_uint32(&cmd[i + 4], len);
        i += 8;
        cmd[i++] = (dir == SG_DXFER_FROM_DEV)?0x80:0;
        cmd[i++] = 0; /* Logical unit */
        cmd[i++] = 0xa; /* Command length */
    }
    return i;
}

int _stlink_usb_version(stlink_t *sl) {
  struct stlink_libusb * const slu = sl->backend_data;
#ifdef D
  if (slu->log & 1)
    printf("_stlink_usb_version\n");
#endif
    uchar* const data = sl->q_buf;
    uchar* const cmd  = sl->c_buf;
    ssize_t size;
    uint rep_len = 6;
    int i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);

    cmd[i++] = STLINK_GET_VERSION;

    size = send_recv(slu, 1, cmd, slu->cmd_len, data, rep_len,CMD_CHECK_REP_LEN,"GET_VERSION",__LINE__);
    if (size == -1) {
        printf("[!] send_recv STLINK_GET_VERSION\n");
        return (int) size;
    }

    return 0;
}

int _stlink_usb_read_debug32(stlink_t *sl, uint32_t addr, uint32_t *data) {
    struct stlink_libusb * const slu = sl->backend_data;
    unsigned char* const rdata = sl->q_buf;
    unsigned char* const cmd  = sl->c_buf;
    ssize_t size;
    const int rep_len = 8;

    int i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);
    cmd[i++] = STLINK_DEBUG_COMMAND;
    cmd[i++] = STLINK_DEBUG_APIV2_READDEBUGREG;
    write_uint32(&cmd[i], addr);
    size = send_recv(slu, 1, cmd, slu->cmd_len, rdata, rep_len, CMD_CHECK_RETRY, "READDEBUGREG",__LINE__);

    if (size < 0) {
        return(-1);
    }

    *data = read_uint32(rdata, 4);
#ifdef D
    if (slu->log & 1)
      printf("READDEBUGREG  %-24s %08x: %08x size %ld\n",regname(addr),addr,*data,size);
#endif

    return(0);
}

int _stlink_usb_write_debug32(stlink_t *sl, uint32_t addr, uint32_t data) {
    struct stlink_libusb * const slu = sl->backend_data;
    unsigned char* const rdata = sl->q_buf;
    unsigned char* const cmd  = sl->c_buf;
    ssize_t size;
    const int rep_len = 2;

    int i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);
    cmd[i++] = STLINK_DEBUG_COMMAND;
    cmd[i++] = STLINK_DEBUG_APIV2_WRITEDEBUGREG;
    write_uint32(&cmd[i], addr);
    write_uint32(&cmd[i + 4], data);
    size = send_recv(slu, 1, cmd, slu->cmd_len, rdata, rep_len, CMD_CHECK_RETRY, "WRITEDEBUGREG",__LINE__);
#ifdef D
    if (slu->log & 1)
      printf("WRITEDEBUGREG %-24s %08x: %08x size %ld\n",regname(addr),addr,data,size);
#endif

    return(size<0?-1:0);
}

int _stlink_usb_write_mem32(stlink_t *sl, uint addr, uchar *buf, ushort len) {
    struct stlink_libusb * const slu = sl->backend_data;
//  uchar* const data = sl->q_buf;
    uchar* const cmd  = sl->c_buf;
    int i, ret;

    i = fill_command(sl, SG_DXFER_TO_DEV, len);
    cmd[i++] = STLINK_DEBUG_COMMAND;
    cmd[i++] = STLINK_DEBUG_WRITEMEM_32BIT;
    write_uint32(&cmd[i], addr);
    write_uint16(&cmd[i + 4], len);
    ret = send_only(slu, 0, cmd, slu->cmd_len,"WRITEMEM_32BIT",__LINE__);
    if (ret == -1)
        return ret;

    ret = send_only(slu, 1, buf, len,"DATA",__LINE__);
    if (ret == -1)
        return ret;

    return 0;
}

int _stlink_usb_current_mode(stlink_t * sl) {
    struct stlink_libusb * const slu = sl->backend_data;
    uchar* const cmd  = sl->c_buf;
    uchar* const data = sl->q_buf;
    ssize_t size;
    int rep_len = 2;
    int i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);

    cmd[i++] = STLINK_GET_CURRENT_MODE;
    size = send_recv(slu, 1, cmd,  slu->cmd_len, data, rep_len,CMD_CHECK_NO,"GET_CURRENT_MODE",__LINE__);
    if (size == -1) {
        printf("[!] send_recv STLINK_GET_CURRENT_MODE\n");
        return -1;
    }
    return sl->q_buf[0];
}

int _stlink_usb_core_id(stlink_t * sl) {
    struct stlink_libusb * const slu = sl->backend_data;
    unsigned char* const cmd  = sl->c_buf;
    unsigned char* const data = sl->q_buf;
    ssize_t size;
    int offset, rep_len = sl->version.jtag_api == STLINK_JTAG_API_V1 ? 4 : 12;
    int i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);

    cmd[i++] = STLINK_DEBUG_COMMAND;

    if (sl->version.jtag_api == STLINK_JTAG_API_V1) {
        cmd[i++] = STLINK_DEBUG_READCOREID;
        offset = 0;
    } else {
        cmd[i++] = STLINK_DEBUG_APIV2_READ_IDCODES;
        offset = 4;
    }

    size = send_recv(slu, 1, cmd, slu->cmd_len, data, rep_len, CMD_CHECK_STATUS, "READ_IDCODES",__LINE__);

    if (size < 0) {
        return(-1);
    }

    sl->core_id = read_uint32(data, offset);
#ifdef D
    if (slu->log & 1)
       printf("COREID IDCODES off %d %x\n",offset,sl->core_id);
#endif

    return(0);
}

int _stlink_usb_status(stlink_t * sl) {
    struct stlink_libusb * const slu = sl->backend_data;
    uchar* const data = sl->q_buf;
    uchar* const cmd  = sl->c_buf;
    ssize_t size;
    int rep_len = 2;
    int i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);

    cmd[i++] = STLINK_DEBUG_COMMAND;
    cmd[i++] = STLINK_DEBUG_GETSTATUS;

    size = send_recv(slu, 1, cmd, slu->cmd_len, data, rep_len,CMD_CHECK_NO,"GETSTATUS",__LINE__);
//  printf("_stlink_usb_status size %d\n",size);
    if (size == -1) {
        printf("[!] send_recv STLINK_DEBUG_GETSTATUS\n");
        return (int) size;
    }
    sl->q_len = (int) size;

    return 0;
}

int _stlink_usb_force_debug(stlink_t *sl) {
    struct stlink_libusb *slu = sl->backend_data;

    int res;

    if (sl->version.jtag_api != STLINK_JTAG_API_V1) {
        res = _stlink_usb_write_debug32(sl, STLINK_REG_DHCSR, STLINK_REG_DHCSR_DBGKEY | STLINK_REG_DHCSR_C_HALT | STLINK_REG_DHCSR_C_DEBUGEN);
        return(res);
    }

    unsigned char* const data = sl->q_buf;
    unsigned char* const cmd  = sl->c_buf;
    ssize_t size;
    int rep_len = 2;
    int i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);

    cmd[i++] = STLINK_DEBUG_COMMAND;
    cmd[i++] = STLINK_DEBUG_FORCEDEBUG;
    size = send_recv(slu, 1, cmd, slu->cmd_len, data, rep_len, CMD_CHECK_RETRY, "FORCEDEBUG",__LINE__);

    return(size<0?-1:0);
}

int _stlink_usb_enter_swd_mode(stlink_t * sl) {
    struct stlink_libusb * const slu = sl->backend_data;
    unsigned char* const cmd  = sl->c_buf;
    ssize_t size;
    unsigned char* const data = sl->q_buf;
    const uint32_t rep_len = sl->version.jtag_api == STLINK_JTAG_API_V1 ? 0 : 2;
    int i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);

    cmd[i++] = STLINK_DEBUG_COMMAND;
    // select correct API-Version for entering SWD mode: V1 API (0x20) or V2 API (0x30).
    cmd[i++] = sl->version.jtag_api == STLINK_JTAG_API_V1 ? STLINK_DEBUG_APIV1_ENTER : STLINK_DEBUG_APIV2_ENTER;
    cmd[i++] = STLINK_DEBUG_ENTER_SWD;
    size = send_recv(slu, 1, cmd, slu->cmd_len, data, rep_len, CMD_CHECK_RETRY, "ENTER_SWD",__LINE__);

    return(size<0?-1:0);
}

int _stlink_usb_exit_dfu_mode(stlink_t* sl) {
    struct stlink_libusb * const slu = sl->backend_data;
    uchar* const cmd = sl->c_buf;
    ssize_t size;
    int i = fill_command(sl, SG_DXFER_FROM_DEV, 0);

    cmd[i++] = STLINK_DFU_COMMAND;
    cmd[i++] = STLINK_DFU_EXIT;

    size = send_only(slu, 1, cmd, slu->cmd_len,"DFU_EXIT",__LINE__);
    if (size == -1) {
        printf("[!] send_recv STLINK_DFU_EXIT\n");
        return (int) size;
    }

    return 0;
}

int _stlink_usb_exit_debug_mode(stlink_t *sl) {
    struct stlink_libusb * const slu = sl->backend_data;
    uchar* const cmd = sl->c_buf;
    ssize_t size;
    int i = fill_command(sl, SG_DXFER_FROM_DEV, 0);

    cmd[i++] = STLINK_DEBUG_COMMAND;
    cmd[i++] = STLINK_DEBUG_EXIT;

    size = send_only(slu, 1, cmd, slu->cmd_len,"DEBUG_EXIT",__LINE__);
    if (size == -1) {
        printf("[!] send_only STLINK_DEBUG_EXIT\n");
        return (int) size;
    }

    return 0;
}

int _stlink_usb_read_mem32(stlink_t *sl, uint addr, uchar *buf, ushort len) {
    struct stlink_libusb * const slu = sl->backend_data;
//  uchar* const data = sl->q_buf;
    uchar* const cmd = sl->c_buf;
    ssize_t size;
    int i = fill_command(sl, SG_DXFER_FROM_DEV, len);

#ifdef D
    if (slu->log & 1)
      printf("_stlink_usb_read_mem32 add %08x len %d\n",addr,len);
#endif
    cmd[i++] = STLINK_DEBUG_COMMAND;
    cmd[i++] = STLINK_DEBUG_READMEM_32BIT;
    write_uint32(&cmd[i], addr);
    write_uint16(&cmd[i + 4], len);

    size = send_recv(slu, 1, cmd, slu->cmd_len, buf, len,CMD_CHECK_NO,"READMEM_32BIT",__LINE__);
    if (size == -1) {
        printf("[!] send_recv STLINK_DEBUG_READMEM_32BIT\n");
        return (int) size;
    }

#ifdef D1
    if (slu->log & 1)
      dumpmem(data,len,0);
#endif
    sl->q_len = (int) size;

    stlink_print_data(sl);
    return 0;
}

int _stlink_usb_read_all_regs(stlink_t *sl, struct stlink_reg *regp) {
    struct stlink_libusb * const slu = sl->backend_data;
    unsigned char* const cmd = sl->c_buf;
    unsigned char* const data = sl->q_buf;
    ssize_t size;
    uint32_t rep_len = sl->version.jtag_api == STLINK_JTAG_API_V1 ? 84 : 88;
    int i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);

    cmd[i++] = STLINK_DEBUG_COMMAND;

    if (sl->version.jtag_api == STLINK_JTAG_API_V1) {
        cmd[i++] = STLINK_DEBUG_APIV1_READALLREGS;
    } else {
        cmd[i++] = STLINK_DEBUG_APIV2_READALLREGS;
    }

    size = send_recv(slu, 1, cmd, slu->cmd_len, data, rep_len, CMD_CHECK_STATUS, "READALLREGS",__LINE__);

    if (size < 0) {
        return(-1);
    }

    /* V1: regs data from offset 0 */
    /* V2: status at offset 0, regs data from offset 4 */
    int reg_offset = sl->version.jtag_api == STLINK_JTAG_API_V1 ? 0 : 4;
    sl->q_len = (int)size;
    stlink_print_data(sl);

    for (i = 0; i < 16; i++) regp->r[i] = read_uint32(sl->q_buf, reg_offset + i * 4);

#ifdef D
    if (slu->log & 1)
    {
      printf("READALLREGS off %d size %ld\n",reg_offset,size);
      for (i = 0; i < 16; i++) printf("R%02d %08x\n",i,regp->r[i]);
    }
#endif
    regp->xpsr       = read_uint32(sl->q_buf, reg_offset + 64);
    regp->main_sp    = read_uint32(sl->q_buf, reg_offset + 68);
    regp->process_sp = read_uint32(sl->q_buf, reg_offset + 72);
    regp->rw         = read_uint32(sl->q_buf, reg_offset + 76);
    regp->rw2        = read_uint32(sl->q_buf, reg_offset + 80);

    if (sl->verbose < 2) { return(0); }

    DLOG(sl,"xpsr       = 0x%08x\n", regp->xpsr);
    DLOG(sl,"main_sp    = 0x%08x\n", regp->main_sp);
    DLOG(sl,"process_sp = 0x%08x\n", regp->process_sp);
    DLOG(sl,"rw         = 0x%08x\n", regp->rw);
    DLOG(sl,"rw2        = 0x%08x\n", regp->rw2);

    return(0);
}

int _stlink_usb_read_reg(stlink_t *sl, int r_idx, struct stlink_reg *regp) {
    struct stlink_libusb * const slu = sl->backend_data;
    unsigned char* const data = sl->q_buf;
    unsigned char* const cmd  = sl->c_buf;
    ssize_t size;
    uint32_t r;
    uint32_t rep_len = sl->version.jtag_api == STLINK_JTAG_API_V1 ? 4 : 8;
    int reg_offset = sl->version.jtag_api == STLINK_JTAG_API_V1 ? 0 : 4;
    int i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);

    cmd[i++] = STLINK_DEBUG_COMMAND;

    if (sl->version.jtag_api == STLINK_JTAG_API_V1) {
        cmd[i++] = STLINK_DEBUG_APIV1_READREG;
    } else {
        cmd[i++] = STLINK_DEBUG_APIV2_READREG;
    }

    cmd[i++] = (uint8_t)r_idx;
    size = send_recv(slu, 1, cmd, slu->cmd_len, data, rep_len, CMD_CHECK_RETRY, "READREG",__LINE__);
#ifdef D
    if (slu->log & 1)
      printf("_stlink_usb_read_reg r_idx %d size %ld\n",r_idx,size);
#endif

    if (size < 0) {
        return(-1);
    }

    sl->q_len = (int)size;
    stlink_print_data(sl);
    r = read_uint32(sl->q_buf, reg_offset);
#ifdef D
    if (slu->log & 1)
      printf("r_idx (%2d) = 0x%08x\n", r_idx, r);
#endif

    switch (r_idx) {
    case 16:
        regp->xpsr = r;
        break;
    case 17:
        regp->main_sp = r;
        break;
    case 18:
        regp->process_sp = r;
        break;
    case 19:
        regp->rw = r; // XXX ?(primask, basemask etc.)
        break;
    case 20:
        regp->rw2 = r; // XXX ?(primask, basemask etc.)
        break;
    default:
        regp->r[r_idx] = r;
    }

    return(0);
}

/* See section C1.6 of the ARMv7-M Architecture Reference Manual */
// values:
//   0..12   R00 .. R12
//  13       R13    SP
//  14       R14    LR
//  15       Debug return address
//  16       xPSR
//  17       MSP
//  18       PSP
//  20       31:24 CONTROL 23:16 FAULTMASK 15:8 BASEPRI 7:0 PRIMASK
//  33       FPCSR
//  64..05   S0 .. S31

int _stlink_usb_read_unsupported_reg(stlink_t *sl, int r_idx, struct stlink_reg *regp) 
{
  uint r;
  int ret;

  sl->q_buf[0] = (uchar) r_idx;
  for (int i = 1; i < 4; i++) 
    sl->q_buf[i] = 0;

  ret = _stlink_usb_write_mem32(sl, DCRSR_ADD, sl->q_buf,4);
  if (ret == -1)
    return ret;

  _stlink_usb_read_mem32(sl, DCRDR_ADD, sl->q_buf, 4);
  if (ret == -1)
    return ret;

  r = read_uint32(sl->q_buf, 0);
  DLOG(sl,"r_idx (%2d) = 0x%08x\n", r_idx, r);

  switch (r_idx) 
  {
    case DCRSR_REGSEL_CONTROL:
      regp->primask = (uint8_t) (r & 0xFF);
      regp->basepri = (uint8_t) ((r>>8) & 0xFF);
      regp->faultmask = (uint8_t) ((r>>16) & 0xFF);
      regp->control = (uint8_t) ((r>>24) & 0xFF);
      break;
    case DCRSR_REGSEL_FPCSR:
      regp->fpscr = r;
      break;
    default:
      if (r_idx >= DCRSR_REGSEL_S0)
      {
        // floating point regs
        regp->s[r_idx - DCRSR_REGSEL_S0] = r;
      }
      break;
  }

  return 0;
}

int _stlink_usb_read_all_unsupported_regs(stlink_t *sl, struct stlink_reg *regp) 
{
  int ret;

  ret = _stlink_usb_read_unsupported_reg(sl, DCRSR_REGSEL_CONTROL, regp);
  if (ret == -1)
    return ret;

  ret = _stlink_usb_read_unsupported_reg(sl, DCRSR_REGSEL_FPCSR, regp);
  if (ret == -1)
    return ret;

  for (int i = 0; i < 32; i++) 
  {
    ret = _stlink_usb_read_unsupported_reg(sl, DCRSR_REGSEL_S0 + i, regp);
    if (ret == -1)
      return ret;
  }

  return 0;
}

int _stlink_usb_write_reg(stlink_t *sl, uint reg, int idx) {
    struct stlink_libusb * const slu = sl->backend_data;
    uchar* const data = sl->q_buf;
    uchar* const cmd  = sl->c_buf;
    ssize_t size;
    uint rep_len = 2;
    int i = fill_command(sl, SG_DXFER_FROM_DEV, rep_len);

    cmd[i++] = STLINK_DEBUG_COMMAND;
    cmd[i++] = STLINK_DEBUG_WRITEREG;
    cmd[i++] = idx;
    write_uint32(&cmd[i], reg);
    size = send_recv(slu, 1, cmd, slu->cmd_len, data, rep_len, CMD_CHECK_RETRY,"WRITEREG",__LINE__);
    if (size == -1) {
        printf("[!] send_recv STLINK_DEBUG_WRITEREG\n");
        return (int) size;
    }
sl->q_len = (int) size;
    stlink_print_data(sl);

    return 0;
}

void ugly_init(int v)
{
}

int ugly_libusb_log_level(enum ugly_loglevel v) {
#ifdef __FreeBSD__
  // FreeBSD includes its own reimplementation of libusb.
  // Its libusb_set_debug() function expects a lib_debug_level
  // instead of a lib_log_level and is verbose enough to drown out
  // all other output.
  switch (v) {
  case UDEBUG:
    return (3); // LIBUSB_DEBUG_FUNCTION + LIBUSB_DEBUG_TRANSFER
  case UINFO:
    return (1); // LIBUSB_DEBUG_FUNCTION only
  case UWARN:
    return (0); // LIBUSB_DEBUG_NO
  case UERROR:
    return (0); // LIBUSB_DEBUG_NO
  }
  return (0);
#else
  switch (v) {
  case UDEBUG:
    return (4);
  case UINFO:
    return (3);
  case UWARN:
    return (2);
  case UERROR:
  case UFATAL:
    return (1);
  }
  return (2);
#endif
}

size_t stlink_serial(struct libusb_device_handle *handle, struct libusb_device_descriptor *desc, char *serial) {
        unsigned char desc_serial[(STLINK_SERIAL_LENGTH) * 2];

        /* truncate the string in the serial buffer */
        serial[0] = '\0';

        /* get the LANGID from String Descriptor Zero */
        int ret = libusb_get_string_descriptor(handle, 0, 0, desc_serial, sizeof(desc_serial));
        if (ret < 4) return 0;

        uint32_t langid = desc_serial[2] | (desc_serial[3] << 8);

        /* get the serial */
        ret = libusb_get_string_descriptor(handle, desc->iSerialNumber, langid, desc_serial,
                sizeof(desc_serial));
        if (ret < 0) return 0; // could not read serial

        unsigned char len = desc_serial[0];

        if (len == ((STLINK_SERIAL_LENGTH + 1) * 2)) { /* len == 50 */
                /* good ST-Link adapter */
                ret = libusb_get_string_descriptor_ascii(
                        handle, desc->iSerialNumber, (unsigned char *)serial, STLINK_SERIAL_BUFFER_SIZE);
                if (ret < 0) return 0;
        } else if (len == ((STLINK_SERIAL_LENGTH / 2 + 1) * 2)) { /* len == 26 */
                /* fix-up the buggy serial */
                for (uint i = 0; i < STLINK_SERIAL_LENGTH; i += 2)
                        sprintf(serial + i, "%02X", desc_serial[i + 2]);
                serial[STLINK_SERIAL_LENGTH] = '\0';
        } else {
                return 0;
        }

        return strlen(serial);
}

stlink_t *open_usbv2(enum ugly_loglevel verbose, 
                     enum connect_type connect, 
                     char serial[STLINK_SERIAL_BUFFER_SIZE], 
                     int freq,
                     int log) 
{
    stlink_t* sl = NULL;
    struct stlink_libusb* slu = NULL;
    int ret = -1;
    int config;

#ifdef D
    if (log & 1)
      printf("open_usbv2 verbose %d connect %d serial %s freq %d log %d\n",verbose,connect,serial,freq,log);
#endif

    sl = calloc(1, sizeof(stlink_t));
    if (sl == NULL) { goto on_malloc_error; }

    slu = calloc(1, sizeof(struct stlink_libusb));
    if (slu == NULL) { goto on_malloc_error; }

    slu->sl = sl;
    ugly_init(verbose);
//  sl->backend = &_stlink_usb_backend;
    sl->backend_data = slu;
    slu->log = log;

    sl->core_stat = TARGET_UNKNOWN;

    ret = libusb_init(&(slu->libusb_ctx));
#ifdef D
    if (log & 1)
      printf("libusb_init -> %d\n",ret);
#endif
    if (ret) {
        WLOG("failed to init libusb context, wrong version of libraries?\n");
        goto on_error;
    }

#if LIBUSB_API_VERSION < 0x01000106
    libusb_set_debug(slu->libusb_ctx, ugly_libusb_log_level(verbose));
#ifdef D
    if (log & 1)
      printf("libusb_set_debug\n");
#endif
#else
    libusb_set_option(slu->libusb_ctx, LIBUSB_OPTION_LOG_LEVEL, ugly_libusb_log_level(verbose));
#ifdef D
    if (log & 1)
      printf("libusb_set_option\n");
#endif
#endif
    libusb_device **list = NULL;
    // TODO: We should use ssize_t and use it as a counter if > 0.
    // As per libusb API: ssize_t libusb_get_device_list (libusb_context *ctx, libusb_device ***list)
    int cnt = (int)libusb_get_device_list(slu->libusb_ctx, &list);
    struct libusb_device_descriptor desc;
    int devBus  = 0;
    int devAddr = 0;

    // TODO: Reading a environment variable in a usb open function is not very nice, this should
    // be refactored and moved into the CLI tools, and instead of giving USB_BUS:USB_ADDR a real
    // stlink serial string should be passed to this function. Probably people are using this
    // but this is very odd because as programmer can change to multiple busses and it is better
    // to detect them based on serial.
    char *device = getenv("STLINK_DEVICE");

    if (device) {
        char *c = strchr(device, ':');

        if (c == NULL) {
            WLOG("STLINK_DEVICE must be <USB_BUS>:<USB_ADDR> format\n");
            goto on_error;
        }

        devBus = atoi(device);
        *c++ = 0;
        devAddr = atoi(c);
        ILOG("bus %03d dev %03d\n", devBus, devAddr);
    }

    while (cnt-- > 0) {
        struct libusb_device_handle *handle;

        libusb_get_device_descriptor(list[cnt], &desc);

        if (desc.idVendor != STLINK_USB_VID_ST) { continue; }

        if (devBus && devAddr) {
            if ((libusb_get_bus_number(list[cnt]) != devBus) ||
                (libusb_get_device_address(list[cnt]) != devAddr)) {
                continue;
            }
        }

        ret = libusb_open(list[cnt], &handle);

        if (ret) { continue; } // could not open device

        size_t serial_len = stlink_serial(handle, &desc, sl->serial);

        libusb_close(handle);

        if (serial_len != STLINK_SERIAL_LENGTH) { continue; } // could not read the serial

        // if no serial provided, or if serial match device, fixup version and protocol
        if (((serial == NULL) || (*serial == 0)) || 
             (memcmp(serial, &sl->serial, STLINK_SERIAL_LENGTH) == 0)) 
        {
            if (STLINK_V1_USB_PID(desc.idProduct)) {
                slu->protocoll = 1;
                sl->version.stlink_v = 1;
            } else if (STLINK_V2_USB_PID(desc.idProduct) || STLINK_V2_1_USB_PID(desc.idProduct)) {
                sl->version.stlink_v = 2;
            } else if (STLINK_V3_USB_PID(desc.idProduct)) {
                sl->version.stlink_v = 3;
            }

            break;
        }

    }

    if (cnt < 0) {
        WLOG ("Couldn't find %s ST-Link devices\n", (devBus && devAddr) ? "matched" : "any");
        libusb_free_device_list(list, 1);
        goto on_error;
    } else {
        ret = libusb_open(list[cnt], &slu->usb_handle);
#ifdef D
        if (log & 1)
          printf("libusb_open -> %d\n", ret);
#endif

        if (ret != 0) {
            WLOG("Error %d (%s) opening ST-Link v%d device %03d:%03d\n", ret,
                 strerror(errno),
                 sl->version.stlink_v,
                 libusb_get_bus_number(list[cnt]),
                 libusb_get_device_address(list[cnt]));
            libusb_free_device_list(list, 1);
            goto on_error;
        }
    }

    libusb_free_device_list(list, 1);
  
    ret = libusb_kernel_driver_active(slu->usb_handle, 0);
#ifdef D
    if (log & 1)
      printf("libusb_kernel_driver_active -> %d\n",ret);
#endif
    if (ret == 1) {
        ret = libusb_detach_kernel_driver(slu->usb_handle, 0);

        if (ret < 0) {
            WLOG("libusb_detach_kernel_driver(() error %s\n", strerror(-ret));
            goto on_libusb_error;
        }
    }

    ret = libusb_get_configuration(slu->usb_handle, &config);
#ifdef D
    if (log & 1)
      printf("libusb_get_configuration -> %d config %d\n",ret,config);
#endif
    if (ret) {
        // this may fail for a previous configured device
        WLOG("libusb_get_configuration()\n");
        goto on_libusb_error;
    }

    if (config != 1) {
        printf("setting new configuration (%d -> 1)\n", config);

        if (libusb_set_configuration(slu->usb_handle, 1)) {
            // this may fail for a previous configured device
            WLOG("libusb_set_configuration() failed\n");
            goto on_libusb_error;
        }
    }

    ret = libusb_claim_interface(slu->usb_handle, 0);
#ifdef D
    if (log & 1)
      printf("libusb_claim_interface -> %d\n",ret);
#endif
    if (libusb_claim_interface(slu->usb_handle, 0)) {
        WLOG("Stlink usb device found, but unable to claim (probably already in use?)\n");
        goto on_libusb_error;
    }

    // TODO: Could use the scanning technique from STM8 code here...
    slu->ep_rep = 1 /* ep rep */ | LIBUSB_ENDPOINT_IN;

    if(desc.idProduct == STLINK_USB_PID_STLINK_NUCLEO ||
        desc.idProduct == STLINK_USB_PID_STLINK_32L_AUDIO ||
        desc.idProduct == STLINK_USB_PID_STLINK_V2_1 ||
        desc.idProduct == STLINK_USB_PID_STLINK_V3_USBLOADER ||
        desc.idProduct == STLINK_USB_PID_STLINK_V3E_PID ||
        desc.idProduct == STLINK_USB_PID_STLINK_V3S_PID ||
        desc.idProduct == STLINK_USB_PID_STLINK_V3_2VCP_PID ||
        desc.idProduct == STLINK_USB_PID_STLINK_V3_NO_MSD_PID ||
        desc.idProduct == STLINK_USB_PID_STLINK_V3P) {
        slu->ep_req = 1 /* ep req */ | LIBUSB_ENDPOINT_OUT;
        slu->ep_trace = 2 | LIBUSB_ENDPOINT_IN;
    } else {
        slu->ep_req = 2 /* ep req */ | LIBUSB_ENDPOINT_OUT;
        slu->ep_trace = 3 | LIBUSB_ENDPOINT_IN;
    }


    slu->sg_transfer_idx = 0;
    slu->cmd_len = (slu->protocoll == 1) ? STLINK_SG_SIZE : STLINK_CMD_SIZE;

    // initialize stlink version (sl->version)
    stlink_version(sl);

    int mode = stlink_current_mode(sl);
    logmsg("open_usbv2: mode %d (STLINK_DEV_DFU_MODE %d)",mode,STLINK_DEV_DFU_MODE);
    if (mode == STLINK_DEV_DFU_MODE) {
        DLOG(sl,"-- exit_dfu_mode\n");
        _stlink_usb_exit_dfu_mode(sl);
    }

    if (connect == CONNECT_UNDER_RESET) {
        // for the connect under reset only
        // OpenOСD says (official documentation is not available) that
        // the NRST pin must be pull down before selecting the SWD/JTAG mode
        printf("mode %d STLINK_DEV_DEBUG_MODE %d\n",mode,STLINK_DEV_DEBUG_MODE);
        if (mode == STLINK_DEV_DEBUG_MODE) {
            DLOG(sl,"-- exit_debug_mode\n");
            _stlink_usb_exit_dfu_mode(sl);
        }

        _stlink_usb_jtag_reset(sl, STLINK_DEBUG_APIV2_DRIVE_NRST_LOW);
    }

    sl->freq = freq;
    // set the speed before entering the mode as the chip discovery phase
    // should be done at this speed too
    // set the stlink clock speed (default is 1800kHz)
    DLOG(sl,"JTAG/SWD freq set to %d\n", freq);
    _stlink_usb_set_swdclk(sl, freq);

    stlink_target_connect(sl, connect);
#ifdef D
    if (log & 1)
      printf("open_usbv2 end\n");
#endif
    return(sl);

on_libusb_error:
    stlink_close(sl);
    return(NULL);

on_error:
    if (slu->libusb_ctx) { libusb_exit(slu->libusb_ctx); }

on_malloc_error:
    if (sl != NULL) { free(sl); }
    if (slu != NULL) { free(slu); }

    return(NULL);
}

stlink_t *open_usb(int verbose, bool reset, char serial[16],int log,int vid,int pid,char *devname)
{
  stlink_t* sl = NULL;
  struct stlink_libusb* slu = NULL;
  int ret = -1;
  int config;

//printf("stlink_open_usb log %d\n",log);
#ifdef D
  if (log & 2)
    printf("stlink_open_usb reset %d serial %s\n",reset,serial);
#endif
  sl  = calloc(1, sizeof (stlink_t));
  slu = calloc(1, sizeof (struct stlink_libusb));
  if (sl == NULL)
    goto on_malloc_error;
  if (slu == NULL)
    goto on_malloc_error;

  sl->backend_data = slu;
  slu->log = log;

  sl->core_stat = STLINK_CORE_STAT_UNKNOWN;
  if (libusb_init(&(slu->libusb_ctx))) 
  {
    WLOG("failed to init libusb context, wrong version of libraries?\n");
    goto on_error;
  }

  libusb_device **list;
  /** @todo We should use ssize_t and use it as a counter if > 0. 
       As per libusb API: ssize_t libusb_get_device_list (libusb_context *ctx, libusb_device ***list) */
  int cnt = (int) libusb_get_device_list(slu->libusb_ctx, &list);
#ifdef D
  if (slu->log & 2)
    printf("found %d device\n",cnt);
#endif
  struct libusb_device_descriptor desc;
  int devBus  = 0;
  int devAddr = 0;

    /* @TODO: Reading a environment variable in a usb open function is not very nice, this
      should be refactored and moved into the CLI tools, and instead of giving USB_BUS:USB_ADDR a real stlink
      serial string should be passed to this function. Probably people are using this but this is very odd because
      as programmer can change to multiple busses and it is better to detect them based on serial.  */
  char *device = getenv("STLINK_DEVICE");
  if (device) 
  {
    char *c = strchr(device,':');
    if (c==NULL) {
      WLOG("STLINK_DEVICE must be <USB_BUS>:<USB_ADDR> format\n");
      goto on_error;
    }
    devBus=atoi(device);
    *c++=0;
    devAddr=atoi(c);
#ifdef D
    if (slu->log & 2)
      printf("STLINK_DEVICE %s bus %d dev %d\n",device,devBus,devAddr);
#endif
    ILOG("bus %03d dev %03d\n",devBus, devAddr);
  }

  while (cnt--) 
  {
    libusb_get_device_descriptor( list[cnt], &desc );
#ifdef D
    if (slu->log & 2)
      printf("idVendor %04x idproduct %04x\n",desc.idVendor,desc.idProduct);
#endif
    if (desc.idVendor != vid)
      continue;

    if (devBus && devAddr) 
    {
      if ((libusb_get_bus_number(list[cnt]) != devBus)
          || (libusb_get_device_address(list[cnt]) != devAddr)) 
        continue;
    }

    if ((desc.idProduct == pid)) // || (desc.idProduct == STLINK_USB_PID_STLINK_NUCLEO)) 
    {
      struct libusb_device_handle *handle;

      ret = libusb_open(list[cnt], &handle);
      if (ret)
        continue;

      sl->serial_size = libusb_get_string_descriptor_ascii(handle, desc.iSerialNumber,
                                                           (uchar *)sl->serial, sizeof(sl->serial));
      libusb_close(handle);
#ifdef D
      if (slu->log & 2)
        printf("serial %s\n",sl->serial);
#endif

      if ((serial == NULL) || (*serial == 0))
        break;

      if (sl->serial_size < 0)
        continue;

      if (memcmp(serial, &sl->serial, sl->serial_size) == 0)
        break;

      continue;
    }

    if (desc.idProduct == STLINK_USB_PID_STLINK) 
    {
      slu->protocoll = 1;
#ifdef D
      if (slu->log & 2)
        printf("idProduct %x protocoll 1\n",desc.idProduct);
#endif
      break;
    }
  }

  if (cnt < 0) 
  {
    WLOG ("Couldn't find %s %s devices\n",(devBus && devAddr)?"matched":"any",devname);
    goto on_error;
  } else 
  {
    ret = libusb_open(list[cnt], &slu->usb_handle);
    if (ret != 0) 
    {
      WLOG("Error %d (%s) opening ST-Link/V2 device %03d:%03d\n",
           ret, strerror (errno), libusb_get_bus_number(list[cnt]), libusb_get_device_address(list[cnt]));
      goto on_error;
    }
  }

  libusb_free_device_list(list, 1);

  int r;
  if ((r = libusb_kernel_driver_active(slu->usb_handle, 0)) == 1) 
  {
    ret = libusb_detach_kernel_driver(slu->usb_handle, 0);
#ifdef D
    if (slu->log & 2)
      printf("libusb_detach_kernel_driver -> %d\n",ret);
#endif
    if (ret < 0) 
    {
      WLOG("libusb_detach_kernel_driver(() error %s\n", strerror(-ret));
      goto on_libusb_error;
    }
  }
#ifdef D 
  if (slu->log & 2)
    printf("libusb_kernel_driver_active -> %d\n",r);
#endif

  if (libusb_get_configuration(slu->usb_handle, &config)) 
  {
    /* this may fail for a previous configured device */
    WLOG("libusb_get_configuration()\n");
    goto on_libusb_error;
  }
#ifdef D
  if (slu->log & 2)
    printf("config %d\n",config);
#endif

  if (config != 1) 
  {
    printf("setting new configuration (%d -> 1)\n", config);
    if ((r = libusb_set_configuration(slu->usb_handle, 1)) )
    {
      /* this may fail for a previous configured device */
      WLOG("libusb_set_configuration() failed err %d\n",r);
      goto on_libusb_error;
    }
  }

  if ((r = libusb_claim_interface(slu->usb_handle, 0))) 
  {
    WLOG("Stlink usb device found, but unable to claim (probably already in use?) err %d\n",r);
    goto on_libusb_error;
  }

    // TODO - could use the scanning techniq from stm8 code here...
  slu->ep_rep = 1 /* ep rep */ | LIBUSB_ENDPOINT_IN;
  if (desc.idProduct == STLINK_USB_PID_STLINK_NUCLEO) 
    slu->ep_req = 1 /* ep req */ | LIBUSB_ENDPOINT_OUT;
  else 
    slu->ep_req = 2 /* ep req */ | LIBUSB_ENDPOINT_OUT;
#ifdef D
  if (slu->log & 2)
    printf("idProduct %x (%x) ep_req %d\n",desc.idProduct,STLINK_USB_PID_STLINK_NUCLEO,slu->ep_req);
#endif

  slu->sg_transfer_idx = 0;
  // TODO - never used at the moment, always CMD_SIZE
  slu->cmd_len = (slu->protocoll == 1)? STLINK_SG_SIZE: STLINK_CMD_SIZE;

  if (stlink_current_mode(sl) == STLINK_DEV_DFU_MODE) 
  {
    ILOG("-- exit_dfu_mode\n");
    stlink_exit_dfu_mode(sl);
  }

  if (stlink_current_mode(sl) != STLINK_DEV_DEBUG_MODE) 
    stlink_enter_swd_mode(sl);

  if (reset) 
  {
    stlink_reset(sl,RESET_AUTO); // ??
    usleep(10000);
  }

  stlink_version(sl);
  ret = stlink_load_device_params(sl);

on_libusb_error:
  if (ret == -1) 
  {
    stlink_close(sl);
    return NULL;
  }

  return sl;

on_error:
  if (slu->libusb_ctx)
    libusb_exit(slu->libusb_ctx);

on_malloc_error:
  if (sl != NULL)
    free(sl);
  if (slu != NULL)
    free(slu);

  return NULL;
}

stlink_t *stlink_open_usbv2(int verbose, enum connect_type c, char serial[STLINK_SERIAL_BUFFER_SIZE],int f,int log)
{
#ifdef D
  if (log & 1)
    printf("stlink_open_usbv2 verbose %d connect %d serial %s f %d log %d\n",verbose,c,serial,f,log);
#endif
  stlink_t *r = open_usbv2(verbose,c,serial,f,log);
#ifdef D
  if (log & 1) 
    printf("stlink_open_usbv2 -> %p\n",r);
#endif  
  return r;
}

stlink_t *stlink_open_usb(int verbose, bool reset, char serial[16],int log)
{
  return open_usb(verbose,reset,serial,log,STLINK_USB_VID_ST,STLINK_USB_PID_STLINK_32L,"STLink/V2");
}

stlink_t *pico_open_usb(int verbose, bool reset, char serial[16],int log)
{
  return open_usb(verbose,reset,serial,log, 0x2e8a, 0x0004,"picoprobe");
}

int stlink_target_connect(stlink_t *sl, enum connect_type connect) 
{
//#ifdef D
  logmsg("stlink_target_connect %d",connect);
//#endif
  if (connect == CONNECT_UNDER_RESET) {
    stlink_enter_swd_mode(sl);

    stlink_jtag_reset(sl, STLINK_DEBUG_APIV2_DRIVE_NRST_LOW);

    // try to halt the core before reset
    // this is useful if the NRST pin is not connected
    _stlink_usb_force_debug(sl);

    // minimum reset pulse duration of 20 us (RM0008, 8.1.2 Power reset)
    usleep(20);

    stlink_jtag_reset(sl, STLINK_DEBUG_APIV2_DRIVE_NRST_HIGH);

    // try to halt the core after reset
    unsigned timeout = time_ms() + 10;
    while (time_ms() < timeout) {
      _stlink_usb_force_debug(sl);
      usleep(100);
    }

    // check NRST connection
    uint32_t dhcsr = 0;
    stlink_read_debug32(sl, STLINK_REG_DHCSR, &dhcsr);
    if ((dhcsr & STLINK_REG_DHCSR_S_RESET_ST) == 0) {
      logmsg("stlink_target_connect NRST is not connected");
    }

    // addition soft reset for halt before the first instruction
    stlink_soft_reset(sl, 1 /* halt on reset */,STLINK_REG_CM3_DEMCR_TRCENA | STLINK_REG_CM3_DEMCR_VC_HARDERR |
            STLINK_REG_CM3_DEMCR_VC_BUSERR | STLINK_REG_CM3_DEMCR_VC_CORERESET);
  }
  if (stlink_current_mode(sl) != STLINK_DEV_DEBUG_MODE &&
        stlink_enter_swd_mode(sl)) {
    printf("Failed to enter SWD mode\n");
    return -1;
  }

  if (connect == CONNECT_NORMAL) {
    logmsg("stlink_target_connect: CONNECT_NORMAL");
    stlink_reset(sl, RESET_AUTO);
  }

  int r = stlink_load_device_params(sl);
#ifdef D
  logmsg("stlink_target_connect return %d",r);
#endif
  return r;
}

int st_dumpmem(stlink_t* sl,uint add, ushort size)
{
  uint i;
  uint *buf;
  buf = malloc(size);
  if (!buf)
    perror("no memory");

  int err = stlink_read_mem32(sl, add,(uchar *) buf, size);
  if (err == 0)
  {
    for (i = 0; i < size / 4; i++)
      printf("%08x: %08x %12d\n",add + i * 4,buf[i],buf[i]);
  } else
  {
    printf("error from stlink_read_mem32 %d\n",err);
    return 1;
  }
  free(buf); 
  return 0;
}

#define PIN 14

int test103(stlink_t* sl)
{
  int i;
// enable GPIOB clock
  uint apb2enr;

  int err = stlink_read_mem32(sl, F1XX_RCC_BASE + F1XX_RCC_APB2ENR_OFF,(uchar *) &apb2enr,sizeof apb2enr);
  CHKERR(err,"stlink_read_mem32");
  printf("abp2enr %08x\n",apb2enr);
  if (!(apb2enr & 8)) // IOPBEN
  {
    apb2enr |= 8;
    err = stlink_write_mem32(sl, F1XX_RCC_BASE + F1XX_RCC_APB2ENR_OFF,(uchar *) &apb2enr,sizeof apb2enr);
    if (err)
    {  
      printf("stlink_write_mem32 %d\n",err);
      return err;
    }
  }
//  set GPIOB.PIN ad output (push pull 20mhz)
  uint crh = 0;
  if (PIN >= 8)
  {
    for (i = 8; i < 16; i++)
    {
      if (i == PIN)
        crh |= 1 << ((i - 8) * 4);
      else
        crh |= 4 << ((i - 8) * 4);
    }
  }
      
  printf("CRH %08x\n",crh);
  err = stlink_write_mem32(sl, F1XX_GPIOB_BASE + F1XX_GPIO_CRH_OFF,(uchar *) &crh,sizeof crh);
  CHKERR(err,"stlink_write_mem32");

// blink pin PORTB.PIN 
  for (i = 0; i < 10; i++)
  {
    uint bsrr;
    bsrr = 1 << PIN;
    printf("%d\n",i);
    err = stlink_write_mem32(sl, F1XX_GPIOB_BASE + F1XX_GPIO_BSRR_OFF,(uchar *) &bsrr,sizeof bsrr);
    CHKERR(err,"stlink_write_mem32");
    usleep(50000);
    bsrr = 1 << (16 + PIN);
    err = stlink_write_mem32(sl, F1XX_GPIOB_BASE + F1XX_GPIO_BSRR_OFF,(uchar *) &bsrr,sizeof bsrr);
    CHKERR(err,"stlink_write_mem32");
    usleep(50000);
  }
  printf("GPIOB\n");
  st_dumpmem(sl,F1XX_GPIOB_BASE,F1XX_GPIO_SIZE);
  
  return 0; 
}

#define MAXBUF 4096 

int writeprog(stlink_t *sl,uchar *prog,uint loadadd,int size)
{
  int s,off;
  off = 0;
  int err = 0;
  for (s = size;s > 0; s -= MAXBUF)
  {
    int s0 = s > MAXBUF ? MAXBUF : s;
    err = stlink_write_mem32(sl, loadadd + off, prog + off, s0);
    CHKERR(err,"stlink_write_mem32");
    off += MAXBUF;
  }
//printf("writeprog err %d\n",err);
  return err;
}

int checkprog(stlink_t *sl,uchar *prog,uchar *bufr,uint loadadd,int size)
{
  int s,off;
  off = 0;
  int nerr = 0;
  int err = 0;
  for (s = size;s > 0; s -= MAXBUF)
  {
    int s0 = s > MAXBUF ? MAXBUF : s;
    err = stlink_read_mem32(sl, loadadd + off, bufr + off, s0);
    CHKERR(err,"stlink_read_mem32");
    off += MAXBUF;
  }
  uint *p1 = (uint *) prog;
  uint *p2 = (uint *) bufr;
  for (int i = 0; i < size / 4; i++)
    if (p1[i] != p2[i])
    {
      nerr++;
      if (nerr < 8)
        printf("%08x: %08x %08x\n",(uint) (loadadd + i * 4),p1[i],p2[i]);
    }
  if (nerr > 0)
  {
    printf("check memory error off %x nerr %d\n",off,nerr);
    FILE * pf = fopen("mem.bin","wb");
    if (pf)
    {
      int n = fwrite(bufr,1,size,pf);
      printf("write %d bytes to mem.bin\n",n);
      fclose(pf);
    } else
      printf("failed to open mem.bin\n");
  }
  if (memcmp(prog,bufr,size) != 0)
  {
    printf("check memory error\n");
    nerr++;
  }
  if (nerr > 0) err = 1;
  if (nerr > 0)
    logmsg("verify prog nerr %d err %d\n",nerr,err);
  return err;
}

int progelf(uint off,int len,FILE *pf,int fl,void *arg)
{
//char *add;
  int  r;
  stlink_t* sl;

  sl = (stlink_t *) arg;
  logmsg("progelf fl %d %08x-%08x %6d",fl,off,off + len - 1,len);
  if (!sl) 
    return len;
  if (fl != 1) return len;
  uchar *bufw = malloc(len);
  if (!bufw) perror("no memory");
  uchar *bufr = malloc(len);
  if (!bufr) perror("no memory");
  r = fread(bufw,1,len,pf);
  if (r != len)
  {
    printf("fread r %d len %d ",r,len);
    perror("fprogelf: fread");
  }
  r = writeprog(sl,bufw,off,len);
  CHKERR(r,"writeprog");
  r = checkprog(sl,bufw,bufr,off,len);
  CHKERR(r,"checkprog");
  free(bufw);
  free(bufr);

  return len;
}

// set/clear hw breakpoint #n

int breakpoint(stlink_t* sl,int n,int add)
{
  uint val;
//static uint break_num,lit_num,break_rev;
//uint mask;
  uint type;
  uint fpb_addr;

  if (code_break_num == 0)
    init_code_breakpoints(sl);
  printf("HW breakpoint %d add %08x\n",n,add);
#ifdef OLD
  if (break_num == 0)
  {
    stlink_write_debug32(sl, STLINK_REG_CM3_FP_CTRL, 0x03 /* KEY | ENABLE */);
    stlink_read_debug32(sl, STLINK_REG_CM3_FP_CTRL, &val);
    break_num = ((val >> 4) & 0xf);
    lit_num = ((val >> 8) & 0xf);
    break_rev = ((val >> 28) & 0xf);
    printf("HW break %d lit %d rev %d\n",break_num,lit_num,break_rev);
  }
#endif
  if (code_break_rev == CODE_BREAK_REV_V1) 
  {
    type = (add & 0x2) ? CODE_BREAK_HIGH : CODE_BREAK_LOW;
    fpb_addr = add & 0x1ffffffc;
  } else
  {
    type = CODE_BREAK_REMAP;
    fpb_addr = add;
  }

  val = add;
  if (val)
     val = (type << 30) | (fpb_addr) | 1;
  printf("write %08x: %08x\n",STLINK_REG_CM3_FP_COMPn(n),val);
  stlink_write_debug32(sl, STLINK_REG_CM3_FP_COMPn(n), val);

  return 0;
}

int fillmem(stlink_t* sl,uint pattern,uint start,uint size)
{
  int err;
  uint buf[MAXBUF / sizeof(int)];

  printf("fillmem pattern %08x start %08x len %d (%xx) end %08x\n",
         pattern,start,size,size,start + size);

  for (int i = 0; i < MAXBUF / sizeof(int); i++)
    buf[i] = pattern;

  while (size > 0)
  {
    int chunk = MAXBUF;
    if (size < chunk) chunk = size;
    err = stlink_write_mem32(sl, start,(uchar *) buf,chunk);
    CHKERR(err,"stlink_write_mem32");
    size -= chunk;
    start += chunk;
  }
  return 0;
}

int loadprg(stlink_t* sl,const char *prg,uint loadadd,uint inipc,uint inisp,int chkmem)
{
  struct stat st;
  struct self elf;
  struct sdeb deb;

  memset(&elf,0,sizeof elf);
  memset(&deb,0,sizeof deb);
  initfdata();

  logmsg("loadprg %s loadadd %08x pc %08x sp %08x",prg,loadadd,inipc,inisp);
  int err = stat(prg,&st);
  uint pc,sp;

  if (err != 0)
  {
    printf("can't stat: %s\n",prg);
    return 1; 
  }
//printf("prog file %s size %zd\n",prg,st.st_size);
  
  elf.log = 0;
  elf.fprog = progelf;
/*
  elf.fsym = fsym;
*/
  elf.arg = sl;
  FILE *pf = fopen(prg,"r");
  if (pf)
  {
    int er = readelf(&elf,pf);
//  printf("readelf -> %d\n",er);
    fclose(pf);
    if (sl && er == 1)
    {
      if (inipc == 0 || inipc == -1)
        inipc = elf.entry & ~1;
      logmsg("set PC %08x SP %08x",inipc,inisp);
      err = stlink_write_reg(sl,inipc,15);
      CHKERR(err,"stlink_write_reg PC");
      err = stlink_write_reg(sl,inisp,13);
      CHKERR(err,"stlink_write_reg SP");
    }
    if (er == 1)
      return 0;
  }
// try to load bynary file
  uchar *bufw = malloc(st.st_size);
  if (!bufw) perror("no memory");
  uchar *bufr = malloc(st.st_size);
  if (!bufr) perror("no memory");
  int fd = open(prg,O_RDONLY);
  if (fd < 0)
  {
    printf("can't open %s\n",prg);
    return 1;
  }
  int n = read(fd,bufw,st.st_size);
  if (inisp == 0xffffffff)
  {
    pc = ((int *)  bufw)[1] & ~1;
    sp = *((int *) bufw); 
  } else
  {
    pc = inipc;
    sp = inisp;
  }
  logmsg("binary file read %d bytes reset PC %08xr, SP %08x",n,pc,sp);

  close(fd);

  if (sl)
  {
    err = writeprog(sl,bufw,loadadd,n);
    if (err == 0 && chkmem)
      err = checkprog(sl,bufw,bufr,loadadd,n);
    if (err == 0)
    {
      err = stlink_write_reg(sl,pc,15);
      CHKERR(err,"stlink_write_reg PC");
      err = stlink_write_reg(sl,sp,13);
      CHKERR(err,"stlink_write_reg SP");
    }
  }
  free(bufr);
  free(bufw);
  return err;
}

static struct code_hw_breakpoint code_breaks[CODE_BREAK_NUM_MAX];

void init_code_breakpoints(stlink_t *sl) {
    uint val;
    memset(sl->q_buf, 0, 4);
#ifdef D
    logmsg("init_code_breakpoints: init_code_breakpoints write STLINK_REG_CM3_FP_CTRL 3");
#endif
    stlink_write_debug32(sl, STLINK_REG_CM3_FP_CTRL, 0x03 /* KEY | ENABLE */);
    stlink_read_debug32(sl, STLINK_REG_CM3_FP_CTRL, &val);
    code_break_num = ((val >> 4) & 0xf);
    code_lit_num = ((val >> 8) & 0xf);
    code_break_rev = ((val >> 28) & 0xf);

    logmsg("Found %i hw breakpoint registers", code_break_num);

    stlink_read_debug32(sl, STLINK_REG_CM3_CPUID, &val);
#ifdef D
    logmsg("init_code_breakpoints cpuid %x %x",val,((val>>4) & 0xFFF));
#endif
    if (((val>>4) & 0xFFF) == 0xc27) {
        logmsg("write STLINK_REG_CM7_FP_LAR %08x %08x\n",STLINK_REG_CM7_FP_LAR,STLINK_REG_CM7_FP_LAR_KEY);
        // Cortex-M7 can have locked to write FP_* registers
        // IHI0029D, p. 48, Lock Access Register
        stlink_write_debug32(sl, STLINK_REG_CM7_FP_LAR, STLINK_REG_CM7_FP_LAR_KEY);
    }

    for (int i = 0; i < code_break_num; i++) {
        code_breaks[i].type = 0;
        stlink_write_debug32(sl, STLINK_REG_CM3_FP_COMPn(i), 0);
    }
}
#define DATA_WATCH_NUM 4

enum watchfun { WATCHDISABLED = 0, WATCHREAD = 5, WATCHWRITE = 6, WATCHACCESS = 7 };

struct code_hw_watchpoint {
    stm32_addr_t addr;
    uint8_t mask;
    enum watchfun fun;
};

static struct code_hw_watchpoint data_watches[DATA_WATCH_NUM];

void init_data_watchpoints(stlink_t *sl) {
    uint32_t data;
    DLOG(sl,"init watchpoints\n");

    // set TRCENA in debug command to turn on DWT unit
#ifdef D
    printf("DBG: init_data_watchpoints calls stlink_read_debug32 STLINK_REG_CM3_DEMCR\n");
#endif
    stlink_read_debug32(sl, STLINK_REG_CM3_DEMCR, &data);
    data |= STLINK_REG_CM3_DEMCR_TRCENA;
    stlink_write_debug32(sl, STLINK_REG_CM3_DEMCR, data);

    // make sure all watchpoints are cleared
    for (int i = 0; i < DATA_WATCH_NUM; i++) {
        data_watches[i].fun = WATCHDISABLED;
        stlink_write_debug32(sl, STLINK_REG_CM3_DWT_FUNn(i), 0);
    }
}

static struct cache_desc_t cache_desc;

// return the smallest R so that V <= (1 << R); not performance critical
static unsigned ceil_log2(unsigned v) {
    unsigned res;

    for (res = 0; (1U << res) < v; res++);

    return(res);
}

static void read_cache_level_desc(stlink_t *sl, struct cache_level_desc *desc) {
    uint ccsidr;
    uint log2_nsets;

    stlink_read_debug32(sl, STLINK_REG_CM7_CCSIDR, &ccsidr);
#ifdef D
    printf("DBG: read_cache_level_desc read STLINK_REG_CM7_CCSIDR %x\n",ccsidr);
#endif
    desc->nsets = ((ccsidr >> 13) & 0x3fff) + 1;
    desc->nways = ((ccsidr >> 3) & 0x1ff) + 1;
    desc->log2_nways = ceil_log2 (desc->nways);
    log2_nsets = ceil_log2 (desc->nsets);
    desc->width = 4 + (ccsidr & 7) + log2_nsets;
    ILOG("%08x LineSize: %u, ways: %u, sets: %u (width: %u)\n",
         ccsidr, 4 << (ccsidr & 7), desc->nways, desc->nsets, desc->width);
}

void init_cache (stlink_t *sl) {
    uint clidr;
    uint ccr;
    uint ctr;
    int i;

    // Check have cache
    stlink_read_debug32(sl, STLINK_REG_CM7_CTR, &ctr);
#ifdef D
    printf("DBG: init_cache read STLINK_REG_CM7_CTR %x\n",ctr);
#endif
    if ((ctr >> 29) != 0x04) {
        cache_desc.used = 0;
        return;
    } else
        cache_desc.used = 1;
    cache_desc.dminline = 4 << ((ctr >> 16) & 0x0f);
    cache_desc.iminline = 4 << (ctr & 0x0f);

    stlink_read_debug32(sl, STLINK_REG_CM7_CLIDR, &clidr);
    cache_desc.louu = (clidr >> 27) & 7;

    stlink_read_debug32(sl, STLINK_REG_CM7_CCR, &ccr);
    ILOG("Chip clidr: %08x, I-Cache: %s, D-Cache: %s\n",
         clidr, ccr & STLINK_REG_CM7_CCR_IC ? "on" : "off", ccr & STLINK_REG_CM7_CCR_DC ? "on" : "off");
    ILOG(" cache: LoUU: %u, LoC: %u, LoUIS: %u\n",
         (clidr >> 27) & 7, (clidr >> 24) & 7, (clidr >> 21) & 7);
    ILOG(" cache: ctr: %08x, DminLine: %u bytes, IminLine: %u bytes\n", ctr,
         cache_desc.dminline, cache_desc.iminline);
    for (i = 0; i < 7; i++) {
        uint ct = (clidr >> (3 * i)) & 0x07;
        cache_desc.dcache[i].width = 0;
        cache_desc.icache[i].width = 0;

        if (ct == 2 || ct == 3 || ct == 4) { // data
            stlink_write_debug32(sl, STLINK_REG_CM7_CSSELR, i << 1);
            ILOG("D-Cache L%d: ", i);
            read_cache_level_desc(sl, &cache_desc.dcache[i]);
        }

        if (ct == 1 || ct == 3) { // instruction
            stlink_write_debug32(sl, STLINK_REG_CM7_CSSELR, (i << 1) | 1);
            ILOG("I-Cache L%d: ", i);
            read_cache_level_desc(sl, &cache_desc.icache[i]);
        }
    }
}

int dumpreg(stlink_t* sl,int flag,const char *desc)
{
  int    j,err;
  char   *ident = "";
  char   buf[42];
  struct stlink_reg regp;

  err = stlink_read_all_regs(sl, &regp);
  CHKERR(err,"stlink_read_all_regs");
  err = stlink_read_all_unsupported_regs(sl, &regp);
  CHKERR(err,"stlink_read_all_unsupported_regs");
  if (desc)
    printf("dumpreg: %s\n",desc);
  for (j = 0; j < 16; j++)
    printf("R%02d %08x %12d %s\n",j,
           regp.r[j],
           regp.r[j],
           tobin(regp.r[j],32,buf));
  printf("xpsr %08x main_sp %08x process_sp %08x rw %08x rw2 %08x\n",
         regp.xpsr,regp.main_sp,regp.process_sp,regp.rw,regp.rw2);
  printf("control %02x faultmask %02x basepri %02x primask %02x fpscr %02x\n",
         regp.control,regp.faultmask,regp.basepri,regp.primask,regp.fpscr);
  if (flag)
  {
    printf("%sfpscr      %08x\n",ident,regp.fpscr);
    for (j = 0; j < 32; j++)
    {
      float *pf;
      pf  = (float *) &regp.s[j];
      printf("%sS%02d %08x %12f\n",
                   ident,j,
                   regp.s[j],*pf);

    }
  }
  return 0;
}

int force_deb(stlink_t* sl,int flag)
{
  logmsg("force_deb enter");
  int st = stlink_force_debug(sl);
  CHKERR(st,"stlink_force_debug");
// with halt_on_reset the cpu break on some fault
// see  Debug Exception and Monitor Control Register, DEMCR
//  st = stlink_soft_reset(sl, 1);
// gdb don't' set it (default mode)
  if (flag)
  {
    st = stlink_soft_reset(sl, 1,STLINK_REG_CM3_DEMCR_TRCENA | 
                                 STLINK_REG_CM3_DEMCR_VC_HARDERR |
                                 STLINK_REG_CM3_DEMCR_VC_BUSERR | 
                                 STLINK_REG_CM3_DEMCR_VC_CORERESET);
    CHKERR(st,"stlink_soft_reset");
  }
/*
  else
    st = stlink_soft_reset(sl, 1,STLINK_REG_CM3_DEMCR_TRCENA  |
                           STLINK_REG_CM3_DEMCR_VC_CORERESET);
*/

  init_code_breakpoints(sl);
  init_data_watchpoints(sl);

  init_cache(sl);
  uint cpuid;
  int err = stlink_read_debug32(sl, CPUID_ADD, &cpuid);
  CHKERR(err,"stlink_read_mem32");
  logmsg("connect cpuid %x",cpuid);
  if (sl->chip_id == STM32_CHIPID_F4)
    checkf4xx(sl);
  printf("force_deb exit\n");
  return 0;
}

// connect and soft reset the target
// flag 1: cpu break on fault
// flag 0: cpu handle fault
// deb  1: force debug

int connect(stlink_t* sl,uint flag,uint deb)
{
  logmsg("connect flag %d debug %d",flag,deb);
  int st = stlink_target_connect(sl, flag);
//CHKERR(st,"stlink_target_connect");

  if (deb)
  {
    st = force_deb(sl,flag);
    CHKERR(st,"force_deb");
  }
  return 0;
}

static int dis_read_mem(struct disasm_par *par,uint pc,uchar *code,size_t len)
{
  return stlink_read_mem32(par->env, pc & ~3 ,code, len);
}

//
// single step nstep instruction
// flag:  1  read cpu registers
//        2  read cpu float regs
//        4  don't mask interrupt
//
int trace(stlink_t* sl,uint nstep,uint flag,uint tracestart)
{
  int  i,j,err;
  char *ident;
//struct t_cpu  cpu  = {0},*p_cpu;
//struct t_dec  /* dec  = {0}, */ *pd;
  struct t_stat stat = {0},*p_st;
//static int init = 0;

//p_cpu = &cpu;
  p_st  = &stat;
//pd    = &dec;
  struct t_cpu *p_cpu = sl->p_cpu;

  sl->p_cpu->logflags = 1;
  logmsg("trace nstep %u flag %x tracestart %d",nstep,flag,tracestart);
//sl->p_cpu->logd = 1;
  sl->p_cpu->execcount = 10;
/*
  if (init == 0)
  {
    initdec(p_cpu);
    initm();
    initcondt();
    init = 1;
  }
*/
  ident = "";
  dumpreg(sl,flag & 2,"trace init");
  err = stlink_read_all_regs(sl, &p_cpu->regp0);
  CHKERR(err,"stlink_read_all_regs");
  err = stlink_read_all_unsupported_regs(sl, &p_cpu->regp0);
  CHKERR(err,"stlink_read_all_unsupported_regs");
 
  ident = "                                    ";
  for (i = 0; i < nstep; i++)
  {
//  printf("%d/%d\n",i,nstep);
    p_cpu->N = (p_cpu->regp0.xpsr >> 31) & 1;
    p_cpu->Z = (p_cpu->regp0.xpsr >> 30) & 1;
    p_cpu->C = (p_cpu->regp0.xpsr >> 29) & 1;
    p_cpu->V = (p_cpu->regp0.xpsr >> 28) & 1;
    p_cpu->IT = ((p_cpu->regp0.xpsr >> 25) & 3) | (((p_cpu->regp0.xpsr >> 10) & 0x3f) << 2);
    p_cpu->ISR = p_cpu->regp0.xpsr & 0x1ff;
    printf("%8d ",i + tracestart);
    struct disasm_par dp = {sl,dis_read_mem};
    disasm(&dp,p_cpu,p_cpu->regp0.r[15],p_st);
    putchar('\n');

    err = stlink_step(sl,(flag & 4) != 0);
    CHKERR(err,"stlink_step");
    if (flag & 3)
    {
      if (flag & 1)
      {
        err = stlink_read_all_regs(sl, &p_cpu->regp);
        CHKERR(err,"stlink_read_all_regs");
      } 
      if (flag & 2)
      {
        err = stlink_read_all_unsupported_regs(sl, &p_cpu->regp);
        CHKERR(err,"stlink_read_all_regs");
      } 
      
      if (p_cpu->regp.xpsr != p_cpu->regp0.xpsr)
      {
        int N,Z,C,V,ISR,IT;

        N = (p_cpu->regp.xpsr >> 31) & 1;
        Z = (p_cpu->regp.xpsr >> 30) & 1;
        C = (p_cpu->regp.xpsr >> 29) & 1;
        V = (p_cpu->regp.xpsr >> 28) & 1;
        IT = ((p_cpu->regp.xpsr >> 25) & 3) | (((p_cpu->regp.xpsr >> 10) & 0x3f) << 2);
        ISR = p_cpu->regp.xpsr & 0x1ff;
 
        printf("%s",ident);
        if (p_cpu->N != N ||
            p_cpu->Z != Z ||
            p_cpu->C != C ||
            p_cpu->V != V)
        {   
          printf("setflags ");
          if (p_cpu->N)   printf("N");
          else            printf("-");
          if (p_cpu->Z)   printf("Z");
          else            printf("-");
          if (p_cpu->C)   printf("C");
          else            printf("-");
          if (p_cpu->V)   printf("V");
          else            printf("-");
          if (p_cpu->ISR) printf("I");
          else            printf("-");

          printf("  -> ");

          if (N)          printf("N");
          else            printf("-");
          if (Z)          printf("Z");
          else            printf("-");
          if (C)          printf("C");
          else            printf("-");
          if (V)          printf("V");
          else            printf("-");
          if (p_cpu->ISR) printf("I");
          else            printf("-");
          printf(" ");
        } else if (IT != p_cpu->IT)
          printf("IT %x -> %x ",p_cpu->IT,IT);
        else if (ISR != p_cpu->ISR)
          printf("ISR %x -> %x ",p_cpu->ISR,ISR);
        printf("%08x\n", p_cpu->regp.xpsr);
      }
      if (p_cpu->regp.main_sp != p_cpu->regp0.main_sp && p_cpu->regp.main_sp != p_cpu->regp.r[SP])
        printf("%smain_sp    %08x -> %08x\n",ident,p_cpu->regp0.main_sp,p_cpu->regp.main_sp);
      if (p_cpu->regp.process_sp != p_cpu->regp0.process_sp && p_cpu->regp.process_sp != p_cpu->regp.r[SP])
        printf("%ssetpsp     %08x -> %08x\n",ident,p_cpu->regp0.process_sp,p_cpu->regp.process_sp);
      if (p_cpu->regp.rw != p_cpu->regp0.rw && p_cpu->regp.rw != p_cpu->regp.r[SP])
        printf("%srw         %08x -> %08x\n",ident,p_cpu->regp0.rw,p_cpu->regp.rw);
      if (p_cpu->regp.rw2 != p_cpu->regp0.rw2 && p_cpu->regp.rw2 != p_cpu->regp.r[SP])
        printf("%ssetprimask %d -> %d\n",ident,p_cpu->regp0.rw2,p_cpu->regp.rw2);
      if (p_cpu->regp.control != p_cpu->regp0.control)
        printf("%scontrol    %08x -> %08x\n",ident,p_cpu->regp0.control,p_cpu->regp.control);
      if (p_cpu->regp.faultmask != p_cpu->regp0.faultmask)
        printf("%sfaultmask  %08x -> %08x\n",ident,p_cpu->regp0.faultmask,p_cpu->regp.faultmask);
      if (p_cpu->regp.basepri != p_cpu->regp0.basepri)
        printf("%sbasepri    %08x -> %08x\n",ident,p_cpu->regp0.basepri,p_cpu->regp.basepri);
      if (p_cpu->regp.primask != p_cpu->regp0.primask)
        printf("%ssetprimask %d -> %d\n",ident,p_cpu->regp0.primask,p_cpu->regp.primask);
      for (j = 0; j < 15; j++) // general register without PC
      {
        if (p_cpu->regp.r[j] != p_cpu->regp0.r[j])
          printf("%sreg        W       R%02d: %08x -> %08x\n",
                 ident,j, p_cpu->regp0.r[j], p_cpu->regp.r[j]);
      }
      if (flag & 2)
      {
        for (j = 0; j < 32; j++) // flating point 
        {
          float *pf0,*pf;

          pf0 = (float *) &p_cpu->regp0.s[j];
          pf  = (float *) &p_cpu->regp.s[j];
          if (p_cpu->regp0.s[j] != p_cpu->regp.s[j])
            printf("%sfpreg      W       S%02d: %08x %12e -> %08x %12e\n",
                   ident,j, 
                   p_cpu->regp0.s[j],*pf0, 
                   p_cpu->regp.s[j],*pf);
        }
      }
    } else
    { // read only PC
      err = stlink_read_reg(sl, 15, &p_cpu->regp);
      CHKERR(err,"stlink_read_all_regs");
    }
    p_cpu->regp0 = p_cpu->regp;
  }
//dumpreg(sl,flag & 2,"trace end");
  return 0;
}

int contcpu(stlink_t *sl)
{
  int err = stlink_run(sl, RUN_NORMAL);
  CHKERR(err,"stlink_run");
  return 0;
}

int runcpu(stlink_t *sl,uint wtime,uint count,uint *mem,int flag)
{
  int  err,ret;
  uint dhcsr;
  uint dwt[20];
  struct stlink_reg regp = {0};

  logmsg("runcpu wtime %d count %d",wtime,count);

  err = stlink_run(sl, RUN_NORMAL);
  CHKERR(err,"stlink_run");
  dhcsr = readdhcsr(sl,1,__LINE__);
  int n = 0;
  int nerr = 0;
  while((dhcsr & 3) != 3 && n < count)
  {
    if (wtime > 0)
      usleep(wtime);
    int st = stlink_status(sl);
    CHKERR(st,"stlink_status");
//  printf("stlink_status %d %d\n",st,sl->core_stat);
    ret = stlink_read_debug32(sl, STLINK_REG_DHCSR, &dhcsr);
    if (ret != 0)
    {
      dhcsr = 0;
      nerr++;
      printf("nerr %d\n",nerr);
      if (nerr > 2)
        break;
    }
//  readdwt(sl,1,n == 0,dwt);
    readdwt(sl,0,n == 0,dwt);
    if (mem)
      mem[n] = dwt[7];
// registers can't be read in running cpu
    if (flag)
    {
      err = stlink_force_debug(sl);
      CHKERR(err,"force_deb");
      err = stlink_read_all_regs(sl, &regp);
      CHKERR(err,"stlink_read_all_regs");
      for (int i = 0; i < 16; i++)
      {
        if (i && i % 4 == 0) putchar('\n');
        printf("R%02d %08x ",i,regp.r[i]);
      }
      putchar('\n');
      printf("xpsr %08x main_sp %08x process_sp %08x rw %08x rw2 %08x\n",
             regp.xpsr,regp.main_sp,regp.process_sp,regp.rw,regp.rw2);
      printf("control %02x faultmask %02x basepri %02x primask %02x fpscr %02x\n",
             regp.control,regp.faultmask,regp.basepri,regp.primask,regp.fpscr);
      printf("\n");
      err = stlink_run(sl, RUN_NORMAL);
      CHKERR(err,"stlink_run");
    }
    n++;
  }

  logmsg("runcpu final dhcsr %08x n %d",dhcsr,n);
  if ((dhcsr & 3) == 3)
    checkfault(sl);
  return 0;
}

int setdwt(stlink_t* sl,int val)
{
  printf("setdwt %08x\n",val);
  int err = stlink_write_mem32(sl, DWT_CTRL_ADD /* 0xe0001000 */, (uchar *) &val, sizeof(val));
  CHKERR(err,"stlink_write_mem32");
  return 0;
}

int readdwt(stlink_t* sl,int fl,int head,uint *pdwt)
{
  static uint dwt[8] = {0};
  if (!pdwt)
    pdwt = dwt;
  static uint last;
  printf("readdwt fl %d head %d\n",fl,head);
  int err = stlink_read_mem32(sl, DWT_CTRL_ADD /*0xe0001000 */ ,(uchar *) pdwt, sizeof dwt);
  CHKERR(err,"stlink_read_mem32");
  if (err == 0)
  {
    int v = pdwt[0] | 1 | (1 << 21) | (1 << 20) | (1 << 19) | (1 << 18) | (1 << 17);
    if (head && (pdwt[0] & v) != v)
    {
      setdwt(sl,v);
      int err = stlink_read_mem32(sl, DWT_CTRL_ADD /* 0xe0001000 */,(uchar *) pdwt, sizeof dwt);
      CHKERR(err,"stlink_read_mem32");
      if ((pdwt[0] & 1) == 0)
        logmsg("can't set dwt");
      logmsg("new dwt %08x",pdwt[0]);
    }
    double dt = pdwt[1];
    dt -= last;
    if (pdwt[1] < last)
      dt += 4294967296.;
    if (fl == 1)
    {
      if (head)
      {
        printf("dt         DWT_CTRL CYCCNT     CPI EXC SLEEP LSU FOLD PC\n");
        printf("---------- -------- ---------- --- --- ---   --- ---  --------\n");
      }
      printf("%10.0f %08x %10u %3d %3d %3d   %3d %3d  %08x\n",
             dt,pdwt[0],pdwt[1],pdwt[2],pdwt[3],pdwt[4],pdwt[5],pdwt[6],pdwt[7]);
    } else if (fl == 2)
      printf("%8.0f DWT CTRL %08x CYCNT %08x CPICNT %02x EXCCNT %02x SLEEPCNT %x LSUCNT %x FOLDCNT %x PCSR %x\n",
             dt,pdwt[0],pdwt[1],pdwt[2],pdwt[3],pdwt[4],pdwt[5],pdwt[6],pdwt[7]);
    last = pdwt[1];
  }
  return 0;
}

int checkfault(stlink_t* sl)
{
  uint scb[256/4];
  int ret = 0;
  printf("checkfault\n");
//uint dhcsr = readdhcsr(sl,1,__LINE__);
  dumpreg(sl,0,"fault");
  ret = stlink_read_mem32(sl, SCB_BASE,(uchar * ) scb, sizeof scb);
  CHKERR(ret,"sstlink_read_mem32");
  for (int i = 0; scb_table[i].name; i++)
  {
    printf("%08x: %-8s %08x ",
           SCB_BASE + scb_table[i].off,
           scb_table[i].name,
           scb[scb_table[i].off / 4]);
    if (scb_table[i].out)
      scb_table[i].out(sl,scb[scb_table[i].off / 4]);
    else
      putchar('\n');
  }
  return 0;
}

#define CH(pc) ((*pc) >= ' ' && (*pc) < 127 ? (*pc) : '.')

void dumpmem32(uint *pm,int size,uint off)
{
//uint prev;
  char *pc;
  char buf[32 + 8 + 1];
  if (size > 256) 
    size = 256;
  pc = (char *) pm;
//prev = pm[0] + 1;
  int j = -1;
  for (int i = 0; i < size; i++,pc += 4)
  {
    if (1) // pm[i] != prev)
    {
      if (i - j > 1)
        printf("........: skip %d words\n",i - j - 1); 
      printf("%08x: %08x %12d '%c%c%c%c' %s\n",
             off + i * 4, pm[i], pm[i],
             CH(pc),CH(pc+1),CH(pc+2),CH(pc+3),tobin(pm[i],32,buf));
//    prev = pm[i];
      j = i;
    }
  }
}

int exec_read(stlink_t* sl,int add,int size,const char *fn)
{
  uint mem[1024];
  int err;
  FILE *pf = 0;

  printf("exec_read add %08x len %d fn %s\n",add,size,fn);
  
  if (fn)
  {
    pf = fopen(fn,"wb");
    if (!pf)
    {
      printf("can't open %s\n",fn);
      return(0);
    }
  } else if (size > 1024)
    size = 1024;
  int nr = 0;
  int off = 0;
  while (nr < size)
  {
    int l = sizeof mem;
    if (l > size - nr) l = size -nr;
    err = stlink_read_mem32(sl, add + off,(uchar *) mem, l);
    CHKERR(err,"stlink_read_mem32");
/*
    for (int i = 0; i < 16; i++)
      printf("%2d: %08x\n",i,mem[i]);
*/
    if (pf)
    {
      int n = fwrite(mem,1,l,pf);
      printf("fwrite -> %d\n",n);
    } else
      dumpmem32(mem,l/4,add + off);
    nr += l;
    off += l;
  }
  if (pf)
    fclose(pf);

  return 0;
}

