#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
// see note on libusb-1.0
//#include <libusb-1.0/libusb.h>
#include "libusb.h"
#include "env.h"
#include "conf.h"
#include "armreg.h"
#include "dapreg.h"
#include "readpcapng-stlink.h"
#include "driver-cmsis.h"

#define REG(n,a,b,t,o,f,h) \
reg[n].name = #n; \
reg[n].add = a; \
reg[n].dpbanksel = b; \
reg[n].ty = t; \
reg[n].op = o; \
reg[n].fhook = h; \
reg[n].fdump = f;

#define REGM(n,a,f,h) \
reg[n].name = #n; \
reg[n].add = a; \
reg[n].dpbanksel = DAP_X; \
reg[n].ty = DAP_AP; \
reg[n].op = DAP_RW; \
reg[n].fdump = f; \
reg[n].fhook = h;

#define OUT_FIELD(reg,fn,fmt) \
{sprintf(p,"%s " #fmt " ",#fn, FIELD(v,reg,fn)); \
 p += strlen(p);}

#define OUT_FIELD_1(reg,fn,fmt) \
{if ((fl & OUT_FULL) || FIELD(v,reg,fn)) OUT_FIELD(reg,fn,fmt)}

#define OUT_FIELD_X(fn,off,mask,fmt) \
{sprintf(p,"%s " #fmt " ",#fn, (((v) & (mask)) >> (off))); p += strlen(p);}

#define OUT_FIELD1_X(fn,off,mask,fmt) \
{if ((fl & OUT_FULL) || (((v) & (mask)) >> (off))) OUT_FIELD_X(fn,off,mask,fmt)}

int dapregid(struct t_dap_reg *pr,int ap,int r,int a,int op)
{
  int rid = -1;

  if (!ap)
  {
    if (!r && a == 0)
      rid = DAP_DP_ABORT;
    else if (r && a == 0)
      rid = DAP_DP_DPIDR;
    else if (!r && a == 8)
      rid = DAP_DP_SELECT;
    else if (r && a == 0xc)
      rid = DAP_DP_RDBUFF;
    else if (a == 4)
    {
      int select = pr[DAP_DP_SELECT].vals;
      int dpbanksel = select & 0xf;
      switch(dpbanksel)
      {
        case 0:
          rid = DAP_DP_CTRL_STAT;
          break;
        case 1:
          rid = DAP_DP_DLCR;
          break;
        case 2:
          rid = DAP_DP_TARGETID;
          break;
        case 3:
          rid = DAP_DP_DLPIDR;
          break;
        case 4:
          rid = DAP_DP_EVENTSTAT;
          break;
      }
    }
  }
  return rid;
}

char *dump_dlpidr(struct t_dap_reg *pr,int fl,int op,int phase,struct buf_env *pbe)
{
  pbe->buf[0] = 0;
  char *p = pbe->buf;
  uint v = (phase & DAP_OP_MASK) == DAP_SEND ? pr->vals : pr->valr;

  OUT_FIELD_X(TINSTANCE, 28,  0xf << 28, %x);
  OUT_FIELD_X(PROTOVSN,   0,  0xf <<  0, %x);

  return pbe->buf;
}

char *dump_dpidr(struct t_dap_reg *pr,int fl,int op,int phase,struct buf_env *pbe)
{
  pbe->buf[0] = 0;
  char *p = pbe->buf;
  uint v = (phase & DAP_OP_MASK) == DAP_SEND ? pr->vals : pr->valr;

  OUT_FIELD_X(REV,    28,  0xf << 28, %d);
  OUT_FIELD_X(PARTNO, 20, 0xff << 20, %x);
  OUT_FIELD_X(MIN,    16,    1 << 16, %x);
  OUT_FIELD_X(VERSION,12,  0xf << 12, %d);
  OUT_FIELD_X(DES,     1,0xeff <<  1, %d);

  return pbe->buf;
}

char *dump_idr(struct t_dap_reg *pr,int fl,int op,int phase,struct buf_env *pbe)
{
  pbe->buf[0] = 0;
  char *p = pbe->buf;
  uint v = (phase & DAP_OP_MASK) == DAP_SEND ? pr->vals : pr->valr;

  OUT_FIELD_X(REV,      28,  0xf << 28, %d);
  OUT_FIELD_X(DESIGNER, 20,0x3ff << 20, %x);
  OUT_FIELD_X(CLASS,    13,  0xf << 13, %x);
  OUT_FIELD_X(VARIANT,   4,  0xf << 4, %d);
  OUT_FIELD_X(TYPE,      0,  0xf << 0, %d);

  return pbe->buf;
}

char *dump_ctrl_stat(struct t_dap_reg *pr,int fl,int op,int phase,struct buf_env *pbe)
{
  pbe->buf[0] = 0;
  char *p = pbe->buf;
  uint v = (phase & DAP_OP_MASK) == DAP_SEND ? pr->vals : pr->valr;

  OUT_FIELD_1(CTRL_STAT,ORUNDETECT,%d);
  OUT_FIELD_1(CTRL_STAT,STICKYORUN,%d);
  OUT_FIELD_1(CTRL_STAT,TRNMODE,%d);
  OUT_FIELD_1(CTRL_STAT,STICKYCMP,%d);
  OUT_FIELD_1(CTRL_STAT,STICKYERR,%d);
  OUT_FIELD_1(CTRL_STAT,READOK,%d);
  OUT_FIELD_1(CTRL_STAT,WDATAERR,%d);
  OUT_FIELD_1(CTRL_STAT,TRNCNT,%d);
  OUT_FIELD_1(CTRL_STAT,CDBGRSTREQ,%d);
  OUT_FIELD_1(CTRL_STAT,CDBGRSTACK,%d);
  OUT_FIELD_1(CTRL_STAT,CDBGPWRUPREQ,%d);
  OUT_FIELD_1(CTRL_STAT,CDBGPWRUPACK,%d);
  OUT_FIELD_1(CTRL_STAT,CSYSPWRUPREQ,%d);
  OUT_FIELD_1(CTRL_STAT,CSYSPWRUPACK,%d);

  return pbe->buf;
}

char *dump_csw(struct t_dap_reg *pr,int fl,int op,int phase,struct buf_env *pbe)
{
  pbe->buf[0] = 0;
  char *p = pbe->buf;
  uint v = pr->vals;

  uint size = FIELD(v,CSW,SIZE);
  if ((fl & OUT_FULL) || size) 
  {
    sprintf(p,"SIZE %d ",size);
    p += strlen(p);
    if (size)
    {
      sprintf(p,"(*%d) ",1 << size);
      p += strlen(p);
    }
  }
  OUT_FIELD_1(CSW,SIZE,%d);
  OUT_FIELD_1(CSW,ADDRINC,%d);
  OUT_FIELD_1(CSW,DEVICEEN,%d);
  OUT_FIELD_1(CSW,TRINPROG,%d);
  OUT_FIELD_1(CSW,MODE,%x);
  OUT_FIELD_1(CSW,TYPE,%x);
  OUT_FIELD_1(CSW,SPIDEN,%d);
  OUT_FIELD_1(CSW,CACHE,%x);
  OUT_FIELD_1(CSW,PROT,%x);
  OUT_FIELD_1(CSW,DBGSWENABLE,%d);

  return pbe->buf;
}

char *dump_select(struct t_dap_reg *pr,int fl,int op,int phase,struct buf_env *pbe)
{
  pbe->buf[0] = 0;
  char *p = pbe->buf;
  uint v = pr->vals;

  OUT_FIELD_X(APSEL,        24, 0xff << 24, %x);
  OUT_FIELD_X(APBANKSEL,     4,  0xf <<  4, %x);
  OUT_FIELD_X(DPBANKSEL,     0,  0xf <<  0, %x);

  return pbe->buf;
}

char *dump_abort(struct t_dap_reg *pr,int fl,int op,int phase,struct buf_env *pbe)
{
  pbe->buf[0] = 0;
  char *p = pbe->buf;
  uint v = (phase & DAP_OP_MASK) == DAP_SEND ? pr->vals : pr->valr;

  OUT_FIELD_X(ORUNERRCLR,    4,   1 <<  4, %d);
  OUT_FIELD_X(WDERRCLR,      3,   1 <<  3, %d);
  OUT_FIELD_X(STKERRCLR,     2,   1 <<  2, %d);
  OUT_FIELD_X(STKCMPCLR,     1,   1 <<  1, %d);
  OUT_FIELD_X(DAPABORT,      0,   1 <<  0, %d);

  return pbe->buf;
}

/*
char *dump_stat_ctrl(struct t_dap_reg *pr,int fl,int op,int phase,struct buf_env *pbe)
{
  pbe->buf[0] = 0;
  char *p = pbe->buf;
  uint v = (phase & DAP_OP_MASK) == DAP_SEND ? pr->vals : pr->valr;

  OUT_FIELD1_X(CSYSPWRUPACK,  31,     1 << 31, %d);
  OUT_FIELD1_X(CSYSPWRUPEQK,  30,     1 << 30, %d);
  OUT_FIELD1_X(CDBGPWRUPACK,  29,     1 << 29, %d);
  OUT_FIELD1_X(CDBGPWRUPEQK,  28,     1 << 28, %d);
  OUT_FIELD1_X(CDBGRSACK,     27,     1 << 27, %d);
  OUT_FIELD1_X(CDBGRSREQ,     26,     1 << 26, %d);
  OUT_FIELD1_X(TRNCNT,        12, 0xfff << 12, %d);
  OUT_FIELD1_X(MASKLANE,       8,   0xf <<  8, %x);
  OUT_FIELD1_X(WDATAERR,       7,     1 <<  7, %d);
  OUT_FIELD1_X(READOK,         6,     1 <<  6, %d);
  OUT_FIELD1_X(STICKYERR,      5,     1 <<  5, %d);
  OUT_FIELD1_X(STICKYCMP,      4,     1 <<  4, %d);
  OUT_FIELD1_X(TRNMODE,        2,     3 <<  2, %d);
  OUT_FIELD1_X(STICKORUN,      1,     1 <<  1, %d);
  OUT_FIELD1_X(ORUNDETECT,     0,     1 <<  0, %d);

  return pbe->buf;
}
*/

static int hook_select(struct t_dap *pdap,struct t_dap_reg *pr,uint v,int fl,int op,int phase,int where)
{
//int select = v;
/*
  int apsel     = (select & DAP_DP_SELECT_APSEL_MASK)     >> DAP_DP_SELECT_APSEL_SHIFT;
  int apbanksel = (select & DAP_DP_SELECT_APBANKSEL_MASK) >> DAP_DP_SELECT_APBANKSEL_SHIFT;
  int dpbanksel = (select & DAP_DP_SELECT_DPBANKSEL_MASK) >> DAP_DP_SELECT_DPBANKSEL_SHIFT;
 */ 
//int apsel = (select >> 24) & 0xf;
//int apbanksel = (select >> 4) & 0xf;
//int dpbanksel = select & 0xf;
//printf("[sel %x %d %x op %d ph %d] ",apsel,apbanksel,dpbanksel,op,phase);

  return 0;
}

static int hook_drw(struct t_dap *pdap,
                    struct t_dap_reg *pr,
                    uint v,int fl,int op,int phase,int where)
{
  uint csw = pdap->regs[DAP_MEMAP_CSW].vals;
  uint addrinc = FIELD(csw,CSW,ADDRINC);
  uint size = FIELD(csw,CSW,SIZE);

//printf("*** hook_drw *** fl %d ph %c where %d ",fl,"WR"[phase],where);
  if ((phase & DAP_OP_MASK) == DAP_SEND)
  {
    arm_setreg(pdap->regs[DAP_MEMAP_TAR].vals,v,WO,where,0);
    if (fl != OUT_NONE && (phase & DAP_DECODE))
    {
      printf("a(TAR -> %s)",arm_getregname(pdap->regs[DAP_MEMAP_TAR].vals,1));
      if (addrinc == 1)
      {
        printf("+=%d ",1 << size);
        pdap->regs[DAP_MEMAP_TAR].vals += (1 << size); // TODO better via a function
      } else if (addrinc == 2)
      {
        printf("+=%d PACK ",1 << size);
        pdap->regs[DAP_MEMAP_TAR].vals += (1 << size); // TODO better via a function
      } else if (addrinc)
        error("unexpected addrinc %d",addrinc);
    }
  } else if ((phase & DAP_OP_MASK) == DAP_RECEIVE)
  {
    arm_setreg(pdap->regs[DAP_MEMAP_TAR].vals,v,RO,where,0);
    if (fl != OUT_NONE && (phase & DAP_DECODE))
    {
      printf("b(TAR -> %s)",arm_getregname(pdap->regs[DAP_MEMAP_TAR].vals,1));
      if (addrinc == 1)
      {
        printf("+=%d ",1 << size);
        pdap->regs[DAP_MEMAP_TAR].vals += (1 << size); // TODO better via a function
      } else if (addrinc == 2)
      {
        printf("+=%d PACK ",1 << size);
        pdap->regs[DAP_MEMAP_TAR].vals += (1 << size); // TODO better via a function
      } else if (addrinc)
        error("unexpected addrinc %d",addrinc);
    }
  }

  return 0;
}

static int hook_bdn(struct t_dap *pdap,struct t_dap_reg *pr,uint v,int fl,int op,int phase,int where)
{
//printf("*** hook_bdn *** fl %d ty %c where %d ",fl,"WR"[op],where);
  if (fl != OUT_NONE && (phase & DAP_DECODE))
  {
    if (pr->add == DAP_MEMAP_BD0_ADD)
      printf("c(TAR -> %s)",arm_getregname(pdap->regs[DAP_MEMAP_TAR].vals,1));
    else
    {
      uint add = (pdap->regs[DAP_MEMAP_TAR].vals & ~0xf) + pr->add - DAP_MEMAP_BD0_ADD;
      printf("d(TAR[+%d] -> %s)",add - pdap->regs[DAP_MEMAP_TAR].vals,
             arm_getregname(add,1));
    }
  }
  return 0;
}

void initdapreg(struct t_dap_reg *reg)
{
  REG(DAP_DP_DPIDR,     DAP_DP_DPIDR_ADD,     DAP_X, DAP_DP, DAP_RO, dump_dpidr    , 0); 
  REG(DAP_DP_ABORT,     DAP_DP_ABORT_ADD,     DAP_X, DAP_DP, DAP_WO, dump_abort    , 0); 
  REG(DAP_DP_CTRL_STAT, DAP_DP_CTRL_STAT_ADD,     0, DAP_DP, DAP_RW, dump_ctrl_stat, 0); 
  REG(DAP_DP_DLCR,      DAP_DP_DLCR_ADD,          1, DAP_DP, DAP_RW, 0             , 0); 
  REG(DAP_DP_SELECT,    DAP_DP_SELECT_ADD,    DAP_X, DAP_DP, DAP_WO, dump_select   , hook_select); 
  REG(DAP_DP_RDBUFF,    DAP_DP_RDBUFF_ADD,    DAP_X, DAP_DP, DAP_RO, 0             , 0); 
  REG(DAP_DP_TARGETID,  DAP_DP_TARGETID_ADD,      2, DAP_DP, DAP_RO, 0             , 0); 
  REG(DAP_DP_TARGETSEL, DAP_DP_TARGETSEL_ADD,     2, DAP_DP, DAP_RW, 0             , 0); 
  REG(DAP_DP_DLPIDR,    DAP_DP_DLPIDR_ADD,        3, DAP_DP, DAP_RO, dump_dlpidr   , 0); 
  REG(DAP_DP_EVENTSTAT, DAP_DP_EVENTSTAT_ADD,     4, DAP_DP, DAP_RO, 0             , 0); 
  REG(DAP_AP_IDR,       DAP_AP_IDR_ADD,       DAP_X, DAP_AP, DAP_RW, dump_idr      , 0); 

  REGM(DAP_MEMAP_CSW,   DAP_MEMAP_CSW_ADD,  dump_csw, 0);
  REGM(DAP_MEMAP_DRW,   DAP_MEMAP_DRW_ADD,  0       , hook_drw);
  REGM(DAP_MEMAP_BD0,   DAP_MEMAP_BD0_ADD,  0       , hook_bdn);
  REGM(DAP_MEMAP_BD1,   DAP_MEMAP_BD1_ADD,  0       , hook_bdn);
  REGM(DAP_MEMAP_BD2,   DAP_MEMAP_BD2_ADD,  0       , hook_bdn);
  REGM(DAP_MEMAP_BD3,   DAP_MEMAP_BD3_ADD,  0       , hook_bdn);
  REGM(DAP_MEMAP_MBT,   DAP_MEMAP_MBT_ADD,  0       , 0);
  REGM(DAP_MEMAP_CFG,   DAP_MEMAP_CFG_ADD,  0       , 0);
  REGM(DAP_MEMAP_BASE,  DAP_MEMAP_BASE_ADD, 0       , 0);
  REGM(DAP_MEMAP_BASEH, DAP_MEMAP_BASEH_ADD,0       , 0);
  REGM(DAP_MEMAP_TAR,   DAP_MEMAP_TAR_ADD,  0       , 0);
  REGM(DAP_MEMAP_TARH,  DAP_MEMAP_TARH_ADD, 0       , 0);
}

void dap_setreg(struct t_dap *pdap,int r,int val,int fl,int op,int phase,int where)
{
//printf("setreg %2d %-20s val %08x fl %d phase %d where %d\n",r,pdap->regs[r].name,val,fl,phase,where);
  char buf[256];
  int rh = 0;
  if (pdap->regs[r].fhook) 
    rh = pdap->regs[r].fhook(pdap,pdap->regs + r,val,fl,op,phase,where);
  if ((phase & DAP_OP_MASK) == DAP_SEND && op == DAP_RO)
    return;
  if (rh == 0)
  {
    if (op == DAP_WO)
    {
      pdap->regs[r].vals = val;
      pdap->regs[r].flag |= DAP_REG_VALID_S;
      pdap->regs[r].countw++;
    } else if (op == DAP_RO)
    {
      pdap->regs[r].valr = val;
      pdap->regs[r].flag |= DAP_REG_VALID_R;
      pdap->regs[r].countr++;
    } else
      error("wrong op in setreg");
    if (pdap->regs[r].wfset == 0)
      pdap->regs[r].wfset = where;
    pdap->regs[r].wlset = where;
  }
  if (pdap->regs[r].fdump && (fl & OUT_OP_MASK))
  {
    struct buf_env be = {.buf = buf, .lbuf = sizeof buf};
    pdap->regs[r].fdump(pdap->regs + r,fl & OUT_OP_MASK,DAP_RO,phase,&be);
    char *name = pdap->regs[r].name;
    if (name && strncmp(name,"DAP_",4) == 0) 
      name += 4;
    if (!name) name = "???";
//  printf("[setreg add %x] ",pdap->regs[r].add);
    printf("%-16s %08x %s ",name,val,buf);
    if (!(fl & OUT_NOCR))
      putchar('\n');
  }
}

void dap_dumpreg(struct t_dap *pdap,int r,char *buf,int fl)
{
  struct buf_env be = {.buf = buf, .lbuf = sizeof buf};
  if (pdap->regs[r].fdump && (fl & OUT_OP_MASK))
    pdap->regs[r].fdump(pdap->regs + r,fl & OUT_OP_MASK,DAP_RO,DAP_RECEIVE,&be);
}

struct t_dap_reg *getdapregbyid(struct t_dap_reg *reg,int id)
{
  if (id >= 0 && id < DAP_NREG)
    return reg + id;
  return 0;
}

struct t_dap_reg *getdapregbyadd(struct t_dap_reg *reg,uint add,int ty)
{
  for (int i = 0; i < DAP_NREG; i++)
    if (reg[i].add == add && reg[i].ty == ty)
    {
//    printf("getdapregbyadd %x %s\n",add,reg[i].name);
      return reg + i;
    }
  return 0;
}

void dump_dapreg(struct t_dap *pdap,int fl)
{
  char buf[1000];
  int count = 0;

  for (int i = 0; i < DAP_NREG; i++)
    if (pdap->regs[i].countr + pdap->regs[i].countw)
    {
      count++;
      break;
    }
  if (count == 0)
    return;

  struct buf_env be = {.buf = buf, .lbuf = sizeof buf};
  printf("dapreg dump targetsel %08x\n",pdap->targetsel);
  printf("id   add        name          rev v(R) send v(W) #R   #W    when    decode\n");
  printf("--- ---- -------------------- -------- -------- ---- ---- ---- ---- ---...\n");
  for (int i = 0; i < DAP_NREG; i++)
    if (pdap->regs[i].countr + pdap->regs[i].countw)
    {
      printf("%3d %4x %-20s ",
             i,pdap->regs[i].add,pdap->regs[i].name);
      if (pdap->regs[i].countr > 0)
        printf("%08x ",pdap->regs[i].valr);
      else
        printf("         "); 
      if (pdap->regs[i].countw > 0)
        printf("%08x ",pdap->regs[i].vals);
      else
        printf("         "); 
      printf("%4d %4d %4d %4d ",
             pdap->regs[i].countr,pdap->regs[i].countw,pdap->regs[i].wfset,pdap->regs[i].wlset);
      if (pdap->regs[i].fdump)
        printf("%s ",pdap->regs[i].fdump(pdap->regs + i,fl,DAP_RO,DAP_RECEIVE,&be));
      putchar('\n');
    }
}

char *dap_regname(struct t_dap *pdap,int ap,int r,int a,int *pid,int op)
{
  static char buf[20];
  int rid = dapregid(pdap->regs,ap,r,a,op);
//printf("[dapregname rid %d]",rid);
  if (rid >= 0)
  {
    if (pid)
      *pid = rid;
    char *p = pdap->regs[rid].name;
    if (strncmp("DAP_DP_",p,7) == 0)
      return p + 7;
    return p;
  } else if (ap)
  {
    int select    = pdap->regs[DAP_DP_SELECT].vals;
//  printf("[dap_regname sel %x]",select);
    int apsel     = (select & DAP_DP_SELECT_APSEL_MASK)     >> DAP_DP_SELECT_APSEL_SHIFT;
    int apbanksel = (select & DAP_DP_SELECT_APBANKSEL_MASK) >> DAP_DP_SELECT_APBANKSEL_SHIFT;
//  int dpbanksel = (select & DAP_DP_SELECT_DPBANKSEL_MASK) >> DAP_DP_SELECT_DPBANKSEL_SHIFT;
    int add       = apbanksel * 16 + a;
    struct t_dap_reg *pr = getdapregbyadd(pdap->regs,add,ap ? DAP_AP : DAP_DP);
    char *rn = pr ? pr->name : "???";
    sprintf(buf,"ap%d:%03x %s",apsel,add,rn);
    if (pid)
    {
      if (pr)
        *pid = pr - pdap->regs;
      else
        *pid = 1024 + add / 4;
//    printf("[dapregname pid %d]",*pid);
    }
    return buf;
  }
  return "???";
}

int dap_getval(struct t_dap *pdap,int regid, uint *val)
{
  if (pdap->regs[regid].flag & DAP_REG_VALID_S)
  {
    *val = pdap->regs[regid].vals;
    return 1;
  }
  return 0;
}


void dap_save(struct t_dap *pdap,int idx)
{
  idx = IDX(idx);

//printf("dap_save TAR %08x\n",pdap->regs[DAP_MEMAP_TAR].vals);
  for (int i = 0; i < DAP_NREG; i++)
  {
    pdap->mesbuf[idx].regs0[i] = pdap->regs[i].vals;
    pdap->mesbuf[idx].flags0[i] = pdap->regs[i].flag;
  }
}

void dap_restore(struct t_dap *pdap,int idx)
{
  idx = IDX(idx);

  for (int i = 0; i < DAP_NREG; i++)
  {
    pdap->regs[i].vals = pdap->mesbuf[idx].regs0[i];
    pdap->regs[i].flag = pdap->mesbuf[idx].flags0[i];
  }
//printf("dap_restore TAR %08x\n",pdap->regs[DAP_MEMAP_TAR].vals);
}
