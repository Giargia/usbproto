#include "env.h"
#include "readpcapng-stlink.h"
#include "dapreg.h"
#include "driver-cmsis.h"
#include "pico-decode.h"

struct __attribute__((__packed__)) pico_cmd 
{
  unsigned char id;
  unsigned char cmd;
  unsigned int  bits;
};

struct tq
{
  int nbit,seq,rw;
  unsigned char *buf;
  struct tq *next;
};

struct tq *first,*last;

int picoseq;

// build SWD header byte
// bit  P
//  0        1 Start
//  1   *    x APnDP
//  2   *    x RnW
//  3   *    A
//  4   *    A
//  5        X Parity
//  6        0 Stop
//  7        1 Park

static char *fmttreq(int v)
{
  static char buf[20];

//int start  = v & 1;
//int stop   = (v >> 6) & 1;
//int park   = (v >> 7) & 1;
  int apndp  = (v >> 1) & 1;
  int rnw    = (v >> 2) & 1;
//int parity = (v >> 5) & 1;
  int A      = ((((v >> 4) & 1) << 1) | ((v >> 3) & 1)) * 4;

  sprintf(buf,"a %x ",A);
  if (apndp)
    strcat(buf,"AP ");
  else
    strcat(buf,"DP ");
  if (rnw)
    strcat(buf,"R ");
  else
    strcat(buf,"W ");

  return buf;
}

int picoprocess(struct tq *p) 
{
  char crw[2];
  int i,start,stop,park,apndp,rnw,parity,A,ack;

  printf("PP picoprocess nbit %3d rw %d seq %6d ",p->nbit,p->rw,p->seq);
  crw[0] = 'W'; 
  crw[1] = 'R';
  if (p->buf)
    for (i = 0; i < (p->nbit + 7) / 8; i++)
      printf("%02x ",p->buf[i]);
  if (p->rw == 1 && p->buf[0] != 0xff)
  {
    start  = p->buf[0] & 1;
    stop   = (p->buf[0] >> 6) & 1;
    park   = (p->buf[0] >> 7) & 1;
    apndp  = (p->buf[0] >> 1) & 1;
    rnw    = (p->buf[0] >> 2) & 1;
    parity = (p->buf[0] >> 5) & 1;
    A      = ((((p->buf[0] >> 4) & 1) << 1) | ((p->buf[0] >> 3) & 1)) * 4;
    if (start == 1 && stop == 0 && park == 1)
    {
      printf("%c add %x P %d apndp %d ",crw[rnw],A,parity,apndp);
      if (rnw == 0) // W
      {
        p = p->next;
        if (p && p->rw == 2 && p->nbit == 5)
        {
          if (p->buf)
          {
            ack = p->buf[0];
            printf("ack[%x] %d%d%d ",ack,(ack >> 1) & 1, (ack >> 2) & 1,(ack >> 3) & 1);
            p = p->next;
            if (p && p->rw == 1 && p->nbit == 33)
            {
              printf("val %02x %02x %02x %02x %02x ",
                     rev(p->buf[0]),rev(p->buf[1]),rev(p->buf[2]),rev(p->buf[3]),rev(p->buf[4]));
              putchar('\n');
              return 3;
            } else 
            {
              printf("missed write value\n");
              return 2;
            }
          } else
          {
            printf("buf null\n");
            return 1;
          } 
        }
      } else // rnw == 1 read
      {
        p = p->next;
        int v;
        if (p && p->rw == 2 && p->nbit == 38)
        {
          if (p->buf)
            for (i = 0; i < (p->nbit + 7) / 8; i++)
              printf("%02x ",p->buf[i]);
          ack = p->buf[0] & 0x1f;
          v = 0;
          for (i = 0; i < 32; i++)
          { 
            int j,b;
            j = i + 4;
            b = (p->buf[j / 8] >> (j % 8)) & 1;
            v |= (b << i); 
          }
          printf("ack[%x] %d%d%d val %08x ",ack,(ack >> 1) & 1, (ack >> 2) & 1,(ack >> 3) & 1,v);
          putchar('\n');
          return 2;
        } else
        {
          printf("error in read\n");
          return 1;
        }
      }
    } else if (p->buf[0] == 0)
    {
      for (i = 0; p->buf[i] == 0 && i < (p->nbit + 7) / 8; i++);
      if (i == (p->nbit + 7) / 8)
        printf("%d idle cycles ",p->nbit);
      else
        printf("??? check ");
    } else
      printf("??? start %d stop %d park %d ",start,stop,park);
  }
  putchar('\n');
  return 1; 
}

void queue(unsigned char *buf,int nbit,int rw,int seq)
{
  struct tq *p;

  p = malloc(sizeof *p);
//printf("PP queue nbit %3d rw %d seq %6d %p\n",nbit,rw,seq,p);
  p->nbit = nbit;
  p->seq  = seq;
  p->rw   = rw;
  p->buf  = 0;
  if (rw == 1)
  {
    p->buf = malloc((nbit + 7) / 8);
    memcpy(p->buf,buf,(nbit + 7) / 8);
  }
  p->next = 0;
  if (last)
    last->next = p;
  else
    first = p;
  last = p;
}

void dumpq()
{
  int i;
  struct tq *p;

  i = 0;
  if (first || last)
    printf("first %p last %p\n",first,last);
  for (p = first; p; p = p->next)
    printf("%2d %p rw %d nbit %3d rw %d seq %6d\n",i++,p,p->rw,p->nbit,p->rw,p->seq); 
}

void consume(int toseq)
{
  int n;
  printf("consume toseq %d\n",toseq);
//dumpq();
  struct tq *p;
  p = first;
  n = 0;
  while (p && p->seq < toseq)
  {
    if (n <= 0)
      n = picoprocess(p);
    n--;
    first = p->next;
    if (p->buf)
      free(p->buf);
    free(p);
    p = first;
  }
  if (!first)
    last = first;
}

void match(unsigned char *buf,int nbit,int rw,int seq)
{
  struct tq *p;

//printf("PP match nbit %3d rw %d seq %6d first %p last %p\n",nbit,rw,seq,first,last);
//dumpq();

  if (first)
  {
    p = first;
    while (p && p->seq < seq)
      p = p->next;
    if (p && p->rw == 2 && p->seq == seq && p->nbit == nbit)
    {
      if (p->buf)
        error("match 2",__LINE__);
      p->buf = malloc((nbit + 7) / 8);
      memcpy(p->buf,buf,(nbit + 7) / 8);
    } else
    {
      dumpq();
      printf("error match 3 @%d\n",__LINE__);
//    error("match 3",__LINE__);
      return;
    }
    return;
  }
  dumpq();
  printf("error match 3 @%d\n",__LINE__);
//error("match 3",__LINE__);
  return;
}

void picoaddbits(unsigned char *buf,int nbit,int rw,int seq)
{

  if (rw == 1) // write
    queue(buf,nbit,rw,seq);
  else if (rw == 2) // read command 
    queue(buf,nbit,rw,seq);
  else if (rw == 3) // read replay
  {
    match(buf,nbit,rw,seq);
  }
}

void decodereppico(uchar *pd,int len,struct t_decode *pdec,int transfer,int unused)
{
  int i,*l;
  unsigned char *buf,*next;
  struct pico_cmd *pc;

  if (len == 0)
    return;
  relineheader();
  l = (int *) pd;
  printf("decodepico<< len %4d/%4d id %02x\n",len,*l,pd[4]);
  if (len != *l)
    printf("wrong len(%d != %d)",len,*l);
  pc = (struct pico_cmd *) (pd + 4);
  while (pc)
  {
    printf("<<[%4zd,%02x] pico_cmd R cmd %02x bits %08x(%3d) ",
           ((uchar *) pc) - pd,pc->id,pc->cmd,pc->bits,pc->bits);
    if (pc->bits & 0xffff0000) printf("*** CHECK check *** ");
    if (pc->cmd == PROBE_READ_BITS)
    {
      uint val32;
      buf = (unsigned char *) (pc + 1);
      picoaddbits(buf,pc->bits & 0xffff,3,pc->id + picoseq);
      printf("read  ");
      for (i = 0; i < (pc->bits + 7)/8; i++)
        printf("%02x ",buf[i]);
      val32 = 0;
/*
      for (i = 0; i < pc->bits; i++)
        printf("%d.",(buf[i / 8] >> (i % 8)) & 1);
*/
      if (pc->bits == 34)
      {
        int p;
        int v = get32p(buf,&p);
        printf("val %08x p %d",v,p);
      } else if (pc->bits - 1 < 32)
      {
        for (i = 1; i < pc->bits - 1; i++)
          val32 = (val32 >> 1) | (((buf[i / 8] >> (i % 8)) & 1) << 31);
        val32 = val32 >> (32 - pc->bits + 2);
        printf(">>[%08x] ",val32);
      } else
      {
        for (i = 1; i < pc->bits - 2; i++)
          val32 = (val32 >> 1) | (((buf[i / 8] >> (i % 8)) & 1) << 31);
        printf("<<[%08x] ",val32);
      }
      next = ((unsigned char *) (pc + 1)) + (pc->bits + 7) / 8;
      if (next < (unsigned char *) (pd + len - sizeof(struct pico_cmd)))
        pc = (struct pico_cmd *) next;
      else
        pc = 0;
    } else
    {
      printf("unknown %d",pc->cmd);
      pc = 0;
    }
    putchar('\n');
  }
  picoseq += 100;
  consume(picoseq);
}

void decodepico(uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  int i,*l,nb;
  int start,stop,park,apndp,rnw,A; // parity;
  unsigned char *buf,*next;
  struct pico_cmd *pc;
  struct t_dap *pdap;

  pdap = (struct t_dap *) pdec->priv;

  if (len == 0) 
    return;
  relineheader();
  l = (int *) pd;
  printf("decodepico>> len %4d/%4d id %02x\n",len,*l,pd[4]);
  if (len != *l)
    printf("wrong len(%d!=%d)",len,*l);
  pc = (struct pico_cmd *) (pd + 4);
  nb = 0;
  while (pc)
  {
    printf(">>[%4zd,%02x] pico_cmd T cmd %02x bits %08x(%3d) ",
           ((uchar *) pc) - pd,pc->id,pc->cmd,pc->bits,pc->bits);
    if (pc->bits & 0xffff0000) printf("*** CHECK check *** ");
    if (pc->cmd == PROBE_WRITE_BITS)
    {
      buf = (unsigned char *) (pc + 1);
      picoaddbits(buf,pc->bits & 0xffff,1,pc->id + picoseq);
      printf("write ");
      for (i = 0; i < (pc->bits + 7)/8; i++)
        printf("%02x ",buf[i]);
      if (buf[0] != 0xff && buf[0] != 0)
      {
//      printf("[nb %d]",nb);
        if (nb == 0)
        {
          i = 0;
          int rid;
          start  = buf[i] & 1;
          stop   = (buf[i] >> 6) & 1;
          park   = (buf[i] >> 7) & 1;
          apndp  = (buf[i] >> 1) & 1;
          rnw    = (buf[i] >> 2) & 1;
//        parity = (buf[i] >> 5) & 1;
          A      = ((((buf[i] >> 4) & 1) << 1) | ((buf[i] >> 3) & 1)) * 4;
          if (pc->bits == 8)
          {
            if (start == 1 && stop == 0 && park == 1)
            {
              printf("req %02x (%s) %s ", buf[i], fmttreq(buf[i]),
                     dap_regname(pdap,apndp,rnw,A,&rid,DAP_SEND));
            } else
              printf("??? start %d stop %d park %d\n",start,stop,park);
          } else if (pc->bits == 36)
          {
            int p;
            int v = get32p(buf + i,&p);
            printf("val %08x p %d",v,p);
          } else
            printf("TODO ");
          if (rnw)
            nb = 0;
          else if (pc->bits == 8) 
            nb += 8;
          else
            nb = 0;
        } else
        {
          nb += pc->bits;
          if (nb >= 8 + 33) nb = 0;
        }
      }
      next = ((unsigned char *) (pc + 1)) + (pc->bits + 7) / 8;
      if (next <= (unsigned char *) (pd + len - sizeof(struct pico_cmd)))
        pc = (struct pico_cmd *) next;
      else
        pc = 0;
    } else if (pc->cmd == PROBE_READ_BITS)
    {
      picoaddbits(buf,pc->bits & 0xffff,2,pc->id + picoseq);
      printf("read  ");
      next = ((unsigned char *) (pc + 1));
      if (next <= (unsigned char *) (pd + len - sizeof(struct pico_cmd)))
        pc = (struct pico_cmd *) next;
      else
        pc = 0;
    } else if (pc->cmd == PROBE_SET_FREQ)
    {
      printf("set speed ");
      next = ((unsigned char *) (pc + 1));
      if (next <= (unsigned char *) (pd + len - sizeof(struct pico_cmd)))
        pc = (struct pico_cmd *) next;
      else
        pc = 0;
    } else
    {
      printf("unknown %d",pc->cmd);
      pc = 0;
    }
    putchar('\n');
  }
}

