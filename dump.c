#include "env.h"
#ifdef SIMUL
#ifndef TMAIN
#include "def.h"
#include "common.h"
#include "config.h"
#include "stb.h"
#include "prique.h"
#include "dbg.h"
#include "conf.h"
#include "devsim.h"
#include "dev.h"
#include "event.h"
#include "obs.h"
#include "as.h"
#include "post.h"
#include "cpu.h"
#include "regdev.h"
#include "simul.h"
#include "simulsub.h"
#include "simulsub2.h"
#include "init.h"

#include "nvic.h"
#include "exec.h"
#endif
#endif

#include "disasm.h"
#include "dump.h"

/*
#define CHKPC  \
if (pc != p_st->pc) printf("****** pc %08x %08x\n",pc,p_st->pc);
*/

/*
** format a field k index into fields array
**                f format
** format table:
** h      LSL or LSR or ASR 
** H      , LSL#3 or empty
** m      registers mask
** M      registers mask
** !      optional !
** [0-4]  efields[n]
** ... todo
** 
*/
char *fmtf(struct t_cpu *p_cpu,struct t_stat *p_st,char *t,char *f,char *po)
{
  int  i,k,j,ok,fv,ty;
  char *sfmt;
  long v;

  k = -1;
  *po = 0;
  fv = v = 0;
//printf("[fmtf %s %s]\n",t,f);
  if (t[0] >= '0' && t[0] < '5' && t[1] == 0)
    k = t[0] - '0';
  ok = 0;
  if (k >= 0)
  {
    if (k >= NEFIELDS) 
      error("efields overflow");
    v = (long) p_st->efields[k];
//  printf("[ef%d %p]\n",k,p_st->efields[k]);
    fv = ok = 1;
  } else if (strlen(t) > 0)
  {
    j = findpos(p_st->pd,t);
    if (j >= 0)
    {
      if (j >= NFIELDS)
        error("fields overflow");
      fv = ok = 1;
      v = p_st->fields[j];
    }
  } else
    ok = 1;
  if (ok)
  {
    if (f[0] == 'H') // sHift 
    {
      ty = (v >> 5) & 3;
      v = v & 0x1f;
      if (ty == 0)
      {
        if (v != 0)
          sprintf(po,", LSL #%d ",(int) v);
      } else if (ty == 1)
      {
        if (v == 0) v = 32;
        sprintf(po,", LSR #%d ",(int) v);
      } else if (ty == 2)
      {
        if (v == 0) v = 32;
        sprintf(po,", ASR #%d ",(int) v);
      } else if (ty == 2)
      {
        if (v == 0)
          sprintf(po,", RRX 1 ");
        else
          sprintf(po,", ROR #%d ",(int) v);
      }
    } else if (f[0] == 'h') // sHift 
    {
      if (v == 0)
        sprintf(po,"LSL ");
      else if (v == 1)
        sprintf(po,"LSR ");
      else if (v == 2)
        sprintf(po,"ASR ");
      else
        sprintf(po,"? t %d ? ",(int) v);
    } else if (f[0] == 'm' || f[0] == 'M')
    {
      char *sep,*p;
      sep = "";
      p = po;
      *p++ = '{';
      for (i = 0; i < 16; i++)
      {
        if ((v >> i) & 1)
        {
          sprintf(p,"%sR%02d",sep,i);
          p += strlen(p);
          sep = ",";
        }
      }
      *p++ = '}';
      *p = 0;
//    sprintf(p,"m=%x ",v);
    } else if (f[0] == '!')
    {
      if (v)
      {
        *po++ = '!';
        *po = 0;
      }
    } else if (f[0] == 'B')
    {
      if (v == 0)
        sprintf(po,"B");
      if (v == 1)
        sprintf(po,"H");
    } else if (f[0] == 'b')
    {
      if (v == 1)
        sprintf(po,"B");
    } else if (f[0] == 's' || f[0] == 'S')
    {
      if (fv)
      {
        if (v)
          sprintf(po,"S");
      } else
      {
        if (p_cpu->IT == 0)
          sprintf(po,"S");
        else 
        {
          int cond;
          cond = (p_cpu->IT >> 4) & 0xf;
          sprintf(po,"%s",condmem[cond]);
        }
      }
    } else if (f[0] == 'e' || f[0] == 'E') // sign Extend
    {
      if (v)
        sprintf(po,"S");
    } else if (f[0] == 'c')
    {
/*
      sprintf(po,"<IT=%x>",p_cpu->IT);
      po += strlen(po);
*/
      if (p_cpu->IT)
      {
        int cond;
        cond = (p_cpu->IT >> 4);
        sprintf(po,"%s",condmem[cond]);
      }
    } else if (f[0] == 'L' || f[0] == 'l')
    {
      if (v)
        sprintf(po,"LDM");
      else
        sprintf(po,"STM");
    } else if (f[0] == '%')
    {
      sprintf(po,f,v);
    } else if (f[0] == 'C')
    {
      sprintf(po,"%s",condmem[v]);
    } else if (f[0] == 'd')
    {
      sprintf(po,"%d",(int) v);
    } else if (f[0] == 'f')
    {
      if (v == 0)
        strcat(po,".F32");
      else
        strcat(po,".D64");
    } else if (f[0] == '*' && v >= 0 && v <= 15)
    {
      int vv = regr(p_cpu,v,OP_NULL); 
      sprintf(po,"%08x",vv);
    } else
    {
      sfmt = "%x";
      if (t[0] == 'r' || t[0] == 'R' || 
          f[0] == 'r' || f[0] == 'R')
      {
        sfmt = "%02d";
        *po++ = 'R';
      }
      sprintf(po,sfmt,v);
    }
  } else
    sprintf(po,"(%s=undef)",t);
    
  return po;
}
/*
** format field: <val> or <val#fmt>     
*/
int dump_gen(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  char *p,token[40],fmt[10],*pt,*pf;
  char out[200],*po;
  int  i,intoken,infmt,nb;

#define CHKBUF(p,buf) if ((p) > (buf) + sizeof(buf)) error("dump_gen buffer overflow @%d",__LINE__);

  if (p_st->prefix)
    logprefix(p_cpu,0);
  intoken = infmt = nb = 0;
  po = out;
  pf = fmt;
  pt = token;
  if (p_f->fmtinst == 0)
  {
    printf("null");
    return 0;
  }
//printf("[%s]\n",p_f->fmtinst);
  for (p = p_f->fmtinst; *p; p++)
  {
    if (*p == '<')
    {
      intoken = 1;
      pt = token;
      pf = fmt;
    } else if (*p == '>')
    {
      if (intoken)
      {
        *pt = *pf = 0;
        fmtf(p_cpu,p_st,token,fmt,po);
        po += strlen(po);
        CHKBUF(po,out);
      }
      intoken = infmt = 0;
    } else if (intoken && *p == '#')
    {
      infmt = 1;
    } else 
    {
      if (infmt)
        *pf++ = *p;
      else if (intoken)
        *pt++ = *p;
      else
      {
        if (*p == ' ')
        {
          if (nb == 0)
          {
            int l;

            l = 8 - (po - out);
            if (l > 0)
            {
              for (i = 0; i < l; i++)
                *po++ = ' ';
            } else
              *po++ = *p;
          }  
          nb++;
        } else
        {
          nb = 0;
          *po++ = *p;
        }
      }
      CHKBUF(po,out);
      CHKBUF(pt,token);
      CHKBUF(pf,fmt);
    }
  }
  *po = 0;
  printf("%s",out);
  if (p_st->skip && p_cpu->logskip)
  {
    if (!p_st->exe)
      printf(" ; SKIP");
  }
  CHKBUF(po,out);
  CHKBUF(pt,token);
  CHKBUF(pf,fmt);
//printf("[done]");
  if (p_st->doline)
    putchar('\n');
  return 0;
#undef CHKBUF
}

int dump_it(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int mask;

  mask = p_st->fields[2];  CHK(p_st,2,"mask",__LINE__);

  if (mask == 8)
    p_st->efields[0] = "";
  else if (mask == 12)
    p_st->efields[0] = "T";
  else if (mask == 4)
    p_st->efields[0] = "E";
  else if (mask == 14)
    p_st->efields[0] = "EE";
  else if (mask == 6)
    p_st->efields[0] = "TE";
  else if (mask == 10)
    p_st->efields[0] = "ET";
  else if (mask == 2)
    p_st->efields[0] = "TT";
  else if (mask == 15)
    p_st->efields[0] = "EEE";
  else if (mask == 1)
    p_st->efields[0] = "TTT";
  else if (mask == 11)
    p_st->efields[0] = "TET";
  else if (mask == 5)
    p_st->efields[0] = "TET";
  else if (mask == 13)
    p_st->efields[0] = "TTE";
  else if (mask == 7)
    p_st->efields[0] = "ETT";
  else
    p_st->efields[0] = "???";

  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_res(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  printf("reserved");
  return 0;
}

int dump_pop(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int rm,m,imm8; // ,add; 

  imm8 = p_st->fields[3]; CHK(p_st,3,"imm8",__LINE__);
  m    = p_st->fields[4]; CHK(p_st,4,"m",__LINE__);

  rm = imm8 | m << 15;    
  p_st->efields[0] = EFIELD(rm);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_push(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int rm,m; 

  rm = p_st->fields[3];
  m = p_st->fields[4];
  rm = rm | m << 14;

  p_st->efields[0] = EFIELD(rm);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_cps(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int im,aif,a,i,f;
  char buf[4],*pb;

  im   = p_st->fields[12];  CHK(p_st,12,"im"   ,__LINE__);
  aif  = p_st->fields[ 8];  CHK(p_st, 8,"aif"  ,__LINE__);

  a = (aif >> 2) & 1;
  i = (aif >> 1) & 1;
  f = aif & 1;

  if (im)
    p_st->efields[0] = "IE";
  else
    p_st->efields[0] = "ID";
  pb = buf;
  p_st->efields[1] = pb;
  if (a)
    *pb++ = 'A';
  if (i)
    *pb++ = 'I';
  if (f)
    *pb++ = 'F';
  *pb = 0;
  dump_gen(p_cpu,p_st,p_f);

  return 0;
}

int dump_addadrt3(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int v,imm,i,imm3,imm8,op;

  i    = p_st->fields[0];  CHK(p_st, 0,"i",   __LINE__);
  imm3 = p_st->fields[8];  CHK(p_st, 8,"imm3",__LINE__);
  imm8 = p_st->fields[10]; CHK(p_st,10,"imm8",__LINE__);
//rd   = p_st->fields[9];  CHK(p_st, 9,"rd",  __LINE__);
  op   = p_st->fields[5];  CHK(p_st, 5,"op1", __LINE__);
//op2  = p_st->fields[5];  CHK(p_st, 5,"op2", __LINE__);

  imm = (i << 11) | (imm3 << 8) | imm8;
  p_st->efields[0] = EFIELD(imm);
  v = p_st->pc + 2; 
  v = ((v + 3) / 4) * 4;
  p_st->efields[1] = EFIELD(v + imm);
  if (op == 1)
    p_f->fmtinst = "ADR<#c>.W    <rd>, <0> ; <1>";
  else
    p_f->fmtinst = "ADDW<#c> <rd>,<rn>,<0>";
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_addimm(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm,i,imm3,imm8;

  i    = p_st->fields[0];  CHK(p_st, 0,"i",   __LINE__);
  imm3 = p_st->fields[8];  CHK(p_st, 8,"imm3",__LINE__);
  imm8 = p_st->fields[10]; CHK(p_st,10,"imm8",__LINE__);

  imm = (i << 11) | (imm3 << 8) | imm8;
  p_st->efields[0] = EFIELD(imm);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_subimm(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm,i,imm3,imm8;

  i    = p_st->fields[0];  CHK(p_st, 0,"i",   __LINE__);
  imm3 = p_st->fields[8];  CHK(p_st, 8,"imm3",__LINE__);
  imm8 = p_st->fields[10]; CHK(p_st,10,"imm8",__LINE__);

  imm = (i << 11) | (imm3 << 8) | imm8;
  p_st->efields[0] = EFIELD(imm);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_nop(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int hint;

  hint = p_st->fields[14];  CHK(p_st,14,"hint",   __LINE__);
  if (hint == 0)
    p_f->fmtinst = "NOP";
  if (hint == 1)
    p_f->fmtinst = "YELD";
  if (hint == 2)
    p_f->fmtinst = "WFE";
  if (hint == 3)
    p_f->fmtinst = "WFI";
  if (hint == 4)
    p_f->fmtinst = "SEV";
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_asp(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int opc,imm7;

  opc  = p_st->fields[9];
  imm7 = p_st->fields[5];
  if (opc == 0)
    p_f->fmtinst = "ADD<#c>  sp,sp,#<0>";
  else
    p_f->fmtinst = "SUB<#c>  sp,sp,#<0>";
  p_st->efields[0] = EFIELD(imm7 * 4);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_rev(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int opc;

  opc = p_st->fields[10];  CHK(p_st,10,"opc2",__LINE__);
  if (opc == 0)
    p_st->efields[0] = "";
  else if (opc == 1)
    p_st->efields[0] = "16";
  else if (opc == 3)
    p_st->efields[0] = "SH";
  else
    p_st->efields[0] = "ERR";
    
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_xxtx(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int opc;

  opc = p_st->fields[10];  CHK(p_st,10,"opc2",__LINE__);

  if (opc < 2)
    p_st->efields[0] = "S";
  else
    p_st->efields[0] = "U";
  if (opc & 1)
    p_st->efields[1] = "B";
  else
    p_st->efields[1] = "H";

  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_cmpr3(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm,imm3,imm2,type,t;

  imm3 = p_st->fields[8];
  imm2 = (p_st->fields[11] >> 2) & 3;
  type = p_st->fields[11] & 3;

  imm = (imm3 << 2) | imm2;
  imm = DecodeImmShift(type,imm,&t);
  p_st->efields[0] = EFIELD(imm);
  dump_gen(p_cpu,p_st,p_f);

  return 0;
}

int dump_bic3(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm,imm2,imm3,type,t;

  imm3 = p_st->fields[8];
  imm2 = (p_st->fields[11] >> 2) & 3;
  type = p_st->fields[11] & 3;
  imm = (imm3 << 2) | imm2;
  imm = DecodeImmShift(type,imm,&t);
  p_st->efields[0] = EFIELD(imm);
  p_st->efields[1] = EFIELD(t);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_and3(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm,imm2,imm3,type,t;

  imm3 = p_st->fields[8];
  imm2 = (p_st->fields[11] >> 2) & 3;
  type = p_st->fields[11] & 3;

  imm = (imm3 << 2) | imm2;
  imm = DecodeImmShift(type,imm,&t);
  p_st->efields[0] = EFIELD(imm);
  p_st->efields[1] = EFIELD(t);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_orr3(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm,imm2,imm3,type,t;

  imm3 = p_st->fields[8];
  imm2 = (p_st->fields[11] >> 2) & 3;
  type = p_st->fields[11] & 3;

  imm = (imm3 << 2) | imm2;
  imm = DecodeImmShift(type,imm,&t);
  p_st->efields[0] = EFIELD(imm);
  p_st->efields[1] = EFIELD(t);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_addsr(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int dn,rdn,d;

  dn = p_st->fields[1];
  rdn = p_st->fields[3];
  
  d = (dn << 3) | rdn;
  p_st->efields[0] = EFIELD(d);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_dpadd(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm8,imm3,i,imm;
  int C;
  i     = p_st->fields[0];
  imm3  = p_st->fields[8];
  imm8  = p_st->fields[10];
  imm   = (i << 11) | (imm3 << 8) | imm8;

  C     = p_cpu->C;
  imm   = ThumbExpandImmWithC(imm,&C);

  p_st->efields[0] = EFIELD(imm);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_dptstand(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int C,rd,S,i,imm3,imm8,imm;

  S     = p_st->fields[3];
  rd    = p_st->fields[9];
  i     = p_st->fields[0];
  imm3  = p_st->fields[8];
  imm8  = p_st->fields[10];
  imm   = (i << 11) | (imm3 << 8) | imm8;
  C     = p_cpu->C;
  imm   = ThumbExpandImmWithC(imm,&C);
  p_st->efields[0] = EFIELD(imm);

  if (S && rd == PC)
    p_f->fmtinst = "TST<#c>     <rn>,#<0>";
  else
    p_f->fmtinst = "AND<#c>     <rd>,<rn>,#<0>";
  return dump_gen(p_cpu,p_st,p_f);
}

int dump_dpmovw(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int i,imm3,imm4,imm8,imm;

  i     = p_st->fields[0];
  imm4  = p_st->fields[7];
  imm3  = p_st->fields[8];
  imm8  = p_st->fields[10];
  imm   = (imm4 << 12) | (i << 11) | (imm3 << 8) | imm8;

  p_st->efields[0] = EFIELD(imm);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_dpmovt(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int i,imm3,imm4,imm8,imm;

  i     = p_st->fields[0];
  imm4  = p_st->fields[7];
  imm3  = p_st->fields[8];
  imm8  = p_st->fields[10];
  imm   = (imm4 << 12) | (i << 11) | (imm3 << 8) | imm8;

  p_st->efields[0] = EFIELD(imm);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_cmp(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  return 0;
}

int dump_dporrmov(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int C,rn,i,imm3,imm8,imm;

  rn    = p_st->fields[7];
  i     = p_st->fields[0];
  imm3  = p_st->fields[8];
  imm8  = p_st->fields[10];
  imm   = (i << 11) | (imm3 << 8) | imm8;
  C     = p_cpu->C;
  imm   = ThumbExpandImmWithC(imm,&C);
  p_st->efields[0] = EFIELD(imm);
  if (rn == PC)
    p_f->fmtinst = "MOV<S#S><#c>    <rd>,#<0>";
  else
    p_f->fmtinst = "ORR<S#S><#c>    <rd>,<rn>,#<0>";
  return dump_gen(p_cpu,p_st,p_f);;
}

int dump_dpornmvn(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int C,rn,i,imm3,imm8,imm;

  rn    = p_st->fields[7];
  i     = p_st->fields[0];
  imm3  = p_st->fields[8];
  imm8  = p_st->fields[10];
  imm   = (i << 11) | (imm3 << 8) | imm8;
  C     = p_cpu->C;
  imm   = ThumbExpandImmWithC(imm,&C);
  p_st->efields[0] = EFIELD(imm);
  if (rn == PC)
    p_f->fmtinst = "MVN<S#S><#c>    <rd>,#<0>";
  else
    p_f->fmtinst = "ORN<S#S><#c>    <rd>,<rn>,#<0>";
  return dump_gen(p_cpu,p_st,p_f);;
}

int dump_dpeor(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int i,imm3,imm8,imm;

  i     = p_st->fields[0];
  imm3  = p_st->fields[8];
  imm8  = p_st->fields[10];
 
  imm   = (i << 11) | (imm3 << 8) | imm8;
  p_st->efields[0] = EFIELD(imm);
  dump_gen(p_cpu,p_st,p_f);

  return 0;
}

int dump_dpmvn(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int i,imm3,imm8,imm;

  i     = p_st->fields[0];
  imm3  = p_st->fields[8];
  imm8  = p_st->fields[10];
 
  imm   = (i << 11) | (imm3 << 8) | imm8;
  p_st->efields[0] = EFIELD(imm);
  dump_gen(p_cpu,p_st,p_f);

  return 0;
}

int dump_dpbic(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int C,i,imm3,imm8,imm;

  i     = p_st->fields[0];
  imm3  = p_st->fields[8];
  imm8  = p_st->fields[10];
 
  imm   = (i << 11) | (imm3 << 8) | imm8;
  C     = p_cpu->C;
  imm   = ThumbExpandImmWithC(imm,&C);

  p_st->efields[0] = EFIELD(imm);
  dump_gen(p_cpu,p_st,p_f);

  return 0;
}

int dump_dprsb(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int i,imm3,imm8,imm;

  i     = p_st->fields[0];
  imm3  = p_st->fields[8];
  imm8  = p_st->fields[10];
  imm   = (i << 11) | (imm3 << 8) | imm8;
  p_st->efields[0] = EFIELD(imm);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_subcmpr3(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int rd,imm2,imm3,type,imm,t;

  rd   = p_st->fields[10]; CHK(p_st,10,"rd",__LINE__);
  imm3 = p_st->fields[8];  CHK(p_st, 8,"imm3",__LINE__);
  imm2 = p_st->fields[11]; CHK(p_st,11,"imm2",__LINE__);

  type = imm2 & 3;
  imm2 = (imm2 >> 2) & 3;

  imm = (imm3 << 2) | imm2;
  imm = DecodeImmShift(type,imm,&t);
  p_st->efields[0] = EFIELD(imm);
  p_st->efields[1] = EFIELD(t);

  if (rd == 15)
    p_f->fmtinst = "CMP<#c>.W <rn>, <rm>, <1#H> #<0>";
  else
    p_f->fmtinst = "SUB<#c>.W <rn>, <rm>, <1#H> #<0>";
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_dpsubcmp(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm8,imm3,i,imm;
  int rd;

  i     = p_st->fields[0];  CHK(p_st, 0,"i",   __LINE__);
  imm3  = p_st->fields[8];  CHK(p_st, 8,"imm3",__LINE__);
  imm8  = p_st->fields[10]; CHK(p_st,10,"imm8",__LINE__);
  rd    = p_st->fields[9];  CHK(p_st, 9,"rd",  __LINE__);

  imm   = (i << 11) | (imm3 << 8) | imm8;
  imm   = ThumbExpandImm(imm);

  p_st->efields[0] = EFIELD(imm);
  if (rd == 15)
    p_f->fmtinst = "CMP.W   <rn>,#<0>";
  else
    p_f->fmtinst = "SUB<S#S><#c>.W   <rd>,<rn>,#<0>";
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_b(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm11;

  imm11 = p_st->fields[0];
  p_st->efields[0] = EFIELD(p_st->pc + 4 + imm11 * 2);

  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_cbz(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int i,imm,imm5;

  i    = p_st->fields[6];
  imm5 = p_st->fields[7];
  imm  = (i << 6) + (imm5 << 1);

  p_st->efields[0] = EFIELD(p_st->pc + 4 + imm);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_cbnz(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int i,imm,imm5;

  i    = p_st->fields[6];
  imm5 = p_st->fields[7];
  imm  = (i << 6) + (imm5 << 1);

  p_st->efields[0] = EFIELD(p_st->pc + 4 + imm);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_bcond(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm8;

  imm8 = p_st->fields[1];

  p_st->efields[0] = EFIELD(p_st->pc + 4 + imm8 * 2);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_bl(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int S,imm10,J1,J2,imm11,imm; 

  S     = p_st->fields[0];
  imm10 = p_st->fields[1];
  J1    = p_st->fields[2];
  J2    = p_st->fields[3];
  imm11 = p_st->fields[4];
  imm   = (S << 24) |
          ((1 - (J1 ^ S)) << 23) |
          ((1 - (J2 ^ S)) << 22) |
          (imm10 << 12) |
          (imm11 << 1);

  if (S)
    imm = imm | 0xfe000000;
  p_st->efields[0] = EFIELD(imm);
  p_st->efields[1] = EFIELD(p_st->pc + 4 + imm);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_bwt3(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm,S,imm6,imm11,J1,J2;

  S     = p_st->fields[ 0];  CHK(p_st, 0,"S"    ,__LINE__);
  imm11 = p_st->fields[ 4];  CHK(p_st, 4,"imm11",__LINE__);
  J1    = p_st->fields[ 2];  CHK(p_st, 2,"j1"   ,__LINE__);
  J2    = p_st->fields[ 3];  CHK(p_st, 3,"j2"   ,__LINE__);
  imm6  = p_st->fields[12];  CHK(p_st,12,"imm6" ,__LINE__);

// imm32 = SignExtend(S:J2:J1:imm6:imm11:'0', 32);

  imm = J2 << 19 |
        J1 << 18 |
        imm6 << 12 |
        imm11 << 1;

  if (S)
    imm = imm | 0xfff00000;

  p_st->efields[0] = EFIELD(imm);
  p_st->efields[1] = EFIELD(p_st->pc + 4 + imm);
  return dump_gen(p_cpu,p_st,p_f);
}

int dump_bwt4(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  return dump_bl(p_cpu,p_st,p_f);
}

int dump_adr(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int sp,imm8,v;

  sp = p_st->fields[0];
  imm8 = p_st->fields[2];
  if (sp)
  {
    p_st->efields[2] = "SP";
    v = regr(p_cpu,SP,OP_NULL); 
  } else
  {
    p_st->efields[2] = "PC";
    v = p_st->pc + 2; 
    v = ((v + 3) / 4) * 4;
  }
  p_st->efields[0] = EFIELD(4 * imm8);
  p_st->efields[1] = EFIELD(v + imm8 * 4);

  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_ldrio(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_strt2(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_strio(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int rn,imm5,add;

  imm5 = p_st->fields[2];
  rn   = p_st->fields[3];
  add  = regr(p_cpu,rn,OP_EXEC); 
  add += imm5 * 4;
  p_st->efields[0] = EFIELD(add);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_ldrsp(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm8;

  imm8 =  p_st->fields[2];
  p_st->efields[0] = EFIELD(imm8 * 4);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_strsp(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm8;

  imm8 =  p_st->fields[2];
  p_st->efields[0] = EFIELD(imm8 * 4);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_ldrlp(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm8,add;

  imm8 = p_st->fields[1];
  add = (p_st->pc + 4 + 4 * imm8) & ~3;
  p_st->efields[0] = EFIELD(4 * imm8);
  p_st->efields[1] = EFIELD(add);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_ldm(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int rn,mask,w;

  rn   = p_st->fields[1];
  mask = p_st->fields[2];
  w    = ((mask >> rn) & 1) == 0;
  p_st->efields[0] = EFIELD(w);

  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_ls4(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int p,m,mask;

  p    = p_st->fields[5];
  m    = p_st->fields[6];
  mask = p_st->fields[8];

  mask = (p << 15) | (m << 14) | mask;

  p_st->efields[0] = EFIELD(mask);
  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_ssat(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm2,imm3,sat_imm,sh;

  imm2 = p_st->fields[12];
  imm3 = p_st->fields[8];
  sh   = p_st->fields[15];
  sat_imm = p_st->fields[16];

  p_st->efields[0] = EFIELD(sat_imm + 1);
  p_st->efields[1] = EFIELD(sh << 6 | (imm3 << 2) | imm2);

  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

int dump_ubfx(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm2,imm3,width;

  imm2 = p_st->fields[12];
  imm3 = p_st->fields[8];
  width = p_st->fields[11];

  p_st->efields[0] = EFIELD((imm3 << 2) | imm2);
  p_st->efields[1] = EFIELD(width + 1);

  dump_gen(p_cpu,p_st,p_f);
  return 0;
}

// data processing

int dump_dpind(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int rd,op,idx;
  struct t_dec *pd;

  op = p_st->fields[4];
  rd = p_st->fields[9]; 

  pd = dectdp + op;
  idx = 0;
  if (op == 4 && rd == PC)
    idx = 1;
  else if (op == 8 && rd == PC)
    idx = 1;
#ifdef DD
  if (p_cpu->logd && p_cpu->execcount >= p_cpu->logd)
  {
    char ne[40];
    char nd[40];

    if (p_st->prefix)
      logprefix(p_cpu,0);
    printf("D dpind op %d rd %d idx %d\n",op,rd,idx);
    if (p_st->prefix)
      logprefix(p_cpu,0);
    printf("D ncall %d pdis %s pexe %s fmtinst %s\n",
           pd->func[idx].ncall,
           fmtfname(p_cpu,pd->func[idx].pdis,nd),
           fmtfname(p_cpu,pd->func[idx].pexec,ne),
           pd->func[idx].fmtinst);
  }
#endif

  if (pd->func[idx].pdis)
    pd->func[idx].pdis(p_cpu,p_st,pd->func + idx);
  else if (pd->func[idx].fmtinst)
    dump_gen(p_cpu,p_st,pd->func + idx);
  else
    printf("TBD %d null function in dectdp op %d idx %d\n",p_cpu->execcount,op,idx);
 
  return 0;
}

// add sub processing

int dump_asind(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int op1,op2,sw;
  struct t_dec *pd;

  op1 = p_st->fields[5];
  op2 = p_st->fields[6];
  sw = (op1 << 2) + op2;
  pd = dectas + sw;

  if (pd->func[0].pdis)
    pd->func[0].pdis(p_cpu,p_st,pd->func);
  else if (pd->func[0].fmtinst)
    dump_gen(p_cpu,p_st,pd->func);
  else
    printf("TBD %d null function in dump_asind op %d\n",p_cpu->execcount,sw);
 
  return 0;
}

// move processing
int dump_mvind(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int op;
  struct t_dec *pd;

  op = p_st->fields[3];
  pd = dectmv + op;

  if (pd->func[0].pdis)
    pd->func[0].pdis(p_cpu,p_st,pd->func);
  else if (pd->func[0].fmtinst)
    dump_gen(p_cpu,p_st,pd->func);
  else
    printf("TBD %d null function in dump_mvind dectmv op %d\n",p_cpu->execcount,op);
 
  return 0;
}

// signed or zero extend 
int dump_sozind(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int op,rn,idx;
  struct t_dec *pd;

  op = p_st->fields[4];
  rn = p_st->fields[6];

  idx = -1;
  if (op == 1 && rn == 15)
    idx = 11;
  else if (op == 5 && rn == 15)
    idx = 9;
  else if (op == 0 && rn == PC)
    idx = 5;
  else if (op == 0 && rn == PC)
    idx = 3;
  else if (op == 1 && rn != PC)
    idx = 8;
  else if (op == 0 && rn != PC)
    idx = 2;
  else if (op == 5 && rn != PC)
    idx = 6;
  
  if (idx < 0)
  {
    warning("idx < 0 in dump_sozind op %d rn %d",op,rn);
    return 0;
  } 
  pd = dectsoz + idx;
  if (pd->func[0].pdis)
    pd->func[0].pdis(p_cpu,p_st,pd->func);
  else if (pd->func[0].fmtinst)
    dump_gen(p_cpu,p_st,pd->func);
  else
    printf("TBD %d null function in dump_sozind dectsoz idx %d\n",p_cpu->execcount,idx);
 
  return 0;
}

// bit field processing
int dump_bfind(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  struct t_dec *pd;
  int rn,op,idx,simm;

  op = p_st->fields[4];  CHK(p_st, 4,"op0",__LINE__);
  rn = p_st->fields[7];  CHK(p_st, 7,"rn" ,__LINE__);
  op = op & 0x7;

  idx = -1;
  simm = 0;
  if (op == 3 && rn == 15)
    idx = 0;
  else if (op == 3)
    idx = 1;
  else if (op == 2)
    idx = 2;
  else if (op == 0)
    idx = 3;
  else if (op == 0)
    idx = 4;
  else if (op == 1 && simm == 0)
    idx = 5;
  else if (op == 6)
    idx = 6;
  else if (op == 4)
    idx = 7;
  else if (op == 5)
    idx = 8;
  else if (op == 1 && simm == 1)
    idx = 9;

  if (idx < 0)
    printf("idx < 0 in dump_bfind\n");
  else
  {
    pd = dectbf + idx;

    if (pd->func[0].pdis)
      pd->func[0].pdis(p_cpu,p_st,pd->func);
    else if (pd->func[0].fmtinst)
      dump_gen(p_cpu,p_st,pd->func);
    else
      printf("TBD %d null function in dump_bfind op %d rn %d simm %d idx %d\n",
             p_cpu->execcount,op,rn,simm,idx);
  } 
  return 0;
}

// data processing constant shift tab 3.25
int dump_dpcsind(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int op,idx,rn,rd,S;
  struct t_dec *pd;

  idx = 0;
  op = p_st->fields[2];   CHK(p_st, 2,"op.1",__LINE__);
  rn = p_st->fields[6];   CHK(p_st, 6,"rn",  __LINE__);
  rd = p_st->fields[10];  CHK(p_st,10,"rd",  __LINE__);
  S  = p_st->fields[5];   CHK(p_st, 5,"S",   __LINE__);

  if (op == 0 && rd == PC && S == 1)
    idx = 1;
  else if (op == 8 && rd == PC && S == 1)
    idx = 1;
  else if (op == 13 && rd == PC && S == 1)
    idx = 1;
  else if (op == 2 && rn != 15) // doc is wrong!!! == instead of !=
    idx = 1;
  else if (op == 6 && S == 0)
    idx = 1;
  else if (op == 3 && rn == PC)
    idx = 1;
  else if (op == 4 && rd == PC && S == 1)
    idx = 1;

  pd = dectdpcs + op;
//printf("dump_dpcsind idx %d op %d\n",idx,op);

  if (pd->func[idx].pdis)
    pd->func[idx].pdis(p_cpu,p_st,pd->func + idx);
  else if (pd->func[idx].fmtinst)
    dump_gen(p_cpu,p_st,pd->func + idx);
  else
    printf("TBD %d null function in dump_dpcsind op %d idx %d\n",p_cpu->execcount,op,idx);

  return 0;
}

// Move, and immediate shift instructions table 3.26
int dump_movishift(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int imm2,imm3,imm5,ty,idx;
  struct t_dec *pd;
 
  imm2 = p_st->fields[11];   CHK(p_st,11,"imm2",__LINE__);
  imm3 = p_st->fields[ 8];   CHK(p_st, 8,"imm3",__LINE__);

  ty = imm2 & 3;
  imm2 = imm2 >> 2;

  imm5 = imm2 | (imm3 << 2);
  p_st->efields[0] = EFIELD(imm5);

  idx = -1;
  if (ty == 0 && imm5 == 0)
    idx = 0;
  else if (ty == 0 && imm5 != 0)
    idx = 1;
  else if (ty == 1)
    idx = 2;
  else if (ty == 2)
    idx = 3;
  else if (ty == 3 && imm5 != 0)
    idx = 4;
  else if (ty == 3 && imm5 == 0)
    idx = 5;

  pd = dectmis + idx;
//printf("dump_movishift ty %d imm5 %d idx %d\n",ty,imm5,idx);
  if (idx < 0)
  {
    printf("wrong idx %d in dump_movishift\n",idx);
    return 0;
  }

  if (pd->func[0].pdis)
    pd->func[0].pdis(p_cpu,p_st,pd->func);
  else if (pd->func[0].fmtinst)
    dump_gen(p_cpu,p_st,pd->func);
  else
    printf("TBD %d null function in dump_movishift idx %d\n",p_cpu->execcount,idx);
  return 0;
}

// register-controlled shift
int dump_rcshind(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  struct t_dec *pd;
  int op;

  op = p_st->fields[3];
  pd = dectrcsh + op;

  if (pd->func[0].pdis)
    pd->func[0].pdis(p_cpu,p_st,pd->func);
  else if (pd->func[0].fmtinst)
    dump_gen(p_cpu,p_st,pd->func);
  else
    printf("TBD %d null function in dump_rcshind op %d\n",p_cpu->execcount,op);

  return 0;
}

int dump_simd(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int op,op2,idx;
  struct t_dec *pd;
  op  = p_st->fields[4];  CHK(p_st, 4,"op.3" ,__LINE__);
  op2 = p_st->fields[11]; CHK(p_st,11,"op2_" ,__LINE__);
  op2 = op2 & 7;

  idx = -1;
  if (op == 0 && op2 == 4)
    idx = 19;

  pd = dectsimd + idx;

  if (pd->func[0].pdis)
    pd->func[0].pdis(p_cpu,p_st,pd->func);
  else if (pd->func[0].fmtinst)
    dump_gen(p_cpu,p_st,pd->func);
  else
    printf("TBD %d null function in dump_simd op %d\n",p_cpu->execcount,idx);

  return 0;
}

// other 3 reg data proc 
int dump_otherind(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int op,op2,idx;
  struct t_dec *pd;

  op  = p_st->fields[4];  CHK(p_st, 4,"op.3" ,__LINE__);
  op2 = p_st->fields[11]; CHK(p_st,11,"op2_" ,__LINE__);
  op2 = op2 & 7;

  idx = -1;
  if (op == 1 && op2 == 2)
    idx = 5;
  else if (op == 3 && op2 == 0)
    idx = 0;
  else if (op == 2 && op2 == 0)
    idx = 9;

  pd = dectother + idx;

  if (pd->func[0].pdis)
    pd->func[0].pdis(p_cpu,p_st,pd->func);
  else if (pd->func[0].fmtinst)
    dump_gen(p_cpu,p_st,pd->func);
  else
    printf("TBD %d null function in dump_otherind op %d\n",p_cpu->execcount,idx);

  return 0;
}

int dump_32mdind(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int op,op2,idx,racc,n,m;
  struct t_dec *pd;

  op   = p_st->fields[4];   CHK(p_st, 4,"op.3",__LINE__);
  op2  = p_st->fields[11];  CHK(p_st,11,"op2_",__LINE__);
  racc = p_st->fields[9];   CHK(p_st, 9,"racc",__LINE__);
  n    = p_st->fields[17];  CHK(p_st,17,"N"   ,__LINE__);
  m    = p_st->fields[18];  CHK(p_st,18,"M"   ,__LINE__);

  p_st->efields[0] = n == 0 ? "B" : "T";
  p_st->efields[1] = m == 0 ? "B" : "T";
  idx = -1;
  if (op == 0 && op2 == 0 && racc != PC)
    idx = 0;
  else if (op == 0 && op2 == 0 && racc == PC)
    idx = 1;
  else if (op == 0 && op2 == 1)
    idx = 2;
  else if (op == 1 && racc != PC)
    idx = 3;
  else if (op == 1 && racc == PC)
    idx = 4;

  pd = dect32md + idx;

  if (pd->func[0].pdis)
    pd->func[0].pdis(p_cpu,p_st,pd->func);
  else if (pd->func[0].fmtinst)
    dump_gen(p_cpu,p_st,pd->func);
  else
    printf("TBD %d null function in dump_32mdind op %d\n",p_cpu->execcount,idx);

  return 0;
}

// 64-bit multiply, multiply-accumulate, and divide instructions
// Table 3-33 Other two-register data processing instructions
int dump_64mdind(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int op,op2,idx;
  struct t_dec *pd;

  op  = p_st->fields[4];   CHK(p_st, 4,"op.3",__LINE__);
  op2 = p_st->fields[11];  CHK(p_st,11,"op2_",__LINE__);

  idx = -1;
  if (op == 0 && op2 == 0)
    idx = 0;
  else if (op == 1 && op2 == 15)
    idx = 1;
  else if (op == 2 && op2 == 0)
    idx = 2;
  else if (op == 3 && op2 == 15)
    idx = 3;
  else if (op == 4 && op2 == 0)
    idx = 4;
  else if (op == 4 && (op2 >= 8 && op2 < 12))
    idx = 5;
  else if (op == 4 && (op2 >= 12 && op2 < 14))
    idx = 6;
  else if (op == 5 && (op2 >= 12 && op2 < 14))
    idx = 7;
  else if (op == 6 && op2 == 0)
    idx = 8;
  else if (op == 6 && op2 == 6)
    idx = 9;

  pd = dect64md + idx;

  if (pd->func[0].pdis)
    pd->func[0].pdis(p_cpu,p_st,pd->func);
  else if (pd->func[0].fmtinst)
    dump_gen(p_cpu,p_st,pd->func);
  else
    printf("TBD %d null function in dump_64mdind op %d\n",p_cpu->execcount,idx);

  return 0;
}

int dump_lsetbind(struct t_cpu *p_cpu,struct t_stat *p_st,struct t_decfun *p_f)
{
  int L,op,idx;

  op  = p_st->fields[9];   CHK(p_st, 9,"op",__LINE__);
  L   = p_st->fields[3];   CHK(p_st, 3,"L" ,__LINE__);

  idx = -1;
  if (op == 0 && L == 1)
    idx = 6;
  else if (op == 1 && L == 1)
    idx = 7;
  if (idx < 0)
    exec_warning(p_cpu,"tbd in dump_lsetbind");
  if (dectlsetb[idx].func[0].pdis)
    dectlsetb[idx].func[0].pdis(p_cpu,p_st,dectlsetb[idx].func);
  else if (dectlsetb[idx].func[0].fmtinst)
    dump_gen(p_cpu,p_st,&dectlsetb[idx].func[0]);
  return 0;
}

int dump_vcvt(struct t_cpu *p_c,struct t_stat *p_st,struct t_decfun *p_f)
{
  int opc2,to_int,dp_op,sz,m,d,vd,op7,vm;

  opc2  = p_st->fields[ 2];   CHK(p_st,  2,"opc2",__LINE__);
  sz    = p_st->fields[ 3];   CHK(p_st,  3,"sz"  ,__LINE__);
  vm    = p_st->fields[ 5];   CHK(p_st,  5,"Vm"  ,__LINE__);
  vd    = p_st->fields[12];   CHK(p_st, 12,"Vd"  ,__LINE__);
  op7   = p_st->fields[14];   CHK(p_st, 14,"op7" ,__LINE__);
  m     = p_st->fields[11];   CHK(p_st, 11,"M"   ,__LINE__);
  d     = p_st->fields[10];   CHK(p_st, 10,"D"   ,__LINE__);

  to_int = (opc2 >> 2) & 1;
  dp_op = sz == 1;

  p_st->efields[0] = EFIELD("");
  if (to_int)
  {
//  uns = (opc2 & 1) == 0;
    if (op7 == 0)
      p_st->efields[0] = EFIELD("R");
      
    d = (vd << 1) | d;
    if (dp_op)
      m = (m << 3) | vm;
    else
      m = (vm << 1) | m;
  } else
  {
//  uns = op7 == 0;
    m = (vm << 1) | m;
    if (dp_op)
      d = (vd << 3) | vd;
    else
      d = (vd << 1) | d;
  }
  p_st->efields[1] = EFIELD(d);
  p_st->efields[2] = EFIELD(m);
  dump_gen(p_c,p_st,p_f);

  return 0;
}

int dump_vpush(struct t_cpu *p_c,struct t_stat *p_st,struct t_decfun *p_f)
{
  dump_gen(p_c,p_st,p_f);

  return 0;
}

int dump_vmovi(struct t_cpu *p_c,struct t_stat *p_st,struct t_decfun *p_f)
{
  int immf,imm4l,imm4h,imm,vd,d,dp_op,sz;

  imm4l = p_st->fields[ 5];   CHK(p_st,  5,"imm4L"  ,__LINE__);
  imm4h = p_st->fields[ 2];   CHK(p_st,  2,"imm4H"  ,__LINE__);
  vd    = p_st->fields[12];   CHK(p_st, 12,"Vd"     ,__LINE__);
  sz    = p_st->fields[ 3];   CHK(p_st,  3,"sz"     ,__LINE__);
  d     = p_st->fields[10];   CHK(p_st, 10,"D"      ,__LINE__);

  immf = 0;
  dp_op = sz == 1;
  imm = imm4h << 4 | imm4l;
  if (dp_op)
    ; // error("TODO dump_vmovi 64");
  else
  {
    d = (vd << 1) | d;
    immf = VFPExpandImm32(imm);
  }
  p_st->efields[1] = EFIELD(d);
  p_st->efields[2] = EFIELD(imm);
  p_st->efields[3] = EFIELD(immf);
  dump_gen(p_c,p_st,p_f);
  return 0;
}

int dump_vcmp(struct t_cpu *p_c,struct t_stat *p_st,struct t_decfun *p_f)
{
  int vd,vm,m,sz,d,dp_op;
  static char p2[10];

  vd  = p_st->fields[12];   CHK(p_st, 12,"Vd",__LINE__);
  vm  = p_st->fields[ 5];   CHK(p_st,  5,"Vm",__LINE__);
  sz  = p_st->fields[ 3];   CHK(p_st,  3,"sz",__LINE__);
  d   = p_st->fields[10];   CHK(p_st, 10,"D" ,__LINE__);
  m   = p_st->fields[11];   CHK(p_st, 11,"M" ,__LINE__);

  dp_op = sz == 1;

  p_st->efields[0] = EFIELD("");
  if (dp_op)
  {
    vd = d << 4 | vd;
    vm = m << 4 | vm;
  } else
  {
    vd = vd << 1 | d;
    vm = vm << 1 | m;
  }
  if (vm == 0)
  {
    p_st->efields[0] = EFIELD("E");
    strcpy(p2,"#0.0");
  } else
    sprintf(p2,"S%d",vm);
  p_st->efields[1] = EFIELD(vd);
  p_st->efields[2] = EFIELD(p2);
  dump_gen(p_c,p_st,p_f);
  return 0;
}

int dump_2r(struct t_cpu *p_c,struct t_stat *p_st,struct t_decfun *p_f)
{
  int vd,vm,m,sz,d,dp_op;

  vd  = p_st->fields[12];   CHK(p_st, 12,"Vd" ,__LINE__);
  vm  = p_st->fields[ 5];   CHK(p_st,  5,"Vm" ,__LINE__);
  sz  = p_st->fields[ 3];   CHK(p_st,  3,"sz" ,__LINE__);
  d   = p_st->fields[10];   CHK(p_st, 10,"D"  ,__LINE__);
  m   = p_st->fields[11];   CHK(p_st, 11,"M"  ,__LINE__);

  dp_op = sz == 1;

  if (dp_op)
  {
    vd = d << 4 | vd;
    vm = m << 4 | vm;
  } else
  {
    vd = vd << 1 | d;
    vm = vm << 1 | m;
  }
  p_st->efields[1] = EFIELD(vd);
  p_st->efields[2] = EFIELD(vm);
  dump_gen(p_c,p_st,p_f);

  return 0;
}

int dump_vstr(struct t_cpu *p_c,struct t_stat *p_st,struct t_decfun *p_f)
{
  int add,sw,d,u,vd,imm8,imm,singr;
  static char buf[40];

//rn   = p_st->fields[ 2];   CHK(p_st,  2,"rn"  ,__LINE__);
  vd   = p_st->fields[ 6];   CHK(p_st,  6,"vd"  ,__LINE__);
  sw   = p_st->fields[ 3];   CHK(p_st,  3,"sw"  ,__LINE__);
  d    = p_st->fields[ 4];   CHK(p_st,  4,"D"   ,__LINE__);
  u    = p_st->fields[ 5];   CHK(p_st,  5,"U"   ,__LINE__);
  imm8 = p_st->fields[ 7];   CHK(p_st,  7,"imm8",__LINE__);

  add = u == 1;
  if (sw == 11) // 64 bit
  {
    singr = 0;
    imm = imm8 << 2;
    vd = (d << 3) | vd;
  } else if (sw == 10) // 32 bit
  {
    singr = 1;
    imm = imm8 << 2;
    vd = (vd << 1) | d;
  }
//r = regr(p_c,rn,OP_EXEC);
  
  buf[0] = 0;
  if (imm)
  {
    if (add)
      sprintf(buf,", #%x",imm);
    else
      sprintf(buf,", -#%x",imm);
  }

  p_st->efields[0] = EFIELD(vd);
  p_st->efields[1] = EFIELD(buf);
  p_st->efields[2] = EFIELD(singr ? "S" : "D");
  dump_gen(p_c,p_st,p_f);

  return 0;
}

int dump_3r(struct t_cpu *p_c,struct t_stat *p_st,struct t_decfun *p_f)
{
  int vd,vn,vm,m,sz,d,n,dp_op;

  vn  = p_st->fields[ 2];   CHK(p_st,  2,"Vn" ,__LINE__);
  vd  = p_st->fields[12];   CHK(p_st, 12,"Vd" ,__LINE__);
  vm  = p_st->fields[ 5];   CHK(p_st,  5,"Vm" ,__LINE__);
  sz  = p_st->fields[ 3];   CHK(p_st,  3,"sz" ,__LINE__);
  d   = p_st->fields[10];   CHK(p_st, 10,"D"  ,__LINE__);
  n   = p_st->fields[14];   CHK(p_st, 14,"N"  ,__LINE__);
  m   = p_st->fields[11];   CHK(p_st, 11,"M"  ,__LINE__);

  dp_op = sz == 1;

  if (dp_op)
  {
    vd = d << 4 | vd;
    vn = n << 4 | vn;
    vm = m << 4 | vm;
  } else
  {
    vd = vd << 1 | d;
    vn = vn << 1 | n;
    vm = vm << 1 | m;
  }
  p_st->efields[1] = EFIELD(vd);
  p_st->efields[2] = EFIELD(vn);
  p_st->efields[3] = EFIELD(vm);
  dump_gen(p_c,p_st,p_f);

  return 0;
}

int dump_vmlx(struct t_cpu *p_c,struct t_stat *p_st,struct t_decfun *p_f)
{
  int op,vd,vn,vm,m,sz,d,n,dp_op,add;

  vn  = p_st->fields[ 2];   CHK(p_st,  2,"Vn" ,__LINE__);
  vd  = p_st->fields[12];   CHK(p_st, 12,"Vd" ,__LINE__);
  vm  = p_st->fields[ 5];   CHK(p_st,  5,"Vm" ,__LINE__);
  sz  = p_st->fields[ 3];   CHK(p_st,  3,"sz" ,__LINE__);
  d   = p_st->fields[10];   CHK(p_st, 10,"D"  ,__LINE__);
  n   = p_st->fields[14];   CHK(p_st, 14,"N"  ,__LINE__);
  m   = p_st->fields[11];   CHK(p_st, 11,"M"  ,__LINE__);
  op  = p_st->fields[15];   CHK(p_st, 15,"op6",__LINE__);

  dp_op = sz == 1;
  add   = op == 0;

  if (dp_op)
  {
    vd = d << 4 | vd;
    vn = n << 4 | vn;
    vm = m << 4 | vm;
  } else
  {
    vd = vd << 1 | d;
    vn = vn << 1 | n;
    vm = vm << 1 | m;
  }

  if (add)
    p_st->efields[0] = EFIELD("A");
  else
    p_st->efields[0] = EFIELD("S");
  p_st->efields[1] = EFIELD(vd);
  p_st->efields[2] = EFIELD(vn);
  p_st->efields[3] = EFIELD(vm);
  dump_gen(p_c,p_st,p_f);

  return 0;
}

int dump_vnmxx(struct t_cpu *p_c,struct t_stat *p_st,struct t_decfun *p_f)
{
  int op,vd,vn,vm,m,sz,d,n,dp_op,op8;

  vn  = p_st->fields[ 2];   CHK(p_st,  2,"Vn" ,__LINE__);
  vd  = p_st->fields[12];   CHK(p_st, 12,"Vd" ,__LINE__);
  vm  = p_st->fields[ 5];   CHK(p_st,  5,"Vm" ,__LINE__);
  sz  = p_st->fields[ 3];   CHK(p_st,  3,"sz" ,__LINE__);
  d   = p_st->fields[10];   CHK(p_st, 10,"D"  ,__LINE__);
  n   = p_st->fields[14];   CHK(p_st, 14,"N"  ,__LINE__);
  m   = p_st->fields[11];   CHK(p_st, 11,"M"  ,__LINE__);
  op  = p_st->fields[15];   CHK(p_st, 15,"op6",__LINE__);
  op8 = p_st->fields[16];   CHK(p_st, 16,"op8",__LINE__);

  dp_op = sz == 1;

  if (dp_op)
  {
    vd = d << 4 | vd;
    vn = n << 4 | vn;
    vm = m << 4 | vm;
  } else
  {
    vd = vd << 1 | d;
    vn = vn << 1 | n;
    vm = vm << 1 | m;
  }
  
  if (op8 == 1 && op == 1)
    p_st->efields[0] = EFIELD("LA");
  else if (op8 == 1 && op == 0)
    p_st->efields[0] = EFIELD("LS");
  else if (op8 == 2)
    p_st->efields[0] = EFIELD("UL");
  else
    p_st->efields[0] = EFIELD("??");

  p_st->efields[1] = EFIELD(vd);
  p_st->efields[2] = EFIELD(vn);
  p_st->efields[3] = EFIELD(vm);
  dump_gen(p_c,p_st,p_f);

  return 0;
}

int dump_vmovcr(struct t_cpu *p_c,struct t_stat *p_st,struct t_decfun *p_f)
{
/*
    1         1                     1         1
    5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
   |1 1 1 0|1 1 1 0|0 0 0|o| Vn    |  Rt   |1 0 1 0|N|0 0|1|0 0 0 0|
                          p
*/
  int n,vn,rn;

  vn  = p_st->fields[ 2];   CHK(p_st,  2,"Vn",__LINE__);
  n   = p_st->fields[14];   CHK(p_st, 14,"N" ,__LINE__);

  rn = vn << 1 | n;
  p_st->efields[0] = EFIELD(rn);
  dump_gen(p_c,p_st,p_f);

  return 0;
}

