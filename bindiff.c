#ifdef __INTER
#define FILE int
#else
#include <stdio.h>
#endif

#define NW (1024 * 16)

void diff(FILE *pf1,FILE *pf2)
{
  int buf1[NW];
  int buf2[NW];
  int i,n1,n2;

  while(1)
  {
    memset(buf1,0,sizeof buf1);
    memset(buf1,0,sizeof buf2);
    n1 = fread(buf1,sizeof buf1[0],NW,pf1);
    n2 = fread(buf2,sizeof buf2[0],NW,pf2);
    printf("read %d %d\n",n1,n2);
    if (n1 <= 0 || n2 <= 0)
      return;
    for (i = 0; i < n1; i++) 
      if (buf1[i] != buf2[i])
        printf("%06x %08x %08x\n",i * 4,buf1[i],buf2[i]);
  }
}

int main(int na,char **v)
{
  FILE *pf1;
  FILE *pf2;
  if (na < 3)
  {
    printf("bindiff file1 file2\n");
    exit(0);
  }
  pf1 = fopen(v[1],"rb");
  if (pf1)
  {
     pf2 = fopen(v[2],"rb");
     if (pf2)
     {
       diff(pf1,pf2);
       fclose(pf2);
     } else 
       printf("error opening %s\n",v[2]);
  } else
    printf("error opening %s\n",v[1]);
  return 0;
}
