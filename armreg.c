#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include "env.h"
#include "conf.h"
#include "disasm.h"
#include "dapreg.h"
#include "armreg.h"
#include "driversub.h"
#include "readpcapng-stlink.h"
#include "driver-cmsis.h"

#define MASK(reg,fn) (reg##_##fn##_MASK)
#define SHIFT(reg,fn)(reg##_##fn##_SHIFT)
#define FIELD(v,reg,fn) (((v) & MASK(reg,fn)) >> SHIFT(reg,fn))

static void outfield(const char *fmt,const char *fn,uint val,
                     char *buf,int lbuf,
                     int  lpre,int lmax,
                     char **p, int *pos)
{
  int lm = strlen(fn) + 12;
  int lm1 = lm; 
  if (*pos + lm >= lmax)
    lm1 = lm + lpre + 1;
  if (*p - buf + lm1 >= lbuf)
  {
    if (*p - buf + 2 < lbuf)
      strcpy(*p,"*"), *p += 2;
  } else
  {
    if (*pos + lm >= lmax)
    {
      sprintf(*p,"\n--%*.*s-- ",lpre-6,lpre-6,"");
      *p += strlen(*p);
      *pos = lpre; 
    }
    sprintf(*p,fmt,fn,val); 
    int l = strlen(*p);
    *p   += l;
    *pos += l; 
  }
}

#define OUTF(fn,off,mask) \
{ \
  if (fl || ((v >> (off)) & (mask))) \
    outfield("%s=%d ",#fn,(v >> (off)) & (mask),buf,lbuf,lpre,lmax,&p,&pos); \
}

#define OUTFX(reg,fn) \
{if (fl || FIELD(v,reg,fn)) outfield("%s=%d ",#fn,FIELD(v,reg,fn),buf,lbuf,lpre,lmax,&p,&pos); };

#define OUTF1(fn,off,mask,o) \
{if ((o == RW || o == op) && (fl || ((v >> (off)) & (mask)))) {\
  outfield("%s=%d ",#fn, (v >> (off)) & (mask),buf,lbuf,lpre,lmax,&p,&pos); }};

#define OUTF2(fn,off,mask,o,fmt) \
{if ((o == RW || o == op) && (fl || ((v >> (off)) & (mask)))) { \
  outfield("%s=" #fmt " ",#fn, (v >> (off)) & (mask),buf,lbuf,lpre,lmax,&p,&pos); }};

char *arm_getregname(int add,int fl)
{
  int i;
  static char buf[120];

  for (i = 0; i < nreg; i++)
    if (add == treg[i].add)
      break;
  if (i < nreg)
  {
    if (fl)
      sprintf(buf,"%08x %s",add,treg[i].name);
    else
      sprintf(buf,"%s",treg[i].name);
  } else
    sprintf(buf,"%08x ???",add);
  return buf;
}

char *fmtflags(struct t_cpu *p_cpu)
{
  static char buf[10];
  char *pb = buf;

  if (p_cpu->N)   *pb++ = 'N';
  else            *pb++ = '-';
  if (p_cpu->Z)   *pb++ = 'Z';
  else            *pb++ = '-';
  if (p_cpu->C)   *pb++ = 'C';
  else            *pb++ = '-';
  if (p_cpu->V)   *pb++ = 'V';
  else            *pb++ = '-';
  if (p_cpu->ISR) *pb++ = 'I';
  else            *pb++ = '-';
  *pb = 0;
  return buf;
}

void arm_setreg(uint regadd,uint val,int ty,int where,int log)
{
  struct t_reg *pr = getregadd(regadd);
  if (pr)
  {
    pr->val = val;
    if (ty == RO)
    {
      pr->valr = val;
      pr->countr++;
    } else if (ty == WO)
    {
      pr->valw = val;
      pr->countw++;
    } else
      printf("wrong type %d (%d %d)",ty,RO,WO);
    pr->wheren = where;
    if (pr->where1 == 0) 
      pr->where1 = where;
    if (log & 1)
      printf("setregval add %08x %s val %08x ty %d w %d\n",
             regadd,pr->name,val,ty,where);
  } else
    if (log & 3)
      printf("setregval add %08x not found\n",regadd);
}

void addregex(char *name,uint add,char *(*fdump)(struct t_reg *,char *,int lbuf,int fl,int op,int lpre,int lmax))
{
  int i;
  int l = 0;
  int h = nreg - 1;

//printf("addreg %08x nreg %d\n",add,nreg);
  while (l <= h)
  {
    i = (l + h) / 2;
    if (treg[i].add < add)
      l = i + 1;
    else if (treg[i].add > add)
      h = i - 1;
    else
    {
      printf("duplicate in addreg name %s add %08x prev %s\n",name,add,treg[i].name);
      if (name)
        treg[i].name = name;
      return;
    }
  }
  if (nreg == NREG)
  {
    static int first = 0;
    if (first == 0)
      printf("treg overflow %d\n",NREG);
    first++;
    return;
  }
//printf("addreg %08x l %d h %d\n",add,l,h);
  for (i = nreg - 1; i > h; i--)
    treg[i + 1] = treg[i];
  treg[l].name  = name;
  treg[l].add   = add;
  treg[l].fdump = fdump;
  nreg++;
//dumpreg(1);
}

void addreg(char *name,uint add)
{
  addregex(name,add,0);
}

void arm_dumpreg(uint add,int val,char *buf,int lbuf,uint fl)
{
  struct t_reg *pr = getregadd(add);
  if (pr)
  {
    pr->val = val;
    if (pr->fdump)
      pr->fdump(pr,buf,lbuf,fl,RO,73,130);
  }
}

struct t_reg *getregadd(int add)
{
  int i;
  int l = 0;
  int h = nreg - 1;

//printf("addreg %08x nreg %d\n",add,nreg);
  while (l <= h)
  {
    i = (l + h) / 2;
    if (treg[i].add < add)
      l = i + 1;
    else if (treg[i].add > add)
      h = i - 1;
    else
      return treg + i;
  }
  if (nreg == NREG)
  {
    static int first = 0;
    if (first == 0)
      printf("treg overflow %d\n",NREG);
    first++;
    return 0;
  }
//printf("addreg %08x l %d h %d\n",add,l,h);
  if (add < 0x50000000) // avoif flash and ram
    return 0;
  for (i = nreg - 1; i > h; i--)
    treg[i + 1] = treg[i];
  treg[l].name = "";
  treg[l].add  = add;
  nreg++;
  return treg + l;
}

struct t_reg *getreg(int add)
{
  int i;
  int l = 0;
  int h = nreg - 1;

//printf("addreg %08x nreg %d\n",add,nreg);
  while (l <= h)
  {
    i = (l + h) / 2;
    if (treg[i].add < add)
      l = i + 1;
    else if (treg[i].add > add)
      h = i - 1;
    else
      return treg + i;
  }
  return 0;
}

static char *dump_dcrsr(struct t_reg *pr,char *buf,int lbuf,int fl,int op,int lpre,int lmax)
{
  buf[0] = 0;
  char *p = buf;
  uint v = pr->val;

  if ((v >> 17) & 1)
    *p++ = 'W';
  else
    *p++ = 'R';
  *p++ = ' ';
  int r = v & 0x7f;
  if (r <= 15)
    sprintf(p,"R%02d",r);
  else if (r > 64)
    sprintf(p,"FP%02d",r - 64);
  else
    switch(r)
    {
      case 16: sprintf(p,"xPSR"); break;
      case 17: sprintf(p,"MSP"); break;
      case 18: sprintf(p,"PSP"); break;
      case 20: sprintf(p,"CONTROL+FAULTMASK+BASEPRI+PRIMASK"); break;
      case 33: sprintf(p,"FPSCR"); break;
      default: *p = 0;
    }

  return buf;
}

static char *dump_dhcsr(struct t_reg *pr,char *buf,int lbuf,int fl,int op,int lpre,int lmax)
{
  buf[0] = 0;
  char *p = buf;
  uint v = pr->val;

  int pos = lpre;
  OUTF2(DBGKEY,16,0xffff,WO,%x);
  OUTF1(S_RESET_ST,25,1,RO);
  OUTF1(S_RETIRE_ST,24,1,RO);
  OUTF1(S_LOCKUP,19,1,RO);
  OUTF1(S_SLEEP,18,1,RO);
  OUTF1(S_HALT,17,1,RO);
  OUTF1(S_REGRDY,16,1,RO);
  OUTF(C_SNAP_STALL,5,1);
  OUTF(C_MASKINT,3,1);
  OUTF(C_STEP,2,1);
  OUTF(C_HALT,1,1);
  OUTF(C_DEBUGEN,0,1);

  return buf;
}

static char *dump_dfsr(struct t_reg *pr,char *buf,int lbuf,int fl,int op,int lpre,int lmax)
{
  buf[0] = 0;
  char *p = buf;
  uint v = pr->val;

  int pos = lpre;
  OUTF(EXTERNAL,4,1);
  OUTF(VCATCH,3,1);
  OUTF(DWTTRAP,2,1);
  OUTF(BKPT,1,1);
  OUTF(HALTED,0,1);

  return buf;
}

static char *dump_fp_ctrl(struct t_reg *pr,char *buf,int lbuf,int fl,int op,int lpre,int lmax)
{
  buf[0] = 0;
  char *p = buf;
  uint v = pr->val;

  int pos = lpre;
  OUTFX(FP_CTRL,EN);
  OUTFX(FP_CTRL,KEY);
  OUTFX(FP_CTRL,NUM_CODE1);
  OUTFX(FP_CTRL,NUM_CODE2);
  OUTFX(FP_CTRL,NUM_LIT);
  OUTFX(FP_CTRL,REV);

  return buf;
}

static char *dump_cpuid(struct t_reg *pr,char *buf,int lbuf,int fl,int op,int lpre,int lmax)
{
  buf[0] = 0;
  char *p = buf;
  uint v = pr->val;

  int pos = lpre;
  OUTFX(CPUID,IMPL);
  OUTFX(CPUID,VARIANT);
  OUTFX(CPUID,CONSTANT);
  OUTFX(CPUID,PARTNO);
  OUTFX(CPUID,REVISION);

  return buf;
}

static char *dump_demcr(struct t_reg *pr,char *buf,int lbuf,int fl,int op,int lpre,int lmax)
{
  buf[0] = 0;
  char *p = buf;
  uint v = pr->val;

  int pos = lpre;
  OUTF(TRCENA,24,1);
  OUTF(MON_REQ,19,1);
  OUTF(MON_STEP,18,1);
  OUTF(MON_PEN,17,1);
  OUTF(MON_EN,16,1);
  OUTF(VC_HARDERR,10,1);
  OUTF(VC_INT,9,1);
  OUTF(VC_BUSERR,8,1);
  OUTF(VC_STATERR,7,1);
  OUTF(VC_CHKERR,6,1);
  OUTF(VC_NOCPERR,5,1);
  OUTF(VC_MEMERR,4,1);
  OUTF(VC_CORERESET,0,1);
  return buf;
}
#undef OUTF

char *dump_rom(struct t_reg *pr,char *buf,int lbuf,int fl,int op,int unused1,int unused2)
{
  if (pr->val)
    sprintf(buf,"-> %08x",(pr->val & ~3) + 0xe00ff000);
  return buf;
}

void arm_initreg(int log)
{
  nreg = 0;
  addreg  ("SCB_AIRCR",         0xe000ed0c);
  addreg  ("DBGMCU_IDCODE",     0xe0042000);
  addreg  ("ACTLR",             0xe000e008);
  addregex("CPUID",             CPUID_ADD, /* 0xe000ed00 */ dump_cpuid);
  addreg  ("ICSR",              0xe000ed04);
  addreg  ("VTOR",              0xe000ed08);
  addreg  ("SCR",               0xe000ed10);
  addreg  ("CCR",               0xe000ed14);
  addreg  ("SHPR1",             0xe000ed18);
  addreg  ("SHPR2",             0xe000ed1c);
  addreg  ("SHPR3",             0xe000ed20);
  addreg  ("SHCSR",             0xe000ed24);
  addreg  ("CFSR",              0xe000ed28);
  addreg  ("HFSR",              0xe000ed2c);
  addreg  ("MMAR",              0xe000ed34);
  addreg  ("BFAR",              0xe000ed38);
  addreg  ("AFSR",              0xe000ed3c);
  addregex("DEMCR",             DEMCR_ADD,dump_demcr);
  addregex("DFSR",              DFSR_ADD, dump_dfsr);
  addregex("DHCSR",             DHCSR_ADD,dump_dhcsr);
  addregex("DCRSR",             DCRSR_ADD,dump_dcrsr);
  addreg  ("DCRDR",             DCRDR_ADD);
  addregex("FP_CTRL",           FP_CTRL_ADD,dump_fp_ctrl);
  addreg  ("FP_REMAP",          FP_REMAP_ADD);
  addreg  ("FP_COMP0",          FP_COMPn_ADD(0));
  addreg  ("FP_COMP1",          FP_COMPn_ADD(1));
  addreg  ("FP_COMP2",          FP_COMPn_ADD(2));
  addreg  ("FP_COMP3",          FP_COMPn_ADD(3));
  addreg  ("FP_COMP4",          FP_COMPn_ADD(4));
  addreg  ("FP_COMP5",          FP_COMPn_ADD(5));
  addreg  ("FP_COMP6",          FP_COMPn_ADD(6));
  addreg  ("FP_COMP7",          FP_COMPn_ADD(7));
  addreg  ("FP_PID4",           0xe0002fd0);
  addreg  ("FP_PID5",           0xe0002fd4);
  addreg  ("FP_PID6",           0xe0002fd8);
  addreg  ("FP_PID7",           0xe0002fdc);
  addreg  ("FP_PID0",           0xe0002fe0);
  addreg  ("FP_PID1",           0xe0002fe4);
  addreg  ("FP_PID2",           0xe0002fe8);
  addreg  ("FP_PID3",           0xe0002fec);
  addreg  ("FP_CID0",           0xe0002ff0);
  addreg  ("FP_CID1",           0xe0002ff4);
  addreg  ("FP_CID2",           0xe0002ff8);
  addreg  ("FP_CID3",           0xe0002ffc);
  addreg  ("DWT_CTRL",          DWT_CTRL_ADD /* 0xe0001000 */);
  addreg  ("DWT_CYCCNT",        0xe0001004);
  addreg  ("DWT_CPICNT",        0xe0001008);
  addreg  ("DWT_EXCCNT",        0xe000100c);
  addreg  ("DWT_SLEEPCNT",      0xe0001010);
  addreg  ("DWT_LSUCNT",        0xe0001014);
  addreg  ("DWT_FOLDCNT",       0xe0001018);
  addreg  ("DWT_PCSR",          0xe000101c);
  addreg  ("DWT_COMP0",         0xe0001020);
  addreg  ("DWT_MASK0",         0xe0001024);
  addreg  ("DWT_FUNCTION0",     0xe0001028);
  addreg  ("DWT_COMP1",         0xe0001030);
  addreg  ("DWT_MASK1",         0xe0001034);
  addreg  ("DWT_FUNCTION1",     0xe0001038);
  addreg  ("DWT_COMP2",         0xe0001040);
  addreg  ("DWT_rMSK2",         0xe0001044);
  addreg  ("DWT_FUNCTION2",     0xe0001048);
  addreg  ("DWT_COMP3",         0xe0001050);
  addreg  ("DWT_MASK3",         0xe0001054);
  addreg  ("DWT_FUNCTION3",     0xe0001058);
  addreg  ("DWT_LAR",           0xe0001FB0);
  addreg  ("DWT_LSR",           0xe0001FB4);
  addreg  ("DWT_DEVARCH",       DWT_DEVARCH_ADD);
  addreg  ("DWT_PID4",          0xe0001fd0);
  addreg  ("DWT_PID5",          0xe0001fd4);
  addreg  ("DWT_PID6",          0xe0001fd8);
  addreg  ("DWT_PID7",          0xe0001fdc);
  addreg  ("DWT_PID0",          0xe0001fe0);
  addreg  ("DWT_PID1",          0xe0001fe4);
  addreg  ("DWT_PID2",          0xe0001fe8);
  addreg  ("DWT_PID3",          0xe0001fec);
  addreg  ("DWT_CID0",          0xe0001ff0);
  addreg  ("DWT_CID1",          0xe0001ff4);
  addreg  ("DWT_CID2",          0xe0001ff8);
  addreg  ("DWT_CID3",          0xe0001ffc);
  addreg  ("UDID0",             0x1fff7a10);
  addreg  ("UDID1",             0x1fff7a14);
  addreg  ("UDID2",             0x1fff7a18);
  addreg  ("FLASHSIZE",         0x1fff7a20);
  addreg  ("FPCCR",             0xe000ef34);
  addreg  ("FPCAR",             0xe000ef38);
  addreg  ("FPDSCR",            0xe000ef3c);
  addreg  ("MVFR0",             0xe000ef40);
  addreg  ("MVFR1",             0xe000ef44);
// Trace macrocell
  addreg  ("ITM_TRACE_PRIV",    0xe0000e40);
  addreg  ("ITM_TRACE_CONTR",   0xe0000e80);
  addreg  ("INTEGRATION_WRITE", 0xe0000ef8);
  addreg  ("ITM_TRACE_EN",      0xe0000e00);
  addreg  ("ITM_LOCK_ACC",      0xe0000fb0);
  addreg  ("ITM_LOCK_STS",      0xe0000fb4);
  addreg  ("ITM_PID4",          0xe0000fd0);
  addreg  ("ITM_PID5",          0xe0000fd4);
  addreg  ("ITM_PID6",          0xe0000fd8);
  addreg  ("ITM_PID7",          0xe0000fdc);
  addreg  ("ITM_PID0",          0xe0000fe0);
  addreg  ("ITM_PID1",          0xe0000fe4);
  addreg  ("ITM_PID2",          0xe0000fe8);
  addreg  ("ITM_PID3",          0xe0000fec);
  addreg  ("ITM_CID0",          0xe0000ff0);
  addreg  ("ITM_CID1",          0xe0000ff4);
  addreg  ("ITM_CID2",          0xe0000ff8);
  addreg  ("ITM_CID3",          0xe0000ffc);
  addreg  ("SCS_ID4",           0xe000efd0);
  addreg  ("SCS_ID0",           0xe000efe0);
  addreg  ("SCS_ID1",           0xe000efe4);
  addreg  ("SCS_ID2",           0xe000efe8);
  addreg  ("SCS_ID3",           0xe000efec);
  addreg  ("SCS_CID0",          0xe000eff0);
  addreg  ("SCS_CID1",          0xe000eff4);
  addreg  ("SCS_CID2",          0xe000eff8);
  addreg  ("SCS_CID3",          0xe000effc);
  addreg  ("TPIU_SSPSR",        0xe0040000);
  addreg  ("TPIU_CSPSR",        0xe0040004);
  addreg  ("TPIU_ACPR",         0xe0040010);
  addreg  ("TPIU_SPPR",         0xe00400f0);
  addreg  ("TPIU_FFSR",         0xe0040300);
  addreg  ("TPIU_FFCR",         0xe0040304);
  addreg  ("TPIU_FSCR",         0xe0040308);
  addreg  ("TPIU_TRIGGER",      0xe0040ee8);
  addreg  ("TPIU_FIFO0",        0xe0040eec);
  addreg  ("TPIU_IATBCTR2",     0xe0040ef0);
  addreg  ("TPIU_FIFO1",        0xe0040efc);
  addreg  ("TPIU_IATBCTR0",     0xe0040ef8);
  addreg  ("TPIU_ITCTRL",       0xe0040f00);
  addreg  ("TPIU_CLAIMSET",     0xe0040fa0);
  addreg  ("TPIU_CLAIMCLR",     0xe0040fa4);
  addreg  ("TPIU_DEVID",        0xe0040fc8);
  addreg  ("TPIU_DEVTYPE",      0xe0040fcc);
  addreg  ("TPIU_PID4",         0xe0040fd0);
  addreg  ("TPIU_PID5",         0xe0040fd4);
  addreg  ("TPIU_PID6",         0xe0040fd8);
  addreg  ("TPIU_PID7",         0xe0040fdc);
  addreg  ("TPIU_PID0",         0xe0040fe0);
  addreg  ("TPIU_PID1",         0xe0040fe4);
  addreg  ("TPIU_PID2",         0xe0040fe8);
  addreg  ("TPIU_PID3",         0xe0040fec);
  addreg  ("TPIU_CID0",         0xe0040ff0);
  addreg  ("TPIU_CID1",         0xe0040ff4);
  addreg  ("TPIU_CID2",         0xe0040ff8);
  addreg  ("TPIU_CID3",         0xe0040ffc);
  addreg  ("ETM_DEVTYPE",       0xe0041fcc);
  addreg  ("ETM_PID4",          0xe0041fd0);
  addreg  ("ETM_PID5",          0xe0041fd4);
  addreg  ("ETM_PID6",          0xe0041fd8);
  addreg  ("ETM_PID7",          0xe0041fdc);
  addreg  ("ETM_PID0",          0xe0041fe0);
  addreg  ("ETM_PID1",          0xe0041fe4);
  addreg  ("ETM_PID2",          0xe0041fe8);
  addreg  ("ETM_PID3",          0xe0041fec);
  addreg  ("ETM_CID0",          0xe0041ff0);
  addreg  ("ETM_CID1",          0xe0041ff4);
  addreg  ("ETM_CID2",          0xe0041ff8);
  addreg  ("ETM_CID3",          0xe0041ffc);
  addregex("ROM_SCS",           0xe00ff000,dump_rom);
  addregex("ROM_DWT",           0xe00ff004,dump_rom);
  addregex("ROM_FPB",           0xe00ff008,dump_rom);
  addregex("ROM_ITM",           0xe00ff00c,dump_rom);
  addregex("ROM_TPIU",          0xe00ff010,dump_rom);
  addregex("ROM_EMT",           0xe00ff014,dump_rom);
  addregex("ROM_END",           0xe00ff018,dump_rom);
  addreg  ("SYSTEM_ACCESS",     0xe00fffcc);
  addreg  ("ROM_PID4",          0xe00fffd0);
  addreg  ("ROM_PID0",          0xe00fffe0);
  addreg  ("ROM_PID1",          0xe00fffe4);
  addreg  ("ROM_PID2",          0xe00fffe8);
  addreg  ("ROM_PID3",          0xe00fffec);
  addreg  ("ROM_CID0",          0xe00ffff0);
  addreg  ("ROM_CID1",          0xe00ffff4);
  addreg  ("ROM_CID2",          0xe00ffff8);
  addreg  ("ROM_CID3",          0xe00ffffc);
// M7 cache
  addreg  ("CLIDR",             0xe000ed78);
  addreg  ("CTR",               0xe000ed7c);
  addreg  ("CCSIDR",            0xe000ed80);
  addreg  ("CSSELR",            0xe000ed84);

// DBGMCU stm32h
  addreg  ("DBGMCU_IDC",        0x5c001000);
  addreg  ("DBGMCU_CR",         0x5c001004);
  addreg  ("DBGMCU_APB3FZ1",    0x5c001034);
  addreg  ("DBGMCU_APB1H",      0x5c001044);
  addreg  ("DBGMCU_APB4",       0x5c001054);
  addreg  ("DBGMCU_PIDR4",      0x5c001fd0);
  addreg  ("DBGMCU_PIDR1",      0x5c001fe4);
  addreg  ("DBGMCU_PIDR2",      0x5c001fe8);
  addreg  ("DBGMCU_CIDR0",      0x5c001ff0);
  addreg  ("DBGMCU_CIDR2",      0x5c001ff8);
  addreg  ("DBGMCU_CIDR3",      0x5c001ffC);
// remapped
  addreg  ("DBGMCU_IDC",        0xe00e1000);
  addreg  ("DBGMCU_CR",         0xe00e1004);
  addreg  ("DBGMCU_APB3FZ1",    0xe00e1034);
  addreg  ("DBGMCU_APB1H",      0xe00e1044);
  addreg  ("DBGMCU_APB4",       0xe00e1054);
  addreg  ("DBGMCU_PIDR4",      0xe00e1fd0);
  addreg  ("DBGMCU_PIDR1",      0xe00e1fe4);
  addreg  ("DBGMCU_PIDR2",      0xe00e1fe8);
  addreg  ("DBGMCU_CIDR0",      0xe00e1ff0);
  addreg  ("DBGMCU_CIDR2",      0xe00e1ff8);
  addreg  ("DBGMCU_CIDR3",      0xe00e1ffC);
  if (log || nreg > NREG * 95 / 100)
    printf("nreg %d/%d\n",nreg,NREG);
}

void arm_dump_allreg(int fl)
{
  char buf[1000];
  int n = 0;
  for (int i = 0; i < nreg; i++)
    if (fl || treg[i].countr + treg[i].countw > 0)
    {
      n++;
      break;
    }
  if (n == 0) return;
  printf("ARM reg dump nreg %d/%d\n",nreg,NREG);
  printf("       name           add    #R   #W    valr      valw    when1  when(n)\n");
  printf("------------------ -------- ---- ---- --------  --------  ------ ------\n");
  for (int i = 0; i < nreg; i++)
    if (fl || treg[i].countr + treg[i].countw > 0)
    {
      printf("%-18s %08x %4d %4d %08x%c %08x%c %6u %6u ",
             treg[i].name,
             treg[i].add,
             treg[i].countr,
             treg[i].countw,
             treg[i].valr,
             treg[i].valr == treg[i].val ? '*' : ' ',
             treg[i].valw,
             treg[i].valw == treg[i].val ? '*' : ' ',
             treg[i].where1,
             treg[i].wheren);
      if (treg[i].fdump)
        printf("%s",treg[i].fdump(treg + i,buf,sizeof buf,1,RO,73,130));
      putchar('\n');
    }
}
