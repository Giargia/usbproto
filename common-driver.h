#ifndef _COMMON_DRIVER_H_
#define _COMMON_DRIVER_H_

struct t_stat;

struct disasm_par
{
  void *env;
  int  (*fread_mem)(struct disasm_par *par,uint pc,uchar *code,size_t len);
};

int  disasm(struct disasm_par *par,struct t_cpu *p_cpu,uint pc,struct t_stat *p_st);

#endif

