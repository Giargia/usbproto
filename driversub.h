#ifndef _DRIVERSUB_H_
#define _DRIVERSUB_H_

#include <stdbool.h>

#define CHKERR(err,info) if (err) {printf("error %d %s line %d\n",err,info,__LINE__); exit(1);}
#define CHKST(st)    {if (st != 0) {printf("CHKST  %d line %d\n",st,__LINE__); exit(0);}}
#define CHKSTR(st,l) {if (st != 0) {printf("CHKSTR %d line %d\n",st,l); return -1;}}

typedef unsigned int uint;
typedef unsigned char uchar;
typedef unsigned short ushort;

typedef uchar uchar;
typedef uint  uint;
typedef uint stm32_addr_t;

#define STLINK_ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))
#define STLINK_SERIAL_LENGTH             24
#define STLINK_SERIAL_BUFFER_SIZE        (STLINK_SERIAL_LENGTH + 1)

enum SCSI_Generic_Direction 
{
  SG_DXFER_TO_DEV=0, 
  SG_DXFER_FROM_DEV=0x80
};

enum connect_type 
{
  CONNECT_HOT_PLUG = 0,
  CONNECT_NORMAL = 1,
  CONNECT_UNDER_RESET = 2,
};

enum reset_type 
{
  RESET_AUTO = 0,
  RESET_HARD = 1,
  RESET_SOFT = 2,
  RESET_SOFT_AND_HALT = 3,
};

/* Statuses of core */
enum target_state 
{
  TARGET_UNKNOWN = 0,
  TARGET_RUNNING = 1,
  TARGET_HALTED = 2,
  TARGET_RESET = 3,
  TARGET_DEBUG_RUNNING = 4,
};

enum ugly_loglevel 
{
  UDEBUG = 90,
  UINFO  = 50,
  UWARN  = 30,
  UERROR = 20,
  UFATAL = 10
};

enum stlink_jtag_api_version 
{
  STLINK_JTAG_API_V1 = 1,
  STLINK_JTAG_API_V2,
  STLINK_JTAG_API_V3,
};

enum stm32_core_id 
{
  STM32_CORE_ID_M0_SWD        = 0x0bb11477,   // (RM0091 Section 32.5.3) F0 SW-DP
  STM32_CORE_ID_M0P_SWD       = 0x0bc11477,   // (RM0444 Section 40.5.3) G0 SW-DP
                                              // (RM0377 Section 27.5.3) L0 SW-DP
  STM32_CORE_ID_M3_r1p1_SWD   = 0x1ba01477,   // (RM0008 Section 31.8.3) F1 SW-DP
  STM32_CORE_ID_M3_r1p1_JTAG  = 0x3ba00477,   // (RM0008 Section 31.6.3) F1 JTAG
  STM32_CORE_ID_M3_r2p0_SWD   = 0x2ba01477,   // (RM0033 Section 32.8.3) F2 SW-DP
                                              // (RM0038 Section 30.8.3) L1 SW-DP
  STM32_CORE_ID_M3_r2p0_JTAG  = 0x0ba00477,   // (RM0033 Section 32.6.3) F2 JTAG
                                              // (RM0038 Section 30.6.2) L1 JTAG
  STM32_CORE_ID_M4_r0p1_SWD   = 0x1ba01477,   // (RM0316 Section 33.8.3) F3 SW-DP
                                              // (RM0351 Section 48.8.3) L4 SW-DP
                                              // (RM0432 Section 57.8.3) L4+ SW-DP
  STM32_CORE_ID_M4_r0p1_JTAG  = 0x4ba00477,   // (RM0316 Section 33.6.3) F3 JTAG
                                              // (RM0351 Section 48.6.3) L4 JTAG
                                              // (RM0432 Section 57.6.3) L4+ JTAG
  STM32_CORE_ID_M4F_r0p1_SWD  = 0x2ba01477,   // (RM0090 Section 38.8.3) F4 SW-DP
                                              // (RM0090 Section 47.8.3) G4 SW-DP
  STM32_CORE_ID_M4F_r0p1_JTAG = 0x4ba00477,   // (RM0090 Section 38.6.3) F4 JTAG
                                              // (RM0090 Section 47.6.3) G4 JTAG
  STM32_CORE_ID_M7F_SWD       = 0x5ba02477,   // (RM0385 Section 40.8.3) F7 SW-DP
                                              // (RM0473 Section 33.4.4) WB SW-DP
                                              // (RM0453 Section 38.4.1) WL SW-DP
  STM32_CORE_ID_M7F_JTAG      = 0x5ba00477,   // (RM0385 Section 40.6.3) F7 JTAG
  STM32_CORE_ID_M7F_M33_SWD   = 0x6ba02477,   // (RM0481 Section 58.3.3) H5 SW-DP
                                              // (RM0433 Section 60.4.1) H7 SW-DP
  STM32_CORE_ID_M7F_M33_JTAG  = 0x6ba00477,   // (RM0481 Section 58.3.1) H5 JTAG
                                              // (RM0433 Section 60.4.1) H7 JTAG
                                              // (RM0473 Section 33.4.1) WB JTAG
                                              // (RM0453 Section 38.3.8) WL JTAG
  STM32_CORE_ID_M33_SWD       = 0x0be02477,   // (RM0438 Section 52.2.10) L5 SW-DP
                                              // (RM0456 Section 65.3.3) U5 SW-DP
  STM32_CORE_ID_M33_JTAGD     = 0x0be01477,   // (RM0438 Section 52.2.10) L5 JTAG-DP
                                              // (RM0456 Section 65.3.3) U5 JTAG-DP
  STM32_CORE_ID_M33_JTAG      = 0x0ba04477,   // (RM0438 Section 52.2.8) L5 JTAG
                                              // (RM0456 Section 56.3.1) U5 JTAG
};

#define STM32_FLASH_L0_REGS_ADDR ((uint32_t) 0x40022000)
#define STM32_FLASH_Lx_REGS_ADDR ((uint32_t) 0x40023c00)
#define STM32_FLASH_Gx_REGS_ADDR ((uint32_t) 0x40022000)
#define STM32_FLASH_L5_REGS_ADDR ((uint32_t) 0x40022000)
#define STM32_FLASH_Gx_OPTR (STM32_FLASH_Gx_REGS_ADDR + 0x20)
#define STM32_FLASH_G4_OPTR_DBANK (22) /* FLASH option register FLASH_OPTR Dual-Bank Mode */
#define STM32_FLASH_L5_OPTR (STM32_FLASH_L5_REGS_ADDR + 0x40)


#ifdef XX
enum stlink_flash_type
{
  STLINK_FLASH_TYPE_UNKNOWN = 0,
  STLINK_FLASH_TYPE_F0,    // used by f0, f1 (except f1xl),f3. 
  STLINK_FLASH_TYPE_F1_XL, // f0 flash with dual bank, apparently 
  STLINK_FLASH_TYPE_F4,    // used by f2, f4 
  STLINK_FLASH_TYPE_F7,
  STLINK_FLASH_TYPE_L0,    // l0, l1 
  STLINK_FLASH_TYPE_L4,    // l4, l4+ 
  STLINK_FLASH_TYPE_G0,
  STLINK_FLASH_TYPE_G4,
  STLINK_FLASH_TYPE_WB,
  STLINK_FLASH_TYPE_H7,
  STLINK_FLASH_TYPE_MAX,
};
#endif

/* STM32 flash types */
enum stm32_flash_type 
{
  STM32_FLASH_TYPE_UNKNOWN   =  0,
  STM32_FLASH_TYPE_C0        =  1,
  STM32_FLASH_TYPE_F0_F1_F3  =  2,
  STM32_FLASH_TYPE_F1_XL     =  3,
  STM32_FLASH_TYPE_F2_F4     =  4,
  STM32_FLASH_TYPE_F7        =  5,
  STM32_FLASH_TYPE_G0        =  6,
  STM32_FLASH_TYPE_G4        =  7,
  STM32_FLASH_TYPE_H7        =  8,
  STM32_FLASH_TYPE_L0_L1     =  9,
  STM32_FLASH_TYPE_L4        = 10,
  STM32_FLASH_TYPE_L5_U5_H5  = 11,
  STM32_FLASH_TYPE_WB_WL     = 12,
};

/* STM32 chip-ids */
// See DBGMCU_IDCODE register (0xe0042000) in appropriate programming manual
// stm32 chipids, only lower 12 bits...

enum stm32_chipids 
{
  STM32_CHIPID_UNKNOWN          = 0x000,

  STM32_CHIPID_F1_MD            = 0x410, /* medium density */
  STM32_CHIPID_F2               = 0x411,
  STM32_CHIPID_F1_LD            = 0x412, /* low density */
  STM32_CHIPID_F4               = 0x413,
  STM32_CHIPID_F1_HD            = 0x414, /* high density */
  STM32_CHIPID_L4               = 0x415,
  STM32_CHIPID_L1_MD            = 0x416, /* medium density */
  STM32_CHIPID_L0_CAT3          = 0x417,
  STM32_CHIPID_F1_CONN          = 0x418, /* connectivity line */
  STM32_CHIPID_F4_HD            = 0x419, /* high density */
  STM32_CHIPID_F1_VL_MD_LD      = 0x420, /* value line medium & low density */
  STM32_CHIPID_F446             = 0x421,
  STM32_CHIPID_F3               = 0x422,
  STM32_CHIPID_F4_LP            = 0x423,
  STM32_CHIPID_L0_CAT2          = 0x425,
  STM32_CHIPID_L1_MD_PLUS       = 0x427, /* medium density plus */
  STM32_CHIPID_F1_VL_HD         = 0x428, /* value line high density */
  STM32_CHIPID_L1_CAT2          = 0x429,
  STM32_CHIPID_F1_XLD           = 0x430, /* extra low density plus */
  STM32_CHIPID_F411xx           = 0x431,
  STM32_CHIPID_F37x             = 0x432,
  STM32_CHIPID_F4_DE            = 0x433,
  STM32_CHIPID_F4_DSI           = 0x434,
  STM32_CHIPID_L43x_L44x        = 0x435,
  STM32_CHIPID_L1_MD_PLUS_HD    = 0x436, /* medium density plus & high density */
  STM32_CHIPID_L152_RE          = 0x437,
  STM32_CHIPID_F334             = 0x438,
  STM32_CHIPID_F3xx_SMALL       = 0x439,
  STM32_CHIPID_F0               = 0x440,
  STM32_CHIPID_F412             = 0x441,
  STM32_CHIPID_F09x             = 0x442,
  STM32_CHIPID_C011xx           = 0x443, /* RM0490 (revision 5), section 30.10.1 "DBG device ID code register (DBG_IDCODE)" */
  STM32_CHIPID_F0xx_SMALL       = 0x444,
  STM32_CHIPID_F04              = 0x445,
  STM32_CHIPID_F303_HD          = 0x446, /* high density */
  STM32_CHIPID_L0_CAT5          = 0x447,
  STM32_CHIPID_F0_CAN           = 0x448,
  STM32_CHIPID_F7               = 0x449, /* Nucleo F746ZG board */
  STM32_CHIPID_C051xx           = 0x44C, /* RM0490 (revision 5), section 30.10.1 "DBG device ID code register (DBG_IDCODE)" */
  STM32_CHIPID_C091xx_C92xx     = 0x44D, /* RM0490 (revision 5), section 30.10.1 "DBG device ID code register (DBG_IDCODE)" */
  STM32_CHIPID_H74xxx           = 0x450, /* RM0433, p.3189 */
  STM32_CHIPID_F76xxx           = 0x451,
  STM32_CHIPID_F72xxx           = 0x452, /* Nucleo F722ZE board */
  STM32_CHIPID_C031xx           = 0x453, /* RM0490 (revision 5), section 30.10.1 "DBG device ID code register (DBG_IDCODE)" */
  STM32_CHIPID_U535_U545        = 0x455, /* RM0456, p.3604 */
  STM32_CHIPID_G0_CAT4          = 0x456, /* G051/G061 */
  STM32_CHIPID_L0_CAT1          = 0x457,
  STM32_CHIPID_F410             = 0x458,
  STM32_CHIPID_U031xx           = 0x459,
  STM32_CHIPID_G0_CAT2          = 0x460, /* G07x/G08x */
  STM32_CHIPID_L496x_L4A6x      = 0x461,
  STM32_CHIPID_L45x_L46x        = 0x462,
  STM32_CHIPID_F413             = 0x463,
  STM32_CHIPID_L41x_L42x        = 0x464,
  STM32_CHIPID_G0_CAT1          = 0x466, /* G03x/G04x */
  STM32_CHIPID_G0_CAT3          = 0x467, /* G0Bx/G0Cx */
  STM32_CHIPID_G4_CAT2          = 0x468, /* RM0440, section 46.6.1 "MCU device ID code" */
  STM32_CHIPID_G4_CAT3          = 0x469,
  STM32_CHIPID_L4Rx             = 0x470, /* RM0432, p.2247, found on the STM32L4R9I-DISCO board */
  STM32_CHIPID_L4PX             = 0x471, /* RM0432, p.2247 */
  STM32_CHIPID_L5x2xx           = 0x472, /* RM0438, p.2157 */
  STM32_CHIPID_U5Fx_U5Gx        = 0x476, /* RM0456, p.3604 */
  STM32_CHIPID_G4_CAT4          = 0x479,
  STM32_CHIPID_H7Ax             = 0x480, /* RM0455, p.2863 */
  STM32_CHIPID_U59x_U5Ax        = 0x481, /* RM0456, p.3604 */
  STM32_CHIPID_U575_U585        = 0x482, /* RM0456, p.3604 */
  STM32_CHIPID_H72x             = 0x483, /* RM0468, p.3199 */
  STM32_CHIPID_H5xx             = 0x484, /* RM0481, p.3085 */
  STM32_CHIPID_U073xx_U083xx    = 0x489,
  STM32_CHIPID_C071xx           = 0x493, /* RM0490 (revision 5), section 30.10.1 "DBG device ID code register (DBG_IDCODE)" */
  STM32_CHIPID_WB55             = 0x495,
  STM32_CHIPID_WLE              = 0x497,
};

enum run_type 
{
  RUN_NORMAL = 0,
  RUN_FLASH_LOADER = 1,
};

#define CODE_BREAK_NUM_MAX   15
#define CODE_BREAK_LOW     0x01
#define CODE_BREAK_HIGH    0x02
#define CODE_BREAK_REMAP   0x04
#define CODE_BREAK_REV_V1  0x00
#define CODE_BREAK_REV_V2  0x01

/* Error code */
#define STLINK_DEBUG_ERR_OK              0x80
#define STLINK_DEBUG_ERR_FAULT           0x81
#define STLINK_DEBUG_ERR_WRITE           0x0c
#define STLINK_DEBUG_ERR_WRITE_VERIFY    0x0d
#define STLINK_DEBUG_ERR_AP_WAIT         0x10
#define STLINK_DEBUG_ERR_AP_FAULT        0x11
#define STLINK_DEBUG_ERR_AP_ERROR        0x12
#define STLINK_DEBUG_ERR_DP_WAIT         0x14
#define STLINK_DEBUG_ERR_DP_FAULT        0x15
#define STLINK_DEBUG_ERR_DP_ERROR        0x16

#define CMD_CHECK_NO         0
#define CMD_CHECK_REP_LEN    1
#define CMD_CHECK_STATUS     2
#define CMD_CHECK_RETRY      3 /* check status and retry if wait error */

#define STLINK_V3_MAX_FREQ_NB            10

#define STLINK_V1_USB_PID(pid) ((pid) == STLINK_USB_PID_STLINK)

#define STLINK_V2_USB_PID(pid) ((pid) == STLINK_USB_PID_STLINK_32L || \
                                (pid) == STLINK_USB_PID_STLINK_32L_AUDIO || \
                                (pid) == STLINK_USB_PID_STLINK_NUCLEO)

#define STLINK_V2_1_USB_PID(pid) ((pid) == STLINK_USB_PID_STLINK_V2_1)

#define STLINK_V3_USB_PID(pid) ((pid) == STLINK_USB_PID_STLINK_V3_USBLOADER || \
                                (pid) == STLINK_USB_PID_STLINK_V3E_PID || \
                                (pid) == STLINK_USB_PID_STLINK_V3S_PID || \
                                (pid) == STLINK_USB_PID_STLINK_V3_2VCP_PID || \
                                (pid) == STLINK_USB_PID_STLINK_V3_NO_MSD_PID)

#define STLINK_SUPPORTED_USB_PID(pid) (STLINK_V1_USB_PID(pid) || \
                                       STLINK_V2_USB_PID(pid) || \
                                       STLINK_V2_1_USB_PID(pid) || \
                                       STLINK_V3_USB_PID(pid))

/* Baud rate divisors for SWDCLK */
#define STLINK_SWDCLK_4MHZ_DIVISOR        0
#define STLINK_SWDCLK_1P8MHZ_DIVISOR      1
#define STLINK_SWDCLK_1P2MHZ_DIVISOR      2
#define STLINK_SWDCLK_950KHZ_DIVISOR      3
#define STLINK_SWDCLK_480KHZ_DIVISOR      7
#define STLINK_SWDCLK_240KHZ_DIVISOR     15
#define STLINK_SWDCLK_125KHZ_DIVISOR     31
#define STLINK_SWDCLK_100KHZ_DIVISOR     40
#define STLINK_SWDCLK_50KHZ_DIVISOR      79
#define STLINK_SWDCLK_25KHZ_DIVISOR     158
#define STLINK_SWDCLK_15KHZ_DIVISOR     265
#define STLINK_SWDCLK_5KHZ_DIVISOR      798

/* NRST pin states */
#define STLINK_DEBUG_APIV2_DRIVE_NRST_LOW  0x00
#define STLINK_DEBUG_APIV2_DRIVE_NRST_HIGH 0x01

#define STLINK_USB_VID_ST                   0x0483
#define STLINK_USB_PID_STLINK               0x3744
#define STLINK_USB_PID_STLINK_32L           0x3748
#define STLINK_USB_PID_STLINK_32L_AUDIO     0x374a
#define STLINK_USB_PID_STLINK_NUCLEO        0x374b
#define STLINK_USB_PID_STLINK_V2_1          0x3752
#define STLINK_USB_PID_STLINK_V3_USBLOADER  0x374d
#define STLINK_USB_PID_STLINK_V3E_PID       0x374e
#define STLINK_USB_PID_STLINK_V3S_PID       0x374f
#define STLINK_USB_PID_STLINK_V3_2VCP_PID   0x3753
#define STLINK_USB_PID_STLINK_V3_NO_MSD_PID 0x3754
#define STLINK_USB_PID_STLINK_V3P           0x3757

#define STLINK_USB_VID_ST             0x0483
#define STLINK_USB_PID_STLINK_32L     0x3748
#define STLINK_USB_PID_STLINK_NUCLEO  0x374b

#define STLINK_USB_PID_STLINK         0x3744
#define STLINK_USB_PID_STLINK_32L     0x3748
#define STLINK_USB_PID_STLINK_NUCLEO  0x374b

#define STLINK_CORE_STAT_UNKNOWN          -1

#define STLINK_SG_SIZE                    31
#define STLINK_CMD_SIZE                   16

#define STLINK_DEV_DFU_MODE             0x00
#define STLINK_DEV_MASS_MODE            0x01
#define STLINK_DEV_DEBUG_MODE           0x02
#define STLINK_DEV_UNKNOWN_MODE -1

#define STLINK_CORE_RUNNING             0x80
#define STLINK_CORE_HALTED              0x81
#define STLINK_CORE_STAT_UNKNOWN          -1

#define STLINK_GET_VERSION              0xf1
#define STLINK_GET_CURRENT_MODE         0xf5
#define STLINK_GET_TARGET_VOLTAGE       0xf7

#define STLINK_DEBUG_COMMAND            0xf2
#define STLINK_DFU_COMMAND              0xf3
#define STLINK_DEBUG_APIV1_RESETSYS     0x03
#define STLINK_DEBUG_APIV1_READALLREGS  0x04
#define STLINK_DEBUG_APIV1_READREG      0x05
#define STLINK_DEBUG_APIV1_WRITEREG     0x06
#define STLINK_DFU_EXIT                 0x07
#define STLINK_DEBUG_APIV1_ENTER        0x20
// #define STLINK_SWD_ENTER                0x30
#define STLINK_DEBUG_APIV2_ENTER        0x30
#define STLINK_DEBUG_APIV2_READ_IDCODES 0x31
// #define STLINK_SWD_READCOREID           0x32  // TBD ????

#define STLINK_DEBUG_APIV2_RESETSYS     0x32
#define STLINK_DEBUG_APIV2_READREG      0x33
#define STLINK_DEBUG_APIV2_WRITEREG     0x34
//#define STLINK_JTAG_WRITEDEBUG_32BIT    0x35 // old
#define STLINK_DEBUG_APIV2_WRITEDEBUGREG    0x35
//#define STLINK_JTAG_READDEBUG_32BIT     0x36 // old
#define STLINK_DEBUG_APIV2_READDEBUGREG 0x36
#define STLINK_DEBUG_APIV2_READALLREGS  0x3a
#define STLINK_JTAG_DRIVE_NRST          0x3c
#define STLINK_DEBUG_APIV2_DRIVE_NRST   0x3c
#define STLINK_DEBUG_APIV2_GETLASTRWSTATUS  0x3b
#define STLINK_DEBUG_APIV2_GETLASTRWSTATUS2 0x3e

#define STLINK_DEBUG_APIV2_SWD_SET_FREQ 0x43
#define STLINK_DEBUG_APIV3_SET_COM_FREQ 0x61
#define STLINK_DEBUG_APIV3_GET_COM_FREQ 0x62

#define STLINK_REG_CM3_CPUID    0xe000ed00
#define STLINK_REG_CM3_FP_CTRL  0xe0002000
#define STLINK_REG_CM3_FP_COMP0 0xe0002008

/* Cortex core ids */
#define STM32VL_CORE_ID 0x1ba01477
#define STM32F7_CORE_ID 0x5ba02477
#define STM32H7_CORE_ID 0x6ba02477      // STM32H7 SWD ID Code
#define STM32H7_CORE_ID_JTAG 0x6ba00477 // STM32H7 JTAG ID Code (RM0433 pg3065)

/* Constant STM32 memory map figures */
#define STM32_SRAM_BASE ((uint)0x20000000)
#define STM32_FLASH_BASE ((uint)0x08000000)
#define STM32_F1_FLASH_BANK2_BASE ((uint)0x08080000)
#define STM32_H7_FLASH_BANK2_BASE ((uint)0x08100000)

#define STM32_F2_OPTION_BYTES_BASE ((uint)0x1FFFC000)
#define STM32_F4_OPTION_BYTES_BASE ((uint)0x40023C14)
#define STM32_F7_OPTION_BYTES_BASE ((uint)0x1FFF0000)
#define STM32_H7_OPTION_BYTES_BASE ((uint)0x5200201C)

#define STM32_G0_OPTION_BYTES_BASE ((uint)0x1FFF7800)
#define STM32_L4_OPTION_BYTES_BASE ((uint)0x1FFF7800)

#define STM32_L0_OPTION_BYTES_BASE ((uint)0x1FF80000)
#define STM32_L1_OPTION_BYTES_BASE ((uint)0x1FF80000)

#define STM32_F0_OPTION_BYTES_BASE ((uint)0x1FFFF800)
#define STM32_F1_OPTION_BYTES_BASE ((uint)0x1FFFF800)
#define STM32_F3_OPTION_BYTES_BASE ((uint)0x1FFFF800)
#define STM32_G4_OPTION_BYTES_BASE ((uint)0x1FFFF800)

/* Debug Halting Control and Status Register */
/* Cortex™-M3 Technical Reference Manual */
/* Debug Halting Control and Status Register */
// Arm v7-M Architecture Reference Manual
#define STLINK_REG_DFSR                     0xe000ed30
#define STLINK_REG_DFSR_HALT                (1 << 0)
#define STLINK_REG_DFSR_BKPT                (1 << 1)
#define STLINK_REG_DFSR_VCATCH              (1 << 3)
#define STLINK_REG_DFSR_EXTERNAL            (1 << 4)
#define STLINK_REG_DFSR_CLEAR               0x0000001F
#define STLINK_REG_DHCSR                    0xe000edf0
#define STLINK_REG_DHCSR_DBGKEY             (0xa05f << 16)
#define STLINK_REG_DHCSR_C_DEBUGEN          (1 << 0)
#define STLINK_REG_DHCSR_C_HALT             (1 << 1)
#define STLINK_REG_DHCSR_C_STEP             (1 << 2)
#define STLINK_REG_DHCSR_C_MASKINTS         (1 << 3)
#define STLINK_REG_DHCSR_S_REGRDY           (1 << 16)
#define STLINK_REG_DHCSR_S_HALT             (1 << 17)
#define STLINK_REG_DHCSR_S_SLEEP            (1 << 18)
#define STLINK_REG_DHCSR_S_LOCKUP           (1 << 19)
#define STLINK_REG_DHCSR_S_RETIRE_ST        (1 << 24)
#define STLINK_REG_DHCSR_S_RESET_ST         (1 << 25)
#define STLINK_REG_DCRSR                    0xe000edf4
#define STLINK_REG_DCRDR                    0xe000edf8
#define STLINK_REG_DEMCR                    0xe000edfc
#define STLINK_REG_DEMCR_TRCENA             (1 << 24)

#define STLINK_REG_CM3_DEMCR                0xe000edfc
#define STLINK_REG_CM3_DEMCR_TRCENA         (1 << 24)
#define STLINK_REG_CM3_DEMCR_VC_HARDERR     (1 << 10)
#define STLINK_REG_CM3_DEMCR_VC_BUSERR      (1 << 8)
#define STLINK_REG_CM3_DEMCR_VC_CORERESET   (1 << 0)
#define STLINK_REG_CM3_DWT_COMPn(n)         (0xe0001020 + n*16)
#define STLINK_REG_CM3_DWT_MASKn(n)         (0xe0001024 + n*16)
#define STLINK_REG_CM3_DWT_FUNn(n)          (0xe0001028 + n*16)

/* Application Interrupt and Reset Control Register */
#define STLINK_REG_AIRCR                    0xe000ed0c
#define STLINK_REG_AIRCR_VECTKEY            0x05fa0000
#define STLINK_REG_AIRCR_SYSRESETREQ        0x00000004
#define STLINK_REG_AIRCR_VECTRESET          0x00000001

#define STM32F0_DBGMCU_CR 0xe0042004
#define STM32F0_DBGMCU_CR_IWDG_STOP 8
#define STM32F0_DBGMCU_CR_WWDG_STOP 9

#define STM32L1_DBGMCU_APB1_FZ 0xe0042008
#define STM32L1_DBGMCU_APB1_FZ_WWDG_STOP 11
#define STM32L1_DBGMCU_APB1_FZ_IWDG_STOP 12

#define STM32F4_DBGMCU_APB1FZR1 0xe0042008
#define STM32F4_DBGMCU_APB1FZR1_WWDG_STOP 11
#define STM32F4_DBGMCU_APB1FZR1_IWDG_STOP 12

#define STM32L0_DBGMCU_APB1_FZ 0x40015808
#define STM32L0_DBGMCU_APB1_FZ_WWDG_STOP 11
#define STM32L0_DBGMCU_APB1_FZ_IWDG_STOP 12

#define STM32H7_DBGMCU_APB1HFZ 0x5c001054
#define STM32H7_DBGMCU_APB1HFZ_IWDG_STOP 18

#define STM32WB_DBGMCU_APB1FZR1 0xe004203C
#define STM32WB_DBGMCU_APB1FZR1_WWDG_STOP 11
#define STM32WB_DBGMCU_APB1FZR1_IWDG_STOP 12

#define STM32Gx_FLASH_REGS_ADDR ((uint)0x40022000)
#define STM32Gx_FLASH_ACR (STM32Gx_FLASH_REGS_ADDR + 0x00)
#define STM32Gx_FLASH_KEYR (STM32Gx_FLASH_REGS_ADDR + 0x08)
#define STM32Gx_FLASH_OPTKEYR (STM32Gx_FLASH_REGS_ADDR + 0x0c)
#define STM32Gx_FLASH_SR (STM32Gx_FLASH_REGS_ADDR + 0x10)
#define STM32Gx_FLASH_CR (STM32Gx_FLASH_REGS_ADDR + 0x14)
#define STM32Gx_FLASH_ECCR (STM32Gx_FLASH_REGS_ADDR + 0x18)
#define STM32Gx_FLASH_OPTR (STM32Gx_FLASH_REGS_ADDR + 0x20)
#define STM32G0_FLASH_REGS_ADDR (STM32Gx_FLASH_REGS_ADDR)
#define STM32G4_FLASH_REGS_ADDR (STM32Gx_FLASH_REGS_ADDR)

// G4 FLASH option register
#define STM32G4_FLASH_OPTR_DBANK (22) /* FLASH_OPTR Dual Bank Mode */

#define STM32_REG_CMx_CPUID_PARTNO_CM0     0xc20
#define STM32_REG_CMx_CPUID_PARTNO_CM0P    0xc60
#define STM32_REG_CMx_CPUID_PARTNO_CM3     0xc23
#define STM32_REG_CMx_CPUID_PARTNO_CM4     0xc24
#define STM32_REG_CMx_CPUID_PARTNO_CM7     0xc27
#define STM32_REG_CMx_CPUID_PARTNO_CM33    0xd21
#define STM32_REG_CMx_CPUID_IMPL_ARM       0x41

/* ARM Cortex-M7 Processor Technical Reference Manual */
/* Cache Control and Status Register */
#define STLINK_REG_CM7_CTR                  0xe000ed7C
#define STLINK_REG_CM7_CLIDR                0xe000ed78
#define STLINK_REG_CM7_CCR                  0xe000ed14
#define STLINK_REG_CM7_CCR_DC               (1 << 16)
#define STLINK_REG_CM7_CCR_IC               (1 << 17)
#define STLINK_REG_CM7_CSSELR               0xe000ed84
#define STLINK_REG_CM7_DCCSW                0xe000ef6C
#define STLINK_REG_CM7_ICIALLU              0xe000ef50
#define STLINK_REG_CM7_CCSIDR               0xe000ed80

#define STLINK_REG_CM3_FP_COMPn(n)          (0xe0002008 + n*4)
#define STLINK_REG_CM7_FP_LAR               0xe0000FB0
#define STLINK_REG_CM7_FP_LAR_KEY           0xc5acce55

// SCB
#define NVIC_BASE   0xe000e000
#define SCB_BASE    0xe000ed00

#define SCB_CPUID_OFF                        0x00
#define SCB_ICSR_OFF                         0x04
#define SCB_VTOR_OFF                         0x08
#define SCB_AIRCR_OFF                        0x0c
#define SCB_SCR_OFF                          0x10
#define SCB_CCR_OFF                          0x14
#define SCB_SHPR1_OFF                        0x18
#define SCB_SHPR2_OFF                        0x1c
#define SCB_SHPR3_OFF                        0x20
#define SCB_SHCSR_OFF                        0x24
#define SCB_CFSR_OFF                         0x28
#define SCB_HFSR_OFF                         0x2c
#define SCB_DFSR_OFF                         0x30
#define SCB_MMFAR_OFF                        0x34
#define SCB_BFAR_OFF                         0x38
#define SCB_AFSR_OFF                         0x3c
#define SCB_CPACR_OFF                        0x88
#define SCB_NSACR_OFF                        0x8c

#define CHIP_F_HAS_DUAL_BANK    (1 << 0)
#define CHIP_F_HAS_SWO_TRACING  (1 << 1)

#define ELOG printf
#ifdef D
#define DLOG(sl,...) if (sl->backend_data->log & 1) printf(__VA_ARGS__);
#else
#define DLOG(sl,...) 
#endif
#define WLOG printf
#define ILOG printf

struct cache_level_desc 
{
  uint nsets;
  uint nways;
  uint log2_nways;
  uint width;
};

struct cache_desc_t 
{
  unsigned used;

// minimal line size in bytes
  uint dminline;
  uint iminline;

// last level of unification (uniprocessor)
  uint louu;

  struct cache_level_desc icache[7];
  struct cache_level_desc dcache[7];
};

struct code_hw_breakpoint 
{
  stm32_addr_t addr;
  int type;
};

typedef struct _cortex_m3_cpuid_ 
{
  uint16_t implementer_id;
  uint16_t variant;
  uint16_t part;
  uchar  revision;
} cortex_m3_cpuid_t;

struct stlink_reg 
{
  uint  r[16];
  uint  s[32];
  uint  xpsr;
  uint  main_sp;
  uint  process_sp;
  uint  rw;
  uint  rw2;
  uchar control;
  uchar faultmask;
  uchar basepri;
  uchar primask;
  uint  fpscr;
};

typedef struct stlink_version_ 
{
  uint stlink_v;
  uint jtag_v;
  uint swim_v;
  uint st_vid;
  uint stlink_pid;
  // jtag api version supported
  enum stlink_jtag_api_version jtag_api;
  // one bit for each feature supported. See macros STLINK_F_*
  uint flags;
} stlink_version_t;

typedef struct 
{
  struct stlink_libusb *backend_data;
  int    core_stat;
  int    serial_size;
  char   serial[20];
  int    core_id;
  int    q_len;
  uint   chip_id;
  int    flash_pgsz;
  int    flash_type;
  int    flash_base;
  int    sram_size;
  int    sram_base;
  int    flash_size;
  int    sys_size;
  int    sys_base;
  int    verbose;
  int    freq;
  uchar  c_buf[32];
  uchar  q_buf[1024*8];
  stlink_version_t version; 
  int    option_base;
  int    option_size;
  int    chip_flags;
  struct t_cpu *p_cpu;
  int    otp_base;
  int    otp_size;
  void   *priv;
} stlink_t;

struct stlink_libusb 
{
  libusb_context* libusb_ctx;
  libusb_device_handle* usb_handle;
  uint ep_req;
  uint ep_rep;
  uint ep_trace;
  int  protocoll;
  uint sg_transfer_idx;
  uint cmd_len;
  int  log;
  int  prev_terminate;
  stlink_t *sl;
};

ssize_t send_recv(struct stlink_libusb* handle, int terminate,
        uchar* txbuf, size_t txsize,
        uchar* rxbuf, size_t rxsize,int check_error,const char *name,int line);

int _stlink_usb_current_mode(stlink_t *sl);
int _stlink_usb_exit_dfu_mode(stlink_t *sl); 
int _stlink_usb_enter_swd_mode(stlink_t *sl);
int _stlink_usb_version(stlink_t *sl);
int _stlink_usb_core_id(stlink_t *sl);
int _stlink_usb_read_debug32(stlink_t *sl, uint addr, uint *data);
int _stlink_usb_status(stlink_t *sl);
int _stlink_usb_step(stlink_t *sl,int flag);
int _stlink_usb_run(stlink_t *sl,enum run_type);
int _stlink_usb_force_debug(stlink_t *sl);
int _stlink_usb_exit_debug_mode(stlink_t *sl);
int _stlink_usb_write_mem32(stlink_t *sl, uint addr, uchar *buf, ushort len);
int _stlink_usb_write_reg(stlink_t *sl, uint reg, int idx);
int _stlink_usb_read_mem32(stlink_t *sl, uint addr, uchar *buf, ushort len);
int _stlink_usb_read_reg(stlink_t *sl, int r_idx, struct stlink_reg *regp);
int _stlink_usb_read_all_regs(stlink_t *sl, struct stlink_reg *regp);
int _stlink_usb_read_all_unsupported_regs(stlink_t *sl, struct stlink_reg *regp);

int stlink_chip_id(stlink_t *sl, uint *chip_id);
int stlink_core_id(stlink_t *sl);
int stlink_read_debug32(stlink_t *sl, uint addr, uint *data);
int _stlink_usb_write_debug32(stlink_t *sl, uint addr, uint data);

int fill_command (stlink_t * sl, enum SCSI_Generic_Direction dir, uint len); 
int stlink_step(stlink_t *sl,int step);
int stlink_run(stlink_t *sl,enum run_type type);
int stlink_status(stlink_t *sl);
int stlink_force_debug(stlink_t *sl);
int stlink_read_all_regs(stlink_t *sl, struct stlink_reg *regp);
int stlink_read_all_unsupported_regs(stlink_t *sl, struct stlink_reg *regp);
int stlink_read_reg(stlink_t *sl, int idx, struct stlink_reg *regp);
int loadprg(stlink_t* sl,const char *prg,uint loadadd,uint inipc,uint inisp,int chkmem);
int stlink_write_mem32(stlink_t *sl, uint addr, uchar *buf,uint len);
int stlink_write_debug32(stlink_t *sl, uint addr, uint data);
int stlink_read_mem32(stlink_t *sl, uint addr, uchar *buf, ushort len);
int st_dumpmem(stlink_t* sl,uint add, ushort size);
int stlink_write_reg(stlink_t *sl, uint reg, int idx) ;
int stlink_target_connect(stlink_t *sl, enum connect_type connect);
int stlink_reset(stlink_t *sl, enum reset_type type);
int stlink_soft_reset(stlink_t *sl, int,int);
stlink_t *open_usb(int verbose, bool reset, char serial[16],int log,int vid,int pid,char *devname);
stlink_t *open_usbv2(enum ugly_loglevel verbose, enum connect_type connect, char serial[STLINK_SERIAL_BUFFER_SIZE], int freq,int log);
stlink_t *stlink_open_usbv2(int verbose, enum connect_type c, char serial[STLINK_SERIAL_BUFFER_SIZE],int f,int log);
stlink_t *stlink_open_usb(int verbose, bool reset, char serial[16],int log);
void init_code_breakpoints(stlink_t *sl);
void init_data_watchpoints(stlink_t *sl);
void init_cache (stlink_t *sl);

int  trace(stlink_t* sl,uint nstep,uint flag,uint start);
int  breakpoint(stlink_t* sl,int n,int add);
int  dumpreg(stlink_t* sl,int flag,const char *desc);
int  fillmem(stlink_t* sl,uint pattern,uint start,uint size);
int  connect(stlink_t* sl,uint flag,uint deb);
int  checkf4xx(stlink_t* sl);
int  exec_read(stlink_t* sl,int add,int size,const char *fn);
int  runcpu(stlink_t* sl,uint wtime,uint count,uint *mem,int flag);
int  contcpu(stlink_t* sl);
uint readdhcsr(stlink_t* sl,int log,int line);
int  writedhcsr(stlink_t* sl,int dhcsr,int line);
int  readdwt(stlink_t* sl,int fl,int head,uint *dwt);
int  checkfault(stlink_t* sl);
struct t_cpu;
struct t_stat;

#endif
