#include "env.h"
// generated file
// by ConstName.java
// on Mon May 23 11:45:51 CEST 2022
// const name generation
// WRONG #ifdef XX
// WRONG #endif
// WRONG #ifdef XX
// WRONG #endif
// WRONG #ifdef XX
// WRONG #endif
// WRONG #ifdef XX
// WRONG #endif
// WRONG #define DW_FRAME_RA_COL  (DW_FRAME_HIGHEST_NORMAL_REGISTER + 1)
// WRONG #define DW_FRAME_STATIC_LINK (DW_FRAME_HIGHEST_NORMAL_REGISTER + 2)
void decode_error(char *where,int v)
{
  printf("decode_error from %s v %d %x\n",where,v,v);
  exit(0);
}

char unmapped_buf[40];
char *unmapped(int v)
{
  sprintf(unmapped_buf,"? %d %x",v,v);
  return unmapped_buf;
}

// prefix DW_ACCESS            3
// prefix DW_ADDR              1
// prefix DW_AT                243
// prefix DW_ATCF              10
// prefix DW_ATE               28
// prefix DW_CC                12
// prefix DW_CFA               33
// prefix DW_CHILDREN          2
// prefix DW_DEFAULTED         3
// prefix DW_DS                5
// prefix DW_DSC               2
// prefix DW_EH                15
// prefix DW_END               5
// prefix DW_FORM              38
// prefix DW_FRAME             109
// prefix DW_ID                4
// prefix DW_IDX               7
// prefix DW_INL               4
// prefix DW_ISA               3
// prefix DW_LANG              42
// prefix DW_LLE               9
// prefix DW_LLEX              5
// prefix DW_LNCT              10
// prefix DW_LNE               17
// prefix DW_LNS               15
// prefix DW_MACINFO           5
// prefix DW_MACRO             14
// prefix DW_OP                186
// prefix DW_ORD               2
// prefix DW_RLE               8
// prefix DW_SECT              8
// prefix DW_TAG               108
// prefix DW_UT                3
// prefix DW_VIRTUALITY        3
// prefix DW_VIS               3
// ff 1,000
char *nametab_dw_access[3];
void init_dw_access()
{
  nametab_dw_access[   0] = "DW_ACCESS_public";
  nametab_dw_access[   1] = "DW_ACCESS_protected";
  nametab_dw_access[   2] = "DW_ACCESS_private";
}

char *decode_dw_access(int v)
{
  if (v < 1 || v > 3) 
    return unmapped(v);
  return nametab_dw_access[v - 1];
}

void deinit_dw_access()
{
}

// ff 1,000
char *nametab_dw_addr[1];
void init_dw_addr()
{
  nametab_dw_addr[   0] = "DW_ADDR_none";
}

char *decode_dw_addr(int v)
{
  if (v < 0 || v > 0) 
    return unmapped(v);
  return nametab_dw_addr[v - 0];
}

void deinit_dw_addr()
{
}

// ff 0,006
char **nametab_dw_at[27];
void init_dw_at()
{
  nametab_dw_at[0] = myalloc(sizeof(char *) *     3); // range      1 -      3
  nametab_dw_at[1] = myalloc(sizeof(char *) *     5); // range      9 -     13
  nametab_dw_at[2] = myalloc(sizeof(char *) *    16); // range     15 -     30
  nametab_dw_at[3] = myalloc(sizeof(char *) *     3); // range     32 -     34
  nametab_dw_at[4] = myalloc(sizeof(char *) *     1); // range     37 -     37
  nametab_dw_at[5] = myalloc(sizeof(char *) *     1); // range     39 -     39
  nametab_dw_at[6] = myalloc(sizeof(char *) *     1); // range     42 -     42
  nametab_dw_at[7] = myalloc(sizeof(char *) *     1); // range     44 -     44
  nametab_dw_at[8] = myalloc(sizeof(char *) *     2); // range     46 -     47
  nametab_dw_at[9] = myalloc(sizeof(char *) *    92); // range     49 -    140
  nametab_dw_at[10] = myalloc(sizeof(char *) *     6); // range   8192 -   8197
  nametab_dw_at[11] = myalloc(sizeof(char *) *    12); // range   8208 -   8219
  nametab_dw_at[12] = myalloc(sizeof(char *) *     1); // range   8230 -   8230
  nametab_dw_at[13] = myalloc(sizeof(char *) *    25); // range   8449 -   8473
  nametab_dw_at[14] = myalloc(sizeof(char *) *     7); // range   8496 -   8502
  nametab_dw_at[15] = myalloc(sizeof(char *) *     9); // range   8705 -   8713
  nametab_dw_at[16] = myalloc(sizeof(char *) *    10); // range   8720 -   8729
  nametab_dw_at[17] = myalloc(sizeof(char *) *    15); // range   8736 -   8750
  nametab_dw_at[18] = myalloc(sizeof(char *) *    12); // range   8752 -   8763
  nametab_dw_at[19] = myalloc(sizeof(char *) *     6); // range   8960 -   8965
  nametab_dw_at[20] = myalloc(sizeof(char *) *     1); // range  12816 -  12816
  nametab_dw_at[21] = myalloc(sizeof(char *) *     3); // range  14848 -  14850
  nametab_dw_at[22] = myalloc(sizeof(char *) *     7); // range  16353 -  16359
  nametab_dw_at[23] = myalloc(sizeof(char *) *     1); // range  16383 -  16383
  nametab_dw_at[24] = myalloc(sizeof(char *) *     1); // range  34624 -  34624
  nametab_dw_at[25] = myalloc(sizeof(char *) *     1); // range  37186 -  37186
  nametab_dw_at[26] = myalloc(sizeof(char *) *     1); // range  38722 -  38722
  nametab_dw_at[0][0] = "DW_AT_sibling";
  nametab_dw_at[0][1] = "DW_AT_location";
  nametab_dw_at[0][2] = "DW_AT_name";
  nametab_dw_at[1][0] = "DW_AT_ordering";
  nametab_dw_at[1][1] = "DW_AT_subscr_data";
  nametab_dw_at[1][2] = "DW_AT_byte_size";
  nametab_dw_at[1][3] = "DW_AT_bit_offset";
  nametab_dw_at[1][4] = "DW_AT_bit_size";
  nametab_dw_at[2][0] = "DW_AT_element_list";
  nametab_dw_at[2][1] = "DW_AT_stmt_list";
  nametab_dw_at[2][2] = "DW_AT_low_pc";
  nametab_dw_at[2][3] = "DW_AT_high_pc";
  nametab_dw_at[2][4] = "DW_AT_language";
  nametab_dw_at[2][5] = "DW_AT_member";
  nametab_dw_at[2][6] = "DW_AT_discr";
  nametab_dw_at[2][7] = "DW_AT_discr_value";
  nametab_dw_at[2][8] = "DW_AT_visibility";
  nametab_dw_at[2][9] = "DW_AT_import";
  nametab_dw_at[2][10] = "DW_AT_string_length";
  nametab_dw_at[2][11] = "DW_AT_common_reference";
  nametab_dw_at[2][12] = "DW_AT_comp_dir";
  nametab_dw_at[2][13] = "DW_AT_const_value";
  nametab_dw_at[2][14] = "DW_AT_containing_type";
  nametab_dw_at[2][15] = "DW_AT_default_value";
  nametab_dw_at[3][0] = "DW_AT_inline";
  nametab_dw_at[3][1] = "DW_AT_is_optional";
  nametab_dw_at[3][2] = "DW_AT_lower_bound";
  nametab_dw_at[4][0] = "DW_AT_producer";
  nametab_dw_at[5][0] = "DW_AT_prototyped";
  nametab_dw_at[6][0] = "DW_AT_return_addr";
  nametab_dw_at[7][0] = "DW_AT_start_scope";
  nametab_dw_at[8][0] = "DW_AT_stride_size";
  nametab_dw_at[8][1] = "DW_AT_upper_bound";
  nametab_dw_at[9][0] = "DW_AT_abstract_origin";
  nametab_dw_at[9][1] = "DW_AT_accessibility";
  nametab_dw_at[9][2] = "DW_AT_address_class";
  nametab_dw_at[9][3] = "DW_AT_artificial";
  nametab_dw_at[9][4] = "DW_AT_base_types";
  nametab_dw_at[9][5] = "DW_AT_calling_convention";
  nametab_dw_at[9][6] = "DW_AT_count";
  nametab_dw_at[9][7] = "DW_AT_data_member_location";
  nametab_dw_at[9][8] = "DW_AT_decl_column";
  nametab_dw_at[9][9] = "DW_AT_decl_file";
  nametab_dw_at[9][10] = "DW_AT_decl_line";
  nametab_dw_at[9][11] = "DW_AT_declaration";
  nametab_dw_at[9][12] = "DW_AT_discr_list";
  nametab_dw_at[9][13] = "DW_AT_encoding";
  nametab_dw_at[9][14] = "DW_AT_external";
  nametab_dw_at[9][15] = "DW_AT_frame_base";
  nametab_dw_at[9][16] = "DW_AT_friend";
  nametab_dw_at[9][17] = "DW_AT_identifier_case";
  nametab_dw_at[9][18] = "DW_AT_macro_info";
  nametab_dw_at[9][19] = "DW_AT_namelist_item";
  nametab_dw_at[9][20] = "DW_AT_priority";
  nametab_dw_at[9][21] = "DW_AT_segment";
  nametab_dw_at[9][22] = "DW_AT_specification";
  nametab_dw_at[9][23] = "DW_AT_static_link";
  nametab_dw_at[9][24] = "DW_AT_type";
  nametab_dw_at[9][25] = "DW_AT_use_location";
  nametab_dw_at[9][26] = "DW_AT_variable_parameter";
  nametab_dw_at[9][27] = "DW_AT_virtuality";
  nametab_dw_at[9][28] = "DW_AT_vtable_elem_location";
  nametab_dw_at[9][29] = "DW_AT_allocated";
  nametab_dw_at[9][30] = "DW_AT_associated";
  nametab_dw_at[9][31] = "DW_AT_data_location";
  nametab_dw_at[9][32] = "DW_AT_stride";
  nametab_dw_at[9][33] = "DW_AT_entry_pc";
  nametab_dw_at[9][34] = "DW_AT_use_UTF8";
  nametab_dw_at[9][35] = "DW_AT_extension";
  nametab_dw_at[9][36] = "DW_AT_ranges";
  nametab_dw_at[9][37] = "DW_AT_trampoline";
  nametab_dw_at[9][38] = "DW_AT_call_column";
  nametab_dw_at[9][39] = "DW_AT_call_file";
  nametab_dw_at[9][40] = "DW_AT_call_line";
  nametab_dw_at[9][41] = "DW_AT_description";
  nametab_dw_at[9][42] = "DW_AT_binary_scale";
  nametab_dw_at[9][43] = "DW_AT_decimal_scale";
  nametab_dw_at[9][44] = "DW_AT_small";
  nametab_dw_at[9][45] = "DW_AT_decimal_sign";
  nametab_dw_at[9][46] = "DW_AT_digit_count";
  nametab_dw_at[9][47] = "DW_AT_picture_string";
  nametab_dw_at[9][48] = "DW_AT_mutable";
  nametab_dw_at[9][49] = "DW_AT_threads_scaled";
  nametab_dw_at[9][50] = "DW_AT_explicit";
  nametab_dw_at[9][51] = "DW_AT_object_pointer";
  nametab_dw_at[9][52] = "DW_AT_endianity";
  nametab_dw_at[9][53] = "DW_AT_elemental";
  nametab_dw_at[9][54] = "DW_AT_pure";
  nametab_dw_at[9][55] = "DW_AT_recursive";
  nametab_dw_at[9][56] = "DW_AT_signature";
  nametab_dw_at[9][57] = "DW_AT_main_subprogram";
  nametab_dw_at[9][58] = "DW_AT_data_bit_offset";
  nametab_dw_at[9][59] = "DW_AT_const_expr";
  nametab_dw_at[9][60] = "DW_AT_enum_class";
  nametab_dw_at[9][61] = "DW_AT_linkage_name";
  nametab_dw_at[9][62] = "DW_AT_string_length_bit_size";
  nametab_dw_at[9][63] = "DW_AT_string_length_byte_size";
  nametab_dw_at[9][64] = "DW_AT_rank";
  nametab_dw_at[9][65] = "DW_AT_str_offsets_base";
  nametab_dw_at[9][66] = "DW_AT_addr_base";
  nametab_dw_at[9][67] = "DW_AT_ranges_base";
  nametab_dw_at[9][68] = "DW_AT_dwo_id";
  nametab_dw_at[9][69] = "DW_AT_dwo_name";
  nametab_dw_at[9][70] = "DW_AT_reference";
  nametab_dw_at[9][71] = "DW_AT_rvalue_reference";
  nametab_dw_at[9][72] = "DW_AT_macros";
  nametab_dw_at[9][73] = "DW_AT_call_all_calls";
  nametab_dw_at[9][74] = "DW_AT_call_all_source_calls";
  nametab_dw_at[9][75] = "DW_AT_call_all_tail_calls";
  nametab_dw_at[9][76] = "DW_AT_call_return_pc";
  nametab_dw_at[9][77] = "DW_AT_call_value";
  nametab_dw_at[9][78] = "DW_AT_call_origin";
  nametab_dw_at[9][79] = "DW_AT_call_parameter";
  nametab_dw_at[9][80] = "DW_AT_call_pc";
  nametab_dw_at[9][81] = "DW_AT_call_tail_call";
  nametab_dw_at[9][82] = "DW_AT_call_target";
  nametab_dw_at[9][83] = "DW_AT_call_target_clobbered";
  nametab_dw_at[9][84] = "DW_AT_call_data_location";
  nametab_dw_at[9][85] = "DW_AT_call_data_value";
  nametab_dw_at[9][86] = "DW_AT_noreturn";
  nametab_dw_at[9][87] = "DW_AT_alignment";
  nametab_dw_at[9][88] = "DW_AT_export_symbols";
  nametab_dw_at[9][89] = "DW_AT_deleted";
  nametab_dw_at[9][90] = "DW_AT_defaulted";
  nametab_dw_at[9][91] = "DW_AT_loclists_base";
  nametab_dw_at[10][0] = "DW_AT_lo_user";
  nametab_dw_at[10][1] = "DW_AT_CPQ_discontig_ranges";
  nametab_dw_at[10][2] = "DW_AT_CPQ_semantic_events";
  nametab_dw_at[10][3] = "DW_AT_CPQ_split_lifetimes_var";
  nametab_dw_at[10][4] = "DW_AT_CPQ_split_lifetimes_rtn";
  nametab_dw_at[10][5] = "DW_AT_CPQ_prologue_length";
  nametab_dw_at[11][0] = "DW_AT_HP_actuals_stmt_list";
  nametab_dw_at[11][1] = "DW_AT_HP_proc_per_section";
  nametab_dw_at[11][2] = "DW_AT_HP_raw_data_ptr";
  nametab_dw_at[11][3] = "DW_AT_HP_pass_by_reference";
  nametab_dw_at[11][4] = "DW_AT_HP_opt_level";
  nametab_dw_at[11][5] = "DW_AT_HP_prof_version_id";
  nametab_dw_at[11][6] = "DW_AT_HP_opt_flags";
  nametab_dw_at[11][7] = "DW_AT_HP_cold_region_low_pc";
  nametab_dw_at[11][8] = "DW_AT_HP_cold_region_high_pc";
  nametab_dw_at[11][9] = "DW_AT_HP_all_variables_modifiable";
  nametab_dw_at[11][10] = "DW_AT_HP_linkage_name";
  nametab_dw_at[11][11] = "DW_AT_HP_prof_flags";
  nametab_dw_at[12][0] = "DW_AT_INTEL_other_endian";
  nametab_dw_at[13][0] = "DW_AT_sf_names";
  nametab_dw_at[13][1] = "DW_AT_src_info";
  nametab_dw_at[13][2] = "DW_AT_mac_info";
  nametab_dw_at[13][3] = "DW_AT_src_coords";
  nametab_dw_at[13][4] = "DW_AT_body_begin";
  nametab_dw_at[13][5] = "DW_AT_body_end";
  nametab_dw_at[13][6] = "DW_AT_GNU_vector";
  nametab_dw_at[13][7] = "DW_AT_GNU_guarded_by";
  nametab_dw_at[13][8] = "DW_AT_GNU_pt_guarded_by";
  nametab_dw_at[13][9] = "DW_AT_GNU_guarded";
  nametab_dw_at[13][10] = "DW_AT_GNU_pt_guarded";
  nametab_dw_at[13][11] = "DW_AT_GNU_locks_excluded";
  nametab_dw_at[13][12] = "DW_AT_GNU_exclusive_locks_required";
  nametab_dw_at[13][13] = "DW_AT_GNU_shared_locks_required";
  nametab_dw_at[13][14] = "DW_AT_GNU_odr_signature";
  nametab_dw_at[13][15] = "DW_AT_GNU_template_name";
  nametab_dw_at[13][16] = "DW_AT_GNU_call_site_value";
  nametab_dw_at[13][17] = "DW_AT_GNU_call_site_data_value";
  nametab_dw_at[13][18] = "DW_AT_GNU_call_site_target";
  nametab_dw_at[13][19] = "DW_AT_GNU_call_site_target_clobbered";
  nametab_dw_at[13][20] = "DW_AT_GNU_tail_call";
  nametab_dw_at[13][21] = "DW_AT_GNU_all_tail_call_sites";
  nametab_dw_at[13][22] = "DW_AT_GNU_all_call_sites";
  nametab_dw_at[13][23] = "DW_AT_GNU_all_source_call_sites";
  nametab_dw_at[13][24] = "DW_AT_GNU_macros";
  nametab_dw_at[14][0] = "DW_AT_GNU_dwo_name";
  nametab_dw_at[14][1] = "DW_AT_GNU_dwo_id";
  nametab_dw_at[14][2] = "DW_AT_GNU_ranges_base";
  nametab_dw_at[14][3] = "DW_AT_GNU_addr_base";
  nametab_dw_at[14][4] = "DW_AT_GNU_pubnames";
  nametab_dw_at[14][5] = "DW_AT_GNU_pubtypes";
  nametab_dw_at[14][6] = "DW_AT_GNU_discriminator";
  nametab_dw_at[15][0] = "DW_AT_VMS_rtnbeg_pd_address";
  nametab_dw_at[15][1] = "DW_AT_SUN_alignment";
  nametab_dw_at[15][2] = "DW_AT_SUN_vtable";
  nametab_dw_at[15][3] = "DW_AT_SUN_count_guarantee";
  nametab_dw_at[15][4] = "DW_AT_SUN_command_line";
  nametab_dw_at[15][5] = "DW_AT_SUN_vbase";
  nametab_dw_at[15][6] = "DW_AT_SUN_compile_options";
  nametab_dw_at[15][7] = "DW_AT_SUN_language";
  nametab_dw_at[15][8] = "DW_AT_SUN_browser_file";
  nametab_dw_at[16][0] = "DW_AT_SUN_vtable_abi";
  nametab_dw_at[16][1] = "DW_AT_SUN_func_offsets";
  nametab_dw_at[16][2] = "DW_AT_SUN_cf_kind";
  nametab_dw_at[16][3] = "DW_AT_SUN_vtable_index";
  nametab_dw_at[16][4] = "DW_AT_SUN_omp_tpriv_addr";
  nametab_dw_at[16][5] = "DW_AT_SUN_omp_child_func";
  nametab_dw_at[16][6] = "DW_AT_SUN_func_offset";
  nametab_dw_at[16][7] = "DW_AT_SUN_memop_type_ref";
  nametab_dw_at[16][8] = "DW_AT_SUN_profile_id";
  nametab_dw_at[16][9] = "DW_AT_SUN_memop_signature";
  nametab_dw_at[17][0] = "DW_AT_SUN_obj_dir";
  nametab_dw_at[17][1] = "DW_AT_SUN_obj_file";
  nametab_dw_at[17][2] = "DW_AT_SUN_original_name";
  nametab_dw_at[17][3] = "DW_AT_SUN_hwcprof_signature";
  nametab_dw_at[17][4] = "DW_AT_SUN_amd64_parmdump";
  nametab_dw_at[17][5] = "DW_AT_SUN_part_link_name";
  nametab_dw_at[17][6] = "DW_AT_SUN_link_name";
  nametab_dw_at[17][7] = "DW_AT_SUN_pass_with_const";
  nametab_dw_at[17][8] = "DW_AT_SUN_return_with_const";
  nametab_dw_at[17][9] = "DW_AT_SUN_import_by_name";
  nametab_dw_at[17][10] = "DW_AT_SUN_f90_pointer";
  nametab_dw_at[17][11] = "DW_AT_SUN_pass_by_ref";
  nametab_dw_at[17][12] = "DW_AT_SUN_f90_allocatable";
  nametab_dw_at[17][13] = "DW_AT_SUN_f90_assumed_shape_array";
  nametab_dw_at[17][14] = "DW_AT_SUN_c_vla";
  nametab_dw_at[18][0] = "DW_AT_SUN_return_value_ptr";
  nametab_dw_at[18][1] = "DW_AT_SUN_dtor_start";
  nametab_dw_at[18][2] = "DW_AT_SUN_dtor_length";
  nametab_dw_at[18][3] = "DW_AT_SUN_dtor_state_initial";
  nametab_dw_at[18][4] = "DW_AT_SUN_dtor_state_final";
  nametab_dw_at[18][5] = "DW_AT_SUN_dtor_state_deltas";
  nametab_dw_at[18][6] = "DW_AT_SUN_import_by_lname";
  nametab_dw_at[18][7] = "DW_AT_SUN_f90_use_only";
  nametab_dw_at[18][8] = "DW_AT_SUN_namelist_spec";
  nametab_dw_at[18][9] = "DW_AT_SUN_is_omp_child_func";
  nametab_dw_at[18][10] = "DW_AT_SUN_fortran_main_alias";
  nametab_dw_at[18][11] = "DW_AT_SUN_fortran_based";
  nametab_dw_at[19][0] = "DW_AT_ALTIUM_loclist";
  nametab_dw_at[19][1] = "DW_AT_use_GNAT_descriptive_type";
  nametab_dw_at[19][2] = "DW_AT_GNAT_descriptive_type";
  nametab_dw_at[19][3] = "DW_AT_GNU_numerator";
  nametab_dw_at[19][4] = "DW_AT_GNU_denominator";
  nametab_dw_at[19][5] = "DW_AT_GNU_bias";
  nametab_dw_at[20][0] = "DW_AT_upc_threads_scaled";
  nametab_dw_at[21][0] = "DW_AT_PGI_lbase";
  nametab_dw_at[21][1] = "DW_AT_PGI_soffset";
  nametab_dw_at[21][2] = "DW_AT_PGI_lstride";
  nametab_dw_at[22][0] = "DW_AT_APPLE_optimized";
  nametab_dw_at[22][1] = "DW_AT_APPLE_flags";
  nametab_dw_at[22][2] = "DW_AT_APPLE_isa";
  nametab_dw_at[22][3] = "DW_AT_APPLE_closure";
  nametab_dw_at[22][4] = "DW_AT_APPLE_major_runtime_vers";
  nametab_dw_at[22][5] = "DW_AT_APPLE_runtime_class";
  nametab_dw_at[22][6] = "DW_AT_APPLE_omit_frame_ptr";
  nametab_dw_at[23][0] = "DW_AT_hi_user";
  nametab_dw_at[24][0] = "DW_AT_MIPS_linkage_name";
  nametab_dw_at[25][0] = "DW_AT_GNU_call_site_value";
  nametab_dw_at[26][0] = "DW_AT_GNU_all_call_sites";
}
char *decode_dw_at(int v)
{
  if (v < 1 || v > 38722) 
    return unmapped(v);
  if (v <= 3)
  {
    if (v < 1)
    return unmapped(v);
    return nametab_dw_at[0][v - 1];
  }
  if (v <= 13)
  {
    if (v < 9)
    return unmapped(v);
    return nametab_dw_at[1][v - 9];
  }
  if (v <= 30)
  {
    if (v < 15)
    return unmapped(v);
    return nametab_dw_at[2][v - 15];
  }
  if (v <= 34)
  {
    if (v < 32)
    return unmapped(v);
    return nametab_dw_at[3][v - 32];
  }
  if (v <= 37)
  {
    if (v < 37)
    return unmapped(v);
    return nametab_dw_at[4][v - 37];
  }
  if (v <= 39)
  {
    if (v < 39)
    return unmapped(v);
    return nametab_dw_at[5][v - 39];
  }
  if (v <= 42)
  {
    if (v < 42)
    return unmapped(v);
    return nametab_dw_at[6][v - 42];
  }
  if (v <= 44)
  {
    if (v < 44)
    return unmapped(v);
    return nametab_dw_at[7][v - 44];
  }
  if (v <= 47)
  {
    if (v < 46)
    return unmapped(v);
    return nametab_dw_at[8][v - 46];
  }
  if (v <= 140)
  {
    if (v < 49)
    return unmapped(v);
    return nametab_dw_at[9][v - 49];
  }
  if (v <= 8197)
  {
    if (v < 8192)
    return unmapped(v);
    return nametab_dw_at[10][v - 8192];
  }
  if (v <= 8219)
  {
    if (v < 8208)
    return unmapped(v);
    return nametab_dw_at[11][v - 8208];
  }
  if (v <= 8230)
  {
    if (v < 8230)
    return unmapped(v);
    return nametab_dw_at[12][v - 8230];
  }
  if (v <= 8473)
  {
    if (v < 8449)
    return unmapped(v);
    return nametab_dw_at[13][v - 8449];
  }
  if (v <= 8502)
  {
    if (v < 8496)
    return unmapped(v);
    return nametab_dw_at[14][v - 8496];
  }
  if (v <= 8713)
  {
    if (v < 8705)
    return unmapped(v);
    return nametab_dw_at[15][v - 8705];
  }
  if (v <= 8729)
  {
    if (v < 8720)
    return unmapped(v);
    return nametab_dw_at[16][v - 8720];
  }
  if (v <= 8750)
  {
    if (v < 8736)
    return unmapped(v);
    return nametab_dw_at[17][v - 8736];
  }
  if (v <= 8763)
  {
    if (v < 8752)
    return unmapped(v);
    return nametab_dw_at[18][v - 8752];
  }
  if (v <= 8965)
  {
    if (v < 8960)
    return unmapped(v);
    return nametab_dw_at[19][v - 8960];
  }
  if (v <= 12816)
  {
    if (v < 12816)
    return unmapped(v);
    return nametab_dw_at[20][v - 12816];
  }
  if (v <= 14850)
  {
    if (v < 14848)
    return unmapped(v);
    return nametab_dw_at[21][v - 14848];
  }
  if (v <= 16359)
  {
    if (v < 16353)
    return unmapped(v);
    return nametab_dw_at[22][v - 16353];
  }
  if (v <= 16383)
  {
    if (v < 16383)
    return unmapped(v);
    return nametab_dw_at[23][v - 16383];
  }
  if (v <= 34624)
  {
    if (v < 34624)
    return unmapped(v);
    return nametab_dw_at[24][v - 34624];
  }
  if (v <= 37186)
  {
    if (v < 37186)
    return unmapped(v);
    return nametab_dw_at[25][v - 37186];
  }
  if (v <= 38722)
  {
    if (v < 38722)
    return unmapped(v);
    return nametab_dw_at[26][v - 38722];
  }
  return 0;
}

void deinit_dw_at()
{
  myfree(nametab_dw_at[0]); // range      1 -      3
  myfree(nametab_dw_at[1]); // range      9 -     13
  myfree(nametab_dw_at[2]); // range     15 -     30
  myfree(nametab_dw_at[3]); // range     32 -     34
  myfree(nametab_dw_at[4]); // range     37 -     37
  myfree(nametab_dw_at[5]); // range     39 -     39
  myfree(nametab_dw_at[6]); // range     42 -     42
  myfree(nametab_dw_at[7]); // range     44 -     44
  myfree(nametab_dw_at[8]); // range     46 -     47
  myfree(nametab_dw_at[9]); // range     49 -    140
  myfree(nametab_dw_at[10]); // range   8192 -   8197
  myfree(nametab_dw_at[11]); // range   8208 -   8219
  myfree(nametab_dw_at[12]); // range   8230 -   8230
  myfree(nametab_dw_at[13]); // range   8449 -   8473
  myfree(nametab_dw_at[14]); // range   8496 -   8502
  myfree(nametab_dw_at[15]); // range   8705 -   8713
  myfree(nametab_dw_at[16]); // range   8720 -   8729
  myfree(nametab_dw_at[17]); // range   8736 -   8750
  myfree(nametab_dw_at[18]); // range   8752 -   8763
  myfree(nametab_dw_at[19]); // range   8960 -   8965
  myfree(nametab_dw_at[20]); // range  12816 -  12816
  myfree(nametab_dw_at[21]); // range  14848 -  14850
  myfree(nametab_dw_at[22]); // range  16353 -  16359
  myfree(nametab_dw_at[23]); // range  16383 -  16383
  myfree(nametab_dw_at[24]); // range  34624 -  34624
  myfree(nametab_dw_at[25]); // range  37186 -  37186
  myfree(nametab_dw_at[26]); // range  38722 -  38722
}

// ff 0,052
char **nametab_dw_atcf[2];
void init_dw_atcf()
{
  nametab_dw_atcf[0] = myalloc(sizeof(char *) *     9); // range     64 -     72
  nametab_dw_atcf[1] = myalloc(sizeof(char *) *     1); // range    255 -    255
  nametab_dw_atcf[0][0] = "DW_ATCF_lo_user";
  nametab_dw_atcf[0][1] = "DW_ATCF_SUN_mop_bitfield";
  nametab_dw_atcf[0][2] = "DW_ATCF_SUN_mop_spill";
  nametab_dw_atcf[0][3] = "DW_ATCF_SUN_mop_scopy";
  nametab_dw_atcf[0][4] = "DW_ATCF_SUN_func_start";
  nametab_dw_atcf[0][5] = "DW_ATCF_SUN_end_ctors";
  nametab_dw_atcf[0][6] = "DW_ATCF_SUN_branch_target";
  nametab_dw_atcf[0][7] = "DW_ATCF_SUN_mop_stack_probe";
  nametab_dw_atcf[0][8] = "DW_ATCF_SUN_func_epilog";
  nametab_dw_atcf[1][0] = "DW_ATCF_hi_user";
}
char *decode_dw_atcf(int v)
{
  if (v < 64 || v > 255) 
    return unmapped(v);
  if (v <= 72)
  {
    if (v < 64)
    return unmapped(v);
    return nametab_dw_atcf[0][v - 64];
  }
  if (v <= 255)
  {
    if (v < 255)
    return unmapped(v);
    return nametab_dw_atcf[1][v - 255];
  }
  return 0;
}

void deinit_dw_atcf()
{
  myfree(nametab_dw_atcf[0]); // range     64 -     72
  myfree(nametab_dw_atcf[1]); // range    255 -    255
}

// ff 0,110
char **nametab_dw_ate[4];
void init_dw_ate()
{
  nametab_dw_ate[0] = myalloc(sizeof(char *) *    18); // range      1 -     18
  nametab_dw_ate[1] = myalloc(sizeof(char *) *     7); // range    128 -    134
  nametab_dw_ate[2] = myalloc(sizeof(char *) *     2); // range    145 -    146
  nametab_dw_ate[3] = myalloc(sizeof(char *) *     1); // range    255 -    255
  nametab_dw_ate[0][0] = "DW_ATE_address";
  nametab_dw_ate[0][1] = "DW_ATE_boolean";
  nametab_dw_ate[0][2] = "DW_ATE_complex_float";
  nametab_dw_ate[0][3] = "DW_ATE_float";
  nametab_dw_ate[0][4] = "DW_ATE_signed";
  nametab_dw_ate[0][5] = "DW_ATE_signed_char";
  nametab_dw_ate[0][6] = "DW_ATE_unsigned";
  nametab_dw_ate[0][7] = "DW_ATE_unsigned_char";
  nametab_dw_ate[0][8] = "DW_ATE_imaginary_float";
  nametab_dw_ate[0][9] = "DW_ATE_packed_decimal";
  nametab_dw_ate[0][10] = "DW_ATE_numeric_string";
  nametab_dw_ate[0][11] = "DW_ATE_edited";
  nametab_dw_ate[0][12] = "DW_ATE_signed_fixed";
  nametab_dw_ate[0][13] = "DW_ATE_unsigned_fixed";
  nametab_dw_ate[0][14] = "DW_ATE_decimal_float";
  nametab_dw_ate[0][15] = "DW_ATE_UTF";
  nametab_dw_ate[0][16] = "DW_ATE_UCS";
  nametab_dw_ate[0][17] = "DW_ATE_ASCII";
  nametab_dw_ate[1][0] = "DW_ATE_HP_float80";
  nametab_dw_ate[1][1] = "DW_ATE_HP_complex_float80";
  nametab_dw_ate[1][2] = "DW_ATE_HP_float128";
  nametab_dw_ate[1][3] = "DW_ATE_HP_complex_float128";
  nametab_dw_ate[1][4] = "DW_ATE_HP_floathpintel";
  nametab_dw_ate[1][5] = "DW_ATE_HP_imaginary_float80";
  nametab_dw_ate[1][6] = "DW_ATE_HP_imaginary_float128";
  nametab_dw_ate[2][0] = "DW_ATE_SUN_interval_float";
  nametab_dw_ate[2][1] = "DW_ATE_SUN_imaginary_float";
  nametab_dw_ate[3][0] = "DW_ATE_hi_user";
}
char *decode_dw_ate(int v)
{
  if (v < 1 || v > 255) 
    return unmapped(v);
  if (v <= 18)
  {
    if (v < 1)
    return unmapped(v);
    return nametab_dw_ate[0][v - 1];
  }
  if (v <= 134)
  {
    if (v < 128)
    return unmapped(v);
    return nametab_dw_ate[1][v - 128];
  }
  if (v <= 146)
  {
    if (v < 145)
    return unmapped(v);
    return nametab_dw_ate[2][v - 145];
  }
  if (v <= 255)
  {
    if (v < 255)
    return unmapped(v);
    return nametab_dw_ate[3][v - 255];
  }
  return 0;
}

void deinit_dw_ate()
{
  myfree(nametab_dw_ate[0]); // range      1 -     18
  myfree(nametab_dw_ate[1]); // range    128 -    134
  myfree(nametab_dw_ate[2]); // range    145 -    146
  myfree(nametab_dw_ate[3]); // range    255 -    255
}

// ff 0,047
char **nametab_dw_cc[4];
void init_dw_cc()
{
  nametab_dw_cc[0] = myalloc(sizeof(char *) *     5); // range      1 -      5
  nametab_dw_cc[1] = myalloc(sizeof(char *) *     2); // range     64 -     65
  nametab_dw_cc[2] = myalloc(sizeof(char *) *     4); // range    101 -    104
  nametab_dw_cc[3] = myalloc(sizeof(char *) *     1); // range    255 -    255
  nametab_dw_cc[0][0] = "DW_CC_normal";
  nametab_dw_cc[0][1] = "DW_CC_program";
  nametab_dw_cc[0][2] = "DW_CC_nocall";
  nametab_dw_cc[0][3] = "DW_CC_pass_by_reference";
  nametab_dw_cc[0][4] = "DW_CC_pass_by_value";
  nametab_dw_cc[1][0] = "DW_CC_GNU_renesas_sh";
  nametab_dw_cc[1][1] = "DW_CC_GNU_borland_fastcall_i386";
  nametab_dw_cc[2][0] = "DW_CC_ALTIUM_interrupt";
  nametab_dw_cc[2][1] = "DW_CC_ALTIUM_near_system_stack";
  nametab_dw_cc[2][2] = "DW_CC_ALTIUM_near_user_stack";
  nametab_dw_cc[2][3] = "DW_CC_ALTIUM_huge_user_stack";
  nametab_dw_cc[3][0] = "DW_CC_hi_user";
}
char *decode_dw_cc(int v)
{
  if (v < 1 || v > 255) 
    return unmapped(v);
  if (v <= 5)
  {
    if (v < 1)
    return unmapped(v);
    return nametab_dw_cc[0][v - 1];
  }
  if (v <= 65)
  {
    if (v < 64)
    return unmapped(v);
    return nametab_dw_cc[1][v - 64];
  }
  if (v <= 104)
  {
    if (v < 101)
    return unmapped(v);
    return nametab_dw_cc[2][v - 101];
  }
  if (v <= 255)
  {
    if (v < 255)
    return unmapped(v);
    return nametab_dw_cc[3][v - 255];
  }
  return 0;
}

void deinit_dw_cc()
{
  myfree(nametab_dw_cc[0]); // range      1 -      5
  myfree(nametab_dw_cc[1]); // range     64 -     65
  myfree(nametab_dw_cc[2]); // range    101 -    104
  myfree(nametab_dw_cc[3]); // range    255 -    255
}

// ff 0,171
char **nametab_dw_cfa[7];
void init_dw_cfa()
{
  nametab_dw_cfa[0] = myalloc(sizeof(char *) *    23); // range      0 -     22
  nametab_dw_cfa[1] = myalloc(sizeof(char *) *     2); // range     28 -     29
  nametab_dw_cfa[2] = myalloc(sizeof(char *) *     3); // range     45 -     47
  nametab_dw_cfa[3] = myalloc(sizeof(char *) *     1); // range     52 -     52
  nametab_dw_cfa[4] = myalloc(sizeof(char *) *     2); // range     63 -     64
  nametab_dw_cfa[5] = myalloc(sizeof(char *) *     1); // range    128 -    128
  nametab_dw_cfa[6] = myalloc(sizeof(char *) *     1); // range    192 -    192
  nametab_dw_cfa[0][0] = "DW_CFA_nop";
  nametab_dw_cfa[0][1] = "DW_CFA_set_loc";
  nametab_dw_cfa[0][2] = "DW_CFA_advance_loc1";
  nametab_dw_cfa[0][3] = "DW_CFA_advance_loc2";
  nametab_dw_cfa[0][4] = "DW_CFA_advance_loc4";
  nametab_dw_cfa[0][5] = "DW_CFA_offset_extended";
  nametab_dw_cfa[0][6] = "DW_CFA_restore_extended";
  nametab_dw_cfa[0][7] = "DW_CFA_undefined";
  nametab_dw_cfa[0][8] = "DW_CFA_same_value";
  nametab_dw_cfa[0][9] = "DW_CFA_register";
  nametab_dw_cfa[0][10] = "DW_CFA_remember_state";
  nametab_dw_cfa[0][11] = "DW_CFA_restore_state";
  nametab_dw_cfa[0][12] = "DW_CFA_def_cfa";
  nametab_dw_cfa[0][13] = "DW_CFA_def_cfa_register";
  nametab_dw_cfa[0][14] = "DW_CFA_def_cfa_offset";
  nametab_dw_cfa[0][15] = "DW_CFA_def_cfa_expression";
  nametab_dw_cfa[0][16] = "DW_CFA_expression";
  nametab_dw_cfa[0][17] = "DW_CFA_offset_extended_sf";
  nametab_dw_cfa[0][18] = "DW_CFA_def_cfa_sf";
  nametab_dw_cfa[0][19] = "DW_CFA_def_cfa_offset_sf";
  nametab_dw_cfa[0][20] = "DW_CFA_val_offset";
  nametab_dw_cfa[0][21] = "DW_CFA_val_offset_sf";
  nametab_dw_cfa[0][22] = "DW_CFA_val_expression";
  nametab_dw_cfa[1][0] = "DW_CFA_low_user";
  nametab_dw_cfa[1][1] = "DW_CFA_MIPS_advance_loc8";
  nametab_dw_cfa[2][0] = "DW_CFA_GNU_window_save";
  nametab_dw_cfa[2][1] = "DW_CFA_GNU_args_size";
  nametab_dw_cfa[2][2] = "DW_CFA_GNU_negative_offset_extended";
  nametab_dw_cfa[3][0] = "DW_CFA_METAWARE_info";
  nametab_dw_cfa[4][0] = "DW_CFA_high_user";
  nametab_dw_cfa[4][1] = "DW_CFA_advance_loc";
  nametab_dw_cfa[5][0] = "DW_CFA_offset";
  nametab_dw_cfa[6][0] = "DW_CFA_restore";
}
char *decode_dw_cfa(int v)
{
  if (v < 0 || v > 192) 
    return unmapped(v);
  if (v <= 22)
  {
    if (v < 0)
    return unmapped(v);
    return nametab_dw_cfa[0][v - 0];
  }
  if (v <= 29)
  {
    if (v < 28)
    return unmapped(v);
    return nametab_dw_cfa[1][v - 28];
  }
  if (v <= 47)
  {
    if (v < 45)
    return unmapped(v);
    return nametab_dw_cfa[2][v - 45];
  }
  if (v <= 52)
  {
    if (v < 52)
    return unmapped(v);
    return nametab_dw_cfa[3][v - 52];
  }
  if (v <= 64)
  {
    if (v < 63)
    return unmapped(v);
    return nametab_dw_cfa[4][v - 63];
  }
  if (v <= 128)
  {
    if (v < 128)
    return unmapped(v);
    return nametab_dw_cfa[5][v - 128];
  }
  if (v <= 192)
  {
    if (v < 192)
    return unmapped(v);
    return nametab_dw_cfa[6][v - 192];
  }
  return 0;
}

void deinit_dw_cfa()
{
  myfree(nametab_dw_cfa[0]); // range      0 -     22
  myfree(nametab_dw_cfa[1]); // range     28 -     29
  myfree(nametab_dw_cfa[2]); // range     45 -     47
  myfree(nametab_dw_cfa[3]); // range     52 -     52
  myfree(nametab_dw_cfa[4]); // range     63 -     64
  myfree(nametab_dw_cfa[5]); // range    128 -    128
  myfree(nametab_dw_cfa[6]); // range    192 -    192
}

// ff 1,000
char *nametab_dw_children[2];
void init_dw_children()
{
  nametab_dw_children[   0] = "DW_CHILDREN_no";
  nametab_dw_children[   1] = "DW_CHILDREN_yes";
}

char *decode_dw_children(int v)
{
  if (v < 0 || v > 1) 
    return unmapped(v);
  return nametab_dw_children[v - 0];
}

void deinit_dw_children()
{
}

// ff 1,000
char *nametab_dw_defaulted[3];
void init_dw_defaulted()
{
  nametab_dw_defaulted[   0] = "DW_DEFAULTED_no";
  nametab_dw_defaulted[   1] = "DW_DEFAULTED_in_class";
  nametab_dw_defaulted[   2] = "DW_DEFAULTED_out_of_class";
}

char *decode_dw_defaulted(int v)
{
  if (v < 0 || v > 2) 
    return unmapped(v);
  return nametab_dw_defaulted[v - 0];
}

void deinit_dw_defaulted()
{
}

// ff 1,000
char *nametab_dw_ds[5];
void init_dw_ds()
{
  nametab_dw_ds[   0] = "DW_DS_unsigned";
  nametab_dw_ds[   1] = "DW_DS_leading_overpunch";
  nametab_dw_ds[   2] = "DW_DS_trailing_overpunch";
  nametab_dw_ds[   3] = "DW_DS_leading_separate";
  nametab_dw_ds[   4] = "DW_DS_trailing_separate";
}

char *decode_dw_ds(int v)
{
  if (v < 1 || v > 5) 
    return unmapped(v);
  return nametab_dw_ds[v - 1];
}

void deinit_dw_ds()
{
}

// ff 1,000
char *nametab_dw_dsc[2];
void init_dw_dsc()
{
  nametab_dw_dsc[   0] = "DW_DSC_label";
  nametab_dw_dsc[   1] = "DW_DSC_range";
}

char *decode_dw_dsc(int v)
{
  if (v < 0 || v > 1) 
    return unmapped(v);
  return nametab_dw_dsc[v - 0];
}

void deinit_dw_dsc()
{
}

// ff 0,059
char **nametab_dw_eh[8];
void init_dw_eh()
{
  nametab_dw_eh[0] = myalloc(sizeof(char *) *     5); // range      0 -      4
  nametab_dw_eh[1] = myalloc(sizeof(char *) *     4); // range      9 -     12
  nametab_dw_eh[2] = myalloc(sizeof(char *) *     1); // range     16 -     16
  nametab_dw_eh[3] = myalloc(sizeof(char *) *     1); // range     32 -     32
  nametab_dw_eh[4] = myalloc(sizeof(char *) *     1); // range     48 -     48
  nametab_dw_eh[5] = myalloc(sizeof(char *) *     1); // range     64 -     64
  nametab_dw_eh[6] = myalloc(sizeof(char *) *     1); // range     80 -     80
  nametab_dw_eh[7] = myalloc(sizeof(char *) *     1); // range    255 -    255
  nametab_dw_eh[0][0] = "DW_EH_PE_absptr";
  nametab_dw_eh[0][1] = "DW_EH_PE_uleb128";
  nametab_dw_eh[0][2] = "DW_EH_PE_udata2";
  nametab_dw_eh[0][3] = "DW_EH_PE_udata4";
  nametab_dw_eh[0][4] = "DW_EH_PE_udata8";
  nametab_dw_eh[1][0] = "DW_EH_PE_sleb128";
  nametab_dw_eh[1][1] = "DW_EH_PE_sdata2";
  nametab_dw_eh[1][2] = "DW_EH_PE_sdata4";
  nametab_dw_eh[1][3] = "DW_EH_PE_sdata8";
  nametab_dw_eh[2][0] = "DW_EH_PE_pcrel";
  nametab_dw_eh[3][0] = "DW_EH_PE_textrel";
  nametab_dw_eh[4][0] = "DW_EH_PE_datarel";
  nametab_dw_eh[5][0] = "DW_EH_PE_funcrel";
  nametab_dw_eh[6][0] = "DW_EH_PE_aligned";
  nametab_dw_eh[7][0] = "DW_EH_PE_omit";
}
char *decode_dw_eh(int v)
{
  if (v < 0 || v > 255) 
    return unmapped(v);
  if (v <= 4)
  {
    if (v < 0)
    return unmapped(v);
    return nametab_dw_eh[0][v - 0];
  }
  if (v <= 12)
  {
    if (v < 9)
    return unmapped(v);
    return nametab_dw_eh[1][v - 9];
  }
  if (v <= 16)
  {
    if (v < 16)
    return unmapped(v);
    return nametab_dw_eh[2][v - 16];
  }
  if (v <= 32)
  {
    if (v < 32)
    return unmapped(v);
    return nametab_dw_eh[3][v - 32];
  }
  if (v <= 48)
  {
    if (v < 48)
    return unmapped(v);
    return nametab_dw_eh[4][v - 48];
  }
  if (v <= 64)
  {
    if (v < 64)
    return unmapped(v);
    return nametab_dw_eh[5][v - 64];
  }
  if (v <= 80)
  {
    if (v < 80)
    return unmapped(v);
    return nametab_dw_eh[6][v - 80];
  }
  if (v <= 255)
  {
    if (v < 255)
    return unmapped(v);
    return nametab_dw_eh[7][v - 255];
  }
  return 0;
}

void deinit_dw_eh()
{
  myfree(nametab_dw_eh[0]); // range      0 -      4
  myfree(nametab_dw_eh[1]); // range      9 -     12
  myfree(nametab_dw_eh[2]); // range     16 -     16
  myfree(nametab_dw_eh[3]); // range     32 -     32
  myfree(nametab_dw_eh[4]); // range     48 -     48
  myfree(nametab_dw_eh[5]); // range     64 -     64
  myfree(nametab_dw_eh[6]); // range     80 -     80
  myfree(nametab_dw_eh[7]); // range    255 -    255
}

// ff 0,020
char **nametab_dw_end[3];
void init_dw_end()
{
  nametab_dw_end[0] = myalloc(sizeof(char *) *     3); // range      0 -      2
  nametab_dw_end[1] = myalloc(sizeof(char *) *     1); // range     64 -     64
  nametab_dw_end[2] = myalloc(sizeof(char *) *     1); // range    255 -    255
  nametab_dw_end[0][0] = "DW_END_default";
  nametab_dw_end[0][1] = "DW_END_big";
  nametab_dw_end[0][2] = "DW_END_little";
  nametab_dw_end[1][0] = "DW_END_lo_user";
  nametab_dw_end[2][0] = "DW_END_hi_user";
}
char *decode_dw_end(int v)
{
  if (v < 0 || v > 255) 
    return unmapped(v);
  if (v <= 2)
  {
    if (v < 0)
    return unmapped(v);
    return nametab_dw_end[0][v - 0];
  }
  if (v <= 64)
  {
    if (v < 64)
    return unmapped(v);
    return nametab_dw_end[1][v - 64];
  }
  if (v <= 255)
  {
    if (v < 255)
    return unmapped(v);
    return nametab_dw_end[2][v - 255];
  }
  return 0;
}

void deinit_dw_end()
{
  myfree(nametab_dw_end[0]); // range      0 -      2
  myfree(nametab_dw_end[1]); // range     64 -     64
  myfree(nametab_dw_end[2]); // range    255 -    255
}

// ff 0,005
char **nametab_dw_form[4];
void init_dw_form()
{
  nametab_dw_form[0] = myalloc(sizeof(char *) *     1); // range      1 -      1
  nametab_dw_form[1] = myalloc(sizeof(char *) *    33); // range      3 -     35
  nametab_dw_form[2] = myalloc(sizeof(char *) *     2); // range   7937 -   7938
  nametab_dw_form[3] = myalloc(sizeof(char *) *     2); // range   7968 -   7969
  nametab_dw_form[0][0] = "DW_FORM_addr";
  nametab_dw_form[1][0] = "DW_FORM_block2";
  nametab_dw_form[1][1] = "DW_FORM_block4";
  nametab_dw_form[1][2] = "DW_FORM_data2";
  nametab_dw_form[1][3] = "DW_FORM_data4";
  nametab_dw_form[1][4] = "DW_FORM_data8";
  nametab_dw_form[1][5] = "DW_FORM_string";
  nametab_dw_form[1][6] = "DW_FORM_block";
  nametab_dw_form[1][7] = "DW_FORM_block1";
  nametab_dw_form[1][8] = "DW_FORM_data1";
  nametab_dw_form[1][9] = "DW_FORM_flag";
  nametab_dw_form[1][10] = "DW_FORM_sdata";
  nametab_dw_form[1][11] = "DW_FORM_strp";
  nametab_dw_form[1][12] = "DW_FORM_udata";
  nametab_dw_form[1][13] = "DW_FORM_ref_addr";
  nametab_dw_form[1][14] = "DW_FORM_ref1";
  nametab_dw_form[1][15] = "DW_FORM_ref2";
  nametab_dw_form[1][16] = "DW_FORM_ref4";
  nametab_dw_form[1][17] = "DW_FORM_ref8";
  nametab_dw_form[1][18] = "DW_FORM_ref_udata";
  nametab_dw_form[1][19] = "DW_FORM_indirect";
  nametab_dw_form[1][20] = "DW_FORM_sec_offset";
  nametab_dw_form[1][21] = "DW_FORM_exprloc";
  nametab_dw_form[1][22] = "DW_FORM_flag_present";
  nametab_dw_form[1][23] = "DW_FORM_strx";
  nametab_dw_form[1][24] = "DW_FORM_addrx";
  nametab_dw_form[1][25] = "DW_FORM_ref_sup";
  nametab_dw_form[1][26] = "DW_FORM_strp_sup";
  nametab_dw_form[1][27] = "DW_FORM_data16";
  nametab_dw_form[1][28] = "DW_FORM_line_strp";
  nametab_dw_form[1][29] = "DW_FORM_ref_sig8";
  nametab_dw_form[1][30] = "DW_FORM_implicit_const";
  nametab_dw_form[1][31] = "DW_FORM_loclistx";
  nametab_dw_form[1][32] = "DW_FORM_rnglistx";
  nametab_dw_form[2][0] = "DW_FORM_GNU_addr_index";
  nametab_dw_form[2][1] = "DW_FORM_GNU_str_index";
  nametab_dw_form[3][0] = "DW_FORM_GNU_ref_alt";
  nametab_dw_form[3][1] = "DW_FORM_GNU_strp_alt";
}
char *decode_dw_form(int v)
{
  if (v < 1 || v > 7969) 
    return unmapped(v);
  if (v <= 1)
  {
    if (v < 1)
    return unmapped(v);
    return nametab_dw_form[0][v - 1];
  }
  if (v <= 35)
  {
    if (v < 3)
    return unmapped(v);
    return nametab_dw_form[1][v - 3];
  }
  if (v <= 7938)
  {
    if (v < 7937)
    return unmapped(v);
    return nametab_dw_form[2][v - 7937];
  }
  if (v <= 7969)
  {
    if (v < 7968)
    return unmapped(v);
    return nametab_dw_form[3][v - 7968];
  }
  return 0;
}

void deinit_dw_form()
{
  myfree(nametab_dw_form[0]); // range      1 -      1
  myfree(nametab_dw_form[1]); // range      3 -     35
  myfree(nametab_dw_form[2]); // range   7937 -   7938
  myfree(nametab_dw_form[3]); // range   7968 -   7969
}

// ff 1,000
char *nametab_dw_frame[109];
void init_dw_frame()
{
  nametab_dw_frame[   0] = "DW_FRAME_CFA_COL";
  nametab_dw_frame[   1] = "DW_FRAME_REG1";
  nametab_dw_frame[   2] = "DW_FRAME_REG2";
  nametab_dw_frame[   3] = "DW_FRAME_REG3";
  nametab_dw_frame[   4] = "DW_FRAME_REG4";
  nametab_dw_frame[   5] = "DW_FRAME_REG5";
  nametab_dw_frame[   6] = "DW_FRAME_REG6";
  nametab_dw_frame[   7] = "DW_FRAME_REG7";
  nametab_dw_frame[   8] = "DW_FRAME_REG8";
  nametab_dw_frame[   9] = "DW_FRAME_REG9";
  nametab_dw_frame[  10] = "DW_FRAME_REG10";
  nametab_dw_frame[  11] = "DW_FRAME_REG11";
  nametab_dw_frame[  12] = "DW_FRAME_REG12";
  nametab_dw_frame[  13] = "DW_FRAME_REG13";
  nametab_dw_frame[  14] = "DW_FRAME_REG14";
  nametab_dw_frame[  15] = "DW_FRAME_REG15";
  nametab_dw_frame[  16] = "DW_FRAME_REG16";
  nametab_dw_frame[  17] = "DW_FRAME_REG17";
  nametab_dw_frame[  18] = "DW_FRAME_REG18";
  nametab_dw_frame[  19] = "DW_FRAME_REG19";
  nametab_dw_frame[  20] = "DW_FRAME_REG20";
  nametab_dw_frame[  21] = "DW_FRAME_REG21";
  nametab_dw_frame[  22] = "DW_FRAME_REG22";
  nametab_dw_frame[  23] = "DW_FRAME_REG23";
  nametab_dw_frame[  24] = "DW_FRAME_REG24";
  nametab_dw_frame[  25] = "DW_FRAME_REG25";
  nametab_dw_frame[  26] = "DW_FRAME_REG26";
  nametab_dw_frame[  27] = "DW_FRAME_REG27";
  nametab_dw_frame[  28] = "DW_FRAME_REG28";
  nametab_dw_frame[  29] = "DW_FRAME_REG29";
  nametab_dw_frame[  30] = "DW_FRAME_REG30";
  nametab_dw_frame[  31] = "DW_FRAME_REG31";
  nametab_dw_frame[  32] = "DW_FRAME_FREG0";
  nametab_dw_frame[  33] = "DW_FRAME_FREG1";
  nametab_dw_frame[  34] = "DW_FRAME_FREG2";
  nametab_dw_frame[  35] = "DW_FRAME_FREG3";
  nametab_dw_frame[  36] = "DW_FRAME_FREG4";
  nametab_dw_frame[  37] = "DW_FRAME_FREG5";
  nametab_dw_frame[  38] = "DW_FRAME_FREG6";
  nametab_dw_frame[  39] = "DW_FRAME_FREG7";
  nametab_dw_frame[  40] = "DW_FRAME_FREG8";
  nametab_dw_frame[  41] = "DW_FRAME_FREG9";
  nametab_dw_frame[  42] = "DW_FRAME_FREG10";
  nametab_dw_frame[  43] = "DW_FRAME_FREG11";
  nametab_dw_frame[  44] = "DW_FRAME_FREG12";
  nametab_dw_frame[  45] = "DW_FRAME_FREG13";
  nametab_dw_frame[  46] = "DW_FRAME_FREG14";
  nametab_dw_frame[  47] = "DW_FRAME_FREG15";
  nametab_dw_frame[  48] = "DW_FRAME_FREG16";
  nametab_dw_frame[  49] = "DW_FRAME_FREG17";
  nametab_dw_frame[  50] = "DW_FRAME_FREG18";
  nametab_dw_frame[  51] = "DW_FRAME_FREG19";
  nametab_dw_frame[  52] = "DW_FRAME_FREG20";
  nametab_dw_frame[  53] = "DW_FRAME_FREG21";
  nametab_dw_frame[  54] = "DW_FRAME_FREG22";
  nametab_dw_frame[  55] = "DW_FRAME_FREG23";
  nametab_dw_frame[  56] = "DW_FRAME_FREG24";
  nametab_dw_frame[  57] = "DW_FRAME_FREG25";
  nametab_dw_frame[  58] = "DW_FRAME_FREG26";
  nametab_dw_frame[  59] = "DW_FRAME_FREG27";
  nametab_dw_frame[  60] = "DW_FRAME_FREG28";
  nametab_dw_frame[  61] = "DW_FRAME_FREG29";
  nametab_dw_frame[  62] = "DW_FRAME_FREG30";
  nametab_dw_frame[  63] = "DW_FRAME_FREG31";
  nametab_dw_frame[  64] = "DW_FRAME_FREG32";
  nametab_dw_frame[  65] = "DW_FRAME_FREG33";
  nametab_dw_frame[  66] = "DW_FRAME_FREG34";
  nametab_dw_frame[  67] = "DW_FRAME_FREG35";
  nametab_dw_frame[  68] = "DW_FRAME_FREG36";
  nametab_dw_frame[  69] = "DW_FRAME_FREG37";
  nametab_dw_frame[  70] = "DW_FRAME_FREG38";
  nametab_dw_frame[  71] = "DW_FRAME_FREG39";
  nametab_dw_frame[  72] = "DW_FRAME_FREG40";
  nametab_dw_frame[  73] = "DW_FRAME_FREG41";
  nametab_dw_frame[  74] = "DW_FRAME_FREG42";
  nametab_dw_frame[  75] = "DW_FRAME_FREG43";
  nametab_dw_frame[  76] = "DW_FRAME_FREG44";
  nametab_dw_frame[  77] = "DW_FRAME_FREG45";
  nametab_dw_frame[  78] = "DW_FRAME_FREG46";
  nametab_dw_frame[  79] = "DW_FRAME_FREG47";
  nametab_dw_frame[  80] = "DW_FRAME_FREG48";
  nametab_dw_frame[  81] = "DW_FRAME_FREG49";
  nametab_dw_frame[  82] = "DW_FRAME_FREG50";
  nametab_dw_frame[  83] = "DW_FRAME_FREG51";
  nametab_dw_frame[  84] = "DW_FRAME_FREG52";
  nametab_dw_frame[  85] = "DW_FRAME_FREG53";
  nametab_dw_frame[  86] = "DW_FRAME_FREG54";
  nametab_dw_frame[  87] = "DW_FRAME_FREG55";
  nametab_dw_frame[  88] = "DW_FRAME_FREG56";
  nametab_dw_frame[  89] = "DW_FRAME_FREG57";
  nametab_dw_frame[  90] = "DW_FRAME_FREG58";
  nametab_dw_frame[  91] = "DW_FRAME_FREG59";
  nametab_dw_frame[  92] = "DW_FRAME_FREG60";
  nametab_dw_frame[  93] = "DW_FRAME_FREG61";
  nametab_dw_frame[  94] = "DW_FRAME_FREG62";
  nametab_dw_frame[  95] = "DW_FRAME_FREG63";
  nametab_dw_frame[  96] = "DW_FRAME_FREG64";
  nametab_dw_frame[  97] = "DW_FRAME_FREG65";
  nametab_dw_frame[  98] = "DW_FRAME_FREG66";
  nametab_dw_frame[  99] = "DW_FRAME_FREG67";
  nametab_dw_frame[ 100] = "DW_FRAME_FREG68";
  nametab_dw_frame[ 101] = "DW_FRAME_FREG69";
  nametab_dw_frame[ 102] = "DW_FRAME_FREG70";
  nametab_dw_frame[ 103] = "DW_FRAME_FREG71";
  nametab_dw_frame[ 104] = "DW_FRAME_FREG72";
  nametab_dw_frame[ 105] = "DW_FRAME_FREG73";
  nametab_dw_frame[ 106] = "DW_FRAME_FREG74";
  nametab_dw_frame[ 107] = "DW_FRAME_FREG75";
  nametab_dw_frame[ 108] = "DW_FRAME_FREG76";
}

char *decode_dw_frame(int v)
{
  if (v < 0 || v > 108) 
    return unmapped(v);
  return nametab_dw_frame[v - 0];
}

void deinit_dw_frame()
{
}

// ff 1,000
char *nametab_dw_id[4];
void init_dw_id()
{
  nametab_dw_id[   0] = "DW_ID_case_sensitive";
  nametab_dw_id[   1] = "DW_ID_up_case";
  nametab_dw_id[   2] = "DW_ID_down_case";
  nametab_dw_id[   3] = "DW_ID_case_insensitive";
}

char *decode_dw_id(int v)
{
  if (v < 0 || v > 3) 
    return unmapped(v);
  return nametab_dw_id[v - 0];
}

void deinit_dw_id()
{
}

// ff 0,001
char **nametab_dw_idx[3];
void init_dw_idx()
{
  nametab_dw_idx[0] = myalloc(sizeof(char *) *     5); // range      1 -      5
  nametab_dw_idx[1] = myalloc(sizeof(char *) *     1); // range   4095 -   4095
  nametab_dw_idx[2] = myalloc(sizeof(char *) *     1); // range   8192 -   8192
  nametab_dw_idx[0][0] = "DW_IDX_compile_unit";
  nametab_dw_idx[0][1] = "DW_IDX_type_unit";
  nametab_dw_idx[0][2] = "DW_IDX_die_offset";
  nametab_dw_idx[0][3] = "DW_IDX_parent";
  nametab_dw_idx[0][4] = "DW_IDX_type_hash";
  nametab_dw_idx[1][0] = "DW_IDX_hi_user";
  nametab_dw_idx[2][0] = "DW_IDX_lo_user";
}
char *decode_dw_idx(int v)
{
  if (v < 1 || v > 8192) 
    return unmapped(v);
  if (v <= 5)
  {
    if (v < 1)
    return unmapped(v);
    return nametab_dw_idx[0][v - 1];
  }
  if (v <= 4095)
  {
    if (v < 4095)
    return unmapped(v);
    return nametab_dw_idx[1][v - 4095];
  }
  if (v <= 8192)
  {
    if (v < 8192)
    return unmapped(v);
    return nametab_dw_idx[2][v - 8192];
  }
  return 0;
}

void deinit_dw_idx()
{
  myfree(nametab_dw_idx[0]); // range      1 -      5
  myfree(nametab_dw_idx[1]); // range   4095 -   4095
  myfree(nametab_dw_idx[2]); // range   8192 -   8192
}

// ff 1,000
char *nametab_dw_inl[4];
void init_dw_inl()
{
  nametab_dw_inl[   0] = "DW_INL_not_inlined";
  nametab_dw_inl[   1] = "DW_INL_inlined";
  nametab_dw_inl[   2] = "DW_INL_declared_not_inlined";
  nametab_dw_inl[   3] = "DW_INL_declared_inlined";
}

char *decode_dw_inl(int v)
{
  if (v < 0 || v > 3) 
    return unmapped(v);
  return nametab_dw_inl[v - 0];
}

void deinit_dw_inl()
{
}

// ff 1,000
char *nametab_dw_isa[3];
void init_dw_isa()
{
  nametab_dw_isa[   0] = "DW_ISA_UNKNOWN";
  nametab_dw_isa[   1] = "DW_ISA_ARM_thumb";
  nametab_dw_isa[   2] = "DW_ISA_ARM_arm";
}

char *decode_dw_isa(int v)
{
  if (v < 0 || v > 2) 
    return unmapped(v);
  return nametab_dw_isa[v - 0];
}

void deinit_dw_isa()
{
}

// ff 0,001
char **nametab_dw_lang[6];
void init_dw_lang()
{
  nametab_dw_lang[0] = myalloc(sizeof(char *) *    36); // range      1 -     36
  nametab_dw_lang[1] = myalloc(sizeof(char *) *     2); // range  32768 -  32769
  nametab_dw_lang[2] = myalloc(sizeof(char *) *     1); // range  34661 -  34661
  nametab_dw_lang[3] = myalloc(sizeof(char *) *     1); // range  36865 -  36865
  nametab_dw_lang[4] = myalloc(sizeof(char *) *     1); // range  37121 -  37121
  nametab_dw_lang[5] = myalloc(sizeof(char *) *     1); // range  65535 -  65535
  nametab_dw_lang[0][0] = "DW_LANG_C89";
  nametab_dw_lang[0][1] = "DW_LANG_C";
  nametab_dw_lang[0][2] = "DW_LANG_Ada83";
  nametab_dw_lang[0][3] = "DW_LANG_C_plus_plus";
  nametab_dw_lang[0][4] = "DW_LANG_Cobol74";
  nametab_dw_lang[0][5] = "DW_LANG_Cobol85";
  nametab_dw_lang[0][6] = "DW_LANG_Fortran77";
  nametab_dw_lang[0][7] = "DW_LANG_Fortran90";
  nametab_dw_lang[0][8] = "DW_LANG_Pascal83";
  nametab_dw_lang[0][9] = "DW_LANG_Modula2";
  nametab_dw_lang[0][10] = "DW_LANG_Java";
  nametab_dw_lang[0][11] = "DW_LANG_C99";
  nametab_dw_lang[0][12] = "DW_LANG_Ada95";
  nametab_dw_lang[0][13] = "DW_LANG_Fortran95";
  nametab_dw_lang[0][14] = "DW_LANG_PLI";
  nametab_dw_lang[0][15] = "DW_LANG_ObjC";
  nametab_dw_lang[0][16] = "DW_LANG_ObjC_plus_plus";
  nametab_dw_lang[0][17] = "DW_LANG_UPC";
  nametab_dw_lang[0][18] = "DW_LANG_D";
  nametab_dw_lang[0][19] = "DW_LANG_Python";
  nametab_dw_lang[0][20] = "DW_LANG_OpenCL";
  nametab_dw_lang[0][21] = "DW_LANG_Go";
  nametab_dw_lang[0][22] = "DW_LANG_Modula3";
  nametab_dw_lang[0][23] = "DW_LANG_Haskel";
  nametab_dw_lang[0][24] = "DW_LANG_C_plus_plus_03";
  nametab_dw_lang[0][25] = "DW_LANG_C_plus_plus_11";
  nametab_dw_lang[0][26] = "DW_LANG_OCaml";
  nametab_dw_lang[0][27] = "DW_LANG_Rust";
  nametab_dw_lang[0][28] = "DW_LANG_C11";
  nametab_dw_lang[0][29] = "DW_LANG_Swift";
  nametab_dw_lang[0][30] = "DW_LANG_Julia";
  nametab_dw_lang[0][31] = "DW_LANG_Dylan";
  nametab_dw_lang[0][32] = "DW_LANG_C_plus_plus_14";
  nametab_dw_lang[0][33] = "DW_LANG_Fortran03";
  nametab_dw_lang[0][34] = "DW_LANG_Fortran08";
  nametab_dw_lang[0][35] = "DW_LANG_RenderScript";
  nametab_dw_lang[1][0] = "DW_LANG_lo_user";
  nametab_dw_lang[1][1] = "DW_LANG_Mips_Assembler";
  nametab_dw_lang[2][0] = "DW_LANG_Upc";
  nametab_dw_lang[3][0] = "DW_LANG_SUN_Assembler";
  nametab_dw_lang[4][0] = "DW_LANG_ALTIUM_Assembler";
  nametab_dw_lang[5][0] = "DW_LANG_hi_user";
}
char *decode_dw_lang(int v)
{
  if (v < 1 || v > 65535) 
    return unmapped(v);
  if (v <= 36)
  {
    if (v < 1)
    return unmapped(v);
    return nametab_dw_lang[0][v - 1];
  }
  if (v <= 32769)
  {
    if (v < 32768)
    return unmapped(v);
    return nametab_dw_lang[1][v - 32768];
  }
  if (v <= 34661)
  {
    if (v < 34661)
    return unmapped(v);
    return nametab_dw_lang[2][v - 34661];
  }
  if (v <= 36865)
  {
    if (v < 36865)
    return unmapped(v);
    return nametab_dw_lang[3][v - 36865];
  }
  if (v <= 37121)
  {
    if (v < 37121)
    return unmapped(v);
    return nametab_dw_lang[4][v - 37121];
  }
  if (v <= 65535)
  {
    if (v < 65535)
    return unmapped(v);
    return nametab_dw_lang[5][v - 65535];
  }
  return 0;
}

void deinit_dw_lang()
{
  myfree(nametab_dw_lang[0]); // range      1 -     36
  myfree(nametab_dw_lang[1]); // range  32768 -  32769
  myfree(nametab_dw_lang[2]); // range  34661 -  34661
  myfree(nametab_dw_lang[3]); // range  36865 -  36865
  myfree(nametab_dw_lang[4]); // range  37121 -  37121
  myfree(nametab_dw_lang[5]); // range  65535 -  65535
}

// ff 1,000
char *nametab_dw_lle[9];
void init_dw_lle()
{
  nametab_dw_lle[   0] = "DW_LLE_end_of_list";
  nametab_dw_lle[   1] = "DW_LLE_base_addressx";
  nametab_dw_lle[   2] = "DW_LLE_startx_endx";
  nametab_dw_lle[   3] = "DW_LLE_startx_length";
  nametab_dw_lle[   4] = "DW_LLE_offset_pair";
  nametab_dw_lle[   5] = "DW_LLE_default_location";
  nametab_dw_lle[   6] = "DW_LLE_base_address";
  nametab_dw_lle[   7] = "DW_LLE_start_end";
  nametab_dw_lle[   8] = "DW_LLE_start_length";
}

char *decode_dw_lle(int v)
{
  if (v < 0 || v > 8) 
    return unmapped(v);
  return nametab_dw_lle[v - 0];
}

void deinit_dw_lle()
{
}

// ff 1,000
char *nametab_dw_llex[5];
void init_dw_llex()
{
  nametab_dw_llex[   0] = "DW_LLEX_end_of_list_entry";
  nametab_dw_llex[   1] = "DW_LLEX_base_address_selection_entry";
  nametab_dw_llex[   2] = "DW_LLEX_start_end_entry";
  nametab_dw_llex[   3] = "DW_LLEX_start_length_entry";
  nametab_dw_llex[   4] = "DW_LLEX_offset_pair_entry";
}

char *decode_dw_llex(int v)
{
  if (v < 0 || v > 4) 
    return unmapped(v);
  return nametab_dw_llex[v - 0];
}

void deinit_dw_llex()
{
}

// ff 0,001
char **nametab_dw_lnct[3];
void init_dw_lnct()
{
  nametab_dw_lnct[0] = myalloc(sizeof(char *) *     8); // range      1 -      8
  nametab_dw_lnct[1] = myalloc(sizeof(char *) *     1); // range   8192 -   8192
  nametab_dw_lnct[2] = myalloc(sizeof(char *) *     1); // range  16383 -  16383
  nametab_dw_lnct[0][0] = "DW_LNCT_path";
  nametab_dw_lnct[0][1] = "DW_LNCT_directory_index";
  nametab_dw_lnct[0][2] = "DW_LNCT_timestamp";
  nametab_dw_lnct[0][3] = "DW_LNCT_size";
  nametab_dw_lnct[0][4] = "DW_LNCT_MD5";
  nametab_dw_lnct[0][5] = "DW_LNCT_GNU_subprogram_name";
  nametab_dw_lnct[0][6] = "DW_LNCT_GNU_decl_file";
  nametab_dw_lnct[0][7] = "DW_LNCT_GNU_decl_line";
  nametab_dw_lnct[1][0] = "DW_LNCT_lo_user";
  nametab_dw_lnct[2][0] = "DW_LNCT_hi_user";
}
char *decode_dw_lnct(int v)
{
  if (v < 1 || v > 16383) 
    return unmapped(v);
  if (v <= 8)
  {
    if (v < 1)
    return unmapped(v);
    return nametab_dw_lnct[0][v - 1];
  }
  if (v <= 8192)
  {
    if (v < 8192)
    return unmapped(v);
    return nametab_dw_lnct[1][v - 8192];
  }
  if (v <= 16383)
  {
    if (v < 16383)
    return unmapped(v);
    return nametab_dw_lnct[2][v - 16383];
  }
  return 0;
}

void deinit_dw_lnct()
{
  myfree(nametab_dw_lnct[0]); // range      1 -      8
  myfree(nametab_dw_lnct[1]); // range   8192 -   8192
  myfree(nametab_dw_lnct[2]); // range  16383 -  16383
}

// ff 0,067
char **nametab_dw_lne[5];
void init_dw_lne()
{
  nametab_dw_lne[0] = myalloc(sizeof(char *) *     5); // range      1 -      5
  nametab_dw_lne[1] = myalloc(sizeof(char *) *     9); // range     17 -     25
  nametab_dw_lne[2] = myalloc(sizeof(char *) *     1); // range     32 -     32
  nametab_dw_lne[3] = myalloc(sizeof(char *) *     1); // range    128 -    128
  nametab_dw_lne[4] = myalloc(sizeof(char *) *     1); // range    255 -    255
  nametab_dw_lne[0][0] = "DW_LNE_end_sequence";
  nametab_dw_lne[0][1] = "DW_LNE_set_address";
  nametab_dw_lne[0][2] = "DW_LNE_define_file";
  nametab_dw_lne[0][3] = "DW_LNE_set_discriminator";
  nametab_dw_lne[0][4] = "DW_LNE_define_file_MD5";
  nametab_dw_lne[1][0] = "DW_LNE_HP_negate_is_UV_update";
  nametab_dw_lne[1][1] = "DW_LNE_HP_push_context";
  nametab_dw_lne[1][2] = "DW_LNE_HP_pop_context";
  nametab_dw_lne[1][3] = "DW_LNE_HP_set_file_line_column";
  nametab_dw_lne[1][4] = "DW_LNE_HP_set_routine_name";
  nametab_dw_lne[1][5] = "DW_LNE_HP_set_sequence";
  nametab_dw_lne[1][6] = "DW_LNE_HP_negate_post_semantics";
  nametab_dw_lne[1][7] = "DW_LNE_HP_negate_function_exit";
  nametab_dw_lne[1][8] = "DW_LNE_HP_negate_front_end_logical";
  nametab_dw_lne[2][0] = "DW_LNE_HP_define_proc";
  nametab_dw_lne[3][0] = "DW_LNE_lo_user";
  nametab_dw_lne[4][0] = "DW_LNE_hi_user";
}
char *decode_dw_lne(int v)
{
  if (v < 1 || v > 255) 
    return unmapped(v);
  if (v <= 5)
  {
    if (v < 1)
    return unmapped(v);
    return nametab_dw_lne[0][v - 1];
  }
  if (v <= 25)
  {
    if (v < 17)
    return unmapped(v);
    return nametab_dw_lne[1][v - 17];
  }
  if (v <= 32)
  {
    if (v < 32)
    return unmapped(v);
    return nametab_dw_lne[2][v - 32];
  }
  if (v <= 128)
  {
    if (v < 128)
    return unmapped(v);
    return nametab_dw_lne[3][v - 128];
  }
  if (v <= 255)
  {
    if (v < 255)
    return unmapped(v);
    return nametab_dw_lne[4][v - 255];
  }
  return 0;
}

void deinit_dw_lne()
{
  myfree(nametab_dw_lne[0]); // range      1 -      5
  myfree(nametab_dw_lne[1]); // range     17 -     25
  myfree(nametab_dw_lne[2]); // range     32 -     32
  myfree(nametab_dw_lne[3]); // range    128 -    128
  myfree(nametab_dw_lne[4]); // range    255 -    255
}

// ff 1,000
char *nametab_dw_lns[15];
void init_dw_lns()
{
  nametab_dw_lns[   0] = "DW_LNS_copy";
  nametab_dw_lns[   1] = "DW_LNS_advance_pc";
  nametab_dw_lns[   2] = "DW_LNS_advance_line";
  nametab_dw_lns[   3] = "DW_LNS_set_file";
  nametab_dw_lns[   4] = "DW_LNS_set_column";
  nametab_dw_lns[   5] = "DW_LNS_negate_stmt";
  nametab_dw_lns[   6] = "DW_LNS_set_basic_block";
  nametab_dw_lns[   7] = "DW_LNS_const_add_pc";
  nametab_dw_lns[   8] = "DW_LNS_fixed_advance_pc";
  nametab_dw_lns[   9] = "DW_LNS_set_prologue_end";
  nametab_dw_lns[  10] = "DW_LNS_set_epilogue_begin";
  nametab_dw_lns[  11] = "DW_LNS_set_isa";
  nametab_dw_lns[  12] = "DW_LNS_set_subprogram";
  nametab_dw_lns[  13] = "DW_LNS_inlined_call";
  nametab_dw_lns[  14] = "DW_LNS_pop_context";
}

char *decode_dw_lns(int v)
{
  if (v < 1 || v > 15) 
    return unmapped(v);
  return nametab_dw_lns[v - 1];
}

void deinit_dw_lns()
{
}

// ff 0,020
char **nametab_dw_macinfo[2];
void init_dw_macinfo()
{
  nametab_dw_macinfo[0] = myalloc(sizeof(char *) *     4); // range      1 -      4
  nametab_dw_macinfo[1] = myalloc(sizeof(char *) *     1); // range    255 -    255
  nametab_dw_macinfo[0][0] = "DW_MACINFO_define";
  nametab_dw_macinfo[0][1] = "DW_MACINFO_undef";
  nametab_dw_macinfo[0][2] = "DW_MACINFO_start_file";
  nametab_dw_macinfo[0][3] = "DW_MACINFO_end_file";
  nametab_dw_macinfo[1][0] = "DW_MACINFO_vendor_ext";
}
char *decode_dw_macinfo(int v)
{
  if (v < 1 || v > 255) 
    return unmapped(v);
  if (v <= 4)
  {
    if (v < 1)
    return unmapped(v);
    return nametab_dw_macinfo[0][v - 1];
  }
  if (v <= 255)
  {
    if (v < 255)
    return unmapped(v);
    return nametab_dw_macinfo[1][v - 255];
  }
  return 0;
}

void deinit_dw_macinfo()
{
  myfree(nametab_dw_macinfo[0]); // range      1 -      4
  myfree(nametab_dw_macinfo[1]); // range    255 -    255
}

// ff 0,055
char **nametab_dw_macro[3];
void init_dw_macro()
{
  nametab_dw_macro[0] = myalloc(sizeof(char *) *    12); // range      1 -     12
  nametab_dw_macro[1] = myalloc(sizeof(char *) *     1); // range    224 -    224
  nametab_dw_macro[2] = myalloc(sizeof(char *) *     1); // range    255 -    255
  nametab_dw_macro[0][0] = "DW_MACRO_define";
  nametab_dw_macro[0][1] = "DW_MACRO_undef";
  nametab_dw_macro[0][2] = "DW_MACRO_start_file";
  nametab_dw_macro[0][3] = "DW_MACRO_end_file";
  nametab_dw_macro[0][4] = "DW_MACRO_define_strp";
  nametab_dw_macro[0][5] = "DW_MACRO_undef_strp";
  nametab_dw_macro[0][6] = "DW_MACRO_import";
  nametab_dw_macro[0][7] = "DW_MACRO_define_sup";
  nametab_dw_macro[0][8] = "DW_MACRO_undef_sup";
  nametab_dw_macro[0][9] = "DW_MACRO_import_sup";
  nametab_dw_macro[0][10] = "DW_MACRO_define_strx";
  nametab_dw_macro[0][11] = "DW_MACRO_undef_strx";
  nametab_dw_macro[1][0] = "DW_MACRO_lo_user";
  nametab_dw_macro[2][0] = "DW_MACRO_hi_user";
}
char *decode_dw_macro(int v)
{
  if (v < 1 || v > 255) 
    return unmapped(v);
  if (v <= 12)
  {
    if (v < 1)
    return unmapped(v);
    return nametab_dw_macro[0][v - 1];
  }
  if (v <= 224)
  {
    if (v < 224)
    return unmapped(v);
    return nametab_dw_macro[1][v - 224];
  }
  if (v <= 255)
  {
    if (v < 255)
    return unmapped(v);
    return nametab_dw_macro[2][v - 255];
  }
  return 0;
}

void deinit_dw_macro()
{
  myfree(nametab_dw_macro[0]); // range      1 -     12
  myfree(nametab_dw_macro[1]); // range    224 -    224
  myfree(nametab_dw_macro[2]); // range    255 -    255
}

// ff 0,735
char **nametab_dw_op[7];
void init_dw_op()
{
  nametab_dw_op[0] = myalloc(sizeof(char *) *     1); // range      3 -      3
  nametab_dw_op[1] = myalloc(sizeof(char *) *     1); // range      6 -      6
  nametab_dw_op[2] = myalloc(sizeof(char *) *   162); // range      8 -    169
  nametab_dw_op[3] = myalloc(sizeof(char *) *     7); // range    224 -    230
  nametab_dw_op[4] = myalloc(sizeof(char *) *     1); // range    232 -    232
  nametab_dw_op[5] = myalloc(sizeof(char *) *    13); // range    240 -    252
  nametab_dw_op[6] = myalloc(sizeof(char *) *     1); // range    255 -    255
  nametab_dw_op[0][0] = "DW_OP_addr";
  nametab_dw_op[1][0] = "DW_OP_deref";
  nametab_dw_op[2][0] = "DW_OP_const1u";
  nametab_dw_op[2][1] = "DW_OP_const1s";
  nametab_dw_op[2][2] = "DW_OP_const2u";
  nametab_dw_op[2][3] = "DW_OP_const2s";
  nametab_dw_op[2][4] = "DW_OP_const4u";
  nametab_dw_op[2][5] = "DW_OP_const4s";
  nametab_dw_op[2][6] = "DW_OP_const8u";
  nametab_dw_op[2][7] = "DW_OP_const8s";
  nametab_dw_op[2][8] = "DW_OP_constu";
  nametab_dw_op[2][9] = "DW_OP_consts";
  nametab_dw_op[2][10] = "DW_OP_dup";
  nametab_dw_op[2][11] = "DW_OP_drop";
  nametab_dw_op[2][12] = "DW_OP_over";
  nametab_dw_op[2][13] = "DW_OP_pick";
  nametab_dw_op[2][14] = "DW_OP_swap";
  nametab_dw_op[2][15] = "DW_OP_rot";
  nametab_dw_op[2][16] = "DW_OP_xderef";
  nametab_dw_op[2][17] = "DW_OP_abs";
  nametab_dw_op[2][18] = "DW_OP_and";
  nametab_dw_op[2][19] = "DW_OP_div";
  nametab_dw_op[2][20] = "DW_OP_minus";
  nametab_dw_op[2][21] = "DW_OP_mod";
  nametab_dw_op[2][22] = "DW_OP_mul";
  nametab_dw_op[2][23] = "DW_OP_neg";
  nametab_dw_op[2][24] = "DW_OP_not";
  nametab_dw_op[2][25] = "DW_OP_or";
  nametab_dw_op[2][26] = "DW_OP_plus";
  nametab_dw_op[2][27] = "DW_OP_plus_uconst";
  nametab_dw_op[2][28] = "DW_OP_shl";
  nametab_dw_op[2][29] = "DW_OP_shr";
  nametab_dw_op[2][30] = "DW_OP_shra";
  nametab_dw_op[2][31] = "DW_OP_xor";
  nametab_dw_op[2][32] = "DW_OP_bra";
  nametab_dw_op[2][33] = "DW_OP_eq";
  nametab_dw_op[2][34] = "DW_OP_ge";
  nametab_dw_op[2][35] = "DW_OP_gt";
  nametab_dw_op[2][36] = "DW_OP_le";
  nametab_dw_op[2][37] = "DW_OP_lt";
  nametab_dw_op[2][38] = "DW_OP_ne";
  nametab_dw_op[2][39] = "DW_OP_skip";
  nametab_dw_op[2][40] = "DW_OP_lit0";
  nametab_dw_op[2][41] = "DW_OP_lit1";
  nametab_dw_op[2][42] = "DW_OP_lit2";
  nametab_dw_op[2][43] = "DW_OP_lit3";
  nametab_dw_op[2][44] = "DW_OP_lit4";
  nametab_dw_op[2][45] = "DW_OP_lit5";
  nametab_dw_op[2][46] = "DW_OP_lit6";
  nametab_dw_op[2][47] = "DW_OP_lit7";
  nametab_dw_op[2][48] = "DW_OP_lit8";
  nametab_dw_op[2][49] = "DW_OP_lit9";
  nametab_dw_op[2][50] = "DW_OP_lit10";
  nametab_dw_op[2][51] = "DW_OP_lit11";
  nametab_dw_op[2][52] = "DW_OP_lit12";
  nametab_dw_op[2][53] = "DW_OP_lit13";
  nametab_dw_op[2][54] = "DW_OP_lit14";
  nametab_dw_op[2][55] = "DW_OP_lit15";
  nametab_dw_op[2][56] = "DW_OP_lit16";
  nametab_dw_op[2][57] = "DW_OP_lit17";
  nametab_dw_op[2][58] = "DW_OP_lit18";
  nametab_dw_op[2][59] = "DW_OP_lit19";
  nametab_dw_op[2][60] = "DW_OP_lit20";
  nametab_dw_op[2][61] = "DW_OP_lit21";
  nametab_dw_op[2][62] = "DW_OP_lit22";
  nametab_dw_op[2][63] = "DW_OP_lit23";
  nametab_dw_op[2][64] = "DW_OP_lit24";
  nametab_dw_op[2][65] = "DW_OP_lit25";
  nametab_dw_op[2][66] = "DW_OP_lit26";
  nametab_dw_op[2][67] = "DW_OP_lit27";
  nametab_dw_op[2][68] = "DW_OP_lit28";
  nametab_dw_op[2][69] = "DW_OP_lit29";
  nametab_dw_op[2][70] = "DW_OP_lit30";
  nametab_dw_op[2][71] = "DW_OP_lit31";
  nametab_dw_op[2][72] = "DW_OP_reg0";
  nametab_dw_op[2][73] = "DW_OP_reg1";
  nametab_dw_op[2][74] = "DW_OP_reg2";
  nametab_dw_op[2][75] = "DW_OP_reg3";
  nametab_dw_op[2][76] = "DW_OP_reg4";
  nametab_dw_op[2][77] = "DW_OP_reg5";
  nametab_dw_op[2][78] = "DW_OP_reg6";
  nametab_dw_op[2][79] = "DW_OP_reg7";
  nametab_dw_op[2][80] = "DW_OP_reg8";
  nametab_dw_op[2][81] = "DW_OP_reg9";
  nametab_dw_op[2][82] = "DW_OP_reg10";
  nametab_dw_op[2][83] = "DW_OP_reg11";
  nametab_dw_op[2][84] = "DW_OP_reg12";
  nametab_dw_op[2][85] = "DW_OP_reg13";
  nametab_dw_op[2][86] = "DW_OP_reg14";
  nametab_dw_op[2][87] = "DW_OP_reg15";
  nametab_dw_op[2][88] = "DW_OP_reg16";
  nametab_dw_op[2][89] = "DW_OP_reg17";
  nametab_dw_op[2][90] = "DW_OP_reg18";
  nametab_dw_op[2][91] = "DW_OP_reg19";
  nametab_dw_op[2][92] = "DW_OP_reg20";
  nametab_dw_op[2][93] = "DW_OP_reg21";
  nametab_dw_op[2][94] = "DW_OP_reg22";
  nametab_dw_op[2][95] = "DW_OP_reg23";
  nametab_dw_op[2][96] = "DW_OP_reg24";
  nametab_dw_op[2][97] = "DW_OP_reg25";
  nametab_dw_op[2][98] = "DW_OP_reg26";
  nametab_dw_op[2][99] = "DW_OP_reg27";
  nametab_dw_op[2][100] = "DW_OP_reg28";
  nametab_dw_op[2][101] = "DW_OP_reg29";
  nametab_dw_op[2][102] = "DW_OP_reg30";
  nametab_dw_op[2][103] = "DW_OP_reg31";
  nametab_dw_op[2][104] = "DW_OP_breg0";
  nametab_dw_op[2][105] = "DW_OP_breg1";
  nametab_dw_op[2][106] = "DW_OP_breg2";
  nametab_dw_op[2][107] = "DW_OP_breg3";
  nametab_dw_op[2][108] = "DW_OP_breg4";
  nametab_dw_op[2][109] = "DW_OP_breg5";
  nametab_dw_op[2][110] = "DW_OP_breg6";
  nametab_dw_op[2][111] = "DW_OP_breg7";
  nametab_dw_op[2][112] = "DW_OP_breg8";
  nametab_dw_op[2][113] = "DW_OP_breg9";
  nametab_dw_op[2][114] = "DW_OP_breg10";
  nametab_dw_op[2][115] = "DW_OP_breg11";
  nametab_dw_op[2][116] = "DW_OP_breg12";
  nametab_dw_op[2][117] = "DW_OP_breg13";
  nametab_dw_op[2][118] = "DW_OP_breg14";
  nametab_dw_op[2][119] = "DW_OP_breg15";
  nametab_dw_op[2][120] = "DW_OP_breg16";
  nametab_dw_op[2][121] = "DW_OP_breg17";
  nametab_dw_op[2][122] = "DW_OP_breg18";
  nametab_dw_op[2][123] = "DW_OP_breg19";
  nametab_dw_op[2][124] = "DW_OP_breg20";
  nametab_dw_op[2][125] = "DW_OP_breg21";
  nametab_dw_op[2][126] = "DW_OP_breg22";
  nametab_dw_op[2][127] = "DW_OP_breg23";
  nametab_dw_op[2][128] = "DW_OP_breg24";
  nametab_dw_op[2][129] = "DW_OP_breg25";
  nametab_dw_op[2][130] = "DW_OP_breg26";
  nametab_dw_op[2][131] = "DW_OP_breg27";
  nametab_dw_op[2][132] = "DW_OP_breg28";
  nametab_dw_op[2][133] = "DW_OP_breg29";
  nametab_dw_op[2][134] = "DW_OP_breg30";
  nametab_dw_op[2][135] = "DW_OP_breg31";
  nametab_dw_op[2][136] = "DW_OP_regx";
  nametab_dw_op[2][137] = "DW_OP_fbreg";
  nametab_dw_op[2][138] = "DW_OP_bregx";
  nametab_dw_op[2][139] = "DW_OP_piece";
  nametab_dw_op[2][140] = "DW_OP_deref_size";
  nametab_dw_op[2][141] = "DW_OP_xderef_size";
  nametab_dw_op[2][142] = "DW_OP_nop";
  nametab_dw_op[2][143] = "DW_OP_push_object_address";
  nametab_dw_op[2][144] = "DW_OP_call2";
  nametab_dw_op[2][145] = "DW_OP_call4";
  nametab_dw_op[2][146] = "DW_OP_call_ref";
  nametab_dw_op[2][147] = "DW_OP_form_tls_address";
  nametab_dw_op[2][148] = "DW_OP_call_frame_cfa";
  nametab_dw_op[2][149] = "DW_OP_bit_piece";
  nametab_dw_op[2][150] = "DW_OP_implicit_value";
  nametab_dw_op[2][151] = "DW_OP_stack_value";
  nametab_dw_op[2][152] = "DW_OP_implicit_pointer";
  nametab_dw_op[2][153] = "DW_OP_addrx";
  nametab_dw_op[2][154] = "DW_OP_constx";
  nametab_dw_op[2][155] = "DW_OP_entry_value";
  nametab_dw_op[2][156] = "DW_OP_const_type";
  nametab_dw_op[2][157] = "DW_OP_regval_type";
  nametab_dw_op[2][158] = "DW_OP_deref_type";
  nametab_dw_op[2][159] = "DW_OP_xderef_type";
  nametab_dw_op[2][160] = "DW_OP_convert";
  nametab_dw_op[2][161] = "DW_OP_reinterpret";
  nametab_dw_op[3][0] = "DW_OP_HP_unknown";
  nametab_dw_op[3][1] = "DW_OP_HP_is_value";
  nametab_dw_op[3][2] = "DW_OP_HP_fltconst4";
  nametab_dw_op[3][3] = "DW_OP_HP_fltconst8";
  nametab_dw_op[3][4] = "DW_OP_HP_mod_range";
  nametab_dw_op[3][5] = "DW_OP_HP_unmod_range";
  nametab_dw_op[3][6] = "DW_OP_HP_tls";
  nametab_dw_op[4][0] = "DW_OP_INTEL_bit_piece";
  nametab_dw_op[5][0] = "DW_OP_APPLE_uninit";
  nametab_dw_op[5][1] = "DW_OP_GNU_encoded_addr";
  nametab_dw_op[5][2] = "DW_OP_GNU_implicit_pointer";
  nametab_dw_op[5][3] = "DW_OP_GNU_entry_value";
  nametab_dw_op[5][4] = "DW_OP_GNU_const_type";
  nametab_dw_op[5][5] = "DW_OP_GNU_regval_type";
  nametab_dw_op[5][6] = "DW_OP_GNU_deref_type";
  nametab_dw_op[5][7] = "DW_OP_GNU_convert";
  nametab_dw_op[5][8] = "DW_OP_PGI_omp_thread_num";
  nametab_dw_op[5][9] = "DW_OP_GNU_reinterpret";
  nametab_dw_op[5][10] = "DW_OP_GNU_parameter_ref";
  nametab_dw_op[5][11] = "DW_OP_GNU_addr_index";
  nametab_dw_op[5][12] = "DW_OP_GNU_const_index";
  nametab_dw_op[6][0] = "DW_OP_hi_user";
}
char *decode_dw_op(int v)
{
  if (v < 3 || v > 255) 
    return unmapped(v);
  if (v <= 3)
  {
    if (v < 3)
    return unmapped(v);
    return nametab_dw_op[0][v - 3];
  }
  if (v <= 6)
  {
    if (v < 6)
    return unmapped(v);
    return nametab_dw_op[1][v - 6];
  }
  if (v <= 169)
  {
    if (v < 8)
    return unmapped(v);
    return nametab_dw_op[2][v - 8];
  }
  if (v <= 230)
  {
    if (v < 224)
    return unmapped(v);
    return nametab_dw_op[3][v - 224];
  }
  if (v <= 232)
  {
    if (v < 232)
    return unmapped(v);
    return nametab_dw_op[4][v - 232];
  }
  if (v <= 252)
  {
    if (v < 240)
    return unmapped(v);
    return nametab_dw_op[5][v - 240];
  }
  if (v <= 255)
  {
    if (v < 255)
    return unmapped(v);
    return nametab_dw_op[6][v - 255];
  }
  return 0;
}

void deinit_dw_op()
{
  myfree(nametab_dw_op[0]); // range      3 -      3
  myfree(nametab_dw_op[1]); // range      6 -      6
  myfree(nametab_dw_op[2]); // range      8 -    169
  myfree(nametab_dw_op[3]); // range    224 -    230
  myfree(nametab_dw_op[4]); // range    232 -    232
  myfree(nametab_dw_op[5]); // range    240 -    252
  myfree(nametab_dw_op[6]); // range    255 -    255
}

// ff 1,000
char *nametab_dw_ord[2];
void init_dw_ord()
{
  nametab_dw_ord[   0] = "DW_ORD_row_major";
  nametab_dw_ord[   1] = "DW_ORD_col_major";
}

char *decode_dw_ord(int v)
{
  if (v < 0 || v > 1) 
    return unmapped(v);
  return nametab_dw_ord[v - 0];
}

void deinit_dw_ord()
{
}

// ff 1,000
char *nametab_dw_rle[8];
void init_dw_rle()
{
  nametab_dw_rle[   0] = "DW_RLE_end_of_list";
  nametab_dw_rle[   1] = "DW_RLE_base_addressx";
  nametab_dw_rle[   2] = "DW_RLE_startx_endx";
  nametab_dw_rle[   3] = "DW_RLE_startx_length";
  nametab_dw_rle[   4] = "DW_RLE_offset_pair";
  nametab_dw_rle[   5] = "DW_RLE_base_address";
  nametab_dw_rle[   6] = "DW_RLE_start_end";
  nametab_dw_rle[   7] = "DW_RLE_start_length";
}

char *decode_dw_rle(int v)
{
  if (v < 0 || v > 7) 
    return unmapped(v);
  return nametab_dw_rle[v - 0];
}

void deinit_dw_rle()
{
}

// ff 1,000
char *nametab_dw_sect[8];
void init_dw_sect()
{
  nametab_dw_sect[   0] = "DW_SECT_INFO";
  nametab_dw_sect[   1] = "DW_SECT_TYPES";
  nametab_dw_sect[   2] = "DW_SECT_ABBREV";
  nametab_dw_sect[   3] = "DW_SECT_LINE";
  nametab_dw_sect[   4] = "DW_SECT_LOC";
  nametab_dw_sect[   5] = "DW_SECT_STR_OFFSETS";
  nametab_dw_sect[   6] = "DW_SECT_MACINFO";
  nametab_dw_sect[   7] = "DW_SECT_MACRO";
}

char *decode_dw_sect(int v)
{
  if (v < 1 || v > 8) 
    return unmapped(v);
  return nametab_dw_sect[v - 1];
}

void deinit_dw_sect()
{
}

// ff 0,002
char **nametab_dw_tag[19];
void init_dw_tag()
{
  nametab_dw_tag[0] = myalloc(sizeof(char *) *     5); // range      1 -      5
  nametab_dw_tag[1] = myalloc(sizeof(char *) *     1); // range      8 -      8
  nametab_dw_tag[2] = myalloc(sizeof(char *) *     2); // range     10 -     11
  nametab_dw_tag[3] = myalloc(sizeof(char *) *     1); // range     13 -     13
  nametab_dw_tag[4] = myalloc(sizeof(char *) *     5); // range     15 -     19
  nametab_dw_tag[5] = myalloc(sizeof(char *) *    55); // range     21 -     75
  nametab_dw_tag[6] = myalloc(sizeof(char *) *     2); // range  16512 -  16513
  nametab_dw_tag[7] = myalloc(sizeof(char *) *     1); // range  16528 -  16528
  nametab_dw_tag[8] = myalloc(sizeof(char *) *    10); // range  16641 -  16650
  nametab_dw_tag[9] = myalloc(sizeof(char *) *    13); // range  16897 -  16909
  nametab_dw_tag[10] = myalloc(sizeof(char *) *     1); // range  17151 -  17151
  nametab_dw_tag[11] = myalloc(sizeof(char *) *     3); // range  20737 -  20739
  nametab_dw_tag[12] = myalloc(sizeof(char *) *     1); // range  20753 -  20753
  nametab_dw_tag[13] = myalloc(sizeof(char *) *     3); // range  34661 -  34663
  nametab_dw_tag[14] = myalloc(sizeof(char *) *     1); // range  34946 -  34946
  nametab_dw_tag[15] = myalloc(sizeof(char *) *     1); // range  35202 -  35202
  nametab_dw_tag[16] = myalloc(sizeof(char *) *     1); // range  40960 -  40960
  nametab_dw_tag[17] = myalloc(sizeof(char *) *     1); // range  40992 -  40992
  nametab_dw_tag[18] = myalloc(sizeof(char *) *     1); // range  65535 -  65535
  nametab_dw_tag[0][0] = "DW_TAG_array_type";
  nametab_dw_tag[0][1] = "DW_TAG_class_type";
  nametab_dw_tag[0][2] = "DW_TAG_entry_point";
  nametab_dw_tag[0][3] = "DW_TAG_enumeration_type";
  nametab_dw_tag[0][4] = "DW_TAG_formal_parameter";
  nametab_dw_tag[1][0] = "DW_TAG_imported_declaration";
  nametab_dw_tag[2][0] = "DW_TAG_label";
  nametab_dw_tag[2][1] = "DW_TAG_lexical_block";
  nametab_dw_tag[3][0] = "DW_TAG_member";
  nametab_dw_tag[4][0] = "DW_TAG_pointer_type";
  nametab_dw_tag[4][1] = "DW_TAG_reference_type";
  nametab_dw_tag[4][2] = "DW_TAG_compile_unit";
  nametab_dw_tag[4][3] = "DW_TAG_string_type";
  nametab_dw_tag[4][4] = "DW_TAG_structure_type";
  nametab_dw_tag[5][0] = "DW_TAG_subroutine_type";
  nametab_dw_tag[5][1] = "DW_TAG_typedef";
  nametab_dw_tag[5][2] = "DW_TAG_union_type";
  nametab_dw_tag[5][3] = "DW_TAG_unspecified_parameters";
  nametab_dw_tag[5][4] = "DW_TAG_variant";
  nametab_dw_tag[5][5] = "DW_TAG_common_block";
  nametab_dw_tag[5][6] = "DW_TAG_common_inclusion";
  nametab_dw_tag[5][7] = "DW_TAG_inheritance";
  nametab_dw_tag[5][8] = "DW_TAG_inlined_subroutine";
  nametab_dw_tag[5][9] = "DW_TAG_module";
  nametab_dw_tag[5][10] = "DW_TAG_ptr_to_member_type";
  nametab_dw_tag[5][11] = "DW_TAG_set_type";
  nametab_dw_tag[5][12] = "DW_TAG_subrange_type";
  nametab_dw_tag[5][13] = "DW_TAG_with_stmt";
  nametab_dw_tag[5][14] = "DW_TAG_access_declaration";
  nametab_dw_tag[5][15] = "DW_TAG_base_type";
  nametab_dw_tag[5][16] = "DW_TAG_catch_block";
  nametab_dw_tag[5][17] = "DW_TAG_const_type";
  nametab_dw_tag[5][18] = "DW_TAG_constant";
  nametab_dw_tag[5][19] = "DW_TAG_enumerator";
  nametab_dw_tag[5][20] = "DW_TAG_file_type";
  nametab_dw_tag[5][21] = "DW_TAG_friend";
  nametab_dw_tag[5][22] = "DW_TAG_namelist";
  nametab_dw_tag[5][23] = "DW_TAG_namelist_items";
  nametab_dw_tag[5][24] = "DW_TAG_packed_type";
  nametab_dw_tag[5][25] = "DW_TAG_subprogram";
  nametab_dw_tag[5][26] = "DW_TAG_template_type_param";
  nametab_dw_tag[5][27] = "DW_TAG_template_value_param";
  nametab_dw_tag[5][28] = "DW_TAG_thrown_type";
  nametab_dw_tag[5][29] = "DW_TAG_try_block";
  nametab_dw_tag[5][30] = "DW_TAG_variant_part";
  nametab_dw_tag[5][31] = "DW_TAG_variable";
  nametab_dw_tag[5][32] = "DW_TAG_volatile_type";
  nametab_dw_tag[5][33] = "DW_TAG_dwarf_procedure";
  nametab_dw_tag[5][34] = "DW_TAG_restrict_type";
  nametab_dw_tag[5][35] = "DW_TAG_interface_type";
  nametab_dw_tag[5][36] = "DW_TAG_namespace";
  nametab_dw_tag[5][37] = "DW_TAG_imported_module";
  nametab_dw_tag[5][38] = "DW_TAG_unspecified_type";
  nametab_dw_tag[5][39] = "DW_TAG_partial_unit";
  nametab_dw_tag[5][40] = "DW_TAG_imported_unit";
  nametab_dw_tag[5][41] = "DW_TAG_mutable_type";
  nametab_dw_tag[5][42] = "DW_TAG_condition";
  nametab_dw_tag[5][43] = "DW_TAG_shared_type";
  nametab_dw_tag[5][44] = "DW_TAG_type_unit";
  nametab_dw_tag[5][45] = "DW_TAG_rvalue_reference_type";
  nametab_dw_tag[5][46] = "DW_TAG_template_alias";
  nametab_dw_tag[5][47] = "DW_TAG_coarray_type";
  nametab_dw_tag[5][48] = "DW_TAG_generic_subrange";
  nametab_dw_tag[5][49] = "DW_TAG_dynamic_type";
  nametab_dw_tag[5][50] = "DW_TAG_atomic_type";
  nametab_dw_tag[5][51] = "DW_TAG_call_site";
  nametab_dw_tag[5][52] = "DW_TAG_call_site_parameter";
  nametab_dw_tag[5][53] = "DW_TAG_skeleton_unit";
  nametab_dw_tag[5][54] = "DW_TAG_immutable_type";
  nametab_dw_tag[6][0] = "DW_TAG_lo_user";
  nametab_dw_tag[6][1] = "DW_TAG_MIPS_loop";
  nametab_dw_tag[7][0] = "DW_TAG_HP_array_descriptor";
  nametab_dw_tag[8][0] = "DW_TAG_format_label";
  nametab_dw_tag[8][1] = "DW_TAG_function_template";
  nametab_dw_tag[8][2] = "DW_TAG_class_template";
  nametab_dw_tag[8][3] = "DW_TAG_GNU_BINCL";
  nametab_dw_tag[8][4] = "DW_TAG_GNU_EINCL";
  nametab_dw_tag[8][5] = "DW_TAG_GNU_template_template_param";
  nametab_dw_tag[8][6] = "DW_TAG_GNU_template_parameter_pack";
  nametab_dw_tag[8][7] = "DW_TAG_GNU_formal_parameter_pack";
  nametab_dw_tag[8][8] = "DW_TAG_GNU_call_site";
  nametab_dw_tag[8][9] = "DW_TAG_GNU_call_site_parameter";
  nametab_dw_tag[9][0] = "DW_TAG_SUN_function_template";
  nametab_dw_tag[9][1] = "DW_TAG_SUN_class_template";
  nametab_dw_tag[9][2] = "DW_TAG_SUN_struct_template";
  nametab_dw_tag[9][3] = "DW_TAG_SUN_union_template";
  nametab_dw_tag[9][4] = "DW_TAG_SUN_indirect_inheritance";
  nametab_dw_tag[9][5] = "DW_TAG_SUN_codeflags";
  nametab_dw_tag[9][6] = "DW_TAG_SUN_memop_info";
  nametab_dw_tag[9][7] = "DW_TAG_SUN_omp_child_func";
  nametab_dw_tag[9][8] = "DW_TAG_SUN_rtti_descriptor";
  nametab_dw_tag[9][9] = "DW_TAG_SUN_dtor_info";
  nametab_dw_tag[9][10] = "DW_TAG_SUN_dtor";
  nametab_dw_tag[9][11] = "DW_TAG_SUN_f90_interface";
  nametab_dw_tag[9][12] = "DW_TAG_SUN_fortran_vax_structure";
  nametab_dw_tag[10][0] = "DW_TAG_SUN_hi";
  nametab_dw_tag[11][0] = "DW_TAG_ALTIUM_circ_type";
  nametab_dw_tag[11][1] = "DW_TAG_ALTIUM_mwa_circ_type";
  nametab_dw_tag[11][2] = "DW_TAG_ALTIUM_rev_carry_type";
  nametab_dw_tag[12][0] = "DW_TAG_ALTIUM_rom";
  nametab_dw_tag[13][0] = "DW_TAG_upc_shared_type";
  nametab_dw_tag[13][1] = "DW_TAG_upc_strict_type";
  nametab_dw_tag[13][2] = "DW_TAG_upc_relaxed_type";
  nametab_dw_tag[14][0] = "DW_TAG_GNU_call_site";
  nametab_dw_tag[15][0] = "DW_TAG_GNU_call_site_parameter";
  nametab_dw_tag[16][0] = "DW_TAG_PGI_kanji_type";
  nametab_dw_tag[17][0] = "DW_TAG_PGI_interface_block";
  nametab_dw_tag[18][0] = "DW_TAG_hi_user";
}
char *decode_dw_tag(int v)
{
  if (v < 1 || v > 65535) 
    return unmapped(v);
  if (v <= 5)
  {
    if (v < 1)
    return unmapped(v);
    return nametab_dw_tag[0][v - 1];
  }
  if (v <= 8)
  {
    if (v < 8)
    return unmapped(v);
    return nametab_dw_tag[1][v - 8];
  }
  if (v <= 11)
  {
    if (v < 10)
    return unmapped(v);
    return nametab_dw_tag[2][v - 10];
  }
  if (v <= 13)
  {
    if (v < 13)
    return unmapped(v);
    return nametab_dw_tag[3][v - 13];
  }
  if (v <= 19)
  {
    if (v < 15)
    return unmapped(v);
    return nametab_dw_tag[4][v - 15];
  }
  if (v <= 75)
  {
    if (v < 21)
    return unmapped(v);
    return nametab_dw_tag[5][v - 21];
  }
  if (v <= 16513)
  {
    if (v < 16512)
    return unmapped(v);
    return nametab_dw_tag[6][v - 16512];
  }
  if (v <= 16528)
  {
    if (v < 16528)
    return unmapped(v);
    return nametab_dw_tag[7][v - 16528];
  }
  if (v <= 16650)
  {
    if (v < 16641)
    return unmapped(v);
    return nametab_dw_tag[8][v - 16641];
  }
  if (v <= 16909)
  {
    if (v < 16897)
    return unmapped(v);
    return nametab_dw_tag[9][v - 16897];
  }
  if (v <= 17151)
  {
    if (v < 17151)
    return unmapped(v);
    return nametab_dw_tag[10][v - 17151];
  }
  if (v <= 20739)
  {
    if (v < 20737)
    return unmapped(v);
    return nametab_dw_tag[11][v - 20737];
  }
  if (v <= 20753)
  {
    if (v < 20753)
    return unmapped(v);
    return nametab_dw_tag[12][v - 20753];
  }
  if (v <= 34663)
  {
    if (v < 34661)
    return unmapped(v);
    return nametab_dw_tag[13][v - 34661];
  }
  if (v <= 34946)
  {
    if (v < 34946)
    return unmapped(v);
    return nametab_dw_tag[14][v - 34946];
  }
  if (v <= 35202)
  {
    if (v < 35202)
    return unmapped(v);
    return nametab_dw_tag[15][v - 35202];
  }
  if (v <= 40960)
  {
    if (v < 40960)
    return unmapped(v);
    return nametab_dw_tag[16][v - 40960];
  }
  if (v <= 40992)
  {
    if (v < 40992)
    return unmapped(v);
    return nametab_dw_tag[17][v - 40992];
  }
  if (v <= 65535)
  {
    if (v < 65535)
    return unmapped(v);
    return nametab_dw_tag[18][v - 65535];
  }
  return 0;
}

void deinit_dw_tag()
{
  myfree(nametab_dw_tag[0]); // range      1 -      5
  myfree(nametab_dw_tag[1]); // range      8 -      8
  myfree(nametab_dw_tag[2]); // range     10 -     11
  myfree(nametab_dw_tag[3]); // range     13 -     13
  myfree(nametab_dw_tag[4]); // range     15 -     19
  myfree(nametab_dw_tag[5]); // range     21 -     75
  myfree(nametab_dw_tag[6]); // range  16512 -  16513
  myfree(nametab_dw_tag[7]); // range  16528 -  16528
  myfree(nametab_dw_tag[8]); // range  16641 -  16650
  myfree(nametab_dw_tag[9]); // range  16897 -  16909
  myfree(nametab_dw_tag[10]); // range  17151 -  17151
  myfree(nametab_dw_tag[11]); // range  20737 -  20739
  myfree(nametab_dw_tag[12]); // range  20753 -  20753
  myfree(nametab_dw_tag[13]); // range  34661 -  34663
  myfree(nametab_dw_tag[14]); // range  34946 -  34946
  myfree(nametab_dw_tag[15]); // range  35202 -  35202
  myfree(nametab_dw_tag[16]); // range  40960 -  40960
  myfree(nametab_dw_tag[17]); // range  40992 -  40992
  myfree(nametab_dw_tag[18]); // range  65535 -  65535
}

// ff 1,000
char *nametab_dw_ut[3];
void init_dw_ut()
{
  nametab_dw_ut[   0] = "DW_UT_compile";
  nametab_dw_ut[   1] = "DW_UT_type";
  nametab_dw_ut[   2] = "DW_UT_partial";
}

char *decode_dw_ut(int v)
{
  if (v < 1 || v > 3) 
    return unmapped(v);
  return nametab_dw_ut[v - 1];
}

void deinit_dw_ut()
{
}

// ff 1,000
char *nametab_dw_virtuality[3];
void init_dw_virtuality()
{
  nametab_dw_virtuality[   0] = "DW_VIRTUALITY_none";
  nametab_dw_virtuality[   1] = "DW_VIRTUALITY_virtual";
  nametab_dw_virtuality[   2] = "DW_VIRTUALITY_pure_virtual";
}

char *decode_dw_virtuality(int v)
{
  if (v < 0 || v > 2) 
    return unmapped(v);
  return nametab_dw_virtuality[v - 0];
}

void deinit_dw_virtuality()
{
}

// ff 1,000
char *nametab_dw_vis[3];
void init_dw_vis()
{
  nametab_dw_vis[   0] = "DW_VIS_local";
  nametab_dw_vis[   1] = "DW_VIS_exported";
  nametab_dw_vis[   2] = "DW_VIS_qualified";
}

char *decode_dw_vis(int v)
{
  if (v < 1 || v > 3) 
    return unmapped(v);
  return nametab_dw_vis[v - 1];
}

void deinit_dw_vis()
{
}

void init_all()
{
  init_dw_access();
  init_dw_addr();
  init_dw_at();
  init_dw_atcf();
  init_dw_ate();
  init_dw_cc();
  init_dw_cfa();
  init_dw_children();
  init_dw_defaulted();
  init_dw_ds();
  init_dw_dsc();
  init_dw_eh();
  init_dw_end();
  init_dw_form();
  init_dw_frame();
  init_dw_id();
  init_dw_idx();
  init_dw_inl();
  init_dw_isa();
  init_dw_lang();
  init_dw_lle();
  init_dw_llex();
  init_dw_lnct();
  init_dw_lne();
  init_dw_lns();
  init_dw_macinfo();
  init_dw_macro();
  init_dw_op();
  init_dw_ord();
  init_dw_rle();
  init_dw_sect();
  init_dw_tag();
  init_dw_ut();
  init_dw_virtuality();
  init_dw_vis();
}
void deinit_all()
{
  deinit_dw_access();
  deinit_dw_addr();
  deinit_dw_at();
  deinit_dw_atcf();
  deinit_dw_ate();
  deinit_dw_cc();
  deinit_dw_cfa();
  deinit_dw_children();
  deinit_dw_defaulted();
  deinit_dw_ds();
  deinit_dw_dsc();
  deinit_dw_eh();
  deinit_dw_end();
  deinit_dw_form();
  deinit_dw_frame();
  deinit_dw_id();
  deinit_dw_idx();
  deinit_dw_inl();
  deinit_dw_isa();
  deinit_dw_lang();
  deinit_dw_lle();
  deinit_dw_llex();
  deinit_dw_lnct();
  deinit_dw_lne();
  deinit_dw_lns();
  deinit_dw_macinfo();
  deinit_dw_macro();
  deinit_dw_op();
  deinit_dw_ord();
  deinit_dw_rle();
  deinit_dw_sect();
  deinit_dw_tag();
  deinit_dw_ut();
  deinit_dw_virtuality();
  deinit_dw_vis();
}

