#ifndef _READELF_H_
#define _READELF_H_
#ifdef EXTERN
#else
#define EXTERN extern
#endif

#define SHT_NULL        0
#define SHT_PROGBITS    1
#define SHT_SYMTAB      2
#define SHT_STRTAB      3
#define SHT_RELA        4
#define SHT_HASH        5
#define SHT_DYNAMIC     6
#define SHT_NOTE        7
#define SHT_NOBITS      8
#define SHT_REL         9
#define SHT_SHLIB      10
#define SHT_DYNSYM     11
 
#define SHF_WRITE            0x1
#define SHF_ALLOC            0x2
#define SHF_EXECINSTR        0x4
#define SHF_MASKPROC  0xf0000000

#define STB_LOCAL       0
#define STB_GLOBAL      1
#define STB_WEAK        2
#define STB_LOPROC     13
#define STB_HIPROC     15

#define STT_NOTYPE      0
#define STT_OBJECT      1
#define STT_FUNC        2
#define STT_SECTION     3
#define STT_FILE        4
#define STT_LOPROC     13
#define STT_HIPROC     15

struct stype
{
  int  type;
  char *sym;
};

EXTERN struct stype ttype[20];

struct fdata
{
  char *name;
  int  off;
  int  size;
  int  flag;
  int  dim;
};

struct sdebe
{
  int   pc;
  short dir;
  short file;
  int   line;
};

struct t_dabb
{
  int lindex;
  int *index;
  int ltag;
  int *tag;
  int *tagoff;

// original .debug_abbrev content
  int  lmem;
  char *mem;
};

struct sdeb
{
  int    ndir,ldir;
  int    nfile,lfile;
  int    nentry,lentry;
  char   **dir;
  char   **file;
  struct sdebe *entry;
  struct t_dabb *abbr;
};

struct self
{
  int    log;
  uint   entry; // entry point
  int    (*fprog)(uint off,int len,FILE *pf,int fl,void *arg);
  int    (*fsym)();
  void   *arg;
  FILE   *pf;
  struct sdeb *pdeb;
  char   *strshpool;  // section string table
  int    strshpools;  // section string table size
  char   *strpool;    // string table
  int    strpools;    // string table size
  char   *debug_str;
};

int    readelf(struct self *pe,FILE *pf);
void   initfdata();
struct t_dabb *dwarf_abbrev(struct self *pe,char *buf,int size);
void   dwarf_info(struct self *pe,char *buf,int size) ;
void   dwarf_line(struct self *pe,char *buf,int size) ;

#endif
