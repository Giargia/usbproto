#include <math.h>
#include "env.h"
#include "util.h"
#include "readpcapng-stlink.h"
// typedef struct libusb_context_ libusb_context;
// typedef struct libusb_device_handle_ libusb_device_handle;
#include "armreg.h"
#include "dapreg.h"
#include "driver-cmsis.h"
#include "pico-decode.h"
#include "dap-decode.h"

#define OUTF(fn,off,mask) \
{if (fl || ((v >> (off)) & (mask))) \
{sprintf(p,"%s %d ",#fn, (v >> (off)) & (mask)); p += strlen(p); }};

#define OUTF1(fn,off,mask,o) \
{if ((o == RW || o == op) && (fl || ((v >> (off)) & (mask)))) \
{sprintf(p,"%s %d ",#fn, (v >> (off)) & (mask)); p += strlen(p); }};

#define OUTF2(fn,off,mask,o,fmt) \
{if ((o == RW || o == op) && (fl || ((v >> (off)) & (mask)))) \
{sprintf(p,"%s " #fmt " ",#fn, (v >> (off)) & (mask)); p += strlen(p); }};

struct t_dec
{
  void (*fdec)(uchar *pd,int len,struct t_decode *pdec,int transfer);
  void (*fdecrep)(uchar *pd,int len,struct t_decode *pdec,int transfer,int);
  void (*finit)(struct t_decode *pdec,int n);
  void (*fend)(struct t_decode *pdec);
};

struct t_reg treg[NREG];
int    nreg;

struct t_add tadd[NADD];
int    nadd;

int deb;
double timeresdiv;
struct t_switch swt[NSW];
// decode add
int dbus,ddevice,dendp;
// filter add
int fbus,fdevice,fendp;
int drep;
double ts,t0,t0s,tsess;
int nrec,nrec0;

int proto   = STLINK;
int verbose = 0;

char *funcdec[64];
char *trandec[8];
char *ctsdec[4];

char funcdecbuf[40];

#define NSES 100
struct t_ses sestab[NSES];
int nses;

struct t_dec dectab[NPROTO];  
struct t_cmd swcmd[256];

int sreq,sindex;

void dumpses()
{
  if (nses > 0)
  {
    printf("Sequence table\n");
    printf("id  # rec     start        time          pause\n");
    printf("--- ------ ------------ ------------ ------------\n");
    for (int i = 0; i < nses; i++)
    {
      printf("%3d %6d %12.6f %12.3f ",
             i,sestab[i].nrec,
             sestab[i].tstart - t0,
             sestab[i].tend - sestab[i].tstart);
      if (i < nses - 1)
        printf("%12.3f",sestab[i + 1].tstart - sestab[i].tend);
      putchar('\n');
    }
  }
}

void initproto()
{
  dectab[PICO].fdec         = decodepico;
  dectab[PICO].fdecrep      = decodereppico;
  dectab[PICO].finit        = initdap;
  dectab[PICO].fend         = enddap;

  dectab[STLINK].fdec       = decodestlink;
  dectab[STLINK].fdecrep    = decoderepstlink;

  dectab[CMSIS_DAP].fdec    = decodedap;
  dectab[CMSIS_DAP].fdecrep = decoderepdap;
  dectab[CMSIS_DAP].finit   = initdap;
  dectab[CMSIS_DAP].fend    = enddap;
}

void myerror(char *n,int line)
{
  printf("error %s at line %d\n",n,line);
  exit(0);
}

void addsec(int nrec,double start,double end)
{
  if (nses < NSES - 1)
  {
    sestab[nses].nrec = nrec;
    sestab[nses].tstart = start;
    sestab[nses].tend = end;
    nses++;
  }
}

static int nrec_last;
void lineheader(int nr,double t,int logh)
{
  if (t - t0s > 1)
  {
    addsec(nrec - nrec0,tsess,t0s);
    if (logh)
    {
      nrec_last = nrec; 
      printf("[%5d rec in %8.3fs empty time slot of %8.3fs]\n",nrec - nrec0,t0s - tsess,t - t0s);
    }
    tsess = t;
    nrec0 = nrec;
  }
  if (logh)
    printf("%5d %10.6f (+%.6f) ",nrec,t - t0,t - t0s);
  t0s = t;
}

void nolineheader(int nr,double t)
{
//t0s = t;
}

void relineheader()
{
  nrec_last = nrec; 
  printf("%5d %10.6f ",nrec,t0s - t0);
}

void lineheader0()
{
  if (nrec != nrec_last)
  {
    nrec_last = nrec; 
    printf("%5d %10.6f ",nrec,t0s - t0);
  } else
    printf("                 ");
}

void iswc(int cmd,int l,int fmt,char *desc)
{
  swcmd[cmd].cmd = cmd;
  swcmd[cmd].l = l;
  swcmd[cmd].fmt = fmt;
  swcmd[cmd].desc = desc;
}

void initswcmd()
{
  iswc(0x10,3,4,"MSB First OUT Bytes clock +VE");
  iswc(0x11,3,4,"MSB First OUT Bytes clock -VE");
  iswc(0x19,3,4,"Clock Data Bytes Out on -ve Clock Edge LSB First");
  iswc(0x1b,3,1,"Clock Data Bits Out on -ve Clock Edge LSB First");
  iswc(0x34,3,4,"MSB First IN/OUT Bytes clock +VE/+VE");
  iswc(0x38,3,4,"Clock Data Bytes In and Out LSB First");
  iswc(0x39,3,4,"Clock Data Bytes In and Out LSB First");
  iswc(0x3c,3,4,"Clock Data Bytes In and Out LSB First");
  iswc(0x3d,3,4,"Clock Data Bytes In and Out LSB First");
  iswc(0x3a,3,4,"Clock Data Bits In and Out LSB First");
  iswc(0x3b,3,1,"Clock Data Bits In and Out LSB First");
  iswc(0x3e,3,1,"Clock Data Bits In and Out LSB First");
  iswc(0x3f,3,1,"Clock Data Bits In and Out LSB First");
  iswc(0x4a,3,1,"Clock data to TMS/CS Pin");
  iswc(0x4b,3,1,"Clock data to TMS/CS Pin");
  iswc(0x6f,3,6,"Clock Data to TMS/CS Pin with Read");
  iswc(0x80,3,2,"Set  O/P Low  Byte (ADBUS)");
  iswc(0x81,3,2,"Read I/P Low  Byte (ADBUS)");
  iswc(0x82,3,2,"Set  O/P High Byte (ACBUS)");
  iswc(0x83,3,2,"Read I/P High Byte (ACBUS)");
  iswc(0x86,3,5,"Set TCK/SK Divisor");
  iswc(0x8a,1,3,"disable clk divide by 5");
  iswc(0x8d,1,3,"Disables 3 phase data clocking");
  iswc(0x97,1,3,"disable adactive clocking");
  iswc(0x81,1,3,"Read Data Bits Low Byte");
  iswc(0x84,1,3,"loopback ON");
  iswc(0x85,1,3,"loopback OFF");
  iswc(0x87,1,3,"Send Immediate");
  iswc(0x8e,2,8,"clock to be output without transferring data");
  iswc(0x8f,3,7,"Clock For n x 8 bits with no data transfer");
  iswc(0xab,1,3,"Typical bad command");
}

void decoderep(uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  int i;
  if (len > 0)
  {
    relineheader();
    printf("<= %02x len %d ",*pd & MB,len);
  } else 
  {
//  printf("<= empty len %d\n",len);
    return;
  }
  for (i = 0; i < len && i < 16 ; i++)
    printf("%02x ",pd[i] & MB);
  if (i < len) 
    printf("..");
  putchar('\n');
}

void decoderepstlink(uchar *pd,int len,struct t_decode *pdec,int transfer,int unused)
{
  int mode,st,id,val,factor;

  if (len == 0)
  {
//  printf("<= %02x empty reply\n",pdec->op);
    return;
  }
  lineheader0();
  printf("<= %02x len %2d op %x op1 %x ",*pd & MB,len,pdec->op,pdec->op1);
  if (pdec->op == STLINK_GET_CURRENT_MODE)
  {
    printf("STLINK_GET_CURRENT_MODE ");
    mode = pd[0] & MB;
    if (mode == STLINK_DEV_DFU_MODE)
      printf("STLINK_DEV_DFU_MODE ");
    else if (mode == STLINK_DEV_MASS_MODE)
      printf("STLINK_DEV_MASS_MODE ");
    else if (mode == STLINK_DEV_DEBUG_MODE)
      printf("STLINK_DEV_DEBUG_MODE ");
    else
      printf("unknown in decoderep %d ",mode);
  } else if (pdec->op == STLINK_DEBUG_COMMAND)
  {
    if (pdec->op1 == STLINK_DEBUG_APIV1_RESETSYS ||
        pdec->op1 == STLINK_DEBUG_GETSTATUS ||
        pdec->op1 == STLINK_DEBUG_APIV2_WRITEDEBUGREG || 
        pdec->op1 == STLINK_DEBUG_APIV2_GETLASTRWSTATUS2 ||
        pdec->op1 == STLINK_DEBUG_ENTER_SWD_NO_RESET)
    { 
      if (pdec->op1 == STLINK_DEBUG_APIV1_RESETSYS)
        printf("STLINK_DEBUG_APIV1_RESETSYS ");
      else if (pdec->op1 == STLINK_DEBUG_GETSTATUS)
        printf("STLINK_DEBUG_GETSTATUS ");
      else if (pdec->op1 == STLINK_DEBUG_APIV2_WRITEDEBUGREG)
        printf("STLINK_DEBUG_APIV2_WRITEDEBUGREG ");
      else if (pdec->op1 == STLINK_DEBUG_ENTER_SWD_NO_RESET)
        printf("STLINK_DEBUG_ENTER_SWD_NO_RESET ");
      st = pd[0] & MB;
      if (st == STLINK_CORE_RUNNING)
        printf("STLINK_CORE_RUNNING ");
      else if (st == STLINK_CORE_HALTED)
        printf("STLINK_CORE_HALTED ");
      else
        printf("unknown %x in decoderep ",st);
    } else if (pdec->op1 == STLINK_DEBUG_APIV2_READALLREGS)
    {
      int i,r;
      printf("STLINK_DEBUG_APIV2_READALLREG");
      for (i = 0; i < 16 && i * 4 + 4 < len; i++)
      {
        r = get32(pd + 4 + i * 4);
        putchar('\n');
        lineheader0();
        printf("   R%02d %08x %12d",i,r,r);
      }
    } else if (pdec->op1 == STLINK_DEBUG_APIV2_READREG)
    {
      int r = get32(pd + 4);
      printf("STLINK_DEBUG_APIV2_READREG %08xx %12dd",r,r);
    } else if (pdec->op1 == STLINK_DEBUG_APIV2_READ_IDCODES)
    {
      printf("STLINK_DEBUG_APIV2_READ_IDCODES ");
      if (len >= 4)
      {
        id = get32(pd);
        printf("core id %08x ",id);
      }
    } else if (pdec->op1 == STLINK_DEBUG_READCOREID)
    {
      printf("STLINK_DEBUG_READCOREID ");
      if (len >= 4)
      {
        id = get32(pd);
        printf("core id %08x ",id);
      }
    } else if (pdec->op1 == STLINK_DEBUG_READMEM_32BIT)
    {
      printf("STLINK_DEBUG_READMEM_32BIT ");
      if (len >= 4)
      {
        val = get32(pd);
        printf("val %08x ",val);
      }
      if (len >= 8)
      {
        val = get32(pd + 4);
        printf("val %08x ",val);
      }
    } else if (pdec->op1 == STLINK_DEBUG_APIV2_READDEBUGREG)
    {
      printf("STLINK_DEBUG_APIV2_READDEBUGREG %s ",arm_getregname(pdec->regadd,1));
      if (len >= 8)
      {
        val = get32(pd + 4);
        printf("val %08x ",val);
        arm_setreg(pdec->regadd,val,RO,nrec,0);
      }
    } else if (pdec->op1 == STLINK_DEBUG_APIV2_SWD_SET_FREQ)
      printf("STLINK_DEBUG_APIV2_SWD_SET_FREQ");
    else if (pdec->op1 == STLINK_DEBUG_APIV2_DRIVE_NRST)
      printf("STLINK_DEBUG_APIV2_DRIVE_NRST");
    else if (pdec->op1 == STLINK_DEBUG_APIV2_RESETSYS)
      printf("STLINK_DEBUG_APIV2_RESETSYS");
    else if (pdec->op1 == STLINK_DEBUG_RUNCORE)
    {
      printf("STLINK_DEBUG_RUNCORE ");
      st = pd[0] & MB;
      if (st == STLINK_CORE_RUNNING)
        printf("STLINK_CORE_RUNNING ");
      else if (st == STLINK_CORE_HALTED)
        printf("STLINK_CORE_HALTED ");
      else
        printf("unknown %x in decoderep ",st);
    } else
      printf("unknown debug request %x ",pdec->op1);
  } else if (pdec->op == STLINK_GET_TARGET_VOLTAGE)
  {
    printf("STLINK_GET_TARGET_VOLTAGE ");
    if (len >= 8)
    {
      val    = get32(pd);
      factor = get32(pd + 4);
      printf("val %x factor %x V %d ",val,factor,2400*val/factor);
    }
  }
  putchar('\n');
/*
  for (i = 0; i < len && i < 16 ; i++)
    printf("%02x ",pd[i] & MB);
  if (i < len) 
    printf("..");
  putchar('\n');
*/
}

void decode(uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  int n;
  char b2[9];
  int ok,sw,l;
  struct t_cmd *pc;

  ok = 1;
  if (len > 0)
  {
    relineheader();
    printf("** decode %02x len %d ",*pd & MB,len);
  } else 
  {
    printf("=> empty len %d\n",len);
    return;
  }
  n = 0;
  while(ok && len > 0)
  {
    if (n > 0)
      printf("                  - decode len %d ",len);
    n++;
    sw = *pd & MB;
    pc = swcmd + sw;
    if (pc->cmd == sw)
    {
      if (pc->fmt == 1)
        printf("%02x len %02x byte %02x %s\n",
               sw,pd[1] & MB,pd[2]&MB,pc->desc);
      else if (pc->fmt == 2)
        printf("%02x val %s dir %s %s\n",
               sw,tobin(pd[1] & MB,8,b2),tobin(pd[2]&MB,8,0),pc->desc);
      else if (pc->fmt == 3)
        printf("%02x %s\n",sw,pc->desc);
      else if (pc->fmt == 4)
      {
        l = get16(pd + 1); 
        printf("%02x len %d+1 %s\n", sw,l,pc->desc);
        pd  += 3 + l + 1;
        len -= 3 + l + 1;    
      } else if (pc->fmt == 5)
      {
        l =  get16(pd + 1); 
        printf("%02x divisor %d %s\n", sw,l,pc->desc);
      } else if (pc->fmt == 6)
      {
        printf("%02x len %02x byte %02x %s\n",
               sw,pd[1] & MB,pd[2]&MB,pc->desc);
      } else if (pc->fmt == 7)
      {
        l =  get16(pd + 1); 
        printf("%02x n %d %s\n", sw,l,pc->desc);
      } else if (pc->fmt == 8)
        printf("%02x len %d %s\n", sw,pd[1] & MB,pc->desc);
      else
        printf("wrong format %d\n",pc->fmt);
      if (pc->fmt <= 3 || pc->fmt >= 5)
      {
        pd  += pc->l;
        len -= pc->l; 
      }
    } else
    {
      printf("%02x unknown\n",sw);
      ok = 0;
    }
  }
}

void decodestlink(uchar *pd,int len,struct t_decode *pdec,int tranfer)
{
  int i,op,n,add,val;


  if (len == 0)
  {
//  printf("=> %02x empty\n",*pd & MB);
    return;
  }

  lineheader0();
//printf("[decode contlen %d]",pdec->contlen);
  if (pdec->contlen)
  {
    printf("=> .. cont %08x:%d",pdec->regadd,pdec->contlen);
    if (len != pdec->contlen)
      printf("/%d",len);
    putchar(' ');
    int add = pdec->regadd;
    printf("%s ",arm_getregname(add,1));
    for (i = 0; i < len / 4 && i < pdec->contlen / 4 && i < 8 ; i++)
    {
      int v = get32(pd + i * 4);
      printf("%08x ",v);
      arm_setreg(add,v,WO,nrec,0);
      add += 4;
    }
    putchar('\n');
    pdec->contlen -= len;
    if (pdec->contlen < 0) 
      pdec->contlen = 0; 
    return;
  } 
  pdec->op = op = *pd & MB;
  pdec->op1 = 0;
  printf("=> %02x len %2d ",op,len);
  n = 0;
  printf("[%02x] ",op);
  if (op == STLINK_GET_CURRENT_MODE)
    printf("STLINK_GET_CURRENT_MODE");
  else if (op == STLINK_GET_VERSION)
    printf("STLINK_GET_VERSION");
  else if (op == STLINK_DFU_COMMAND)
  {
    printf("STLINK_DFU_COMMAD ");
    for (n = 1; n < len && (pd[n] & MB) && (pd[n] & MB) != 0xff; n++)
    {
      pdec->op1 = op = pd[n] & MB;
      printf("[%02x] ",op);
      if (op == STLINK_DFU_EXIT)
        printf("STLINK_DFU_EXIT ");
      else
        printf("???");
    }
  } else if (op == STLINK_DEBUG_COMMAND)
  {
    printf("STLINK_DEBUG_COMMAND ");
    for (n = 1; n < len && (pd[n] & MB) && (pd[n] & MB) != 0xff; n++)
    {
      pdec->op1 = op = pd[n] & MB;
      printf("[%02x] ",op);
      if (op == STLINK_DEBUG_ENTER_JTAG_RESET)
        printf("STLINK_DEBUG_ENTER_JTAG_RESET ");
      else if (op == STLINK_DEBUG_APIV2_READ_IDCODES)
        printf("STLINK_DEBUG_APIV2_READ_IDCODES ");
      else if (op == STLINK_DEBUG_APIV2_RESETSYS)
        printf("STLINK_DEBUG_APIV2_RESETSYS ");
      else if (op == STLINK_DEBUG_GETSTATUS)
        printf("STLINK_DEBUG_GETSTATUS ");
      else if (op == STLINK_DEBUG_FORCEDEBUG)
        printf("STLINK_DEBUG_FORCEDEBUG ");
      else if (op == STLINK_DEBUG_APIV1_RESETSYS)
        printf("STLINK_DEBUG_APIV1_RESETSYS ");
      else if (op == STLINK_DEBUG_APIV1_READALLREGS)
        printf("STLINK_DEBUG_APIV1_READALLREGS ");
      else if (op == STLINK_DEBUG_APIV1_READREG)
      {
        printf("STLINK_DEBUG_APIV1_READREG ind %d ",pd[n + 1]);
        n++;
      } else if (op == STLINK_DEBUG_APIV2_READREG)
      {
        printf("STLINK_DEBUG_APIV2_READREG ind %d ",pd[n + 1]);
        n++;
      } else if (op == STLINK_DEBUG_APIV1_WRITEREG ||
                 op == STLINK_DEBUG_APIV2_WRITEREG)
      {
        if (op == STLINK_DEBUG_APIV1_WRITEREG)
          printf("STLINK_DEBUG_APIV1_WRITEREG ");
        else 
          printf("STLINK_DEBUG_APIV2_WRITEREG ");
        if (n + 1 < len) 
        {
          printf("regid %d ",pd[n + 1] & MB);
          n++;
        }
        if (n + 4 < len)
        {
          add = get32(pd + n + 1);
          printf("val %08x ",add);
          n += 4;
        }
      } else if (op == STLINK_DEBUG_APIV2_GETLASTRWSTATUS2)
        printf("STLINK_DEBUG_APIV2_GETLASTRWSTATUS2 ");
      else if (op == STLINK_DEBUG_READMEM_32BIT)
      {
        printf("STLINK_DEBUG_READMEM_32BIT ");
        if (n + 4 < len)
        {
          add = get32(pd + n + 1);
          printf("%08x ",add);
          n += 4;
        }
        if (n + 4 < len)
        {
          add = get32(pd + n + 1);
          printf("%08x ",add);
          n += 4;
        }
      } else if (op == STLINK_DEBUG_WRITEMEM_32BIT)
      {
        int l = -1;
        printf("STLINK_DEBUG_WRITEMEM_32BIT ");
        if (n + 4 < len)
        {
          add = get32(pd + n + 1);
          printf("%08x",add);
          pdec->regadd = add;
          n += 4;
        }
        if (n + 2 < len)
        {
          l = get16(pd + n + 1);
          pdec->contlen = l;
          printf(":%d ",l);
          n += 2;
        }
      }
      else if (op == STLINK_DEBUG_RUNCORE)
        printf("STLINK_DEBUG_RUNCORE ");
      else if (op == STLINK_DEBUG_STEPCORE)
        printf("STLINK_DEBUG_STEPCORE ");
      else if (op == STLINK_DEBUG_APIV1_SETFP)
        printf("STLINK_DEBUG_APIV1_SETFP ");
      else if (op == STLINK_DEBUG_WRITEMEM_8BIT)
        printf("STLINK_DEBUG_WRITEMEM_8BIT ");
      else if (op == STLINK_DEBUG_APIV1_CLEARFP)
        printf("STLINK_DEBUG_APIV1_CLEARFP ");
      else if (op == STLINK_DEBUG_APIV1_WRITEDEBUGREG)
        printf("STLINK_DEBUG_APIV1_WRITEDEBUGREG ");
      else if (op == STLINK_DEBUG_APIV1_ENTER)
        printf("STLINK_DEBUG_APIV1_ENTER ");
      else if (op == STLINK_DEBUG_EXIT)
        printf("STLINK_DEBUG_EXIT ");
      else if (op == STLINK_DEBUG_READCOREID)
        printf("STLINK_DEBUG_READCOREID ");
      else if (op == STLINK_DEBUG_ENTER_SWD_NO_RESET)
        printf("STLINK_DEBUG_ENTER_SWD_NO_RESET ");
      else if (op == STLINK_DEBUG_APIV2_ENTER)
        printf("STLINK_DEBUG_APIV2_ENTER ");
      else if (op == STLINK_DEBUG_APIV2_DRIVE_NRST)
      {
        n++;
        printf("STLINK_DEBUG_APIV2_DRIVE_NRST val %d ",pd[n]);
      } else if (op == STLINK_DEBUG_APIV2_WRITEREG)
        printf("STLINK_DEBUG_APIV2_WRITEREG ");
      else if (op == STLINK_DEBUG_APIV2_GETLASTRWSTATUS2)
        printf("STLINK_DEBUG_APIV2_GETLASTRWSTATUS2 ");
      else if (op == STLINK_DEBUG_APIV2_READALLREGS)
        printf("STLINK_DEBUG_APIV2_READALLREGS ");
      else if (op == STLINK_DEBUG_APIV2_READDEBUGREG)
      {
        printf("STLINK_DEBUG_APIV2_READDEBUGREG ");
        if (n + 4 < len)
        {
          add = get32(pd + n + 1);
          printf("%s ",arm_getregname(add,1));
          pdec->regadd = add;
          n += 4;
        }
      } else if (op == STLINK_DEBUG_APIV2_WRITEDEBUGREG)
      {
        printf("STLINK_DEBUG_APIV2_WRITEDEBUGREG ");
        if (n + 4 < len)
        {
          add = get32(pd + n + 1);
          printf("%s ",arm_getregname(add,1));
          n += 4;
        }
        if (n + 4 < len)
        {
          val = get32(pd + n + 1);
          printf("%08x ",val);
          n += 4;
          arm_setreg(add,val,WO,nrec,0);
        }
      } else 
        printf("unknown %x in decode ",op);
    }
  } else if (op == STLINK_GET_TARGET_VOLTAGE)
    printf("STLINK_GET_TARGET_VOLTAGE");
  else
    printf("unknown in decode ");
  putchar('\n');
}

char *getfuncdec(int f,int l)
{
  if (f >= 0 && f < 64)
  {
    if (f == 8)
    {
      char *s;
      if (l > 0) s = "response"; else s = "status";
      sprintf(funcdecbuf,"%s %s",funcdec[f],s);
      return funcdecbuf;
    }
    return funcdec[f];
  } else
    return "???";
}

char *fmtrt(int rt,char *p)
{
  int d7,d65,d40;
  d7  = (rt >> 7) & 1;
  d65 = (rt >> 5) & 3;
  d40 = rt & 0x1f;

  sprintf(p,"%d.%d.%x(",d7,d65,d40);
  if (d7)
    strcat(p,"H>D.");
  else
    strcat(p,"D>H.");
  if (d65 == 0)
    strcat(p,"std.");
  else if (d65 == 1)
    strcat(p,"cls.");
  else if (d65 == 2)
    strcat(p,"vdr.");
  else
    strcat(p,"res.");
  if (d40 == 0)
    strcat(p,"dev)");
  else if (d40 == 1)
    strcat(p,"int)");
  else if (d40 == 2)
    strcat(p,"oth)");
  else
    strcat(p,"res)");
  return p;
}

char *gettrandec(int f)
{
  if (f >= 0 && f < 8)
    return trandec[f];
  else
    return "???";
}

char *getctsdec(int cts)
{
  if (cts >= 0 && cts < 4)
    return ctsdec[cts];
  return "???";
}

void setup(uchar *p,int l)
{
  char  rt; // request type
  char  req; // request code
  short val;
  short index;
  short len;
  char buf[40];

  lineheader0();
  printf("setup l %d ",l);
  if (l >= 8)
  {
    rt    = p[0];
    req   = p[1];
    val   = get16(p + 2); 
    index = get16(p + 4);
    len   = get16(p + 6);
    printf("request type %02x=%s request %02x val %04x index %04x len %d",
            rt & MB, fmtrt(rt & MB,buf),
            req & MB, val & MW,index & MW, len & MW);
    sreq = req & MB;
    sindex = index & MW;
  } else 
    printf("packet too short (min 8)");
  putchar('\n');
}

void othersetupresp(uchar *p,int l)
{
  int i;
  if (sreq == 0x90) // read eeprom
  {
    lineheader0();
    printf("EEPROM %04x ",sindex);
    for (i = 0; i < l; i++) 
      printf("%02x ",p[i] & MB);
    for (i = 0; i < l; i++) 
    {
      if (p[i] >= ' ' && p[i] < 127)
        printf("%c",p[i] & MB);
      else
        printf("."); 
    }
    putchar('\n');
  } /* else
    printf("othersetup sreq %x\n",sreq);
*/
}

void descriptor(struct t_add *padd,uchar *p,int l)
{
  int    len;
  int    type;
  ushort bcd,bcddev;
  ushort vendor;
  ushort product;
  short  maxpack;
  char   iser,iman,iprod,nc;
  char   *cadd;

//printf("descriptor l %d p %p %02x %02x\n",l,p,p[0],p[1]);
//memdump(p,l,0,0);
  while (l >= 2)
  {
    len  = p[0] & MB;
    type = p[1] & MB;
    if (1) //len != 0)
    {
      lineheader0();
      printf("D: len %d type %d (from req %02x) ", len, type,sreq);
      if (type == 1)
      {
        if (len < 18) 
          printf("packet too short for device descriptor min 18");
        else
        {
          bcd     = get16(p + 2); // (p[2] & MB) | (p[3] << 8);
          vendor  = get16(p + 8); // (p[8] & MB) | (p[9] << 8);
          product = get16(p + 10); // (p[10] & MB) | (p[11] << 8);
          bcddev  = get16(p + 12); // (p[12] & MB) | (p[13] << 8);
          iman  = p[14];
          iprod = p[15];
          iser  = p[16];
          nc    = p[17];
          padd->vendor = vendor;
          padd->product = product;

          printf("device bcd %04x class %d subclass %d proto %d maxlen %d\n",
                 bcd & MW,p[4] & MB,p[5] & MB,p[6] & MB,p[7] & MB);
          lineheader0();
          printf("- vendor %04x product %04x bcddev %04x iman %d iprod %d iser %d num conf %d",
                 vendor & MW, product & MW,bcddev & MW,iman & MB,
                 iprod & MB,iser & MB, nc & MB);
        }
      } else if (type == 2)
      {
        short totlen;

        totlen = get16(p + 2); // (p[2] & MB) | (p[3] << 8);
        printf("configuration totlen %d num inter %d val %d iconf %d att %x power %d",
               totlen & MW,p[4] & MB,p[5] & MB,p[6] & MB,p[7] & MB,p[8] & MB);
      } else if (type == 4)
      {
        printf("interface int num  %d alt set %d num endpoint %d int class %d sub %d proto %d int %d",
               p[2] & MB,p[3] & MB,p[4] & MB,p[5] & MB,p[6] & MB, p[7] & MB, p[8] & MB);
      } else if (type == 5)
      {
        maxpack = get16(p + 4); //(p[4] & MB) | (p[4] << 8);
        if ((p[2] >> 7) & 1)
          cadd = "IN "; 
        else
          cadd = "OUT";
        printf("endpoint add %02x(%s) attributes %02x max packet size %d interval %d",
               p[2] & MB,cadd,p[3] & MB,maxpack & MW,p[6] & MB);
      }
      if (len == 0)
        l = 0;
      l -= len;
      p += len;
      putchar('\n');
    } 
   
  } 
}

char *fmtep(int bus,int dev,int ep)
{
  static char buf[20];
  sprintf(buf,"%d.%d.%d",bus,dev,ep);
  return buf;
}

void dumpadd()
{
  int i;

  printf("dumpadd add %d.%d.%d proto %d\n",
         dbus,ddevice,dendp,proto);
  printf("filter add %d.%d.%d\n",fbus,fdevice,fendp);
  printf("count setup descr other decode dir  add      vendor prod\n");
  printf("----- ----- ----- -----  -----   - --------    ---- ----\n");
  for (i = 0; i < nadd; i++)
  {
    printf("%5d %5d %5d %5d  %5d   %d %-8s ",
           tadd[i].count,
           tadd[i].setup,
           tadd[i].desc,
           tadd[i].othersetup,
           tadd[i].decode,
           tadd[i].dir,
           fmtep(tadd[i].bus,
                 tadd[i].dev,
                 tadd[i].ep));
    if (tadd[i].vendor || tadd[i].product)
      printf("   %04x %04x",
             tadd[i].vendor,
             tadd[i].product);
    putchar('\n');
  }
}

struct t_add *addpacket(int dir,int bus,int dev,int ep)
{
  int i;

  ep = ep & 0x7f;
  for (i = 0; i < nadd; i++)
  {
    if (tadd[i].dir == dir &&
        tadd[i].bus == bus &&
        tadd[i].dev == dev &&
        tadd[i].ep  == ep)
    {
      tadd[i].count++;
      return tadd + i;
    }
  } 

  if (nadd + 1 >= NADD)
    myerror("tadd overflow",__LINE__);
  tadd[nadd].dir = dir;
  tadd[nadd].bus = bus;
  tadd[nadd].dev = dev;
  tadd[nadd].ep  = ep;
  tadd[nadd].count = 0;
//printf("addpacket dir %d bus %d dev %d ep %x -> %d\n",dir,bus,dev,ep,nadd);
  nadd++;
  return tadd + nadd - 1;
}

int usbheader(uchar *pd,struct t_decode *pdec,double ts,int *filter)
{
  int   edir;
  short *phl;
//int   *irpID;
//int   *status;
  short *func;
  char  *info;
  short *bus;
  short *device;
  char  *endpoint;
  char  *transfer;
  int   *datalen;
  char  *cts; // control transfer stage
  char  *cinfo,*cdir;
  struct t_add *padd;
  char  invalid;

  invalid = -1;

//sreq = 0;
  phl      = (short *) pd;
//irpID    = (int *)   (pd + 2);
//status   = (int *)   (pd + 2 + 8);
  func     = (short *) (pd + 2 + 8 + 4);
  info     = (char *)  (pd + 2 + 8 + 4 + 2);
  bus      = (short *) (pd + 2 + 8 + 4 + 2 + 1);
  device   = (short *) (pd + 2 + 8 + 4 + 2 + 1 + 2);
  endpoint = (char *)  (pd + 2 + 8 + 4 + 2 + 1 + 2 + 2);
  transfer = (char *)  (pd + 2 + 8 + 4 + 2 + 1 + 2 + 2 + 1);
  datalen  = (int *)   (pd + 2 + 8 + 4 + 2 + 1 + 2 + 2 + 1 + 1);

  edir = (*endpoint >> 7) & 1;
  if (edir & 1) cdir = "IN"; else cdir = "OUT";
  if (*info & 1) cinfo = "D->H"; else cinfo = "H->D";
  *filter = 0;
  if ((fbus == -1 || fbus == *bus) &&
      (fdevice == -1 || fdevice == *device))
  {
    padd = addpacket(edir,*bus,*device,*endpoint);
    *filter = 1;
    lineheader(nrec,ts,pdec->logh);
    if (pdec->logh)
    {
      printf("WIN[l %d;func %x=%s;info %x=%s;dir %d=%s;%d.%d.%d;transfer %d=%s;datalen=%d",
           *phl,*func,getfuncdec(*func,*datalen),
           *info,cinfo,edir,cdir,*bus,*device,
           *endpoint & 0x7f,*transfer,gettrandec(*transfer),*datalen);
    }
    if (*phl > 27)
    {
      cts = (char *) (pd + 2 + 8 + 4 + 2 + 1 + 2 + 2 + 1 + 1 + 4);
      if (pdec->logh)
        printf(";cts %d=%s",*cts,getctsdec(*cts));
    } else 
      cts = &invalid;
    if (pdec->logh)
      printf("]\n");
//  if ((*info & 1) == 0 && (*func == 0 || *func == 0xb || *func == 0x17) && *transfer == 2)
    if ((*info & 1) == 0 && *cts == 0 && *transfer == 2)
    {
      padd->setup++;
      setup(pd + *phl,*datalen);
    }
    if ((*info & 1) == 1 && *func == 0x8 && *transfer == 2 && *datalen > 0)
    {
      phl      = (short *) pd; // TODO phl get dirty in inter !!!
//    printf("pd %p phl %p off %d\n",pd,phl,*phl);
      if (sreq == 6)
      {
        padd->desc++;
        descriptor(padd,pd + *phl,*datalen);
      } else
      {
        padd->othersetup++;
        othersetupresp(pd + *phl,*datalen);
      }
      sreq = 0;
    }
/*
    if (*transfer == 3) // bulk
      printf("edir %d bus %d/%d device %d/%d endpoint %d/%d\n",
            edir,*bus,dbus,*device,ddevice,*endpoint & 0x7f,dendp);
*/
    if (edir == 0 &&
        *bus == dbus && 
        *device == ddevice && 
        (*endpoint & 0x7f) == dendp)
    {
      padd->decode++;
      drep = 1;
      if (dectab[proto].fdec)
        dectab[proto].fdec(pd + *phl,*datalen,pdec,*transfer);
      else
        decode(pd + *phl,*datalen,pdec,*transfer);
    } else if (edir == 1 &&
               *bus == dbus &&
               *device == ddevice && 
               drep == 1)
    {
//    drep = 0; // allow multiple replay and empty replay
      if (dectab[proto].fdecrep)
        dectab[proto].fdecrep(pd + *phl,*datalen,pdec,*transfer,1);
      else
        decoderep(pd + *phl,*datalen,pdec,*transfer);
    }
  } else
    nolineheader(nrec,ts);
  return *datalen;
}

void options(FILE *pf,int len)
{
  int n;
  short opth[2];
  char *val;

  printf("options len %d\n",len);
  while (len > 0)
  {
    n = fread(&opth,1,sizeof(opth),pf);
    if (n == sizeof opth)
    {
      printf("%4d opt code %d len %d ",len,opth[0],opth[1]);
/*
      if (opth[0] == 2 ||
          opth[0] == 3 ||
          opth[0] == 4)
*/
      if (opth[0] > 0)
      {
        val = malloc(((opth[1] + 3) / 4) * 4);
        n = fread(val,1,((opth[1] + 3) / 4) * 4,pf);
        if (n == ((opth[1] + 3) / 4) * 4)
        {
          if (opth[0] == 9) // if/tsresol
          {
            if (val[0] > 0)
              timeresdiv = pow(10.,(double) val[0]);
            else
              timeresdiv = pow(2.,(double) -val[0]);
          }
          if (opth[1] == 1)
            printf("%d",val[0]);
          else
            printf("%*.*s",opth[1],opth[1],val);
        } else
          myerror("fread",__LINE__);
        free(val);
      } else
      {
        n = fseek(pf,((opth[1] + 3) / 4) * 4,SEEK_CUR);
        if (n != 0) 
          myerror("fseek",__LINE__);
      }
      len -= sizeof(opth) + ((opth[1] + 3) / 4) * 4;
      putchar('\n');
    } else
      myerror("fread",__LINE__);
  }
}

int hshb(FILE *pf,int len,struct t_decode *pdec)
{
  struct t_shb shb;
  int n;

  n = fread(&shb,1,sizeof(shb),pf);
  if (n == sizeof(shb))
  {
    printf("Section Header Block\n");
    printf("byte order magic %x major %d minor %d section len %d %d\n",
           shb.bom,shb.major,shb.minor,shb.sl[0],shb.sl[1]);
    options(pf,len - sizeof shb - 8 - 4);
  } else
    myerror("fread",__LINE__);
  return 1;
}

int hidb(FILE *pf,int len,struct t_decode *pdec)
{
  struct t_idb idb;
  int n;

  n = fread(&idb,1,sizeof(idb),pf);
  if (n == sizeof(idb))
  {
    printf("Interface Description Block\n");
    printf("link type %x snaplen %d\n",idb.lt,idb.snaplen);
    options(pf,len - sizeof idb - 8 - 4);
  } else
    myerror("fread",__LINE__);
  return 1;
}

int hepb(FILE *pf,int len,struct t_decode *pdec)
{
  int n,dl,filter;
  time_t lt;
  uchar *pd;
  short  *hl;
  struct t_epb epb;
  struct tm *ptm;
 
  nrec++;
  n = fread(&epb,1,sizeof(epb),pf);
  if (n == sizeof(epb))
  {
    ts = epb.tsh;
    if (ts < 0) ts += 4294967296.0;
    if (epb.tsl < 0) ts++;
    ts = ts * 4294967296.0;
    ts += epb.tsl;
    ts /= timeresdiv;
    if (nrec == 1) 
    {
      lt = ts; 
      ptm = localtime(&lt);
      printf("set t0 %f (%d %d) timeresdiv %f %4d/%2d/%2d %02d:%02d:%02d\n",
             ts,epb.tsh,epb.tsl,
             timeresdiv,
             ptm->tm_mday,ptm->tm_mon,1900 + ptm->tm_year,
             ptm->tm_hour,ptm->tm_min,ptm->tm_sec);
      printf("--- end of header ---\n\n");
      tsess = t0s = t0 = ts;
    }
    if (ts < t0)
    {
      printf("t0 %f\n",t0);
      printf("epb h %d l %d\n",epb.tsh,epb.tsl);
      printf("ts %f\n",ts);
      exit(0);
    }
//  lineheader(nrec,ts);
/*
    printf("epb iid %x ts %.0f cpl %d opl %d\n",
            epb.iid,ts,epb.cpl,epb.opl);
*/
    pd = malloc(epb.cpl);
    n = fread(pd,1,epb.cpl,pf);
    if (n != epb.cpl) 
      myerror("fread",__LINE__);
    dl = usbheader(pd,pdec,ts,&filter);
    hl = (short *) pd;
//  memdump(pd,epb.cpl,0);
    if (dl > 0 && filter && verbose)
    {
      if (dl > 1024)
        memdump(pd + *hl,128,"B>> ",1);
      else
        memdump(pd + *hl,dl,"B>> ",1);
    }
    
    free(pd); 
  }
  return 1;
}

void initswt()
{
  swt[0].blocktype = 0;
  swt[0].handler   = hshb;
  swt[1].blocktype = 1;
  swt[1].handler   = hidb;
  swt[6].blocktype = 6;
  swt[6].handler   = hepb;
}

int readblock(FILE *pf,struct t_decode *psd)
{
  int n,h0[2],h1[1],pos0,pos,pos1;

  pos0 = ftell(pf);
  n = fread(h0,1,sizeof h0,pf);
  if (n == sizeof h0)
  {
    pos = ftell(pf);
    n = fseek(pf,h0[1] - sizeof(h0) - sizeof(h1),SEEK_CUR);
    if (n != 0) 
      myerror("fseek",__LINE__);
    n = fread(&h1,1,sizeof(h1),pf);
    if (n != sizeof(h1))
    {
      printf("file read error n %d\n",n);
      return 0;
    }
    if (deb)
      printf("%08x readblock len %d %d type %08x\n",pos0,h0[1],h1[0],h0[0]);
    if (h0[0] == 0x0a0d0d0a)
      h0[0] = 0;
    if (h0[0] >= 0 && h0[0] < NSW)
    {
      if (swt[h0[0]].handler)
      {
        pos1 = ftell(pf);
        n = fseek(pf,pos,SEEK_SET);
        if (n != 0) 
          myerror("fseek",__LINE__);
        swt[h0[0]].ncall++;
        swt[h0[0]].handler(pf,h1[0],psd);
        n = fseek(pf,pos1,SEEK_SET);
        if (n != 0) 
          myerror("fseek",__LINE__);
      } else
        printf("no function on block type %d\n",h0[0]);
    } else
      printf("block type out of range %d\n",h0[0]);
  } else
  {
    if (feof(pf))
    {
      addsec(nrec - nrec0,tsess,t0s);
      printf("EOF\n");
      dumpadd();
      arm_dump_allreg(0);
      dumpq();
      dumpses();
    } else
      printf("file read error n %d \n",n);
    return 0;
  }
  return 1;
}

void setfadd(char *add)
{
  int n,a,b,c;
  n = sscanf(add,"%d.%d.%d",&a,&b,&c);
  if (n == 1)
  {
    fbus = a;
    fdevice = -1;
    fendp = -1;
  } else if (n == 2)
  {
    fbus = a;
    fdevice = b;
    fendp = -1;
  } else if (n == 3)
  {
    fbus = a;
    fdevice = b;
    fendp = c;
  } else 
  {
    printf("n %d %s ",n,add);
    myerror("wrong address",__LINE__);
  }
}

void setdadd(char *add)
{
  int n,a,b,c;
  n = sscanf(add,"%d.%d.%d",&a,&b,&c);
  if (n == 3)
  {
    dbus = a;
    ddevice = b;
    dendp = c;
  } else 
  {
    printf("n %d %s ",n,add);
    myerror("wrong address",__LINE__);
  }
}

void readfile(FILE *pf,int logh)
{
  struct t_decode sd;
  int ok,n;

  memset(&sd,0,sizeof sd);
  sd.logh = logh;
  if (dectab[proto].finit)
    dectab[proto].finit(&sd,2);
  n = 0;
  for (ok = 1; ok; n++)
    ok = readblock(pf,&sd);
  if (dectab[proto].fend)
    dectab[proto].fend(&sd);
}

int main(int n,char **av) 
{
  int i;
  char *fn;
  FILE *pf;
  verbose = 0;
  int logh = 0;

  fn = "usbmouse.pcapng";
  dbus = ddevice = dendp = 0;
  fbus = fdevice = fendp = -1; // allow all
  for (i = 1; i < n; i++)
  {
    if (strcmp(av[i],"-v") == 0)
    {
      verbose = 1;
      logh = 1;
    } if (strcmp(av[i],"-lh") == 0)
      logh = 1;
    else if (strcmp(av[i],"-nlh") == 0)
      logh = 0;
    if (strcmp(av[i],"-a") == 0 && i < n - 1)
    {
      setdadd(av[i+1]);
      i++;
    } else if (strcmp(av[i],"-f") == 0 && i < n - 1)
    {
      setfadd(av[i+1]);
      i++;
    } else if (strcmp(av[i],"-pico") == 0)
    {
      proto = PICO;
    } else if (strcmp(av[i],"-stlink") == 0)
    {
      proto = STLINK;
    } else if (strcmp(av[i],"-cmsis-dap") == 0)
    {
      proto = CMSIS_DAP;
    } else
      fn = av[i];
  }

  initswt();
  initswcmd();
  arm_initreg(verbose);
  initproto();
  
  funcdec[URB_FUNCTION_SELECT_CONFIGURATION]          = "select configuration";
  funcdec[URB_FUNCTION_CONTROL_TRANSFER]              = "control transfer";
  funcdec[URB_FUNCTION_BULK_OR_INTERRUPT_TRANSFER]    = "bulk or interrupt";
  funcdec[URB_FUNCTION_GET_DESCRIPTOR_FROM_DEVICE]    = "get descriptor from device";
  funcdec[URB_FUNCTION_VENDOR_DEVICE]                 = "vendor device";
  funcdec[URB_FUNCTION_CLASS_INTERFACE]               = "class interface";
  funcdec[URB_FUNCTION_GET_DESCRIPTOR_FROM_INTERFACE] = "get descriptor from interface";

  trandec[1]    = "interrupt";
  trandec[2]    = "control";
  trandec[3]    = "bulk";
 
  ctsdec[0]     = "setup";
  ctsdec[1]     = "data";
  ctsdec[2]     = "status";
  ctsdec[3]     = "complete";

  pf = fopen(fn,"rb");
  if (pf)
  {
    printf("read input file %s\n",fn);
    printf("decode %d.%d.%d filter %d.%d.%d\n",
           dbus,ddevice,dendp,fbus,fdevice,fendp);
    readfile(pf,logh);
    fclose(pf);
  } else
    printf("can't open %s\n",fn);
  return 0;
}
