#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
// see note on libusb-1.0
//#include <libusb-1.0/libusb.h>
#include "libusb.h"

#include "env.h"
#include "util.h"
#include "conf.h"
#include "readpcapng-stlink.h"
#include "dap-decode.h"
#include "dapreg.h"
#include "armreg.h"
#include "driver-cmsis.h"
#include "driver-pico.h"

#include "disasm.h"
#include "common-driver.h"
#include "dump.h"
#include "test.h"
#include "jim-interface-stlink.h"

#include <jim.h>

static const char *strpar(Jim_Interp *interp,
                          int n,int argc,
                          Jim_Obj *const *argv,
                          const char *pname,
                          const char *fname,
                          const char *dval)
{
  const char *v = dval;

  if (argc > n)
  {
    int len;
    v = Jim_GetString(argv[n],&len);
  } 
  return v;
}

static long longpar(Jim_Interp *interp,
                    int n,int argc,
                    Jim_Obj *const *argv,
                    const char *pname,
                    const char *fname,
                    long dval)
{
  long v = dval;

  if (argc > n)
  {
    int res = Jim_GetLong(interp,argv[n],&v);
    if (res != JIM_OK)
      error("wrong parameter %d %s in %s",n,pname,fname);
  }
  return v;
}

static int jim_comment(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  printf("### ");
  for (int i = 1; i < argc; i++)
  {
    Jim_Obj *jo = argv[i];
    printf("%s ",Jim_GetString(jo,0));
  }
  putchar('\n');

  return JIM_OK;
}

static int jim_connect(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  long flag = 0,deb = 0;
  stlink_t *sl = interp->cmdPrivData;

  if (argc < 2)
    error("wrong number of parameters in connect");

  flag = longpar(interp,1,argc,argv,"flag","connect",0);
  deb  = longpar(interp,2,argc,argv,"deb","connect",0);
  logmsg("connect flag %d deb %d",flag,deb);

  connect(sl,flag,deb);
  return JIM_OK;
}

static int jim_test(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  int len;

  logmsg("in jim_test argc %d private %p",argc,interp->cmdPrivData);
  for (int i = 0; i < argc; i++)
  {
    Jim_Obj *jo = argv[i];
    printf("%d %p bytes %p typ %p %s rc %d len %d %d ",
           i,jo,jo->bytes,jo->typePtr,jo->typePtr->name,jo->refCount,jo->length,
           Jim_IsList(jo));
    const Jim_ObjType *ot = jo->typePtr;
    if (ot)
      printf("name %s flags %x ",ot->name,ot->flags);
    printf("val ");
    long l;
    double d;
    if (Jim_GetLong(interp,jo,&l) == JIM_OK)
      printf("long %ld ",l);
    else if (Jim_GetDouble(interp,jo,&d) == JIM_OK)
      printf("double %f ",d);
    else
      printf("str %s ",Jim_GetString(jo,&len));

    putchar('\n');
  }
  return JIM_OK;
}

static int jim_fill(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  long pattern,add,size;
  stlink_t *sl = interp->cmdPrivData;

  if (argc < 3)
    error("wrong number of parameters in fill");
  pattern = longpar(interp,1,argc,argv,"pattern","fill",0);
  add     = longpar(interp,2,argc,argv,"add","fill",0);
  size    = longpar(interp,3,argc,argv,"size","fill",4);

  logmsg("fill pattern %x add %x size %d",pattern,add,size);
  fillmem(sl,pattern,add,size);

  return JIM_OK;
}

static int jim_trace(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  long nstep = 1,flag = 0,start = 0;
  stlink_t *sl = interp->cmdPrivData;

  if (argc < 3)
    error("wrong number of parameters in trace");

  nstep = longpar(interp,1,argc,argv,"nstep","trace",1);
  flag  = longpar(interp,2,argc,argv,"flag","trace",0);
  start = longpar(interp,3,argc,argv,"start","trace",0);

  logmsg("trace nstep %d flag %d start %d",nstep,flag,start);
  trace(sl,nstep,flag,start);
  return JIM_OK;
}

// print reg
static int jim_preg(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  stlink_t *sl = interp->cmdPrivData;
  long flag;
  const char *desc;

  if (argc < 2)
    error("wrong number of parameters in preg");

  flag = longpar(interp,1,argc,argv,"flag","preg",0);
  desc = strpar(interp,2,argc,argv,"desc","preg","");

  logmsg("preg flag %d desc %s",flag,desc);
  dumpreg(sl,flag,desc);
  return JIM_OK;
}

static int jim_read(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  stlink_t *sl = interp->cmdPrivData;
  long add,size;
  const char *fn;

  if (argc < 3)
    error("wrong number of parameters in read(add,size[,fileName])");

  add  = longpar(interp,1,argc,argv,"add","read",0);
  size = longpar(interp,2,argc,argv,"size","read",4);
  fn   = strpar(interp,3,argc,argv,"fileName","read",0);

  logmsg("read add %x size %d file %s",add,size,fn ? fn : "none");
  exec_read(sl,add,size,fn);

  return JIM_OK;
}

static int jim_dump(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  printf("TODO jim_dump\n");
  exit(0); 
  return JIM_OK;
}

static int jim_continue(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  stlink_t *sl = interp->cmdPrivData;

  logmsg("continue");
  contcpu(sl);
  
  return JIM_OK;
}

// qsort compare
static int cmp(const void *p,const void *q)
{
  const uint *a,*b;
  a = (const uint *) p;
  b = (const uint *) q;
  return *a - *b;
}

static int dis_read_mem(struct disasm_par *par,uint pc,uchar *code,size_t len)
{
  return stlink_read_mem32(par->env, pc & ~3 ,code, len);
}

static int jim_run(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  stlink_t *sl = interp->cmdPrivData;
  long wtime = 100000, count = 1;
  struct t_stat st,*p_st;
  struct t_cpu  cpu  = {0},*p_cpu;

  p_st = &st;
  p_cpu = &cpu;

  if (argc < 2)
    error("wrong number of parameters in run");

  wtime = longpar(interp,1,argc,argv,"wtime","run",100000);
  count = longpar(interp,2,argc,argv,"count","run",1);

  logmsg("run wtime %d count %d",wtime,count);

  uint *mem = malloc(count * sizeof(uint));
  runcpu(sl,wtime,count,mem,1);
  qsort(mem,count,sizeof(mem[0]),cmp);
  int i,j = 0;
  struct disasm_par dp = {sl,dis_read_mem};
/*
  for (i = 0; i < count; i++)
    printf("X %2d %08x\n",i,mem[i]);
*/
  for (i = 1; i < count; i++)
    if (mem[i] != mem[i - 1])
    {
      printf("[%4d] ",i - j);
      disasm(&dp,p_cpu,mem[i - 1],p_st);
//    putchar('\n');
      j = i;
    }
  printf("[%4d] ",i - j);
  disasm(&dp,p_cpu,mem[i - 1],p_st);

//putchar('\n');
  free(mem);
  
  return JIM_OK;
}

static int jim_load(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  stlink_t *sl = interp->cmdPrivData;
  long pc,sp,loadadd;

  if (argc < 3)
    error("wrong arguments in load(finename,initial_pc,initial_sp)");

  const char *fn = strpar(interp,1,argc,argv,"fileName","read",0);
  pc = longpar(interp,2,argc,argv,"initialPC","load",0);
  sp = longpar(interp,3,argc,argv,"initialSP","load",0);
  loadadd = longpar(interp,4,argc,argv,"loadadd","load",0x20000000);

  logmsg("load fn %s pc %x sp %x loadadd %x",fn,pc,sp,loadadd);
  loadprg(sl,fn,loadadd,pc,sp,1);
  return JIM_OK;
}

static int jim_break(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  stlink_t *sl = interp->cmdPrivData;
  long add,id;

  if (argc < 2)
    error("wrong arguments in break(address, id)");

  add = longpar(interp,1,argc,argv,"add","break",0);
  id  = longpar(interp,2,argc,argv,"id","break",0);

  logmsg("break add %x id %d",add,id);
  breakpoint(sl,id,add);

  return JIM_OK;
}

static int jim_exit(Jim_Interp *interp, int argc, Jim_Obj *const *argv)
{
  exit(0); 
  return JIM_OK;
}

void jim_file_stlink(char *fname,stlink_t *cp)
{
  logmsg("jim_file %s cp %p",fname,cp);
  Jim_Interp *interp;
  struct jim_data data;
  int error;

  assert(cp->priv == 0 && "cp->priv not null");
  cp->priv = &data;
  /* Create an interpreter. */
  interp = Jim_CreateInterp();
  assert(interp != NULL && "couldn't create interpreter");

  /* We register base commands, so that we actually implement Tcl. */
  Jim_RegisterCoreCommands(interp);

  /* And initialise any static extensions */
  Jim_InitStaticExtensions(interp);

  /* Register our Jim commands. */
  Jim_CreateCommand(interp, "test"        , jim_test        , cp, NULL);
  Jim_CreateCommand(interp, "connect"     , jim_connect     , cp, NULL);
  Jim_CreateCommand(interp, "fill"        , jim_fill        , cp, NULL);
  Jim_CreateCommand(interp, "trace"       , jim_trace       , cp, NULL);
  Jim_CreateCommand(interp, "preg"        , jim_preg        , cp, NULL);
  Jim_CreateCommand(interp, "read"        , jim_read        , cp, NULL);
  Jim_CreateCommand(interp, "dump"        , jim_dump        , cp, NULL);
  Jim_CreateCommand(interp, "run"         , jim_run         , cp, NULL);
  Jim_CreateCommand(interp, "load"        , jim_load        , cp, NULL);
  Jim_CreateCommand(interp, "break"       , jim_break       , cp, NULL);
  Jim_CreateCommand(interp, "exit"        , jim_exit        , cp, NULL);
  Jim_CreateCommand(interp, "comment"     , jim_comment     , cp, NULL);
  Jim_CreateCommand(interp, "continue"    , jim_continue    , cp, NULL);

/*
 * Parse a script
 */
  error = Jim_EvalFile(interp, fname);
  if (error == JIM_ERR) 
  {
    Jim_MakeErrorMessage(interp);
    fprintf(stderr, "%s\n", Jim_GetString(Jim_GetResult(interp), NULL));
  }

  Jim_FreeInterp(interp);
  cp->priv = 0;
  return;
}
