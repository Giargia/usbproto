#define F1XX_RCC_BASE            0x40021000
#define F1XX_RCC_SIZE                (4*10)

#define F1XX_RCC_CR_OFF                 0x0
#define F1XX_RCC_CFGR_OFF               0x4
#define F1XX_RCC_CIR_OFF                0x8
#define F1XX_RCC_APB2RSTR_OFF           0xc
#define F1XX_RCC_APB1RSTR_OFF          0x10
#define F1XX_RCC_AHBENR_OFF            0x14
#define F1XX_RCC_APB2ENR_OFF           0x18
#define F1XX_RCC_APB1ENR_OFF           0x1c
#define F1XX_RCC_BDCR_OFF              0x20
#define F1XX_RCC_CSR_OFF               0x24

#define F1XX_GPIOA_BASE          0x40010800
#define F1XX_GPIOB_BASE          0x40010c00
#define F1XX_GPIOC_BASE          0x40011000
#define F1XX_GPIO_SIZE                (4*7)

#define F1XX_GPIO_CRL_OFF               0x0
#define F1XX_GPIO_CRH_OFF               0x4
#define F1XX_GPIO_IDR_OFF               0x8
#define F1XX_GPIO_ODR_OFF               0xc
#define F1XX_GPIO_BSRR_OFF             0x10
#define F1XX_GPIO_BRR_OFF              0x14
#define F1XX_GPIO_LCKR_OFF             0x18

#define F4XX_RCC_BASE            0x40023800
#define F4XX_RCC_SIZE                (0x88)
