#

st-info.pcappng

1) no usb device
start capture, no traffic
2) insert stlinkV2 connected to stm32f407
first 13 packets
3) /cygdrive/c/prog_src/my_fix/stlink-master/st-info --probe > st-info.log

08/11/2023
- cmsis-dap with pyocd
- air32f103 + luaos
- gdb
- wireshark

trace file: 

decode with: cmsis-dap-air32f103.pcapng

readpcapng-stlink -a 1.16.3 -cmsis-dap cmsis-dap-air32f103.pcapng

pyocd:

$ pyocd.exe gdbserver
0006930 W Generic 'cortex_m' target type is selected by default; is this intentional? You will be able to debug most devices, but not program  flash. To set the target type use the '--target' argument or 'target_override' option. Use 'pyocd list --targets' to see available targets types. [board]
0006930 I Target type is cortex_m [board]
0007060 I DP IDR = 0x2ba01477 (v1 rev2) [dap]
0007080 I AHB-AP#0 IDR = 0x24770011 (AHB-AP var1 rev2) [discovery]
0007090 I AHB-AP#0 Class 0x1 ROM table #0 @ 0xe00ff000 (designer=000 part=000) [rom_table]
0007100 I [0]<e000e000:SCS v7-M class=14 designer=43b:Arm part=000> [rom_table]
0007100 I [1]<e0001000:DWT v7-M class=14 designer=43b:Arm part=002> [rom_table]
0007100 I [2]<e0002000:FPB v7-M class=14 designer=43b:Arm part=003> [rom_table]
0007110 I [3]<e0000000:ITM v7-M class=14 designer=43b:Arm part=001> [rom_table]
0007120 I [4]<e0040000:TPIU M3 class=9 designer=43b:Arm part=923 devtype=11 archid=0000 devid=ca1:0:0> [rom_table]
0007130 I [5]<e0041000:ETM M3 class=9 designer=43b:Arm part=924 devtype=13 archid=0000 devid=0:0:0> [rom_table]
0007130 I CPU core #0 is Cortex-M3 r2p0 [cortex_m]
0007140 I 4 hardware watchpoints [dwt]
0007150 I 6 hardware breakpoints, 4 literal comparators [fpb]
0007210 I Semihost server started on port 4444 (core 0) [server]
0010365 I GDB server started on port 3333 (core 0) [gdbserver]
0048300 I Client connected to port 3333! [gdbserver]
0084210 I Client detached [gdbserver]
0084220 I Client disconnected from port 3333! [gdbserver]
0084340 I Semihost server stopped [server]

V.Giargia@AT-VGiargia5 ~

gdb:

$ arm-none-eabi-gdb
GNU gdb (GNU Arm Embedded Toolchain 10.3-2021.10) 10.2.90.20210621-git
Copyright (C) 2021 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Type "show copying" and "show warranty" for details.
This GDB was configured as "--host=i686-w64-mingw32 --target=arm-none-eabi".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<https://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".
Type "apropos word" to search for commands related to "word".
(gdb) tar rem :3333
Remote debugging using :3333
warning: No executable has been specified and target does not support
determining executable automatically.  Try using the "file" command.
0x08013406 in ?? ()
(gdb) info reg
r0             0x0                 0
r1             0xffff              65535
r2             0x0                 0
r3             0x20002518          536880408
r4             0x800               2048
r5             0x16                22
r6             0x0                 0
r7             0x0                 0
r8             0x0                 0
r9             0xb                 11
r10            0x0                 0
r11            0x2000004c          536870988
r12            0x80143e1           134300641
sp             0x20002560          0x20002560
lr             0x80133bb           0x80133bb
pc             0x8013406           0x8013406
xpsr           0x41000000          1090519040
msp            0x20002db8          0x20002db8
psp            0x20002560          0x20002560
primask        0x0                 0
control        0x2                 2
basepri        0x0                 0
faultmask      0x0                 0
(gdb) stepi
0x08013408 in ?? ()
(gdb)
0x08013410 in ?? ()
(gdb)
0x08013412 in ?? ()
(gdb) c
Continuing.

Program received signal SIGINT, Interrupt.
0x08013414 in ?? ()
(gdb) where
#0  0x08013414 in ?? ()
#1  0x080133ba in ?? ()
Backtrace stopped: previous frame identical to this frame (corrupt stack?)
(gdb) info reg
r0             0x80000000          -2147483648
r1             0xffff              65535
r2             0x0                 0
r3             0x20002518          536880408
r4             0x800               2048
r5             0x16                22
r6             0x0                 0
r7             0x0                 0
r8             0x0                 0
r9             0xb                 11
r10            0x0                 0
r11            0x2000004c          536870988
r12            0x80143e1           134300641
sp             0x20002560          0x20002560
lr             0x80133bb           0x80133bb
pc             0x8013414           0x8013414
xpsr           0x81000000          -2130706432
msp            0x20002db8          0x20002db8
psp            0x20002560          0x20002560
primask        0x0                 0
control        0x2                 2
basepri        0x0                 0
faultmask      0x0                 0
(gdb) quit
A debugging session is active.

        Inferior 1 [Remote target] will be detached.

Quit anyway? (y or n) y
Detaching from program: , Remote target
Ending remote debugging.
[Inferior 1 (Remote target) detached]

V.Giargia@AT-VGiargia5 /cygdrive/c/_valter/software/inter/win

24/11/2023

test run 1
file cmsis-dap_run1.pcapng
     cmsis-dap_run1.log pyOCD output
action             lines in wireshark
                   12
- connect cable -> 50
- pyOCD.exe -gdbserver -v 292
- stop pyOCD       312
- disconnect cable  316

test run 2
file cmsis-dap_run2.pcapng
     cmsis-dap_run2.log pyOCD output

- connect cable -> 50
- pyOCD.exe -gdbserver -v 292
gdb             -> 448
gdbcommad
stepi
stepi
cont
^C
quit            -> 2196
- disconnect cable -> 2200

extra output due to reconnected mouse

run2 gdb output:
$ arm-none-eabi-gdb
GNU gdb (GNU Arm Embedded Toolchain 10.3-2021.10) 10.2.90.20210621-git
Copyright (C) 2021 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Type "show copying" and "show warranty" for details.
This GDB was configured as "--host=i686-w64-mingw32 --target=arm-none-eabi".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<https://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".
Type "apropos word" to search for commands related to "word".
(gdb) tar rem :3333
Remote debugging using :3333
warning: No executable has been specified and target does not support
determining executable automatically.  Try using the "file" command.
0x080013c2 in ?? ()
(gdb) info reg
r0             0x12c               300
r1             0x5                 5
r2             0xe000e000          -536813568
r3             0xe000e000          -536813568
r4             0x0                 0
r5             0x200014c0          536876224
r6             0x0                 0
r7             0x0                 0
r8             0x0                 0
r9             0x0                 0
r10            0x8003bd8           134233048
r11            0x0                 0
r12            0x0                 0
sp             0x20001b18          0x20001b18
lr             0x8001ec1           0x8001ec1
pc             0x80013c2           0x80013c2
xpsr           0x61000000          1627389952
fpscr          0x3000000           50331648
msp            0x20001b18          0x20001b18
psp            0x0                 0x0
primask        0x0                 0
control        0x4                 4
basepri        0x0                 0
faultmask      0x0                 0
(gdb) stepi
0x080013c6 in ?? ()
(gdb) stepi
0x080013c8 in ?? ()
(gdb) c
Continuing.

Program received signal SIGINT, Interrupt.
0x080013c0 in ?? ()
(gdb) info regi
r0             0x96                150
r1             0x5                 5
r2             0xe000e000          -536813568
r3             0xe000e000          -536813568
r4             0x0                 0
r5             0x200014c0          536876224
r6             0x0                 0
r7             0x0                 0
r8             0x0                 0
r9             0x0                 0
r10            0x8003bd8           134233048
r11            0x0                 0
r12            0x0                 0
sp             0x20001b18          0x20001b18
lr             0x8001ec1           0x8001ec1
pc             0x80013c0           0x80013c0
xpsr           0x61000000          1627389952
fpscr          0x3000000           50331648
msp            0x20001b18          0x20001b18
psp            0x0                 0x0
primask        0x0                 0
control        0x4                 4
basepri        0x0                 0
faultmask      0x0                 0
(gdb) quit
A debugging session is active.

        Inferior 1 [Remote target] will be detached.

Quit anyway? (y or n) y
Detaching from program: , Remote target
Ending remote debugging.
[Inferior 1 (Remote target) detached]


