#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include "libusb.h"
#include "driversub.h"
#include "chipid.h"
#include "conf.h"

stlink_t *open_usb(int verbose, bool reset, char serial[16],int log,int vid,int pid,char *devname)
{
  stlink_t* sl = NULL;
  struct stlink_libusb* slu = NULL;
  int ret = -1;
  int config;

//printf("stlink_open_usb log %d\n",log);
#ifdef D
  if (log & 2)
    printf("stlink_open_usb reset %d serial %s\n",reset,serial);
#endif
  sl  = calloc(1, sizeof (stlink_t));
  slu = calloc(1, sizeof (struct stlink_libusb));
  if (sl == NULL)
    goto on_malloc_error;
  if (slu == NULL)
    goto on_malloc_error;

  sl->backend_data = slu;
  slu->log = log;

  sl->core_stat = STLINK_CORE_STAT_UNKNOWN;
  if (libusb_init(&(slu->libusb_ctx))) 
  {
    WLOG("failed to init libusb context, wrong version of libraries?\n");
    goto on_error;
  }

  libusb_device **list;
  /** @todo We should use ssize_t and use it as a counter if > 0. 
       As per libusb API: ssize_t libusb_get_device_list (libusb_context *ctx, libusb_device ***list) */
  int cnt = (int) libusb_get_device_list(slu->libusb_ctx, &list);
#ifdef D
  if (slu->log & 2)
    printf("found %d device\n",cnt);
#endif
  struct libusb_device_descriptor desc;
  int devBus  = 0;
  int devAddr = 0;

    /* @TODO: Reading a environment variable in a usb open function is not very nice, this
      should be refactored and moved into the CLI tools, and instead of giving USB_BUS:USB_ADDR a real stlink
      serial string should be passed to this function. Probably people are using this but this is very odd because
      as programmer can change to multiple busses and it is better to detect them based on serial.  */
  char *device = getenv("STLINK_DEVICE");
  if (device) 
  {
    char *c = strchr(device,':');
    if (c==NULL) {
      WLOG("STLINK_DEVICE must be <USB_BUS>:<USB_ADDR> format\n");
      goto on_error;
    }
    devBus=atoi(device);
    *c++=0;
    devAddr=atoi(c);
#ifdef D
    if (slu->log & 2)
      printf("STLINK_DEVICE %s bus %d dev %d\n",device,devBus,devAddr);
#endif
    ILOG("bus %03d dev %03d\n",devBus, devAddr);
  }

  while (cnt--) 
  {
    libusb_get_device_descriptor( list[cnt], &desc );
#ifdef D
    if (slu->log & 2)
      printf("idVendor %04x idproduct %04x\n",desc.idVendor,desc.idProduct);
#endif
    if (desc.idVendor != vid)
      continue;

    if (devBus && devAddr) 
    {
      if ((libusb_get_bus_number(list[cnt]) != devBus)
          || (libusb_get_device_address(list[cnt]) != devAddr)) 
        continue;
    }

    if ((desc.idProduct == pid)) // || (desc.idProduct == STLINK_USB_PID_STLINK_NUCLEO)) 
    {
      struct libusb_device_handle *handle;

      ret = libusb_open(list[cnt], &handle);
      if (ret)
        continue;

      sl->serial_size = libusb_get_string_descriptor_ascii(handle, desc.iSerialNumber,
                                                           (uchar *)sl->serial, sizeof(sl->serial));
      libusb_close(handle);
#ifdef D
      if (slu->log & 2)
        printf("serial %s\n",sl->serial);
#endif

      if ((serial == NULL) || (*serial == 0))
        break;

      if (sl->serial_size < 0)
        continue;

      if (memcmp(serial, &sl->serial, sl->serial_size) == 0)
        break;

      continue;
    }

    if (desc.idProduct == STLINK_USB_PID_STLINK) 
    {
      slu->protocoll = 1;
#ifdef D
      if (slu->log & 2)
        printf("idProduct %x protocoll 1\n",desc.idProduct);
#endif
      break;
    }
  }

  if (cnt < 0) 
  {
    WLOG ("Couldn't find %s %s devices\n",(devBus && devAddr)?"matched":"any",devname);
    goto on_error;
  } else 
  {
    ret = libusb_open(list[cnt], &slu->usb_handle);
    if (ret != 0) 
    {
      WLOG("Error %d (%s) opening ST-Link/V2 device %03d:%03d\n",
           ret, strerror (errno), libusb_get_bus_number(list[cnt]), libusb_get_device_address(list[cnt]));
      goto on_error;
    }
  }

  libusb_free_device_list(list, 1);

  int r;
  if ((r = libusb_kernel_driver_active(slu->usb_handle, 0)) == 1) 
  {
    ret = libusb_detach_kernel_driver(slu->usb_handle, 0);
#ifdef D
    if (slu->log & 2)
      printf("libusb_detach_kernel_driver -> %d\n",ret);
#endif
    if (ret < 0) 
    {
      WLOG("libusb_detach_kernel_driver(() error %s\n", strerror(-ret));
      goto on_libusb_error;
    }
  }
#ifdef D 
  if (slu->log & 2)
    printf("libusb_kernel_driver_active -> %d\n",r);
#endif

  if (libusb_get_configuration(slu->usb_handle, &config)) 
  {
    /* this may fail for a previous configured device */
    WLOG("libusb_get_configuration()\n");
    goto on_libusb_error;
  }
#ifdef D
  if (slu->log & 2)
    printf("config %d\n",config);
#endif

  if (config != 1) 
  {
    printf("setting new configuration (%d -> 1)\n", config);
    if ((r = libusb_set_configuration(slu->usb_handle, 1)) )
    {
      /* this may fail for a previous configured device */
      WLOG("libusb_set_configuration() failed err %d %s\n",r,libusb_error_name(r));
      goto on_libusb_error;
    }
  }

  if ((r = libusb_claim_interface(slu->usb_handle, 0))) 
  {
    WLOG("Stlink usb device found, but unable to claim (probably already in use?) err %d %s\n",r,libusb_error_name(r));
    goto on_libusb_error;
  }

    // TODO - could use the scanning techniq from stm8 code here...
  slu->ep_rep = 1 /* ep rep */ | LIBUSB_ENDPOINT_IN;
  if (desc.idProduct == STLINK_USB_PID_STLINK_NUCLEO) 
    slu->ep_req = 1 /* ep req */ | LIBUSB_ENDPOINT_OUT;
  else 
    slu->ep_req = 2 /* ep req */ | LIBUSB_ENDPOINT_OUT;
#ifdef D
  if (slu->log & 2)
    printf("idProduct %x (%x) ep_req %d\n",desc.idProduct,STLINK_USB_PID_STLINK_NUCLEO,slu->ep_req);
#endif

  slu->sg_transfer_idx = 0;
  // TODO - never used at the moment, always CMD_SIZE
  slu->cmd_len = (slu->protocoll == 1)? STLINK_SG_SIZE: STLINK_CMD_SIZE;

/*
  if (stlink_current_mode(sl) == STLINK_DEV_DFU_MODE) 
  {
    ILOG("-- exit_dfu_mode\n");
    stlink_exit_dfu_mode(sl);
  }

  if (stlink_current_mode(sl) != STLINK_DEV_DEBUG_MODE) 
    stlink_enter_swd_mode(sl);

  if (reset) 
  {
    stlink_reset(sl);
    usleep(10000);
  }

  stlink_version(sl);
  ret = stlink_load_device_params(sl);
*/
on_libusb_error:
  if (ret == -1) 
  {
//  stlink_close(sl);
    return NULL;
  }

  return sl;

on_error:
  printf("call libusb_exit\n");
  if (slu->libusb_ctx)
    libusb_exit(slu->libusb_ctx);

on_malloc_error:
  if (sl != NULL)
    free(sl);
  if (slu != NULL)
    free(slu);

  return NULL;
}

stlink_t *stlink_open_usb(int verbose, bool reset, char serial[16],int log)
{
  return open_usb(verbose,reset,serial,log,STLINK_USB_VID_ST,STLINK_USB_PID_STLINK_32L,"STLink/V2");
}

stlink_t *pico_open_usb(int verbose, bool reset, char serial[16],int log)
{
  return open_usb(verbose,reset,serial,log, 0x2e8a, 0x0004,"picoprobe");
}

int main(int na,char **v)
{
//  stlink_t* sl = stlink_open();
  int i,log = 0;
  int n,add = 0,len = 0;
  int reset = 0;

  printf("don't work see driver-cmsis -vendor 0x2e8a -product 0x4\n");
  exit(0);

  for (i = 1; i < na; i++)
  {
    if (strcmp(v[i],"-add") == 0 && i < na - 1)
    {
      n = sscanf(v[++i],"%i",&add);
      if (n != 1)
        add = 0;
      else if (len == 0) 
        len = 64;
    } else if (strcmp(v[i],"-len") == 0 && i < na - 1)
    {
      n = sscanf(v[++i],"%i",&len);
      if (n != 1)
        len = add ? 64 : 0;
    } else if (strcmp(v[i],"-log") == 0 && i < na - 1)
      log = atoi(v[++i]);
    else if (strcmp(v[i],"-reset") == 0)
       reset = 1;
    printf("add %08x len %d\n",add,len);
    
  }
    
  char serial[80] = {0};
  printf("reset %d\n",reset);
  stlink_t* sl = pico_open_usb(1, reset, serial,log);
  
  if (sl)
  {
    int t,res;
    uchar txbuf[1024];
    int   txsize;
    printf("device found\n");
//  sleep(60);
    txsize = 7;
    txbuf[0] = 0xff;
    txbuf[1] = 0xff;
    txbuf[2] = 0xff;
    txbuf[3] = 0xff;
    txbuf[4] = 0xff;
    txbuf[5] = 0xff;
    txbuf[6] = 0x00;
    t = libusb_bulk_transfer(sl->backend_data->usb_handle, sl->backend_data->ep_req,
            txbuf,
            (int) txsize,
            &res,
            3000);
    printf("t %d res %d\n",t,res);

    libusb_close(sl->backend_data->usb_handle);
  }
//libusb_exit(ctx);
}

