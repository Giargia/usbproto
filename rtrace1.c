// trace format:
/*
status at 0x8004f4f8
status
[  0] 00000800 0000.0000.0000.0000.0000.1000.0000.0000
[  1] a84f7159 1010.1000.0100.1111.0111.0001.0101.1001
[  2] 0f749509 0000.1111.0111.0100.1001.0101.0000.1001
*/
// [0] count
// [1] .. [n]   cd<dwt_cyccnt low 30 bit> c = swd clock bit d = swd data bit
/*
The probe outputs data to SWDIO on the falling edge of SWDCLK. The probe captures data from SWDIO on the rising edge of SWDCLK. The target outputs data to SWDIO on the rising edge of SWDCLK. The target captures data from SWDIO on the rising edge of SWDCLK.

*/


#define FILE int
#define CLK  30 
#define DATA 31

void decode(int *data,int ndata)
{
  int i,j,clk,dat,tick,tick0;
  int clk0,dat0;
  int v;

  printf("decode ndata %d data[0] %d\n",ndata,data[0]);

  for (i = 1; i < data[0] && i < ndata; i++)
  { 
    if (i % 32 == 1) printf("%\n%4d: ",i);
    clk  = (data[i] >>  CLK) & 1;
    dat  = (data[i] >> DATA) & 1;
    tick = data[i] & 0x3fffffff;
    printf("%d%d.",clk,dat);
  }
  putchar('\n');
  j = clk0 = dat0 = tick0 = v = 0;
  for (i = 1; i < data[0] && i < ndata; i++)
  { 
    clk  = (data[i] >>  CLK) & 1;
    dat  = (data[i] >> DATA) & 1;
    tick = data[i] & 0x3fffffff;
    if (tick - tick0 > 200) 
    {
      printf("*%02x*",v);
      v = 0;
    }
    if (clk == 0 && clk0 == 1)
    {
      j++;
      v = (v >> 1) | (dat0 << 7);
      printf("%d.",dat0);
      if (j % 32 == 0)
        putchar('\n');
    }
    clk0 = clk;
    dat0 = dat; 
    tick0 = tick;
  }
  putchar('\n');
  j = clk0 = dat0 = tick0 = v = 0;
  for (i = 1; i < data[0] && i < ndata; i++)
  { 
    clk  = (data[i] >>  CLK) & 1;
    dat  = (data[i] >> DATA) & 1;
    tick = data[i] & 0x3fffffff;
    if (tick - tick0 > 200) 
    {
      printf("*%02x*",v);
      v = 0;
    }
    if (clk == 0 && clk0 == 1)
    {
      j++;
      v = (v >> 1) | (dat0 << 7);
      printf("%d.",dat0);
      if (j % 32 == 0)
        putchar('\n');
    }
    clk0 = clk;
    dat0 = dat; 
    tick0 = tick;
  }
  putchar('\n');
}

int rfile(FILE *pf,int *data,int ndata)
{
  int  n,i,d,d0,clk,dat,tick;
  char line[200];
  char *p;
  
  d0 = 0;
  while (p = fgets(line,sizeof line,pf))
  {
    n = sscanf(line,"\[%d\] %x",&i,&d);
    if (n == 2)
    {
      clk  = (d >>  CLK) & 1;
      dat  = (d >> DATA) & 1;
      tick = d & 0x3fffffff;
#ifdef D 
      if (i == 0)
        printf("count %d\n",d);
      else
        printf("%4d %08x c %d d %d cy %10u delta %10u\n",i,d,clk,dat,tick,tick - d0);
#endif
      if (i < ndata)
        data[i] = d;
      else
        printf("data overflow %d >= %d\n",i,ndata);
      d0 = d & 0x3fffffff;
    }
  }
  return i + 1;
}

int data[2048];

int main(int na,char **v)
{
  int n;
  char *fn;
  FILE *pf;

  if (na > 1) 
    fn = v[1];
  else
    fn = "swdtrace.lis";
  pf = fopen(fn,"r");
  if (pf)
  {
    n = rfile(pf,data,sizeof(data)/sizeof data[0]);
    fclose(pf);
    printf("found %d data\n",n);
    decode(data,n);
  } else
    printf("can't open %s\n",fn);
  return 0; 
}
