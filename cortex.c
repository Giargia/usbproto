#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
// see note on libusb-1.0
//#include <libusb-1.0/libusb.h>
#include "libusb.h"

#include "env.h"
#include "util.h"
#include "conf.h"
#include "readpcapng-stlink.h"
#include "dap-decode.h"
#include "dapreg.h"
#include "armreg.h"
#include "driver-cmsis.h"
#include "driver-pico.h"

#include "disasm.h"
#include "dump.h"
#include "test.h"
#include "jim-interface-cmsis.h"
#include "cortex.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

/* Supported Cortex-M Cores */
static const struct cortex_m_part_info cortex_m_parts[] = {
  {
    .impl_part = CORTEX_M0_PARTNO,
    .name = "Cortex-M0",
    .arch = ARM_ARCH_V6M,
  },
  {
    .impl_part = CORTEX_M0P_PARTNO,
    .name = "Cortex-M0+",
    .arch = ARM_ARCH_V6M,
  },
  {
    .impl_part = CORTEX_M1_PARTNO,
    .name = "Cortex-M1",
    .arch = ARM_ARCH_V6M,
  },
  {
    .impl_part = CORTEX_M3_PARTNO,
    .name = "Cortex-M3",
    .arch = ARM_ARCH_V7M,
    .flags = CORTEX_M_F_TAR_AUTOINCR_BLOCK_4K,
  },
  {
    .impl_part = CORTEX_M4_PARTNO,
    .name = "Cortex-M4",
    .arch = ARM_ARCH_V7M,
    .flags = CORTEX_M_F_HAS_FPV4 | CORTEX_M_F_TAR_AUTOINCR_BLOCK_4K,
  },
  {
    .impl_part = CORTEX_M7_PARTNO,
    .name = "Cortex-M7",
    .arch = ARM_ARCH_V7M,
    .flags = CORTEX_M_F_HAS_FPV5,
  },
  {
    .impl_part = CORTEX_M23_PARTNO,
    .name = "Cortex-M23",
    .arch = ARM_ARCH_V8M,
  },
  {
    .impl_part = CORTEX_M33_PARTNO,
    .name = "Cortex-M33",
    .arch = ARM_ARCH_V8M,
    .flags = CORTEX_M_F_HAS_FPV5,
  },
  {
    .impl_part = CORTEX_M35P_PARTNO,
    .name = "Cortex-M35P",
    .arch = ARM_ARCH_V8M,
    .flags = CORTEX_M_F_HAS_FPV5,
  },
  {
    .impl_part = CORTEX_M55_PARTNO,
    .name = "Cortex-M55",
    .arch = ARM_ARCH_V8M,
    .flags = CORTEX_M_F_HAS_FPV5,
  },
  {
    .impl_part = STAR_MC1_PARTNO,
    .name = "STAR-MC1",
    .arch = ARM_ARCH_V8M,
    .flags = CORTEX_M_F_HAS_FPV5,
  },
  {
    .impl_part = INFINEON_SLX2_PARTNO,
    .name = "Infineon-SLx2",
    .arch = ARM_ARCH_V8M,
  },
  {
    .impl_part = REALTEK_M200_PARTNO,
    .name = "Real-M200 (KM0)",
    .arch = ARM_ARCH_V8M,
  },
  {
    .impl_part = REALTEK_M300_PARTNO,
    .name = "Real-M300 (KM4)",
    .arch = ARM_ARCH_V8M,
    .flags = CORTEX_M_F_HAS_FPV5,
  },
};

// cortex_m.c cortex_m_examine
int cmsis_examine(struct t_cmsisprobe *cp,int fl)
{
  char buf[256];
  uint res = LIBUSB_OK;

  ENTRYP("cmsis_examine","targetsel %08x %d",cp->pmdap->daps[cp->c_dapidx].targetsel,fl);
  int s = get_count();
  int s1 = 0;
  if (fl)
  {
    res = cmsis_find_mem_ap(cp);
    CHKSEQ1(s + 10);
    res = cmsis_mem_ap_init(cp,0);
    CHKSEQ1(s + 14);
    uint cpuid,demcr;
    if (cp->ndap == 1)
    {
      struct t_dap *pdap = cp->pmdap->daps + cp->r_dapidx;
      pdap->use_packet = 1;
      res = cmsis_read_mem32(cp,CPUID_ADD,1,&cpuid,SET_CSW | SET_TAR);
    } else
      res = cmsis_read_mem32(cp,CPUID_ADD,1,&cpuid,SET_CSW | SET_TAR);

    uint impl_part = cpuid & (CPUID_IMPL_MASK | CPUID_PARTNO_MASK);
    const struct cortex_m_part_info *pinfo = 0;
    for (uint n = 0; n < ARRAY_SIZE(cortex_m_parts); n++) {
      if (impl_part == cortex_m_parts[n].impl_part) {
        pinfo = &cortex_m_parts[n];
        break;
      }
    }
    if (!impl_part)
      error("unrecognized cpuid %08x implementor %x",cpuid,impl_part);

    int var = (cpuid & CPUID_VARIANT_MASK)  >> CPUID_VARIANT_SHIFT;
    int rev = (cpuid & CPUID_REVISION_MASK) >> CPUID_REVISION_SHIFT;

    logmsg("cmsis_examine CPUID %08x implementor %x %s variant %d rev %d",
           cpuid,impl_part,pinfo->name,var,rev);
     
    cp->fpv4 = pinfo->flags & CORTEX_M_F_HAS_FPV4;
    cp->fpv5 = pinfo->flags & CORTEX_M_F_HAS_FPV5;
    if ((pinfo->flags & CORTEX_M_F_HAS_FPV4) || 
        (pinfo->flags & CORTEX_M_F_HAS_FPV5)) 
    {
      uint mvfr0,mvfr1;
      res = cmsis_read_mem32(cp,MVFR0_ADD,1,&mvfr0,SET_TAR);
      res = cmsis_read_mem32(cp,MVFR1_ADD,1,&mvfr1,0);
      logmsg("mvfr0 %08x mvfr1 %08x",mvfr0,mvfr1);
    }

    CHKSEQ1(s + 16);
    res = cmsis_read_mem32(cp,DHCSR_ADD,1,&cp->dhcsr,SET_TAR);
    arm_dumpreg(DHCSR_ADD,cp->dhcsr,buf,sizeof buf,0);
    logmsg("cmsis_examine DHCSR %08x %s",cp->dhcsr,buf);
    CHKSEQ1(s + 18);
    if ((cp->dhcsr & DHCSR_C_DEBUGEN) == 0)
    {
      cp->dhcsr = DHCSR_DBGKEY | DHCSR_C_DEBUGEN;
      res = cmsis_write_mem32(cp,DHCSR_ADD,1,&cp->dhcsr,SET_TAR);
      s1 = 2;
    }
//  printf("*** dhcsr %08x\n",cp->dhcsr);
    demcr = DEMCR_TRCENA; // | prev val TODO
    res = cmsis_write_mem32(cp,DEMCR_ADD,1,&demcr,SET_TAR);
    res = cmsis_read_mem32(cp,FP_CTRL_ADD,1,&cp->bt.fp_ctrl,SET_TAR);
    uint en      = (cp->bt.fp_ctrl & FP_CTRL_EN_MASK)        >> FP_CTRL_EN_SHIFT;
    uint ncode1  = (cp->bt.fp_ctrl & FP_CTRL_NUM_CODE1_MASK) >> FP_CTRL_NUM_CODE1_SHIFT;
    uint ncode2  = (cp->bt.fp_ctrl & FP_CTRL_NUM_CODE2_MASK) >> FP_CTRL_NUM_CODE2_SHIFT;
    cp->fp_nlit  = (cp->bt.fp_ctrl & FP_CTRL_NUM_LIT_MASK)   >> FP_CTRL_NUM_LIT_SHIFT;
    cp->fp_ncomp = ncode1 | ncode2 << 4;
    if (cp->plu->log)
      logmsg("cmsis_examine: FP_CTRL %08x (en %d nc1 %d nl %d nc2 %d n_comp %d)",
             cp->bt.fp_ctrl,en,ncode1,cp->fp_nlit,ncode2,cp->fp_ncomp);
    CHKSEQ1(s + 22 + s1);
    for (int i = 0; i < cp->fp_ncomp + cp->fp_nlit; i++)
    {
      if (cp->plu->log)
        logmsg("remove break %d add %08x",i,FP_COMPn_ADD(i));
      res = cmsis_write_32(cp,FP_COMPn_ADD(i),0,i == 0 ? 0 + SET_TAR : 0);
    }
    if (cp->fp_ncomp > NBRK)
      error("n_comp > NBRK");
    for (int i = 0; i < cp->fp_ncomp; i++)
      cp->bt.brk[i].flag |= BRK_F_HW;

    CHKSEQ1(s + 30 + s1);
    cmsis_dwt_setup(cp);
    CHKSEQ1(s + 38 + s1);
  } else
  {
    res = cmsis_multi_select(cp);
    res = cmsis_mem_ap_init(cp,SET_SEL);
  }
  EXIT("cmsis_examine");

  return res;
}
