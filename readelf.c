/*
18 02 1010 TODO FIX strtab vs shstrtab
see https://stackoverflow.com/questions/11289843/string-table-in-elf

out:
W: duplicated strpool i 23 shstrndx 23

*/
#include "env.h"
#include "dwarf.h"

#include "dwarf_auto.c"
#include "readelf.h"

#define LINEH_DEF_OPC 12

struct fdata elfh[20];
struct fdata progh[20];
struct fdata sech[20];
struct fdata symtab[20];
struct fdata dline[20];

struct t_dabb *dwarf_abbrev(struct self *pe,char *buf,int size);
void freeabbr(struct t_dabb *pd);
void dwarf_info(struct self *pe,char *buf,int size) ;
void dwarf_line(struct self *pe,char *buf,int size) ;

int getint(char *p)
{
  int n;
  n = (((p[3] & MB) * 256 + (p[2] & MB)) * 256 +
        (p[1] & MB)) * 256 + (p[0] & MB);
  return n;
}

int getbyte(char *p)
{
  int n;
  n = (p[0] & MB);
  return n;
}

int getshort(char *p)
{
  int n;
  n = (p[1] & MB) * 256 + (p[0] & MB);
  return n & MW;
}

char *fmtshffl(int fl,char *o)
{
  strcpy(o,"----");
  if (fl & SHF_WRITE)
    o[0] = 'w'; 
  if (fl & SHF_ALLOC)  
    o[1] = 'a'; 
  if (fl & SHF_EXECINSTR)  
    o[2] = 'x'; 
  return o;
}

void loadf(struct self *pe,long fp,char *mem,int len,struct fdata *ps,char **add,char *str,int log)
{
  char fbuf[20];
  int v,l,i,idx,dim,off;
  short *psh;
  int   *pin;

// printf("loadf log %d\n",log);
  i = v = 0;
  while (ps->name)
  {
    dim = ps->dim;
    if (dim == 0) dim = 1;
    for (idx = 0; idx < dim; idx++)
    {
      off = ps->off + idx * ps->size;
      if (log)
      {
        l = strlen(ps->name);
        printf("%08lx %2x: %s",fp + off,ps->off + idx * ps->size,ps->name);
        if (dim > 1)
          printf("[%2d]",idx);
        else
          printf("....");
        for (l = 20 - l; l > 0; l--) printf(".");
        printf(": ");
      }
      if (ps->size == 1)
      {
        v = getbyte(mem + off); 
        if (log) printf("      %02x %12d",v,v);
      } else if (ps->size == 2)
      {
        v = getshort(mem + off); 
        if (log) printf("    %04x %12d",v,v);
      } else if (ps->size == 4)
      {
        v = getint(mem + off); 
        if (log) printf("%08x %12d",v,v);
      }
//    if (log && ps->flag & 1) printf(" [str %p v %d] ",str,v);
      if (add && add[i]) 
      {
        if (ps->size == 1)
          add[i][idx] = v;
        else if (ps->size == 2)
        {
          psh = (short *) add[i];
          psh[idx] = v;
        } else if (ps->size == 4)
        {
          pin = (int *) add[i];
          pin[idx] = v;
        } else
          error("bad length");
        if (add && !add[i]) add = 0;
      }
      if ((log && ps->flag & 1) && str)
      {
        if (v < pe->strpools)
          printf(" %s",str + v);
        else
          printf(" out of string space");
      }
      if (log && (ps->flag & 2))
        if (v >= 0 && v <= sizeof(ttype) / sizeof(ttype[0]))
          if (ttype[v].sym)
            printf(" %s",ttype[v].sym);
      if (log && (ps->flag & 4))
        printf(" %s",fmtshffl(v,fbuf));
      if (log) putchar('\n'); 
    }
    ps++;
    i++;
  }
}

void dumpf(struct self *pe,long fp,char *mem,int len,struct fdata *ps,char **add,char *str)
{
  loadf(pe,fp,mem,len,ps,add,str,1);
}

void initdline(int dim)
{
  int i,off;

  i = 0;
  off = 0;
  dline[i].name = "length";
  dline[i].off  = off;
  dline[i].size = 4;
  off += dline[i].size;
  i++;
  dline[i].name = "version";
  dline[i].off  = off;
  dline[i].size = 2;
  off += dline[i].size;
  i++;
  dline[i].name = "hlength";
  dline[i].off  = off;
  dline[i].size = 4;
  off += dline[i].size;
  i++;
  dline[i].name = "min_ins_l";
  dline[i].off  = off;
  dline[i].size = 1;
  off += dline[i].size;
  i++;
  dline[i].name = "def_is_stmt";
  dline[i].off  = off;
  dline[i].size = 1;
  off += dline[i].size;
  i++;
  dline[i].name = "line_base";
  dline[i].off  = off;
  dline[i].size = 1;
  off += dline[i].size;
  i++;
  dline[i].name = "line_range";
  dline[i].off  = off;
  dline[i].size = 1;
  off += dline[i].size;
  i++;
  dline[i].name = "opcode_base";
  dline[i].off  = off;
  dline[i].size = 1;
  off += dline[i].size;
  i++;
  dline[i].name = "std_opcode_l";
  dline[i].off  = off;
  dline[i].size = 1;
  dline[i].dim  = dim;
  off += dline[i].size * dline[i].dim;
  i++;
}

void initfdata()
{
/*
SHT_NULL 0 Marks an unused section header
SHT_PROGBITS 1 Contains information defined by the program
SHT_SYMTAB 2 Contains a linker symbol table
SHT_STRTAB 3 Contains a string table
SHT_RELA 4 Contains “Rela” type relocation entries 
SHT_HASH 5 Contains a symbol hash table 
SHT_DYNAMIC 6 Contains dynamic linking tables
SHT_NOTE 7 Contains note information
SHT_NOBITS 8 Contains uninitialized space; does not occupy any space in the file
SHT_REL 9 Contains “Rel” type relocation entries
SHT_SHLIB 10 Reserved
SHT_DYNSYM 11 Contains a dynamic loader symbol tabl
*/
  int i,off;

  i = 0;
  ttype[i].type = i;
  ttype[i].sym  = "SHT_NULL";
  i++;
  ttype[i].type = i;
  ttype[i].sym  = "SHT_PROGBITS";
  i++;
  ttype[i].type = i;
  ttype[i].sym  = "SHT_SYMTAB";
  i++;
  ttype[i].type = i;
  ttype[i].sym  = "SHT_STRTAB";
  i++;
  ttype[i].type = i;
  ttype[i].sym  = "SHT_RELA";
  i++;
  ttype[i].type = i;
  ttype[i].sym  = "SHT_HASH";
  i++;
  ttype[i].type = i;
  ttype[i].sym  = "SHT_DYNAMIC";
  i++;
  ttype[i].type = i;
  ttype[i].sym  = "SHT_NOTE";
  i++;
  ttype[i].type = i;
  ttype[i].sym  = "SHT_NOBIT";
  i++;
  ttype[i].type = i;
  ttype[i].sym  = "SHT_REL";
  i++;

  i = 0;
  off = 16;
  elfh[i].name = "type";
  elfh[i].off  = off;
  elfh[i].size = 2;
  off += elfh[i].size;
  i++;
  elfh[i].name = "machine";
  elfh[i].off  = off;
  elfh[i].size = 2;
  off += elfh[i].size;
  i++;
  elfh[i].name = "version";
  elfh[i].off  = off;
  elfh[i].size = 4;
  off += elfh[i].size;
  i++;
  elfh[i].name = "entry";
  elfh[i].off  = off;
  elfh[i].size = 4;
  off += elfh[i].size;
  i++;
  elfh[i].name = "phoff";
  elfh[i].off  = off;
  elfh[i].size = 4;
  off += elfh[i].size;
  i++;
  elfh[i].name = "shoff";
  elfh[i].off  = off;
  elfh[i].size = 4;
  off += elfh[i].size;
  i++;
  elfh[i].name = "flags";
  elfh[i].off  = off;
  elfh[i].size = 4;
  off += elfh[i].size;
  i++;
  elfh[i].name = "ehsize";
  elfh[i].off  = off;
  elfh[i].size = 2;
  off += elfh[i].size;
  i++;
  elfh[i].name = "phentsize";
  elfh[i].off  = off;
  elfh[i].size = 2;
  off += elfh[i].size;
  i++;
  elfh[i].name = "phnum";
  elfh[i].off  = off;
  elfh[i].size = 2;
  off += elfh[i].size;
  i++;
  elfh[i].name = "shentsize";
  elfh[i].off  = off;
  elfh[i].size = 2;
  off += elfh[i].size;
  i++;
  elfh[i].name = "shnum";
  elfh[i].off  = off;
  elfh[i].size = 2;
  off += elfh[i].size;
  i++;
  elfh[i].name = "shstrndx";
  elfh[i].off  = off;
  elfh[i].size = 2;
  off += elfh[i].size;
  i++;

  i = 0;
  off = 0;
  progh[i].name = "type";
  progh[i].off  = off;
  progh[i].size = 4;
  off += progh[i].size;
  i++;
  progh[i].name = "offset";
  progh[i].off  = off;
  progh[i].size = 4;
  off += progh[i].size;
  i++;
  progh[i].name = "vaddr";
  progh[i].off  = off;
  progh[i].size = 4;
  off += progh[i].size;
  i++;
  progh[i].name = "paddr";
  progh[i].off  = off;
  progh[i].size = 4;
  off += progh[i].size;
  i++;
  progh[i].name = "fileSize";
  progh[i].off  = off;
  progh[i].size = 4;
  off += progh[i].size;
  i++;
  progh[i].name = "memSize";
  progh[i].off  = off;
  progh[i].size = 4;
  off += progh[i].size;
  i++;
  progh[i].name = "flags";
  progh[i].off  = off;
  progh[i].size = 4;
  off += progh[i].size;
  i++;
  progh[i].name = "align";
  progh[i].off  = off;
  progh[i].size = 4;
  off += progh[i].size;
  i++;

  i = 0;
  off = 0;
  sech[i].name = "name";
  sech[i].off  = off;
  sech[i].size = 4;
  sech[i].flag = 1;
  off += sech[i].size;
  i++;
  sech[i].name = "type";
  sech[i].off  = off;
  sech[i].size = 4;
  sech[i].flag = 2;
  off += sech[i].size;
  i++;
  sech[i].name = "flags";
  sech[i].off  = off;
  sech[i].size = 4;
  sech[i].flag = 4;
  off += sech[i].size;
  i++;
  sech[i].name = "addr";
  sech[i].off  = off;
  sech[i].size = 4;
  off += sech[i].size;
  i++;
  sech[i].name = "offset";
  sech[i].off  = off;
  sech[i].size = 4;
  off += sech[i].size;
  i++;
  sech[i].name = "size";
  sech[i].off  = off;
  sech[i].size = 4;
  off += sech[i].size;
  i++;
  sech[i].name = "link";
  sech[i].off  = off;
  sech[i].size = 4;
  off += sech[i].size;
  i++;
  sech[i].name = "info";
  sech[i].off  = off;
  sech[i].size = 4;
  off += sech[i].size;
  i++;
  sech[i].name = "addralign";
  sech[i].off  = off;
  sech[i].size = 4;
  off += sech[i].size;
  i++;
  sech[i].name = "entSize";
  sech[i].off  = off;
  sech[i].size = 4;
  off += sech[i].size;
  i++;

  i = 0;
  off = 0;
  symtab[i].name = "st_name";
  symtab[i].off  = off;
  symtab[i].size = 4;
  symtab[i].flag = 1;
  off += symtab[i].size;
  i++;
  symtab[i].name = "st_addr";
  symtab[i].off  = off;
  symtab[i].size = 4;
  off += symtab[i].size;
  i++;
  symtab[i].name = "st_size";
  symtab[i].off  = off;
  symtab[i].size = 4;
  off += symtab[i].size;
  i++;
  symtab[i].name = "st_info";
  symtab[i].off  = off;
  symtab[i].size = 1;
  off += symtab[i].size;
  i++;
  symtab[i].name = "st_other";
  symtab[i].off  = off;
  symtab[i].size = 1;
  off += symtab[i].size;
  i++;
  symtab[i].name = "st_shndx";
  symtab[i].off  = off;
  symtab[i].size = 2;
  off += symtab[i].size;
  i++;
  initdline(LINEH_DEF_OPC);
}

struct eh
{
  int type;
  int machine;
  int version;
  int entry;
  int phoff;
  int shoff;
  int flags;
  short ehsize;
  short phentsize;
  short phnum;
  short shentsize;
  short shnum;
  short shstrndx;
};

struct ph
{
  int type;
  int offset;
  int vaddr;
  int paddr;
  int fileSize;
  int memSize;
  int flags;
  int align;
};

struct sh
{
  int name;
  int type;
  int flags;
  int addr;
  int offset;
  int size;
  int link;
  int info;
  int addralign;
  int entSize;
};

struct symt
{
  int name;
  int value;
  int size;
  char info;
  char other;
  short shndx;
};

struct lineh 
{
  int length;
  short version;
  int header_length;
  char min_instruction_length;
  char default_is_stmt;
  char line_base;
  char line_range;
  char opcode_base;
  char std_opcode_lengths[LINEH_DEF_OPC];
};

void dumpstr(char *str,int len)
{
  int n;
  char *p;

  p = str;
  n = 0;
  while (p - str < len)
  {
    n++;
    printf("%5d %6ld: %s\n",n,(long) (p-str),p);
    p += strlen(p) + 1;
  }
}

void readstring(struct self *pe,FILE *pf,struct eh *h)
{
  int i,s,s1,n;
  long fp;
  char mem[10*4],*addr[20]; 
  char fbuf[20];
  struct sh hs;

  n = 0;
  addr[n++] = (char *) &hs.name;
  addr[n++] = (char *) &hs.type;
  addr[n++] = (char *) &hs.flags;
  addr[n++] = (char *) &hs.addr;
  addr[n++] = (char *) &hs.offset;
  addr[n++] = (char *) &hs.size;
  addr[n++] = (char *) &hs.link;
  addr[n++] = (char *) &hs.info;
  addr[n++] = (char *) &hs.addralign;
  addr[n++] = (char *) &hs.entSize;
  s = ftell(pf);
  if (pe->log)
    printf("readstring current pos %d\n",s);
  n = fseek(pf,h->shoff,0);
  if (n != 0)
    error("fseek");
  n = sizeof mem;
  i = 0;
  while (n == sizeof mem && i < h->shnum)
  {
    int nn;

    fp = ftell(pf);
    n = fread(mem,1,sizeof mem,pf);
//  printf("read %d\n",n);
    if (n == sizeof mem)
    {
      i++;
//    printf("\n------------ sec header %d\n",i);
      loadf(pe,fp,mem,sizeof mem,sech,addr,pe->strpool,0);
      if (pe->log)
        printf("%2d name %6d type %08x flags %8x %s\n",
               i,hs.name,hs.type,hs.flags,fmtshffl(hs.flags,fbuf));
      if (hs.type == SHT_STRTAB)
      {
        s1 = ftell(pf);
        if (pe->log)
          printf("seek point %d\n",s1);
        nn = fseek(pf,hs.offset,0);
        if (nn)
          error("fseek");
        if (i != h->shstrndx + 1)
        {
          if (pe->strpools != 0)
          {
            warning("duplicated strpool i %d shstrndx %d",i,h->shstrndx + 1);
          }
          pe->strpools = hs.size;
          pe->strpool = myalloc(hs.size);
          if (pe->log)
            printf("strpool size %d %p\n",hs.size,pe->strpool);
          nn = fread(pe->strpool,1,hs.size,pf);
          if (nn != hs.size)
            error("fread 3");
#ifdef DSTR
          dumpstr(pe->strpool,hs.size);
#endif
        } else 
        {
          pe->strshpools = hs.size;
          pe->strshpool = myalloc(hs.size);
          if (pe->log)
            printf("strshpool size %d\n",hs.size);
          nn = fread(pe->strshpool,1,hs.size,pf);
          if (nn != hs.size)
            error("fread 4");
#ifdef DSTR
          dumpstr(pe->strshpool,hs.size);
#endif
        } 
        if (pe->log)
          printf("reset seek point to %d\n",s1);
        nn = fseek(pf,s1,0);
        if (nn)
          error("fseek");
      } 
    }
  }

  n = fseek(pf,s,0);
  if (n)
    error("fseek");
}

void dumpsymtab(struct self *pe,struct sh *ph,FILE *pf)
{
  int    i,s,n;
  long   fp;
  char   *addr[20];
  char   mem[16];
  struct symt st;

  n = 0;
  addr[n++] = (char *) &st.name;
  addr[n++] = (char *) &st.value;
  addr[n++] = (char *) &st.size;
  addr[n++] = (char *) &st.info;
  addr[n++] = (char *) &st.other;
  addr[n++] = (char *) &st.shndx;

  s = ftell(pf);
  if (pe->log)
    printf("dumpsymtab current pos %d\n",s);
  n = fseek(pf,ph->offset,0);
  if (n != 0)
    error("fseek");
  n = sizeof mem;
  i = 0;
  while (n == sizeof mem && i * sizeof(mem) < ph->size)
  {
    fp = ftell(pf);
    n = fread(mem,1,sizeof mem,pf);
//  erintf("fread %d\n",n);
    if (n == sizeof mem)
    {
      i++;
#ifdef DS
      printf("------ symbol %d\n",i);
      loadf(pe,fp,mem,sizeof mem,symtab,addr,pe->strpool,1);
      printf("info %x bind %d ty %d\n",st.info,st.info >> 4,st.info &0xf);
#else
      loadf(pe,fp,mem,sizeof mem,symtab,addr,pe->strpool,0);
#endif
      if (pe->fsym)
        pe->fsym(pe->arg,st.name + pe->strpool,st.value,st.size,st.info);
//    if ((st.info & 0xf) == STT_SECTION)
#ifdef DEBSYM
      if (strncmp(pe->strpool + st.name,".debug_",7) == 0)
        addsymany(st.value, st.info, pe->strpool + st.name);
#endif
    }
  }
  
  n = fseek(pf,s,0);
  if (n != 0)
    error("fseek");
}

int *sortsec(struct self *pe,FILE *pf,struct eh *h)
{
  int  n,i,j,k;
  char *addr[20],mem[10*4]; // ,*str;
  int  *off,*out,*ord;
  struct sh hs;

  n = 0;
  addr[n++] = (char *) &hs.name;
  addr[n++] = (char *) &hs.type;
  addr[n++] = (char *) &hs.flags;
  addr[n++] = (char *) &hs.addr;
  addr[n++] = (char *) &hs.offset;
  addr[n++] = (char *) &hs.size;
  addr[n++] = (char *) &hs.link;
  addr[n++] = (char *) &hs.info;
  addr[n++] = (char *) &hs.addralign;
  addr[n++] = (char *) &hs.entSize;

  off = mycalloc(sizeof(off[0]),h->shnum); 
  ord = mycalloc(sizeof(ord[0]),h->shnum); 
  out = mycalloc(sizeof(out[0]),h->shnum); 

//memset(ord,0,sizeof ord);

//printf("sortsec off %p ord %p out %p\n",off,ord,out);

  n = fseek(pf,h->shoff,0);
  if (pe->log)
    printf("sortsec off %d shnum %d\n",h->shoff,h->shnum);
  if (n != 0)
    error("fseek");
  n = sizeof mem;
  i = 0;
  if (pe->log)
  {
    printf("#   off              name             Type    Flags      add     off    size(d) size(x) ord\n");
    printf("-- ------ -------------------------- ------- -------- -------- -------- ------- ------- ---\n");
  }
  while (n == sizeof mem && i < h->shnum)
  {
    int n,s;
    s = ftell(pf);
    n = fread(mem,1,sizeof mem,pf);
    if (n == sizeof mem)
    {
      off[i] = s; 
      ord[i] = 0;
#ifdef DH
      printf("\noff %x ------------ sec header %d\n",s,i);
      loadf(pe,s,mem,sizeof mem,sech,addr,pe->strshpool,1);
#else
      loadf(pe,s,mem,sizeof mem,sech,addr,pe->strshpool,0);
#endif

      if (strncmp(pe->strshpool + hs.name,".debug_str",10) == 0)
      {
        ord[i] = 3;
      } else if (hs.type == SHT_SYMTAB)
        ord[i] = 2;
      else if (hs.type == SHT_PROGBITS && hs.flags)
      {
        ord[i] = 1;
      } else if (strncmp(pe->strshpool + hs.name,".debug_abbrev",13) == 0)
      {
        ord[i] = 3;
      } else if (strncmp(pe->strshpool + hs.name,".debug_line",11) == 0)
      {
        ord[i] = 2;
      } else if (strncmp(pe->strshpool + hs.name,".debug_info",11) == 0)
      {
        ord[i] = 4;
      }

      if (pe->log)
        printf("%2d %06x %-25s %8x %8x %8x %8x %8d %8x %d\n",
               i,s,
               pe->strshpool + hs.name, 
               hs.type,
               hs.flags,
               hs.addr,
               hs.offset,
               hs.size,hs.size,
               ord[i]);
      i++;
    } else
      error("read");
  }
#ifdef D
  for (i = 0; i < h->shnum; i++)
    printf("i %2d ord %2d off %06x\n",i,ord[i],off[i]);
#endif
  k = 0;
  for (j = 1; j < 10; j++)
    for (i = 0; i < h->shnum; i++)
      if (ord[i] == j)
        out[k++] = off[i];
//printf("sortsec off %p ord %p out %p\n",off,ord,out);
  if (off) 
    myfree(off);
#ifdef D
  for (j = 0; out[j]; j++)
    printf("out %2d %06x\n",j,out[j]);
#endif
  return out;
}

char *copysec(FILE *pf,int off,int len)
{
  int n,s;
  char *buf;
  
  buf = myalloc(len);
  s = ftell(pf);
  n = fseek(pf,off,0);
  if (n)
    error("fseek");
  n = fread(buf,1,len,pf);
  if (n != len)
    error("fread");
  n = fseek(pf,s,0);
  if (n)
    error("fseek");

  return buf;
}

void execsec(struct self *pe,FILE *pf,struct eh *h,int *off)
{
  int    n,i,s;
  char   *addr[20],mem[10*4]; 
  char   *buf;
  struct sh hs;

  n = 0;
  addr[n++] = (char *) &hs.name;
  addr[n++] = (char *) &hs.type;
  addr[n++] = (char *) &hs.flags;
  addr[n++] = (char *) &hs.addr;
  addr[n++] = (char *) &hs.offset;
  addr[n++] = (char *) &hs.size;
  addr[n++] = (char *) &hs.link;
  addr[n++] = (char *) &hs.info;
  addr[n++] = (char *) &hs.addralign;
  addr[n++] = (char *) &hs.entSize;

  for (i = 0; off[i]; i++)
  {
    n = fseek(pf,off[i],0);
    if (n != 0)
      error("fseek");
    n = fread(mem,1,sizeof mem,pf);
    if (n == sizeof mem)
    {
#ifdef DH
      printf("\noff %x ------------ sec header %d\n",s,i);
      loadf(pe,off[i],mem,sizeof mem,sech,addr,pe->strshpool,1);
#else
      loadf(pe,off[i],mem,sizeof mem,sech,addr,pe->strshpool,0);
#endif
      if (hs.type == SHT_SYMTAB)
        dumpsymtab(pe,&hs,pf);
      if (pe->pdeb && strncmp(pe->strshpool + hs.name,".debug_str",10) == 0)
      {
        pe->debug_str = copysec(pf,hs.offset,hs.size);
      } else if (hs.type == SHT_PROGBITS && hs.flags)
      {
        s = ftell(pf);
        n = fseek(pf,hs.offset,0);
        if (n)
          error("fseek");
        if (hs.size > 0 && (hs.flags & SHF_ALLOC) && pe->fprog)
        {
          info("execsec %-20s f %02x add %08x size %6d (0x%04x) offset %6x ",
               pe->strshpool + hs.name,hs.flags,
               hs.addr,hs.size,hs.size,hs.offset);
          n = pe->fprog(hs.addr,hs.size,pf,3,pe->arg);
          if (n != hs.size)
            warning("pff ret %d size %d",n,hs.size);
        }
        n = fseek(pf,s,0);
        if (n)
          error("fseek");
      } else if (pe->pdeb && strncmp(pe->strshpool + hs.name,".debug_",7) == 0)
      {
        buf = copysec(pf,hs.offset,hs.size);
        if (strncmp(pe->strshpool + hs.name,".debug_abbrev",13) == 0)
          pe->pdeb->abbr = dwarf_abbrev(pe,buf,hs.size);
        else if (strncmp(pe->strshpool + hs.name,".debug_info",11) == 0)
          dwarf_info(pe,buf,hs.size);
        else if (strncmp(pe->strshpool + hs.name,".debug_line",11) == 0)
          dwarf_line(pe,buf,hs.size);
        if (buf) 
          myfree(buf);
      }
    }
  }
}

void readprog(struct self *pe,FILE *pf,struct eh *h)
{
  int    n,m,s,i;
  long   fp;
  char   *addr[20],mem[8*4];
  struct ph hp;

//printf("readprog entry %08x\n",h->entry);
  pe->entry = h->entry;

//printf("readprog log %d\n",pe->log);
//printf("phoff %d\n",h->phoff);
  n = fseek(pf,h->phoff,0);
//printf("fseek %d\n",n);
  n = 0;
  addr[n++] = (char *) &hp.type;
  addr[n++] = (char *) &hp.offset;
  addr[n++] = (char *) &hp.vaddr;
  addr[n++] = (char *) &hp.paddr;
  addr[n++] = (char *) &hp.fileSize;
  addr[n++] = (char *) &hp.memSize;
  addr[n++] = (char *) &hp.flags;
  addr[n++] = (char *) &hp.align;
  n = sizeof mem;
  i = 0;
  while (n == sizeof mem && i < h->phnum)
  {
    fp = ftell(pf);
    n = fread(mem,1,sizeof mem,pf);
//  printf("read %d\n",n);
    if (n == sizeof mem)
    {
      int len;

      i++;
      if (pe->log)
        printf("prog header %d @%6lx\n",i,ftell(pf));
      loadf(pe,fp,mem,sizeof mem,progh,addr,0,pe->log);
      s = ftell(pf);
      if (pe->log)
        printf("prog %2d offset in elf %6x size %6d vaddr %5x\n",
               i,hp.offset,hp.fileSize,hp.vaddr);
      m = fseek(pf,hp.offset,0);
      if (m)
        error("fseek");
      if (pe->log)
        printf("read off %6x fileSize %6d\n",hp.offset,hp.fileSize);
      len = hp.memSize;
      if (hp.fileSize < len)
        len = hp.fileSize;
      if (len > 0)
      {
        if (pe->fprog)
        {
          info("readprog f %02x add %08x size %6d (0x%04x) offset %8x",
               hp.flags,
               hp.paddr,len,len,hp.offset);
          m = pe->fprog(hp.paddr,len,pf,1,pe->arg);
          if (m != len)
            error("fread 6");
        }
      }
      if (len < hp.memSize && pe->fprog)
      {
        FILE *nl;
        nl = fopen("/dev/zero","r");
        if (!nl) 
          error("can't open /dev/zero");
        info("readprog zeroing add %08x size %6d (0x%04x)",
             hp.paddr + len,hp.memSize - len,hp.memSize - len);
        m = pe->fprog(hp.paddr + len,hp.memSize - len,nl,2,pe->arg);
        if (m != hp.memSize - len)
          error("fread 6");
        fclose(nl);
      }
      m = fseek(pf,s,0);
      if (m)
        error("fseek");
    }
  }
}

int readelf(struct self *pe,FILE *pf)
{
  char *addr[20],mem[52];
  int  n,*off;
  long fp;
  struct eh h;

  n = 0;
  pe->pf = pf;
  init_all();
//printf("add %p n %d &h %p\n",addr,n,&h);
  addr[n++] = (char *) &h.type;
  addr[n++] = (char *) &h.machine;
  addr[n++] = (char *) &h.version;
  addr[n++] = (char *) &h.entry;
  addr[n++] = (char *) &h.phoff;
  addr[n++] = (char *) &h.shoff;
  addr[n++] = (char *) &h.flags;
  addr[n++] = (char *) &h.ehsize;
  addr[n++] = (char *) &h.phentsize;
  addr[n++] = (char *) &h.phnum;
  addr[n++] = (char *) &h.shentsize;
  addr[n++] = (char *) &h.shnum;
  addr[n++] = (char *) &h.shstrndx;
  addr[n] = 0;

  fp = ftell(pf);
  n = fread(mem,1,sizeof mem,pf);
//printf("read %d\n",n);
  if (n == sizeof mem)
  {
    if (mem[0] == 0x7f && mem[1] == 'E' &&
        mem[2] == 'L'  && mem[3] == 'F')
    {
      if (pe->log)
      {
        printf("magic OK\n");
        printf("encoding %d\n",mem[5]);
      }
      if (pe->log) 
        printf("elfh @%6lx\n",ftell(pf));
      loadf(pe,fp,mem,sizeof mem,elfh,addr,0,pe->log);
      if (pe->log) 
        printf("shstrndx %d\n",h.shstrndx);
      
      readstring(pe,pf,&h);
      readprog(pe,pf,&h);
      off = sortsec(pe,pf,&h);
      execsec(pe,pf,&h,off);
      if (off) 
        myfree(off);
    } else
    {
      if (pe->log)
        printf("wrong elf magic\n");
      return 0;
    }
  } else
  {
    if (pe->log)
      printf("error from fread");
    return 0;
  }
  deinit_all();
  if (pe->log)
    printf("strpool %p\n",pe->strpool);
  if (pe->strshpool)
    myfree(pe->strshpool);
  if (pe->strpool)
    myfree(pe->strpool);
  if (pe->debug_str)
    myfree(pe->debug_str);
  if (pe->pdeb)
  {
    int i;
    if (pe->pdeb->dir)
    {
      for (i = 0; i < pe->pdeb->ldir; i++)
        myfree(pe->pdeb->dir[i]);
      myfree(pe->pdeb->dir);
    }
    if (pe->pdeb->file)
      myfree(pe->pdeb->file);
    if (pe->pdeb->entry)
      myfree(pe->pdeb->entry);
    freeabbr(pe->pdeb->abbr);
  }
  return 1;
}

#ifdef _DOC_

see https://en.wikipedia.org/wiki/LEB128#Decode_unsigned_integer

Decode unsigned integer

result = 0;
shift = 0;
while (true) {
  byte = next byte in input;
  result |= (low-order 7 bits of byte) << shift;
  if (high-order bit of byte == 0)
    break;
  shift += 7;
}

Decode signed integer

result = 0;
shift = 0;

/* the size in bits of the result variable, e.g., 64 if result's type is int64_t */
size = number of bits in signed integer;

do {
  byte = next byte in input;
  result |= (low-order 7 bits of byte << shift);
  shift += 7;
} while (high-order bit of byte != 0);

/* sign bit of byte is second high-order bit (0x40) */
if ((shift <size) && (sign bit of byte is set))
  /* sign extend */
  result |= (~0 << shift);

#endif

// limit 2 char 
int ru128_2(char *p0,int *v)
{
  char *p;
  int n;

  p = p0;
  n = (*p++) & MB;
#ifdef DDWARF
  printf("%02x ",n);
#endif
  if (n & 0x80)
  {
#ifdef DDWARF
    printf("%02x ",*p & MB);
#endif
    n = (n & ~0x80) | (((*p++) & MB) << 7);
  }
  *v = n;
  return p - p0;
}

int ru128(char *p0,int *v)
{
  char *p;
  int  n,b,s;

  p = p0;
  n = s = 0;
  while(1)
  {
    b = (*p++) & MB;
    n |= (b & 0x7f) << s;  
    if ((b & 0x80) == 0)
      break;
    s += 7;
  } 
  *v = n;
  return p - p0;
}

int rs128(char *p0,int *v)
{
  char *p;
  int  n,b,s;

  p = p0;
  n = s = 0;
  do 
  {
    b = (*p++) & MB;
    n |= (b & 0x7f) << s;  
    s += 7;
  } while(b & 0x80);
  if (s < 32 && (b & 0x40))
    n |= (~0 << s);
  *v = n;
  return p - p0;
}

void initdeb(struct sdeb *pdeb)
{
  int l;
  char **p;
  struct sdebe *pe;

  p = 0;
  if (pdeb->ndir <= pdeb->ldir)
  {
    l = pdeb->ndir;
    if (l == 0) l = 10;
    else        l = l * 2;
    pdeb->ndir = l;
    p = myalloc((sizeof p) * l);
    if (pdeb->dir)
    {
      memcpy(p,pdeb->dir,(sizeof *p) * pdeb->ldir);
      myfree(pdeb->dir);
    }
    pdeb->dir = p;
  }
  if (pdeb->nfile <= pdeb->lfile)
  {
    l = pdeb->nfile;
    if (l == 0) l = 10;
    else        l = l * 2;
    pdeb->nfile = l;
    p = myalloc((sizeof p) * l);
    if (pdeb->file)
    {
      memcpy(p,pdeb->file,(sizeof *p) * pdeb->lfile);
      myfree(pdeb->file);
    }
    pdeb->file = p;
  }
  if (pdeb->nentry <= pdeb->lentry)
  {
    l = pdeb->nentry;
    if (l == 0) l = 10;
    else        l = l * 2;
    pdeb->nentry = l;
    pe = myalloc((sizeof *pe) * l);
    if (pdeb->entry)
    {
      if (!p) error("initdeb");
      memcpy(p,pdeb->entry,(sizeof *pe) * pdeb->lentry);
      myfree(pdeb->entry);
    }
    pdeb->entry = pe;
  }
}

void debaddfile(struct self *pe,char *fn)
{
}

void debadddir(struct self *pe,char *fn)
{
  int l;
  char *p;

  if (pe->pdeb)
  {
    printf("debadddir %d/%d %s\n",pe->pdeb->ldir,pe->pdeb->ndir,fn);
    if (pe->pdeb->ldir >= pe->pdeb->ndir)
      initdeb(pe->pdeb);
    l = strlen(fn);
    p = myalloc(l + 1);
    strcpy(p,fn);
    pe->pdeb->dir[pe->pdeb->ldir++] = p;    
  }
}

void dwarf_line(struct self *pe,char *buf,int size) 
{
  int n,pos,i,off,offo,opcode_idx;
  int opc,ty,l,o,par,lpar,done;
  char *addr[20],fn[1000],*p,*q;
  struct lineh lh;
// State machine
  int fileno,dirno,is_stm,line,address,col;

  address = 0;
  if (pe->log)
    printf("in dwarf_line size %x\n",size);
  n = 0;
  addr[n++] = (char *) &lh.length;
  addr[n++] = (char *) &lh.version;
  addr[n++] = (char *) &lh.header_length;
  addr[n++] = (char *) &lh.min_instruction_length;
  addr[n++] = (char *) &lh.default_is_stmt;
  addr[n++] = (char *) &lh.line_base;
  addr[n++] = (char *) &lh.line_range;
  addr[n++] = (char *) &lh.opcode_base;
  addr[n++] = (char *) &lh.std_opcode_lengths[0];

  pos = ftell(pe->pf);

  off = 0;
  offo = -1;
  opcode_idx = -1;
  for (i = 0; dline[i].name; i++)
    if (strcmp("std_opcode_l", dline[i].name) == 0)
       opcode_idx = i;
  if (opcode_idx == -1) error("cant't happen");
  offo = dline[opcode_idx].off + dline[opcode_idx].size * dline[opcode_idx].dim;
  
  while(off < size)
  { 
    int off0;

    off0 = off;
    loadf(pe,pos + off,buf + off,size,dline,addr,0,pe->log);
    if (lh.opcode_base - 1 != LINEH_DEF_OPC)
    { 
//    printf("opcode_base %d offo %d\n",lh.opcode_base,offo);
      initdline(lh.opcode_base - 1);
      offo = dline[opcode_idx].off + dline[opcode_idx].size * dline[opcode_idx].dim;
/*
      printf("opcode_idx %d off %d size %d dim %d\n",opcode_idx,
             dline[opcode_idx].off , dline[opcode_idx].size , dline[opcode_idx].dim);
*/
//    loadf(pe,pos + off,buf + off,size,dline,addr,0,pe->log);
//    printf("opcode_base %d offo %d\n",lh.opcode_base,offo);
//    dumpmem1("",buf + off,256,off);
    }
    off += offo;
    while (buf[off])
    {
      q = fn;
      for (p = buf + off; *p; p++)
        *q++ = *p;
      *q = 0;
      if (pe->log)
        printf("off %4x dir %s\n",off,fn);
      debadddir(pe,fn);
      off = p - buf + 1; 
    }
    off++;
    while (buf[off])
    {
      q = fn;
      for (p = buf + off; *p; p++)
        *q++ = *p;
      *q = 0;
      if (pe->log)
        printf("off %4x file %s dir %d\n",off,fn,p[1]);
      debaddfile(pe,fn);
      off = p - buf + 4; 
    }
    off++;
    
//  dumpmem1("",buf + off,256,off);
    done = 0;
    line = 1;
    col = 0;
//  discriminator = 0;
    fileno = 0; 
    dirno = 0;
    is_stm = lh.default_is_stmt;
//  printf("Start is_stm %d\n",is_stm);
    while(!done && off < off0 + lh.length + 4)
    {
      o = off;
      opc = buf[off++] & MB;
      ty = 0;
      l = 0;
      if (opc >= lh.opcode_base) 
      {
        ty = 2;
        opc -= lh.opcode_base;
      }
      if (opc == 0)
      {
        l = buf[off++] & MB;
        ty = 1;
        opc = buf[off] & MB;
      }
#ifdef DDWARF
      printf("%4x: l %d ty %d opc %d ",o,l,ty,opc);
#endif
      lpar = par = 0;
      if (ty == 0)
      {
        int op,v;
        v = 0;
        par = lh.std_opcode_lengths[opc - 1];
#ifdef DDWARF
        printf("par %d ",par);
        if (par > 0)
          printf(": ");
#endif
        if (par < 0)
          error("par < 0");
        op = off;
//      for (ii = 0; ii < par; ii++)
        if (par == 1)
        {
          if (opc == DW_LNS_fixed_advance_pc)
          {
            lpar += 2;
            v = ((buf[op + 1] & MB) << 8) + (buf[op] & MB); 
#ifdef DDWARF
            printf("%02x %02x %04x ",buf[op] & MB ,buf[op + 1] &MB,v);
#endif
          } else if (opc == DW_LNS_advance_line)
            lpar += rs128(buf + op,&v);
          else
            lpar += ru128(buf + op,&v);
#ifdef DDWARF
          printf("%d ",v);
#endif
        }

        if (opc == DW_LNS_copy)
        {
#ifdef DDWARF
          printf("Copy ");
#endif
          ;
        } else if (opc == DW_LNS_advance_pc)
        {
          address += v * lh.min_instruction_length;
#ifdef DDWARF
          printf("Advance PC by %d to %08x ",v * lh.min_instruction_length,address);
#endif
        } else if (opc == DW_LNS_advance_line)
        {
          line += v;
#ifdef DDWARF
          printf("Advance Line by %d to %d ",v,line);
#endif
        } else if (opc == DW_LNS_set_column)
        {
#ifdef DDWARF
          printf("Set Column to %d ",v);
#endif
          col = v;
        } else if (opc == DW_LNS_negate_stmt)
        {
#ifdef DDWARF
          printf("is_stm %d ",is_stm);
#endif
          is_stm = 1 - is_stm;
#ifdef DDWARF
          printf("Set is_stm %d ",is_stm);
#endif
        } else if (opc == DW_LNS_const_add_pc)
        {
          address += ((255  - lh.opcode_base)/ lh.line_range) * lh.min_instruction_length;
#ifdef DDWARF
          printf("Advance PC by const %d to %08x ",
                 ((255 - lh.opcode_base)/ lh.line_range) * lh.min_instruction_length,
                 address);
#endif
        } else if (opc == DW_LNS_fixed_advance_pc)
        {
          address += v;
          printf("Advance PC by fixed size amount %d to %08x",v,address);
        } else if (opc == DW_LNS_set_file)
        {
#ifdef DDWARF
          printf("Set file Name entry %d ",v);
#endif
          fileno = v;
        } else
          printf("??? ");
      } else if (ty == 1)
      {
        int *v;
        if (opc == 1)
        {
#ifdef DDWARF
          printf("End of sequence\n");
#endif
//        done = 1;
          line = 1;
          col = 0;
//        discriminator = 0;
          fileno = 0; 
          dirno = 0;
          is_stm = lh.default_is_stmt;
        } else if (opc == 2)
        {
          v = (int *) (buf + off + 1);
#ifdef DDWARF
          printf(" Set Address to %04x ",*v);
#endif
          address = *v;
        } else if (opc == 4)
        {
#ifdef DDWARF
          int discriminator;

          discriminator = buf[off + 1] & MB;
          printf("set discriminator %d ",discriminator); 
#endif
        } else
          printf("??? ");
      } else if (ty == 2)
      {
        address += (opc / lh.line_range) * lh.min_instruction_length;
        line += lh.line_base + opc % lh.line_range;
#ifdef DDWARF
        printf("Special opcode %d pc advance %d to %08x line %d to %d",
               opc,(opc / lh.line_range) * lh.min_instruction_length,address,
               lh.line_base + opc % lh.line_range,line);
#endif
      }
#ifdef DDWARF
      putchar('\n');
#endif
      if (pe->log)
      {
        if (done)
          printf("L;\n");
        else
          printf("L; %4x; %d; %08x; %d; %d; %6d; %3d\n",
                 o,is_stm,address,dirno,fileno,line,col);
      }
      off = off + l + lpar; 
      if (l < 0)
        error("l < 0");
    }
#ifdef DDWARF
    printf("off %x length %x\n",off , lh.length);
#endif
  }
}

void dwarf_info(struct self *pe,char *buf,int size) 
{
  int *len,*off,base,j,k,n,tn;
  int an,tag,at,form;
  int f,*ifa,fl,fadd,tagidx;
  short *sfa;
  char *ver;
  char *pf;
  char *pan; 

  pf = 0;
  fl = 0;
  f = 0;
  if (pe->log)
    printf("in dwarf_info sec .debug_info size %x\n",size);
  if (pe->pdeb)
    initdeb(pe->pdeb);
  for (tn = base = 0; base < size; tn++)
  {
//  dumpmem(buf + base,128,0);
    len = (int *) (buf + base);
    ver = buf + 4 + base;
    off = (int *) (buf + 6 + base);
//  ps = buf + 9 + base;
 
    fadd = base + 11;

/*
  Compilation Unit @ offset 0x3818:
   Length:        0x890 (32-bit)
   Version:       3
   Abbrev Offset: 0x0
   Pointer Size:  4
*/
    if (pe->log)
    {
      printf("  Compilation Unit @ offset 0x%x:\n",base);
      printf("   Length:        0x%x (32-bit)\n",*len);
      printf("   Version:       %d\n",*ver);
      printf("   Abbrev Offset: 0x%x\n",*off);
      printf("   Pointer Size:  4\n");
    }
    
    tagidx = -1;
    for (n = 0; n < pe->pdeb->abbr->ltag; n++)
    {
//    printf("%d tag %d tagoff %x\n",n,pe->pdeb->abbr->tag[n],pe->pdeb->abbr->tagoff[n]);
      if (pe->pdeb->abbr->tagoff[n] == *off)
        tagidx = n;
    }

//  printf("tagidx %d\n",tagidx);

    for (n = 0; fadd < base + *len; n++)
    {
      int l;

      pan = buf + fadd;
      l = ru128(pan,&an);
//    an = *pan;
/*  
      printf("base %4x len %x fadd %x ver %d off %x ptr size %d abb # %d\n",
             base,*len,fadd,*ver,*off,*ps,an);
*/
      if (pe->log)
        printf(" <0><%x>: Abbrev Number: %d ",fadd,an);
      fadd += l;
      if (an > 0)
      {
        j = pe->pdeb->abbr->index[an + pe->pdeb->abbr->tag[tagidx]];
#ifdef DDWARF
        printf("abbr %d tn %d tag %d off %x\n",an,tn,pe->pdeb->abbr->tag[tn],j);
#endif
        if (j + l < 0 || j + l > pe->pdeb->abbr->lmem)
          error("abbr->mem overflow");
        tag   = pe->pdeb->abbr->mem[j + l] & MB;
        k     = l + 1;
        if (tag > 127)
          tag =  (tag << 8) + pe->pdeb->abbr->mem[j + k++];
//      child = pe->pdeb->abbr->mem[j + k] & MB; 
        k++;
#ifdef DDWARF
        printf("%04x: n %d tag 0x%04x %6d %s\n",
               j,n,tag,tag,decode_dw_tag(tag));
#endif
        if (pe->log)
          printf("(%s)\n",decode_dw_tag(tag));
        if (tag > 127) k++;
//      printf("k %d\n",k);
        for (j = j + k; pe->pdeb->abbr->mem[j] ; j +=2)
        {
          at =   pe->pdeb->abbr->mem[j] & MB;
          if (at > 127)
          {
            j++;
            at = (at << 8) + (pe->pdeb->abbr->mem[j] & MB);
          }
          form = pe->pdeb->abbr->mem[j + 1] & MB;
#ifdef DDWARF
          printf("%04x: %3d 0x%04x %6d 0x%02x %-30s %-30s\n", 
                 j,at,at,form,form,decode_dw_at(at),
                 decode_dw_form(form));
#endif
          if (pe->log)
            printf("    <%x>   %-18s : ",fadd,decode_dw_at(at));
          if (form == DW_FORM_indirect) 
            fadd += ru128(buf + fadd,&form);
          if (form == DW_FORM_strp || form == DW_FORM_addr || 
              form == DW_FORM_data4 || form == DW_FORM_sec_offset ||
              form == DW_AT_type || form == DW_FORM_ref4 || 
              form == DW_FORM_ref_addr)
          {
            ifa = (int *) (buf + fadd);
            f = *ifa;
            fl = 4;
//          printf("l=4 ");
          } else if (form == DW_FORM_ref2)
          {
            sfa = (short *) (buf + fadd);
            f = *sfa;
            fl = 2;
//          printf("l=2 ");
          } else if (form == DW_FORM_flag)
          {
            f = buf[fadd] & MB;
            fl = 1;
          } else if (form == DW_FORM_string)
          {
            int ll;
            pf = buf + fadd;
            for (ll = 0; buf[fadd + ll]; )
              ll++;
//          printf("ll %d ",ll);
            fl = ll + 1;
//          if (fl > 4) fl = 4;
          } else if (form == DW_FORM_data2)
          {
            sfa = (short *) (buf + fadd);
            f = *sfa & MW;
            fl = 2;
//          printf("l=2 ");
          } else if (form == DW_FORM_data1 || 
                     form == DW_FORM_exprloc || 
                     form == DW_FORM_block1)
          {
            f = buf[fadd] & MB;
            fl = 1;
//          printf("l=1 ");
          } else if (form == DW_FORM_udata)
          { // uleb128
            fl = ru128(buf + fadd,&f);
          } else if (form == DW_FORM_ref_udata)
          { // uleb128
            fl = ru128(buf + fadd,&f);
          } else if (form == DW_FORM_block)
          {
            fl = ru128(buf + fadd,&f);
//          printf("block len %d ",f);
            fl += f;
          } else if (form == DW_FORM_sdata)
          { // sleb128
printf("\nHERE form %d fadd %x buf %x %x\n ",form,fadd,buf[fadd] & MB,buf[fadd + 1] & MB);
            fl = rs128(buf + fadd,&f);
printf("fl %d f %d\n",fl,f);
          } else if (form == DW_FORM_flag_present)
          {
            f = 1;
            fl = 0;
          } else
            printf("??? %x %s ",form,decode_dw_form(form));
          if (form == DW_FORM_exprloc || form == DW_FORM_block1)
          {
            int i;

            if (pe->log)
            {
              printf("%d byte block: ",f);
              for (i = 0; i < f; i++)
                printf("%x ",buf[fadd + 1 + i] & MB);
              printf("(%s)",decode_dw_op(buf[fadd + 1] & MB));
            }
          } else
          {
            if (pe->log) 
            {
              printf(" <0x%x> %d ",f,f);
              if (form == DW_FORM_string)
                printf("%s ",pf);
              if (form == DW_FORM_strp && pe->debug_str)
                printf("%s ",pe->debug_str + f);
             }
          }
          if (pe->log)
            putchar('\n');
          fadd += fl;
          if (form == DW_FORM_exprloc || form == DW_FORM_block1)
            fadd += f;
        }
      }
    }
    base += *len + 4; 
    if (pe->log)
      printf("next %x\n",base);
  }
}

void dwarf_str(struct self *pe,char *buf,int size) 
{
  printf("dwarf_str len %d\n",size);
  dumpmem((uchar *) buf,size,0);
}

void freeabbr(struct t_dabb *pd)
{
  if (!pd)
    return;
  if (pd->mem)
    myfree(pd->mem);
  if (pd->index)
    myfree(pd->index);
  if (pd->tag)
    myfree(pd->tag);
  if (pd->tagoff)
    myfree(pd->tagoff);
  myfree(pd);
}

struct t_dabb *dwarf_abbrev(struct self *pe,char *buf,int size) 
{
  int li,ti,i,j,n,tag,child,at,form,k,lf,lf1;
  struct t_dabb *pd;

  if (pe->log)
    printf("in dwarf_abbrev size %d\n",size);

  pd = myalloc(sizeof(struct t_dabb));
  pd->lmem = size;
  pd->mem = myalloc(size);
  memcpy(pd->mem,buf,size);
  pd->lindex = 5000;
  pd->index = myalloc(sizeof(pd->index[0]) * pd->lindex);
  pd->ltag = 50;
  pd->tag = myalloc(sizeof(pd->tag[0]) * pd->ltag);
  pd->tagoff = myalloc(sizeof(pd->tag[0]) * pd->ltag);
  memset(pd->tagoff,-1,sizeof(pd->tag[0]) * pd->ltag);
  li = ti = 0; 
  j = 0;

  if (pe->log)
    printf("  Number TAG (0x%x) size %x\n",j,size);
//printf("put tag[%d] 0x%x\n",ti,j);
  pd->tag[ti] = j;
  pd->tagoff[ti] = 0;
  ti++;
  for (i = 0; j < size - 4; i++)
  {
//   printf("--- j %d size %d\n",j,size);
#ifdef DDWARF
    dumpmem(buf + j,16,(char *) j);
#endif
    n     = buf[j] & MB;
    if (n == 0)
    {
      while (buf[j + 1] == 0) j++;
      j += ru128(buf + j,&n);
      
      if (pe->log)
        printf("  Number TAG (at 0x%x) n %d\n",j,n);
      if (ti >= pd->ltag)
        error("tag overflow");
//    printf("put tag[%d] %d 0x%x index[%d] 0x%x\n",ti,li,j,li,pd->index[li]);
      pd->tag[ti] = li;
      pd->tagoff[ti] = j;
      ti++;
    }
    lf = ru128_2(buf + j,&n);
    lf1 = ru128_2(buf + j + lf,&tag);
/*
    tag   = buf[j + 1] & MB;
    k     = 2;
    if (tag > 127)
      tag =  (tag << 8) + buf[j + k++];
*/
    k = lf + lf1;
    child = buf[j + k++] & MB; 
    if (li >= pd->lindex)
    {
      printf("li %d lindex %d ",li,pd->lindex);
      error("index overflow");
    }
//  printf("put index[%d] %d 0x%x\n",li + 1,j,j);
    pd->index[++li] = j;
#ifdef DDWARF
    printf("%04x: n %d tag 0x%04x %6d %s child %d li %d j %d\n",
           j,n,tag,tag,decode_dw_tag(tag),child,li - 1,j);
#endif
    if (pe->log)
    {
      printf("%4d      %s    ",n,decode_dw_tag(tag));
      if (child)
        printf("[has children]\n");
      else
        printf("[no children]\n");
    }
    if (tag > 127) k++;
//  printf("k %d\n",k);
    for (j = j + k; buf[j]; j +=2)
    {
      at =   buf[j] & MB;
      if (at > 127)
      {
        j++;
        at = (at << 8) + (buf[j] & MB);
      }
      form = buf[j + 1] & MB;
#ifdef DDWARF
      printf("%04x: %3d 0x%04x %6d 0x%02x %-30s %-30s\n", 
             j,at,at,form,form,decode_dw_at(at),
             decode_dw_form(form));
#endif
      if (pe->log)
        printf("    %-18s %s\n",decode_dw_at(at),decode_dw_form(form));
    }
    if (pe->log)
      printf("    DW_AT value: 0     DW_FORM value: 0\n");
    j += 2;
  }
  
  return pd;
}

#ifdef TEST
#define MEMS (128*KILO)

int loglev;
int mysym(void *unused,char *name,int val,int size,int info)
{
  if (loglev & 2)
    printf("symbol name %s val %08x size %d bind %d type %d\n",
           name,val,size,info >> 4,info & 0xf);
  return 0;
}

int main(int na, char **v)
{
  char   fn[300];
  FILE   *pf;
  struct self elf;
  struct sdeb deb;
  int    i;

  loglev = 0;
  memset(&elf,0,sizeof elf);
  memset(&deb,0,sizeof deb);
  initfdata();
//checkend();
  fn[0] = 0;
  for (i = 1; i < na; i++)
  {
    if (strcmp(v[i],"-log") == 0 && i < na - 1)
    {
      loglev = atoi(v[i + 1]);
      i++;
    } else
      strcpy(fn,v[i]);
  }
  if (fn[0])
  {
/*
    ram = myalloc(MEMS);
    memset(ram,0,MEMS);
*/
    printf("reading %s loglev %d\n",fn,loglev);
    pf = fopen(fn,"r");
    if (pf)
    {
      elf.fprog = 0;
      elf.fsym  = mysym;
      elf.log   = loglev;
//      elf.pdeb  = &deb;
      readelf(&elf,pf);
      fclose(pf);
    }
  } else
    printf("usage readhex <file>\n");
  return 0;
}
#endif
