// message format 
// @see https://arm-software.github.io/CMSIS_5/DAP/html/group__DAP__TransferBlock.html and similar 
//
#include "env.h"
#include "libusb.h"
#include "armreg.h"
#include "dapreg.h"
#include "readpcapng-stlink.h"
#include "driver-cmsis.h"
#include "dap-decode.h"

#define OUTF(fn,off,mask) \
{if (fl || ((v >> (off)) & (mask))) {sprintf(p,"%s %d ",#fn, (v >> (off)) & (mask)); p += strlen(p); }};

#define OUTF1(fn,off,mask,o) \
{if ((o == RW || o == op) && (fl || ((v >> (off)) & (mask)))) {sprintf(p,"%s %d ",#fn, (v >> (off)) & (mask)); p += strlen(p); }};

#define OUTF2(fn,off,mask,o,fmt) \
{if ((o == RW || o == op) && (fl || ((v >> (off)) & (mask)))) {sprintf(p,"%s " #fmt " ",#fn, (v >> (off)) & (mask)); p += strlen(p); }};

static char *dapinfo[256];

static struct t_dap_dec dap_dec[256];

static char *fmttreq(int v)
{
  static char buf[20];

  sprintf(buf,"a %x ",v & 0xc);
  if (v & 1)
    strcat(buf,"AP ");
  else
    strcat(buf,"DP ");
  if (v & 2)
    strcat(buf,"R ");
  else
    strcat(buf,"W ");
  if ((v >> 4) & 1)
    strcat(buf,"R V M ");
  if ((v >> 5) & 1)
    strcat(buf,"W V M ");
  if ((v >> 7) & 1)
    strcat(buf,"T ");

  return buf;
}

static void s_info(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  printf("ID %02x %s ",pd[1],dapinfo[pd[1]] ? dapinfo[pd[1]] : "???");
}

static void s_connect(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  printf("port type %d ",pd[1]);
  if (pd[1] == 0)
    printf("default mode ");
  else if (pd[1] == 1)
    printf("SWD mode ");
  else if (pd[2] == 2)
    printf("JTAG mode ");
  else
    printf("???"); 
}

static void s_swd_conf(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  printf("config %02x ",pd[1]);
}

static void s_t_conf(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  printf("idle %d wait retry %d match retry %d ",pd[1],get16(pd + 2),get16(pd + 4));
}

static void s_tran(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  printf("idx %02x count %d len %d\n",pd[1],pd[2],len);
  int j = 3;
  for (int i = 0; i < pd[2] && j < len; i++)
  {
    if (i > 0) putchar('\n');
    int req = pd[j];
    int w = ((req >> 1) & 1) == 0;
    int id;
    int ap = req & 1;
    int a  = req & 0xc;
    int r  = (req >> 1) & 1;
    printf("%s %2d[%2d] req %02x (%s) %-12s ",
           pdap->s_prefix,
           i,j,req,fmttreq(req),dap_regname(pdap,ap,r,a,&id,DAP_SEND));
    struct t_dap_reg *pr = getdapregbyid(pdap->regs,id);
    j++;
    if (w)
    {
      uint v = get32(pd + j);
      printf("=%08x ",v);
      if (pr)
        dap_setreg(pdap,id,v,OUT_MIN | OUT_NOCR,DAP_WO,DAP_SEND | DAP_DECODE,pdap->pmdap->counts + pdap->pmdap->countr);
      j += 4;
    } else
      dap_setreg(pdap,id,0,OUT_MIN | OUT_NOCR,DAP_RO,DAP_SEND | DAP_DECODE,pdap->pmdap->counts + pdap->pmdap->countr);
  }
}

static void s_tran_b(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  int req = pd[4];
  int w  = ((req >> 1) & 1) == 0;
  int ap = req & 1;
  int a  = req & 0xc;
  int r  = (req >> 1) & 1;
  int k  = 5;
  ushort count = get16(pd + 2);

  printf("idx %02x count %d\n",pd[1],count);
  int id  = -1;
  char *rn = dap_regname(pdap,ap,r,a,&id,DAP_RECEIVE);
  struct t_dap_reg *pr = getdapregbyid(pdap->regs,id);
  printf("%s req %02x (%s) %-12s ",
         pdap->s_prefix,req,fmttreq(req),
         rn);
  for (int i = 0; i < count; i++)
  {
    putchar('\n');
    if (w)
    {
      uint v = get32(pd + k);
      printf("%s[%2d %02x=%02x] =%08x ",pdap->s_prefix,i,k,pd[k],v);
      if (pr)
        dap_setreg(pdap,id,v,OUT_MIN | OUT_NOCR,DAP_WO,DAP_SEND | DAP_DECODE,pdap->pmdap->counts + pdap->pmdap->countr);
      k += 4;
    } else
      dap_setreg(pdap,id,0,OUT_MIN | OUT_NOCR,DAP_RO,DAP_SEND | DAP_DECODE,pdap->pmdap->counts + pdap->pmdap->countr);
  }
}

static void s_clock(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  printf("%d ",get32(pd + 1)); 
}

static void s_swj_seq(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  printf("bit count %d data \"",pd[1]);
  for (int i = 0; i < (pd[1] + 7) / 8; i++)
    printf("%02x ",pd[2 + i]);
  printf("\"");
}

static void s_swd_seq(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  putchar('\n');
  
  printf("%s sequence count %d ",pdap->s_prefix,pd[1]);
  int i,j;
  int s = 2;
  pdap->pmdap->phase = 0;
  for (i = 0; i < pd[1]; i++)
  {
    int n = pd[s] & 0x3f;
    if (n == 0) n = 64;
    int r = (pd[s] >> 7) & 1;
    printf("%d:(%d%c",i,n,"OI"[r]);
    s++;
    if (!r) 
    {
      if (pdap->pmdap->phase == 0 && i == 0 && (n + 7) / 8 == 1 && pd[s] == 0x99)
        pdap->pmdap->phase = 1;
      else if (pdap->pmdap->phase == 1 && i == 2 && n == 33)
      {
        pdap->pmdap->phase = 2;
        pdap->pmdap->targetsel = get32(pd + s);
      }
      printf("[");
      for (j = 0; j < (n + 7) / 8;j++)
        printf("%02x ",pd[s + j]);
      printf("]");
      s += j;
    }
    printf(") ");
    if (pdap->pmdap->phase == 2)
      printf("targetsel %08x ",pdap->pmdap->targetsel);
  }
}

static void r_info(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  int l = pd[1];
  printf("len %d val ",l);
  for (int i = 0; i < l && i < len - 2; i++)
    printf("%02x ",pd[2 + i]);
  printf(" \"");
  for (int i = 0; i < l && i < len - 2; i++)
  { 
    int ch = pd[2 + i];
    if (ch < ' ' || ch >= 127) ch = '.';
    printf("%c",ch);
  }
  printf(" \"");
}

static void r_connect(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  printf("status %d ",pd[1]);
  if (pd[1] == 0)
    printf("ERROR ");
  else if (pd[1] == 1)
    printf("SWD ");
  else if (pd[1] == 2)
    printf("DAP ");
}

static uchar *getmesptr(struct t_dap *pdap,int *l)
{
  int idx = IDX(pdap->ridx);
  if (l) *l = pdap->mesbuf[idx].len;
  return pdap->mesbuf[idx].pd;
}

struct t_dap *dap_switch(struct t_dap *pdap,uint ts)
{
  struct t_multidap *pmdap = pdap->pmdap;
  int found = -1;
  for (int i = 0; i < pmdap->ndap; i++)
  {
    if (pmdap->daps[i].targetsel == ts)
    {
      found = i;
      break;
    }
  }
  if (found == -1)
  {
    for (int i = 0; i < pmdap->ndap; i++)
    {
      if (pmdap->daps[i].targetsel == 0)
      {
        found = i;
        break;
      }
    }
  }
  if (found >= 0)
  {
    pdap = pmdap->daps + found;
    pmdap->c_idx = found;
    pdap->targetsel = ts;
  } else
    error("dap table full can't insert new target %08x @%d",ts,__LINE__);
  return pdap;
}

static void r_swd_seq(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  int i,j;
  int s = 2;
  int p = 2;
  int slen;
  uchar *sbuf = getmesptr(pdap,&slen);
  for (i = 0; i < sbuf[1]; i++)
  {
    int n = sbuf[s] & 0x3f;
    if (n == 0) n = 64;
    int r = (sbuf[s] >> 7) & 1;
    printf("%d-%c ",n,"OI"[r]);
    s++;
    if (r)
    {
      if (pdap->pmdap->phase == 2 && i == 1 && (n + 7) / 8 == 1 && pd[p] == 0x1f)
      {
        printf("ACK ");
        pdap = dap_switch(pdap,pdap->pmdap->targetsel);
      }
      printf("[");
      char *sep = "";
      for (j = 0; j < (n + 7) / 8;j++)
      {
        printf("%s%02x",sep,pd[p++]);
        sep = " ";
      }
      printf("] ");
    } else
    {
      printf("[");
      char *sep = "";
      for (j = 0; j < (n + 7) / 8; j++)
      {
        printf("%s%02x",sep,sbuf[s + j]);
        sep = " ";
      }
      printf("] ");
      s += j;
    }
  }
}

static void r_status(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  printf("status %d ",pd[1]);
  if (pd[1] == 0)
    printf("OK ");
  else
    printf("ERR ???");
}

static void r_tran_b(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  int slen;
  int sk = 5; // ptr in sent buf
  int k = 4; // response in pd

  uchar  *sbuf = getmesptr(pdap,&slen);
  ushort count = get16(pd + 1);
  printf("count %d resp ",count);
  if (pd[3] == DAP_TRANSFER_OK)
  {
    printf("OK\n");
    int req = sbuf[4];
    int w   = ((req >> 1) & 1) == 0;
    int ap  = req & 1;
    int a   = req & 0xc;
    int r   = (req >> 1) & 1;
    int id  = -1;
    char *rn = dap_regname(pdap,ap,r,a,&id,DAP_RECEIVE);
    struct t_dap_reg *pr = getdapregbyid(pdap->regs,id);
    printf("%s req %02x (%s) %-12s ",
           pdap->r_prefix,req,fmttreq(req),
           rn);
    for (int i = 0; i < count; i++)
    {
      putchar('\n');
      if (w)
      {
        uint v = get32(sbuf + sk);
        printf("%s[%2d %02x=%02x] =%08x ",pdap->r_prefix,i,sk,sbuf[sk],v);
        sk += 4;
      } else
      {
        printf("%s[%2d %02x=%02x] ",pdap->r_prefix,i,k,pd[k]);
        int v = get32(pd + k);
        if (pr) 
          dap_setreg(pdap,id,v,OUT_MIN | OUT_NOCR,DAP_RO,DAP_RECEIVE | DAP_DECODE,pdap->pmdap->counts + pdap->pmdap->countr);
        printf("=%08x ",v);
        k += 4;
      }
    }
  } else if (pd[3] == DAP_TRANSFER_NO_TARGET)
    printf("ERROR NO TARGET");
  else
    printf("%x ? ",pd[3]);
}

static void r_tran(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer)
{
  int j = 3; // query in pdap->pd
  int k = 3; // response in pd

  int slen;
  uchar *sbuf = getmesptr(pdap,&slen);
  printf("count %d response %02x query was %d\n",pd[1],pd[2],sbuf[2]);
  for (int i = 0; i < sbuf[2] && j < slen; i++)
  {
    int req = sbuf[j];
    int ap  = req & 1;
    int a   = req & 0xc;
    int w   = ((req >> 1) & 1) == 0;
    int t   = ((req >> 7) & 1); // timestamp
    int r   = (req >> 1) & 1;
    if (i > 0) 
      putchar('\n');
    int id = -1;
    printf("%s %2d[%2d] req %02x (%s) %-12s ",
           pdap->r_prefix,
           i,j,req,fmttreq(req),
           dap_regname(pdap,ap,r,a,&id,DAP_RECEIVE));
    struct t_dap_reg *pr = getdapregbyid(pdap->regs,id);
    j++;
    if (w) 
    {
      uint v = get32(sbuf + j);

      if (pr) 
        dap_setreg(pdap,id,v,OUT_MIN | OUT_NOCR,DAP_WO,DAP_RECEIVE | DAP_DECODE,pdap->counts + pdap->countr);
      printf("=%08x ",v);
      if (id == DAP_MEMAP_TAR)
      {
        printf("%s ",arm_getregname(v,0));
      }
      if (id == DAP_DP_SELECT)
      {
        int select = v;

        int apsel     = (select & DAP_DP_SELECT_APSEL_MASK)     >> DAP_DP_SELECT_APSEL_SHIFT;
        int apbanksel = (select & DAP_DP_SELECT_APBANKSEL_MASK) >> DAP_DP_SELECT_APBANKSEL_SHIFT;
        int dpbanksel = (select & DAP_DP_SELECT_DPBANKSEL_MASK) >> DAP_DP_SELECT_DPBANKSEL_SHIFT;
        printf("[sel %x %d %x] ",apsel,apbanksel,dpbanksel);
      } 
      j += 4;
    } else // read
    {
      int ts = 0;
      if (t)
      {
        ts = get32(pd + k);
        k += 4;
      } 
      int v = get32(pd + k);
      if (pr) 
        dap_setreg(pdap,id,v,OUT_MIN | OUT_NOCR,DAP_RO,DAP_RECEIVE | DAP_DECODE,pdap->counts + pdap->countr);
      printf("%08x ",v);
      if (t)
      {
        printf("T %8d ",ts);
        k += 4;
      } 
      k += 4;
    }
  }
}

#define DD(id,n,fs,fr) \
dap_dec[id].name = n; \
dap_dec[id].fdsend = fs; \
dap_dec[id].fdrec = fr;

struct t_multidap *new_multidap(int ndap)
{
  struct t_multidap *pmdap;
  struct t_dap *pdap;

  pmdap = calloc(sizeof(*pmdap) + sizeof(*pdap) * ndap,1);
  pmdap->ndap = ndap;
  for (int i = 0; i < ndap; i++)
  {
    pdap = pmdap->daps + i;
    pdap->idx = i;
    pdap->pmdap = pmdap;
    pdap->s_prefix = "";
    pdap->r_prefix = "";
    initdapreg(pdap->regs);
  }
  pdap = pmdap->daps + pmdap->c_idx;

  DD(ID_DAP_INFO,                "Info"              ,s_info       ,r_info);
  DD(ID_DAP_LED,                 "HostStatus"        ,0            ,0);
  DD(ID_DAP_CONNECT,             "Connect"           ,s_connect    ,r_connect);
  DD(ID_DAP_DISCONNECT,          "Disconnect"        ,0            ,r_status);
  DD(ID_DAP_TRANSFER_CONFIGURE,  "TransferConfigure" ,s_t_conf     ,r_status);
  DD(ID_DAP_TRANSFER,            "Transfer"          ,s_tran       ,r_tran);
  DD(ID_DAP_TRANSFER_BLOCK,      "TransferBlock"     ,s_tran_b     ,r_tran_b);
  DD(ID_DAP_TRANSFER_ABORT,      "TransferAbort"     ,0            ,0);
  DD(ID_DAP_WRITE_ABORT,         "WriteAbort"        ,0            ,0);
  DD(ID_DAP_DELAY,               "Delay"             ,0            ,0);
  DD(ID_DAP_RESET_TARGET,        "ResetTarget"       ,0            ,0);
  DD(ID_DAP_SWJ_PINS,            "SWJ_Pins"          ,0            ,0);
  DD(ID_DAP_SWJ_CLOCK,           "SWJ_Clock"         ,s_clock      ,r_status);
  DD(ID_DAP_SWJ_SEQUENCE,        "SWJ_Sequence"      ,s_swj_seq    ,r_status);
  DD(ID_DAP_SWD_CONFIGURE,       "SWD_Configure"     ,s_swd_conf   ,r_status);
  DD(ID_DAP_SWD_SEQUENCE,        "SWD_Sequence"      ,s_swd_seq    ,r_swd_seq);
  DD(ID_DAP_JTAG_SEQUENCE,       "JTAG_Sequence"     ,0            ,0);
  DD(ID_DAP_JTAG_CONFIGURE,      "JTAG_Configure"    ,0            ,0);
  DD(ID_DAP_JTAG_IDCODE,         "JTAG_IdCode"       ,0            ,0);

#undef DD

  dapinfo[DAP_INFO_VENDOR]        = "vendor name";
  dapinfo[DAP_INFO_PRODUCT]       = "product name";
  dapinfo[DAP_INFO_SER_NUM]       = "serial number";
  dapinfo[DAP_INFO_FW_VER]        = "proto version";
  dapinfo[DAP_INFO_DEVICE_VENDOR] = "target device name";
  dapinfo[DAP_INFO_DEVICE_NAME]   = "target device name";
  dapinfo[7]                      = "target board vendor";
  dapinfo[8]                      = "target board name";
  dapinfo[9]                      = "product firmware version";
  dapinfo[DAP_INFO_CAPABILITIES]  = "capabilities";
  dapinfo[0xf1]                   = "test domain server";
  dapinfo[0xfb]                   = "UART receive buffer size";
  dapinfo[0xfc]                   = "UART transmit buffer size";
  dapinfo[0xfd]                   = "SWO trace buffer size";
  dapinfo[DAP_INFO_PACKET_COUNT]  = "max packet count";
  dapinfo[DAP_INFO_PACKET_SIZE]   = "max packet size";

  return pmdap;
}

void initdap(struct t_decode *pdec,int ndap)
{
  struct t_multidap *pmdap;

  pmdap = new_multidap(ndap);
  pdec->priv = pmdap;
  return;
}

void enddap(struct t_decode *pdec)
{
  struct t_multidap *pmdap;
  pmdap = (struct t_multidap *) pdec->priv;

  for (int i = 0; i < pmdap->ndap; i++)
    dump_dapreg(pmdap->daps + i,OUT_MIN);

  free(pmdap);
}

void decodedap(uchar *pd,int len,struct t_decode *pdec,int transfer)
{ 
  struct t_multidap *pmdap;
  struct t_dap *pdap;

  if (len == 0) return;
  if (transfer != 1) return;
  pmdap = (struct t_multidap *) pdec->priv;
  pdap = pmdap->daps + pmdap->c_idx;
  pdap->counts++;
  pdap->pmdap->counts++;
  dap_set_s_mes(pdec,pd,len,pdap->pmdap->counts);
  dap_dec[*pd].count++;
  printf("%s %d+%d=%d T(%d+%d=%d) dap idx %d [%02x:%s] ",
         pdap->s_prefix,
         pdap->counts,pdap->countr,pdap->counts + pdap->countr,
         pdap->pmdap->counts,pdap->pmdap->countr,pdap->pmdap->counts + pdap->pmdap->countr,
         pdap->idx,
         *pd,dap_dec[*pd].name ? dap_dec[*pd].name : "???");
  if (dap_dec[*pd].fdsend)
    dap_dec[*pd].fdsend(pdap,pd,len,pdec,transfer);
  putchar('\n');
}

void decoderepdap(uchar *pd,int len,struct t_decode *pdec,int transfer,int nomod)
{
  struct t_multidap *pmdap;
  struct t_dap *pdap;
  pmdap = (struct t_multidap *) pdec->priv;
  pdap = pmdap->daps + pmdap->c_idx;

  if (pdap->ridx >= pdap->widx)
    error("decoderepdap: ring buffer underrun ridx %d widx %d",pdap->ridx,pdap->widx);
  if (len == 0) return;
  if (transfer != 1) return;
  uint regs0[DAP_NREG];
  for (int i = 0; i < DAP_NREG; i++)
    regs0[i] = pdap->regs[i].vals;
  dap_restore(pdap,pdap->ridx);
  if (!nomod)
  {
    pdap->countr++;
    pdap->pmdap->countr++;
  }
  printf("%s %d+%d=%d T(%d+%d=%d) %02x %s ",
         pdap->r_prefix,
         pdap->counts,pdap->countr,pdap->counts + pdap->countr,
         pdap->pmdap->counts,pdap->pmdap->countr,pdap->pmdap->counts + pdap->pmdap->countr,
         *pd,dap_dec[*pd].name ? dap_dec[*pd].name : "???");
  if (dap_dec[*pd].fdrec)
    dap_dec[*pd].fdrec(pdap,pd,len,pdec,transfer);
  putchar('\n');
  if (pdap->counts != pdap->countr)
  {
    printf("decoderepdap %3d+%3d=%3d send message id %d\n",
           pdap->counts,pdap->countr,pdap->counts + pdap->countr,
           pdap->mesbuf[IDX(pdap->ridx)].s_mes_id);
    printf("decoderepdap saved tar %08x\n",
           pdap->mesbuf[IDX(pdap->ridx)].regs0[DAP_MEMAP_TAR]);
    char pre[40];
    sprintf(pre,"SB %d",pdap->mesbuf[IDX(pdap->ridx)].s_mes_id);
    memdump(pdap->mesbuf[IDX(pdap->ridx)].pd,64,pre,0);
  }
  for (int i = 0; i < DAP_NREG; i++)
    pdap->regs[i].vals = regs0[i];
  if (!nomod)
    pdap->ridx++;
}

void dap_dump(void)
{
  printf("dap packet count stat\n");
  printf("dec hex count  name\n");
  printf("--- --  ------ ----------\n");
  for (int i = 0; i < 256; i++)
    if (dap_dec[i].count)
      printf("%3d %02x: %6d %s\n",i,i,dap_dec[i].count,dap_dec[i].name);
}

void dap_set_s_mes(struct t_decode *pdec,uchar *pd,int len,uint id)
{
  struct t_multidap *pmdap;
  struct t_dap *pdap;
  pmdap = (struct t_multidap *) pdec->priv;
  pdap = pmdap->daps + pmdap->c_idx;
  struct t_dap_buf *pb;

  if (pdap->widx - pdap->ridx >= NMESBUF)
    error("dap_set_s_mes: ring buffer overflow widx %d - ridx %d >= %d",
          pdap->widx,pdap->ridx,NMESBUF); 
  int idx = IDX(pdap->widx);
  dap_save(pdap,idx);
  pdap->widx++;
  pb = pdap->mesbuf + idx;

  if (pb->allo < len)
  {
    pb->pd = realloc(pb->pd,len);
    pb->allo = len;
  }
  pb->len = len;
  pb->s_mes_id = id;
  memcpy(pb->pd,pd,len);
}

void dap_set_prefix(struct t_decode *pdec,char *s_pre,char *r_pre)
{
  struct t_multidap *pmdap;
  struct t_dap *pdap;
  pmdap = (struct t_multidap *) pdec->priv;
  for (int i = 0; i < pmdap->ndap; i++)
  {
    pdap = pmdap->daps + i;
    pdap->s_prefix = malloc(strlen(s_pre) + 12);
    pdap->r_prefix = malloc(strlen(r_pre) + 12);
    sprintf(pdap->s_prefix,"%s.%d",s_pre,i);
    sprintf(pdap->r_prefix,"%s.%d",r_pre,i);
  }
}
