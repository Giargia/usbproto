#ifndef _DRIVER_CMSIS_H_
#define _DRIVER_CMSIS_H_

#define NDAP             2
#define NBRK            20

#define LIBUSB_OK        0
#define NRETVAL         20 

#define LOG_BASIC        1
#define LOG_FUN          2

#define POLL_MULTI       1
#define POLL_STICKY      2

#define SET_TAR          1
#define SET_CSW          2
#define SET_CSW2         4
#define SET_SEL          8
#define SET_SEL2      0x10 
#define GET_RDBUFF    0x20
#define USE_BD1       0x40
#define USE_BD2       0x80
#define USE_BD3      0x100
#define SET_AUTO     0x200
// #define SET_PACK     0x400
#define SET_NOPACK   0x400

#define BOOTROM_MAGIC        0x01754d
#define BOOTROM_MAGIC_ADDR 0x00000010

#define MAKE_TAG(a, b) (((b)<<8) | a)
#define FUNC_DEBUG_TRAMPOLINE       MAKE_TAG('D', 'T')
#define FUNC_DEBUG_TRAMPOLINE_END   MAKE_TAG('D', 'E')
#define FUNC_FLASH_EXIT_XIP         MAKE_TAG('E', 'X')
#define FUNC_CONNECT_INTERNAL_FLASH MAKE_TAG('I', 'F')
#define FUNC_FLASH_RANGE_ERASE      MAKE_TAG('R', 'E')
#define FUNC_FLASH_RANGE_PROGRAM    MAKE_TAG('R', 'P')
#define FUNC_FLASH_FLUSH_CACHE      MAKE_TAG('F', 'C')
#define FUNC_FLASH_ENTER_CMD_XIP    MAKE_TAG('C', 'X')
#define FIELD_(v,f) ((v & f##_MASK) >> f##_SHIFT)

#define CHK_LIBUSB(api,ret) \
  {if ((ret) != LIBUSB_OK) fail(api,ret,__LINE__); else if (loglev)   logmsg("%s OK",api);}

#define CHK_LIBUSB1(api,ret) \
  {if ((ret) != LIBUSB_OK) fail(api,ret,__LINE__); else if (plu->log) logmsg("%s OK",api);}

#define CHKSEQ(s)  \
  {if (pp->plu->log && s != pp->plu->count) \
   printf("CHKSEQ %5d %5d @%d\n",s,pp->plu->count,__LINE__);}

#define CHKSEQ1(s) \
{\
  if (cp->plu->log) {\
    printf("CHKSEQ %5d %5d @%d ",s,cp->plu->count,__LINE__); \
    if (s != cp->plu->count) printf("ERR"); \
    putchar('\n'); \
  } \
}

#define ENTRY(m) \
if (cp->plu->log & LOG_FUN) \
{ if (func_lev < 20) func_count[func_lev] = cp->plu->count; \
  printf("F%*.*s",func_lev*2,func_lev*2,""); \
  logmsg(m " ENTRY"); \
  func_lev++; \
}

#define ENTRYP(m,f,par...) \
if (cp->plu->log & LOG_FUN) \
{ if (func_lev < 20) func_count[func_lev] = cp->plu->count; \
  printf("F%*.*s",func_lev*2,func_lev*2,""); \
  logmsg(m " ENTRY " f,par); \
  func_lev++; \
}

#define EXIT(m) \
if (cp->plu->log & LOG_FUN) \
{ func_lev--; \
  printf("F%*.*s",func_lev*2,func_lev*2,""); \
  int c = (func_lev < 20) ? func_count[func_lev] : 0;\
  logmsg(m " EXIT (%d)",cp->plu->count - c); \
}

#define EXITP(m,f,par...) \
if (cp->plu->log & LOG_FUN) \
{ func_lev--; \
  printf("F%*.*s",func_lev*2,func_lev*2,""); \
  int c = (func_lev < 20) ? func_count[func_lev] : 0;\
  logmsg(m " EXIT (%d) " f,cp->plu->count - c,par); \
}

enum 
{
  ID_DAP_INFO                = 0x00,
  ID_DAP_LED                 = 0x01,
  ID_DAP_HOST_STATUS         = 0x01, // ID_DAP_LED alias
  ID_DAP_CONNECT             = 0x02,
  ID_DAP_DISCONNECT          = 0x03,
  ID_DAP_TRANSFER_CONFIGURE  = 0x04,
  ID_DAP_TRANSFER            = 0x05,
  ID_DAP_TRANSFER_BLOCK      = 0x06,
  ID_DAP_TRANSFER_ABORT      = 0x07,
  ID_DAP_WRITE_ABORT         = 0x08,
  ID_DAP_DELAY               = 0x09,
  ID_DAP_RESET_TARGET        = 0x0a,
  ID_DAP_SWJ_PINS            = 0x10,
  ID_DAP_SWJ_CLOCK           = 0x11,
  ID_DAP_SWJ_SEQUENCE        = 0x12,
  ID_DAP_SWD_CONFIGURE       = 0x13,
  ID_DAP_JTAG_SEQUENCE       = 0x14,
  ID_DAP_JTAG_CONFIGURE      = 0x15,
  ID_DAP_JTAG_IDCODE         = 0x16,
  ID_DAP_SWD_SEQUENCE        = 0x1d,
};

enum 
{
  DAP_INFO_VENDOR            = 0x01,
  DAP_INFO_PRODUCT           = 0x02,
  DAP_INFO_SER_NUM           = 0x03,
  DAP_INFO_FW_VER            = 0x04,
  DAP_INFO_DEVICE_VENDOR     = 0x05,
  DAP_INFO_DEVICE_NAME       = 0x06,
  DAP_INFO_CAPABILITIES      = 0xf0,
  DAP_INFO_PACKET_COUNT      = 0xfe,
  DAP_INFO_PACKET_SIZE       = 0xff,
};

enum 
{
  DAP_TRANSFER_APnDP         = 1 << 0,
  DAP_TRANSFER_RnW           = 1 << 1,
  DAP_TRANSFER_A2            = 1 << 2,
  DAP_TRANSFER_A3            = 1 << 3,
  DAP_TRANSFER_MATCH_VALUE   = 1 << 4,
  DAP_TRANSFER_MATCH_MASK    = 1 << 5,
};

enum 
{
  DAP_TRANSFER_INVALID       = 0,
  DAP_TRANSFER_OK            = 1 << 0,
  DAP_TRANSFER_WAIT          = 1 << 1,
  DAP_TRANSFER_FAULT         = 1 << 2,
  DAP_TRANSFER_ERROR         = 1 << 3,
  DAP_TRANSFER_MISMATCH      = 1 << 4,
  DAP_TRANSFER_NO_TARGET     = 7,
};

enum 
{
  DAP_PORT_SWD               = 1 << 0,
  DAP_PORT_JTAG              = 1 << 1,
};

enum 
{
  DAP_SWJ_SWCLK_TCK          = 1 << 0,
  DAP_SWJ_SWDIO_TMS          = 1 << 1,
  DAP_SWJ_TDI                = 1 << 2,
  DAP_SWJ_TDO                = 1 << 3,
  DAP_SWJ_nTRST              = 1 << 5,
  DAP_SWJ_nRESET             = 1 << 7,
};

enum 
{
  DAP_OK                     = 0x00,
  DAP_ERROR                  = 0xff,
};

enum
{
  S_UNKNOWN,
  S_RESET,
  S_LOCKUP,
  S_SLEEPING,
  S_HALTED,
  S_RUNNING,
  S_DEBRUN
};

enum
{
// Component identification register offsets.
  PIDR4   = 0xfd0,
  PIDR0   = 0xfe0,
  CIDR0   = 0xff0,
  IDR_END = 0x1000
};

enum
{
  DAP_SEND    = 0,
  DAP_RECEIVE = 1,
  DAP_OP_MASK = 1,
  DAP_DECODE  = 2
};

struct t_set_reg
{
  int  regid;
  uint regval;
};

// see openocd src/jtag/swd.h
/*
 * The following sequences are updated to
 * ARM(tm) Debug Interface v5 Architecture Specification    ARM IHI 0031E
 */

/**
 * SWD Line reset.
 *
 * SWD Line reset is at least 50 SWCLK cycles with SWDIO driven high,
 * followed by at least two idle (low) cycle.
 * Bits are stored (and transmitted) LSB-first.
 */
static const uchar swd_seq_line_reset[] = 
{
/* At least 50 SWCLK cycles with SWDIO high */
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
/* At least 2 idle (low) cycles */
  0x00,
};
static const unsigned swd_seq_line_reset_len = 64;

/**
 * JTAG-to-SWD sequence.
 *
 * The JTAG-to-SWD sequence is at least 50 TCK/SWCLK cycles with TMS/SWDIO
 * high, putting either interface logic into reset state, followed by a
 * specific 16-bit sequence and finally a line reset in case the SWJ-DP was
 * already in SWD mode.
 * Bits are stored (and transmitted) LSB-first.
 */
static const uchar swd_seq_jtag_to_swd[] = 
{
/* At least 50 TCK/SWCLK cycles with TMS/SWDIO high */
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
/* Switching sequence from JTAG to SWD */
  0x9e, 0xe7,
/* At least 50 TCK/SWCLK cycles with TMS/SWDIO high */
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
/* At least 2 idle (low) cycles */
  0x00,
};
static const unsigned swd_seq_jtag_to_swd_len = 136;
/**
 * SWD-to-JTAG sequence.
 *
 * The SWD-to-JTAG sequence is at least 50 TCK/SWCLK cycles with TMS/SWDIO
 * high, putting either interface logic into reset state, followed by a
 * specific 16-bit sequence and finally at least 5 TCK/SWCLK cycles with
 * TMS/SWDIO high to put the JTAG TAP in Test-Logic-Reset state.
 * Bits are stored (and transmitted) LSB-first.
 */
static const uchar swd_seq_swd_to_jtag[] = 
{
/* At least 50 TCK/SWCLK cycles with TMS/SWDIO high */
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
/* Switching sequence from SWD to JTAG */
  0x3c, 0xe7,
/* At least 5 TCK/SWCLK cycles with TMS/SWDIO high */
  0xff,
};
static const unsigned swd_seq_swd_to_jtag_len = 80;

/**
 * SWD-to-dormant sequence.
 *
 * This is at least 50 SWCLK cycles with SWDIO high to put the interface
 * in reset state, followed by a specific 16-bit sequence.
 * Bits are stored (and transmitted) LSB-first.
 */
static const uchar swd_seq_swd_to_dormant[] = 
{
/* At least 50 SWCLK cycles with SWDIO high */
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
/* Switching sequence from SWD to dormant */
  0xbc, 0xe3,
};
static const unsigned swd_seq_swd_to_dormant_len = 72;

/**
 * Dormant-to-SWD sequence.
 *
 * This is at least 8 TCK/SWCLK cycles with TMS/SWDIO high to abort any ongoing
 * selection alert sequence, followed by a specific 128-bit selection alert
 * sequence, followed by 4 TCK/SWCLK cycles with TMS/SWDIO low, followed by
 * a specific protocol-dependent activation code. For SWD the activation code
 * is an 8-bit sequence. The sequence ends with a line reset.
 * Bits are stored (and transmitted) LSB-first.
 */
static const uchar swd_seq_dormant_to_swd[] = 
{
/* At least 8 SWCLK cycles with SWDIO high */
  0xff,
/* Selection alert sequence */
  0x92, 0xf3, 0x09, 0x62, 0x95, 0x2d, 0x85, 0x86,
  0xe9, 0xaf, 0xdd, 0xe3, 0xa2, 0x0e, 0xbc, 0x19,
/*
 * 4 SWCLK cycles with SWDIO low ...
 * + SWD activation code 0x1a ...
 * + at least 8 SWCLK cycles with SWDIO high
 */
  0xa0, /* ((0x00)      & GENMASK(3, 0)) | ((0x1a << 4) & GENMASK(7, 4)) */
  0xf1, /* ((0x1a >> 4) & GENMASK(3, 0)) | ((0xff << 4) & GENMASK(7, 4)) */
  0xff,
/* At least 50 SWCLK cycles with SWDIO high */
  0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
/* At least 2 idle (low) cycles */
  0x00,
};
static const unsigned swd_seq_dormant_to_swd_len = 224;

/**
 * JTAG-to-dormant sequence.
 *
 * This is at least 5 TCK cycles with TMS high to put the interface
 * in test-logic-reset state, followed by a specific 31-bit sequence.
 * Bits are stored (and transmitted) LSB-first.
 */
static const uchar swd_seq_jtag_to_dormant[] = 
{
/* At least 5 TCK cycles with TMS high */
  0xff,
/*
 * Still one TCK cycle with TMS high followed by 31 bits JTAG-to-DS
 * select sequence 0xba, 0xbb, 0xbb, 0x33,
 */
  0x75, /* ((0xff >> 7) & GENMASK(0, 0)) | ((0xba << 1) & GENMASK(7, 1)) */
  0x77, /* ((0xba >> 7) & GENMASK(0, 0)) | ((0xbb << 1) & GENMASK(7, 1)) */
  0x77, /* ((0xbb >> 7) & GENMASK(0, 0)) | ((0xbb << 1) & GENMASK(7, 1)) */
  0x67, /* ((0xbb >> 7) & GENMASK(0, 0)) | ((0x33 << 1) & GENMASK(7, 1)) */
};
static const unsigned swd_seq_jtag_to_dormant_len = 40;
/**
 * Dormant-to-JTAG sequence.
 *
 * This is at least 8 TCK/SWCLK cycles with TMS/SWDIO high to abort any ongoing
 * selection alert sequence, followed by a specific 128-bit selection alert
 * sequence, followed by 4 TCK/SWCLK cycles with TMS/SWDIO low, followed by
 * a specific protocol-dependent activation code. For JTAG there are two
 * possible activation codes:
 * - "JTAG-Serial": 12 bits 0x00, 0x00
 * - "Arm CoreSight JTAG-DP": 8 bits 0x0a
 * We use "JTAG-Serial" only, which seams more generic.
 * Since the target TAP can be either in Run/Test Idle or in Test-Logic-Reset
 * states, Arm recommends to put the TAP in Run/Test Idle using one TCK cycle
 * with TMS low. To keep the sequence length multiple of 8, 8 TCK cycle with
 * TMS low are sent (allowed by JTAG state machine).
 * Bits are stored (and transmitted) LSB-first.
 */
static const uchar swd_seq_dormant_to_jtag[] = 
{
/* At least 8 TCK/SWCLK cycles with TMS/SWDIO high */
  0xff,
/* Selection alert sequence */
  0x92, 0xf3, 0x09, 0x62, 0x95, 0x2d, 0x85, 0x86,
  0xe9, 0xaf, 0xdd, 0xe3, 0xa2, 0x0e, 0xbc, 0x19,
/*
 * 4 TCK/SWCLK cycles with TMS/SWDIO low ...
 * + 12 bits JTAG-serial activation code 0x00, 0x00
 */
  0x00, 0x00,
/* put the TAP in Run/Test Idle */
  0x00,
};
static const unsigned swd_seq_dormant_to_jtag_len = 160;

#define ABORT_DAPABORT           0x00000001
#define ABORT_STKCMPCLR          0x00000002
#define ABORT_STKERRCLR          0x00000004
#define ABORT_WDERRCLR           0x00000008
#define ABORT_ORUNERRCLR         0x00000010

// DP Control / Status Register bit definitions
#define CTRL_STAT_ORUNDETECT_MASK     0x00000001
#define CTRL_STAT_ORUNDETECT_SHIFT             0
#define CTRL_STAT_STICKYORUN_MASK     0x00000002
#define CTRL_STAT_STICKYORUN_SHIFT             1
#define CTRL_STAT_TRNMODE_MASK        0x000000c0
#define CTRL_STAT_TRNMODE_SHIFT                2
#define CTRL_STAT_STICKYCMP_MASK      0x00000010
#define CTRL_STAT_STICKYCMP_SHIFT              4
#define CTRL_STAT_STICKYERR_MASK      0x00000020
#define CTRL_STAT_STICKYERR_SHIFT              5
#define CTRL_STAT_READOK_MASK         0x00000040
#define CTRL_STAT_READOK_SHIFT                 6
#define CTRL_STAT_WDATAERR_MASK       0x00000080
#define CTRL_STAT_WDATAERR_SHIFT               7
#define CTRL_STAT_MASKLANE_MASK       0x00000f00
#define CTRL_STAT_MASKLANE_SHIFT               8
#define CTRL_STAT_TRNCNT_MASK         0x00fff000
#define CTRL_STAT_TRNCNT_SHIFT                12
#define CTRL_STAT_CDBGRSTREQ_MASK      (1 << 26)
#define CTRL_STAT_CDBGRSTREQ_SHIFT            26
#define CTRL_STAT_CDBGRSTACK_MASK      (1 << 27)
#define CTRL_STAT_CDBGRSTACK_SHIFT            27
#define CTRL_STAT_CDBGPWRUPREQ_MASK   0x10000000
#define CTRL_STAT_CDBGPWRUPREQ_SHIFT          28
#define CTRL_STAT_CDBGPWRUPACK_MASK   0x20000000
#define CTRL_STAT_CDBGPWRUPACK_SHIFT          29
#define CTRL_STAT_CSYSPWRUPREQ_MASK   0x40000000
#define CTRL_STAT_CSYSPWRUPREQ_SHIFT          30
#define CTRL_STAT_CSYSPWRUPACK_MASK   0x80000000
#define CTRL_STAT_CSYSPWRUPACK_SHIFT          31

#define CTRL_STAT_TRNNORMAL      0x00000000
#define CTRL_STAT_MASKLANE       0x00000f00

// AP CSW Control and Status Word definitions
#define CSW_SIZE_MASK            0x00000007
#define CSW_SIZE_SHIFT                    0
#define CSW_SIZE8                0x00000000
#define CSW_SIZE16               0x00000001
#define CSW_SIZE32               0x00000002
#define CSW_SIZE64               0x00000003
#define CSW_SIZE128              0x00000004
#define CSW_SIZE256              0x00000005
#define CSW_ADDRINC_MASK         0x00000030
#define CSW_ADDRINC_SHIFT                 4 
#define CSW_NADDRINC             0x00000000 // No increment
#define CSW_SADDRINC             0x00000010 // Single increment by SIZE field
#define CSW_PADDRINC             0x00000020 // Packed increment, supported only on M3/M3 AP
#define CSW_DEVICEEN_MASK        0x00000040
#define CSW_DEVICEEN_SHIFT                6
#define CSW_TRINPROG_MASK        0x00000080 // Not implemented on M33 AHB5-AP
#define CSW_TRINPROG_SHIFT                7 // Not implemented on M33 AHB5-AP
#define CSW_MODE_MASK            (0xf << 8)
#define CSW_MODE_SHIFT                    8
#define CSW_TYPE_MASK           (0xf << 12)
#define CSW_TYPE_SHIFT                   12 
#define CSW_ERRNPASS             0x00010000 // MEM-APv2 only
#define CSW_ERRSTOP              0x00020000 // MEM-APv2 only
#define CSW_SDEVICEEN_MASK       0x00800000 // Also called SPIDEN in ADIv5
#define CSW_SDEVICEEN_SHIFT              23 // Also called SPIDEN in ADIv5
#define CSW_SPIDEN_MASK          CSW_SDEVICEEN_MASK
#define CSW_SPIDEN_SHIFT         CSW_SDEVICEEN_SHIFT

#define CSW_CACHE_MASK          (0xf << 24) 
#define CSW_CACHE_SHIFT                 24  
#define CSW_PROT_MASK             (7 << 28) 
#define CSW_PROT_SHIFT                   28  
#define CSW_MSTRTYPE             0x20000000 // Only present in M3/M3 AHB-AP, RES0 in others
#define CSW_MSTRCORE             0x00000000
#define CSW_MSTRDBG              0x20000000
#define CSW_DBGSWENABLE_MASK     0x80000000 // Only present in CSSoC-400 APB-AP, RES0 in others
#define CSW_DBGSWENABLE_SHIFT            31 


#define CSW_HPROT_MASK           0x0f000000 //  HPROT[3:0]
#define CSW_HPROT_SHIFT                  24

#define CSW_HNONSEC_MASK         0x40000000
#define CSW_HNONSEC_SHIFT                30

/**** --------- verify
#define CSW_8BIT                0
#define CSW_16BIT               1
#define CSW_32BIT               2
#define CSW_ADDRINC_OFF     0UL
#define CSW_ADDRINC_SINGLE  (1UL << 4)
#define CSW_ADDRINC_PACKED  (2UL << 4)
#define CSW_DEVICE_EN       (1UL << 6)
#define CSW_TRIN_PROG       (1UL << 7)
--------- verify ****/

#define DAP_RO 1
#define DAP_WO 0
#define DAP_RW 2

#define DAP_DP 0
#define DAP_AP 1

#define DAP_MEMAP 2

#define DAP_X  255

#define DAP_DP_ABORT             0
#define DAP_DP_DPIDR             1
#define DAP_DP_SELECT            2
#define DAP_DP_RDBUFF            3
#define DAP_DP_CTRL_STAT         4 
#define DAP_DP_DLCR              5
#define DAP_DP_TARGETID          6
#define DAP_DP_DLPIDR            7
#define DAP_DP_EVENTSTAT         8
#define DAP_DP_TARGETSEL         9

#define DAP_DP_DPIDR_ADD         0
#define DAP_DP_ABORT_ADD         0
#define DAP_DP_CTRL_STAT_ADD     4
#define DAP_DP_DLCR_ADD          4
#define DAP_DP_SELECT_ADD        8
#define DAP_DP_RDBUFF_ADD       12
#define DAP_DP_TARGETID_ADD      4
#define DAP_DP_TARGETSEL_ADD    12
#define DAP_DP_DLPIDR_ADD        4
#define DAP_DP_EVENTSTAT_ADD     4
#define DAP_AP_IDR_ADD        0xfc

#define DAP_MEMAP_CSW           10 
#define DAP_MEMAP_TAR           11
#define DAP_MEMAP_TARH          12
#define DAP_MEMAP_DRW           13
#define DAP_MEMAP_BD0           14
#define DAP_MEMAP_BD1           15
#define DAP_MEMAP_BD2           16
#define DAP_MEMAP_BD3           17
#define DAP_MEMAP_MBT           18
#define DAP_MEMAP_BASEH         19
#define DAP_MEMAP_CFG           20
#define DAP_MEMAP_BASE          21
#define DAP_AP_IDR              22

// reg add / 4
#define DAP_MEMAP_CSW_ADD        0
#define DAP_MEMAP_TAR_ADD        4
#define DAP_MEMAP_TARH_ADD       8
#define DAP_MEMAP_DRW_ADD      0xc
#define DAP_MEMAP_BD0_ADD     0x10
#define DAP_MEMAP_BD1_ADD     0x14
#define DAP_MEMAP_BD2_ADD     0x18
#define DAP_MEMAP_BD3_ADD     0x1c
#define DAP_MEMAP_MBT_ADD     0x20
#define DAP_MEMAP_BASEH_ADD   0xf0
#define DAP_MEMAP_CFG_ADD     0xf4
#define DAP_MEMAP_BASE_ADD    0xf8
#define DAP_AP_IDR_ADD        0xfc

#define DAP_DP_SELECT_APSEL_MASK         0xff00000
#define DAP_DP_SELECT_APSEL_SHIFT               24

#define DAP_DP_SELECT_APBANKSEL_MASK     0x00000f0
#define DAP_DP_SELECT_APBANKSEL_SHIFT            4

#define DAP_DP_SELECT_DPBANKSEL_MASK     0x000000f
#define DAP_DP_SELECT_DPBANKSEL_SHIFT            0

union ptr
{
  uchar  *puc;
  ushort *pus;
  uint   *pui;
  void   *pv;
};

typedef struct libusb_context libusb_context; 
typedef struct libusb_device_handle libusb_device_handle; 

struct t_libusb
{
  double tstart;
  libusb_context *ctx;
  libusb_device_handle *handle;
  uchar *txbuf;
  uchar *rxbuf;
  int    iface;
  int    epr;
  int    epw;
  int    log;      // general log flags
  int    tlog;     // tcl log flags
  int    timeout;
  int    bufsize;
  int    count;
  struct t_decode dec;
};

enum
{
  BRK_F_HW  = 1,
  BRK_F_ACT = 2,
};

struct t_brk
{
  uint   add;
  uint   count;
  ushort save;
  ushort flag; // BRK_F_*
};

struct t_brkt
{
  uint   fp_ctrl; // cached value of FP_CTRL
  int    nbrk;
  struct t_brk brk[NBRK];
};

struct t_cmsisbuf
{
  uint   id;
  uchar  *pbuf; // current pointer to txbuf
  uchar  *txbuf;
// rxbuf is in t_libusb (a single receive buffer is enough)
// remember return value pointer (queued read operation, may be null)
  uint   nextrv; // next retval id
  union  ptr retval[NRETVAL];
  uchar  retvals[NRETVAL];
// remember regid of register read
  uint   regid[NRETVAL];
  struct t_cmsisbuf *pnext;
  struct t_cmsisbuf *pprev;
};

struct t_cmsisprobe
{
  struct t_libusb *plu;
// see https://arm-software.github.io/CMSIS_5/DAP/html/group__DAP__Info.html
  uint   capabilities;

  int    bufcount;
  int    buflimit;
  struct t_cmsisbuf *bufhead;
  struct t_cmsisbuf *buftail;
  uint   r_used;   // used space in receive buffer
  uint   speed;
  int    ndap;     // multidrop if ndap > 1
  struct t_multidap *pmdap;
  int    r_dapidx; // requested index inside dapinfo
  int    c_dapidx; // current 
  uchar  flushed;  // buffer just flushed
  uint   cumulate_sticky;
  struct t_brkt bt;
// TODO fields depends on cpu 
// better move them somewhere ...
  uint   dhcsr; // cached dhcsr
// field from FP_CTRL
  uchar  fp_ncomp; 
  uchar  fp_nlit;
// field from DWT_CTRL
  uchar  dwt_num_comp;
  uchar  fpv4;  // has fpv4 extension
  uchar  fpv5;  // has fpv5 extension

  void   *priv;    // private data for jim interface
};

extern int func_lev;
extern int func_count[20];

extern int loglev;

int   cmsis_que_cmd(struct t_cmsisprobe *cp,int regid,int op,uint val,void *rval,uchar size);
void  DAP_error(char *str,int res,int line);
int   parity32(uint v);
uint  dhcsr_state(uint dhcsr);
int   set32p(uchar *p,uint v);
void  logmsg(char *e,...);
int   send(struct t_libusb *plu,int txlen,int line);
uchar swd_cmd(struct t_dap *pdap,int regid,int op);
int   receive(struct t_libusb *plu,int *res,int line);
int   setreqblock(struct t_cmsisprobe *cp,uchar *buf,int reg,int op,uint val,int log);
int   sendrec(libusb_device_handle *handle,int ep,
              uchar *txbuf,int ltx,
              uchar *rxbuf,int lrx,int line);
int   sendrec1(struct t_libusb *plu,int txlen,int line);
int   get_count();
int   cmsis_setup(struct t_cmsisprobe *cp);
int   cmsis_poll(struct t_cmsisprobe *cp,int fl,int poll_flag,uint dfsr,uint *regs);
int   cmsis_gdb_connect(struct t_cmsisprobe *cp);
int   cmsis_halt(struct t_cmsisprobe *cp,int maskint);
int   cmsis_read_flash(struct t_cmsisprobe *cp,int num);
int   cmsis_read_mem(struct t_cmsisprobe *cp,uint add,int size,uint len,uchar *mem);
int   cmsis_examine(struct t_cmsisprobe *cp,int fl);
int   cmsis_assert_reset(struct t_cmsisprobe *cp);
int   cmsis_target_reset(struct t_cmsisprobe *cp);
int   cmsis_step(struct t_cmsisprobe *cp);
int   cmsis_step1(struct t_cmsisprobe *cp,uint add,int idx,uint *regs);
int   cmsis_read_reg(struct t_cmsisprobe *cp,int rid,uint *val,int fl);
int   cmsis_cleanup(struct t_cmsisprobe *cp);
int   cmsis_read_regs_f(struct t_cmsisprobe *cp,int fl,uint *regs,int fl1);
int   cmsis_step_fast(struct t_cmsisprobe *cp);
int   cmsis_trace_ex(struct t_cmsisprobe *cp,uint nstep,uint flag,uint tracestart);
int   cmsis_cont(struct t_cmsisprobe *cp);
int   cmsis_set_break(struct t_cmsisprobe *cp,uint add,int idx);
int   cmsis_write_16(struct t_cmsisprobe *cp,uint add,ushort val,int fl);
int   cmsis_write_32(struct t_cmsisprobe *cp,uint add,uint val,int fl);
int   cmsis_write_mem16(struct t_cmsisprobe *cp,uint add,uint count,ushort *mem,int fl);
int   cmsis_write_mem32(struct t_cmsisprobe *cp,uint add,uint count,uint *mem,int fl);
void  cmsis_dumpreg(struct t_cmsisprobe *cp,uint *regs);
int   cmsis_debug_entry(struct t_cmsisprobe *cp,int dfsr_val,int fl,uint *regs);
int   cmsis_que_flush(struct t_cmsisprobe *cp,int receive);
int   cmsis_clear_sticky_errors(struct t_cmsisprobe *cp);
int   cmsis_dap_init_all(struct t_cmsisprobe *cp);
int   cmsis_find_mem_ap(struct t_cmsisprobe *cp);
int   cmsis_mem_ap_init(struct t_cmsisprobe *cp,int);
int   cmsis_dwt_setup(struct t_cmsisprobe *cp);
int   cmsis_multi_select(struct t_cmsisprobe *cp);
int   cmsis_read_mem32(struct t_cmsisprobe *cp,uint add,uint count,uint *mem,int fl);
int   cmsis_target_resume(struct t_cmsisprobe *cp,struct t_set_reg *psr,int fl);
int   cmsis_loadprg(struct t_cmsisprobe *cp,const char *prg,uint loadadd,uint inipc,uint inisp,int chkmem);
int   cmsis_runcpu(struct t_cmsisprobe *cp,uint wtime,uint count,uint *mem,int flag);

#endif
