/*
The probe outputs data to SWDIO on the falling edge of SWDCLK. The probe captures data from SWDIO on the rising edge of SWDCLK. The target outputs data to SWDIO on the rising edge of SWDCLK. The target captures data from SWDIO on the rising edge of SWDCLK.

*/
#define FILE int
#define CLK  0
#define DATA 1

int seq[256];

void init()
{
  int i,v;
  for (i = 0; i < 16; i++)
  {
    v = (1 << 7) + (i << 3) + (((nbit(i) + 1) & 1) << 2) + 1;
    seq[v] = 1;
#ifdef D
    printf("init i %2d %02x\n",i,v);
#endif
  }
}

void decode(int d,int p)
{
  char *sep;
  int j;
  sep = "";
  for (j = 15; j >= 0; j--)
  {
    printf("%s",sep);
    printf("%d",d >> (2 * j + p) & 1);
    sep = ".";
  }
}

int ss;
int v0,pos,count;

int chkseq(int v)
{
  ss = (ss << 1) & 0xff;
  ss |= (v & 1);
//printf("[%x]",ss);
  if (v == 1 && v0 == 0)
    pos = count;
  if (v == 0 && v0 == 1)
    if (count - pos > 8)
      printf("[%d one]",count - pos);

  count++;
  v0 = v;
      
  if (seq[ss])
  {
    printf("[%d %d %d]*",(ss >> 6) & 1,(ss >> 5) & 1,(ss >> 3) & 3);
    return 1;
  } else
    return 0;
}

char binbuf[40];
char *tobin(int v,int n)
{
  int i;
  for (i = 0; i < n; i++)
    binbuf[i] = ((v >> i) & 1) + '0';
  return binbuf; 
}

struct dstat
{
  int dir;
  int dow;
  int dor;
  int dircount;
  int bitcountt;
  int bitcount;
  int bitval,bitval1;
  int val32;
  int last1;
  int last0;
};

void newbit(struct dstat *ds,int bit)
{
  int  reset;
  int  start,stop,park,apndp,rnw,parity,A,cpar;
  char rwc[2],apdpc[2];

  rwc[0] = 'W'; rwc[1] = 'R';
  apdpc[0] = 'D'; apdpc[1] = 'A';
  reset = 0;
  if (bit == 1)
  {
    if(ds->last1 < 0)
      ds->last1 = ds->bitcount;
    ds->last0 = -1;
  } else
  {
    if (ds->last0 < 0)
      ds->last0 = ds->bitcount;
    if (ds->last1 >= 0 && ds->bitcount - ds->last1 > 50)
      printf("[RE]"); 
    ds->last1 = -1;
  }
  if (bit == 1 && ds->last1 >= 0 && ds->bitcount - ds->last1 == 50)
    reset = 1;
  ds->bitval  = (ds->bitval >> 1) | (bit << 7);
  ds->bitval1 = (ds->bitval1 >> 1) | (bit << 7);
  start  = ds->bitval1 & 1;
  stop   = (ds->bitval1 >> 6) & 1;
  park   = (ds->bitval1 >> 7) & 1;
  apndp  = (ds->bitval1 >> 1) & 1;
  rnw    = (ds->bitval1 >> 2) & 1;
  parity = (ds->bitval1 >> 5) & 1;
  A      = ((((ds->bitval1 >> 3) & 1) << 1) | ((ds->bitval1 >> 4) & 1));
  cpar   = apndp ^ rnw ^ (A & 1) ^ ((A >> 1) & 1); 
  
  if (ds->dir)
    printf("R%d.",bit);
  else
    printf("%d.",bit);

  ds->bitcount++;
  ds->bitcountt++;
  if (ds->dow == 0 && ds->dor == 0 && start == 1 && stop == 0 && park == 1)
  {
    if (parity == cpar)
    {
      printf("\n[%02x %c%c%x]",ds->bitval1,rwc[rnw],apdpc[apndp],A * 4);
      ds->dir = 1;
      ds->bitcount = 0;
      ds->bitval = 0;
      if (rnw)
      {
        ds->dircount = 38;
        ds->val32 = 0;
        ds->dor = 1;
      } else
      {
        ds->dircount = 6;
        ds->dow = 1;
        ds->val32 = 0;
      }
    } else
      printf("[SX]");
  }
 
  if (ds->dir)
  {
    ds->dircount--;
    if (ds->dircount == 0)
      printf("[P%08x:%d]",ds->val32,bit);
    ds->val32 = ds->val32 >> 1;
    ds->val32 = (ds->val32 & 0x7fffffff) | bit << 31;
    if (ds->dircount < 0)
    {
/*
      ds->bitcount = 1;
      ds->bitval  = bit << 7;
*/
      ds->bitcount = 0;
      ds->bitval   = 0;
      ds->dir = 0;
      ds->dor = 0;
      if (ds->dow)
        ds->dircount = 33;
    }
  } else if (ds->dow)
  {
    ds->dircount--;
    if (ds->dircount == 0)
      printf("[P%08x:%d]",ds->val32,bit);
    else
    {
      ds->val32 = ds->val32 >> 1;
      ds->val32 = (ds->val32 & 0x7fffffff) | bit << 31;
//    printf("(%d:%x)",ds->dircount,ds->val32);
    }
    if (ds->dircount < 0)
    {
      ds->dow = 0;
/*
      ds->bitcount = 1;
      ds->bitval  = bit << 7;
*/
      ds->bitcount = 0;
      ds->bitval   = 0;
    }
  }

  if (reset) 
  {
    ds->dor = ds->dow = 0;
    printf("[Rst]");
  }
  if (ds->bitcount && ds->bitcount % 8 == 0) 
  {
    printf("[%02x]",ds->bitval);
    ds->bitval = 0;
  }
  if (ds->bitcountt % 32 == 0) putchar('\n');
}

void decodebit(int *data,int n)
{
  int i,l;
  int v[2],v0[2];
  struct dstat ds;

  memset(&ds,0,sizeof ds);
  v0[0] = v0[1] = 0;
  for (i = 0; i < n; i++)
  {
    for (l = 15; l >= 0; l--)
    {
      v[0] = (data[i] >> (l * 2 + CLK)) & 1;
      v[1] = (data[i] >> (l * 2 + DATA)) & 1;
      if (ds.dir == 0 && v[0] == 1 && v0[0] == 0)
        newbit(&ds,v0[1]);
      else if (ds.dir == 1 && v[0] == 0 && v0[0] == 1)
        newbit(&ds,v0[1]);
      v0[0] = v[0];
      v0[1] = v[1];
    }
  }
}

void dumptick(int *data,int n,int k,int v0[2])
{
  int i,j,l,ok,v[2],v0s[2],pos;
  int v1,v2,count1,count2;
  int bytes[10];

  v0s[0] = v0[0];
  v0s[1] = v0[1];

  pos = 0;
  v1 = v2 = 0;
  count1 = count2 = 0;
  for (i = 0; i < n; i += k)
  {
#ifdef D
    printf("(%3d, CLK) ",i);
    for (j = 0; j < k && i + j < n; j++) 
    {
      decode(data[i + j],CLK);
      printf(".");
    }
    putchar('\n');
    printf("(%3d,DATA) ",i);
    for (j = 0; j < k && i + j < n; j++) 
    {
      decode(data[i + j],DATA);
      printf(".");
    }
    putchar('\n');
#endif

    for (j = 0; j < k && i + j < n; j++) 
    {
//    printf("(%d,%08x)^ ",i+j,data[i + j]);
      for (l = 15; l >= 0; l--)
      {
        v[0] = (data[i + j] >> (l * 2 + CLK)) & 1;
        v[1] = (data[i + j] >> (l * 2 + DATA)) & 1;
        if (v[0] == 1 && v0[0] == 0)
        {
//        printf("%d.",v0[1]);
          count1++;
          v1 = (v1 >> 1) | (v0[1] << 7);
          if (count1 == 8)
          {
            printf("%02x.",v1);
            bytes[j] = v1;
            count1 = v1 = 0;
          }
/*
*/
        }
        v0[0] = v[0];
        v0[1] = v[1];
      }
    }
    printf(" [");
    for (j = 0; j < k; j++)
      printf("%s.",tobin(bytes[j],8));
    printf("]\n");
    for (j = 0; j < k && i + j < n; j++) 
    {
//    printf("(%d,%08x)v ",i+j,data[i + j]);
      for (l = 15; l >= 0; l--)
      {
        v[0] = (data[i + j] >> (l * 2 + CLK)) & 1;
        v[1] = (data[i + j] >> (l * 2 + DATA)) & 1;
        if (v[0] == 0 && v0s[0] == 1)
        {
//        printf("%d.",v0s[1]);
          count2++;
          v2 = (v2 >> 1) | (v0s[1] << 7);
          if (count2 == 8)
          {
            printf("%02x.",v2);
            count2 = v2 = 0;
          }
        }
        v0s[0] = v[0];
        v0s[1] = v[1];
      }
    }
    putchar('\n');

    putchar('\n');
  }
}

int rfile(FILE *pf,int *data,int ndata)
{
  int n,i,d;
  char line[200];
  char *p;
  
  while (p = fgets(line,sizeof line,pf))
  {
    n = sscanf(line,"\[%d\] %x",&i,&d);
    if (n == 2)
    {
#ifdef D 
      printf("%3d %08x CLK  ",i,d);
      decode(d,CLK);
      putchar('\n');
      printf("             DATA ");
      decode(d,DATA);
      putchar('\n');
      putchar('\n');
#endif
      if (i < ndata)
        data[i] = d;
      else
        printf("data overflow %d >= %d\n",i,ndata);
    }
  }
  return i + 1;
}

int data[2048];

int main(int na,char **v)
{
  int n,v0[2];
  char *fn;
  FILE *pf;

  if (na > 1) 
    fn = v[1];
  else
    fn = "swdtrace.lis";
  pf = fopen(fn,"r");
  if (pf)
  {
    init();
    n = rfile(pf,data,sizeof(data)/sizeof data[0]);
    fclose(pf);
    printf("found %d data (%d tick)\n",n,n * 16);
    v0[0] = v0[1] = 0;
    decodebit(data,n);
    dumptick(data,n,4,v0);
  } else
    printf("can't open %s\n",fn);
  return 0; 
}
