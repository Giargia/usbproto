#include "env.h"
#include "util.h"

int logm;

uint nbit(uint v)
{
  int i,n;
  for (i = n = 0; i < 32; i++)
  {
    if (v & 1) n++;
    v = v >> 1;
  }
  return n;
}

char *tobin(int v,int n,char *buf)
{
  int i;
  char *out,*p,*fmt;
  static char tobinbuf[33];

  if (buf) out = buf;
  else     out = tobinbuf;
  p = out;
  fmt = "01";
  for (i = n - 1; i >= 0; i--)
  {
    *p++ = fmt[((v >> i) & 1)];
    if (i % 4 == 0 && i)
      *p++ = '.';
  }
  *p = 0;
  return out;
}

int get32(uchar *p)
{
  int i;

  i = (p[3] & MB) << 24 | (p[2] & MB) << 16 | (p[1] & MB) << 8 | (p[0] & MB);
  return i;
}

int get32p(uchar *pb,int *p)
{
  int i;
  i = get32(pb);
  *p = pb[4];
  return i;
}

int get16(uchar *p)
{
  int i;

  i = (p[1] & MB) << 8 | (p[0] & MB);
  return i;
}

int set16(uchar *p,int v)
{
  p[1] = (v >>  8) & MB;
  p[0] = v & MB;
  return 2;
}

int set32(uchar *p,int v)
{
  p[3] = (v >> 24) & MB;
  p[2] = (v >> 16) & MB;
  p[1] = (v >>  8) & MB;
  p[0] = v & MB;
  return 4;
}

int rev(int v)
{
  return (v & 1) << 7 | ((v >> 1) & 1) << 6 | ((v >> 2) & 1) << 5 |
         ((v >> 3) & 1) << 4 | ((v >> 4) & 1) << 3 | ((v >> 5) & 1) << 2 |
         ((v >> 6) & 1) << 1 | ((v >> 7) & 1) ;
}

double dtime()
{
  double t;
  struct timespec now;
  clock_gettime(CLOCK_REALTIME, &now);
  t = now.tv_sec + now.tv_nsec / 1e9;
  return t;
}

static uint count = 0;
int util_count()
{
  return count;
}

void memdump(uchar *add,int len,char *pre,int fl)
{
  int i,j;
  uchar *badd;

  badd = add;
  if (fl)
  {
    count++;
    printf("%s dumpbuf %6d len %d\n",pre,count,len);
//  if (len < 64) len = 64;
  } else
    printf("%s dumpbuf\n",pre);
  
  while(len > 0)
  {
    j = len;
    if (j > 16) j = 16;
    printf("%s%04zx: ",pre,add - badd);
    for (i = 0; i < j; i = i + 1)
      printf("%02x ",add[i]);
#ifdef XX
    for (; i < 16; i = i + 1) printf("   ");
    for (i = 0; i < j; i = i + 1)
    {
      uchar c = add[i];
      if (c < ' ' || c >= 127) c = '.';
      printf("%c",c);
    }
#endif
    putchar('\n');
    if (len >= 16)
      len = len - 16;
    else
      len = 0;
    add = add + 16;
  }
}

void dumpmem(uchar *add,size_t len,uchar *off)
{
  int   i,j;
  uchar *badd;
  uchar c;

  badd = add;
  uchar *end = add + len;
  printf("In dumpmem add %p len %ld\n",add,len);
  while(add < end)
  {
    j = len;
    if (j > 16) j = 16;
    printf("%10p: ",add - badd + off);
    for (i = 0; i < j; i = i + 1)
      printf("%02x ",add[i] & 0xff);
    for (; i < 16; i = i + 1) printf("   ");
    for (i = 0; i < j; i = i + 1)
    {
      c = add[i];
      if (c < ' ' || c >= 127) c = '.';
      printf("%c",c);
    }
    putchar('\n');
    len = len - 16;
    add = add + 16;
  }
}

void *myalloc(size_t s)
{
  static int nm;
  nm++;
  void *p = malloc(s);
  if (logm)
    printf("M %6zd %p\n",s,p);
/*
  if (s > 10000 || nm == 584)
    printf("HERE s %d p %p nm %d\n",s,p,nm);
*/
  return p;
}

void *mycalloc(size_t s,size_t n)
{
  void *p = calloc(s,n);
  if (logm)
    printf("C %6zd %6zd %p\n",s,n,p);
  return p;
}

void myfree(void *p)
{
  if (logm)
    printf("F %p\n",p);
  free(p);
}

void error(char *e,...)
{
  va_list args;
  va_start(args,e);
  printf("E: ");
  vprintf(e,args);
  putchar('\n');
  fflush(stdout);
  exit(0);
}

void warning(char *e,...)
{
  va_list args;
  va_start(args,e);
  printf("W: ");
  vprintf(e,args);
  putchar('\n');
  fflush(stdout);
}

void info(char *e,...)
{
  va_list args;
  va_start(args,e);
  printf("I: ");
  vprintf(e,args);
  putchar('\n');
  fflush(stdout);
}

