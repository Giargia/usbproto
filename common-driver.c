#include <stdio.h>
#include "env.h"
#include "armreg.h"
#include "disasm.h"
#include "dump.h"
#include "common-driver.h"

//int disasm(stlink_t* sl,struct t_cpu *p_cpu,int pc,struct t_stat *p_st)
int disasm(struct disasm_par *par,struct t_cpu *p_cpu,uint pc,struct t_stat *p_st)
{
  int  err;
  int  l,ist,ist2;
  ushort code[4];
//struct t_stat stat = {0},*p_st;
  struct t_dec  /* dec  = {0},*/ *pd;

//err = stlink_read_mem32(sl, pc & ~3 ,(uchar *) code, sizeof code);
  err = par->fread_mem(par, pc & ~3 ,(uchar *) code, sizeof code);
  CHKERR(err,"stlink_read_mem32");
  p_st->pc = pc;
  int idx = 0;
  if ((pc & 3) == 2)
    idx = 1;
  ist2 = code[idx];
  ist  = code[idx + 1];
//  printf("ist %04x %04x \n",ist2,ist);
  if (((ist2 >> 12) & 0xf) == 0xf ||
     ((ist2 >> 11) & 0x1f) == 0x1d)
  {
    p_st->inst = (ist2 << 16) | (ist & 0xffff);
    l = 4;
  } else
  {
    p_st->inst = ist2;
    l = 2;
  }
  p_st->il = l;

  printf("%08x ",pc);
  if (p_st->il == 4)
    printf("%04x %04x ",p_st->inst >> 16,p_st->inst & MW);
  else
    printf("%04x      ",p_st->inst & MW);
  if (p_cpu->logflags)
    printf("%s ",fmtflags(p_cpu));
    
  pd = decode(p_cpu,p_st,0);
  if (pd->nfunc <= p_st->swi || p_st->swi < 0)
  {
    printf("nf %d swi %d\n",pd->nfunc,p_st->swi);
    printf("no func to call\n");
  }
  if (pd->nfunc > 0)
  {
    if (pd->func[p_st->swi].pdis)
      pd->func[p_st->swi].pdis(p_cpu,p_st,pd->func + p_st->swi);
    else if (pd->func[p_st->swi].fmtinst)
      dump_gen(p_cpu,p_st,pd->func + p_st->swi);
    else
    {
      printf("TBD null function swi %d\n",p_st->swi);
//    error("TDB");
    }
  } else
  {
    printf("TBD nfunc %d\n",pd->nfunc);
//  error("no func to call");
  }
  return 0;
}

