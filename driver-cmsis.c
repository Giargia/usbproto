/*
 libusb cygwin distribution is an old version that don't work with cmis-dap
 latest version on 9/11/23 (1.0.26) has been downloaded and extracted here for cygwin-32 and cygwin-64
 file are:
- libusb.h (same for 32 and 64)
- cygusb_32-1.0.dll fail
- cygusb_62-1.0.dll fail
- libsb_32-1.0.dll fail
- libsb_64-1.0.dll OK

so build with 64 bit !!!

dap protocol:

https://arm-software.github.io/CMSIS_5/DAP/html/group__DAP__Connect.html
*/

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
// see note on libusb-1.0
//#include <libusb-1.0/libusb.h>
#include "libusb.h"

#include "env.h"
#include "util.h"
#include "conf.h"
#include "readpcapng-stlink.h"
#include "dap-decode.h"
#include "dapreg.h"
#include "armreg.h"
#include "driver-cmsis.h"
#include "driver-pico.h"

#include "disasm.h"
#include "common-driver.h"
#include "dump.h"
#include "test.h"
#include "jim-interface-cmsis.h"

int func_lev;
int func_count[20];

int loglev;

// struct t_dap s_dap;
static uchar parity[256];

struct t_reg treg[NREG];
int    nreg;

const char *state_name[] = 
{ 
  "UNKNOWN",
  "RESET",
  "LOCKUP",
  "SLEEPING",
  "HALTED",
  "RUNNING",
  "DEBRUN"
};

static double start_time;
static int static_count;

static int regmap[] = 
{
  0,1,2,3,4,5,6,7,8,9,10,11,12,
  SP_RN,LR_RN,PC_RN,
  XPSR_RN,MAIN_SP_RN,PROCESS_SP_RN,CONTROL_RN
};

static char *regname[] =
{
  "R00", "R01", "R02", "R03", 
  "R04", "R05", "R06", "R07", 
  "R08", "R09", "R10", "R11",
  "R12", "SP",  "LR",  "PC",
  "XPSR","PSP", "MSP", "---"
};

int get_count()
{
  return static_count;
}

void logmsg(char *e,...)
{
  char mes[1024];
  static double last = 0;
  va_list args;
  va_start(args,e);
  vsprintf(mes,e,args);
  double t = dtime();
  if (last == 0) last = t;
  printf("[%9.6f %6.0f %4d] %s\n",t - start_time,(t - last) * 1e6,get_count(),mes);
  last = t;
}

int parity32(uint v)
{
  return (parity[(v >> 24) & MB] + parity[(v >> 16) & MB] + parity[(v >> 8) & MB] + parity[v & MB]) & 1;
}

int set32p(uchar *p,uint v)
{
  p[3] = (v >> 24) & MB;
  p[2] = (v >> 16) & MB;
  p[1] = (v >>  8) & MB;
  p[0] = v & MB; 
  int par = parity[p[0]] + parity[p[1]] + parity[p[2]] + parity[p[3]];
  p[4] = (par & 1) ;
  return 5;
}

void fail(char *api,int ret,int line)
{
  if (ret != 0)
  {
    printf("ERROR %s -> %d %s @%d\n",api,ret,libusb_error_name(ret),line);
    exit(0);
  }
}

void DAP_error(char *str,int res,int line)
{
  printf("error %s res %d @%d\n",str,res,line);
  exit(0);
}

uint dhcsr_state(uint dhcsr)
{
  if (dhcsr & DHCSR_S_RESET_ST)
    return S_RESET;
  if (dhcsr & DHCSR_S_LOCKUP)
    return S_LOCKUP;
/*
  else if (dhcsr & DHCSR_S_SLEEP)
    return S_SLEEPING;
*/
  else if (dhcsr & DHCSR_S_HALT)
    return S_HALTED;
  else 
    return S_RUNNING;
}

int usb_connect(struct t_libusb *plu,int vendor,int product)
{
  int ret;
  plu->handle = 0;

  if (plu->log)
    logmsg("usb_connect ctx %p vendor %4x product %4x",plu->ctx,vendor,product);

  libusb_device **list = NULL;
  size_t cnt = libusb_get_device_list(plu->ctx, &list);
//struct libusb_device_descriptor desc;
  if (plu->log)
    printf("libusb_get_device_list -> %zd\n",cnt);
  if (cnt > 0)
  {
    printf("#   add   ty  bcd  class subclass  proto maxpack vendor product dev  #conf\n");
    printf("-- ----- --- ----- ----- -------- ------ ------- ------ ------- ---- ---\n");
  }

  int n = 0;
  for (int i = 0; i < cnt; i++)
  {
    struct libusb_device_descriptor desc;
    ret = libusb_get_device_descriptor(list[i], &desc);
    CHK_LIBUSB("libusb_get_device_descriptor",ret);
    int busn = libusb_get_bus_number(list[i]);
    int deva = libusb_get_device_address(list[i]);
    printf("%2d %2d.%2d %3d %5x %5x %8x %6x  %6d   %04x    %04x %04x %3d\n",
           i,
           busn,deva,
           desc.bDescriptorType,
           desc.bcdUSB,
           desc.bDeviceClass,
           desc.bDeviceSubClass,
           desc.bDeviceProtocol,
           desc.bMaxPacketSize0,
           desc.idVendor,
           desc.idProduct,
           desc.bcdDevice,
           desc.bNumConfigurations);
    if (desc.idVendor == vendor && desc.idProduct == product)
    {
      n++;
      if (plu->handle == 0)
      {
        ret = libusb_open(list[i], &plu->handle);
        CHK_LIBUSB("libusb_open",ret);
        struct libusb_config_descriptor *config_desc;
        ret = libusb_get_config_descriptor(list[i], 0, &config_desc);
        CHK_LIBUSB("libusb_get_config_descriptor",ret);
        printf("-- configurationValue %d # interfaces %d\n",config_desc->bConfigurationValue,config_desc->bNumInterfaces);
        for (int i = 0; i < config_desc->bNumInterfaces; i++)
        {
          const struct libusb_interface_descriptor *intf_desc = &config_desc->interface[i].altsetting[0];
          int in = intf_desc->bInterfaceNumber;
          printf("---- %2d: int# %d iInterface %d #endpoint %d ",i,in,intf_desc->iInterface,intf_desc->bNumEndpoints);
          if (intf_desc->iInterface != 0)
          {
            uchar interface_str[256] = {0};

            int n = libusb_get_string_descriptor_ascii(plu->handle, 
                                                       intf_desc->iInterface,
                                                       interface_str, 
                                                       sizeof(interface_str));
            if (n > 0)
              printf("%s",interface_str);
          }
          putchar('\n');
          for (int e = 0; e < intf_desc->bNumEndpoints; e++)
            printf("------ %2d: ep type %x add %x attr %x class %d maxpack %d\n",
                   e,
                   intf_desc->endpoint[e].bDescriptorType,
                   intf_desc->endpoint[e].bEndpointAddress,
                   intf_desc->endpoint[e].bmAttributes,
                   intf_desc->bInterfaceClass,
                   intf_desc->endpoint[e].wMaxPacketSize);
        }
        libusb_unref_device (list[i]);
      } else
        libusb_unref_device (list[i]);
    } else
      libusb_unref_device (list[i]);
  }
  libusb_free_device_list(list, 0);
  if (plu->log)
    logmsg("usb_connect handle %p",plu->handle);
  return plu->handle ? 0 : 99;
}

void claim(libusb_context *ctx,libusb_device_handle *handle,int interface)
{
  int ret,config;

  ret = libusb_get_configuration(handle, &config);
  CHK_LIBUSB("libusb_get_configuration",ret);
  if (config != 1)
  {
    ret = libusb_set_configuration(handle, 1);
    CHK_LIBUSB("libusb_set_configuration",ret);
  }
  
  ret = libusb_claim_interface(handle, interface);
  logmsg("libusb_claim_interface %d ret %d",interface,ret);
//CHK_LIBUSB("libusb_claim_interface",ret);
}

int dap_send(struct t_libusb *plu,uchar *txbuf,int txlen,int line)
{
  int ret,res;

  plu->count++;
  static_count++;
  if (plu->log)
  {
    logmsg("%4d dap_send %d @%d",plu->count,txlen,line);
    memdump(txbuf,txlen,"B>> ",1);
    decodedap(txbuf,txlen,&plu->dec,1);
  }
  ret = libusb_bulk_transfer(plu->handle,plu->epw,txbuf, plu->bufsize, &res, plu->timeout);
  CHK_LIBUSB(">> libusb_bulk_transfer",ret);
  return ret;
}

int dap_receive(struct t_libusb *plu,int *res,int line)
{
  int ret;

  ret = libusb_bulk_transfer(plu->handle,plu->epr, plu->rxbuf, plu->bufsize, res, plu->timeout);
  plu->count++;
  static_count++;
  CHK_LIBUSB("<< libusb_bulk_transfer",ret);
  if (plu->log)
  {
    logmsg("%4d rec done @%d",plu->count,line); 
    memdump(plu->rxbuf,plu->bufsize,"B<< ",1);
    decoderepdap(plu->rxbuf,plu->bufsize,&plu->dec,1,0);
  }
  return ret;
}

int send(struct t_libusb *plu,int txlen,int line)
{
  int ret,res;

  plu->count++;
  static_count++;
  if (plu->log)
  {
    logmsg("%4d send %02x @%d",txlen,plu->txbuf[4],line); 
    memdump(plu->txbuf,txlen,"B>> ",1);
  }
  ret = libusb_bulk_transfer(plu->handle,plu->epw, plu->txbuf, txlen, &res, plu->timeout);
  CHK_LIBUSB(">> libusb_bulk_transfer",ret);
  return ret;
}

int receive(struct t_libusb *plu,int *res,int line)
{
  int ret;

  while(1)
  {
    ret = libusb_bulk_transfer(plu->handle,plu->epr, plu->rxbuf, plu->bufsize, res, plu->timeout);
    CHK_LIBUSB("<< libusb_bulk_transfer",ret);
    if (plu->log)
      logmsg("receive res %d ret %d",ret,*res);
    if (ret == LIBUSB_OK && *res > 0)
    {
      plu->count++;
      static_count++;
      if (plu->log)
      {
        logmsg("%4d receive done res %d @%d",plu->count,*res,line); 
        memdump(plu->rxbuf,*res,"B<< ",1);
      }
      return ret;
    }
  }
  return ret;  
}

int sendrec(libusb_device_handle *handle,int ep,
            uchar *txbuf,int ltx,
            uchar *rxbuf,int lrx,int line)
{
  int ret,res;

  static int count = 0;
//plu->count++;
//if (plu->log)
  {
    logmsg("%4d sendrec @%d",++count,line); 
    memdump(txbuf, ltx,"B>> ",1);
  }
  ret = libusb_bulk_transfer(handle, ep, txbuf, ltx, &res, 3000);
  CHK_LIBUSB(">> libusb_bulk_transfer",ret);
  ret = libusb_bulk_transfer(handle, ep + 0x80, rxbuf, lrx, &res, 3000);
  CHK_LIBUSB("<< libusb_bulk_transfer",ret);
//plu->count++;
//if (plu->log)
  {
    logmsg("%4d sendrec-old done @%d",count,line); 
    memdump(rxbuf,lrx,"B<< ",1);
  }
  return ret;  
}

int sendrec1(struct t_libusb *plu,int txlen,int line)
{
  int ret,res;
/* moved in main
  if (plu->count == 0)
    initdap(&dec,2);
*/
  plu->count++;
  static_count++;
  if (plu->log)
  {
    logmsg("%4d sendrec1 %d @%d",plu->count,txlen,line); 
    memdump(plu->txbuf,txlen,"B>> ",1);
    decodedap(plu->txbuf,plu->bufsize,&plu->dec,1);
  }
  ret = libusb_bulk_transfer(plu->handle,plu->epw, plu->txbuf, plu->bufsize, &res, plu->timeout);
  CHK_LIBUSB(">> libusb_bulk_transfer",ret);
  ret = libusb_bulk_transfer(plu->handle,plu->epr, plu->rxbuf, plu->bufsize, &res, plu->timeout);
  plu->count++;
  static_count++;
  CHK_LIBUSB("<< libusb_bulk_transfer",ret);
  if (plu->log)
  {
    logmsg("%4d sendrec done @%d",plu->count,line); 
    memdump(plu->rxbuf,plu->bufsize,"B<< ",1);
    decoderepdap(plu->rxbuf,plu->bufsize,&plu->dec,1,0);
  }
  
  return ret;  
}

int setreqblock(struct t_cmsisprobe *cp,uchar *buf,int reg,int op,uint val,int log)
{
  struct t_dap *pdap = cp->pmdap->daps + cp->c_dapidx;
  *buf++ = (pdap->regs[reg].add & 0xc) | pdap->regs[reg].ty | (op << 1);
  if (log)
    printf("setreqblock reg %d %s op %d -> %02x ",reg,pdap->regs[reg].name,op,buf[-1]);
  int r = 1;
  if (op == DAP_WO)
  {
    if (log) 
      printf("val %08x\n",val);
    r += set32(buf,val);
    struct t_libusb *plu = cp->plu;
//  pdap->regs[reg].vals = val;
    dap_setreg(pdap,reg,val,log ? OUT_MIN : OUT_NONE,DAP_WO,DAP_SEND,plu->count);
#ifdef OLD
    if (pdap->regs[reg].fdump)
    {
      char buf[256];
      struct buf_env be = {.buf = buf, .lbuf = sizeof buf};

      pdap->regs[reg].fdump(pdap->regs + reg,0,DAP_RO,DAP_SEND,&be);
      char *name = pdap->regs[reg].name;
      if (name && strncmp(name,"DAP_",4) == 0) 
        name += 4;
      if (!name) name = "???";
      printf("W%x: %-16s %08x %s\n",pdap->regs[reg].add,name,val,buf);
    }
#endif
  } else if (log) 
    putchar('\n');
  return r;
}

void flog(libusb_context *ctx, enum libusb_log_level level, const char *str)
{
  printf("%s",str);
}

int cmsis_transfer_config(struct t_cmsisprobe *cp,ushort cycles,ushort wait,ushort retry)
{
  struct t_libusb *plu = cp->plu;

  memset(plu->txbuf,0,plu->bufsize);
  int i = 0;
  plu->txbuf[i++] = ID_DAP_TRANSFER_CONFIGURE;
  plu->txbuf[i++] = cycles;
  i += set16(plu->txbuf + i,wait);
  i += set16(plu->txbuf + i,retry);
  int res = sendrec1(plu,i,__LINE__);
  if (res == LIBUSB_OK && plu->rxbuf[0] == ID_DAP_TRANSFER_CONFIGURE && plu->rxbuf[1] == DAP_OK)
    return res;
  else
    DAP_error("ID_DAP_SWJ_CLOCK",res,__LINE__);
  return res;
}

int cmsis_swd_config(struct t_cmsisprobe *cp,uchar mode)
{
  struct t_libusb *plu = cp->plu;

  memset(plu->txbuf,0,plu->bufsize);
  int i = 0;
  plu->txbuf[i++] = ID_DAP_SWD_CONFIGURE;
  plu->txbuf[i++] = mode;
  int res = sendrec1(plu,i,__LINE__);
  if (res == LIBUSB_OK && plu->rxbuf[0] == ID_DAP_SWD_CONFIGURE && plu->rxbuf[1] == DAP_OK)
    return res;
  else
    DAP_error("ID_DAP_SWJ_CLOCK",res,__LINE__);
  return res;
}

int cmsis_host_status(struct t_cmsisprobe *cp,uchar type,uchar status)
{
  struct t_libusb *plu = cp->plu;

  memset(plu->txbuf,0,plu->bufsize);
  int i = 0;
  plu->txbuf[i++] = ID_DAP_HOST_STATUS;
  plu->txbuf[i++] = type;
  plu->txbuf[i++] = status;
  int res = sendrec1(plu,i,__LINE__);
  if (res == LIBUSB_OK && plu->rxbuf[0] == ID_DAP_HOST_STATUS && plu->rxbuf[1] == DAP_OK)
    return res;
  else
    DAP_error("ID_DAP_SWJ_CLOCK",res,__LINE__);
  return res;
}

int cmsis_swj_clock(struct t_cmsisprobe *cp)
{
  struct t_libusb *plu = cp->plu;

  memset(plu->txbuf,0,plu->bufsize);
  int i = 0;
  plu->txbuf[i++] = ID_DAP_SWJ_CLOCK;
  i += set32(plu->txbuf + i,cp->speed);
  int res = sendrec1(plu,i,__LINE__);
  if (res == LIBUSB_OK && plu->rxbuf[0] == ID_DAP_SWJ_CLOCK && plu->rxbuf[1] == DAP_OK)
    return res;
  else
    DAP_error("ID_DAP_SWJ_CLOCK",res,__LINE__);
  return res;
}

int cmsis_swj_pins(struct t_cmsisprobe *cp,uchar output, uchar select,ushort wait)
{
  struct t_libusb *plu = cp->plu;

  memset(plu->txbuf,0,plu->bufsize);
  int i = 0;
  plu->txbuf[i++] = ID_DAP_SWJ_PINS;
  plu->txbuf[i++] = output;
  plu->txbuf[i++] = select;
  i += set32(plu->txbuf + i,wait);
  int res = sendrec1(plu,i,__LINE__);
  if (res == LIBUSB_OK && plu->rxbuf[0] == ID_DAP_SWJ_PINS)
    return res;
  else
    DAP_error("ID_DAP_SWJ_PINS",res,__LINE__);
  return res;
}

int cmsis_info(struct t_cmsisprobe *cp,uchar type)
{
  struct t_libusb *plu = cp->plu;

  memset(plu->txbuf,0,plu->bufsize);
  int i = 0;
  plu->txbuf[i++] = ID_DAP_INFO;
  plu->txbuf[i++] = type;
  int res = sendrec1(plu,i,__LINE__);
  if (res == LIBUSB_OK && plu->rxbuf[0] == ID_DAP_INFO)
    return res; 
  else
  {
    char msg[80];
    sprintf(msg,"cmsis_info type %02x",type);
    DAP_error(msg,res,__LINE__);
  }
  return res;
}

int cmsis_swj_sequence(struct t_cmsisprobe *cp,uchar bitcount,const uchar *s)
{
  struct t_libusb *plu = cp->plu;

  ENTRY("cmsis_swj_sequence");
  memset(plu->txbuf,0,plu->bufsize);
  int i = 0;
  plu->txbuf[i++] = ID_DAP_SWJ_SEQUENCE;
  plu->txbuf[i++] = bitcount;
  for (int j = 0; j < (bitcount + 7) / 8; j++)
    plu->txbuf[i++] = s[j];
  int res = sendrec1(plu,i,__LINE__);
  if (res == LIBUSB_OK && plu->rxbuf[0] == ID_DAP_SWJ_SEQUENCE && plu->rxbuf[1] == DAP_OK)
  {
    EXIT("cmsis_swj_sequence");
    return res;
  } else
    DAP_error("cmsis_swj_sequence",res,__LINE__);

  EXIT("cmsis_swj_sequence");
  return res;
}

int cmsis_connect(struct t_cmsisprobe *cp,uchar mode)
{
  struct t_libusb *plu = cp->plu;

  ENTRY("cmsis_connect");
  memset(plu->txbuf,0,plu->bufsize);
  int i = 0;
  plu->txbuf[i++] = ID_DAP_CONNECT;
  plu->txbuf[i++] = mode;
  int res = sendrec1(plu,i,__LINE__);
  if (res == LIBUSB_OK && plu->rxbuf[0] == ID_DAP_CONNECT && plu->rxbuf[1] == mode)
  {
    EXIT("cmsis_connect");
    return res;
  } else
    DAP_error("ID_DAP_CONNECT",res,__LINE__);
  EXIT("cmsis_connect");

  return res;
}

int cmsis_disconnect(struct t_cmsisprobe *cp)
{
  struct t_libusb *plu = cp->plu;

  ENTRY("cmsis_disconnect");
  memset(plu->txbuf,0,plu->bufsize);
  int i = 0;
  plu->txbuf[i++] = ID_DAP_DISCONNECT;
  int res = sendrec1(plu,i,__LINE__);
  if (res == LIBUSB_OK && plu->rxbuf[0] == ID_DAP_DISCONNECT && plu->rxbuf[1] == DAP_OK)
  {
    EXIT("cmsis_disconnect");
    return res;
  } else
    DAP_error("ID_DAP_DISCONNECT",res,__LINE__);
  EXIT("cmsis_disconnect");
  return res;
}

int cmsis_connect_single(struct t_cmsisprobe *cp)
{
  int s = get_count();
  int res;
  ENTRY("cmsis_connect_single");
  res = cmsis_disconnect(cp);
  CHKSEQ1(s + 2);
  res = cmsis_connect(cp,DAP_PORT_SWD);
  res = cmsis_swj_sequence(cp,swd_seq_jtag_to_swd_len,swd_seq_jtag_to_swd);
  CHKSEQ1(s + 8);
  res = cmsis_swj_clock(cp);
  CHKSEQ1(s + 8);
  uint dpidr;
  cmsis_que_cmd(cp,DAP_DP_DPIDR,DAP_RO,0,&dpidr,4);
  cmsis_que_flush(cp,1);
  logmsg("DPIDR %08x",dpidr);
  cmsis_clear_sticky_errors(cp);
  cmsis_que_flush(cp,1);
  
  EXIT("cmsis_connect_single");
  return res;
}

int cmsis_connect_multi(struct t_cmsisprobe *cp)
{
  ENTRYP("cmsis_connect_multi","c/r dapidx %d/%d",cp->c_dapidx,cp->r_dapidx);
  int s = get_count();
// [  0.270125   26] cmsis_dap.c: cmsis_dap_swd_switch_seq entry
  int res = cmsis_disconnect(cp);
  CHKSEQ1(s + 2);
  res = cmsis_connect(cp,DAP_PORT_SWD);
  CHKSEQ1(s + 4);
  res = cmsis_swj_sequence(cp,swd_seq_jtag_to_dormant_len,swd_seq_jtag_to_dormant);
  CHKSEQ1(s + 6);
  res = cmsis_swj_clock(cp);
  CHKSEQ1(s + 8);
// [  0.385071   34] cmsis_dap.c: cmsis_dap_swd_switch_seq exit
// [  0.385119   34] cmsis_dap.c: cmsis_dap_swd_switch_seq entry
  res = cmsis_disconnect(cp);
  CHKSEQ1(s + 10);
  res = cmsis_connect(cp,DAP_PORT_SWD);
  CHKSEQ1(s + 12);
  res = cmsis_swj_sequence(cp,swd_seq_dormant_to_swd_len,swd_seq_dormant_to_swd);
  CHKSEQ1(s + 14);
  res = cmsis_swj_clock(cp); 
  CHKSEQ1(s + 16);
// [  0.505157   42] cmsis_dap.c: cmsis_dap_swd_switch_seq exit
  res = cmsis_swj_sequence(cp,swd_seq_line_reset_len,swd_seq_line_reset);
  res = cmsis_swj_clock(cp); 
  CHKSEQ1(s + 20);
  EXIT("cmsis_connect_multi");
  return res;
}

// build SWD header byte
// bit  P
//  0        1 Start
//  1   *    x APnDP
//  2   *    x RnW
//  3   *    A
//  4   *    A
//  5        X Parity
//  6        0 Stop
//  7        1 Park

uchar swd_cmd(struct t_dap *pdap,int regid,int op)
{
  // id 4 parity bits
  int a = (pdap->regs[regid].add >> 2) & 3;
  int id = (op << 1) | pdap->regs[regid].ty | ((a & 1) << 2) | ((a >> 1) << 3);
  int p = parity[id];
/*
  printf("SWD %s add %x a %d op %d ty %d id %x p %d\n",
         s_dap.regs[regid].name,
         s_dap.regs[regid].add,a,op,s_dap.regs[regid].ty, id,p);
*/
  return 0x81 | (id << 1) | (p << 5);
}

uchar dap_transfer_cmd(struct t_dap *pdap,int regid,int op)
{
  uchar a = (pdap->regs[regid].add >> 2) & 3;
  uchar r = pdap->regs[regid].ty | (op << 1) | (a << 2) ;
  return r;
}

int cmsis_swd_write_reg(struct t_cmsisprobe *cp,uchar cmd,uint val)
{
  struct t_libusb *plu = cp->plu;

  memset(plu->txbuf,0,plu->bufsize);
  int i = 0;
  plu->txbuf[i++] = ID_DAP_SWD_SEQUENCE;
  plu->txbuf[i++] = 3;
  plu->txbuf[i++] = 8;
  plu->txbuf[i++] = cmd;
  plu->txbuf[i++] = 0x85;
  plu->txbuf[i++] = 33;
  i += set32p(plu->txbuf + i,val);
  int res = sendrec1(plu,i,__LINE__);
  if (res == LIBUSB_OK && plu->rxbuf[0] == ID_DAP_SWD_SEQUENCE && plu->rxbuf[1] == DAP_OK)
  {
    return res;
  } else
    DAP_error("ID_DAP_SWD_SEQUENCE",res,__LINE__);
  return res;
}

static struct t_cmsisbuf *newbuf(struct t_libusb *plu)
{
  struct t_cmsisbuf *p = calloc(sizeof(*p),1);
  p->txbuf = p->pbuf = calloc(sizeof(p->txbuf[0]),plu->bufsize);
  return p;
}

void dump_que(struct t_cmsisprobe *cp)
{
  printf("dump_que h %p t %p count %d\n",cp->bufhead,cp->buftail,cp->bufcount);
  struct t_cmsisbuf *pbt = cp->buftail;

  int i = 0;
  while(pbt)
  {
    printf("%2d at %p next %p prev %p\n",i++,pbt,pbt->pnext,pbt->pprev);
    pbt = pbt->pnext;
  }
  if (cp->bufcount != i)
    error("incorrect bufcount %d vs %d",cp->bufcount,i);
}

int cmsis_que_read(struct t_cmsisprobe *cp,int upto)
{
  int res,rec;
  struct t_libusb *plu = cp->plu;
  struct t_cmsisbuf *pbt = cp->buftail;
  struct t_dap *pdap = cp->pmdap->daps + cp->c_dapidx;

  if (plu->log) //  && cp->buftail != cp->bufhead)
    dump_que(cp);

  while(pbt && cp->bufcount > upto)
  {
    res = dap_receive(plu,&rec,__LINE__);
    if (res == LIBUSB_OK)
    {
      uchar op    = pbt->txbuf[0];
      uchar count = pbt->txbuf[2];
      ushort count2 = get16(pbt->txbuf + 2);
#ifdef D
      printf("cmsis_que_read op %d/%d ",plu->rxbuf[0],op);
      if (op == ID_DAP_TRANSFER_BLOCK)
        printf("count2 %d/%d\n",get16(plu->rxbuf + 1),count2);
      else
        printf("count %d/%d\n",plu->rxbuf[1],count);
#endif
      if (plu->rxbuf[0] == op)
      {
        if (op == ID_DAP_TRANSFER && plu->rxbuf[1] == count)
        {
          int pt = 3;
          int pr = 3;
          int j = 0;
          for (int i = 0; i < count; i++)
          {
/*
            printf("pt %2d pr %2d j %d op %02x R %d\n",
                   pt,pr,j,plu->txbuf[pt],(plu->txbuf[pt] >> 1) & 1);
*/
            if ((pbt->txbuf[pt] >> 1) & 1) // R
            {
              uint v = get32(plu->rxbuf + pr);
//            printf("R v %08x ret %p\n",v,pbt->retval[j]);
              pr += 4;
              if (pbt->retval[j].pui)
              {
// size may containds 0x80 flag to handle unalined address
                switch(pbt->retvals[j] & 0xf)
                {
                  case 4:
                    *(pbt->retval[j].pui) = v; break;
                  case 2:
//                  printf("set short v %0x flag %02x -> %p\n",v,pbt->retvals[j],pbt->retval[j].pus);
                    if (pbt->retvals[j] & 0x80)
                      *(pbt->retval[j].pus) = v >> 16; 
                    else
                      *(pbt->retval[j].pus) = v; 
                    break;
                  case 1:
                    *(pbt->retval[j].puc) = v; break; // TODO verify
                  default:
                    error("unexpected @%d",__LINE__);
                }
              }    
              dap_setreg(pdap,pbt->regid[j],v,plu->log ? OUT_MIN : OUT_NONE,DAP_RO,DAP_RECEIVE,plu->count);
              j++;
            } else
              pt += 4;
            pt++;
          }
        } else if (op == ID_DAP_TRANSFER_BLOCK && get16(plu->rxbuf + 1) == count2)
        {
          int k = 4;
          for (int i = 0; i < count2; i++)
          {
            uint v = get32(plu->rxbuf + k);
            if (pbt->retval[0].pui)
              *(pbt->retval[0].pui + i) = v; 
//          printf("%p: %08x\n",pbt->retval[0].pui + i,v);
            k += 4;
          }
        } else
          error("unexpected op %02x count %d rxbuf[1] %d count2 %d @%d",
                op,count,plu->rxbuf[1],count2,__LINE__);
// reset buffer
        if (pbt == cp->bufhead) // last buffer ?
        {                       // we leave it on que in empty state
          pbt->pbuf = pbt->txbuf;
          return res;           // normal while/function exit
        } else
        {
          cp->buftail = pbt->pnext;
          cp->bufcount--;
          if (cp->bufcount < 0)
            error("ring buffer underflow @%d",__LINE__);
          pbt->pnext->pprev = 0;
          free(pbt->txbuf);
          free(pbt);
          pbt = cp->buftail;
        }
      } else
        error("wrong reply op (send %02x rec %02x)",op,plu->rxbuf[0]);
    } else
    {
      DAP_error("ID_DAP_TRANSFER",res,__LINE__);
      return res;
    }
  }
  return res;
}

// flush input buffer sending it to the USB link (if empty no effect)
// and 
// receive == 1 handle the replay (may be more than one)
// receive == 0 enque the send buffer for future reference and switch to a new empty buffer
//
// ouput buffer can be queued inside t_cmsisbuf inserted in a double linked queue
// input buffwe is single and put in t_libusb struct (this may seem a little strange and may be will be fixed)

int cmsis_que_flush(struct t_cmsisprobe *cp,int receive)
{
  struct t_libusb *plu = cp->plu;
  struct t_cmsisbuf *pbh = cp->bufhead;

//ENTRY("cmsis_que_flush");

  if (cp->plu->log) // && receive != 1)
    logmsg("cmsis_que_flush receive %d bufcount/limit %d/%d",
           receive,cp->bufcount,cp->buflimit);
  if (pbh == 0)
    error("null bufhead @%d",__LINE__);
  if (pbh->pbuf == pbh->txbuf || 
      (pbh->pbuf - pbh->txbuf == 3 && pbh->txbuf[2] == 0))
  { // nothing to do
    // reset buffer
    pbh->pbuf = pbh->txbuf;
//  EXIT("cmsis_que_flush");
    return LIBUSB_OK;
  } 
  uint used = pbh->pbuf - pbh->txbuf;
  if (used > plu->bufsize)
    error("txbuf overflow @%d",__LINE__);
  
  int res = dap_send(plu,pbh->txbuf,used,__LINE__);
  if (receive == 0)
  {
    if (cp->bufcount >= cp->buflimit)
    {
      if (cp->plu->log)
        logmsg("cmsis_que_flush.1 receive %d bufcount/limit %d/%d",
               receive,cp->bufcount,cp->buflimit);
      res = cmsis_que_read(cp,cp->buflimit - 1);
      if (cp->plu->log)
        logmsg("cmsis_que_flush.2 receive %d bufcount/limit %d/%d",
               receive,cp->bufcount,cp->buflimit);
// we keep the last buffer 
      if (cp->bufcount == 1)
        return res;
    } 
// queue pbh and add a new empty buffer on head of queue
    struct t_cmsisbuf *pb = newbuf(plu);
    pb->pnext   = 0;
    pb->pprev   = pbh;
    if (cp->bufhead == cp->buftail)
      cp->buftail = pbh;
    cp->bufhead = pb;
    pbh->pnext  = pb;
    cp->bufcount++;
    if (cp->bufcount > cp->buflimit)
      error("buffer overflow @%d",__LINE__);
    return res;
  }
  res = cmsis_que_read(cp,0);
//EXIT("cmsis_que_flush");
  return res;
}

// read/write via transfer lock
int cmsis_que_tb(struct t_cmsisprobe *cp,int regid,int op,void *rval,uint size)
{
  int res = LIBUSB_OK;
  struct t_libusb   *plu = cp->plu;
  struct t_cmsisbuf *pbh = cp->bufhead;
  struct t_dap *pdap = cp->pmdap->daps + cp->c_dapidx;
  union ptr p;

  ENTRY("cmsis_que_tb");
  p.pv = rval;
  int maxl = op == DAP_RO ? 14 : 14;
  while (size)
  {
//  printf("cmsis_que_tb size %d\n",size);
    int l = size > maxl ? maxl : size;
    if (l <= 4)
    {
      pbh->pbuf    = pbh->txbuf;
      p.pui += size - l;
      for (int j = 0; j < l; j++)
        res = cmsis_que_cmd(cp,regid,op,0,p.pui++,4);
      if (op == DAP_RO)
        res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4);  
    } else
    {
      memset(pbh->txbuf,0,plu->bufsize);
      pbh->pbuf    = pbh->txbuf;
      pbh->pbuf    = pbh->txbuf;
      *pbh->pbuf++ = ID_DAP_TRANSFER_BLOCK;
      *pbh->pbuf++ = 0; // DAP index unused
      pbh->pbuf   += set16(pbh->pbuf,l);

      *pbh->pbuf++ = dap_transfer_cmd(pdap,regid,op);
      if (op == DAP_WO)
      {
        for (int i = 0; i < l; i++)
        {
//        printf("W i %2d v %08x\n",i,p.pui[i]);
          pbh->pbuf += set32(pbh->pbuf,p.pui[i]);
        }
      }
      pbh->nextrv = 0;
      pbh->retval[pbh->nextrv].pv = p.pv;
      pbh->retvals[pbh->nextrv] = size;
      pbh->regid[pbh->nextrv++] = regid;
      p.pui += l;
    }
    size -= l;
    if (size)
    {
      res = cmsis_que_flush(cp,0);
      pbh = cp->bufhead;
    }
  }
  EXIT("cmsis_que_tb");
  return res;
}

// size may containds 0x80 flag to handle unalined address

int cmsis_que_cmd(struct t_cmsisprobe *cp,int regid,int op,uint val,void *rval,uchar size)
{
  int res = LIBUSB_OK;
  struct t_libusb   *plu = cp->plu;
  struct t_cmsisbuf *pbh = cp->bufhead;
  struct t_dap *pdap = cp->pmdap->daps + cp->c_dapidx;
  union ptr p;

  p.pv = rval;

// size may containds 0x80 flag to handle unalined address
  uchar s = size & 0xf;
  if (!(s == 1 || s == 2 || s == 4))
    error("unexpected @%d",__LINE__);

  if (pbh == 0)
    error("null bufhead @%d",__LINE__);
  if (pbh->pbuf == 0) 
    error("null pbuf @%d",__LINE__);
  uint s_used = pbh->pbuf - pbh->txbuf;
  uint s_avail = plu->bufsize - s_used;
  uint s_req = op == DAP_WO ? 6 : 2;
  uint r_req = op == DAP_WO ? 0 : 4;
  uint r_avail = plu->bufsize - cp->r_used;
/*
  if (plu->log)
    printf("cmsis_que_cmd used %d/%d avail %d/%d required %d/%d nextrv %d\n",
           s_used,cp->r_used,
           s_avail,r_avail,
           s_req,r_req,
           pbh->nextrv);
*/
  cp->flushed = 0;
  if (s_avail < s_req || r_avail < r_req)
  {
    res = cmsis_que_flush(cp,0);
    pbh = cp->bufhead;
    cp->flushed = 1;
  }
  if (pbh->pbuf == pbh->txbuf)
  {
    memset(pbh->txbuf,0,plu->bufsize);
    pbh->pbuf    = pbh->txbuf;
    *pbh->pbuf++ = ID_DAP_TRANSFER;
    *pbh->pbuf++ = 0; // DAP index unused
    *pbh->pbuf++ = 0; // count to fix later
    pbh->nextrv  = 0; // # returned value pointers 
    cp->r_used   = 3; // receive size
  }
  if (op == DAP_RO) // check space for read pointer
    if (pbh->nextrv >= NRETVAL) 
      error("retval overflow @%d",__LINE__);
  pbh->txbuf[2]++;
  uchar a       = pdap->regs[regid].add;
  uchar ty      = pdap->regs[regid].ty;
  uint select;
  if (ty == DAP_AP)
  {
    int valid = dap_getval(pdap,DAP_DP_SELECT,&select);
    if (valid)
    {
      int apbanksel = (select & DAP_DP_SELECT_APBANKSEL_MASK) >> DAP_DP_SELECT_APBANKSEL_SHIFT;
      if (a >> 4 != apbanksel)
        logmsg("cmsis_que_cmd dap idx %d wrong apbanksel a %x apbanksel %x DAP_DP_SELECT %08x regid %d %s",
               pdap->idx,a,apbanksel,select,regid,pdap->regs[regid].name);
    } else
      logmsg("DAP_DP_SELECT invalid");
  }
  *pbh->pbuf++ = dap_transfer_cmd(pdap,regid,op);
  if (op == DAP_WO)
  {
    pbh->pbuf += set32(pbh->pbuf,val);
    if (regid == DAP_DP_SELECT && val == select)
      logmsg("cmsis_que_cmd warning: set DAP_DP_SELECT to same value %08x",val);
    if (regid == DAP_MEMAP_CSW)
    {
      uint csw;
      int valid = dap_getval(pdap,DAP_MEMAP_CSW,&csw);
      if (valid && csw == val)
        logmsg("cmsis_que_cmd warning: set DAP_DP_CSW to same value %08x",val);
    }
    if (regid == DAP_MEMAP_TAR)
    {
      uint tar;
      int valid = dap_getval(pdap,DAP_MEMAP_TAR,&tar);
      if (valid && tar == val)
        logmsg("cmsis_que_cmd warning: set DAP_DP_TAR to same value %08x",val);
    }
    dap_setreg(pdap,regid,val,plu->log ? OUT_MIN : OUT_NONE,DAP_WO,DAP_SEND,plu->count);
  } else if (op == DAP_RO)
  {
    cp->r_used += 4;
    pbh->retval[pbh->nextrv].pv = p.pv;
    pbh->retvals[pbh->nextrv] = size;
    pbh->regid[pbh->nextrv++] = regid;
  } else 
    error("cmsis_que_cmd wrong type @%d",__LINE__);
// check for TAR autoincrement
  if (regid == DAP_MEMAP_DRW)
  {
    uint csw = pdap->regs[DAP_MEMAP_CSW].vals;
    uint addrinc = FIELD(csw,CSW,ADDRINC);
    uint size = FIELD(csw,CSW,SIZE);
    if (addrinc == 1)
    {
      if (cp->plu->log)
        logmsg("cmsis_que_cmd: autoincrement TAR %08x -> %08x",
               pdap->regs[DAP_MEMAP_TAR].vals,
               pdap->regs[DAP_MEMAP_TAR].vals + (1 << size));
      pdap->regs[DAP_MEMAP_TAR].vals += (1 << size);
    } else if (addrinc == 2) // packed
    {
      if (cp->plu->log)
        logmsg("cmsis_que_cmd: autoincrement TAR %08x -> %08x",
               pdap->regs[DAP_MEMAP_TAR].vals,
               pdap->regs[DAP_MEMAP_TAR].vals + (1 << size));
      pdap->regs[DAP_MEMAP_TAR].vals += (1 << size);
    } else if (addrinc != 0)
      error("unexpected @%d",__LINE__); 
  }
  return res;
}

int cmsis_clear_sticky_errors(struct t_cmsisprobe *cp)
{
// adi_v5_swd.c
//  swd->write_reg(swd_cmd(false, false, DP_ABORT),
//              STKCMPCLR | STKERRCLR | WDERRCLR | ORUNERRCLR, 0);
  return cmsis_que_cmd(cp,DAP_DP_ABORT,DAP_WO,
                       ABORT_STKCMPCLR | ABORT_STKERRCLR | ABORT_WDERRCLR | ABORT_ORUNERRCLR,0,4);
}

// [4.736304 1182] arm_dap.c: dap_cleanup_all 
int cmsis_cleanup(struct t_cmsisprobe *cp)
{
  int res = LIBUSB_OK;
  res = cmsis_disconnect(cp);
  res = cmsis_connect(cp,DAP_PORT_SWD);
  if (cp->ndap == 1)
    res = cmsis_swj_sequence(cp,swd_seq_swd_to_jtag_len,swd_seq_swd_to_jtag);
  else
    res = cmsis_swj_sequence(cp,swd_seq_swd_to_dormant_len,swd_seq_swd_to_dormant);
  res = cmsis_swj_clock(cp); 
  res = cmsis_disconnect(cp);
  res = cmsis_host_status(cp,1,0);
  res = cmsis_host_status(cp,0,0);

  return res;
}

int cmsis_multi_select(struct t_cmsisprobe *cp)
{
  struct t_dap *pdap = cp->pmdap->daps + cp->c_dapidx;

  ENTRYP("cmsis_multi_select","c/r dapidx %d/%d",cp->c_dapidx,cp->r_dapidx);
  if (cp->c_dapidx == cp->r_dapidx)
    logmsg("cmsis_multi_select call useless");
  cp->c_dapidx = cp->r_dapidx;
  if (cp->c_dapidx < 0 || cp->c_dapidx >= cp->ndap)
    error("wrong dapidx %d",cp->c_dapidx);
  int res = cmsis_swj_sequence(cp,swd_seq_line_reset_len,swd_seq_line_reset);
  res = cmsis_swj_clock(cp); 
//
// sequence is almost an equal copy of cmsis_dap_init_all (differnt ABORT flags)
// TODO: check to unify
//
  uchar cmd = swd_cmd(pdap,DAP_DP_TARGETSEL,DAP_WO);
  res = cmsis_swd_write_reg(cp,cmd,cp->pmdap->daps[cp->c_dapidx].targetsel); // use constant form rp2040 datasheet
  uint dpidr,dlpidr;
  res = cmsis_que_cmd(cp,DAP_DP_DPIDR,DAP_RO,0,&dpidr,4);
  res = cmsis_que_cmd(cp,DAP_DP_ABORT,DAP_WO,ABORT_ORUNERRCLR,0,4);
  res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,3,0,4); // auto ?
  res = cmsis_que_cmd(cp,DAP_DP_DLPIDR,DAP_RO,0,&dlpidr,4);
  res = cmsis_que_flush(cp,1);
  char buf[500];
//dap_dumpreg(&s_dap,DAP_DP_DLPIDR,buf,OUT_OP_MASK);
  dap_dumpreg(pdap,DAP_DP_DLPIDR,buf,OUT_OP_MASK);
  if (cp->plu->log)
    logmsg("cmsis_multi_select: dpidr %08x dlpidr %08x %s",dpidr,dlpidr,buf);
  EXIT("cmsis_multi_select");
  return res;
}

// dap_dp_init
int cmsis_dp_init(struct t_cmsisprobe *cp)
{
  int s = get_count();
  int res = LIBUSB_OK;

  ENTRY("cmsis_dp_init");
  cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,0,0,4); // auto ?
  cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_WO,
                CTRL_STAT_CDBGPWRUPREQ_MASK | 
                CTRL_STAT_CSYSPWRUPREQ_MASK | 
                CTRL_STAT_STICKYERR_MASK,0,4);
  cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_RO,0,0,4);
  cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_WO,
                CTRL_STAT_CDBGPWRUPREQ_MASK | 
                CTRL_STAT_CSYSPWRUPREQ_MASK,0,4); 
  uint ctrl_stat;
  cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_RO,0,&ctrl_stat,4);
  cmsis_que_flush(cp,1);
  logmsg("dp_init: ctrl_stat %08x",ctrl_stat);
  CHKSEQ1(s + 26);
  while(1)
  {
    cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_RO,0,&ctrl_stat,4);
    cmsis_que_flush(cp,1);
    if (ctrl_stat & CTRL_STAT_CSYSPWRUPACK_MASK)
      break;
  }
  cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_RO,0,0,4);
// CTRL_STAT_ORUNDETECT is set but after removed in cmsis_dap: kludge(accrocchio ?)
  cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_WO,
                CTRL_STAT_CDBGPWRUPREQ_MASK | CTRL_STAT_CSYSPWRUPREQ_MASK
               /* | CTRL_STAT_ORUNDETECT_MASK */,0,4);
  cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_RO,0,0,4);
  res = cmsis_que_flush(cp,1);
  CHKSEQ1(s + 6);
  EXIT("cmsis_dp_init");
  return res;
}

int cmsis_dap_init_all(struct t_cmsisprobe *cp)
{

  struct t_dap *pdap = cp->pmdap->daps + cp->c_dapidx;
  ENTRYP("cmis_dap_init_all","c/r dapidx %d/%d",cp->c_dapidx,cp->r_dapidx);
  if (cp->c_dapidx == cp->r_dapidx)
    logmsg("cmsis_multi_select call useless");
  cp->c_dapidx = cp->r_dapidx;
  if (cp->c_dapidx < 0 || cp->c_dapidx >= cp->ndap)
    error("wrong dapidx %d",cp->c_dapidx);

  int s = get_count();
  int res;
  if (cp->ndap > 1)
  {
    res = cmsis_connect_multi(cp);
    CHKSEQ1(s + 20);

    uchar cmd = swd_cmd(pdap,DAP_DP_TARGETSEL,DAP_WO);
    res = cmsis_swd_write_reg(cp,cmd,cp->pmdap->daps[cp->c_dapidx].targetsel); // use constant form rp2040 datasheet
    CHKSEQ1(s + 22);

    uint dpidr,dlpidr;
    cmsis_que_cmd(cp,DAP_DP_DPIDR,DAP_RO,0,&dpidr,4);
    cmsis_clear_sticky_errors(cp);
    cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,3,0,4); // auto ?
    cmsis_que_cmd(cp,DAP_DP_DLPIDR,DAP_RO,0,&dlpidr,4);
    cmsis_que_flush(cp,1);
    logmsg("dpidr %08x dlpidr %08x",dpidr,dlpidr);
  } else
    res = cmsis_connect_single(cp);
  cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,0,0,4); // auto ?
  cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_WO,
                CTRL_STAT_CDBGPWRUPREQ_MASK | 
                CTRL_STAT_CSYSPWRUPREQ_MASK | 
                CTRL_STAT_STICKYERR_MASK,0,4);
  cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_RO,0,0,4);
  cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_WO,
                CTRL_STAT_CDBGPWRUPREQ_MASK | 
                CTRL_STAT_CSYSPWRUPREQ_MASK,0,4); 
  uint ctrl_stat;
  cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_RO,0,&ctrl_stat,4);
  cmsis_que_flush(cp,1);
  logmsg("init_dap: ctrl_stat %08x",ctrl_stat);
  CHKSEQ1(s + 26);
  while(1)
  {
    cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_RO,0,&ctrl_stat,4);
    cmsis_que_flush(cp,1);
    if (ctrl_stat & CTRL_STAT_CSYSPWRUPACK_MASK)
      break;
  }
  cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_RO,0,0,4);
// CTRL_STAT_ORUNDETECT is set but after removed in cmsis_dap: kludge(accrocchio ?)
  cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_WO,
                CTRL_STAT_CDBGPWRUPREQ_MASK | CTRL_STAT_CSYSPWRUPREQ_MASK
               /* | CTRL_STAT_ORUNDETECT_MASK */,0,4);
  cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_RO,0,0,4);
  res = cmsis_que_flush(cp,1);
  CHKSEQ1(s + 30);
  EXIT("cmsis_dap_init_all");

  return res;
}

int cmsis_find_get_ap(struct t_cmsisprobe *cp)
{
  int res;
  ENTRY("cmsis_find_get_ap");
  if (cp->ndap > 1)
    res = cmsis_multi_select(cp);
  for (int i = 0; i < 1; i++) // ??
  {
    struct t_dap *pdap = cp->pmdap->daps + cp->r_dapidx;
    uint select = cp->ndap == 1 ? 0xf0 : 0xf3; // TODO fix
    pdap->dp_banksel = cp->ndap == 1 ? 0x0 : 0x3; // TODO fix
    res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,i << 24 | select,0,4); // TODO fix 
    uint apidr;
    res = cmsis_que_cmd(cp,DAP_AP_IDR,DAP_RO,0,&apidr,4);
    res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4);
    res = cmsis_que_flush(cp,1);
    logmsg("ap %d apidr %08x",i,apidr);
  }
  EXIT("cmsis_find_get_ap");
  return res;
}

int cmsis_write_32_a(struct t_cmsisprobe *cp,uint add,uint val,int fl)
{
  int res,csw_val;

  struct t_dap *pdap = cp->pmdap->daps + cp->r_dapidx;
/**
  if (pdap->use_packet)      // use packet increment
    csw_val = (1 << CSW_DBGSWENABLE_SHIFT) | 
              (2 << CSW_PROT_SHIFT) | 
              (2 << CSW_CACHE_SHIFT) | 
               CSW_SIZE32 | CSW_PADDRINC;
  else
    csw_val = (1 << CSW_DBGSWENABLE_SHIFT) | 
              (2 << CSW_PROT_SHIFT) | 
              (2 << CSW_CACHE_SHIFT) | 
               CSW_SIZE32 | CSW_SADDRINC;
**/
  csw_val = (1 << CSW_DBGSWENABLE_SHIFT) | 
            (2 << CSW_PROT_SHIFT) | 
            (2 << CSW_CACHE_SHIFT) | 
             CSW_SIZE32 | CSW_SADDRINC;
  ENTRYP("cmsis_write_32_a","add %08x val %08x fl %x",add,val,fl);
  if (fl & SET_AUTO)
  {
    int sel = pdap->regs[DAP_DP_SELECT].vals;
    int tar = pdap->regs[DAP_MEMAP_TAR].vals;
    int csw = pdap->regs[DAP_MEMAP_CSW].vals;

/* TODO check why ...
    if (csw != csw_val)
      fl |= SET_CSW;
*/
    if (tar != add)
      fl |= SET_TAR;
    if (fl & (SET_CSW | SET_TAR))
      if ((sel & ~DAP_DP_SELECT_DPBANKSEL_MASK) != pdap->dp_banksel) 
        fl |= SET_SEL;
    if ((fl & SET_SEL) || sel != (0x10 | pdap->dp_banksel)) // TODO fix
        fl |= SET_SEL2;
    if (cp->plu->log)
      printf("cmsis_write_32_a auto sel %x tar %x csw %x fl %x\n",sel,tar,csw,fl);
  }
  if (fl & SET_SEL)
    res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,pdap->dp_banksel,0,4);
  if (fl & SET_CSW)
    res = cmsis_que_cmd(cp,DAP_MEMAP_CSW,DAP_WO,csw_val,0,4); // TODO fix 
  if (fl & SET_TAR)
    res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,add,0,4);  
  if (fl & SET_SEL2)
    res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,0x10 | pdap->dp_banksel,0,4);
  if (fl & USE_BD2)
    res = cmsis_que_cmd(cp,DAP_MEMAP_BD2,DAP_WO,val,0,4);
  else
    res = cmsis_que_cmd(cp,DAP_MEMAP_BD0,DAP_WO,val,0,4);
  res = cmsis_que_flush(cp,1);
  EXIT("cmsis_write_32_a");
  return res;
}

// arm_adi_v5.c: mem_ap_setup_transfer ENTRY 
int cmsis_ap_setup_t(struct t_cmsisprobe *cp,uint csw,uint tar,int fl)
{
  int res = LIBUSB_OK;

  logmsg("cmsis_ap_setup_t csw %08x tar %08x",csw,tar);
  if (fl & SET_CSW)
    res = cmsis_que_cmd(cp,DAP_MEMAP_CSW,DAP_WO,csw,0,4);  
  if (fl & SET_TAR)
    res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,tar,0,4);  

  return res;
}

// arm_adi_v5.c: mem_ap_write_u32
int cmsis_write_u32(struct t_cmsisprobe *cp,uint add,uint val,int fl)
{
  int res = LIBUSB_OK;

  if (cp->plu->log)
    logmsg("cmsis_write_u32 %08x -> %08x",add,val);
//mem_ap_setup_transfer
  uint csw = CSW_SIZE32 | (0 & CSW_ADDRINC_MASK);
  res = cmsis_ap_setup_t(cp,csw,DHCSR_ADD,fl);
  int id = (fl >> 4) & 3;
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD0 + id,DAP_WO,val,0,4);  
// NO flush !!!
  return res;
}

// special case of mem_ap_write 
// mem_ap_write_u32 with 2 register in TAR range 
// TODO very difficult case to FIX
int cmsis_write_mem32_x(struct t_cmsisprobe *cp,uint v0,uint v1)
{
  int res;

  ENTRYP("cmsis_write_mem32_x","v0 %x v1 %x",v0,v1);
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD2,DAP_WO,v0,0,4);  
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD3,DAP_WO,v1,0,4);  
  res = cmsis_que_flush(cp,1);
  EXIT("cmsis_write_mem32_x");

  return res;
}

int cmsis_write_mem32(struct t_cmsisprobe *cp,uint add,uint count,uint *mem,int fl)
{
  int res,csw_val;

  struct t_dap *pdap = cp->pmdap->daps + cp->r_dapidx;
  if (pdap->use_packet)      // use packet increment
    csw_val = (1 << CSW_DBGSWENABLE_SHIFT) | 
              (2 << CSW_PROT_SHIFT) | 
              (2 << CSW_CACHE_SHIFT) | 
               CSW_SIZE32 | CSW_PADDRINC;
  else
    csw_val = (1 << CSW_DBGSWENABLE_SHIFT) | 
              (2 << CSW_PROT_SHIFT) | 
              (2 << CSW_CACHE_SHIFT) | 
               CSW_SIZE32 | CSW_SADDRINC;

  ENTRYP("cmsis_write_mem32","add %08x count %d fl %x mem[0] %08x",add,count,fl,mem[0]);
  if (fl & SET_AUTO)
  {
    int sel = pdap->regs[DAP_DP_SELECT].vals;
    int tar = pdap->regs[DAP_MEMAP_TAR].vals;
    int csw = pdap->regs[DAP_MEMAP_CSW].vals;
    if (csw != csw_val)
      fl |= SET_CSW;
    if (tar != add)
      fl |= SET_TAR;
    if (fl & (SET_CSW | SET_TAR))
      if ((sel & ~DAP_DP_SELECT_DPBANKSEL_MASK) != pdap->dp_banksel) 
        fl |= SET_SEL;
    if (!(fl & (USE_BD1 | USE_BD2 | USE_BD3)) &&
        (sel != 0 && !(fl & SET_SEL)))
      fl |= SET_SEL2;
    if (cp->plu->log)
      printf("cmsis_write_mem32 auto sel %x tar %x csw %x fl %x\n",sel,tar,csw,fl);
  }
  if (fl & SET_SEL)
  {
    int sel = pdap->dp_banksel; // TODO verify
    res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,sel,0,4);
  }
  if (fl & SET_CSW)
    res = cmsis_que_cmd(cp,DAP_MEMAP_CSW,DAP_WO,csw_val,0,4); // TODO fix 
  if (fl & (USE_BD1 | USE_BD2 | USE_BD3))
  {
    if (count != 1)
      error("unsupported yet");
    if (fl & SET_TAR)
    {
      if (fl & USE_BD1)
        add -= 4;
      else if (fl & USE_BD2)
        add -= 8;
      else if (fl & USE_BD3)
        add -= 12;
      res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,add,0,4);  
      res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,0x13,0,4);
      if (fl & USE_BD1)
        res = cmsis_que_cmd(cp,DAP_MEMAP_BD1,DAP_WO,mem[0],0,4);  
      else if (fl & USE_BD2)
        res = cmsis_que_cmd(cp,DAP_MEMAP_BD2,DAP_WO,mem[0],0,4);  
      else if (fl & USE_BD3)
        res = cmsis_que_cmd(cp,DAP_MEMAP_BD3,DAP_WO,mem[0],0,4);  
    }
  } else
  {
    if (fl & SET_TAR)
      res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,add,0,4);  
//  printf("cmsis_write_mem32 now sel %x\n",pdap->regs[DAP_DP_SELECT].vals);
// DRW requires SELECT == 0
    if (fl & SET_SEL2)
      res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,0x00,0,4);
    int sf = 0;
    for (int i = 0; i < count; i++)
    {
      res = cmsis_que_cmd(cp,DAP_MEMAP_DRW,DAP_WO,mem[i],0,4);  
      if (cp->flushed) 
      {
//      printf("flushed i %d\n",i);
        sf = i;
      }
      if (sf && i - sf >= 4)
      {
        res = cmsis_que_tb(cp,DAP_MEMAP_DRW,DAP_WO,mem + sf,count - sf); 
        break;
      }
    }
  }
  res = cmsis_que_flush(cp,1);
  EXIT("cmsis_write_mem32");
  return res;
}

int cmsis_write_mem16(struct t_cmsisprobe *cp,uint add,uint count,ushort *mem,int fl)
{
  if (cp->plu->log)
    logmsg("cmsis_write_mem16 %08x -> %08x fl %x",add,mem[0],fl);
  int csw_val = (1 << CSW_DBGSWENABLE_SHIFT) | 
                (2 << CSW_PROT_SHIFT) | 
                (2 << CSW_CACHE_SHIFT) | 
                CSW_SIZE16 | CSW_SADDRINC;
  if (count > 1)
    error("count > 1 in cmsis_write_mem16 @%d",__LINE__);
  int res;
  if (fl & SET_AUTO)
  {
    struct t_dap *pdap = cp->pmdap->daps + cp->r_dapidx;
    int sel = pdap->regs[DAP_DP_SELECT].vals;
    int tar = pdap->regs[DAP_MEMAP_TAR].vals;
    int csw = pdap->regs[DAP_MEMAP_CSW].vals;
    if (csw != csw_val)
      fl |= SET_CSW;
    if (tar != add)
      fl |= SET_TAR;
    if (fl & (SET_CSW | SET_TAR))
      if (sel != 0x3) // TODO fix
        fl |= SET_SEL;
  }
  if (fl & SET_CSW)
    res = cmsis_que_cmd(cp,DAP_MEMAP_CSW,DAP_WO,csw_val,0,4);  
  if (fl & SET_TAR)
    res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,add,0,4);  
  if (add & 2)
    res = cmsis_que_cmd(cp,DAP_MEMAP_DRW,DAP_WO,mem[0] << 16,0,2);  
  else
    res = cmsis_que_cmd(cp,DAP_MEMAP_DRW,DAP_WO,mem[0],0,2);  
  res = cmsis_que_flush(cp,1);
  return res;
}

// arm_adi_v5.c: mem_ap_read
int cmsis_read_mem32(struct t_cmsisprobe *cp,uint add,uint count,uint *mem,int fl)
{
  int res;
  int csw_val;
  struct t_dap *pdap = cp->pmdap->daps + cp->r_dapidx;

  if (pdap->use_packet)      // use packet increment
    csw_val = (1 << CSW_DBGSWENABLE_SHIFT) | 
              (2 << CSW_PROT_SHIFT) | 
              (2 << CSW_CACHE_SHIFT) | 
               CSW_SIZE32 | CSW_PADDRINC;
  else
    csw_val = (1 << CSW_DBGSWENABLE_SHIFT) | 
              (2 << CSW_PROT_SHIFT) | 
              (2 << CSW_CACHE_SHIFT) | 
               CSW_SIZE32 | CSW_SADDRINC;
  ENTRYP("cmsis_read_mem32","add %08x count %d fl %x csw_val %08x",
         add,count,fl,csw_val);
  if (fl & SET_AUTO)
  {
    uint sel1;
    uint sel = pdap->regs[DAP_DP_SELECT].vals;
    int valid = dap_getval(pdap,DAP_DP_SELECT,&sel1);
    if (!valid || sel != sel1)
      logmsg("cmsis_read_mem32 valid %s sel %x sel1 %x\n",valid,sel,sel1);
    int tar = pdap->regs[DAP_MEMAP_TAR].vals;
    int csw = pdap->regs[DAP_MEMAP_CSW].vals;
    if (csw != csw_val)
      fl |= SET_CSW;
    if (tar != add)
      fl |= SET_TAR;
    if (fl & (SET_CSW | SET_TAR))
    {
      if ((sel & ~DAP_DP_SELECT_DPBANKSEL_MASK) != 0) 
        fl |= SET_SEL;
    }
    fl |= GET_RDBUFF; // why ???
    if (cp->plu->log)
      logmsg("cmsis_read_mem32 SET_AUTO sel %08x tar %08x csw %08x fl %x",sel,tar,csw,fl);
  }

  if (fl & SET_SEL)
  {
    int sel = pdap->dp_banksel; // TODO verify
    res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,sel,0,4);
  }
  if (fl & SET_CSW)
    res = cmsis_que_cmd(cp,DAP_MEMAP_CSW,DAP_WO,csw_val,0,4);  
  if (fl & SET_TAR)
    res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,add,0,4);  
  int sf = 0;
  for (int i = 0; i < count; i++)
  {
    res = cmsis_que_cmd(cp,DAP_MEMAP_DRW,DAP_RO,0,mem + i,4);  
    if (cp->flushed) 
    {
//    printf("flushed i %d\n",i);
      sf = i;
    }
    if (sf && i - sf >= 4)
    {
      res = cmsis_que_tb(cp,DAP_MEMAP_DRW,DAP_RO,mem + sf,count - sf); 
      break;
    }
  }
  if (count == 1 || fl & GET_RDBUFF) // why ?
    res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4); 
  res = cmsis_que_flush(cp,1);

  EXIT("cmsis_read_mem32");
  return res;
}

int cmsis_read_mem16(struct t_cmsisprobe *cp,uint add,uint count,ushort *mem,int fl)
{
  int res;
  struct t_dap *pdap = cp->pmdap->daps + cp->r_dapidx;
  int csw_val;
  int pack = pdap->use_packet && !(fl & SET_NOPACK);
  if (pack)      // use packet increment
    csw_val = (1 << CSW_DBGSWENABLE_SHIFT) | 
              (2 << CSW_PROT_SHIFT) | 
              (2 << CSW_CACHE_SHIFT) | 
               CSW_SIZE16 | CSW_PADDRINC;
  else
    csw_val = (1 << CSW_DBGSWENABLE_SHIFT) | 
              (2 << CSW_PROT_SHIFT) | 
              (2 << CSW_CACHE_SHIFT) | 
               CSW_SIZE16 | CSW_SADDRINC;
  ENTRYP("cmsis_read_mem16","add %08x count %d fl %d",add,count,fl);
  if (fl & SET_AUTO)
  {
    struct t_dap *pdap = cp->pmdap->daps + cp->r_dapidx; 
    int sel = pdap->regs[DAP_DP_SELECT].vals;
    int tar = pdap->regs[DAP_MEMAP_TAR].vals;
    int csw = pdap->regs[DAP_MEMAP_CSW].vals;
    if (csw != csw_val) 
      fl |= SET_CSW;
    if (tar != add)
      fl |= SET_TAR;
    if (fl & (SET_CSW | SET_TAR))
      if ((sel & ~DAP_DP_SELECT_DPBANKSEL_MASK) != 0) 
        fl |= SET_SEL;
    fl |= GET_RDBUFF; // why ???
  }
  if (fl & SET_SEL)
  {
    int sel = pdap->dp_banksel; // TODO verify
    res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,sel,0,4);
  }
  if (fl & SET_CSW)
    res = cmsis_que_cmd(cp,DAP_MEMAP_CSW,DAP_WO,csw_val,0,4);  
  if (fl & SET_TAR)
    res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,add,0,4); 
  if (pack)
  {
    for (int i = 0; i < count; i += 2)
      res = cmsis_que_cmd(cp,DAP_MEMAP_DRW,DAP_RO,0,mem + i,4); 
  } else
  {
    for (int i = 0; i < count; i++)
    {
      uchar flag = 0;
      if (((add  + i * 2) & 3) == 2)
        flag = 0x80; // address non multiple of 4 have to be handled when received
      res = cmsis_que_cmd(cp,DAP_MEMAP_DRW,DAP_RO,0,mem + i,2 | flag); 
    }
  }
  if (count == 1 || fl & GET_RDBUFF)
    res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4); 
  res = cmsis_que_flush(cp,1);
  EXIT("cmsis_read_mem16");
  return res;
}

// TODO use of BD3
int cmsis_read_32_x(struct t_cmsisprobe *cp,uint add,uint *val)
{
  int res;
  ENTRYP("cmsis_read_32_x","add %08x",add);
  uint tar;
  struct t_dap *pdap = cp->pmdap->daps + cp->r_dapidx;
  int valid = dap_getval(pdap,DAP_MEMAP_TAR,&tar);
  res = cmsis_que_cmd(cp,DAP_MEMAP_CSW,DAP_WO,0xa2000002,0,4); // TODO fix 
  if (!valid || tar != add - 12)
    res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,add - 12,0,4); // account use of BD3 (+ 12)
  res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,0x10,0,4);
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD3,DAP_RO,0,val,4);
  res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4);
  res = cmsis_que_flush(cp,1);

  EXIT("cmsis_read_32_x");
  return res;
}

// variant of cmsis_read_32 
// using swd_queue_ap_bankselect
// 
int cmsis_read_32_a(struct t_cmsisprobe *cp,uint add,uint *val,int fl)
{
  int csw_val,res;
  struct t_dap *pdap = cp->pmdap->daps + cp->r_dapidx;
  int csw = pdap->regs[DAP_MEMAP_CSW].vals;

/**
  if (pdap->use_packet)      // use packet increment
    csw_val = (1 << CSW_DBGSWENABLE_SHIFT) | 
              (2 << CSW_PROT_SHIFT) | 
              (2 << CSW_CACHE_SHIFT) | 
               CSW_SIZE32 | CSW_PADDRINC;
  else
    csw_val = (1 << CSW_DBGSWENABLE_SHIFT) | 
              (2 << CSW_PROT_SHIFT) | 
              (2 << CSW_CACHE_SHIFT) | 
               CSW_SIZE32 | CSW_SADDRINC;
**/
  csw_val = (1 << CSW_DBGSWENABLE_SHIFT) | 
            (2 << CSW_PROT_SHIFT) | 
            (2 << CSW_CACHE_SHIFT) | 
             CSW_SIZE32 | (csw & CSW_ADDRINC_MASK);
  ENTRYP("cmsis_read_32_a","add %08x fl %x csw_val %08x",
         add,fl,csw_val);
  if (fl & SET_AUTO)
  {
    int sel = pdap->regs[DAP_DP_SELECT].vals;
    int tar = pdap->regs[DAP_MEMAP_TAR].vals;
    if (csw != csw_val)
      fl |= SET_CSW;
    if (tar != add)
      fl |= SET_TAR;
    if (fl & (SET_CSW | SET_TAR))
    {
      if ((sel & ~DAP_DP_SELECT_DPBANKSEL_MASK) != 0) 
        fl |= SET_SEL;
    }
    if ((fl & SET_SEL) || sel != (0x10 | pdap->dp_banksel)) // TODO fix
        fl |= SET_SEL2;
    if (cp->plu->log)
      logmsg("cmsis_read_mem32 SET_AUTO sel %08x tar %08x csw %08x fl %x",sel,tar,csw,fl);
  }
  if (fl & SET_SEL)
    res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,pdap->dp_banksel,0,4);
  if (fl & SET_CSW)
    res = cmsis_que_cmd(cp,DAP_MEMAP_CSW,DAP_WO,csw_val,0,4);  
  if (fl & SET_CSW2)
    res = cmsis_que_cmd(cp,DAP_MEMAP_CSW,DAP_WO,0xa2000022,0,4); // TODO fix 
  if (fl & SET_TAR)
  {
    if (fl & (USE_BD1 | USE_BD2 | USE_BD3))
      error("TODO @%d",__LINE__);
    res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,add,0,4);
  }
  if (fl & SET_SEL2)
  {
    res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,0x10 | pdap->dp_banksel,0,4);
  } if (fl & (USE_BD1 | USE_BD2))
    error("TODO @%d",__LINE__);
  if (fl & USE_BD3) 
    res = cmsis_que_cmd(cp,DAP_MEMAP_BD3,DAP_RO,0,val,4);
  else
    res = cmsis_que_cmd(cp,DAP_MEMAP_BD0,DAP_RO,0,val,4);
  res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4);
  res = cmsis_que_flush(cp,1);

  EXIT("cmsis_read_32_a");
  return res;
}

int cmsis_read_32(struct t_cmsisprobe *cp,uint add,uint *val,int fl)
{
  return cmsis_read_mem32(cp,add,1,val,fl); 
}

int cmsis_write_32(struct t_cmsisprobe *cp,uint add,uint val,int fl)
{
  return cmsis_write_mem32(cp,add,1,&val,fl); 
}

int cmsis_read_16(struct t_cmsisprobe *cp,uint add,ushort *val,int fl)
{
  return cmsis_read_mem16(cp,add,1,val,fl); 
}

int cmsis_write_16(struct t_cmsisprobe *cp,uint add,ushort val,int fl)
{
  return cmsis_write_mem16(cp,add,1,&val,fl); 
}

int cmsis_read_mem(struct t_cmsisprobe *cp,uint add,int size,uint len,uchar *mem)
{
  int res = LIBUSB_OK;
  ENTRYP("cmsis_read_mem","add %08x size %d len %d dapidx %d/%d",add,size,len,cp->c_dapidx,cp->r_dapidx);
  if (cp->r_dapidx != cp->c_dapidx)
  {
    res = cmsis_multi_select(cp);
  }
  switch(size)
  {
    case 1:
      error("TODO in cmsis_read_mem @%d",__LINE__);
      break;
    case 2:
      res = cmsis_read_mem16(cp,add,len,(ushort *) mem,SET_AUTO | (len == 1 ? SET_NOPACK : 0)); 
      break;
    case 4:
      res = cmsis_read_mem32(cp,add,len,(uint *) mem,SET_AUTO); 
      break;
    default:
      error("unexpected @%d",__LINE__);
  }
  EXIT("cmsis_read_mem");
  return res;
}

// dap_dp_init_or_reconnect
int cmsis_dp_init_con(struct t_cmsisprobe *cp,int fl)
{
  ENTRY("cmsis_dp_init_con");
  cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,0x10,0,4); // auto ?
  uint ctrl_stat;
  cmsis_que_cmd(cp,DAP_DP_CTRL_STAT,DAP_RO,0,&ctrl_stat,4);
  int res = cmsis_que_flush(cp,1);
  if (cp->plu->log)
    logmsg("cmsis_dp_init_con: ctrl_stat %08x",ctrl_stat);
  if (fl == 0)
    cmsis_dp_init(cp);
  else
    error("TODO");
  EXIT("cmsis_dp_init_con");
  return res;
}

// cortex_m.c cortex_m_dwt_setup
int cmsis_dwt_setup(struct t_cmsisprobe *cp)
{
  int res;
  int s = get_count();
  uint dwt_ctrl,dwt_devarch;
  ENTRY("cmsis_dwt_setup");

  res = cmsis_read_32(cp,DWT_CTRL_ADD,&dwt_ctrl,SET_TAR);
  res = cmsis_read_32(cp,DWT_DEVARCH_ADD,&dwt_devarch,SET_TAR);
  cp->dwt_num_comp = FIELD_(dwt_ctrl,DWT_CTRL_NUM_COMP);
  logmsg("DWT_CTRL %08x num_comp %d DWT_DEVARCH %08x",dwt_ctrl,cp->dwt_num_comp,dwt_devarch);

  for (int i = 0; i < cp->dwt_num_comp; i++)
    res = cmsis_write_32(cp,DWT_FUNn(i),0,SET_TAR); 
  CHKSEQ1(s + 8);
  EXIT("cmsis_dwt_setup");

  return res;
}

int cmsis_find_mem_ap(struct t_cmsisprobe *cp)
{
  int s = get_count();
  ENTRY("cmsis_find_mem_ap");
  int res = cmsis_find_get_ap(cp);
  CHKSEQ1(s + 10);

  EXIT("cmsis_find_mem_ap");
  return res;
}

// arm_adi_v5.c: mem_ap_init
int cmsis_mem_ap_init(struct t_cmsisprobe *cp,int fl)
{
  int s = get_count();
  uint cfg;
  int res = LIBUSB_OK;

  ENTRY("cmsis_mem_ap_init");

  if (fl & SET_SEL)
  {
    uint select = cp->ndap == 1 ? 0xf0 : 0xf3; // TODO fix
    res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,select,0,4);
  }
  res = cmsis_que_cmd(cp,DAP_MEMAP_CFG,DAP_RO,0,&cfg,4);
  res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4);
  res = cmsis_que_flush(cp,1);
  if (cp->plu->log)
    logmsg("cmsis_mem_ap_init DAP_MEMAP_CFG %08x",cfg);
//         retval = mem_ap_setup_transfer(ap, CSW_8BIT | CSW_ADDRINC_PACKED, 0,4);
  int select = cp->ndap == 1 ? 0 : 3;
  res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,select,0,4); // TODO fix 
  res = cmsis_que_cmd(cp,DAP_MEMAP_CSW,DAP_WO,0xa2000020,0,4); // TODO fix 
  res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,0,0,4);  
  uint csw;
  res = cmsis_que_cmd(cp,DAP_MEMAP_CSW,DAP_RO,0,&csw,4);  
  res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4);
  res = cmsis_que_flush(cp,1);
  if (cp->plu->log)
    logmsg("cmsis_mem_ap_init: csw %08x",csw);
  CHKSEQ1(s + 4);

  EXIT("cmsis_mem_ap_init");

  return res;
}

// rtos.c rtos_get_gdb_reg 
//
int cmsis_read_reg(struct t_cmsisprobe *cp,int rid,uint *val,int fl)
{
  int res = LIBUSB_OK;
  int csw_val = 0xa2000012; // TODO fix
  int add = DHCSR_ADD;

  ENTRYP("cmsis_read_reg","regid %d fl %x",rid,fl);
  if (fl & SET_AUTO)
  {
    struct t_dap *pdap = cp->pmdap->daps + cp->r_dapidx;
    int sel = pdap->regs[DAP_DP_SELECT].vals;
    int tar = pdap->regs[DAP_MEMAP_TAR].vals;
    int csw = pdap->regs[DAP_MEMAP_CSW].vals;
    if (csw != csw_val)
      fl |= SET_CSW;
    if (tar != add)
      fl |= SET_TAR;
    if (fl & (SET_CSW | SET_TAR))
      if (sel != 0x3) // TODO fix
        fl |= SET_SEL;
    if ((fl & SET_SEL) || sel != 0x13) // TODO fix
        fl |= SET_SEL2;
  }
  if (fl & SET_SEL)
    res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,0x03,0,4);
  if (fl & SET_CSW)
    res = cmsis_que_cmd(cp,DAP_MEMAP_CSW,DAP_WO,csw_val,0,4);
  if (fl & SET_TAR)
    res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,add,0,4);
  if (fl & SET_SEL2)
    res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,0x13,0,4);
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD1,DAP_WO,rid,0,4);
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD0,DAP_RO,0,&cp->dhcsr,4);
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD2,DAP_RO,0,val,4);
  res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4);
  res = cmsis_que_flush(cp,1);

  if (cp->plu->log)
    logmsg("cmsis_read_reg: dhcsr %08x rid %d val %08x",cp->dhcsr,rid,*val);

  EXIT("cmsis_read_reg");
  return res;
}

int cmsis_unset_break(struct t_cmsisprobe *cp,uint add,int idx)
{
  int res = LIBUSB_OK;

  ENTRYP("cmsis_unset_break","add %08x idx %d",add,idx);
  if (idx < 0 || idx > NBRK)
    logmsg("cmsis_unset_break idx out of range %d [0..%d]",idx,NBRK);
  else
  {
    res = cmsis_write_32(cp,FP_COMPn_ADD(idx),0,SET_AUTO);
    if (res == LIBUSB_OK)
      cp->bt.brk[idx].flag &= ~BRK_F_ACT;
  }
  return res;
}

int cmsis_set_break(struct t_cmsisprobe *cp,uint add,int idx)
{
  int res = LIBUSB_OK;

  ENTRYP("cmsis_set_break","add %08x idx %d",add,idx);
  if (idx < 0 || idx > NBRK)
    logmsg("cmsis_set_break idx out of range %d [0..%d]",idx,NBRK);
  else
  {
    if (add == 0 && !(cp->bt.brk[idx].flag & BRK_F_ACT))
      logmsg("cmsis_set_break: idx %d try to deactivate not active breakpoint, ignore it",idx);
    else if (add && (cp->bt.brk[idx].flag & BRK_F_ACT))
      logmsg("cmsis_set_break: idx %d break is alreay active");
    else
    {
      if (cp->bt.brk[idx].flag & BRK_F_HW)
      {
        if (add > 0x1fffffff)
        {
          logmsg("cmsis_set_break idx %d hw address range limited to [0..1fffffff]");
          res = 96;
        } else
        {
// set breakpoint in fpb.py
          uint match = (add & 2) ? FP_COMP_MATCH_UP : FP_COMP_MATCH_LOW; 
          logmsg("cmsis_set_break set hw break %d comp %08x set add %08x val %08x",
                 idx,add,
                 FP_COMPn_ADD(idx),
                 match | (add & FP_COMP_COMP_MASK) | FP_COMP_ENABLE);
          res = cmsis_write_32(cp,FP_COMPn_ADD(idx),
                               match | 
                               (add & FP_COMP_COMP_MASK) | FP_COMP_ENABLE,
                               SET_AUTO);
          if (res == LIBUSB_OK)
          {
            if (!(cp->bt.fp_ctrl & FP_CTRL_EN_MASK))
            {
              uint fp_ctrl = FP_CTRL_KEY_MASK | FP_CTRL_EN_MASK;
              res = cmsis_write_32(cp,FP_CTRL_ADD,fp_ctrl,SET_AUTO);
              if (res == LIBUSB_OK)
              {
                res = cmsis_read_32(cp,FP_CTRL_ADD,&fp_ctrl,SET_AUTO);
                cp->bt.fp_ctrl = fp_ctrl;
              }
            }
            if (add)
            {
              cp->bt.brk[idx].flag |= BRK_F_ACT;
              cp->bt.brk[idx].count = 0;
              cp->bt.brk[idx].add = add;
            } else
            {
              cp->bt.brk[idx].flag &= ~BRK_F_ACT;
            }
          }
        }
      } else
      {
        if (add > 0 && add <= 0x1fffffff)
        {
          logmsg("can't write to flash");
          res = 97;
        } else
        {
          ushort test;
          if (add == 0 && (cp->bt.brk[idx].flag & BRK_F_ACT))
          { 
            res = cmsis_write_16(cp,cp->bt.brk[idx].add,cp->bt.brk[idx].save,SET_AUTO); // SET_CSW | SET_TAR);
            res = cmsis_read_16(cp,cp->bt.brk[idx].add,&test,SET_AUTO);
            if (test != cp->bt.brk[idx].save)
            {
              logmsg("cmsis_set_break badd %08x old %04x read back %04x",
                     cp->bt.brk[idx].add,cp->bt.brk[idx].save,test);
            res = 98;
            } else
              cp->bt.brk[idx].flag &= ~BRK_F_ACT;
          } else if (add && !(cp->bt.brk[idx].flag & BRK_F_ACT))
          {
/*
            uint v32,v32_0;
            res = cmsis_read_32(cp,add & ~0x3,&v32_0,SET_AUTO);
*/
            res = cmsis_read_16(cp,add,&cp->bt.brk[idx].save,SET_AUTO); // SET_CSW | SET_TAR);
//          printf("cmsis_set_break add %08x v32 %08x v16 %04x\n",add,v32_0,cp->bt.brk[idx].save);
            res = cmsis_write_16(cp,add,0xbe00,SET_AUTO); // SET_CSW | SET_TAR);
            res = cmsis_read_16(cp,add,&test,SET_AUTO); // SET_CSW | SET_TAR);
/*
            res = cmsis_read_32(cp,add & ~0x3,&v32,SET_AUTO);
            printf("cmsis_set_break add %08x v32 %08x -> %08x\n",add,v32_0,v32);
*/
            if (test != 0xbe00)
            {
              logmsg("cmsis_set_break add %08x old %04x set be00 read back %04x",
                     add,cp->bt.brk[idx].save,test);
              res = 99;
            } else
            {
              cp->bt.brk[idx].flag |= BRK_F_ACT;
              cp->bt.brk[idx].count = 0;
              cp->bt.brk[idx].add = add;
            }
          }
        }
      }
    }
  }
  EXIT("cmsis_set_break");
  return res;
}

int cmsis_get_state(struct t_cmsisprobe *cp)
{
  struct t_libusb *plu = cp->plu;
  char buf[1000];
//struct t_libusb *plu = cp->plu;

  uint dhcsr;
  cmsis_read_32(cp,DHCSR_ADD,&dhcsr,SET_AUTO);
  arm_dumpreg(DHCSR_ADD,dhcsr,buf,sizeof buf,0);
  if (plu->log)
    logmsg("get_state: dhcsr %08x %s",dhcsr,buf);
  cp->dhcsr = dhcsr;
  if (dhcsr & DHCSR_S_RESET_ST)
  {
    uint dhcsr1;
    cmsis_read_32(cp,DHCSR_ADD,&dhcsr1,SET_AUTO);
    cp->dhcsr = dhcsr;
    if (dhcsr1 & DHCSR_S_RESET_ST)
      return S_RESET;
  }
  return dhcsr_state(dhcsr);
}

int cmsis_cont(struct t_cmsisprobe *cp)
{
  int res = LIBUSB_OK;
// resume in cortex_m.py

  for (int idx = 0; idx < NBRK; idx++)
    if (cp->bt.brk[idx].flag & BRK_F_ACT)
      logmsg("active break idx %2d add %08x",idx,cp->bt.brk[idx].add);

  res = cmsis_write_32(cp,DHCSR_ADD,DHCSR_DBGKEY | DHCSR_C_DEBUGEN,SET_AUTO);
  int count = 0;
  uint st = -1;
  while (count < 10)
  {
    st = cmsis_get_state(cp);
    logmsg("cmsis_cont: count %d st %d %s",count,st,state_name[st]);
    if (st == S_HALTED) break;
    usleep(10000);
    count++;
  }
  if (st != S_HALTED)
  {
// force debug stop
    res = cmsis_write_32_a(cp,DHCSR_ADD,DHCSR_C_HALT | 
                                        DHCSR_C_DEBUGEN | 
                                        DHCSR_DBGKEY ,SET_AUTO); // SET_SEL2); 
/*
    res = cmsis_debug_entry(cp,DFSR_BKPT | DFSR_HALTED,0);
*/
    res = cmsis_read_32_a(cp,DHCSR_ADD,&cp->dhcsr,SET_AUTO); // SET_SEL | SET_TAR | SET_SEL2); 
    char buf[1000];
    arm_dumpreg(DHCSR_ADD,cp->dhcsr,buf,sizeof buf,0);
    st = cmsis_get_state(cp);
    logmsg("cmsis_cont: force halt dhcsr %08x %s st %d %s",cp->dhcsr,buf,st,state_name[st]);
  }
  uint pc;
  res = cmsis_read_reg(cp,PC_RN,&pc,SET_AUTO);
  int idx;
  for (idx = 0; idx < NBRK; idx++)
  {
    if (cp->bt.brk[idx].flag & BRK_F_ACT && cp->bt.brk[idx].add == pc)
    {
      logmsg("cmsis_cont: stop pc %08x break %d",pc,idx);
      break;
    }
  }
  if (idx >= NBRK)
    logmsg("cmsis_cont: stop pc %08x NOT at breakpoint",pc);
// get_t_response [gdbserver]
// is_debug_trap [cortex_m]
//pico_is_debug_trap(cp); // use diffrent register list
  uint dfsr,demcr;
  res = cmsis_read_32(cp,DFSR_ADD,&dfsr,SET_AUTO);
  res = cmsis_read_32(cp,DEMCR_ADD,&demcr,SET_AUTO);
  char buf[1000];
  arm_dumpreg(DFSR_ADD,dfsr,buf,sizeof buf,0);
  logmsg("cmsis_cont: dfsr %08x %s",dfsr,buf);
  arm_dumpreg(DEMCR_ADD,demcr,buf,sizeof buf,0);
  logmsg("cmsis_cont: demcr %08x %s",demcr,buf);
/*
  uint vals[4];
  int rid4[] = {7,13,14,15};
  cmsis_read_regs(cp,rid4,SIZE(rid4),vals,1);
*/
  return res;
}

int cmsis_rem_break(struct t_cmsisprobe *cp,int idx)
{
// remove breakpoint
  return cmsis_write_32(cp,FP_COMPn_ADD(idx),0,SET_AUTO);
}

void cmsis_dumpreg(struct t_cmsisprobe *cp,uint *regs)
{
  for (int i = 0; i < SIZE(regmap); i++)
    printf("%-8s: %08x\n",regname[i],regs[i]);
}

// cortex_m.c: cortex_m_fast_read_all_regs
int cmsis_read_regs_f(struct t_cmsisprobe *cp,int fl,uint *regs,int fl1)
{
  int res = LIBUSB_OK;
  struct t_dap *pdap = cp->pmdap->daps + cp->r_dapidx;
  ENTRYP("cmsis_read_regs_f","fl %x fl1 %x",fl,fl1);
  if (fl & SET_AUTO)
  {
    uint sel,tar;
    int valid_sel = dap_getval(pdap,DAP_DP_SELECT,&sel);
    int valid_tar = dap_getval(pdap,DAP_MEMAP_TAR,&tar);
    if (!valid_tar || tar != DHCSR_ADD)
      fl |= SET_TAR;
    if (fl & (SET_TAR))
      if (sel != pdap->dp_banksel) // TODO fix
        fl |= SET_SEL;
    if ((fl & SET_SEL) || !valid_sel || sel != (0x10 | pdap->dp_banksel)) // TODO fix
      fl |= SET_SEL2;
  }

  if (fl & SET_SEL)
    res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,pdap->dp_banksel,0,4);
  if (fl & SET_TAR)
    res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,DHCSR_ADD,0,4);
  if (fl & SET_SEL2)
    res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,0x10 | pdap->dp_banksel,0,4);
  uint dhcsr[64 + 32];
  for (int i = 0; i < SIZE(regmap); i++)
  {
// write reg # in DCRSR (&DCRSR = &DHCSR + 4)
    res = cmsis_que_cmd(cp,DAP_MEMAP_BD1,DAP_WO,regmap[i],0,4);
    if ((fl1 & 1) == 0) // don't read dhcsr
      res = cmsis_que_cmd(cp,DAP_MEMAP_BD0,DAP_RO,0,dhcsr + i,4);
    res = cmsis_que_cmd(cp,DAP_MEMAP_BD2,DAP_RO,0,regs + i,4);
    if ((fl1 & 2) == 0) //  don't read rdbuff
      res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4);
  } 
  if (cp->fpv4 || cp->fpv5)
  {
    for (int i = 0x40; i < 0x60; i++)
    {
// write reg # in DCRSR (&DCRSR = &DHCSR + 4)
      res = cmsis_que_cmd(cp,DAP_MEMAP_BD1,DAP_WO,i,0,4);
      if ((fl1 & 1) == 0) // don't read dhcsr
        res = cmsis_que_cmd(cp,DAP_MEMAP_BD0,DAP_RO,0,dhcsr + i,4);
      res = cmsis_que_cmd(cp,DAP_MEMAP_BD2,DAP_RO,0,regs + i,4);
      if ((fl1 & 2) == 0) //  don't read rdbuff
        res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4);
    } 
    res = cmsis_que_cmd(cp,DAP_MEMAP_BD1,DAP_WO,0x21,0,4);
    if ((fl1 & 1) == 0) // don't read dhcsr
      res = cmsis_que_cmd(cp,DAP_MEMAP_BD0,DAP_RO,0,dhcsr + 0x21,4);
    res = cmsis_que_cmd(cp,DAP_MEMAP_BD2,DAP_RO,0,regs + 0x21,4);
    if ((fl1 & 2) == 0) //  don't read rdbuff
      res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4);
  }
  res = cmsis_que_flush(cp,1);
  if (cp->plu->log)
  {
    for (int i = 0; i < SIZE(regmap); i++)
      printf("%-8s: %08x\n",regname[i],regs[i]);
    if (cp->fpv4 || cp->fpv5)
      printf("%-8s: %08x\n","FPSCR",regs[0x21]);
    for (int i = 0; i < 16; i++)
      printf("S%02d    : %08x\n",i,regs[0x40 + i]);
  }

  EXIT("cmsis_read_regs_f");
  return res;
}

// cortex_m.c: cortex_m_debug_entry
//             cortex_m_clear_halt
int cmsis_debug_entry(struct t_cmsisprobe *cp,int dfsr_val,int fl,uint *regs)
{
  char buf[1000];
  int res = LIBUSB_OK;

  ENTRYP("cmsis_debug_entry","dfsr %08x flag %d",dfsr_val,fl);
  res = cmsis_write_32_a(cp,DHCSR_ADD,DHCSR_C_HALT | 
                                      DHCSR_C_DEBUGEN | 
                                      DHCSR_DBGKEY ,0); 
  uint dfsr;
  res = cmsis_read_32_a(cp,DFSR_ADD,&dfsr,SET_SEL | SET_TAR | SET_SEL2); 
  arm_dumpreg(DFSR_ADD,dfsr,buf,sizeof buf,0);
  logmsg("cmsis_debug_entry dfsr  %08x %s -> %08x",dfsr,buf,dfsr_val);
  res = cmsis_write_32_a(cp,DFSR_ADD,dfsr_val,0); 
  res = cmsis_read_32_a(cp,DHCSR_ADD,&cp->dhcsr,SET_SEL | SET_TAR | SET_SEL2); 
  arm_dumpreg(DHCSR_ADD,cp->dhcsr,buf,sizeof buf,0);
  logmsg("cmsis_debug_entry dhcsr %08x %s",cp->dhcsr,buf);

  if (fl)
  {
    if (!regs)
      error("cmsis_debug_entry null regs");
    res = cmsis_read_regs_f(cp,SET_AUTO,regs,0);
  }
  EXIT("cmsis_debug_entry");

  return res;
}

// cortex_m_endreset_event

int cmsis_endreset(struct t_cmsisprobe *cp)
{
  int res = LIBUSB_OK;
  int s = get_count();

  ENTRY("cmsis_endreset");
  uint demcr;
  res = cmsis_read_32_a(cp,DEMCR_ADD,&demcr,USE_BD3); 
/* TODO
  W + R combined

  res = cmsis_write_32(cp,DCRDR_ADD,0x00000000,USE_BD2); 
  res = cmsis_read_32_a(cp,DHCSR_ADD,&cp->dhcsr,0); 
*/
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD2,DAP_WO,0,0,4);
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD0,DAP_RO,0,&cp->dhcsr,4);
  res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4);
  res = cmsis_que_flush(cp,1);
  CHKSEQ1(s + 4);
// end TODO W + R combined  
// TODO
// W + W combined
//  [4.129316 890] arm_adi_v5.c: mem_ap_write_u32 ENTRY add e000edfc val 01000000
//  [4.129498 890] target.c target_write_u32 ENTRY address e0002000 val 00000003
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD3,DAP_WO,0x01000000,0,4);
  res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,0x3,0,4);
  res = cmsis_que_cmd(cp,DAP_MEMAP_CSW,DAP_WO,0xa2000012,0,4);  
  res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,FP_CTRL_ADD,0,4);
  res = cmsis_que_cmd(cp,DAP_MEMAP_DRW,DAP_WO,0x3,0,4);
  res = cmsis_que_flush(cp,1);
  CHKSEQ1(s + 6);
// end W + W
  res = cmsis_read_mem32(cp,FP_CTRL_ADD,1,&cp->bt.fp_ctrl,SET_TAR); 
  logmsg("cmsis_endreset fpctrl %08x",cp->bt.fp_ctrl);
  CHKSEQ1(s + 8);
  for (int i = 0; i < cp->fp_ncomp + cp->fp_nlit; i++)
  {
    if (cp->plu->log)
      logmsg("remove break %d add %08x",i,FP_COMPn_ADD(i));
    res = cmsis_write_32(cp,FP_COMPn_ADD(i),0,i == 0 ? 0 + SET_TAR : 0); 
  }
  CHKSEQ1(s + 14);

  for (int i = 0; i < cp->dwt_num_comp; i++)
  {
    res = cmsis_write_32(cp,DWT_COMPn(i),0,SET_TAR); 
    res = cmsis_write_32(cp,DWT_MASKn(i),0,0); 
    res = cmsis_write_32(cp,DWT_FUNn(i),0,0); 
  }
  CHKSEQ1(s + 14 + cp->dwt_num_comp * 6);
  res = cmsis_read_32_a(cp,DHCSR_ADD,&cp->dhcsr,SET_TAR | SET_SEL2); 
  if (cp->plu->log)
    logmsg("cmsis_endreset dhcsr %08x",cp->dhcsr);

  EXIT("cmsis_endreset");
  return res;
}

int cmsis_poll(struct t_cmsisprobe *cp,int fl,int poll_flag,uint dfsr,uint *regs)
{
  int res;
  struct t_dap *pdap = cp->pmdap->daps + cp->r_dapidx; 

  int s = get_count();
  int s1 = 0;
  uint pre = pdap->status;
  ENTRYP("cmsis_poll","fl %x dfsr %08x st %s poll_flag %x r_targetsel %08x c/r dapidx %d/%d",
         fl,dfsr,state_name[pre],poll_flag,pdap->targetsel,cp->c_dapidx,cp->r_dapidx);
  if (poll_flag & POLL_MULTI)
  {
    res = cmsis_multi_select(cp);
    CHKSEQ1(s + 8);
    s1 = 8;
  }
  s = get_count();
  res = cmsis_read_32_a(cp,DHCSR_ADD,&cp->dhcsr,fl); 
  uint st = dhcsr_state(cp->dhcsr);
  cp->cumulate_sticky |= cp->dhcsr;
  logmsg("cmsis_poll %08x DHCSR  %08x cumulate %08x st %d->%d %s->%s",
         pdap->targetsel,cp->dhcsr,cp->cumulate_sticky,
         pre,st,
         state_name[pre],state_name[st]);
  CHKSEQ1(s + s1 + 2);
  if (cp->cumulate_sticky & DHCSR_S_RESET_ST)
    cp->cumulate_sticky &= ~DHCSR_S_RESET_ST;
  if (poll_flag & POLL_STICKY)
  {
    pdap->status = S_RESET;
    
    EXITP("cmsis_poll","sticky status %d %s",pdap->status,state_name[pdap->status]);
    return res;
  }

  if (pdap->status == S_RESET)
  {
    res = cmsis_endreset(cp);
    pre = pdap->status = S_RUNNING;
  } /* else 
    pdap->status = st;
    */
  if (cp->plu->log)
    logmsg("cmsis_poll dhcsr %08x halted %d pre %d",cp->dhcsr,cp->dhcsr & DHCSR_S_HALT,pre);
  if (cp->dhcsr & DHCSR_S_HALT) 
  {
    pdap->status = S_HALTED;
    if (pre == S_DEBRUN || pre == S_RUNNING) // TODO check
      res = cmsis_debug_entry(cp,dfsr,1,regs);
  }

  if (pdap->status == S_UNKNOWN)
    if (cp->dhcsr & (DHCSR_S_RETIRE_ST | DHCSR_S_SLEEP))
      pdap->status = S_RUNNING;

  EXITP("cmsis_poll","status %d %s",pdap->status,state_name[pdap->status]);
  return res;
}

int cmsis_halt(struct t_cmsisprobe *cp,int maskint)
{
  ENTRYP("cmsis_halt","maskint %d",maskint);
  int res;
  if (cp->ndap > 1)
    res = cmsis_multi_select(cp);
//printf("*** dhcsr %08x cp->dhcsr -> ",cp->dhcsr);
  cp->dhcsr = (cp->dhcsr & ~DHCSR_DBGKEY_MASK) | DHCSR_C_HALT | DHCSR_DBGKEY;
//printf("%08x\n",cp->dhcsr);
  res = cmsis_write_32_a(cp,DHCSR_ADD,cp->dhcsr,SET_AUTO); 
  if (!!(cp->dhcsr & DHCSR_C_MASKINTS) != maskint)
  {
    if (maskint)
      cp->dhcsr |= DHCSR_C_MASKINTS;
    else
      cp->dhcsr &= ~DHCSR_C_MASKINTS;
    res = cmsis_write_32_a(cp,DHCSR_ADD,cp->dhcsr,SET_AUTO); 
  }
      
  EXIT("cmsis_halt");
  return res;
}

// flash/nor/rp2040.c rp2040_lookup_symbol
int cmsis_lookup_sym(struct t_cmsisprobe *cp,uint tag,ushort *symbol,int fl)
{
  uint v;
  ushort tabe;

  struct t_dap *pdap = cp->pmdap->daps + cp->r_dapidx; 
  ENTRYP("cmsis_lookup_sym","tag %x fl %x",tag,fl);
  int res = cmsis_read_32(cp,BOOTROM_MAGIC_ADDR,&v,fl);
  logmsg("bootrom magic %08x %08x: %08x %08x tag %04x",
         pdap->targetsel,BOOTROM_MAGIC_ADDR,v,BOOTROM_MAGIC,tag);
  res = cmsis_read_16(cp,BOOTROM_MAGIC_ADDR + 4,&tabe,SET_CSW);
  if (cp->plu->log)
    logmsg("%08x: tabe %04x",BOOTROM_MAGIC_ADDR + 4,tabe);

  ushort et;
  for (int i = 0 ; i < 50; i++)
  {
//  printf("tabe %04x\n",tabe);
    res = cmsis_read_16(cp,tabe,&et,/* SET_CSW | */ SET_TAR);
    if (cp->plu->log)
      logmsg("%04x: %04x %c%c tag %08x",tabe,et,et >> 8,et & 0xff,tag);
    if (et != tag)
      tabe += 4;
    else
    {
      res = cmsis_read_16(cp,tabe + 2,symbol,0);
      if (cp->plu->log)
        logmsg("cmsis_lookup_sym tag %02x symbol %04x",tag,*symbol);
      EXIT("cmsis_lookup_sym");
      return res;      
    }
  }
  EXIT("cmsis_lookup_sym");
  
  return res;
}

// cortex_m.c: cortex_m_store_core_reg_u32
int cmsis_write_core_reg(struct t_cmsisprobe *cp,int regid,uint val,int fl)
{
  int res = LIBUSB_OK;

  ENTRYP("cmsis_write_core_reg","regid %d val %08x fl %x",regid,val,fl);
  if (fl & SET_TAR)
    res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,DHCSR_ADD,0,4);
  if (fl & SET_SEL2)
    res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,0x13,0,4);
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD2,DAP_WO,val,0,4);
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD1,DAP_WO,0x00010000 | regid,0,4);
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD0,DAP_RO,0,&cp->dhcsr,4);
  res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4);
  cmsis_que_flush(cp,1);

  if (cp->plu->log)
    logmsg("cmsis_write_core_reg dhcsr %08x",cp->dhcsr);
  EXIT("cmsis_write_core_reg");
  return res;
}

// cortex_m.c: cortex_m_restore_one
int cmsis_restore_one(struct t_cmsisprobe *cp,struct t_set_reg *psr,int fl)
{
  int res = LIBUSB_OK;

  ENTRY("cmsis_restore_one");
  for (; psr->regid >= 0; psr++,fl = 0)
    res = cmsis_write_core_reg(cp,psr->regid,psr->regval,fl);
  EXIT("cmsis_restore_one");
  return res;
}

// armv7m.c armv7m_wait_algorithm
int cmsis_wait_call(struct t_cmsisprobe *cp,uint dfsr)
{
  ENTRY("cmsis_wait_call");
  int res = cmsis_poll(cp,0,0,dfsr,0);
  EXIT("cmsis_wait_call");
  return res;
}

// cortex_m.c: cortex_m_write_debug_halt_mask
int cmsis_debug_halt_mask(struct t_cmsisprobe *cp,int fl,int val)
{
  int res = LIBUSB_OK;
  uint dhcsr = DHCSR_DBGKEY | (val & ~DHCSR_DBGKEY_MASK);
  ENTRYP("cmsis_debug_halt_mask","val %08x -> %08x fl %08x",val,dhcsr,fl);
  res = cmsis_write_32_a(cp,DHCSR_ADD,dhcsr,fl); 
#ifdef OLD

  if (fl & SET_TAR)
    res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,DHCSR_ADD,0,4);  
  if (fl & SET_SEL2)
    res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,0x13,0,4);
  cp->dhcsr = DHCSR_DBGKEY | val;
  if (cp->plu->log)
    logmsg("cmsis_debug_halt_mask write dhcsr %08x",cp->dhcsr);
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD0,DAP_WO,val,0,4);
  res = cmsis_que_flush(cp,1);
#endif
  EXIT("cmsis_debug_halt_mask");
  return res;
}

// target.c target_resume
int cmsis_target_resume(struct t_cmsisprobe *cp,struct t_set_reg *psr,int fl)
{
  ENTRY("cmsis_target_resume");
  int ret = cmsis_restore_one(cp,psr,fl);
  ret = cmsis_debug_halt_mask(cp,SET_AUTO | SET_CSW,DHCSR_C_DEBUGEN);  // TODO fix SET_CSW
  cp->pmdap->daps[cp->c_dapidx].status = S_DEBRUN; // ??
  EXIT("cmsis_target_resume");
  return ret;
}

// flash/nor/rp2040.c rp2040_call_rom_func
int cmsis_call_rom(struct t_cmsisprobe *cp,struct t_set_reg *psr,int fl,uint dfsr)
{
  ENTRY("cmsis_call_rom");

  if (cp->plu->log)
    logmsg("cmsis_call_rom fl %x dfsr %08x",fl,dfsr);

  int res = cmsis_target_resume(cp,psr,fl);
  res = cmsis_wait_call(cp,dfsr);

  EXIT("cmsis_call_rom");
  return res;
}

static uint save_mem[64];
// flash/nor/rp2040.c rp2040_stack_grab_and_prep
int cmsis_grab_stack(struct t_cmsisprobe *cp)
{
  int res = LIBUSB_OK;

  ENTRY("cmsis_grab_stack");
  CHKSEQ1(528);
  res = cmsis_read_mem32(cp,0x20010000,SIZE(save_mem),save_mem,SET_TAR | SET_CSW); 
/*
  for (int i = 0; i < SIZE(save_mem); i++)
    printf("M %08x: %08x\n",0x20010000 + i * 4,save_mem[i]);
*/
  struct t_set_reg sr[16];
  int i = 0;
  sr[i].regid  = 20;
  sr[i].regval = 0x02000001;
  i++;
  sr[i].regid  = 16;
  sr[i].regval = 0x01000000;
  i++;
  sr[i].regid  = 15;
  sr[i].regval = 0x000001a8;
  i++;
  sr[i].regid  = 13;
  sr[i].regval = 0x20010100;
  i++;
  sr[i].regid  = 7;
  sr[i].regval = 0x2491;
  i++;
  sr[i].regid  = -1;
  sr[i].regval = 0;
  res = cmsis_call_rom(cp,sr,SET_TAR | SET_SEL2,DFSR_HALTED | DFSR_BKPT);
  i = 0;
  sr[i].regid  = 20;
  sr[i].regval = 0x02000001;
  i++;
  sr[i].regid  = 18;
  sr[i].regval = 0x20011340;
  i++;
  sr[i].regid  = 16;
  sr[i].regval = 0x01000000;
  i++;
  sr[i].regid  = 15;
  sr[i].regval = 0x000001a8;
  i++;
  sr[i].regid  = 14;
  sr[i].regval = 0xa5a5a5a5;
  i++;
  sr[i].regid  = 13;
  sr[i].regval = 0x20010100;
  i++;
/*
  sr[i].regid  = 12;
  sr[i].regval = 0xa5a5a5a5;
  i++;
*/
  sr[i].regid  = 7;
  sr[i].regval = 0x23e5;
  i++;
  sr[i].regid  = 6;
  sr[i].regval = 0x200201fc;
  i++;
  sr[i].regid  = 3;
  sr[i].regval = 0x00000001;
  i++;
  sr[i].regid  = 2;
  sr[i].regval = 0xa5a5a5a5;
  i++;
  sr[i].regid  = 1;
  sr[i].regval = 0xa5a5a5a5;
  i++;
  sr[i].regid  = -1;
  sr[i].regval = 0;
  i++;
  res = cmsis_call_rom(cp,sr,0,DFSR_HALTED | DFSR_BKPT);
  i = 0;
  CHKSEQ1(610);
  EXIT("cmsis_grab_stack");
  return res;
}

// flash/nor/rp2040.c rp2040_spi_read_flash_id
int cmsis_read_flash_id(struct t_cmsisprobe *cp)
{
#define SPIFLASH_READ_ID            0x9F /* Read Flash Identification */
  const uint qspi_ctrl_addr = 0x4001800c;
  const uint ssi_dr0        = 0x18000060;
  int res = LIBUSB_OK;
  uint ctrl;

  int s = get_count();
  ENTRY("cmsis_read_flash_id");
// rp2040_ssel_active begin
  res = cmsis_read_32(cp,qspi_ctrl_addr,&ctrl,SET_SEL + SET_TAR);
  logmsg("cmsis_read_flash_id qspi ctrl %08x",ctrl);
  res = cmsis_write_32(cp,qspi_ctrl_addr,0x00000200,SET_TAR); 
// rp2040_ssel_active end 
  CHKSEQ1(s + 4);
  for (int i = 0; i < 4 && res == LIBUSB_OK; i++)
    res = cmsis_write_32(cp,ssi_dr0,SPIFLASH_READ_ID,SET_TAR); 
  CHKSEQ1(s + 12);
  uint status;
  uint device_id = 0;
  for (int i = 0; i < 4 && res == LIBUSB_OK; i++)
  {
    res = cmsis_read_32(cp,ssi_dr0,&status,SET_TAR);
    device_id >>= 8;
    device_id |= (status & 0xff) << 24;
  }
  CHKSEQ1(s + 20);
  logmsg("cmsis_read_flash_id %08x",device_id);
  res = cmsis_read_32(cp,qspi_ctrl_addr,&ctrl,SET_TAR);
  res = cmsis_write_32(cp,qspi_ctrl_addr,0x00000300,SET_TAR); 
  CHKSEQ1(s + 24);
  EXIT("cmsis_read_flash_id");

  return res;
}

int cmsis_free_working_area(struct t_cmsisprobe *cp)
{
  ENTRY("cmsis_free_working_area");
  uint res = LIBUSB_OK;
/* force save_mem to match openocd run
  int i = 0;

  save_mem[i++] = 0x2d8223a2;
  save_mem[i++] = 0x7fa6f96b;
  save_mem[i++] = 0x28563a5b;
  save_mem[i++] = 0xc1975260;
  save_mem[i++] = 0x738a9140;
  save_mem[i++] = 0x062770df;
  save_mem[i++] = 0x02fd2ed9;
  save_mem[i++] = 0xb1a3b743;
  save_mem[i++] = 0x1344f030;
  save_mem[i++] = 0x4531cfec;

  save_mem[i++] = 0x13dacb1d;
  save_mem[i++] = 0x023926b1;
  save_mem[i++] = 0x7f61062f;
  save_mem[i++] = 0x95b94ebe;
  save_mem[i++] = 0xa92a4153;
  save_mem[i++] = 0x5ab8a854;
  save_mem[i++] = 0xa34245b0;
  save_mem[i++] = 0xdd518aa2;
  save_mem[i++] = 0xd4153687;
  save_mem[i++] = 0xf7a3f19e;
  save_mem[i++] = 0xe5fb30b5;
  save_mem[i++] = 0x22bb8a8d;
  save_mem[i++] = 0x59b40411;
  save_mem[i++] = 0xc6398b28;

  save_mem[i++] = 0x607812d0;
  save_mem[i++] = 0xda008f2f;
  save_mem[i++] = 0xfb30b720;
  save_mem[i++] = 0xb816162b;
  save_mem[i++] = 0x1f1e6f02;
  save_mem[i++] = 0x18068d0a;
  save_mem[i++] = 0x7638f5fc;
  save_mem[i++] = 0x491bf91f;
  save_mem[i++] = 0x60d97a09;
  save_mem[i++] = 0xb9dbf809;
  save_mem[i++] = 0xe70ef645;
  save_mem[i++] = 0xd50e5464;
  save_mem[i++] = 0x55c0c4d1;
  save_mem[i++] = 0x028c2a7b;

  save_mem[i++] = 0x16d090db;
  save_mem[i++] = 0x173484a6;
  save_mem[i++] = 0x6dfb1380;
  save_mem[i++] = 0xa5c53d49;
  save_mem[i++] = 0x87cbd8a9;
  save_mem[i++] = 0x9ee7c76d;
  save_mem[i++] = 0x9068454c;
  save_mem[i++] = 0x993ce796;
  save_mem[i++] = 0xcebcd233;
  save_mem[i++] = 0xc269bfa7;
  save_mem[i++] = 0x5c706eba;
  save_mem[i++] = 0xb544e2d2;
  save_mem[i++] = 0x5b301865;
  save_mem[i++] = 0x29bb2c70;

  save_mem[i++] = 0xd6fa9d93;
  save_mem[i++] = 0x428dcb84;
  save_mem[i++] = 0x701feda1;
  save_mem[i++] = 0x48a1d72d;
  save_mem[i++] = 0x2c2423da;
  save_mem[i++] = 0x8899571f;
  save_mem[i++] = 0x59a332bc;
  save_mem[i++] = 0xd1843b27;
  save_mem[i++] = 0xd66a4184;
  save_mem[i++] = 0xe1c8a827;
  save_mem[i++] = 0x1848e5cc;
  save_mem[i++] = 0xd9175f42;
*/

  CHKSEQ1(740);
  res = cmsis_write_mem32(cp,0x20010000,64,save_mem,SET_SEL | SET_TAR); 
  EXIT("cmsis_free_working_area");
  return res;
}

// flash/nor/rp2040.c rp2040_finalize_stack_free
int cmsis_final_stack(struct t_cmsisprobe *cp)
{
  ENTRY("cmsis_final_stack");
  CHKSEQ1(634);
  uint res = LIBUSB_OK;
  struct t_set_reg sr[16];
  int i = 0;
  sr[i].regid  = 20;
  sr[i].regval = 0x02000001;
  i++;
  sr[i].regid  = 18;
  sr[i].regval = 0x20011340;
  i++;
  sr[i].regid  = 16;
  sr[i].regval = 0x01000000;
  i++;
  sr[i].regid  = 15;
  sr[i].regval = 0x000001a8;
  i++;
  sr[i].regid  = 14;
  sr[i].regval = 0xa5a5a5a5;
  i++;
  sr[i].regid  = 13;
  sr[i].regval = 0x20010100;
  i++;
  sr[i].regid  = 12;
  sr[i].regval = 0xa5a5a5a5;
  i++;
  sr[i].regid  = 7;
  sr[i].regval = 0x2351;
  i++;
  sr[i].regid  = 6;
  sr[i].regval = 0x200201fc;
  i++;
  sr[i].regid  = 3;
  sr[i].regval = 0x00000001;
  i++;
  sr[i].regid  = 2;
  sr[i].regval = 0xa5a5a5a5;
  i++;
  sr[i].regid  = 1;
  sr[i].regval = 0xa5a5a5a5;
  i++;
  sr[i].regid  = 0;
  sr[i].regval = 0x00000000;
  i++;
  sr[i].regid  = -1;
  sr[i].regval = 0;
  res = cmsis_call_rom(cp,sr,SET_TAR | SET_SEL2,DFSR_HALTED | DFSR_BKPT);

  i = 0;
  sr[i].regid  = 20;
  sr[i].regval = 0x02000001;
  i++;
  sr[i].regid  = 18;
  sr[i].regval = 0x20011340;
  i++;
  sr[i].regid  = 16;
  sr[i].regval = 0x01000000;
  i++;
  sr[i].regid  = 15;
  sr[i].regval = 0x000001a8;
  i++;
  sr[i].regid  = 14;
  sr[i].regval = 0xa5a5a5a5;
  i++;
  sr[i].regid  = 13;
  sr[i].regval = 0x20010100;
  i++;
  sr[i].regid  = 7;
  sr[i].regval = 0x2321;
  i++;
  sr[i].regid  = 6;
  sr[i].regval = 0x200201fc;
  i++;
  sr[i].regid  = 3;
  sr[i].regval = 0x1;
  i++;
  sr[i].regid  = 2;
  sr[i].regval = 0xa5a5a5a5;
  i++;
  sr[i].regid  = -1;
  sr[i].regval = 0;
  res = cmsis_call_rom(cp,sr,0,DFSR_HALTED | DFSR_BKPT);

  res = cmsis_free_working_area(cp);
  EXIT("cmsis_final_stack");
  CHKSEQ1(730);

  return res;
}

/*
      [2.763055 232] flash/nor/core.c: get_flash_bank_by_num ENTRY num 0
        [2.763089 232] flash/nor/rp2040.c rp2040_flash_probe ENTRY
          [2.763104 232] flash/nor/rp2040.c rp2040_lookup_symbol ENTRY tag 00005444
*/
int cmsis_read_flash(struct t_cmsisprobe *cp,int num)
{
  ushort deb,debe;
  ushort flash_exit,conn,flash_range_erase;
  ushort flash_range_prog,flash_flush_cache;
  ushort flash_enter_cmd;
  struct t_dap *pdap = cp->pmdap->daps + cp->r_dapidx; 

  ENTRYP("cmsis_read_flash","targesel %08x",pdap->targetsel);

  uint res = cmsis_lookup_sym(cp,FUNC_DEBUG_TRAMPOLINE,&deb,SET_SEL | SET_TAR);
  res = cmsis_lookup_sym(cp,FUNC_DEBUG_TRAMPOLINE_END,&debe,SET_CSW | SET_TAR);
  res = cmsis_lookup_sym(cp,FUNC_FLASH_EXIT_XIP,&flash_exit,SET_CSW | SET_TAR);
  res = cmsis_lookup_sym(cp,FUNC_CONNECT_INTERNAL_FLASH,&conn,SET_CSW | SET_TAR);
  res = cmsis_lookup_sym(cp,FUNC_FLASH_RANGE_ERASE,&flash_range_erase,SET_CSW | SET_TAR);
  res = cmsis_lookup_sym(cp,FUNC_FLASH_RANGE_PROGRAM,&flash_range_prog,SET_CSW | SET_TAR);
  res = cmsis_lookup_sym(cp,FUNC_FLASH_FLUSH_CACHE,&flash_flush_cache,SET_CSW | SET_TAR);
  res = cmsis_lookup_sym(cp,FUNC_FLASH_ENTER_CMD_XIP,&flash_enter_cmd,SET_CSW | SET_TAR);
  res = cmsis_grab_stack(cp);
  res = cmsis_read_flash_id(cp);
  res = cmsis_final_stack(cp);

  EXIT("cmsis_read_flash");
  return res;
}

// server.c add_connection
int cmsis_gdb_connect(struct t_cmsisprobe *cp)
{
  ENTRY("cmsis_gdb_connect");
  int res = LIBUSB_OK;
  int s = get_count();
  cp->r_dapidx = 0;
  res = cmsis_poll(cp,SET_SEL2,POLL_MULTI,DFSR_HALTED,0);
  CHKSEQ1(s + 10);
  cp->r_dapidx = 1;
  res = cmsis_poll(cp,SET_SEL2,POLL_MULTI,DFSR_HALTED,0);
  CHKSEQ1(s + 20);

  cp->r_dapidx = 0;
  res = cmsis_halt(cp,0);
  cp->r_dapidx = 1;
  res = cmsis_halt(cp,0);
  CHKSEQ1(s + 40);
  cp->r_dapidx = 0;
  res = cmsis_poll(cp,SET_SEL2,POLL_MULTI,DFSR_HALTED,0);
  CHKSEQ1(s + 58);
  cmsis_read_flash(cp,0);
  EXIT("cmsis_gdb_connect");
  return res;
}

int cmsis_setup(struct t_cmsisprobe *cp)
{
  struct t_libusb *plu = cp->plu;

  ENTRY("cmsis_setup");

  int res = cmsis_info(cp,DAP_INFO_CAPABILITIES);
  if (res == LIBUSB_OK && plu->rxbuf[1] == 1 /* len */)
  {
    cp->capabilities = get16(plu->rxbuf + 2);
    char buf[100];
    buf[0] = 0;
    if (cp->capabilities & 1) 
      strcpy(buf,"SWD,");
    if (cp->capabilities & 2)
      strcat(buf,"JTAG,");
    if (cp->capabilities & 4)
      strcat(buf,"SWO UART,");
    if (cp->capabilities & 8)
      strcat(buf,"SWO Manchester,");
    if (cp->capabilities & 16)
      strcat(buf,"Atomic,");
    if (cp->capabilities & 32)
      strcat(buf,"Timer,");
    if (cp->capabilities & 64)
      strcat(buf,"SWO Streaming,");
    if (cp->capabilities & 128)
      strcat(buf,"UART,");
    if (cp->capabilities & 256)
      strcat(buf,"USB COM");
    
    logmsg("capabilities %02x=%s ",plu->rxbuf[2],buf);
  } 

  res = cmsis_info(cp,DAP_INFO_FW_VER);
  if (res == LIBUSB_OK && plu->rxbuf[1] > 0)
    logmsg("cmsis proto version: %*.*s",plu->rxbuf[1],plu->rxbuf[1],plu->rxbuf + 2);

  res = cmsis_info(cp,DAP_INFO_SER_NUM);
  if (res == LIBUSB_OK && plu->rxbuf[1] > 0)
    logmsg("cmsis serial #: %*.*s",plu->rxbuf[1],plu->rxbuf[1],plu->rxbuf + 2);

  res = cmsis_connect(cp,DAP_PORT_SWD);
  if (res == LIBUSB_OK)
  {
    logmsg("cmsis SWD connect");
  } else
    DAP_error("ID_DAP_CONNECT",res,__LINE__);

  res = cmsis_info(cp,DAP_INFO_PACKET_SIZE);
  if (res == LIBUSB_OK && plu->rxbuf[1] == 2)
  {
    ushort l = get16(plu->rxbuf + 2);
    logmsg("cmsis max packet size: %d",l);
  } else
    DAP_error("ID_DAP_INFO",res,__LINE__);

  res = cmsis_info(cp,DAP_INFO_PACKET_COUNT);
  if (res == LIBUSB_OK && plu->rxbuf[1] == 1)
  {
    logmsg("cmsis max packet count: %d",plu->rxbuf[2]);
  } else
    DAP_error("ID_DAP_INFO",res,__LINE__);

  res = cmsis_swj_pins(cp,0,0,0);
  res = cmsis_swj_clock(cp);

  res = cmsis_transfer_config(cp,0,64,0);
   
  res = cmsis_swd_config(cp,0);

  res = cmsis_host_status(cp,0,1);
  res = cmsis_host_status(cp,1,1);
// [  0.267958   24] cmsis_dap.c: cmsis_dap_init exit
  CHKSEQ1(24);
// [  0.269963   26] adapter.c: adapter_init exit
  res = cmsis_swj_clock(cp);
  CHKSEQ1(26);

// core.0
  cp->r_dapidx = 0;
  res = cmsis_dap_init_all(cp);
// core.1
  if (cp->ndap > 1)
  {
    cp->r_dapidx = 1;
    res = cmsis_dap_init_all(cp);
    CHKSEQ1(86);
  }

// core.0
  cp->r_dapidx = 0;

  res = cmsis_examine(cp,1);
  CHKSEQ1(126);
// core.1
  if (cp->ndap > 1)
  {
    cp->r_dapidx = 1;
    res = cmsis_examine(cp,1);
    CHKSEQ1(166);
  }
  EXIT("cmsis_setup");

  return res;
}

int cmsis_loop_poll(struct t_cmsisprobe *cp,int n,uint fl1,uint fl2,uint fln)
{
  int res = LIBUSB_OK;
//struct t_libusb *plu = cp->plu;

  ENTRYP("cmsis_loop_poll","n %d",n);

  int s = get_count();

  for (int i = 0; i < n; i++)
  {
    if (cp->plu->log)
      logmsg("cmsis_loop_poll loop i %d seq %d",i,get_count());
    cp->r_dapidx = 0;
    res = cmsis_poll(cp,fl1,POLL_MULTI,DFSR_HALTED,0);
    CHKSEQ1(s + 10 + 20 * i);
    cp->r_dapidx = 1;
    res = cmsis_poll(cp,fl2,POLL_MULTI,DFSR_HALTED,0);
    CHKSEQ1(s + 20 + 20 * i);
    fl1 = fl2 = fln;
  }

  EXIT("cmsis_loop_poll");
  return res; 
}

// target.c target_process_reset 
int cmsis_reset(struct t_cmsisprobe *cp)
{
  int res = LIBUSB_OK;

  cp->r_dapidx = 0;
  res = cmsis_examine(cp,0);
  cp->r_dapidx = 1;
  res = cmsis_examine(cp,0);

  return res;
}

// [4.462579 894] target.c: handle_target_reset ENTRY
int cmsis_target_reset(struct t_cmsisprobe *cp)
{
  int res = LIBUSB_OK;

  ENTRY("cmsis_target_reset");

  res = cmsis_swj_pins(cp,0xa0,0xa0,0);
  
  EXIT("cmsis_target_reset");
  return res;
}

// cortex_m_assert_reset
int cmsis_assert_reset(struct t_cmsisprobe *cp)
{
  int res = LIBUSB_OK;

  ENTRY("cmsis_assert_reset");
  res = cmsis_multi_select(cp);
  res = cmsis_read_32_a(cp,DHCSR_ADD,&cp->dhcsr,SET_CSW2 | SET_TAR | SET_SEL2); 
//  res = cmsis_write_32(cp,DCRDR_ADD,0,SET_TAR); 
// write DCRDR = 0
//       DEMCR = 0x01000501
  res = cmsis_write_mem32_x(cp,0,0x01000501);
  res = cmsis_write_32(cp,AIRCR_ADD,0x05fa0004,SET_SEL | SET_TAR | SET_SEL2 | USE_BD3); 
  res = cmsis_dp_init_con(cp,0);
  uint aircr;
  res = cmsis_read_32_x(cp,AIRCR_ADD,&aircr); 
  struct t_dap *pdap = cp->pmdap->daps + cp->c_dapidx; 
  pdap->status = S_RESET;
  logmsg("cmsis_assert_reset aircr %08x set status RESET targetsel %08x",
         aircr,pdap->targetsel);
  EXIT("cmsis_assert_reset");

  return res;
}

int cmsis_step1(struct t_cmsisprobe *cp,uint add,int idx,uint *regs)
{
  ENTRY("cmsis_step1");
  int res = cmsis_set_break(cp,add,idx);
  res = cmsis_debug_halt_mask(cp, SET_AUTO, DHCSR_C_DEBUGEN);
  res = cmsis_read_32_a(cp,DHCSR_ADD,&cp->dhcsr,0); 
  res = cmsis_unset_break(cp,add,idx);
  res = cmsis_debug_halt_mask(cp, SET_AUTO, DHCSR_C_DEBUGEN | DHCSR_C_HALT | DHCSR_C_MASKINTS);
  res = cmsis_debug_halt_mask(cp, SET_AUTO, DHCSR_C_DEBUGEN | DHCSR_C_HALT | DHCSR_C_MASKINTS);
  res = cmsis_debug_halt_mask(cp, SET_AUTO, DHCSR_C_DEBUGEN | DHCSR_C_STEP | DHCSR_C_MASKINTS);
  res = cmsis_debug_halt_mask(cp, SET_AUTO, DHCSR_C_DEBUGEN | DHCSR_C_HALT | DHCSR_C_STEP | DHCSR_C_MASKINTS);
  res = cmsis_debug_halt_mask(cp, SET_AUTO, DHCSR_C_DEBUGEN | DHCSR_C_HALT | DHCSR_C_STEP);
  res = cmsis_read_32_a(cp,DHCSR_ADD,&cp->dhcsr,0); 
  res = cmsis_debug_entry(cp,DFSR_BKPT | DFSR_HALTED,1,regs);

  EXIT("cmsis_step1");
  return res;
}

int cmsis_step(struct t_cmsisprobe *cp)
{
  int res;
  uint regs[0x60];

  ENTRY("cmsis_step");
  res = cmsis_write_32(cp,FP_COMPn_ADD(0),0x800000e9,SET_TAR); 
  CHKSEQ1(1046);
  if (cp->ndap > 1)
  {
    cp->r_dapidx = 1;
    res = cmsis_multi_select(cp);
    res = cmsis_write_32(cp,FP_COMPn_ADD(0),0x800000e9,SET_TAR); 
    CHKSEQ1(1056);
    cp->r_dapidx = 0;
    res = cmsis_multi_select(cp);
    res = cmsis_debug_halt_mask(cp,SET_TAR | SET_SEL2,DHCSR_C_DEBUGEN);
    CHKSEQ1(1066);
  }
  res = cmsis_read_32_a(cp,DHCSR_ADD,&cp->dhcsr,0); 
  logmsg("cmsis_step dhcsr %08x",cp->dhcsr);
//     [4.612931 1068] cortex_m.c: cortex_m_unset_breakpoint ENTRY
  CHKSEQ1(1068);
  res = cmsis_write_32(cp,FP_COMPn_ADD(0),0x00000000,SET_SEL | SET_TAR); 
  if (cp->ndap > 1)
  {
    CHKSEQ1(1070);
    cp->r_dapidx = 1;
    res = cmsis_multi_select(cp);
    res = cmsis_write_32(cp,FP_COMPn_ADD(0),0x00000000,SET_TAR); 
    CHKSEQ1(1080);
    cp->r_dapidx = 0;
    res = cmsis_multi_select(cp);
  }
  res = cmsis_debug_halt_mask(cp,
                              SET_TAR | SET_SEL2,
                              DHCSR_C_DEBUGEN | DHCSR_C_HALT | DHCSR_C_MASKINTS);
  CHKSEQ1(1090);
  res = cmsis_debug_halt_mask(cp,
                              0,
                              DHCSR_C_DEBUGEN | DHCSR_C_HALT | DHCSR_C_MASKINTS);
  CHKSEQ1(1092);
  res = cmsis_debug_halt_mask(cp,
                              0,
                              DHCSR_C_DEBUGEN | DHCSR_C_STEP | DHCSR_C_MASKINTS);
  res = cmsis_debug_halt_mask(cp,
                              0,
                              DHCSR_C_DEBUGEN | DHCSR_C_STEP | DHCSR_C_HALT | DHCSR_C_MASKINTS);
  CHKSEQ1(1096);
  res = cmsis_debug_halt_mask(cp,
                              0,
                              DHCSR_C_DEBUGEN | DHCSR_C_STEP | DHCSR_C_HALT );
  res = cmsis_read_32_a(cp,DHCSR_ADD,&cp->dhcsr,0); 
  logmsg("cmsis_step dhcsr %08x",cp->dhcsr);
  CHKSEQ1(1100);
  logmsg("cmsis_step call cmsis_debug_entry dfsr %08x",DFSR_BKPT | DFSR_HALTED);
  res = cmsis_debug_entry(cp,DFSR_BKPT | DFSR_HALTED,1,regs);
  CHKSEQ1(1116);

  EXIT("cmsis_step");

  return res;
}

int cmsis_step_fast(struct t_cmsisprobe *cp)
{
  int res = LIBUSB_OK;
//struct t_libusb *plu = cp->plu;

  ENTRY("cmsis_step_fast");

  res = cmsis_read_32_a(cp,DHCSR_ADD,&cp->dhcsr,SET_SEL2); 
  if (cp->plu->log)
    logmsg("cmsis_step_fast: dhcsr %08x",cp->dhcsr);
// clear_debug_cause_bits [cortex_m]
  res = cmsis_write_32_a(cp,DHCSR_ADD,DFSR_EXTERNAL | DFSR_VCATCH |
                            DFSR_DWTTRAP | DFSR_BKPT | DFSR_HALTED,SET_AUTO);
/*
  here: remove/insert breakpoint
*/
// step after # Get current state.
  uint maskints = cp->dhcsr & DHCSR_C_MASKINTS;
  uint pmov = cp->dhcsr & DHCSR_C_PMOV;
  cp->dhcsr = DHCSR_DBGKEY | DHCSR_C_DEBUGEN | DHCSR_C_STEP | pmov;
// if (something..) // TODO check
  res = cmsis_write_32_a(cp, DHCSR_ADD, cp->dhcsr | DHCSR_C_HALT,SET_AUTO);
// step starting loop
  while (1)
  {
    res = cmsis_write_32_a(cp, DHCSR_ADD, cp->dhcsr,SET_AUTO);
    res = cmsis_read_32_a(cp,DHCSR_ADD,&cp->dhcsr,SET_AUTO); 
    if (cp->plu->log)
      logmsg("cmsis_step_fast: dhcsr %08x",cp->dhcsr);
    if (cp->dhcsr & DHCSR_C_HALT)
      break;
    else
      error("unexpected @%d",__LINE__);
  }
// if (something..) // TODO check
  res = cmsis_write_32_a(cp, DHCSR_ADD, DHCSR_DBGKEY | DHCSR_C_DEBUGEN |
                             DHCSR_C_HALT | maskints | pmov,SET_AUTO);
  EXIT("cmsis_step_fast");
  return res;
}

int cmsis_gdb_do(struct t_cmsisprobe *cp)
{
  int    res;
  ushort s[2];
  uint   v;

  ENTRY("cmsis_gdb_do");
  CHKSEQ1(730);
  cp->r_dapidx = 0;
  res = cmsis_poll(cp,SET_TAR | SET_SEL2,0,DFSR_HALTED,0);
  cp->r_dapidx = 1;
  CHKSEQ1(732);
  res = cmsis_poll(cp,SET_SEL2,POLL_MULTI,DFSR_HALTED,0);
  cp->r_dapidx = 0;
  res = cmsis_poll(cp,SET_TAR | SET_SEL2,0,DFSR_HALTED,0);
  CHKSEQ1(758);
  cp->r_dapidx = 1;
  CHKSEQ1(732);
  res = cmsis_poll(cp,SET_SEL2,POLL_MULTI,DFSR_HALTED,0);
  res = cmsis_multi_select(cp);
  cp->r_dapidx = 0;
  res = cmsis_read_mem16(cp,0x20007946,2,s,SET_CSW | SET_TAR | GET_RDBUFF);
  CHKSEQ1(768);
  res = cmsis_poll(cp,SET_CSW | SET_TAR | SET_SEL2,0,DFSR_HALTED,0);
//res = cmsis_loop_poll(cp,1,SET_SEL2,SET_SEL2,SET_SEL2,0);
  CHKSEQ1(770);
  cp->r_dapidx = 1;
  res = cmsis_poll(cp,SET_SEL2,POLL_MULTI,DFSR_HALTED,0);
  CHKSEQ1(780);
  cp->r_dapidx = 0;
  res = cmsis_multi_select(cp);
  res = cmsis_read_16(cp,0x20007946,s,SET_CSW | SET_TAR);
  CHKSEQ1(790);
  res = cmsis_poll(cp,SET_CSW | SET_TAR | SET_SEL2,0,DFSR_HALTED,0);
  CHKSEQ1(792);
  cp->r_dapidx = 1;
  res = cmsis_poll(cp,SET_SEL2,POLL_MULTI,DFSR_HALTED,0);
  CHKSEQ1(802);
  cp->r_dapidx = 0;
  res = cmsis_reset(cp);
  CHKSEQ1(826);
  cp->r_dapidx = 0;
  res = cmsis_assert_reset(cp);
  CHKSEQ1(850);
  cp->r_dapidx = 1;
  res = cmsis_assert_reset(cp);
  CHKSEQ1(874);
  res = cmsis_target_reset(cp);
  CHKSEQ1(876);
  cp->r_dapidx = 0;
  res = cmsis_poll(cp,SET_TAR | SET_SEL2,POLL_MULTI | POLL_STICKY,DFSR_HALTED,0);
  CHKSEQ1(886);
  res = cmsis_poll(cp,0,0,DFSR_HALTED | DFSR_VCATCH,0);
  CHKSEQ1(934);
  cp->r_dapidx = 1;
  res = cmsis_poll(cp,SET_TAR | SET_SEL2,POLL_MULTI | POLL_STICKY,DFSR_HALTED | DFSR_VCATCH,0);
  CHKSEQ1(944);
  res = cmsis_poll(cp,0,0,DFSR_HALTED | DFSR_VCATCH,0);
  CHKSEQ1(992);
  cp->r_dapidx = 0;
  res = cmsis_poll(cp,SET_SEL2,POLL_MULTI,DFSR_HALTED,0);
  CHKSEQ1(1002);
  cp->r_dapidx = 1;
  res = cmsis_poll(cp,SET_SEL2,POLL_MULTI,DFSR_HALTED,0);
  CHKSEQ1(1012);
  cp->r_dapidx = 0;
  res = cmsis_poll(cp,SET_SEL2,POLL_MULTI,DFSR_HALTED,0);
  CHKSEQ1(1022);
  cp->r_dapidx = 1;
  res = cmsis_poll(cp,SET_SEL2,POLL_MULTI,DFSR_HALTED,0);
  CHKSEQ1(1032);
  uint val[16];
  cp->r_dapidx = 0;
  res = cmsis_multi_select(cp);
  res = cmsis_read_mem32(cp,0x20007880,SIZE(val),val,SET_TAR | GET_RDBUFF); 
  CHKSEQ1(1044);
  cmsis_step(cp);  
  CHKSEQ1(1116);
  res = cmsis_poll(cp,0,0,DFSR_HALTED | DFSR_VCATCH,0);
  CHKSEQ1(1118);
  res = cmsis_poll(cp,0,0,DFSR_HALTED | DFSR_VCATCH,0);
  CHKSEQ1(1120);
  cp->r_dapidx = 1;
  res = cmsis_poll(cp,SET_TAR | SET_SEL2,POLL_MULTI,DFSR_HALTED,0);
  CHKSEQ1(1130);
  cp->r_dapidx = 0;
  res = cmsis_multi_select(cp);
  res = cmsis_read_32(cp,0xec,&v,SET_TAR);
  CHKSEQ1(1140);
  res = cmsis_read_16(cp,0xec,s,SET_CSW | SET_TAR);
  CHKSEQ1(1142);
/*
[4.766404 1142] gdb_server.c: gdb_get_packet EXIT buffer p11,2   (0)
[4.766418 1142] arm_adi_v5.c: mem_ap_write_u32 ENTRY add e000edf4 val 00000011
*/
//res = cmsis_write_32(cp,DCRSR_ADD,0x00000011,SET_TAR); 
/* TODO W + R + R combined
  W DCRSR (BD1 = TAR + 4)
  R DHCSR (BD0 = TAR)
  R DCRDR (BD2 = TAR + 4)
*/
  res = cmsis_que_cmd(cp,DAP_MEMAP_CSW,DAP_WO,0xa2000012,0,4);
  res = cmsis_que_cmd(cp,DAP_MEMAP_TAR,DAP_WO,DHCSR_ADD,0,4);
  res = cmsis_que_cmd(cp,DAP_DP_SELECT,DAP_WO,0x13,0,4);
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD1,DAP_WO,0x11,0,4);  
  uint dcrdr;
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD0,DAP_RO,0,&cp->dhcsr,4);
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD2,DAP_RO,0,&dcrdr,4);
  res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4);
  res = cmsis_que_flush(cp,1);
  logmsg("cmsis_gdb_do dhcsr %08x dcrdr %08x",cp->dhcsr,dcrdr);
/* TODO W + R + R combined
*/
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD1,DAP_WO,0x12,0,4);  
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD0,DAP_RO,0,&cp->dhcsr,4);
  res = cmsis_que_cmd(cp,DAP_MEMAP_BD2,DAP_RO,0,&dcrdr,4);
  res = cmsis_que_cmd(cp,DAP_DP_RDBUFF,DAP_RO,0,0,4);
  res = cmsis_que_flush(cp,1);
  logmsg("cmsis_gdb_do dhcsr %08x dcrdr %08x",cp->dhcsr,dcrdr);
  CHKSEQ1(1146);
  res = cmsis_read_32(cp,0xec,&v,SET_SEL | SET_TAR);
  CHKSEQ1(1148);
  res = cmsis_read_16(cp,0xec,s,SET_CSW | SET_TAR);
  CHKSEQ1(1150);
  res = cmsis_poll(cp,SET_CSW | SET_TAR | SET_SEL2,0,DFSR_HALTED,0);
  CHKSEQ1(1152);
  cp->r_dapidx = 1;
  res = cmsis_poll(cp,SET_SEL2,POLL_MULTI,DFSR_HALTED,0);
  CHKSEQ1(1162);
  cp->r_dapidx = 0;
  res = cmsis_poll(cp,SET_SEL2,POLL_MULTI,DFSR_HALTED,0);
  CHKSEQ1(1172);
  cp->r_dapidx = 1;
  res = cmsis_multi_select(cp);
  res = cmsis_read_32_a(cp,DHCSR_ADD,&cp->dhcsr,SET_SEL2); 
  CHKSEQ1(1182);

  EXIT("cmsis_gdb_do");
  return res;
}

int setupex(struct t_cmsisprobe *cp)
{
  struct t_libusb *plu = cp->plu;
  struct t_dap *pdap = cp->pmdap->daps + cp->c_dapidx;

  memset(plu->txbuf,0,plu->bufsize);
  int i = 0;
  plu->txbuf[i++] = ID_DAP_SWJ_SEQUENCE;
  plu->txbuf[i++] = 51;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  int res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_SWJ_SEQUENCE && plu->rxbuf[1] == DAP_OK)
  {  
    if (plu->log)
      logmsg("SWJ sequence 1 OK");
  } else
    DAP_error("SWJ sequence 1",res,__LINE__);

  memset(plu->txbuf,0,plu->bufsize);
  i = 0;
  plu->txbuf[i++] = ID_DAP_SWJ_SEQUENCE;
  plu->txbuf[i++] = 39;
  plu->txbuf[i++] = 0xba;
  plu->txbuf[i++] = 0xbb;
  plu->txbuf[i++] = 0xbb;
  plu->txbuf[i++] = 0x33;
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_SWJ_SEQUENCE && plu->rxbuf[1] == DAP_OK)
  {  
    if (plu->log)
      logmsg("SWJ sequence 1 OK");
  } else
    DAP_error("SWJ sequence 1",res,__LINE__);

  memset(plu->txbuf,0,plu->bufsize);
  i = 0;
  plu->txbuf[i++] = ID_DAP_SWJ_SEQUENCE;
  plu->txbuf[i++] = 136;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0x92;
  plu->txbuf[i++] = 0xf3;
  plu->txbuf[i++] = 0x09;
  plu->txbuf[i++] = 0x62;
  plu->txbuf[i++] = 0x95;
  plu->txbuf[i++] = 0x2d;
  plu->txbuf[i++] = 0x85;
  plu->txbuf[i++] = 0x86;
  plu->txbuf[i++] = 0xe9;
  plu->txbuf[i++] = 0xaf;
  plu->txbuf[i++] = 0xdd;
  plu->txbuf[i++] = 0xe3;
  plu->txbuf[i++] = 0xa2;
  plu->txbuf[i++] = 0x0e;
  plu->txbuf[i++] = 0xbc;
  plu->txbuf[i++] = 0x19;
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_SWJ_SEQUENCE && plu->rxbuf[1] == DAP_OK)
  {  
    if (plu->log)
      logmsg("SWJ sequence 1 OK");
  } else
    DAP_error("SWJ sequence 1",res,__LINE__);

  memset(plu->txbuf,0,plu->bufsize);
  i = 0;
  plu->txbuf[i++] = ID_DAP_SWJ_SEQUENCE;
  plu->txbuf[i++] = 12;
  plu->txbuf[i++] = 0xa0;
  plu->txbuf[i++] = 0x01;
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_SWJ_SEQUENCE && plu->rxbuf[1] == DAP_OK)
  {  
    if (plu->log)
      logmsg("SWJ sequence 1 OK");
  } else
    DAP_error("SWJ sequence 1",res,__LINE__);

  memset(plu->txbuf,0,plu->bufsize);
  i = 0;
  plu->txbuf[i++] = ID_DAP_SWJ_SEQUENCE;
  plu->txbuf[i++] = 51;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_SWJ_SEQUENCE && plu->rxbuf[1] == DAP_OK)
  {  
    if (plu->log)
      logmsg("SWJ sequence 1 OK");
  } else
    DAP_error("SWJ sequence 1",res,__LINE__);

  memset(plu->txbuf,0,plu->bufsize);
  i = 0;
  plu->txbuf[i++] = ID_DAP_SWJ_SEQUENCE;
  plu->txbuf[i++] = 2;
  plu->txbuf[i++] = 0;
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_SWJ_SEQUENCE && plu->rxbuf[1] == DAP_OK)
  {  
    if (plu->log)
      logmsg("SWJ sequence 1 OK");
  } else
    DAP_error("SWJ sequence 1",res,__LINE__);

  i = 0;
  memset(plu->txbuf,0,plu->bufsize);
  plu->txbuf[i++] = ID_DAP_TRANSFER_BLOCK;
  plu->txbuf[i++] = 0;     // SWD ignore
  int count = 1;
  set16(plu->txbuf + i,count); // count
  i += 2;
  i += setreqblock(cp,plu->txbuf + i,DAP_DP_DPIDR,DAP_RO,0,plu->log);
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_TRANSFER_BLOCK)
  {
    if (get16(plu->rxbuf + 1) == count && plu->rxbuf[3] == DAP_TRANSFER_OK)
    {
      if (plu->log)
        logmsg("transfer block OK");
      dap_setreg(pdap,DAP_DP_DPIDR,get32(plu->rxbuf + 4),plu->log ? OUT_MIN : OUT_NONE,DAP_WO,DAP_SEND,0);
    } else if (plu->rxbuf[3] == DAP_TRANSFER_NO_TARGET)
    {
      logmsg("no target");
      DAP_error("transfer block",res,__LINE__);
    } else 
      DAP_error("transfer block",res,__LINE__);
  } else
    DAP_error("transfer block",res,__LINE__);

  return 0;
}

// ID_DAP_SWJ_CLOCK set clock speed 
// ID_DAP_CONNECT connect
// ID_DAP_TRANSFER_CONFIGURE
// ID_DAP_SWJ_SEQUENCE * 3
//
int setup(struct t_cmsisprobe *cp,int speed)
{
  struct t_libusb *plu = cp->plu;
  struct t_dap *pdap = cp->pmdap->daps + cp->c_dapidx;

  if (speed == 0)
    speed = 1000000;

  int res;

  int i = 0;
  memset(plu->txbuf,0,plu->bufsize);
  plu->txbuf[i++] = ID_DAP_SWJ_CLOCK;
  i += set32(plu->txbuf + i,speed); // speed
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_SWJ_CLOCK && plu->rxbuf[1] == DAP_OK)
  {
    if (plu->log)
      logmsg("setup set clock %d OK",speed);
  } else
    DAP_error("set clock",res,__LINE__);

  i = 0;
  memset(plu->txbuf,0,plu->bufsize);
  plu->txbuf[i++] = ID_DAP_CONNECT;
  plu->txbuf[i++] = DAP_PORT_SWD; // SWD 
  res = sendrec1(plu,i,__LINE__);
//memdump(rxbuf,20,0,0);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_CONNECT && plu->rxbuf[1] == DAP_PORT_SWD)
  {
    if (plu->log)
      logmsg("SWD OK"); 
  } else
    DAP_error("set SWD",res,__LINE__);

  memset(plu->txbuf,0,plu->bufsize);
  i = 0;
  plu->txbuf[i++] = ID_DAP_TRANSFER_CONFIGURE; // DAP_TransferConfigure
  plu->txbuf[i++] = 2; // idle cycles
  set16(plu->txbuf + i,0x80); // wait retry
  i += 2;
  set16(plu->txbuf + i,0); // max retry
  i += 2;
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_TRANSFER_CONFIGURE && plu->rxbuf[1] == DAP_OK)
  {
    if (plu->log)
      logmsg("Configure OK");
  } else
    DAP_error("configure",res,__LINE__);

  memset(plu->txbuf,0,plu->bufsize);
  i = 0;
  plu->txbuf[i++] = ID_DAP_SWD_CONFIGURE;  // DAP_SWD_Configure
  plu->txbuf[i++] = 0; // config byte use defaults
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_SWD_CONFIGURE && plu->rxbuf[1] == DAP_OK)
  {  
    if (plu->log)
      logmsg("SWD configure OK");
  } else
    DAP_error("SWD configure",res,__LINE__);

  memset(plu->txbuf,0,plu->bufsize);
  i = 0;
  plu->txbuf[i++] = ID_DAP_SWJ_SEQUENCE;
  plu->txbuf[i++] = 51;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_SWJ_SEQUENCE && plu->rxbuf[1] == DAP_OK)
  {  
    if (plu->log)
      logmsg("SWJ sequence 1 OK");
  } else
    DAP_error("SWJ sequence 1",res,__LINE__);

  memset(plu->txbuf,0,plu->bufsize);
  i = 0;
  plu->txbuf[i++] = ID_DAP_SWJ_SEQUENCE;
  plu->txbuf[i++] = 16;
  plu->txbuf[i++] = 0x9e;
  plu->txbuf[i++] = 0xe7;
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_SWJ_SEQUENCE && plu->rxbuf[1] == DAP_OK)
  {  
    if (plu->log)
      logmsg("SWJ sequence 2 OK");
  } else
    DAP_error("SWJ sequence 2",res,__LINE__);
  
  memset(plu->txbuf,0,plu->bufsize);
  i = 0;
  plu->txbuf[i++] = ID_DAP_SWJ_SEQUENCE;
  plu->txbuf[i++] = 51;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  plu->txbuf[i++] = 0xff;
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_SWJ_SEQUENCE && plu->rxbuf[1] == DAP_OK)
  {  
    if (plu->log)
      logmsg("SWJ sequence 3 OK");
  } else
    DAP_error("SWJ sequence 3",res,__LINE__);

  memset(plu->txbuf,0,plu->bufsize);
  i = 0;
  plu->txbuf[i++] = ID_DAP_SWJ_SEQUENCE;
  plu->txbuf[i++] = 8;
  plu->txbuf[i++] = 0;
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_SWJ_SEQUENCE && plu->rxbuf[1] == DAP_OK)
  {  
    if (plu->log)
      logmsg("SWJ sequence 4 OK");
  } else
    DAP_error("SWJ sequence 4",res,__LINE__);

  i = 0;
  memset(plu->txbuf,0,plu->bufsize);
  plu->txbuf[i++] = ID_DAP_TRANSFER_BLOCK;
  plu->txbuf[i++] = 0;     // SWD ignore
  int count = 1;
  set16(plu->txbuf + i,count); // count
  i += 2;
  i += setreqblock(cp,plu->txbuf + i,DAP_DP_DPIDR,DAP_RO,0,plu->log);
  res = sendrec1(plu,i,__LINE__);
  if (res == 0 && plu->rxbuf[0] == ID_DAP_TRANSFER_BLOCK)
  {
    if (get16(plu->rxbuf + 1) == count && plu->rxbuf[3] == DAP_TRANSFER_OK)
    {
      if (plu->log)
        logmsg("transfer block OK");
      dap_setreg(pdap,DAP_DP_DPIDR,get32(plu->rxbuf + 4),plu->log ? OUT_MIN : OUT_NONE,DAP_WO,DAP_SEND,0);
    } else if (plu->rxbuf[3] == DAP_TRANSFER_NO_TARGET)
    {
      logmsg("warning: no target; try throu dormient state");
      setupex(cp); // try rp2040
    } else 
      DAP_error("transfer block",res,__LINE__);
  } else
    DAP_error("transfer block",res,__LINE__);

  return 0;
}

static struct t_cpu cpu;

static void disasm_init()
{
  if (cpu.init == 0)
  {
    initdec(&cpu);
    initm();
    initcondt();
    cpu.init = 1;
  }
}

static int dis_read_mem(struct disasm_par *par,uint pc,uchar *code,size_t len)
{
  return cmsis_read_mem32(par->env,pc & ~3,len,(uint *) code,SET_AUTO);
}

void mapregs(uint *reg,struct stlink_reg *p)
{
//logmsg("mapregs R15 %08x",reg[15]);
  for (int i = 0; i < 16; i++)
    p->r[i] = reg[i];

  p->xpsr      = reg[XPSR_IDX];
  uint c       = reg[CONTROL_IDX];
  p->control   = (c >> 24) & MB;
  p->faultmask = (c >> 16) & MB;
  p->basepri   = (c >> 8) & MB;
  p->primask   = c & MB;
}
// single step nstep instruction
// flag:  1  read cpu registers
//        2  read cpu float regs
//
int cmsis_trace_ex(struct t_cmsisprobe *cp,uint nstep,uint flag,uint tracestart)
{
  int    i,j,logregini,logregend;
  char   *ident;
  struct t_stat stat = {0},*p_st;
  struct t_cpu *p_cpu = &cpu;
  double start = dtime();
  uint   regs[0x60];

  disasm_init();
  p_st  = &stat;

  ENTRYP("cmsis_trace_ex","nstep %ud flag %d tracestart %ud",nstep,flag,tracestart);

  logregini = logregend = nstep > 1;
  p_cpu->logflags = 1;
  if (cp->plu->log)
    logmsg("trace nstep %u flag %x tracestart %d",nstep,flag,tracestart);
  p_cpu->logd = 0;
  p_st->prefix = 0;
  p_cpu->execcount = 10;

  if (logregini)
  {
    logmsg("#### initial register values:");
    cmsis_read_regs_f(cp,SET_AUTO,regs,0);
    for (int i = 0; i < SIZE(regmap); i++)
      printf("%-8s: %08x\n",regname[i],regs[i]);
    mapregs(regs,&p_cpu->regp0);
  }

  ident = "                                    ";
  for (i = 0; i < nstep; i++)
  {
//  printf("%d/%d\n",i,nstep);
    p_cpu->N = (p_cpu->regp0.xpsr >> 31) & 1;
    p_cpu->Z = (p_cpu->regp0.xpsr >> 30) & 1;
    p_cpu->C = (p_cpu->regp0.xpsr >> 29) & 1;
    p_cpu->V = (p_cpu->regp0.xpsr >> 28) & 1;
    p_cpu->IT = ((p_cpu->regp0.xpsr >> 25) & 3) | (((p_cpu->regp0.xpsr >> 10) & 0x3f) << 2);
    p_cpu->ISR = p_cpu->regp0.xpsr & 0x1ff;
    printf("%8d ",i + tracestart);
    struct disasm_par dp = {.env = cp, .fread_mem = dis_read_mem};
    disasm(&dp,p_cpu,p_cpu->regp0.r[15],p_st);

//    disasm(cp,p_cpu,p_cpu->regp0.r[15],p_st);
    putchar('\n');

    cmsis_step_fast(cp);
    if (flag)
    {
      if (flag & 1)
      {
        cmsis_read_regs_f(cp,SET_AUTO,regs,3);
        mapregs(regs,&p_cpu->regp);
      }
      if (p_cpu->regp.xpsr != p_cpu->regp0.xpsr)
      {
        int N,Z,C,V,ISR,IT;

        N = (p_cpu->regp.xpsr >> 31) & 1;
        Z = (p_cpu->regp.xpsr >> 30) & 1;
        C = (p_cpu->regp.xpsr >> 29) & 1;
        V = (p_cpu->regp.xpsr >> 28) & 1;
        IT = ((p_cpu->regp.xpsr >> 25) & 3) | (((p_cpu->regp.xpsr >> 10) & 0x3f) << 2);
        ISR = p_cpu->regp.xpsr & 0x1ff;
 
        printf("%s",ident);
        if (p_cpu->N != N ||
            p_cpu->Z != Z ||
            p_cpu->C != C ||
            p_cpu->V != V)
        {   
          printf("setflags ");
          if (p_cpu->N)   printf("N");
          else            printf("-");
          if (p_cpu->Z)   printf("Z");
          else            printf("-");
          if (p_cpu->C)   printf("C");
          else            printf("-");
          if (p_cpu->V)   printf("V");
          else            printf("-");
          if (p_cpu->ISR) printf("I");
          else            printf("-");

          printf("  -> ");

          if (N)          printf("N");
          else            printf("-");
          if (Z)          printf("Z");
          else            printf("-");
          if (C)          printf("C");
          else            printf("-");
          if (V)          printf("V");
          else            printf("-");
          if (p_cpu->ISR) printf("I");
          else            printf("-");
          printf(" ");
        } else if (IT != p_cpu->IT)
          printf("IT %x -> %x ",p_cpu->IT,IT);
        else if (ISR != p_cpu->ISR)
          printf("ISR %x -> %x ",p_cpu->ISR,ISR);
        printf("%08x\n", p_cpu->regp.xpsr);
      }
      if (p_cpu->regp.main_sp != p_cpu->regp0.main_sp && p_cpu->regp.main_sp != p_cpu->regp.r[SP])
        printf("%smain_sp    %08x -> %08x\n",ident,p_cpu->regp0.main_sp,p_cpu->regp.main_sp);
      if (p_cpu->regp.process_sp != p_cpu->regp0.process_sp && p_cpu->regp.process_sp != p_cpu->regp.r[SP])
        printf("%ssetpsp     %08x -> %08x\n",ident,p_cpu->regp0.process_sp,p_cpu->regp.process_sp);
      if (p_cpu->regp.rw != p_cpu->regp0.rw && p_cpu->regp.rw != p_cpu->regp.r[SP])
        printf("%srw         %08x -> %08x\n",ident,p_cpu->regp0.rw,p_cpu->regp.rw);
      if (p_cpu->regp.rw2 != p_cpu->regp0.rw2 && p_cpu->regp.rw2 != p_cpu->regp.r[SP])
        printf("%ssetprimask %d -> %d\n",ident,p_cpu->regp0.rw2,p_cpu->regp.rw2);
      if (p_cpu->regp.control != p_cpu->regp0.control)
        printf("%scontrol    %02x -> %02x\n",ident,p_cpu->regp0.control,p_cpu->regp.control);
      if (p_cpu->regp.faultmask != p_cpu->regp0.faultmask)
        printf("%sfaultmask  %08x -> %08x\n",ident,p_cpu->regp0.faultmask,p_cpu->regp.faultmask);
      if (p_cpu->regp.basepri != p_cpu->regp0.basepri)
        printf("%sbasepri    %08x -> %08x\n",ident,p_cpu->regp0.basepri,p_cpu->regp.basepri);
      if (p_cpu->regp.primask != p_cpu->regp0.primask)
        printf("%ssetprimask %d -> %d\n",ident,p_cpu->regp0.primask,p_cpu->regp.primask);
      for (j = 0; j < 15; j++) // general register without PC
      {
        if (p_cpu->regp.r[j] != p_cpu->regp0.r[j])
        {
          printf("%sreg        W       R%02d: %08x -> %08x\n",
                 ident,j, p_cpu->regp0.r[j], p_cpu->regp.r[j]);
        }
      }
    } else
    { // read only PC
/*
      err = stlink_read_reg(sl, 15, &p_cpu->regp);
      CHKERR(err,"stlink_read_all_regs");
*/    ;
    }
    p_cpu->regp0 = p_cpu->regp;
  }
  if (logregend)
  {
    printf("#### final register values:\n");
    cmsis_read_regs_f(cp,SET_AUTO,regs,0);
    for (int i = 0; i < SIZE(regmap); i++)
      printf("%-8s: %08x\n",regname[i],regs[i]);
  }
  if (nstep > 1)
    printf("%d instruction(s) in %.6fs\n",nstep,dtime() - start);
  EXIT("cmsis_trace_ex");
  return 0;
}

struct rep_ctx
{
  int   lineno;
  int   dir;
  int   nl,nmsg,len;
  int   pos,allo;
  uchar *buf;
};

int replay_line(struct rep_ctx *rc,char *line,int dir,struct t_cmsisprobe *sp)
{
  int  n,l,off,v;
  char *p = strstr(line,"dumpbuf");
  int  err = 0;

  if (p)
  {
    rc->nl  = 1;
    rc->dir = dir;
    rc->pos = 0;
    n = sscanf(p,"dumpbuf %d len %d",&rc->nmsg,&rc->len);
//  printf("replay_line dir %d n %d nmsg %d len %d\n",dir,n,rc->nmsg,rc->len);
    if (n != 2)
    {
//    logmsg("replay_line: error line %d",rc->lineno);
      err = 1;
    }
  } else
  {
    n = sscanf(line + 3,"%x:%n",&off,&l);
//  printf("line: %d %x %d %s",n,off,l,line + 3 + l);
    if (n == 1)
    {
      if (off != rc->pos)
      {
//      logmsg("replay_line: wrong offset line %d",rc->lineno);
        err = 2;
        return err;
      }
      for (p = line + 3 + l; *p ; p += 3)
      {
        n = sscanf(p," %x",&v);
        if (n != 1) 
          break;
        rc->buf[rc->pos++] = v;
      }
      if (err)
        return err;
      if (rc->pos == rc->len)
      {
        if (sp->plu->log)
        {
          logmsg("replay_line read buffer %d bytes",rc->len);
          memdump(rc->buf,rc->len,dir ? "R>> " : "R<< ",1);
        }
        if (dir)
        {
          if (sp->plu->handle) // connection ?
            dap_send(sp->plu,rc->buf,rc->len,__LINE__);
          else // no noccection; only dump the message
            decodedap(rc->buf,rc->len,&sp->plu->dec,1);
        } else
        {
          int res = 0;
          if (sp->plu->handle) // connection ?
          {
            dap_receive(sp->plu,&res,__LINE__);
            int nerr = res != rc->len;
            for (int i = 0; i < rc->len; i++)
              nerr += rc->buf[i] != sp->plu->rxbuf[i];
            if (nerr > 0)
            {
              logmsg("buffer difference found len %d %d nerr %d",res,rc->len,nerr);
              if (!sp->plu->log)
              {
                memdump(rc->buf,rc->len,dir ? "R>> " : "R<< ",1);
                // decoderepdap(rc->buf,rc->len,&sp->plu->dec,1,1);
                memdump(sp->plu->rxbuf,res,dir ? "B>> " : "B<< ",1);
                decoderepdap(sp->plu->rxbuf,res,&sp->plu->dec,1,1);
              }  
            } else if (sp->plu->log)
              logmsg("check buffer OK len %d");
          } else
            decoderepdap(rc->buf,rc->len,&sp->plu->dec,1,0);
        }
      } else if (rc->pos > rc->len)
        err = 4;
    } else
    {
//    logmsg("replay_line: error line %d",rc->lineno);
      err = 5; 
    }
  }
  return err;
}

void replay(char *fn,struct t_cmsisprobe *sp)
{
  char line[2000];
  char *p;
  struct rep_ctx rc = {0};

  rc.allo = 64;
  rc.buf = malloc(rc.allo);
  rc.lineno = 1;
  FILE *pf = fopen(fn,"r");
  if (pf)
  {
    logmsg("replay: file %s",fn);
    while ((p = fgets(line,sizeof line,pf)))
    {
      if (p[0] == 'B')
      {
        if ((p[1] == '>' && p[2] == '>') ||
            (p[1] == '<' && p[2] == '<'))
        {
          int e = replay_line(&rc,line,p[1] == '>',sp);
          if (e)
          {
            logmsg("replay error %d line %d",e,rc.lineno);
            break;
          }
        }
      }
      rc.lineno++;
    }
    fclose(pf);
  } else
    logmsg("replay: can't open file %s",fn);
  free(rc.buf);
}

int connect_usb(struct t_cmsisprobe *sp,uint vendor,uint product,int timeout,int speed,int lverbose,int ndap)
{
  struct t_libusb *plu = sp->plu;
  const struct libusb_version* version = libusb_get_version();
  logmsg("Using libusb v%d.%d.%d.%d vendor %04x product %04x",
         version->major, version->minor, version->micro, version->nano,
         vendor,product);

  int ret = libusb_init(&plu->ctx);
  CHK_LIBUSB("libusb_init",ret);

  if (ret != 0)
    return ret;

  libusb_set_log_cb(plu->ctx,flog,LIBUSB_LOG_CB_GLOBAL);
  if (lverbose)
    libusb_set_option(plu->ctx,LIBUSB_OPTION_LOG_LEVEL,lverbose);

  usb_connect(plu,vendor,product);
  if (plu->handle)
  {
    plu->iface = -1;
    if (vendor == 0x2e8a && product == 4) // old picoprobe
    {
      plu->iface = 2;
      claim(plu->ctx,plu->handle,plu->iface);
      plu->epw = 4;
      plu->epr = 5 + 0x80;
      plu->timeout = timeout;
      plu->bufsize = 1024;
      logmsg("epw %2x epr %2x timeout %d bufsize %d",plu->epw,plu->epr,plu->timeout,plu->bufsize);
      plu->txbuf = calloc(plu->bufsize,sizeof plu->txbuf[0]);
      plu->rxbuf = calloc(plu->bufsize,sizeof plu->rxbuf[0]);
      struct t_picoprobe sp;
      sp.plu = plu;
      sp.r_dapidx = 0;
      sp.c_dapidx = 0;
      sp.ndap  = ndap;
      sp.pmdap = new_multidap(ndap);
      sp.pmdap->daps[0].targetsel = 0x01002927;
      initdapreg(sp.pmdap->daps[0].regs);
      sp.pmdap->daps[1].targetsel = 0x11002927;
      initdapreg(sp.pmdap->daps[1].regs);
      pico_connect(&sp);
//    pico_test_dbg(&sp);
//    pico_test_dbg1(&sp);
//    pico_test_dbg2(&sp);
//    read rom to a file
//    pico_read(&sp,0,16 * 1024,"bootrom.bin");
/* trace N instruction */
      pico_trace(&sp,0);
      pico_trace_ex(&sp,10,1,0);
//    pico_set_break(&sp,0x00002518);
      pico_set_break(&sp,0x00002444);
      pico_cont(&sp);
      pico_rem_break(&sp);
      pico_trace_ex(&sp,30,1,5158);
      pico_disconnect(&sp);
      exit(0);
    } else
    {
      int buflimit = 1;
      if (vendor == 0x2e8a) // picoprobe
      {
        plu->iface = 0;
        claim(plu->ctx,plu->handle,plu->iface); // interface 0 // TODO fix
        plu->epw = 4;
        plu->epr = 0x85;
        buflimit = 1;
      } else
      {
        plu->iface = 2;
        claim(plu->ctx,plu->handle,plu->iface);
        plu->epw = 3;
        plu->epr = 3 + 0x80;
        buflimit = 4;
      }
      plu->timeout = timeout;
      plu->bufsize = 64; // TODO et from interface
      logmsg("epw %2x epr %2x timeout %d bufsize %d",plu->epw,plu->epr,plu->timeout,plu->bufsize);
      plu->txbuf = calloc(plu->bufsize,sizeof plu->txbuf[0]);
      plu->rxbuf = calloc(plu->bufsize,sizeof plu->rxbuf[0]);
//    printf("test %d\n",test);
      sp->bufhead = sp->buftail = newbuf(plu);
      sp->bufcount = 1;
      sp->buflimit = buflimit;
      sp->speed = speed;
      sp->r_dapidx = 0;
      sp->c_dapidx = 0;
      sp->ndap = ndap;
      sp->pmdap = new_multidap(ndap);
      if (ndap > 1)
      { // rp2040 specific TODO generalize a bit ...
        sp->pmdap->daps[0].targetsel = 0x01002927;
        initdapreg(sp->pmdap->daps[0].regs);
        sp->pmdap->daps[1].targetsel = 0x11002927;
        initdapreg(sp->pmdap->daps[1].regs);
      }
    }
  }

  return ret;
}

int main(int na,char **v)
{
  int  ret       = -1;
  int  lverbose  = 0; // libusb log
  int  speed     = 0;
  int  timeout   = 1000;
  int  test      = 0;
  int  conn_flag = 1;
  int  vendor    = 0xc251;  
  int  product   = 0xf001;
  char *fin      = 0;
  int  finp      = -1;
  char *frep     = 0;
  int  frepp     = -1;
  int  ndap      = NDAP;
  struct t_libusb slu = {0}, *plu;
  struct t_cmsisprobe sp = {0};
  plu = sp.plu = &slu;

  plu = &slu;
  
  for (int i = 1; i < na; i++)
  {
    if (strcmp(v[i],"-tv") == 0)
      plu->tlog = 0xffffffff;
    else if (strcmp(v[i],"-lv") == 0 && i < na - 1)
      lverbose = atoi(v[++i]);
    else if (strcmp(v[i],"-ndap") == 0 && i < na - 1)
      ndap = atoi(v[++i]);
    else if (strcmp(v[i],"-v") == 0)
    {
      plu->log = 0xffffffff;
      loglev = 1;
    } else if (strcmp(v[i],"-test") == 0)
      test = 1;
    else if (strcmp(v[i],"-test1") == 0)
      test = 1;
    else if (strcmp(v[i],"-test2") == 0)
      test = 2;
    else if (strcmp(v[i],"-test3") == 0)
      test = 3;
    else if (strcmp(v[i],"-nc") == 0)
      conn_flag = 0;
    else if ((strcmp(v[i],"-in") == 0 && i < na - 1))
    {
      finp = i;
      fin = v[++i];
    } else if ((strcmp(v[i],"-vendor") == 0 && i < na - 1))
      sscanf(v[++i],"%i",&vendor);
    else if ((strcmp(v[i],"-product") == 0 && i < na - 1))
      sscanf(v[++i],"%i",&product);
    else if ((strcmp(v[i],"-s") == 0 && i < na - 1))
    {
      speed = atoi(v[++i]);
    } else if ((strcmp(v[i],"-replay") == 0 && i < na - 1))
    {
      frepp = i;
      frep = v[++i];
    } else if ((strcmp(v[i],"-t") == 0 && i < na - 1))
    {
      timeout = atoi(v[++i]);
      if (timeout == 0)
        timeout = 1000;
    } else
      error("unrecognized parameter %s",v[i]); 
  }
  if (plu->log)
  {
    printf("command line:\n");
      for (int i = 0; i < na; i++)
        printf("%s ",v[i]);
    printf("\n");
  }
  if (speed == 0)
    speed = 100000;
  if (ndap > NDAP)
    error("max dap # supported is %d",NDAP);
  initdap(&plu->dec,ndap);
  dap_set_prefix(&plu->dec,"dap >>","dap <<");

  start_time = plu->tstart = dtime();
  arm_initreg(0);
  if (plu->log)
    logmsg("verbose %d lverbose %d tverbose %d speed %d timeout %d test %d",
           plu->log,lverbose,plu->tlog,speed,timeout,test);

  for (int i = 0; i < 256; i++)
    parity[i] = nbit(i) & 1;

  if (conn_flag)
  {
    ret = connect_usb(&sp,vendor,product,timeout,speed,lverbose,ndap);
    if (ret)
      exit(0);
    if (!plu->handle)
      logmsg("no device found (looking for: vendor %x prod %x)",vendor,product);
  }
  if (plu->handle)
  {
    if (frep && (finp == -1 || finp > frepp))
      replay(frep,&sp);
    if (fin)
      jim_file_cmsis(fin,&sp);
    else if (frep == 0)
    {
      printf("test %d\n",test);
      if (test == 1)
        dotest(&sp,plu,plu->handle,3);
      else if (test == 2)
      {
        setup(&sp,speed);
        test3(&sp);
      } else 
      {
        ret = cmsis_setup(&sp);
        ret = cmsis_loop_poll(&sp,2,SET_TAR | SET_SEL2,SET_TAR | SET_SEL2,SET_SEL2);
        ret = cmsis_gdb_connect(&sp);
//      ret = cmsis_loop_poll(&sp,3,SET_TAR | SET_SEL2,SET_TAR | SET_SEL2,SET_SEL2);
        ret = cmsis_gdb_do(&sp);
        ret = cmsis_cleanup(&sp);
      }
    }
    ret = libusb_release_interface(plu->handle,plu->iface);
//  logmsg("libusb_release_interface %d ret %d",plu->iface,ret);
    CHK_LIBUSB("libusb_release_interface",ret);
  } else
  {
    if (!conn_flag && frep)
      replay(frep,&sp);
  }
  if (plu->handle)
    libusb_close(plu->handle);
  if (conn_flag)
    libusb_exit(plu->ctx);
  enddap(&plu->dec);
//dump_dapreg(&s_dap,OUT_MIN);
  if (plu->log)
    arm_dump_allreg(0);
  if (plu->log)
    logmsg("exit");
}

int cmsis_loadprg(struct t_cmsisprobe *cp,const char *prg,uint loadadd,uint inipc,uint inisp,int chkmem)
{
  printf("cmsis_loadprg TODO\n");
  return LIBUSB_OK;
}

int cmsis_runcpu(struct t_cmsisprobe *cp,uint wtime,uint count,uint *mem,int flag)
{
  printf("cmsis_runcpu TODO\n");
  return LIBUSB_OK;
}

#ifdef XX
int runcpu(stlink_t *sl,uint wtime,uint count,uint *mem,int flag)
{
  int  err,ret;
  uint dhcsr;
  uint dwt[20];
  struct stlink_reg regp = {0};

  logmsg("runcpu wtime %d count %d",wtime,count);

  err = stlink_run(sl, RUN_NORMAL);
  CHKERR(err,"stlink_run");
  dhcsr = readdhcsr(sl,1,__LINE__);
  int n = 0;
  int nerr = 0;
  while((dhcsr & 3) != 3 && n < count)
  {
    if (wtime > 0)
      usleep(wtime);
    int st = stlink_status(sl);
    CHKERR(st,"stlink_status");
//  printf("stlink_status %d %d\n",st,sl->core_stat);
    ret = stlink_read_debug32(sl, STLINK_REG_DHCSR, &dhcsr);
    if (ret != 0)
    {
      dhcsr = 0;
      nerr++;
      printf("nerr %d\n",nerr);
      if (nerr > 2)
        break;
    }
//  readdwt(sl,1,n == 0,dwt);
    readdwt(sl,0,n == 0,dwt);
    if (mem)
      mem[n] = dwt[7];
// registers can't be read in running cpu
    if (flag)
    {
      err = stlink_force_debug(sl);
      CHKERR(err,"force_deb");
      err = stlink_read_all_regs(sl, &regp);
      CHKERR(err,"stlink_read_all_regs");
      for (int i = 0; i < 16; i++)
      {
        if (i && i % 4 == 0) putchar('\n');
        printf("R%02d %08x ",i,regp.r[i]);
      }
      putchar('\n');
      printf("xpsr %08x main_sp %08x process_sp %08x rw %08x rw2 %08x\n",
             regp.xpsr,regp.main_sp,regp.process_sp,regp.rw,regp.rw2);
      printf("control %02x faultmask %02x basepri %02x primask %02x fpscr %02x\n",
             regp.control,regp.faultmask,regp.basepri,regp.primask,regp.fpscr);
      printf("\n");
      err = stlink_run(sl, RUN_NORMAL);
      CHKERR(err,"stlink_run");
    }
    n++;
  }

  logmsg("runcpu final dhcsr %08x n %d",dhcsr,n);
  if ((dhcsr & 3) == 3)
    checkfault(sl);
  return 0;
}
#endif

