#ifndef _DRIVER_PICO_H_
#define _DRIVER_PICO_H_

enum
{
  PICO_WRITE = 1,
  PICO_READ,
  PICO_SPEED
};

struct t_picoprobe
{
  struct t_libusb *plu;
  uchar  *pbuf; // current pointer to txbuf
  uint   id;    // only low byte used by protocol
  uint   ap;
  int    ndap;
  struct t_multidap *pmdap;
  int    r_dapidx; // requested index inside dapinfo
  int    c_dapidx; // current
};

void pico_disconnect(struct t_picoprobe *pp);
void pico_rem_break(struct t_picoprobe *pp);
void pico_connect(struct t_picoprobe *pp);
void pico_cont(struct t_picoprobe *pp);
void pico_set_break(struct t_picoprobe *pp,uint add);
int  pico_trace_ex(struct t_picoprobe *pp,uint nstep,uint flag,uint tracestart);
void pico_trace(struct t_picoprobe *pp,int nstep);

#endif
