#ifndef _DAP_REG_H
#define _DAP_REG_H

#define NAP 2
#define DAP_NREG   23

#define OUT_NONE    0
#define OUT_MIN     1
#define OUT_FULL    2
#define OUT_OP_MASK 3
#define OUT_NOCR   16

#define MASK(reg,fn) (reg##_##fn##_MASK)
#define SHIFT(reg,fn)(reg##_##fn##_SHIFT)
#define FIELD(v,reg,fn) (((v) & MASK(reg,fn)) >> SHIFT(reg,fn))

struct t_dap;

#define DAP_REG_VALID_S 1
#define DAP_REG_VALID_R 2

struct t_dap_reg
{
  uint  add;
  char  *name;
  uint  vals; // register write during send
  uint  valr; // register read during receive
  uint  countr,countw;
  uint  wfset; // where first used 
  uint  wlset; // where last  used 
  char  *(* fdump)(struct t_dap_reg *,int fl,int op,int phase,struct buf_env *ppe);
  int   (*fhook)(struct t_dap *pdap,struct t_dap_reg *,uint val,int fl,int op,int phase,int where);
  uchar op; // R, W, RW
  uchar ty; // ap, dp, mem_ap
  uchar dpbanksel;
  uchar flag; // DAP_REG_*
};

struct t_dap_buf
{
  uint  s_mes_id;
// copy of send message
  int   allo,
        len;
  uchar *pd;
// copy of dap regs at start of transmission
  uint  regs0[DAP_NREG];
  uchar flags0[DAP_NREG];
};

#define NMESBUF            (1 << 4)
#define MESBUFMASK      (NMESBUF-1)
#define IDX(i)     (i & MESBUFMASK)

struct t_dap
{
  uint   targetsel;
  uint   idx; // idx in t_multidap
  struct t_multidap *pmdap;
// used by driver-cmsis
  uint status;    
  uchar use_packet;
  uchar dp_banksel;
// count of handled messages (send/rec)
  int   counts,countr;
// send message id;
  struct t_dap_reg regs[DAP_NREG];
  char *s_prefix;
  char *r_prefix;
// ring buffer with ridx read ptr
//                  widx write ptr
// use MESBUFMASK bits as idx
  struct t_dap_buf mesbuf[NMESBUF];
  uint  ridx;
  uint  widx;
};

struct t_multidap
{
  uint ndap;
  uint c_idx;     // current dap idx in use
// used by decode
  uint phase;     // decode phase
  uint targetsel; // targetsel during multidrop switch
// count of handled messages (send/rec) 
  int   counts,countr;
// 
// last field for dynamic array allocation
  struct t_dap daps[0];
};

int    dap_regid(struct t_dap_reg *reg,int ap,int r,int add,int op);
void   dap_setreg(struct t_dap *pdap,int r,int val,int fl,int op,int phase,int where);
void   initdapreg(struct t_dap_reg *reg);
void   dump_dapreg(struct t_dap *pdap,int fl);
char   *dap_regname(struct t_dap *pdap,int ap,int r,int add,int *pid,int op);
void   dap_save(struct t_dap *pdap,int idx);
void   dap_restore(struct t_dap *pdap,int idx);
void   dap_dumpreg(struct t_dap *pdap,int r,char *buf,int fl);
struct t_dap_reg *getdapregbyadd(struct t_dap_reg *reg,uint add,int ty);
struct t_dap_reg *getdapregbyid(struct t_dap_reg *reg,int id);
int    dap_getval(struct t_dap *pdap,int regid, uint *val);

#endif
