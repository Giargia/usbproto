# test
set A 0x11
set B 0x10
set C 0x13

proc exam {a} {
  rem "exam $a"
  read_mem 0xe000e000 4 8
  read_mem 0xe000e100 4 1
  read_mem 0xe000e200 4 1
  read_mem 0xe000e280 4 1
  read_mem 0xe000ed04 4 1
  read_mem 0x20041fc0 4 16 
  read_mem 0x20011100 4 16 
  read_mem 0x200107f0 4 64 
}

setup
check 166 [info source source]
target 0
poll $A 1 1
checkr 10 [info source source]
read_mem 0xe000ed00 4 6
read_mem 0xe000ed24 4 1
read_mem 0xe000e004 4 1
target 1
poll $A 1 1
checkr 10 [info source source]
target 0
poll $B 1 1
checkr 10 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]

target 0
poll $B 1 1
checkr 10 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]
target 0
halt
checkr 10 [info source source]
target 1
halt
checkr 10 [info source source]
target 0
poll $B 1 1
checkr 26 [info source source]
read_flash
checkr 478 [info source source]

poll $A 0 1
checkr 2 [info source source]
target 1
poll $B 1 1
checkr 26 [info source source]

target 0
poll $B 1 1
checkr 10 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]

rem {[  4.134858  798] gdb_server.c: fetch_packet EXIT buffer m20007946,4y-map:read::0,1000}
target 0

exam [info source source]
checkr 10 [info source source]

rem {[  4.147531  808] gdb_server.c: fetch_packet EXIT buffer m20007946,2}
checkr 2 [info source source]

poll $C 0 1
checkr 2 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]

target 0
examine
checkr 12 [info source source]

target 1
examine
checkr 12 [info source source]

target 0
assert_reset
checkr 24 [info source source]

target 1
assert_reset
checkr 24 [info source source]

target_reset
checkr 2 [info source source]

target 0
poll $A 3 1
checkr 10 [info source source]

exam [info source source]
set_break 0x1000021a 0
continue
set_break 0 0
exam [info source source]

set_break 0x20008840 10
continue
set_break 0 10
exam [info source source]
set_break 0x200069a8 11

trace 10 0 
exam [info source source]

continue
set_break 0 11
exam [info source source]

trace 500 164689 
exam [info source source]

rem {
trace 10 0
exam [info source source]
trace 300 0
exam [info source source]
}

rem {
trace 1000 [expr 159626-95] 
exam [info source source]
trace 1000 [expr 159626-95+1000] 
exam [info source source]
trace 3000 [expr 159626-95+2000] 
exam [info source source]
}

cleanup
