# test
# check interrupt
set A 0x11
set B 0x10
set C 0x13

proc exam {a b} {
  rem "exam $a $b"
  read_all_reg 512
  read_mem 0xe000e100 4 1
  read_mem 0xe000e200 4 1
  read_mem 0xe000e280 4 1
  read_mem 0xe000ed04 4 1
  read_mem 0x20041fa0 4 24 
}

proc break_on1 {} {
#  set_break 0x100001c2  0
#  set_break 0x100001c4  1
#  set_break 0x100001c0  2
#  set_break 0x100001cc  3
}

proc break_on2 {} {
#  set_break 0x20006808 10 
# set_break 0x20006834 11 
#  set_break 0x2000688e 11 
#  set_break 0x200068a8 12 
#  set_break 0x200020ed 13 
  set_break 0x20009ce0 14 
#  set_break 0x20006894 15 
#  set_break 0x200068a8 16 
}

proc break_off1 {} {
#  set_break 0  0
#  set_break 0  1
#  set_break 0  2
#  set_break 0  3
}
proc break_off2 {} {
#  set_break 0 10 
#  set_break 0 11 
#  set_break 0 12 
#  set_break 0 13 
  set_break 0 14 
#  set_break 0 15 
#  set_break 0 16 
}

setup
check 166 [info source source]
target 0
poll $A 1 1
checkr 10 [info source source]
exam [info source source] "init" 
target 1
poll $A 1 1
checkr 10 [info source source]
target 0
poll $B 1 1
checkr 10 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]

target 0
poll $B 1 1
checkr 10 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]
target 0
halt
checkr 10 [info source source]
target 1
halt
checkr 10 [info source source]
target 0
poll $B 1 1
checkr 26 [info source source]
read_flash
checkr 478 [info source source]

poll $A 0 1
checkr 2 [info source source]
target 1
poll $B 1 1
checkr 26 [info source source]

target 0
poll $B 1 1
checkr 10 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]
target 0

checkr 10 [info source source]

checkr 2 [info source source]

poll $C 0 1
checkr 2 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]

target 0
examine
checkr 12 [info source source]

target 1
examine
checkr 12 [info source source]

target 0
assert_reset
checkr 24 [info source source]

target 1
assert_reset
checkr 24 [info source source]

target_reset
checkr 2 [info source source]

target 0
poll $A 3 1
checkr 10 [info source source]

exam [info source source] "after reset"
read_mem 0x20000000 4 48 

set_break 0x1000021a 0
continue
set_break 0 0
exam [info source source] "at 0x1000021a break"
read_mem 0x20000000 4 48 
# set_break 0x20008840 10
set_break 0x20009ce0 10
# set_break 0x20007550 10
continue
set_break 0 10 
trace 20 0

#break_on1
#break_on2
set_break 0x200069a8 10
#set_break 0x20009ce0 10
continue
exam [info source source] "exeption"
#break_off1
#break_off2
set_break 0 10
trace 20 0

read_mem 0xe0001000 4 16
read_mem 0xe0002000 4 16
read_mem 0xe0001000 4 16

rem {
exam [info source source] "exeption"
break_off1
break_off2
trace 200 0
break_on1
break_on2
continue
exam [info source source] "2^ exception"
break_off1
break_off2
}

dump_armregs

cleanup
