# test
set A 0x11
set B 0x10
set C 0x13

setup
check 166 [info source source]
target 0
poll $A 1 1
checkr 10 [info source source]
target 1
poll $A 1 1
checkr 10 [info source source]
target 0
poll $B 1 1
checkr 10 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]

target 0
poll $B 1 1
checkr 10 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]
target 0
halt
checkr 10 [info source source]
target 1
halt
checkr 10 [info source source]
target 0
poll $B 1 1
checkr 26 [info source source]
read_flash
checkr 478 [info source source]

poll $A 0 1
checkr 2 [info source source]
target 1
poll $B 1 1
checkr 26 [info source source]

target 0
poll $B 1 1
checkr 10 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]

rem {[  4.134858  798] gdb_server.c: fetch_packet EXIT buffer m20007946,4y-map:read::0,1000}
target 0
read_mem 0x20007946 2 2 
checkr 10 [info source source]

rem {[  4.147531  808] gdb_server.c: fetch_packet EXIT buffer m20007946,2}
read_mem 0x20007946 2  
checkr 2 [info source source]

poll $C 0 1
checkr 2 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]

target 0
examine
checkr 12 [info source source]

target 1
examine
checkr 12 [info source source]

target 0
assert_reset
checkr 24 [info source source]

target 1
assert_reset
checkr 24 [info source source]

target_reset
checkr 2 [info source source]

target 0
poll $A 3 1
checkr 10 [info source source]

poll 0 0 9
checkr 48 [info source source]

target 1
poll $A 3 1
checkr 10 [info source source]

poll 0 0 9
checkr 48 [info source source]

target 0
poll $B 1 1
checkr 10 [info source source]

target 1
poll $B 1 1
checkr 10 [info source source]

target 0
poll $B 1 1
checkr 10 [info source source]

target 1
poll $B 1 1
checkr 10 [info source source]

rem {[4.865815 1052] gdb_server.c: gdb_get_packet EXIT buffer p11md,72657365742068616c74} 
rem {[4.870676 1052] gdb_server.c: gdb_get_packet EXIT buffer m20007880,4065742068616c74}
target 0
read_mem 0x20007880 4 16 
checkr 12 [info source source]

rem {    [4.890436 1064] gdb_server.c: gdb_get_packet EXIT buffer vCont;s:1;c0 }
step
checkr 72 [info source source]
poll 0 0 1
checkr 74 [info source source]
poll 0 0 1
checkr 2 [info source source]

target 1 
poll $A 1 1
checkr 10 [info source source]

rem {[5.042298 1150] gdb_server.c: gdb_get_packet EXIT buffer gCont;s:1;c }
rem {[5.043555 1150] gdb_server.c: gdb_get_packet EXIT buffer mec,4;s:1;c }
target 0
read_mem 0xec 4 1 
checkr 10 [info source source]
read_mem 0xec 2 1 
checkr 2 [info source source]
read_reg 17
read_reg 18
read_mem 0xec 4 1 
read_mem 0xec 2 1 
checkr 8 [info source source]
poll $C 0 1
checkr 2 [info source source]

target 1
poll $B 1 1 
checkr 10 [info source source]

target 0
poll $B 1 1
checkr 10 [info source source]

target 1
poll $B 1 1
checkr 10 [info source source]

dump_armregs
cleanup
