# test
set A 0x11
set B 0x10
set C 0x13

setup
check 166 [info source source]
target 0
poll $A 1 1
checkr 10 [info source source]
read_mem 0xe000e200 4 1 
read_mem 0xe000e280 4 1 
target 1
poll $A 1 1
checkr 10 [info source source]
target 0
poll $B 1 1
checkr 10 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]

target 0
poll $B 1 1
checkr 10 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]
target 0
halt
checkr 10 [info source source]
target 1
halt
checkr 10 [info source source]
target 0
poll $B 1 1
checkr 26 [info source source]
read_flash
checkr 478 [info source source]

poll $A 0 1
checkr 2 [info source source]
target 1
poll $B 1 1
checkr 26 [info source source]

target 0
poll $B 1 1
checkr 10 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]

rem {[  4.134858  798] gdb_server.c: fetch_packet EXIT buffer m20007946,4y-map:read::0,1000}
target 0
read_mem 0x20007946 2 2 
checkr 10 [info source source]

rem {[  4.147531  808] gdb_server.c: fetch_packet EXIT buffer m20007946,2}
read_mem 0x20007946 2  
checkr 2 [info source source]

poll $C 0 1
checkr 2 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]

target 0
examine
checkr 12 [info source source]

target 1
examine
checkr 12 [info source source]

target 0
assert_reset
checkr 24 [info source source]

target 1
assert_reset
checkr 24 [info source source]

target_reset
checkr 2 [info source source]

target 0
poll $A 3 1
checkr 10 [info source source]

read_mem 0xe000e200 4 1 
read_mem 0xe000e280 4 1 
read_mem 0x5011008c 4 4

set_break 0x1000021a 0
continue
set_break 0 0
set_break 0x2454 1
continue
read_mem 0xe000e200 4 1 
read_mem 0xe000e280 4 1 
read_mem 0x5011008c 4 4
set_break 0 1
set_break 0x20002540 10
continue
set_break 0 10
read_mem 0xe000e100 4 1 
read_mem 0xe000e180 4 1 
read_mem 0xe000e200 4 1 
read_mem 0xe000e280 4 1 
read_mem 0x5011008c 4 4
for {set i 0} {$i < 16} {incr i} {
  trace 1 [expr 155611+$i] 
  read_mem 0xe000e100 4 1 
  read_mem 0xe000e180 4 1 
  read_mem 0xe000e200 4 1 
  read_mem 0xe000e280 4 1 
}
read_mem 0x5011008c 4 4

cleanup
