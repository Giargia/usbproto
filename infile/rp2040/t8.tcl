# test
set A 0x11
set B 0x10
set C 0x13

setup
check 166 [info source source]
target 0
poll $A 1 1
checkr 10 [info source source]
target 1
poll $A 1 1
checkr 10 [info source source]
target 0
poll $B 1 1
checkr 10 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]

target 0
poll $B 1 1
checkr 10 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]
target 0
halt
checkr 10 [info source source]
target 1
halt
checkr 10 [info source source]
target 0
poll $B 1 1
checkr 26 [info source source]
read_flash
checkr 478 [info source source]

poll $A 0 1
checkr 2 [info source source]
target 1
poll $B 1 1
checkr 26 [info source source]

target 0
poll $B 1 1
checkr 10 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]

rem {[  4.134858  798] gdb_server.c: fetch_packet EXIT buffer m20007946,4y-map:read::0,1000}
target 0
checkr 10 [info source source]

rem {[  4.147531  808] gdb_server.c: fetch_packet EXIT buffer m20007946,2}
checkr 2 [info source source]

poll $C 0 1
checkr 2 [info source source]
target 1
poll $B 1 1
checkr 10 [info source source]

target 0
examine
checkr 12 [info source source]

target 1
examine
checkr 12 [info source source]

target 0
assert_reset
checkr 24 [info source source]

target 1
assert_reset
checkr 24 [info source source]

target_reset
checkr 2 [info source source]

target 0
poll $A 3 1
checkr 10 [info source source]

read_mem 0xe000e100 4 1
read_mem 0xe000e180 4 1
read_mem 0xe000e200 4 1
read_mem 0xe000e280 4 1
read_mem 0xe000ed04 4 1
# read_mem 0x2001fff0 4 8 
read_all_reg 512
set_break 0x1000021a 0
continue
set_break 0 0
trace 2 0
read_mem 0xe000e100 4 1
read_mem 0xe000e180 4 1
read_mem 0xe000e200 4 1
read_mem 0xe000e280 4 1
read_mem 0xe000ed04 4 1
set_break 0x200069d6 10
continue
set_break 0 10
read_mem 0xe000e100 4 1
read_mem 0xe000e180 4 1
read_mem 0xe000e200 4 1
read_mem 0xe000e280 4 1
read_mem 0xe000ed04 4 1
trace 100 0 
read_mem 0xe000e100 4 1
read_mem 0xe000e180 4 1
read_mem 0xe000e200 4 1
read_mem 0xe000e280 4 1
read_mem 0xe000ed04 4 1
trace 100 0 
read_mem 0xe000e100 4 1
read_mem 0xe000e180 4 1
read_mem 0xe000e200 4 1
read_mem 0xe000e280 4 1
read_mem 0xe000ed04 4 1

cleanup
