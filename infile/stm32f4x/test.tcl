# test
#set add1 0x08000226
#set add2 0x080035e2
#set add3 0x080040e8
#set add4 0x0800022a
#set add5 0x080040ea

set sw 0

if {$sw == 1} {
  set add1 0x080040e8
  set add2 0x0800022a
  set add3 0x080040ea
  set add4 0x0800022a
  set add5 0x080035dc
  set add6 0x080040ee
  set add7 0x08004344
} else {
  set add1 0x080040ea
  set add2 0x0800022a
  set add3 0x080035dc
  set add4 0x080040ee
  set add5 0x080035de
  set add6 0x080040ee
  set add7 0x08000c78
}

proc read_1 {add} {
  set m [expr {$add % 4}]
  if {$m == 0} {
    read_mem $add 4 1
    read_mem $add 2 1
  } else {
    read_mem $add 2 2
    read_mem $add 2 1
  }
}

proc read_2 {add} {
  read_mem $add 2 2
  read_mem $add 2 1
  read_mem $add 2 2
  read_mem $add 2 1
}

init_const
setup
read_mem  0xe0042004 4 1
write_mem 0xe0042004 4 7
read_mem  0xe0042008 4 1
write_mem 0xe0042008 4 0x00001800 

poll $SET_AUTO 0 0
for {set i 0} {$i < 2} {incr i} {
  poll $SET_AUTO 0 0 
}

halt
poll $SET_AUTO 0 1 
read_mem 0xe0042000 4 1
read_mem 0x1fff7a22 2 1
# poll $SET_AUTO 0 0 
read_1 $add1
read_2 $add2
step1 $add1 0
poll $SET_AUTO 0 0 
poll $SET_AUTO 0 0 
read_1 $add3
read_1 $add4
read_1 $add3
#poll $SET_AUTO 0 0 
step1 $add3 0
poll $SET_AUTO 0 0 
#poll $SET_AUTO 0 0 
read_1 $add5
read_1 $add6
read_1 $add5
read_1 $add6
read_1 $add6
resume 0
poll $SET_AUTO 0 0 
halt
poll $SET_AUTO 0 1 
poll $SET_AUTO 0 1 
read_1 $add7

for {set i 0} {$i < 6} {incr i} {
  poll $SET_AUTO 0 0 
}

cleanup
