# test
set add1 0x08000226
set add2 0x080035e2
set add3 0x080040e8
set add4 0x0800022a
set add5 0x080040ea
set SET_AUTO 0x200

setup
read_mem  0xe0042004 4 1
write_mem 0xe0042004 4 7
read_mem  0xe0042008 4 1
write_mem 0xe0042008 4 0x00001800

halt
read_all_reg 0
trace 20 0
resume 0
halt
read_all_reg 0
resume 0
cleanup
