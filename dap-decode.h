#ifndef _DAP_DECODE_H_
#define _DAP_DECODE_H_

struct t_dap;

struct t_dap_dec
{
  char *name;
  uint count;
  void (*fdsend)(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer);
  void (*fdrec) (struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer);
  int  lsend; // length of send buffer if fixed
  int  lrrc;  // length of receive buffer if fixed
  int  (*flsend)(struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer);
  int  (*flrec) (struct t_dap *pdap,uchar *pd,int len,struct t_decode *pdec,int transfer);
};

void initdap(struct t_decode *pdec,int ndap);
void enddap(struct t_decode *pdec);
void decodedap(uchar *pd,int len,struct t_decode *pdec,int transfer);
void decoderepdap(uchar *pd,int len,struct t_decode *pdec,int transfer,int nomod);
void dap_dump(void);
void dap_set_s_mes(struct t_decode *pdec,uchar *smes,int len,uint id);
void dap_set_prefix(struct t_decode *pdec,char *s_pre,char *r_pre);
struct t_multidap *new_multidap(int ndap);

#endif
