#ifndef _DISASM_ENV_H
#define _DISASM_ENV_H

#include <libusb-1.0/libusb.h>
#include "driversub.h"

// cpu read/write op cause
#define OP_FETCH  0
#define OP_EXEC   1 // cpu + dma
#define OP_NULL   2
#define OP_DEB    3
#define OP_RESET  4
#define OP_INTE   5
#define OP_PROG   6
#define OP_SIMUL  7
#define OP_MAX    OP_SIMUL

// see Debug Core Register Selector Register, DCRSR
#define SP_RN         13
#define LR_RN         14
#define PC_RN         15
#define XPSR_RN       16 
#define MAIN_SP_RN    17 
#define PROCESS_SP_RN 18 
#define CONTROL_RN    20 
#define FAULTMASK_RN  20 
#define BASEPRI_RN    20
#define PRIMASK_RN    20
#define FPCSR_RN      33 

// core register in compact array
#define SP_IDX         13 
#define LR_IDX         14 
#define PC_IDX         15 
#define XPSR_IDX       16   
#define MAIN_SP_IDX    17         
#define PROCESS_SP_IDX 18 
#define CONTROL_IDX    19 
#define FAULTMASK_IDX  19 
#define BASEPRI_IDX    19
#define PRIMASK_IDX    19
#define FPCSR_IDX      20 

struct t_core_reg
{
  uint r[21];
  uint s[32];
};

struct t_cpu 
{
  uint   execcount;
  uint   execpc;
  int    N,Z,C,V,I,IT,ISR;
// TODO substitute stlink_reg with t_core_reg
// but it requeres a lot of changes
//struct t_core_reg regp,regp0;
  struct stlink_reg regp,regp0;
  uchar  logd,logskip,logflags;
  uchar  init;
};

struct t_stat  // status
{
  uint   pc;
  int    il;         // instruction length (2 or 4)
  uint   inst;       // inst value 2 or 4 bytes
  int    nf;         // # of fields
  int    fields[NFIELDS]; // fields
  void   *efields[NEFIELDS]; // extra fields
  int    swi;        // switch index
  int    exe;        // exec inst? IT logic
//uint   eclock;     // extra clocks
  struct t_dec *pd;
// dump(disas) inst flags
  int    prefix;     // prefix out
  int    skip;       // skip logic
  int    doline;     // emit \n
//struct t_as *p_asfetch; // fetch as
};

void logprefix(struct t_cpu *p_cpu,int l);
void exec_error(struct t_cpu *p_c,char *e,...);
void exec_warning(struct t_cpu *p_c,char *e,...);
void warning(char *e,...);
char *binbuf(int n,int l,char *buf);
int  regr(struct t_cpu *p_cpu,int,int);

#endif // _DISASM_ENV_H
