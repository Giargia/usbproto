#ifndef _ENV_H_
#define _ENV_H_
#ifdef __INTER
#define FILE int
#else

/*
#include <windows.h>
#include <dbghelp.h>
#include <assert.h>
#include <imagehlp.h>
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>

typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned int uint;
typedef long long tprio;
#endif

#include "util.h"

#define KILO 1024
#define MEGA (KILO*KILO)
#define GIGA (MEGA*K)

#define MB  0xff        // Byte mask
#define MSB 0x80        // Byte sign mask
#define MW  0xffff      // Word mask
#define MSW 0x8000      // Word sign mask
#define M3  0xffffff    // 3 byte mask

// # elemnt of array
#define SIZE(v) (sizeof(v)/sizeof(v[0]))

#endif
